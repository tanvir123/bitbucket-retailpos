<?php
class unit
{
    public $module='unit';
    public $log;
    private $language,$lang_arr,$default_lang_arr;
    
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }
	public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(25,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(25);

            $ObjUserDao = new \database\unitdao();
            $data = $ObjUserDao->unitlist(50,'0','');
			$template = $twig->loadTemplate('unit.html');
            $this->loadLang();	
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
			$senderarr['module'] = $this->module;
			$senderarr['datalist'] = $data;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	
	public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
            $flag = \util\validate::check_notnull($data,array('name','shortcode','unit'));
            if($flag=='true')
            {
                $reqarr = array(
                            'name'=>$data['name'],
                            'shortcode'=>$data['shortcode'],
                            'unit'=>$data['unit'],
                            'measuretype'=>$data['measuretype'],
                            'is_systemdefined'=>0,
                            'rdo_status'=>$data['rdo_status'],
                            "id"=>$data['id'],
                            "module" => $this->module
                        );
                $this->loadLang();
                $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);
				$ObjUnitDao = new \database\unitdao();
				$data = $ObjUnitDao->addUnit($reqarr,$languageArr,$defaultlanguageArr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>$this->default_lang_arr['Missing_Field']));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }

    public function getrelatedRecords($data)
    {
        try {
            $this->log->logIt($this->module . " - getrelatedRecords");
            global $twig;
            global $commonurl;
            $flag = 0;
            if (!isset($data) || !isset($data['id']) || !isset($data['module'])) {
                $flag == 1;
                $Obj = "";
            } else {
                $ObjrawDao = new \database\unitdao();
                $res = $ObjrawDao->getrelatedRecords($data);
                $bj = json_decode($res, 1);
                $Obj = $bj['Data'];
            }
            $this->loadLang();
            $template = $twig->loadTemplate('dependency.html');
            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['flag'] = $flag;
            $senderarr['rec'] = $Obj;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $ret = array('data' => $template->render($senderarr));
            return json_encode($ret);
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getrelatedRecords - " . $e);
            return false;
        }
    }

	public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            global $twig;
            $limit=50;
            $offset=0;
            $name="";
            
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
			
            $ObjUserDao = new \database\unitdao();
			$data = $ObjUserDao->unitlist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	
	public function getUnitRec($data)
    {
        try {
            $this->log->logIt($this->module . " - getUnitRec");
            $ObjUnitDao = new \database\unitdao();
            $data = $ObjUnitDao->getUnitRec($data);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getUnitRec - " . $e);
            return false;
        }
    }
    public function getAllUnit()
    {
        try {
            $this->log->logIt($this->module . " - getAllUnit");
            $ObjUnitDao = new \database\unitdao();
            $data = $ObjUnitDao->getAllUnit();
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getAllUnit - " . $e);
            return false;
        }
    }

    public function loadLang()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$unit;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}
?>