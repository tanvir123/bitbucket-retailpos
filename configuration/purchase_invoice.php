<?php

class purchase_invoice
{
    public $module = 'purchase_invoice';
    public $log;
    private $language, $lang_arr, $default_lang_arr;
    public $ObjGrnDao;
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->ObjGrnDao = new \database\purchase_invoicedao();
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(68,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(68);
            $ObjCommonDao = new \database\commondao();

            $arr_purchase_orders = $this->ObjGrnDao->grnlist(50,'0' ,'', '', '', '','');
            $template = $twig->loadTemplate('purchase_invoice.html');

            $vendor_list = $this->ObjGrnDao->getAllVendorList();

            $currency_sign =$ObjCommonDao->getCurrencySignCompany();
            $indentNo = $ObjCommonDao->getIndentDocumentNumbering('good_receipt_note', '');
            
            $ObjCategoryDao = new \database\raw_categorydao();
            $catData = $ObjCategoryDao->getAllCategory();
            $cat_list = json_decode($catData, 1);
            
            $js_date = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $this->loadLang();
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['login_type'] = CONFIG_LOGINTYPE;
            $sender_arr['module'] = $this->module;
            $sender_arr['datalist'] = $arr_purchase_orders;
            $sender_arr['indentid'] = $indentNo;
            $sender_arr['category'] = $cat_list['Data'];
            $sender_arr['vendor_list'] = $vendor_list;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $sender_arr['langlist'] = json_decode($languageArr);
            $sender_arr['jsdateformat'] = $js_date;
            $sender_arr['roundoff'] = $round_off;
            $sender_arr['currencysign'] = $currency_sign;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $sender_arr['default_langlist'] = json_decode($defaultlanguageArr);
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            $sender_arr['lang_type'] = CONFIG_CUSTOM_LANG;
            $sender_arr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            echo $template->render($sender_arr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }


    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');

            $limit = 50;
            $offset = 0;
            $fromdate = "";
            $todate = "";
            $grnNo = "";
            $vendorid = "";
            $voucherno = "";

            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['fromdate']) && $data['fromdate'] != "")
                $fromdate = $data['fromdate'];
            if (isset($data['todate']) && $data['todate'] != "")
                $todate = $data['todate'];
            if (isset($data['grnno']) && $data['grnno'] != "")
                $grnNo = $data['grnno'];
            if (isset($data['voucherno']) && $data['voucherno'] != "")
                $voucherno = $data['voucherno'];
            if (isset($data['vendorid']) && $data['vendorid'] != "")
                $vendorid = $data['vendorid'];

            $data =  $this->ObjGrnDao->grnlist($limit, $offset, $fromdate, $todate, $grnNo, $vendorid,$voucherno);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }
    public function loadgrn($data)
    {
        try {
            $this->log->logIt($this->module." - loadgrn");
            global $twig;
            $vendor_list = $this->ObjGrnDao->getAllVendorList();

            $ObjCommonDao = new \database\commondao();
            $currency_sign =$ObjCommonDao->getCurrencySignCompany();

            if($data['id']=='' || $data['id'] ==0) {
                $grnNo = $ObjCommonDao->getIndentDocumentNumbering('good_receipt_note', '');
                $voucherNo = $ObjCommonDao->getIndentDocumentNumbering('grn_voucher_no', '');
            }
            else{ $grnNo=''; $voucherNo='';}
            $applicable_taxes = $this->ObjGrnDao->getAppliedTax();

            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $js_date = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $template = $twig->loadTemplate('grn_detail.html');
            $this->loadLang();
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['store'] = CONFIG_IS_STORE;
            $sender_arr['module'] = $this->module;
            $sender_arr['datalist'] = $data;
            $sender_arr['id'] = isset($data['id']) ? $data['id'] : 0;
            $sender_arr['orderid'] = isset($data['orderid']) ? $data['orderid'] : 0;
            $sender_arr['currencysign'] = $currency_sign;
            $sender_arr['grnNo'] = $grnNo;
            $sender_arr['voucherNo'] = $voucherNo;
            $sender_arr['vendor_list'] = $vendor_list;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $sender_arr['langlist'] = json_decode($languageArr);
            $sender_arr['applicable_taxes'] = $applicable_taxes;
            $sender_arr['jsdateformat'] = $js_date;
            $sender_arr['roundoff'] = $round_off;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $sender_arr['default_langlist'] = json_decode($defaultlanguageArr);
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            echo json_encode(array("Success" => "True", "Data" => $template->render($sender_arr)));
        } catch (Exception $e) {
            $this->log->logIt($this->module." - loadgrn - ".$e);
            return false;
        }
    }
    public function addGrn($data)
    {
        try {
            $this->log->logIt($this->module.' - addGrn');
            $req_arr = array(
                "grn_items" => $data['grn_items'],
                "total_amount" => $data['total_amount'],
                "voucher_no" => $data['voucher_no'],
                "grn_date" => $data['grn_date'],
                "grn_sel_vendor" => $data['vendor_id'],
                "grn_remarks" => $data['grn_remarks'],
                "id" => $data['id'],
                "module" => $this->module
            );
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr=json_decode($defaultlanguageArr);
            $result = $this->ObjGrnDao->addGrn($req_arr,$defaultlanguageArr);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module.' - addGrn - '.$e);
        }
    }
    public function addGrnFromPO($data)
    {
        try {
            $this->log->logIt($this->module.' - addGrnFromPO');
            $req_arr = array(
                "grn_items" => $data['grn_items'],
                "total_amount" => $data['total_amount'],
                "voucher_no" => $data['voucher_no'],
                "grn_date" => $data['grn_date'],
                "grn_sel_vendor" => $data['vendor_id'],
                "grn_remarks" => $data['grn_remarks'],
                "orderid" => $data['orderid'],
                "module" => $this->module
            );
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr=json_decode($defaultlanguageArr);
            $result = $this->ObjGrnDao->addGrnFromPO($req_arr,$defaultlanguageArr);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module.' - addGrnFromPO - '.$e);
        }
    }
    public function getgrnRecordsbyID($data)
    {
        try {
            $this->log->logIt($this->module . " - getgrnRecordsbyID");
            $data = $this->ObjGrnDao->getgrnRecordsbyID($data);

            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getgrnRecordsbyID - " . $e);
            return false;
        }
    }
    public function getVendorCategory($data)
    {
        try {
            $this->log->logIt($this->module . " - getVendorCategory");
            $result = $this->ObjGrnDao->getVendorCategory($data);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getVendorCategory - " . $e);
            return false;
        }
    }
    public function getRawMaterialByCategory($data)
    {
        try {
            $this->log->logIt($this->module . ' - getRawMaterialByCategory');
            $result = $this->ObjGrnDao->getRawMaterialFromCategory($data);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getRawMaterialByCategory - ' . $e);
        }
    }
    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$purchase_invoice;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
    public function printGrn($data)
    {
        try
        {
            $this->log->logIt($this->module.' - printGrn');

            $id = $data['id'];
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $headerData = $this->ObjGrnDao->printGrn($id,$defaultlanguageArr);
            if($headerData){
                $headerData['default_langlist'] = json_decode($defaultlanguageArr);
                $indentlanguage = new \util\language('AuthorizeReceivedindent');
                $llang_arr = \common\staticlang::$AuthorizeReceivedindent;
                $languageArr=html_entity_decode(json_encode($indentlanguage->loadlanguage($llang_arr)),ENT_QUOTES);
                $headerData['langlist'] = json_decode($languageArr);
            }
            $PrintReport = new \common\printreport();

            $PrintReport->addTemplate('purchase_invoice');
            $width = '793px';
            $height = '1122px';

            $PrintReport->addRecord($headerData);
            $data = $PrintReport->Request();
            return json_encode(array("Success"=>"True","Data"=>$data, "Height"=>$height, "Width"=>$width));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - printGrn - '.$e);
        }
    }
}

?>