<?php

/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 13/4/18
 * Time: 3:59 PM
 */
class store_settings
{
    public $module='store_settings';
    public $log;
    private $language,$lang_arr,$default_lang_arr,$store_language;
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->store_language = new \util\language('store_salutation');
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(63,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(63);
            $ObjStaticArray =new \common\staticarray();

            $ObjDao = new \database\storesettingsdao();
            $data = $ObjDao->loadStoreDisplaySettings();

            $data_arr = json_decode($data,true);
            $arr_timezones = $ObjStaticArray->timezones;
            $arr_date_format = $ObjStaticArray->default_dateformat;
            $arr_time_format = $ObjStaticArray->default_timeformat;

            $salutation_lang_arr = \common\staticlang::$store_salutation;
            $salutation = $this->store_language->loadlanguage($salutation_lang_arr);
            $salutationArr=html_entity_decode(json_encode($salutation),ENT_QUOTES);
            $salutationArr = (array)json_decode($salutationArr);

            $arr_round_off = $ObjStaticArray->roundoff;
            $template = $twig->loadTemplate('store_settings.html');
            $this->loadLang();
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['login_type'] = CONFIG_LOGINTYPE;
            $sender_arr['module'] = $this->module;
            $sender_arr['timezoneslist'] = $arr_timezones;
            $sender_arr['default_dateformat'] = $arr_date_format;
            $sender_arr['default_timeformat'] = $arr_time_format;
            $sender_arr['salutations'] = $salutationArr;
            $sender_arr['roundoff'] = $arr_round_off;
            $sender_arr['countrylist'] = $data_arr['Data']['countrylist'];
            $sender_arr['datalist'] = $data_arr['Data']['displaysettings'];
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $sender_arr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $sender_arr['default_langlist'] = json_decode($defaultlanguageArr);
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            $sender_arr['lang_type'] = CONFIG_CUSTOM_LANG;
            $sender_arr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];

            echo $template->render($sender_arr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function editStoreSettings($data)
    {
        try
        {
            $this->log->logIt($this->module.' - editStoreSettings');
            $flag = \util\validate::check_notnull($data,array('select_time_format','select_date_format','timezone','select_country','select_round_type','select_salutation','select_nationality'));
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr=json_encode($defaultlanguageArr);
            if($flag=='true'){
                $req_arr = array(
                    "select_time_format" => $data['select_time_format'],
                    "select_date_format"=>$data['select_date_format'],
                    "select_timezone"=>$data['timezone'],
                    "select_country"=>$data['select_country'],
                    "select_nationality"=>$data['select_nationality'],
                    "select_salutation"=>$data['select_salutation'],
                    "select_round_type"=>$data['select_round_type'],
                    "select_decimal_digit"=>$data['select_decimal_digit'],
                    "txtlimit"=>isset($data['txtlimit'])?$data['txtlimit']:"",
                    // "datetime"=> \util\util::getLocalDateTime(),
                    "module" => $this->module
                );
                $ObjDao = new \database\storesettingsdao();
                $result = $ObjDao->editStoreDisplaySettings($req_arr,$languageArr);
                return $result;
            }else
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->Missing_Field));
        }catch(Exception $e){
            $this->log->logIt($this->module.' - editStoreSettings - '.$e);
        }
    }

    public function loadLang()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$store_settings;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}
?>