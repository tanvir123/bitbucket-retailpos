<?php
class previewinvoice
{
    public $module='previewinvoice';
    public $log;
    private $language,$lang_arr,$default_lang_arr;
    public $jsdateformat;
    
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_PrintInvoice');
    }
    public function load($data)
    {
        try
        {
            $this->log->logIt($this->module.' - load');

            $dao = new \dao();
            $resarr = array();
            $todaysdate = \util\util::getTodaysDateTime();
            $date = \util\util::getTodaysDate();
            $this->loadLang();
            $languageArr = html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $language_Arr=json_decode($languageArr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
            $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_lang= json_decode($defaultlanguageArr);
            $PrintReport = new \common\printreport();

            $strSql = " SELECT companyname,currency_sign,registration_no1 AS NIT  FROM " . CONFIG_DBN . ".cfcompany WHERE companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $rec1 = $dao->executeRow();

            $strSql2 = " SELECT CFL.locationname, CFL.address1,CONCAT(CFL.address2,',',CFL.city,',',CFL.state) AS address, 
                         VWC.countryName, CFL.currency_sign, CFL.phone, CFL.fax, 
                         CFL.website FROM " . CONFIG_DBN . ".cflocation AS CFL
                         INNER JOIN " . CONFIG_DBN . ".vwcountry VWC ON VWC.id = CFL.country WHERE CFL.companyid=:companyid AND CFL.locationid=:locationid";
            $dao->initCommand($strSql2);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $rec2 = $dao->executeRow();


            $strSql2 = "SELECT meta_value FROM " . CONFIG_DBN . ". cflocationtemplate_meta  WHERE
                            companyid=:companyid AND locationid=:locationid AND meta_key=:meta_key 
                            ORDER BY locationmetaunkid DESC LIMIT 1";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->addParameter(':meta_key', "template".$data['id']);
            $template_detail= $dao->executeRow();
            if(isset($template_detail['meta_value']))
            {
                $template_detail=(array)json_decode($template_detail['meta_value']);
            }
            $resarr['companyinfo'] = $rec1;
            $resarr['locationinfo'] = $rec2;
            $resarr['company_logo'] =  CONFIG_BASE_URL."companyadmin/assets/company_logo/pureITES.jpg";
            $resarr['foliono'] = 'INV001';
            $resarr['tablename'] = '001';
            $resarr['todays_date'] = $todaysdate;

            if($data['id']=='3' && isset($template_detail))
            {
                $resarr['NIT'] = $template_detail['nit_number'];
                if($template_detail['main_start_no'] != '' && $template_detail['endno'] !='' )
                {
                    $resarr['Registration_detail'] =$language_Arr->LANG3." ".$language_Arr->LANG18." ".
                        $language_Arr->LANG37." ".$template_detail['dian_num']. " of".
                        " ".$template_detail['appliesfrom']." ".$language_Arr->LANG38." ". $default_lang->FROM. " ".$template_detail['main_start_no']." ". $default_lang->TO." ".$template_detail['endno'];                }
                 $resarr['billing_station'] = CONFIG_UID;
                $resarr['welcome_msg'] = $template_detail['welcomemsg'];
                $resarr['note'] = $template_detail['note'];
            }

            $resarr['user_name'] = CONFIG_UNM;

            $resarr['date'] = $date;
            $powered_by= '';

            $powered_by=CONFIG_POWERED_BY;
            if($powered_by =='')
                $powered_by ='Pure ITES';
            else
                $powered_by=CONFIG_POWERED_BY;

            $resarr['powered_by'] = $powered_by;



            if($data['id']==1){
                $PrintReport->addTemplate('sampleinvoice1');
                $width = '302px';
                $height = '793px';
            }
            if($data['id']==2){
                $PrintReport->addTemplate('sampleinvoice2');
                $width = '793px';
                $height = '1122px';
            }
            if($data['id']==3){
                $PrintReport->addTemplate('sampleinvoice3');
                $width = '302px';
                $height = '793px';
            }
            if($data['id']==4){
                $PrintReport->addTemplate('sampleinvoice4');
                $width = '302px';
                $height = '793px';
            }
            $ObjDao = new \database\displaysettingsdao();
            $tagDetail=$ObjDao->getTagrecords();
            if($tagDetail){
                $tagDetail=json_decode($tagDetail['meta_value'],1);
                $resarr['tagdetail']=$tagDetail;
            }
            $PrintReport->Language($languageArr);
            $PrintReport->FrontLanguage($defaultlanguageArr);
            $PrintReport->addRecord($resarr);

            $data = $PrintReport->Request();
            return json_encode(array("Success"=>"True","Data"=>$data, "Height"=>$height, "Width"=>$width));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_PrintInvoice;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }





}
?>