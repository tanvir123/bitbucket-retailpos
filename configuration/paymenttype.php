<?php
class paymenttype   
{
    public $module='paymenttype';
    public $log;
    private $language,$lang_arr,$default_lang_arr;
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_paymenttype');
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(3,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(3);

            $ObjPayTypeDao = new \database\paymenttypedao();
			$data = $ObjPayTypeDao->paymenttypelist(50,"0",'');
            $template = $twig->loadTemplate('paymenttype.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function addeditpaymenttype($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditpaymenttype');
            $flag = \util\validate::check_notnull($data,array('shortcode','paymentmethod','type','active','paymenttypeid'));
            if($flag=='true'){
                $reqarr = array(
                            "paymenttypeid" => $data['paymenttypeid'],
                            "shortcode"=>$data['shortcode'],
                            "paymentmethod"=>$data['paymentmethod'],
                            "type"=>$data['type'],
                            "active"=>$data['active'],
                            "module" => $this->module
                        );
                $ObjPaymentTypeDao = new \database\paymenttypedao();
                $this->loadLang();
                $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);
                $data = $ObjPaymentTypeDao->addPaymentType($reqarr,$languageArr,$defaultlanguageArr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>'Not Proper Record'));
                
        }catch(Exception $e){
            $this->log->logIt($this->module.' - addeditpaymenttype - '.$e);
        }
    }

    public function getPaymentTypeRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getPaymentTypeRec");
			$ObjPaymentTypeDao = new \database\paymenttypedao();
			$data = $ObjPaymentTypeDao->getPaymentTypeRec($data);
            return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getPaymentTypeRec - ".$e);
			return false; 
		}
	}
    public function getPaymentTypes($data)
    {
        try
        {
            $this->log->logIt($this->module." - getPaymentTypes");
            $ObjPaymentTypeDao = new \database\paymenttypedao();
            $result = $ObjPaymentTypeDao->getPaymentTypes($data);
            return $result;
        }catch(Exception $e){
            $this->log->logIt($this->module." - getPaymentTypes - ".$e);
            return false;
        }
    }
    public function getAllPaymentTypes()
    {
        try
        {
            $this->log->logIt($this->module." - getAllPaymentTypes");
            $ObjPaymentTypeDao = new \database\paymenttypedao();
            $result = $ObjPaymentTypeDao->getAllPaymentTypes();
            return $result;
        }catch(Exception $e){
            $this->log->logIt($this->module." - getAllPaymentTypes - ".$e);
            return false;
        }
    }
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            $ObjUserDao = new \database\paymenttypedao();
			$res = $ObjUserDao->paymenttypelist($limit,$offset,$name);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_paymenttype;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }

}
?>