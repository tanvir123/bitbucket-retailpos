<?php
class discount   
{
    public $module='discount';
    public $log;
    private $language,$lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_discount');
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(4,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(4);

            $ObjUserDao = new \database\menu_categorydao();
            $datacat = $ObjUserDao->categorylist('','','',1);
            $categorylist=json_decode($datacat,true);

            $this->loadLang();
            $jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $ObjUserDao = new \database\discountdao();
            $data = $ObjUserDao->disclist(50,"0",'');

            $Objtabinstadao = new \database\synctabinstadao();
            $tbdata = $Objtabinstadao->tabint();

            $template = $twig->loadTemplate('discount.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['datalist'] = $data;
            $senderarr['tablist'] = $tbdata;
            $senderarr['module'] = $this->module;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['categorylist'] = $categorylist[0]['data'];
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
            $flag1 = 'true';
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);
            $flag2 = \util\validate::check_notnull($data,array('shortname','discount','postrule','opendisc','rdo_status'));
            if($data['opendisc']=='0')
				$flag1 = \util\validate::check_numeric($data,array('value'));
            else
				$data['value'] = '';
			if($flag1=='true' && $flag2=='true'){
                $reqarr = array(
                            "shortname" => $data['shortname'],
                            "discount"=>$data['discount'],
                            "postrule"=>$data['postrule'],
                            "opendisc"=>$data['opendisc'],
                            "value"=>(isset($data['value']) && $data['value'] != '')?$data['value']:0,
                            "rdo_status"=>$data['rdo_status'],
                            "id"=>$data['id'],
                            "module" => $this->module,
                            "cat_discount" => isset($data['cat_discount'])?$data['cat_discount']:[],
                        );
                $ObjDao = new \database\discountdao();
                $data = $ObjDao->addDiscount($reqarr,$languageArr,$defaultlanguageArr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>SOME_FIELD_MISSING));
                    
        }catch(Exception $e){
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }

    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            global $twig;
            $limit=50;
            $offset=0;
            $name="";
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            $ObjDao = new \database\discountdao();
			$data = $ObjDao->disclist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    
    public function getDiscRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getDiscRec");
			$ObjDao = new \database\discountdao();
			$data = $ObjDao->getDiscRec($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getDiscRec - ".$e);
			return false; 
		}
	}
    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_discount;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }

    function getdiscountformtabinsta()
    {
        try
        {
            $this->log->logIt($this->module.'-getdiscountformtabinsta');
            $modulename = $this->module;

            $ObjDao = new \database\apisettingsdao();
            $data = $ObjDao->loadCompanyInfo();
            $data_arr = json_decode($data,true);
            $tabinsta_app_id = $data_arr['Data']['company_info']['tabinsta_app_id'];
            $tabinsta_app_secret = $data_arr['Data']['company_info']['tabinsta_app_secret'];

            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,CONFIG_DISCOUNTSYNC);
            curl_setopt($ch,CURLOPT_HTTPHEADER,array(
                "Content-Type:application/json",
                "app-id:$tabinsta_app_id",
                "app-secret:$tabinsta_app_secret",
            ));
            curl_setopt($ch,CURLOPT_HTTPGET,1);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            $res = curl_exec($ch);
            curl_close($ch);
            $data = trim($res);
            $dataa = json_decode($data,true);

            $listobj = new \database\synctabinstadao();
            $finaldata = $listobj->updatediscountfromtabinsta($dataa,$modulename);
            return $finaldata;

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-getdiscountformtabinsta -'.$e);
        }
    }


}
?>