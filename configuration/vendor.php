<?php
class vendor
{
    public $module='vendor';
    public $log;
    private $language,$lang_arr,$default_lang_arr,$store_language;
    
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->store_language = new \util\language('store_salutation');
    }
	public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(26,$this->module);
            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(26);

            $ObjUserDao = new \database\vendordao();
            $data = $ObjUserDao->vendorlist(50,'0','');
            $template = $twig->loadTemplate('vendor.html');

            $salutation_lang_arr = \common\staticlang::$store_salutation;
            $salutation = $this->store_language->loadlanguage($salutation_lang_arr);
            $salutationArr=html_entity_decode(json_encode($salutation),ENT_QUOTES);
            $salutationArr = (array)json_decode($salutationArr);

            $ObjCommonDao = new \database\commondao();
            $countrylist = $ObjCommonDao->getCountryList();

            $datalist = array();
            $datalist['salutation'] = \database\parameter::getParameter('salutation');
            $datalist['country'] = \database\parameter::getParameter('country');

            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
			$senderarr['module'] = $this->module;
            $senderarr['salutations'] = $salutationArr;
            $senderarr['countrylist'] = $countrylist;
            $senderarr['datalist'] = $data;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['dataarray'] = $datalist;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	
	public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
            $flag = \util\validate::check_notnull($data,array('name','company','email','mobile','reg_no'));
            if($flag=='true')
            {
                $reqarr = array(
                            'salutation'=>$data['salutation'],
                            'name'=>$data['name'],
                            'company'=>$data['company'],
                            'email'=>$data['email'],
                            'address'=>$data['address'],
                            'country'=>$data['country'],
                            'state'=>$data['state'],
                            'city'=>$data['city'],
                            'zip'=>$data['zip'],
                            'mobile'=>$data['mobile'],
                            'phone'=>$data['phone'],
                            'fax'=>$data['fax'],
                            'reg_no'=>$data['reg_no'],
                            'rdo_status'=>$data['rdo_status'],
                            "vendorid"=>$data['vendorid'],
                            "contactid"=>$data['contactid'],
                            "module" => $this->module
                        );
                $this->loadLang();
                $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);
				$ObjVendorDao = new \database\vendordao();
				$rec = $ObjVendorDao->addVendor($reqarr,$languageArr,$defaultlanguageArr);
                return $rec;
            }else
                return json_encode(array('Success'=>'False','Message'=>$this->default_lang_arr['Missing_Field']));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }
	
	public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            global $twig;
            $limit=50;
            $offset=0;
            $name="";
            
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
			
            $ObjUserDao = new \database\vendordao();
			$data = $ObjUserDao->vendorlist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function getrelatedRecords($data)
    {
        try {
            $this->log->logIt($this->module . " - getrelatedRecords");
            global $twig;
            global $commonurl;
            $flag = 0;
            if (!isset($data) || !isset($data['id']) || !isset($data['module'])) {
                $flag == 1;
                $Obj = "";
            } else {
                $ObjrawDao = new \database\vendordao();
                $res = $ObjrawDao->getrelatedRecords($data);
                $bj = json_decode($res, 1);
                $Obj = $bj['Data'];
            }
            $this->loadLang();
            $template = $twig->loadTemplate('dependency.html');
            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['flag'] = $flag;
            $senderarr['rec'] = $Obj;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $ret = array('data' => $template->render($senderarr));
            return json_encode($ret);
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getrelatedRecords - " . $e);
            return false;
        }
    }

	public function getVendorRec($data)
	{
		try
		{
            $this->log->logIt($this->module." - getVendorRec");
            $ObjVendorDao = new \database\vendordao();
			$data = $ObjVendorDao->getVendorRec($data);
            return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - getVendorRec - ".$e);
			return false; 
		}
	}
    public function getAllVendor()
    {
        try
        {
            $this->log->logIt($this->module." - getAllVendor");
            $ObjVendorDao = new \database\vendordao();
            $data = $ObjVendorDao->getAllVendor();
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getAllVendor - ".$e);
            return false;
        }
    }
     public function loadLang()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$vendor;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}
?>