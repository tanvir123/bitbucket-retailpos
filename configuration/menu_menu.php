<?php
require_once(dirname(__FILE__)."/common/s3fileUpload.php");

class menu_menu   
{
    public $module='menu_menu';
    public $log;
    private $language,$lang_arr,$default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_menu_menu');
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(6,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(6);

            $Zomatosetting = $OBJCOMMONDAO->getZomatosetting();

            $jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $ObjUserDao = new \database\menu_menudao();
            $data = $ObjUserDao->menulist(50,'0','');

            $Objtabinstadao = new \database\synctabinstadao();
            $tbdata = $Objtabinstadao->tabint();
            $this->loadLang();
            $template = $twig->loadTemplate('menu_menu.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['datalist'] = $data;
            $senderarr['tablist'] = $tbdata;
			$senderarr['module'] = $this->module;
            $senderarr['jsdateformat'] = $jsdate;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['is_zomato'] = $Zomatosetting;
            $senderarr['round_off'] = $round_off;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    
    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
			$flag = \util\validate::check_notnull($data,array('name','rdo_status'));
			$this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $OBJCOMMONDAO = new \database\commondao();

            /* zomato menu Schedules time */

            $Zomatosetting = $OBJCOMMONDAO->getZomatosetting();

            $timings = [];

            if($Zomatosetting == 1)
            {

                $startDate  = isset($data['startDate']) && $data['startDate'] !=''?$data['startDate']:'';
                $endDate  = isset($data['endDate']) && $data['endDate'] !=''?$data['endDate']:'';

                if($startDate != '' && $endDate != ''){
                    if(strtotime($endDate) < strtotime($startDate)){
                         return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG40));
                    }else{
                        $timings['startDate'] = $startDate;
                        $timings['endDate'] = $endDate;
                    }
                 }else{
                    $timings['startDate']  = $timings['endDate'] = '';
                }

                $timelist = [1,2,3];
                for ($i=0;$i<count($timelist);$i++) {
                    $day = (string)$timelist[$i][0];

                    $fromtime1 = isset($data['time' . $timelist[$i].'From'])?$data['time' . $timelist[$i].'From']:'';
                    $totime1 = isset($data['time' . $timelist[$i].'To'])?$data['time' . $timelist[$i].'To']:'';

                    if($fromtime1 != '' && $totime1 != ''){
                        $fromtime = strtotime($fromtime1);
                        $totime = strtotime($totime1);

                        if($fromtime>=$totime)                    {
                            return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG25));
                        }else{
                            $timings['time' . $timelist[$i].'From'] = $data['time' . $timelist[$i].'From'];
                            $timings['time' . $timelist[$i].'To'] = $data['time' . $timelist[$i].'To'];
                        }
                    }else{
                        $timings['time' . $timelist[$i].'From']  = $timings['time' . $timelist[$i].'To'] = '';
                    }

                }

                $daylist = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
                $cnt=0;

                for ($i=0;$i<count($daylist);$i++) {
                    $day = 'day'.$daylist[$i];
                    if(isset($data[$day])){
                        $true = 'true';
                        $cnt++;
                    }else{
                        $true = 'false';
                    }
                    $timings[$day] = $true;
                }
                $timings['service'] = 'delivery';
                if($cnt == 0){
                    return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG39));
                }
            }

            if($flag=='true') {
                //$bucket = CONFIG_BUCKET_NAME;

                //$directoryname = 'pure-ipos-menu.images';
                $flag_file = isset($_FILES['upload_img']) ? $_FILES['upload_img'] : "";

                $img_flag = "";
                if ($flag_file['tmp_name'] != "") {
                    $directoryname = dirname(__FILE__).'/imageconfiguration/';
                    if(!is_dir($directoryname)){
                        //Directory does not exist, so lets create it.
                        mkdir($directoryname, 0777, true);
                    }
                    $temporary = explode(".", $flag_file["name"]);
                    $file_extension = end($temporary);
                    $flag_name = time().'-'.$flag_file["name"];
                    $flag_dirname = dirname(__FILE__).'/imageconfiguration/'.$flag_name;
                    $flag_db = "";
//                    $flag_name = time() . '-' . $flag_file["name"];
                    if ((($flag_file["type"] == "image/png") || ($flag_file["type"] == "image/jpg") || ($flag_file["type"] == "image/jpeg"))
                        && ($flag_file["size"] < 24800000))
                    {
                        move_uploaded_file($flag_file["tmp_name"],$flag_dirname);
                        $flag_db = $flag_name;
//                        $arr_file = array();
//                        $arr_file[0]['name'] = $directoryname . '/' . $flag_name;
//                        $arr_file[0]['tmp_name'] = $flag_file['tmp_name'];
//                        $arr_file[0]['type'] = $flag_file["type"];
//
//                        $ObjFile = new s3fileUpload();
//                        $ObjFile->createBucket($bucket);
//                        $resFile = $ObjFile->uploadFiles($arr_file, $bucket);
//                        if ($resFile != '' && $resFile != 0) {
//                            $img_flag = urldecode($resFile[0]);
                    }
                    $img_flag = (file_exists("imageconfiguration/$flag_db"))?$flag_db:"";
//                    else {
//                            $img_flag = '';
//                        }
                    }
                    if($img_flag!="")
                    {
                        $imagename = CONFIG_COMMON_URL.'imageconfiguration/'.$data['imagephoto'];
                        if($imagename!="")
                        {
                            $path =CONFIG_COMMON_URL.'imageconfiguration/'.$imagename;
                            if(file_exists($path))
                            {
                                unlink($path);
                            }
                        }
                        $res = CONFIG_COMMON_URL.'imageconfiguration/'.$img_flag; //add
                    }else{
                        $res = $data['imagephoto'];
                    }



//                if ($img_flag != "") {
//                    $imagename = $data['imagephoto'];
//                    if ($imagename != "") {
//                        $rmv_file = array();
//                        $rmv_file[] = $imagename;
//                        $ObjFile = new s3fileUpload();
//                        $ObjFile->deleteFiles($rmv_file, $bucket);
//                    }
//                    $res = $img_flag;
//                } else {
//                    $res = $data['imagephoto'];
//                }

                $reqarr = array(
					"name" => $data['name'],
					"longdescription" => $data['longdescription'],
					"image" => $res,
					"rdo_status"=>$data['rdo_status'],
					"id"=>$data['id'],
					"timings"=>isset($timings)?htmlspecialchars(json_encode($timings)):"",
					"zomato_itemrate"=>isset($data['zomato_itemrate'])?$data['zomato_itemrate']:0,
                    "module" => $this->module
				);
                $ObjMenuDao = new \database\menu_menudao();
				$data = $ObjMenuDao->addMenu($reqarr,$languageArr,$defaultlanguageArr);
				return $data;
            }else{
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
            }
		}
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }

	
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            $ObjUserDao = new \database\menu_menudao();
			$data = $ObjUserDao->menulist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    
	public function getMenuRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getMenuRec");
			$ObjMenuDao = new \database\menu_menudao();
			$data = $ObjMenuDao->getMenuRec($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getMenuRec - ".$e);
			return false; 
		}
	}

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_menu_menu;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }

    public function syncmenu()
    {
        try
        {
            $this->log->logIt($this->module.'-syncmenu');
            $modulename = $this->module;
            $location = \database\parameter::getparameter('tabinsta_locationid',CONFIG_CID,CONFIG_LID);

            $ObjDao = new \database\apisettingsdao();
            $data = $ObjDao->loadCompanyInfo();
            $data_arr = json_decode($data,true);
            $tabinsta_app_id = $data_arr['Data']['company_info']['tabinsta_app_id'];
            $tabinsta_app_secret = $data_arr['Data']['company_info']['tabinsta_app_secret'];

            $arr = array(
                "location_id"=>$location
            );

            $arr_str = json_encode($arr);

            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,CONFIG_MENUSYNC);
            curl_setopt($ch,CURLOPT_HTTPHEADER,array(
                        "Content-Type:application/json",
                         "app-id:$tabinsta_app_id",
                         "app-secret:$tabinsta_app_secret",
                ));
            curl_setopt($ch,CURLOPT_POST,1);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$arr_str);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            $res = curl_exec($ch);
            curl_close($ch);
            $data = trim($res);
            $dataa = json_decode($data,true);

            $listobj = new \database\synctabinstadao();
            $finaldata = $listobj->updatemenufromtabinsta($dataa,$modulename);
            return $finaldata;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-syncmenu'.$e);
        }
    }

    public function getCategory($data)
    {
        try
        {
            $this->log->logIt($this->module." - getCategory");
             $ObjMenuDao = new \database\menu_menudao();
            $data = $ObjMenuDao->getCategory($data);
            return $data;
        }catch(Exception $e){
            $this->log->logIt($this->module." - getCategory - ".$e);
            return false;
        }
    }
    public function addcategory($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addcategory');
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);
                $reqarr = array(
                    "breakfast_category_id" => $data['breakfast_id'],
                    "lunch_category_id"=>$data['lunch_id'],
                    "dinner_category_id"=>$data['dinner_id'],
                    "breakfast_name"=>$data['breakfast_name'],
                    "lunch_name"=>$data['lunch_name'],
                    "dinner_name"=>$data['dinner_name'],
                    "id"=>$data['id'],
                    "module" => $this->module
                );
                $this->loadLang();

                $ObjMenuDao = new \database\menu_menudao();
                $data = $ObjMenuDao->addCategory($reqarr,$languageArr,$defaultlanguageArr);
                return $data;

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addcategory - '.$e);
        }
    }

    public function syncMenutoZoamto()
    {
        try
        {
            $this->log->logIt($this->module.'-syncMenutoZoamto');

            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $langlist = json_decode($languageArr);

            $ZomatoApiIntegrateDao = new \database\zomato_api_integrationdao();
            $data = $ZomatoApiIntegrateDao->syncMenu($langlist);

            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-syncMenutoZoamto'.$e);
        }
    }
}


?>