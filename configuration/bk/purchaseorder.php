<?php

class purchaseorder
{
    public $module = 'purchaseorder';
    public $log;
    private $language, $lang_arr, $default_lang_arr;
    public $ObjPurchaseDao;
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->ObjPurchaseDao = new \database\purchaseorderdao();
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(28, $this->module);
            $ObjCommonDao = new \database\commondao();

            $arr_purchase_orders = json_encode(array(array("cnt" => 0, "data" => [])));
            $template = $twig->loadTemplate('purchaseorder.html');

            $vendor_list = $this->ObjPurchaseDao->getAllVendorList();

            $currency_sign =$ObjCommonDao->getCurrencySignCompany();
            $indentNo = $ObjCommonDao->getIndentDocumentNumbering('purchase_order', '');
            
            $ObjCategoryDao = new \database\raw_categorydao();
            $catData = $ObjCategoryDao->getAllCategory('');
            $cat_list = json_decode($catData, 1);
            
            $js_date = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $this->loadLang();
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['login_type'] = CONFIG_LOGINTYPE;
            $sender_arr['module'] = $this->module;
            $sender_arr['datalist'] = $arr_purchase_orders;
            $sender_arr['indentid'] = $indentNo;
            $sender_arr['category'] = $cat_list['Data'];
            $sender_arr['vendor_list'] = $vendor_list;
            $sender_arr['langlist'] = $this->lang_arr;
            $sender_arr['jsdateformat'] = $js_date;
            $sender_arr['roundoff'] = $round_off;
            $sender_arr['currencysign'] = $currency_sign;
            $sender_arr['default_langlist'] = $this->default_lang_arr;
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            $sender_arr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($sender_arr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');

            $limit = 50;
            $offset = 0;
            $fromdate = "";
            $todate = "";
            $indent = "";
            $storeid = "";
            $statusid = "";

            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['fromdate']) && $data['fromdate'] != "")
                $fromdate = $data['fromdate'];
            if (isset($data['todate']) && $data['todate'] != "")
                $todate = $data['todate'];
            if (isset($data['indent']) && $data['indent'] != "")
                $indent = $data['indent'];
            if (isset($data['locationid']) && $data['locationid'] != "")
                $storeid = $data['locationid'];
            if (isset($data['statusid']) && $data['statusid'] != "")
                $statusid = $data['statusid'];

            $ObjUserDao = new \database\indentrequestdao();
            $data = $ObjUserDao->indentlist($limit, $offset, $fromdate, $todate, $indent, $storeid, $statusid);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function loadPurchaseOrder($data)
    {
        try {
            $this->log->logIt($this->module." - loadPurchaseOrder");
            global $twig;
            $vendor_list = $this->ObjPurchaseDao->getAllVendorList();

            $ObjCommonDao = new \database\commondao();
            $currency_sign =$ObjCommonDao->getCurrencySignCompany();

            $indentNo = $ObjCommonDao->getIndentDocumentNumbering('purchase_order', '');
            $applicable_taxes = $this->ObjPurchaseDao->getAppliedTax();

            $js_date = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $template = $twig->loadTemplate('purchaseorderrequest.html');
            $this->loadLang();
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['store'] = CONFIG_IS_STORE;
            $sender_arr['module'] = $this->module;
            $sender_arr['datalist'] = $data;
            $sender_arr['id'] = $data['id'];
            $sender_arr['currencysign'] = $currency_sign;
            $sender_arr['indentid'] = $indentNo;
            $sender_arr['vendor_list'] = $vendor_list;
            $sender_arr['langlist'] = $this->lang_arr;
            $sender_arr['applicatble_taxes'] = $applicable_taxes;
            $sender_arr['jsdateformat'] = $js_date;
            $sender_arr['default_langlist'] = $this->default_lang_arr;
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            echo json_encode(array("Success" => "True", "Data" => $template->render($sender_arr)));
        } catch (Exception $e) {
            $this->log->logIt($this->module." - loadPurchaseOrder - ".$e);
            return false;
        }
    }

    public function getrawmaterialoncategory($data)
    {
        try {
            $this->log->logIt($this->module . ' - getrawmaterialoncategory');
            $data = $this->ObjPurchaseDao->getrawmaterialoncategory($data);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getrawmaterialoncategory - ' . $e);
        }
    }



    public function getUnitRate($data)
    {
        try {
            $this->log->logIt($this->module . " - getUnitRate");
            $ObjUnitDao = new \database\purchaseorderdao();
            $data = $ObjUnitDao->getUnitRate($data);

            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getUnitRate - " . $e);
            return false;
        }
    }
	
	public function getVendorCategory($data)
	{
		try {
			$this->log->logIt($this->module . " - getVendorCategory");
			$ObjUnitDao = new \database\purchaseorderdao();
			$result = $ObjUnitDao->getVendorCategory($data);
			return $result;
		} catch (Exception $e) {
			$this->log->logIt($this->module . " - getVendorCategory - " . $e);
			return false;
		}
	}

    public function getIndentRecordsonID($data)
    {
        try {
            $this->log->logIt($this->module . " - getIndentRecordsonID");
            $ObjUnitDao = new \database\indentrequestdao();
            $data = $ObjUnitDao->getIndentRecordsonID($data);

            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getIndentRecordsonID - " . $e);
            return false;
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $arr = array(
                "LANG1" => "Purchase Order",
                "LANG2" => "Name",
                "LANG3" => "Enter Name",
                "LANG4" => "No.",
                "LANG5" => "Date",
                "LANG6" => "Category",
                "LANG7" => "Category",
                "LANG8" => "Item",
                "LANG9" => "Item Name",
                "LANG10" => "Qty",
                "LANG11" => "Unit",
                "LANG12" => "Remarks",
                "LANG13" => "Rate(Per Unit)",
                "LANG14" => "Total",
                "LANG15" => "Amount",
                "LANG16" => "Vendor",
                "LANG17" => "Edit",
                "LANG18" => "Update",
                "LANG19" => "Total Amount",
                "LANG20" => "Reason",
                "LANG22" => "Delete",
                "LANG23" => "Store",
                "LANG24" => "Status",
                "LANG25" => "Remark",
                "LANG26" => "Amount",
                "LANG27" => "Start Date",
                "LANG28" => "End Date",
                "LANG29" => "Vendor",
                "LANG30" => "Pending",
                "LANG31" => "Partially Completed",
                "LANG32" => "Completed",
                "LANG33" => "Add",
                "LANG34" => "No.",
                "LANG35" => "Cancel",
            );
            $this->lang_arr = $this->language->loadlanguage($arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}

?>