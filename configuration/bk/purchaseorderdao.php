<?php

namespace database;

class purchaseorderdao
{
    public $module = 'DB_purchaseorder';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function purchaselist($limit, $offset, $fromdate, $todate, $indent, $storeid, $statusid)
    {
        try {

            $this->log->logIt($this->module . ' - purchaselist');

            $objutil = new \util\util;
            $fromdate = $objutil->convertDateToMySql($fromdate);
            $todate = $objutil->convertDateToMySql($todate);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            // $round_off = \database\parameter::getParameter('digitafterdecimal');
            // IFNULL(ROUND(CRI.totalamount, $round_off),0) AS totalamount1


            $strSql = "SELECT CRI.*,
             CASE 
                        WHEN CRI.status=0 THEN 'Pending'
                        WHEN CRI.status=1 THEN 'Partially Completed'                       
                        ELSE  'Completed'
                        END as indent_stat,
            CL.storename, IFNULL(DATE_FORMAT(CRI.indent_date,'" . $mysqlformat . "'),'') AS indentdate,IFNULL(CFU1.username,'') AS createduser,
                                    IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CRI.createddatetime,'" . $mysqlformat . "'),'') AS created_date,
                                    IFNULL(DATE_FORMAT(CRI.modifieddatetime,'" . $mysqlformat . "'),'') AS modified_date                                   
                                    FROM " . CONFIG_DBN . ".cfrequestindent AS CRI
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU1 ON CRI.createduser=CFU1.userunkid AND CRI.companyid=CFU1.companyid
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU2 ON CRI.modifieduser=CFU2.userunkid AND CRI.modifieduser=CFU2.userunkid AND CRI.companyid=CFU2.companyid
                                    LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS CL ON CRI.recstoreid=CL.storeunkid AND CRI.companyid=CL.companyid
                                    WHERE CRI.companyid=:companyid AND CRI.storeid=:storeid AND CRI.is_deleted=0";

            if ($indent != "") {
                $strSql .= " AND CRI.indent_doc_num LIKE '%" . $indent . "%'";
            }
            if ($storeid != 0) {
                $strSql .= " AND CRI.recstoreid LIKE '%" . $storeid . "%'";
            }
            if ($statusid != 0) {
                $strSql .= " AND CRI.status LIKE '%" . $statusid . "%'";
            }
            if ($fromdate != "" && $todate != "") {
                $strSql .= " AND CRI.indent_date  BETWEEN  '" . $fromdate . "' AND '" . $todate . "' ";
            }


            $strSql .= " ORDER BY CRI.indentid DESC";
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $data = $dao->executeQuery();
            $this->log->logIt($data);
            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
                $dao->initCommand($strSql);

            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return json_encode($retvalue);
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return json_encode($retvalue);
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - indentlist - ' . $e);
        }
    }

    public function getrawmaterialoncategory($data)
    {
        try {
            $this->log->logIt($this->module . " - getrawmaterialoncategory");
            $dao = new \dao();

            $strSql = " SELECT id,name,rateperunit FROM " . CONFIG_DBN . ".cfrawmaterial
             WHERE is_active=1 and is_deleted=0 AND lnkcategoryid=:lnkcategoryid AND companyid=:companyid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':lnkcategoryid', $data['rawcatid']);

            $res = $dao->executeQuery();

            return json_encode(array("Success" => "True", "Data" => $res));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getrawmaterialoncategory - " . $e);
            return false;
        }
    }

    public function getUnitRate($data)
    {
        try {
            $this->log->logIt($this->module . " - getUnitRate");
            $dao = new \dao();
            //$round_off = \database\parameter::getParameter('digitafterdecimal');
            $strSql = " SELECT U.name,U.unitunkid,U.unit,CRL.rateperunit
              FROM " . CONFIG_DBN . ".cfrawmaterial CRM 
            LEFT JOIN " . CONFIG_DBN . ". cfunit U ON CRM.measuretype=U.measuretype AND U.companyid=:companyid AND U.is_active=1 and U.is_deleted=0
            LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial_loc CRL ON CRL.lnkrawmaterialid=CRM.id AND CRL.storeid=:storeid 
            WHERE CRM.id =:id AND CRM.companyid=:companyid ";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', $data['locationid']);
            $dao->addParameter(':id', $data['rawid']);

            $res = $dao->executeQuery();
            return json_encode(array("Success" => "True", "Data" => $res));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getUnitRate - " . $e);
            return false;
        }
    }

    public function getAllVendorList()
    {
        try {
            $this->log->logIt($this->module . '-getAllVendorList');
            $dao = new \dao();
            $strSql = "SELECT CFV.vandorunkid, TC.business_name FROM ".CONFIG_DBN.".cfvandor CFV INNER JOIN ".CONFIG_DBN.".trcontact TC ON TC.contactunkid = CFV.lnkcontactid WHERE CFV.companyid=:companyid AND  CFV.is_active=1 AND  CFV.is_deleted=0";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $rec = $dao->executeQuery();
            return $rec;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . '-getAllVendorList -' . $e);
        }
    }

    public function getVendorCategory($data)
    {
        try{
			$this->log->logIt($this->module.' - getVendorCategory');
			$categories = [];
			if(isset($data['vendorid']) && $data['vendorid']>0)
            {
				$dao = new \dao();
				$dao->initCommand("SELECT DISTINCT lnkcategoryid,CTRC.name as category_name FROM ".CONFIG_DBN.".cfrawmaterial CFR INNER JOIN ".CONFIG_DBN.".cfrawmaterial_category CTRC 
				    ON CTRC.id = CFR.lnkcategoryid AND CTRC.is_deleted=0 AND CTRC.companyid = :companyid
				     WHERE CFR.lnkvandorid = :lnkvandorid AND CFR.companyid = :companyid AND CFR.is_deleted=0 AND CFR.is_active=1 ORDER BY CTRC.name ASC ");
				$dao->addParameter(':lnkvandorid',$data['vendorid']);
				$dao->addParameter(':companyid',CONFIG_CID);
				$categories  = $dao->executeQuery();
            }
			return json_encode(array('Success' => 'True', 'Message' => 'Vendor categories','categories' => $categories));
        }catch (\Exception $e)
        {
			$this->log->logIt($this->module.' - getVendorCategory - '.$e);
        }
    }
	
	public function getAppliedTax()
	{
		try
		{
			$this->log->logIt($this->module.' - getAppliedTax');
			$todaysdate = \util\util::getLocalDate();
			$dao = new \dao();
			
			$strSql = " SELECT CASE WHEN CFTD.postingrule=1 THEN '%' WHEN CFTD.postingrule=2
                       THEN 'FLAT' END as pytype,CFTD.amount,CFT.taxunkid,CFT.lnkmasterunkid,CFTD.taxdetailunkid,CFT.tax,CFTD.postingrule,CFTD.taxapplyafter
                        FROM ".CONFIG_DBN.".cftax AS CFT
                        INNER JOIN ".CONFIG_DBN.".cftaxdetail CFTD
                          ON CFT.taxunkid = CFTD.taxunkid AND
                             CFTD.taxdetailunkid = (SELECT taxdetailunkid FROM ".CONFIG_DBN.".cftaxdetail WHERE taxunkid=CFT.taxunkid ORDER BY entrydatetime DESC LIMIT 1)
                        WHERE CFT.companyid =:companyid  AND CFT.isactive=1 AND CFT.is_deleted=0 AND CAST(CFTD.taxdate AS DATE)<=:todaysdate ";
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$strSql .= " AND CFT.locationid = :locationid ";
			}
			
			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$strSql .= " AND CFT.storeid = :storeid ";
			}
			
			$strSql .=" ORDER BY CFT.taxunkid";
			
			
			
			
			$dao->initCommand($strSql);
			$dao->addParameter(':todaysdate',$todaysdate);
			$dao->addParameter(':companyid',CONFIG_CID);
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$dao->addparameter(':locationid',CONFIG_LID);
			}
			
			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$dao->addparameter(':storeid',CONFIG_SID);
			}
			$res = $dao->executeQuery();
			return $res;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module.' - getAppliedTax - '.$e);
		}
	}
}

?>

