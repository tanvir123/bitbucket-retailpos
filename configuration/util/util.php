<?php
namespace util;
class util
{
	public $log;
	public $module = "Util";
	
	function __construct()
    {
        $this->log = new \util\logger();
    }

	function date_diffcal($dat1,$dat2)
	{
	  $tmp_dat1 = mktime(0,0,0,
		 substr($dat1,5,2),substr($dat1,8,2),substr($dat1,0,4));
	  $tmp_dat2 = mktime(0,0,0,
		 substr($dat2,5,2),substr($dat2,8,2),substr($dat2,0,4));

	  $yeardiff = date('Y',$tmp_dat1)-date('Y',$tmp_dat2);
	  $diff = date('z',$tmp_dat1)-date('z',$tmp_dat2) +
			   floor($yeardiff /4)*1461;

	  for ($yeardiff = $yeardiff % 4; $yeardiff>0; $yeardiff--)
	   {
		 $diff += 365 + date('L',
			 mktime(0,0,0,1,1,
			   intval(
				 substr(
				   (($tmp_dat1>$tmp_dat2) ? $dat1 : $dat2),0,4))
			   -$yeardiff+1));
	   }

	  return $diff;
	}

	public function getDateFormatWise($date='',$dateformat='')
	{
		switch ($dateformat) {
		case 'dd-mm-yy':
			list($checkin_day,$checkin_month,$checkin_year)=explode("-",$date);
			break;
		case 'mm-dd-yy':
			list($checkin_month,$checkin_day,$checkin_year)=explode("-",$date);
			break;
		case 'yy-mm-dd':
			list($checkin_year,$checkin_month,$checkin_day)=explode("-",$date);
			break;
		case 'yy-dd-mm':
			list($checkin_year,$checkin_day,$checkin_month)=explode("-",$date);
			break;
		case 'mm-yy-dd':
			list($checkin_month,$checkin_year,$checkin_day)=explode("-",$date);
			break;
		case 'dd-yy-mm':
			list($checkin_day,$checkin_year,$checkin_month)=explode("-",$date);
			break;
		case 'd MM yy':
			list($checkin_day,$month,$checkin_year)=explode(" ",$date);
			for($i=1;$i<=12;$i++){
			if(date("F", mktime(0, 0, 0, $i, 1, 0)) == $month){
				$month = (strlen($i)==1)?"0".$i:$i;
				break;
				}
			}
			$checkin_month=$month;
			break;
		case 'd MM,yy':
			$day=explode(" ",$date);
			$checkin_day=$day[0];
			$month=explode(",",$day[1]);
			$year=explode(",",$date);
			$checkin_year=$year[1];
			for($i=1;$i<=12;$i++){
			if(date("F", mktime(0, 0, 0, $i, 1, 0)) == $month[0]){
				$checkin_month = (strlen($i)==1)?"0".$i:$i;
				break;
				}
			}
			break;
		}
		$in_date1=mktime(0,0,0,$checkin_month,$checkin_day,$checkin_year);
		return $in_date1;
	}

	public function Config_Format_Date($date_val,$dateformat)
	{
			$date_list=date($dateformat, strtotime($date_val));
			return $date_list;
	}

	public function Config_Format_Time($Time_val,$timeformat)
	{
			$date_list=date($timeformat, strtotime($Time_val));
			return $date_list;
	}

	public function getNights($checkindate='',$checkoutdate)
	{
		list($checkin_year,$checkin_month,$checkin_day)=explode("-",$checkindate);
		list($checkout_year,$checkout_month,$checkout_day)=explode("-",$checkoutdate);
		$out_date1=mktime(0,0,0,$checkout_month,$checkout_day,$checkout_year);
		$in_date1=mktime(0,0,0,$checkin_month,$checkin_day,$checkin_year);
		$nights = abs(($out_date1 - $in_date1) / (3600 * 24));
		return $nights;
	}

	public function generateDateRange($strDateFrom='',$strDateTo='')
	{
		$aryRange=array();
		$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

	    if ($iDateTo>=$iDateFrom) {
			array_push($aryRange,date('Y-m-d',$iDateFrom));

			while ($iDateFrom<$iDateTo) {
			  $iDateFrom+=86400; // add 24 hours
			  array_push($aryRange,date('Y-m-d',$iDateFrom));
			  }
		}//if
		return $aryRange;
	}

	public function getFormattedNumber($value)
	{
        $roundoffdigit = \database\parameter::getParameter('digitafterdecimal');
		return number_format($value, $roundoffdigit);
	}

	public function getSemiFormattedNumber($value)
	{
        $roundoffdigit = \database\parameter::getParameter('digitafterdecimal');
		return number_format($value, $roundoffdigit);
	}

	public function convertMySqlTime($date)
	{
		$timeformat = \database\parameter::getParameter('timeformat');
		$date=trim($date);
		if(strlen($date)==0) return '';
		if(strlen($date)>10)
			$dt = date_create_from_format('Y-m-d H:i:s',$date);
		else
			$dt = date_create_from_format('H:i:s',$date);
		return $dt->format($timeformat);
	}

	function convert_line_breaks($string, $line_break=PHP_EOL) {
		$patterns = array(
				    "/(<br>|<br \/>|<br\/>)\s*/i",
				    "/(\r\n|\r|\n)/"
		);
		    $replacements = array(
					PHP_EOL,
					$line_break
		);
		$string = preg_replace($patterns, $replacements, $string);
		return $string;
	}

	public function in_arrayr( $needle, $haystack ) {
	$d='';$t=0;
		$cntsp=0;
	 	foreach( $haystack as $v ){
				foreach($v as $k=>$val)
				{
					if($k=='applyon_rateplan' && $val==$needle)
					{
						echo $needle."-".$cntsp."\n";
						return $needle."-".$cntsp;
						$t++;
						break;
					}
				}
				if($t>0)
					break;
			$cntsp++;
		}
	}

	public function in_array_booked( $needle, $haystack) {
		$log=new logger();
		#$log->LogIt("needle ".$needle);
		#$log->LogIt($haystack);
		if(!empty($haystack))
		{
			if(in_array($needle,$haystack))
			{
				#$log->LogIt($needle."  match.... ");
				#$log->LogIt($haystack);
				return 1;
			}
		}
		else
			return 0;
	}

	function hmac_sha1($key, $data)
	{
		// Adjust key to exactly 64 bytes
		if (strlen($key) > 64) {
			$key = str_pad(sha1($key, true), 64, chr(0));
		}
		if (strlen($key) < 64) {
			$key = str_pad($key, 64, chr(0));
		}

		// Outter and Inner pad
		$opad = str_repeat(chr(0x5C), 64);
		$ipad = str_repeat(chr(0x36), 64);

		// Xor key with opad & ipad
		for ($i = 0; $i < strlen($key); $i++) {
			$opad[$i] = $opad[$i] ^ $key[$i];
			$ipad[$i] = $ipad[$i] ^ $key[$i];
		}

		return sha1($opad.sha1($ipad.$data, true));
	}

	function fillArrayCombo($arrName, $selected='')
	{
		$strHTML = "";
		reset($arrName);
		while(list($key,$val) = each($arrName))
		{
			$strHTML .= "<option value=\"". $key. "\"";
			if($selected == $key)
				$strHTML .= " selected ";
			$strHTML .= ">".$val. "</option>";
		}
		return $strHTML;
	}

	public function convertDateToMySql($date)
	{
		$dateformat = \database\parameter::getParameter('dateformat');
		$datetimeformat = $dateformat.' '.\database\parameter::getParameter('timeformat');
		$date=trim($date);
		if(strlen($date)==0) return '';
		if(strlen($date)>10)
		{
			$dt = date_create_from_format($datetimeformat,$date);
		}
		else
		{
			$dt = date_create_from_format($dateformat,$date);
		}
		return $dt->format('Y-m-d');
	}

	public function getmysqlnplusminusdate($date,$number)
	{
		$date=trim($date);
		if(strlen($date)==0) return '';
		if(strlen($date)>10)
			$stop_date = date('Y-m-d H:i:s', strtotime($date . ' '.$number.' day'));
		else
			$stop_date = date('Y-m-d', strtotime($date . ' '.$number.' day'));
		return $stop_date;
	}

	public function DateDiff($d1,$d2)
	{
		$interval=date_diff(date_create(date('Y-m-d',strtotime($d1))),date_create(date('Y-m-d',strtotime($d2))));
		return $interval->format("%a");
	}

	public function getLocalDateTime()
	{
		$timezone = \database\parameter::getParameter('timezone');
		$timediff=explode(":",$timezone);
		$localdatetime = gmdate("Y-m-d H:i:s",mktime(date("H")+($timediff[0]),date("i")+($timediff[1]),date("s"),date("n"),date("j"),date("Y")));
		return $localdatetime;
	}
    public function getLocalDateTime_ISO()
    {
        $timezone = \database\parameter::getParameter('timezone');
        $timediff=explode(":",$timezone);
        $localdatetime = mktime(date("H")+($timediff[0]),date("i")+($timediff[1]),date("s"),date("n"),date("j"),date("Y"));
        return $localdatetime;
    }
	public function getTodaysDate()
	{
		$todaysdate = \database\parameter::getParameter('todaysdate');
		return $todaysdate;
	}
	public function getTodaysDateTime()
	{
		$todaysdate = \database\parameter::getParameter('todaysdate');
		$timezone = \database\parameter::getParameter('timezone');
		$timediff=explode(":",$timezone);
		$localdatetime = gmdate("H:i:s",mktime(date("H")+($timediff[0]),date("i")+($timediff[1]),date("s"),date("n"),date("j"),date("Y")));

		return $todaysdate." ".$localdatetime;
	}

	public function getLocalDate()
	{
		$timezone = \database\parameter::getParameter('timezone');
		$timediff=explode(":",$timezone);
		$localdatetime = gmdate("Y-m-d",mktime(date("H")+($timediff[0]),date("i")+($timediff[1]),date("s"),date("n"),date("j"),date("Y")));
		return $localdatetime;
	}
	public function getYearCode()
	{
		$timezone = \database\parameter::getParameter('timezone');
		$timediff=explode(":",$timezone);
		$localdatetime = gmdate("Y-m-d",mktime(date("H")+($timediff[0]),date("i")+($timediff[1]),date("s"),date("n"),date("j"),date("Y")));
		$date = \DateTime::createFromFormat("Y-m-d", $localdatetime);
		$year = $date->format("Y");
		return $year;
	}
	public function getLocalTime()
	{
		$timezone = \database\parameter::getParameter('timezone');
		$timediff=explode(":",$timezone);
		$localdatetime = gmdate("H:i:s",mktime(date("H")+($timediff[0]),date("i")+($timediff[1]),date("s"),date("n"),date("j"),date("Y")));
		return $localdatetime;
	}
    public static function getauthtoken($companyid=0)
    {
        $key = 'authdaisy';
        return $companyid.'00'.md5(uniqid(date("Y-m-d H:i:s").'-'.rand().'-'.$key, true));
    }
	#For ipay88 payment
	function iPay88_signature($source)
	{
		$hexSource=sha1($source);
		$strlen = strlen($hexSource);
		$bin ='';
		for($i=0;$i<strlen($hexSource);$i=$i+2)
		{
		  $bin.=chr(hexdec(substr($hexSource,$i,2)));
		}
		return base64_encode($bin);
	}


	function strip_html_tags( $text )
	{
		// PHP's strip_tags() function will remove tags, but it
		// doesn't remove scripts, styles, and other unwanted
		// invisible text between tags.  Also, as a prelude to
		// tokenizing the text, we need to insure that when
		// block-level tags (such as <p> or <div>) are removed,
		// neighboring words aren't joined.
		$text = preg_replace(
			array(
				// Remove invisible content
				'@<head[^>]*?>.*?</head>@siu',
				'@<style[^>]*?>.*?</style>@siu',
				'@<script[^>]*?.*?</script>@siu',
				'@<object[^>]*?.*?</object>@siu',
				'@<embed[^>]*?.*?</embed>@siu',
				'@<applet[^>]*?.*?</applet>@siu',
				'@<noframes[^>]*?.*?</noframes>@siu',
				'@<noscript[^>]*?.*?</noscript>@siu',
				'@<noembed[^>]*?.*?</noembed>@siu',

				// Add line breaks before & after blocks
				'@<((br)|(hr))@iu',
				'@</?((address)|(blockquote)|(center)|(del))@iu',
				'@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
				'@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
				'@</?((table)|(th)|(td)|(caption))@iu',
				'@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
				'@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
				'@</?((frameset)|(frame)|(iframe))@iu',
			),
			array(
				' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
				"\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
				"\n\$0", "\n\$0",
			),$text );

		// Remove all remaining tags and comments and return.
		return strip_tags($text);
	}

	function imageResize($width, $height, $target, $flag=1) {
	//takes the larger size of the width and height and applies the
	//formula accordingly...this is so this script will work
	//dynamically with any size image
	if($width > $height) {
	$percentage = ($target / $width);
	} else {
	$percentage = ($target / $height);
	}
	//gets the new value and applies the percentage, then rounds the value
	$width = round($width * $percentage);
	$height = round($height * $percentage);
	//returns the new sizes in html image tag format...this is so you
	//can plug this function inside an image tag and just get the
	if($flag==1)
		return "width=\"$width\" height=\"$height\"";
	else
		return "width=\"$width\"";
	}

	function getmonth ($month = null, $year = null)
	{
		  #The current month is used if none is supplied.
		  if (is_null($month))
			  $month = date('n');

		  #The current year is used if none is supplied.
		  if (is_null($year))
			  $year = date('Y');

		  #Verifying if the month exist
		  if (!checkdate($month, 1, $year))
			  return null;

		  #Calculating the days of the month
		  $first_of_month = mktime(0, 0, 0, $month, 1, $year);
		  $days_in_month = date('t', $first_of_month);
		  $last_of_month = mktime(0, 0, 0, $month, $days_in_month, $year);

		  $m = array();
		  $m['first_mday'] = 1;
		  $m['first_wday'] = date('w', $first_of_month);
		  $m['first_weekday'] = strftime('%A', $first_of_month);
		  $m['first_yday'] = date('z', $first_of_month);
		  $m['first_week'] = date('W', $first_of_month);
		  $m['last_mday'] = $days_in_month;
		  $m['last_wday'] = date('w', $last_of_month);
		  $m['last_weekday'] = strftime('%A', $last_of_month);
		  $m['last_yday'] = date('z', $last_of_month);
		  $m['last_week'] = date('W', $last_of_month);
		  $m['mon'] = $month;
		  $m['month'] = strftime('%B', $first_of_month);
		  $m['year'] = $year;

		  return $m;
	}

	function addDaysinDate($date,$days)
	{
		list($year,$month,$day)=explode("-",$date);
		$resdate=date('Y-m-d',mktime(0,0,0,$month,$day+($days),$year));
		return $resdate;
	}

	function removeDaysinDate($date,$days)
	{
		list($year,$month,$day)=explode("-",$date);
		$resdate=date('Y-m-d',mktime(0,0,0,$month,$day-($days),$year));
		return $resdate;
	}

	function resize($img, $w, $h, $newfilename)
	{
		 #Check if GD extension is loaded
		 if (!extension_loaded('gd') && !extension_loaded('gd2')) {
			trigger_error("GD is not loaded", E_USER_WARNING);
			return false;
		 }

		 #Get Image size info
		 $imgInfo = getimagesize($img);
		 switch ($imgInfo[2]) {
			case 1: $im = imagecreatefromgif($img); break;
			case 2: $im = imagecreatefromjpeg($img);  break;
			case 3: $im = imagecreatefrompng($img); break;
			default:  trigger_error('Unsupported filetype!', E_USER_WARNING);  break;
		 }

		 #If image dimension is smaller, do not resize
		 if ($imgInfo[0] <= $w && $imgInfo[1] <= $h) {
			$nHeight = $imgInfo[1];
			$nWidth = $imgInfo[0];
		 }else{
		  if ($w/$imgInfo[0] > $h/$imgInfo[1]) {
			$nWidth = $w;
			$nHeight = $imgInfo[1]*($w/$imgInfo[0]);
		  }else{
			$nWidth = $imgInfo[0]*($h/$imgInfo[1]);
			$nHeight = $h;
		  }
		 }

		 $nWidth = round($nWidth);
		 $nHeight = round($nHeight);
		 $newImg = imagecreatetruecolor($nWidth, $nHeight);

		 /* Check if this image is PNG or GIF, then set if Transparent*/
		 if(($imgInfo[2] == 1) OR ($imgInfo[2]==3)){
			  imagealphablending($newImg, true);
			  imagesavealpha($newImg,false);
			  $transparent = imagecolorallocate($newImg, 255, 255, 255);
			  imagefill($newImg,0,0,$transparent);
		 }

		 imagecopyresampled($newImg, $im, 0, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);

		 #Generate the file, and rename it to $newfilename
		 switch ($imgInfo[2]) {
			case 1: imagegif($newImg,$newfilename); break;
			case 2: imagejpeg($newImg,$newfilename);  break;
			case 3: imagepng($newImg,$newfilename); break;
			default:  trigger_error('Failed resize image!', E_USER_WARNING);  break;
		 }
	  return $newfilename;
	}

	public function VisitorIP()
	{
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$TheIp=$_SERVER['HTTP_X_FORWARDED_FOR'];
		else
			$TheIp=$_SERVER['REMOTE_ADDR'];

		$iparr = explode(',',$TheIp);

		if(count($iparr)>0)
		{
			$TheIp=trim($iparr[0]);
		}

		return trim($TheIp);
	}

	public function VisitorProxyIP()
	{
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$TheIp=$_SERVER['HTTP_X_FORWARDED_FOR'];
		else
			$TheIp=$_SERVER['REMOTE_ADDR'];

		return trim($TheIp);
	}

	public function getLocationFromIP($ip)
	{
		try
		{
			$result = array();
			$ip2location_data = ip2location_finder($ip);
			if(is_array($ip2location_data) && !empty($ip2location_data))
			{
				if($ip2location_data['RESPONSE'] == 'TIMEOUT')
				{
					return array();
				}
				else
				{
					$result['country'] = $ip2location_data['countryname'];
					$result['city'] = $ip2location_data['city'];
				}
			}
			else
			{
				return array();
			}
		}
		catch(Exception $e)
		{
			return array();
		}
		return $result;
	}

	public function convertMySqlDate($date)
	{
		$date=trim($date);
		$dateformat = \database\parameter::getParameter('dateformat');
		if(strlen($date)==0) return '';
		if(strlen($date)>10)
			$dt = date_create_from_format('Y-m-d H:i:s',$date);
		else
			$dt = date_create_from_format('Y-m-d',$date);
		return $dt->format($dateformat);
	}

	function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
	}

	function AllowOnlyNumeric($string)
	{
		if($string!='')
		{
			if (preg_match('/^[0-9]*$/', $string)) {
				return true;
			} else {
				return false;
			}
		}
		//return false;
	}

	function replace_between($str, $needle_start, $needle_end, $replacement) {
		$pos = strpos($str, $needle_start);
		$start = $pos === false ? 0 : $pos + strlen($needle_start);

		$pos = strpos($str, $needle_end, $start);
		$end = $start === false ? strlen($str) : $pos;

		return substr_replace($str,$replacement,  $start, $end - $start);
	}

	function removeptag($string)
	{
		$tags_to_strip = Array("p");
		$replace_with="";
		foreach ($tags_to_strip as $tag)
		{
		    $string = preg_replace("/<\\/?" . $tag . "(.|\\s)*?>/",$replace_with,$string);

		}
		return $string;
	}

	function removeBrtag($string)
	{
		$tags_to_strip = Array("br");
		$replace_with="";
		foreach ($tags_to_strip as $tag)
		{
		    $string = preg_replace("/<\\/?" . $tag . "(.|\\s)*?>/",$replace_with,$string);

		}
		return $string;
	}

	function convertTimeToMySql($date)
	{
		$timeformat = \database\parameter::getParameter('timeformat');
		$datetimeformat = \database\parameter::getParameter('dateformat').' '.$timeformat;
		$date=trim($date);
		if(strlen($date)==0) return '';
		if(strlen($date)>10)
			$dt = date_create_from_format($datetimeformat,$date);
		else
			$dt = date_create_from_format($timeformat,$date);
        return $dt->format('H:i:s');
	}

	public function smart_resize_image($file,$string=null,$width=0,$height = 0, $proportional = false, $output = 'file', $delete_original = true, $use_linux_commands = false, $quality  = 100,$grayscale = false)
	{
			if ( $height <= 0 && $width <= 0 ) return false;
			if ( $file === null && $string === null ) return false;
			# Setting defaults and meta
			$info                         = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
			$image                        = '';
			$final_width                  = 0;
			$final_height                 = 0;
			list($width_old, $height_old) = $info;
				$cropHeight = $cropWidth = 0;
			# Calculating proportionality
			if ($proportional) {
			  if      ($width  == 0)  $factor = $height/$height_old;
			  elseif  ($height == 0)  $factor = $width/$width_old;
			  else                    $factor = min( $width / $width_old, $height / $height_old );
			  $final_width  = round( $width_old * $factor );
			  $final_height = round( $height_old * $factor );
			}
			else {
			  $final_width = ( $width <= 0 ) ? $width_old : $width;
			  $final_height = ( $height <= 0 ) ? $height_old : $height;
				  $widthX = $width_old / $width;
				  $heightX = $height_old / $height;

				  $x = min($widthX, $heightX);
				  $cropWidth = ($width_old - $width * $x) / 2;
				  $cropHeight = ($height_old - $height * $x) / 2;
			}
			# Loading image to memory according to type
			switch ( $info[2] ) {
			  case IMAGETYPE_JPEG:  $file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);  break;
			  case IMAGETYPE_GIF:   $file !== null ? $image = imagecreatefromgif($file)  : $image = imagecreatefromstring($string);  break;
			  case IMAGETYPE_PNG:   $file !== null ? $image = imagecreatefrompng($file)  : $image = imagecreatefromstring($string);  break;
			  default: return false;
			}

			# Making the image grayscale, if needed
			if ($grayscale) {
			  imagefilter($image, IMG_FILTER_GRAYSCALE);
			}

			# This is the resizing/resampling/transparency-preserving magic
			$image_resized = imagecreatetruecolor( $final_width, $final_height );
			if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
			  $transparency = imagecolortransparent($image);
			  $palletsize = imagecolorstotal($image);
			  if ($transparency >= 0 && $transparency < $palletsize) {
				$transparent_color  = imagecolorsforindex($image, $transparency);
				$transparency       = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
				imagefill($image_resized, 0, 0, $transparency);
				imagecolortransparent($image_resized, $transparency);
			  }
			  elseif ($info[2] == IMAGETYPE_PNG) {
				imagealphablending($image_resized, false);
				$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
				imagefill($image_resized, 0, 0, $color);
				imagesavealpha($image_resized, true);
			  }
			}
			imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);


			# Taking care of original, if needed
			if ( $delete_original ) {
			  if ( $use_linux_commands ) exec('rm '.$file);
			  else @unlink($file);
			}
			# Preparing a method of providing result
			switch ( strtolower($output) ) {
			  case 'browser':
				$mime = image_type_to_mime_type($info[2]);
				header("Content-type: $mime");
				$output = NULL;
			  break;
			  case 'file':
				$output = $file;
			  break;
			  case 'return':
				return $image_resized;
			  break;
			  default:
			  break;
			}

			# Writing image according to type to the output destination and image quality
			switch ( $info[2] ) {
			  case IMAGETYPE_GIF:   imagegif($image_resized, $output);    break;
			  case IMAGETYPE_JPEG:  imagejpeg($image_resized, $output, $quality);   break;
			  case IMAGETYPE_PNG:
				$quality = 9 - (int)((0.9*$quality)/10.0);
				imagepng($image_resized, $output, $quality);
				break;
			  default: return false;
			}
			return true;
	}

    function cleanVariables($array) {
        $flat = array();
        if(is_array($array) && count($array)>0)
		{
			foreach($array as $k=>$value) {
				if (is_array($value)) {
					$flat[$k] = self::cleanVariables($value);
				}
				else {
					$flat[$k] = trim(htmlentities(urldecode(self::strip_unsafe($value)), ENT_QUOTES, 'UTF-8'));
				}
			}
		}

        return $flat;
    }
    function strip_unsafe($string, $img=false)
    {
        $unsafe=array(


            '/<iframe(.*?)>/is',
            '/<\/iframe>/is',
            '/<title(.*?)>/is',
            '/<\/title>/is',
            '/<pre(.*?)>/is',
            '/<\/pre>/is',
            '/<frame(.*?)>/is',
            '/<\/frame>/is',
            '/<frameset(.*?)>/is',
            '/<\/frameset>/is',
            '/<object(.*?)>/is',
            '/<\/object>/is',
            '/<embed(.*?)>/is',
            '/<\/embed>/is',
            '/<applet(.*?)>/is',
            '/<\/applet>/is',
            '/<meta(.*?)>/is',
            '/<\/meta>/is',
            '/<link(.*?)>/is',
            '/<\/link>/is',
            '/<iframe(.*?)<\/iframe>/is',
            '/<title(.*?)<\/title>/is',
            '/<pre(.*?)<\/pre>/is',
            '/<frame(.*?)<\/frame>/is',
            '/<frameset(.*?)<\/frameset>/is',
            '/<object(.*?)<\/object>/is',
            '/<script(.*?)<\/script>/is',
            '/<embed(.*?)<\/embed>/is',
            '/<applet(.*?)<\/applet>/is',
            '/<meta(.*?)>/is',
            '/<!doctype(.*?)>/is',
            '/<link(.*?)>/is',
//            '/<body(.*?)>/is',
//            '/<\/body>/is',
//            '/<head(.*?)>/is',
//            '/<\/head>/is',
            '/onload="(.*?)"/is',
            '/onclick="(.*?)"/is',
            '/onunload="(.*?)"/is',
            '/ondblclick="(.*?)"/is',
            '/onmouseover="(.*?)"/is',
            '/on[^=]+="(.*?)"/is',
//            '/<html(.*?)>/is',
//            '/<\/html>/is',
            '/<script(.*?)>/is',
//            '/<style(.*?)>/is',
            '/<object(.*?)>/is',
            '/<\/script>/is');

        if ($img==true)
        {
            $unsafe[]='/<img(.*?)>/is';
        }

        $string=preg_replace($unsafe, "", html_entity_decode($string));

        return $string;
    }

    public static function input()
	{
        $log = new logger();
		try
		{
            $log->logIt("in input >> ");
            $requestData = file_get_contents("php://input");
            $log->logIt("input parameter >> ".print_r($requestData,true));
            $header = getallheaders();
			$request = (array) json_decode($requestData);
			$final_arr = array("headers"=>$header,"request"=>$request);
			header('Content-Type:application/json');
            return json_encode($final_arr);
		}
		catch(Exception $e)
		{
            $log->logIt("Error in input >> ".$e);
		}
    }

    public static function response($status=200,$message,$response)
	{
        header('Content-Type: application/json');
        header('x', true, $status);
        $respon_arr = array("message"=>$message,"data"=>$response);
		return json_encode($respon_arr);
	}
	public static function gethash($companyid="")
	{
		if($companyid=="")
			$companyid = CONFIG_CID;

		return $companyid.'00'.md5(uniqid(date("Y-m-d H:i:s").'-'.rand(), true));
	}

    public static function fn_hash_password($password)
    {
        $secret = 'gZdT4kxSYTFFubWOzkOlyzNo73xAtbxT';
        $salt = hash('sha1',$secret . rand() . time());
        return ["salt" => $salt,"hash" => hash('sha256',$password . $salt)];
    }

    public static function fn_verify_password($raw_password,$salt,$hashed_password)
    {
        return strcmp(hash('sha256',$raw_password . $salt),$hashed_password) == 0;
    }

	public static function getoneDarray($arr,$field)
	{
		$arr1 = array();
		foreach($arr AS $val)
		{
			array_push($arr1,$val["".$field.""]);
		}
		return $arr1;
	}
	public static function getkeyfieldarray($arr,$field)
	{
		$arr1 = array();
		foreach($arr AS $val)
		{
			if(isset($val[$field]))
			{
				$arr1[$val[$field]] = $val;
			}
		}
		return $arr1;
	}
	public static function convert_html_specials($arr)
	{
		$dummy_arr = array();
		if (is_array($arr)) {
			foreach($arr AS $key=>$value)
			{
				if (is_array($value)) {
					$dummy_arr[$key] = self::convert_html_specials($value);
				}
				else{
					$dummy_arr[$key] =  htmlspecialchars_decode($value, ENT_QUOTES);
				}
				return $dummy_arr;
			}
		}
		else{
			return  htmlspecialchars_decode($arr, ENT_QUOTES);
		}
	}
    /*function convertNumber($number,$prefix='',$suffix='', $uppercase = false,$defaultlanguageArr='')
    {
        $dao = new \dao();
        $strCompany = "SELECT main_currency,fractional_currency FROM ".CONFIG_DBN.".cfcompany WHERE companyid=:companyid";
        $dao->initCommand($strCompany);
        $dao->addParameter(':companyid',CONFIG_CID);
        $resCompany= $dao->executeRow();
        if($resCompany){
            $defaultlanguageArr=json_decode($defaultlanguageArr);
            $no = round($number);
            $point = round($number - $no, 2) * 100;
            $hundred = null;
            $digits_1 = strlen($no);
            $i = 0;
            $str = array();
            $words = array('0' => '', '1' => $defaultlanguageArr->ONE, '2' => $defaultlanguageArr->TWO,
                '3' => $defaultlanguageArr->THREE, '4' => $defaultlanguageArr->FOUR, '5' => $defaultlanguageArr->FIVE, '6' => $defaultlanguageArr->SIX,
                '7' => $defaultlanguageArr->SEVEN, '8' => $defaultlanguageArr->EIGHT, '9' => $defaultlanguageArr->NINE,
                '10' => $defaultlanguageArr->TEN, '11' => $defaultlanguageArr->ELEVEN, '12' => $defaultlanguageArr->TWELVE,
                '13' => $defaultlanguageArr->THIRTEEN, '14' => $defaultlanguageArr->FOURTEEN,
                '15' => $defaultlanguageArr->FIFTEEN, '16' => $defaultlanguageArr->SIXTEEN, '17' => $defaultlanguageArr->SEVENTEEN,
                '18' => $defaultlanguageArr->EIGHTEEN, '19' =>$defaultlanguageArr->NINETEEN, '20' => $defaultlanguageArr->TWENTY,
                '30' => $defaultlanguageArr->THIRTY, '40' => $defaultlanguageArr->FOURTY, '50' => $defaultlanguageArr->FIFTY,
                '60' => $defaultlanguageArr->SIXTY, '70' => $defaultlanguageArr->SEVENTY,
                '80' => $defaultlanguageArr->EIGHTY, '90' => $defaultlanguageArr->NINETY);
            $digits = array('', $defaultlanguageArr->HUNDRED, $defaultlanguageArr->THOUSAND, $defaultlanguageArr->LAKH, $defaultlanguageArr->CRORE);
            while ($i < $digits_1) {
                $divider = ($i == 2) ? 10 : 100;
                $number = floor($no % $divider);
                $no = floor($no / $divider);
                $i += ($divider == 10) ? 1 : 2;
                if ($number) {
                    $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                    $hundred = ($counter == 1 && $str[0]) ? ' '.$defaultlanguageArr->AND.' ' : null;
                    $str [] = ($number < 21) ? $words[$number] .
                        " " . $digits[$counter] . $plural . " " . $hundred
                        :
                        $words[floor($number / 10) * 10]
                        . " " . $words[$number % 10] . " "
                        . $digits[$counter] . $plural . " " . $hundred;
                } else $str[] = null;
            }
            $str = array_reverse($str);
            $result = implode('', $str);
            $points = ($point) ?
                "." . $words[abs($point / 10)] . " " .
                $words[$point = $point % 10] : '';
            if($result){
                $result=$result . $resCompany['main_currency'];
            }
            if($points){
                $points=$points . " ".$resCompany['fractional_currency'];
            }
            return $result." " .$points ;
        }else{
            return '';
        }


    }*/


    public function convertNumber($number,$prefix='',$suffix='', $uppercase = false,$defaultlanguageArr='')
    {
        $dao = new \dao();
        if ($number != 0) {
            $string = "";
            $x='';
            $y='';
            if ($number < 0) {
                $string = "Minus ";
                $no = $number = abs($number);
            }else{
                $no = $number;
            }
            $number1 = $number;
            $strCompany = "SELECT main_currency,fractional_currency FROM " . CONFIG_DBN . ".cfcompany WHERE companyid=:companyid";
            $dao->initCommand($strCompany);
            $dao->addParameter(':companyid', CONFIG_CID);
            $resCompany = $dao->executeRow();
            if ($resCompany) {
                $defaultlanguageArr = json_decode($defaultlanguageArr);
                $hundred = null;
                $digits_1 = strlen($no);
                $i = 0;
                $str = array();
                $words = array('0' => '', '1' => $defaultlanguageArr->ONE, '2' => $defaultlanguageArr->TWO,
                    '3' => $defaultlanguageArr->THREE, '4' => $defaultlanguageArr->FOUR, '5' => $defaultlanguageArr->FIVE, '6' => $defaultlanguageArr->SIX,
                    '7' => $defaultlanguageArr->SEVEN, '8' => $defaultlanguageArr->EIGHT, '9' => $defaultlanguageArr->NINE,
                    '10' => $defaultlanguageArr->TEN, '11' => $defaultlanguageArr->ELEVEN, '12' => $defaultlanguageArr->TWELVE,
                    '13' => $defaultlanguageArr->THIRTEEN, '14' => $defaultlanguageArr->FOURTEEN,
                    '15' => $defaultlanguageArr->FIFTEEN, '16' => $defaultlanguageArr->SIXTEEN, '17' => $defaultlanguageArr->SEVENTEEN,
                    '18' => $defaultlanguageArr->EIGHTEEN, '19' => $defaultlanguageArr->NINETEEN, '20' => $defaultlanguageArr->TWENTY,
                    '30' => $defaultlanguageArr->THIRTY, '40' => $defaultlanguageArr->FOURTY, '50' => $defaultlanguageArr->FIFTY,
                    '60' => $defaultlanguageArr->SIXTY, '70' => $defaultlanguageArr->SEVENTY,
                    '80' => $defaultlanguageArr->EIGHTY, '90' => $defaultlanguageArr->NINETY);
                $digits = array('', $defaultlanguageArr->HUNDRED, $defaultlanguageArr->THOUSAND, $defaultlanguageArr->LAKH, $defaultlanguageArr->CRORE);
                $j = 0;

                while ($i < $digits_1) {
                    $divider = ($i == 2) ? 10 : 100;
                    $number = floor($no % $divider);
                    $no = floor($no / $divider);
                    $i += ($divider == 10) ? 1 : 2;
                    if ($number) {
                        $x = floor($number / 10) * 10;
                        $y = $number % 10;
                        $plural = (($counter = count($str)) && $number > 9) ? '\'s' : null;
                        $hundred = ($counter == 1 && $str[0]) ? $defaultlanguageArr->AND : null;
                        if ($number < 21) {
                            $rev_string = $words[$number] . " " . $digits[$counter] . $plural . " " . $hundred;
                        } else {
                            if ($j == 0 && $x > 20 && $y == 0 && $plural == 0 && $number > 21) {
                                $rev_string = ($words[floor($number / 10) * 10] . " " . $digits[$counter] . $plural . " " . $hundred);
                            } else {
                                $rev_string = $words[floor($number / 10) * 10] . " " . $words[$number % 10] . " " . $digits[$counter] . $plural . " " . $hundred;
                            }
                        }
//                        $test = ($j==0 && $x>20 && $y==0 && $plural==0 && $number>21);
                        $str [] = $rev_string;
                        $j++;
                    } else $str[] = null;
                }
                $str = array_reverse($str);
                $result = implode('', $str);

                /* fraction no. */
                $fraction = null;
                if (strpos($number1, '.') !== false) {
                    list($number1, $fraction) = explode('.', $number1);
                }
                if (null !== $fraction && is_numeric($fraction)) {
                    $digits_1 = strlen($fraction);
                    $di = 0;
                    $d_str = array();
                    $dj = 0;
                    while ($di < $digits_1) {
                        $divider = ($di == 2) ? 10 : 100;
                        $number1 = floor($fraction % $divider);
                        $fraction = floor($fraction / $divider);
                        $di += ($divider == 10) ? 1 : 2;
                        if ($number1) {
                            $plural = (($counter = count($d_str)) && $number1 > 9) ? '\'s' : null;
                            $hundred = ($counter == 1 && $d_str[0]) ? $defaultlanguageArr->AND : null;
                            if($number1 < 21) {
                                $rev_string = $words[$number1] . " " . $digits[$counter] . $plural . " " . $hundred;
                            } else{
                                if($dj == 0 && $x > 20 && $y == 0 && $plural == 0 && $number > 21) {
                                    $rev_string = ($words[floor($number1 / 10) * 10] . " " . $digits[$counter] . $plural . " " . $hundred);
                                } else{
                                    $rev_string = $words[floor($number1 / 10) * 10] . " " . $words[$number1 % 10] . " " . $digits[$counter] . $plural . " " . $hundred;
                                }
                            }
                            $d_str[] = $rev_string;
                            $dj;
                        } else $d_str[] = null;
                    }
                    $d_str = array_reverse($d_str);
                    $d_result = implode('', $d_str);
                }
                /* fraction no. */
                if ($result) {
                    $result = $result . $resCompany['main_currency'];
                }
                if ($d_result) {
                    $d_result = $d_result . " " .$resCompany['fractional_currency'];
                }
                $string .= $result;
                if ($d_result != '') {
                    $string .= " $defaultlanguageArr->AND " . $d_result;
                }
                $string .= " Only";
                return $string;
            } else {
                return '';
            }
        } else {
            return "$defaultlanguageArr->ZERO";
        }
    }



    function convertGroup($index)
    {
        switch($index)
        {
            case 11: return " decillion";
            case 10: return " nonillion";
            case 9: return " octillion";
            case 8: return " septillion";
            case 7: return " sextillion";
            case 6: return " quintrillion";
            case 5: return " quadrillion";
            case 4: return " trillion";
            case 3: return " billion";
            case 2: return " million";
            case 1: return " thousand";
            case 0: return "";
        }
    }

    function convertThreeDigit($dig1, $dig2, $dig3)
    {
        $output = "";

        if($dig1 == "0" && $dig2 == "0" && $dig3 == "0") return "";

        if($dig1 != "0")
        {
            $output .= self::convertDigit($dig1)." hundred";
            if($dig2 != "0" || $dig3 != "0") $output .= " and ";
        }

        if($dig2 != "0") $output .= self::convertTwoDigit($dig2, $dig3);
        else if($dig3 != "0") $output .= self::convertDigit($dig3);

        return $output;
    }

    function convertTwoDigit($dig1, $dig2)
    {
        if($dig2 == "0")
        {
            switch($dig1)
            {
                case "1": return "ten";
                case "2": return "twenty";
                case "3": return "thirty";
                case "4": return "forty";
                case "5": return "fifty";
                case "6": return "sixty";
                case "7": return "seventy";
                case "8": return "eighty";
                case "9": return "ninety";
            }
        }
        else if($dig1 == "1")
        {
            switch($dig2)
            {
                case "1": return "eleven";
                case "2": return "twelve";
                case "3": return "thirteen";
                case "4": return "fourteen";
                case "5": return "fifteen";
                case "6": return "sixteen";
                case "7": return "seventeen";
                case "8": return "eighteen";
                case "9": return "nineteen";
            }
        }
        else
        {
            $temp = self::convertDigit($dig2);
            if($temp==null)
            {
                return self::convertDigit($dig1);
            }else{
                switch($dig1)
                {
                    case "2": return "twenty-$temp";
                    case "3": return "thirty-$temp";
                    case "4": return "forty-$temp";
                    case "5": return "fifty-$temp";
                    case "6": return "sixty-$temp";
                    case "7": return "seventy-$temp";
                    case "8": return "eighty-$temp";
                    case "9": return "ninety-$temp";
                }
            }

        }
    }

    function convertDigit($digit)
    {
        switch($digit)
        {
            case "0": return "zero";
            case "1": return "one";
            case "2": return "two";
            case "3": return "three";
            case "4": return "four";
            case "5": return "five";
            case "6": return "six";
            case "7": return "seven";
            case "8": return "eight";
            case "9": return "nine";
        }
    }


    public static function arrangeArrayPair($mainArray,$keyLabel,$valueLabel)
    {
        $newArray = [];
        if(is_array($mainArray))
        {
            $newArray = array_combine(
                array_map(
                    function ($value) use ($keyLabel) {
                        return $value[$keyLabel];
                    },
                    $mainArray
                ),
                array_map(
                    function ($value) use ($valueLabel) {
                        return $value[$valueLabel];
                    },
                    $mainArray
                )
            );
        }

        return $newArray;
    }

    public static function unsetNullArrayValue($array)
    {
        foreach ($array as $key => $value) {
            $value = trim($value);
            if (empty($value))
                unset($array[$key]);
        }

        return $array;
    }

    function array_pluck($array,$key)
    {
        if(is_array($array)) {
            return array_map(function ($v) use ($key) {
                if (is_object($v)) {
                    return isset($v->$key) ? $v->$key : '';
                } else {
                    return isset($v[$key]) ? $v[$key] : '';
                }
            }, $array);
        }
        else
        {
            return $array;
        }
    }

    public function msort($array, $key, $sort_flags = SORT_REGULAR) {
        if (is_array($array) && count($array) > 0) {
            if (!empty($key)) {
                $mapping = array();
                foreach ($array as $k => $v) {
                    $sort_key = '';
                    if (!is_array($key)) {
                        $sort_key = $v[$key];
                    } else {
                        // @TODO This should be fixed, now it will be sorted as string
                        foreach ($key as $key_key) {
                            $sort_key .= $v[$key_key];
                        }
                        $sort_flags = SORT_STRING;
                    }
                    $mapping[$k] = $sort_key;
                }
                asort($mapping, $sort_flags);
                $sorted = array();
                foreach ($mapping as $k => $v) {
                    $sorted[] = $array[$k];
                }
                return $sorted;
            }
        }
        return $array;
    }
}
?>