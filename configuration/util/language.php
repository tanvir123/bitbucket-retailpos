<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 14/8/17
 * Time: 3:19 PM
 */
namespace util;
class language
{
    public $module = "language";
    public $redis;
    public $redis_default;
    public $log;
    public $concatstore_location;
    public $concatstore_locationdefault;
    public $load_default;
    public $langmodule;
    public $is_redis;
    public $dbprefix;

    function __construct($module=''){
        $this->langmodule = $module;
        if(CONFIG_CUSTOM_LANG==1)
        {
            $this->redis = new \Redis();
            $this->is_redis = $this->redis->connect(CONFIG_REDIS_HOST, CONFIG_REDIS_PORT);
            if($this->is_redis){
                $this->redis->select(15);
                $this->createtable();
            }
        }
        $this->log = new \util\logger();


        if(defined('CONFIG_SID') && CONFIG_SID > 0){
            $this->concatstore_location = CONFIG_UID;
            $this->dbprefix = "3";
            $this->load_default = \common\staticlang::$storedefault;
            $this->concatstore_locationdefault = "storedefault";
        }
        if(defined('CONFIG_LID') && CONFIG_LID > 0){
            $this->concatstore_location = CONFIG_UID;
            $this->dbprefix = "2";
            $this->load_default = \common\staticlang::$config_default;
            $this->concatstore_locationdefault = "config_default";
        }


    }

    public function createtable()
    {
        try
        {
            if(CONFIG_CUSTOM_LANG==1 && $this->is_redis==1)
            {
                $isexist = $this->redis->exists($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->langmodule."_".CONFIG_LANG);
                if(!$isexist){
                    $this->redis->set($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->langmodule."_".CONFIG_LANG,json_encode(array()));
                }
                $isexist = $this->redis->exists($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->concatstore_locationdefault."_".CONFIG_LANG);
                if(!$isexist){
                    $this->redis->set($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->concatstore_locationdefault."_".CONFIG_LANG,json_encode(array()));
                }
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." createtable - ".$e);
        }
    }
    public function loadlanguage($arr)
    {
        try
        {
            $this->confirmwithdeflang($arr);
             if(CONFIG_CUSTOM_LANG==1 && $this->is_redis==1)
            {

                $langarr = json_decode($this->redis->get($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->langmodule."_".CONFIG_LANG),true);
                $retarr = array();
                $iswrite = 0;
                foreach($arr AS $key=>$val){
                    if(isset($langarr[$key]) && $langarr[$key] != '' ){
                        $retarr[$key] = $langarr[$key];
                    }
                    else{
                        $iswrite = 1;
                        $retarr[$key] = $arr[$key];
                    }
                }
                if($iswrite){
                    $this->writeredis_lang($retarr);
                }
                return $retarr;
            }
            else{
                return $arr;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." loadlanguage - ".$e);
        }
    }
    public function writeredis_lang($retarr)
    {
        try{
            if($this->is_redis==1){
                $this->redis->set($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->langmodule."_".CONFIG_LANG,json_encode($retarr));
            }
        }
        catch(Exception $e){
            $this->log->logIt($this->module." writeredis_lang - ".$e);
        }
    }
    public function writeredisdefault_lang($retarr)
    {
        try{
            if($this->is_redis==1){
                $this->redis->set($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->concatstore_locationdefault."_".CONFIG_LANG,json_encode($retarr));
            }
        }
        catch(Exception $e){
            $this->log->logIt($this->module." writeredis_lang - ".$e);
        }
    }
    public function loaddefaultlanguage()
    {
        try
        {
            $default_arr = $this->load_default;


            if(CONFIG_CUSTOM_LANG==1 && $this->is_redis==1)
            {
                $langarr = json_decode($this->redis->get($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->concatstore_locationdefault."_".CONFIG_LANG),true);
                $retarr = array();
                $iswrite = 0;
                foreach($default_arr AS $key=>$val){
                    if(isset($langarr[$key]) && $langarr[$key] != '' ){
                        $retarr[$key] = $langarr[$key];
                    }
                    else{
                        $iswrite = 1;
                        $retarr[$key] = $default_arr[$key];
                    }
                }
                if($iswrite){
                    $this->writeredisdefault_lang($retarr);
                }
                return $retarr;
            }
            else{
                return $default_arr;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." loaddefaultlanguage - ".$e);
        }
    }
    public function confirmwithdeflang($arr)
    {
        try
        {
            $redis_0 = new \Redis();
            $red = $redis_0->connect(CONFIG_REDIS_HOST, CONFIG_REDIS_PORT);
            if($red){
                $redis_0->select(000);
                if(!$redis_0->exists($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->langmodule."_".CONFIG_LANG)){
                    $iswrite = 1;
                }
                else{
                    $langarr = json_decode($redis_0->get($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->langmodule."_".CONFIG_LANG),true);
                    $iswrite = 0;
                    if(count($langarr)!=count($arr))
                    {
                        $iswrite = 1;
                    }
                    else{
                        foreach($arr AS $key=>$val){
                            if(!isset($langarr[$key]) || $arr[$key] != $langarr[$key]){
                                $iswrite = 1;
                            }
                        }
                    }
                }
                if($iswrite){
                    $redis_0->set($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->langmodule."_".CONFIG_LANG,json_encode($arr));
                }
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." confirmwithdeflang - ".$e);
        }
    }
    public function confirmdefaultlang()
    {
        try
        {
            $default_arr = $this->load_default;
            $redis_0 = new \Redis();
            $rec = $redis_0->connect(CONFIG_REDIS_HOST, CONFIG_REDIS_PORT);
            if($rec){
                $redis_0->select(000);
                if(!$redis_0->exists($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->concatstore_locationdefault."_".CONFIG_LANG)){
                    $iswrite = 1;
                }
                else{
                    $langarr = json_decode($redis_0->get($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->concatstore_locationdefault."_".CONFIG_LANG),true);
                    $iswrite = 0;
                    if(count($langarr)!=count($default_arr))
                    {
                        $iswrite = 1;
                    }
                    else{
                        foreach($default_arr AS $key=>$val){
                            if(!isset($langarr[$key]) || $default_arr[$key] != $langarr[$key]){
                                $iswrite = 1;
                            }
                        }
                    }
                }
                if($iswrite){
                    $redis_0->set($this->dbprefix."_".CONFIG_CID."_".$this->concatstore_location."_".CONFIG_DBN."_".$this->concatstore_locationdefault."_".CONFIG_LANG,json_encode($default_arr));
                }
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." loaddefaultlanguage - ".$e);
        }
    }
}
?>