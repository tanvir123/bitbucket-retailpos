<?php
class login
{
        public $module = 'pos_login';
        public $log;
        public $dbconnection;
        public $ObjConn;

        public function __construct()
        {
            $this->log=new \util\logger();
            $this->ObjConn = new dbconnect();
        }
        public function checkcredentials($data)
        {
            try
            {
                    $this->log->logIt($this->module." - checkcredentials");

                    $uname = $data['uname'];
                    $cid = $data['cid'];
                    if(isset($data['encoded']) && $data['encoded']=="1")
                            $pwd = $data['pwd'];
                    else
                            $pwd = md5($data['pwd']);
                    $dao = new \dao();
                    $strSql = "SELECT * FROM sysuser where username=:username AND password=:password AND companyid=:companyid AND isactive=1 AND is_deleted=0";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':username',$uname);
                    $dao->addParameter(':password',$pwd);
                    $dao->addParameter(':companyid',$cid);
                    $rec = $dao->executeQuery();
                    if(count($rec)>0)
                    {
                        $strSql1 = "SELECT * FROM `syscompany` WHERE syscompanyid=:syscompanyid";
                        $dao->initCommand($strSql1);
                        $dao->addParameter(':syscompanyid',$cid);
                        $company = $dao->executeRow();

                        $DBNAME= $company['databasename'];

                        if($company['isactive']==0){
                            return json_encode(array("Success"=>"False","Message"=>"Your account has been inactivated"));
                        }
                        if($rec[0]['locationid']==NULL || $rec[0]['locationid']=='' || $rec[0]['locationid']==0){
                            return json_encode(array("Success"=>"False","Message"=>"Your are not assigned access to any location"));
                        }else{
                            /* Get Location List */
                                $strSql2 = "SELECT syslocationid,locationname as name,1 as type FROM syslocation WHERE syslocationid IN (".$rec[0]['locationid'].") AND is_active=1 AND is_deleted=0 AND companyid=:companyid";
                                $dao->initCommand($strSql2);
                                $dao->addParameter(':companyid',$cid);
                                $rec2 = $dao->executeQuery();
                            /* Get Location List */
                            /* Get Store List */
                            $storeSql = "SELECT store_available FROM ".$DBNAME.".cfcompany WHERE companyid=:companyid ";
                            $dao->initCommand($storeSql);
                            $dao->addParameter(':companyid',$cid);
                            $store_rec = $dao->executeRow();
                            $store_available=$store_rec['store_available'];

                            $rec3=array();
                            if($store_available==1) {
                                $strSql3 = "SELECT storeunkid,storename as name,2 as type FROM " . $DBNAME . ".cfstoremaster WHERE storeunkid IN (" . $rec[0]['storeid'] . ") AND is_active=1 AND is_deleted=0 AND companyid=:companyid";
                                $dao->initCommand($strSql3);
                                $dao->addParameter(':companyid', $cid);
                                $rec3 = $dao->executeQuery();
                            }

                            /* Get Store List */
                            $rec3 = array_merge($rec2,$rec3);
                            $arr_list = json_encode($rec3);
                            return json_encode(array("Success"=>"True","Message"=>"Logged in successfully","location_cnt"=>count($rec3),"data_list"=>$arr_list));
                        }
                    }
                    else{
                        return json_encode(array("Success"=>"False","Message"=>"Invalid username and password"));
                    }
            }
            catch(Exception $e)
                {
                    $this->log->logIt($this->module." - checkcredentials -".$e);
                    return 0;
                }
        }
        public function login_location($data)
        {
            try
            {
                $this->log->logIt($this->module." - login_location");
                $dao = new \dao();
                $uname = $data['uname'];
                $type = $data['type'];
                $cid = $data['cid'];
                if($type==1){
                    $lid = $data['lid'];
                    $sid = 0;
                }else{
                    $lid = 0;
                    $sid = $data['lid'];
                }
                if(isset($data['encoded']) && $data['encoded']=="1"){
                    $pwd = $data['pwd'];
                }else{
                    $pwd = md5($data['pwd']);
                }

                if($type==1){
                    $strSql = "SELECT USR.*,IFNULL(SC.translate_languages,'') as translate_languages,SC.databasename,SC.bucket_name,SL.locationname as lcName,SC.is_oem FROM sysuser AS USR
                           INNER JOIN syscompany AS SC ON USR.companyid=SC.syscompanyid AND SC.isactive=1 
                           INNER JOIN syslocation AS SL ON SL.companyid=SC.syscompanyid AND SL.is_active=1 AND SL.is_deleted=0
                           WHERE USR.companyid=:companyid AND USR.username=:username AND USR.password=:password 
                           AND USR.isactive=1 AND USR.is_deleted=0 AND FIND_IN_SET($lid,USR.locationid) AND SL.syslocationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':username',$uname);
                    $dao->addParameter(':password',$pwd);
                    $dao->addParameter(':companyid',$cid);
                    $dao->addParameter(':locationid',$lid);
                    $rec = $dao->executeQuery();

                }else{
                    $strSql1 = "SELECT databasename FROM `syscompany` WHERE syscompanyid=:syscompanyid";
                    $dao->initCommand($strSql1);
                    $dao->addParameter(':syscompanyid',$cid);
                    $dbname = $dao->executeRow();
                    $databasename=$dbname['databasename'];

                    $strSql = "SELECT USR.*,SC.databasename,SC.bucket_name,SS.storename as lcName,SC.is_oem FROM sysuser AS USR
                           INNER JOIN syscompany AS SC ON USR.companyid=SC.syscompanyid AND SC.isactive=1 
                           INNER JOIN ".$databasename.".cfstoremaster AS SS ON SS.companyid=SC.syscompanyid AND SS.is_active=1 AND SS.is_deleted=0
                           WHERE USR.companyid=:companyid AND USR.username=:username AND USR.password=:password 
                           AND USR.isactive=1 AND USR.is_deleted=0 AND FIND_IN_SET($sid,USR.storeid) AND SS.storeunkid=:storeunkid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':username',$uname);
                    $dao->addParameter(':password',$pwd);
                    $dao->addParameter(':companyid',$cid);
                    $dao->addParameter(':storeunkid',$sid);
                    $rec = $dao->executeQuery();
                }

                if(count($rec)>0)
                {
                    $dbname = $rec[0]['databasename'];
                    $str = "SELECT store_available FROM ".$dbname.".cfcompany WHERE companyid=:companyid ";
                    $dao->initCommand($str);
                    $dao->addParameter(':companyid',$cid);
                    $apisettings = $dao->executeRow();

                    if($apisettings && $lid){

                        /*$str = " SELECT
                         FROM ".$dbname.".cflocation WHERE locationid=:locationid ";
                        $dao->initCommand($str);
                        $dao->addParameter(':locationid', $lid);
                        $apilocationsettings = $dao->executeRow();*/

                        /*if($apilocationsettings){
                            $apisettings['pms_integration']=$apilocationsettings['pms_integration'];
                        }*/
                    }

                    $strSql1 = "SELECT meta_value FROM syscompany_meta WHERE syscompanyid=:syscompanyid AND meta_key=:meta_key";
                    $dao->initCommand($strSql1);
                    $dao->addParameter(':syscompanyid',$cid);
                    $dao->addParameter(':meta_key','is_banquet_available');
                    $banquet = $dao->executeRow();

                    //to get group id
					$oemDetails  = array('name' => '','logo' => '','title' =>'','id' => '');
					if($rec[0]['is_oem'] > 0)
					{
						$dao->initCommand("SELECT * FROM oem_settings WHERE id = :id");
						$dao->addParameter(":id",$rec[0]['is_oem']);
						$oemDetails = $dao->executeRow();
					}

					if($type==1){
                        $systemdefined = $rec[0]['sysdefined'];
                        if ($systemdefined != 1) {
                            $strSql = "SELECT GROUP_CONCAT(DISTINCT(CRP.lnkprivilegegroupid),'') as lnkprivilegegroupid 
                              FROM " . $dbname . ".cfroleprivileges as CRP 
                              INNER JOIN sysprivilegegroup as SPG ON CRP.lnkprivilegegroupid=SPG.privilegegroupunkid 
                              WHERE  CRP.companyid=:companyid AND CRP.lnkroleid=:lnkroleid AND type=3";

                            $dao->initCommand($strSql);
                            $dao->addParameter(':lnkroleid', $rec[0]['roleunkid']);
                            $dao->addParameter(':companyid', $cid);
                            $groupid = $dao->executeRow();
                        }
                        else{

                            $strSql = "SELECT  GROUP_CONCAT(DISTINCT(privilegegroupunkid),'') as lnkprivilegegroupid
                              FROM sysprivilegegroup WHERE type=3";
                                $dao->initCommand($strSql);
                                $groupid = $dao->executeRow();
                            }
                            $group_privilege = $groupid['lnkprivilegegroupid'];

                    }else{
                        $systemdefined = $rec[0]['sysdefined'];
                        if ($systemdefined != 1) {
                            $strSql = "SELECT GROUP_CONCAT(DISTINCT(CRP.lnkprivilegegroupid),'') as lnkprivilegegroupid 
                              FROM " . $dbname . ".cfroleprivileges as CRP 
                              INNER JOIN sysprivilegegroup as SPG ON CRP.lnkprivilegegroupid=SPG.privilegegroupunkid 
                              WHERE  CRP.companyid=:companyid AND CRP.lnkroleid=:lnkroleid AND type=2";

                            $dao->initCommand($strSql);
                            $dao->addParameter(':lnkroleid', $rec[0]['roleunkid']);
                            $dao->addParameter(':companyid', $cid);
                            $groupid = $dao->executeRow();
                        }
                        else{

                            $strSql = "SELECT  GROUP_CONCAT(DISTINCT(privilegegroupunkid),'') as lnkprivilegegroupid
                              FROM sysprivilegegroup WHERE type=2";
                            $dao->initCommand($strSql);
                            $groupid = $dao->executeRow();

                        }
                        $group_privilege = $groupid['lnkprivilegegroupid'];
					}
                    $token = \util\util::getauthtoken($cid);
                        $arr_token = array(
                            "access_token" => $token,
                            "dbname" => $dbname,
                            "bucket_name" => $rec[0]['bucket_name'],
                            "translate_languages" => isset($rec[0]['translate_languages'])?$rec[0]['translate_languages']:"",
                            "username" => $rec[0]['username'],
                            "userid" => $rec[0]['userunkid'],
                            "user_type" => $rec[0]['user_type'],
                            "sysdefined" => $rec[0]['sysdefined'],
                            "companyid" => $cid,
                            "locationid" => $lid,
                            "storeid" => $sid,
                            "login_type"=>$type,
                            "locationname" => $rec[0]['lcName'],
                            "lang_type" => $rec[0]['lang_type'],
                            "lang_code" => $rec[0]['lang_code'],
                          //  "tabinsta" => $apisettings['tabinsta_integration'],
                            'oem_name' => $oemDetails['name'],
                            'oem_logo' => $oemDetails['logo'],
                            'oem_title' => $oemDetails['title'],
                            'oem_id' => $oemDetails['id'],
                            //"pms" => $apisettings['pms_integration'],
                            "store" => $apisettings['store_available'],
                            "banquet" => $banquet['meta_value'],
                            "groupid" => $group_privilege,
                            "roleid" => $rec[0]['roleunkid'],
                        );
                        $ObjFunction = new \common\functions();
                        $res_token = $ObjFunction->addAuthToken($arr_token);
                        return json_encode(array("Success"=>"True","Message"=>"Logged in successfully","Data"=>$res_token));
                }
                else{
                    return json_encode(array("Success"=>"False","Message"=>"Invalid username and password"));
                }
            }
            catch(Exception $e)
            {
                $this->log->logIt($this->module." - login_location -".$e);
                return 0;
            }
        }
        public function checktoken($data)
        {
            try
            {
                $this->log->logIt($this->module." - checktoken");
                $access_token = $data['token'];
                $ObjEncDec = new \common\encdec;
                $un = $ObjEncDec->openssl('decrypt',$data['un']);
                $ObjFunction = new \common\functions();
                $rec = $ObjFunction->checkToken($access_token,$un);
                if($rec==0)
                    return json_encode(array("Success"=>"False"));
                else
                    return json_encode(array("Success"=>"True", "Data"=>$rec, "cnt"=>1));
            }
            catch(Exception $e)
            {
                $this->log->logIt($this->module." - checktoken -".$e);
                return json_encode(array("Success"=>"False"));
            }
        }
}

?>