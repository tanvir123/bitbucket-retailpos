<?php

class menu
{
	public $module = 'Service_request';
	public $log;
	public $dbconnection;
	public $encdec;
	public $account_code;
	public $dbname;
	public $dbconnect;	

	public function __construct()
	{
			$this->log=new \util\logger();
	}
    // Category
    public function loadcategorylist($data)
	{
		try{
			$this->log->logIt($this->module." - loadcategorylist");
			$limit = $data['limit'];
			$offset = $data['offset'];
			$name = $data['nm'];
			
			$is_service = '';
			if(isset($data['is_service']))
				$is_service = $data['is_service'];	
			
			$ObjUserDao = new \database\menu_categorydao();
			$data = $ObjUserDao->categorylist($limit,$offset,$name,$is_service);
			
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loadcategorylist - ".$e);
			return false; 
		}
	}
   public function addCategory($data){
		try{
			$this->log->logIt($this->module." - addCategory");
			
			$ObjMenuCategoryDao = new \database\menu_categorydao();
			$data = $ObjMenuCategoryDao->addCategory($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - addCategory - ".$e);
			return false; 
		}
	}

	public function getTaxRec($data)
	{
		
		try
		{
			$this->log->logIt($this->module." - getTaxRec");
			$ObjMenuCategoryDao = new \database\menu_categorydao();
			$data1 = $ObjMenuCategoryDao->getTaxRec($data);
			return $data1;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getTaxRec - ".$e);
			return false; 
		}
	}

	
	//End Category
	
	//code for item..
	public function additem($data){
		try{
			$this->log->logIt($this->module." - additem");
			
			
			return $data;
		}catch(Exception $e)
		{
			$this->log->logIt($this->module." - additem - ".$e);
			return false; 
		}
	}

	public function loaditemlist($data)
	{
		try{
			$this->log->logIt($this->module." - loaditemlist");
			$limit = $data['limit'];
			$offset = $data['offset'];
			$name = $data['nm'];
			
			$is_service = '';
			if(isset($data['is_service']))
				$is_service = $data['is_service'];	
			
			$ObjmenuitemDao = new \database\menu_itemdao();
			$data = $ObjmenuitemDao->itemlist($limit,$offset,$name,$is_service);
			
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loaditemlist - ".$e);
			return false; 
		}
	}
	public function getrec($data)
	{
		
		try
		{
			$this->log->logIt($this->module." - getrec");
			$ObjmenuitemDao = new \database\menu_itemdao();
			$data1 = $ObjmenuitemDao->getrec($data);
			return $data1;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getrec - ".$e);
			return false; 
		}
	}
	// end of item

	//code for modifier item 
	public function addmoditem($data){
		try{
			$this->log->logIt($this->module." - addmoditem");
			
			$ObjmenuitemDao = new \database\menu_modifier_itemdao();
			$data = $ObjmenuitemDao->additem($data);
			return $data;
		}catch(Exception $e)
		{
			$this->log->logIt($this->module." - addmoditem - ".$e);
			return false; 
		}
	}

	public function loadmoditem($data)
	{
		try{
			$this->log->logIt($this->module." - loadmoditem");
			$limit = $data['limit'];
			$offset = $data['offset'];
			$name = $data['nm'];
			
			$is_service = '';
			if(isset($data['is_service']))
				$is_service = $data['is_service'];	
			
			$ObjmenumoditemDao = new \database\menu_modifier_itemdao();
			$data = $ObjmenumoditemDao->moditemlist($limit,$offset,$name,$is_service);
			
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loadmoditem - ".$e);
			return false; 
		}
	}
	public function modgetitem($data)
	{
		
		try
		{
			$this->log->logIt($this->module." - modgetitem");
			$ObjmenuitemDao = new \database\menu_modifier_itemdao();
			$data1 = $ObjmenuitemDao->moditem($data);
			return $data1;
		}catch(Exception $e){
			$this->log->logIt($this->module." - modgetitem - ".$e);
			return false; 
		}
	}

	// for menu hours
	public function addmenuhour($data)
	{
		try
		{
			$this->log->logIt($this->module." - addmenuhour");
			
			$ObjmenuhourDao = new \database\menu_menuhoursdao();
			$data = $ObjmenuhourDao->addmenuhour($data);
			return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - addmenuhour - ".$e);
			return false; 
		}
	}
	public function loadtaxlist($data)
	{
		try
		{
			$this->log->logIt($this->module." - loadtaxlist");
			$limit = $data['limit'];
			$offset = $data['offset'];
			$name = $data['nm'];
			$ObjmenuhourDao = new \database\menu_menuhoursdao();
			$data = $ObjmenuhourDao->menuhourslist($limit,$offset,$name,'');
			
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loadtaxlist - ".$e);
			return false; 
		}
	}
	public function getRecmenuhour($data)
	{
		try
		{
			$this->log->logIt($this->module." - getRecmenuhour");
			
			$ObjmenuhourDao = new \database\menu_menuhoursdao();
			$data = $ObjmenuhourDao->getRecmenuhour($data);
			return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - getRecmenuhour - ".$e);
			return false; 
		}
	}
	// End menu hours
	
	// Menu
	public function loadmenulist($data)
	{
		try{
			$this->log->logIt($this->module." - loadmenulist");
			$limit = $data['limit'];
			$offset = $data['offset'];
			$name = $data['nm'];
			
			$is_service = '';
			if(isset($data['is_service']))
				$is_service = $data['is_service'];	
			
			$ObjUserDao = new \database\menu_menudao();
			$data = $ObjUserDao->menulist($limit,$offset,$name,$is_service);
			
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loadmenulist - ".$e);
			return false; 
		}
	}
	
	public function addMenu($data){
		try{
			$this->log->logIt($this->module." - addMenu");
			$ObjMenuDao = new \database\menu_menudao();
			$data = $ObjMenuDao->addMenu($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - addMenu - ".$e);
			return false; 
		}
	}
	
	public function getMenuRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getMenuRec");
			$ObjMenuDao = new \database\menu_menudao();
			$data = $ObjMenuDao->getMenuRec($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getMenuRec - ".$e);
			return false; 
		}
	}
	
	// End Menu
	
	
	
	
	
	//for modifier
	public function addmodifier($data)
	{
		try
		{
			$this->log->logIt($this->module." -  addmodifier");
			
			$ObjmodifierDao = new \database\menu_modifierdao();
			$data = $ObjmodifierDao->addmodifier($data);
			return $data;
		
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - addmodifier - ".$e);
			return false; 
		}
	}
	public function loadmodifierlist($data)
	{
		try
		{
			$this->log->logIt($this->module." - loadmodifierlist");
			$limit = $data['limit'];
			$offset = $data['offset'];
			$name = $data['nm'];
			
			$is_service = '';
			if(isset($data['is_service']))
				$is_service = $data['is_service'];	
			
			$ObjmodifierDao = new \database\menu_modifierdao();
			$data = $ObjmodifierDao->modifierlist($limit,$offset,$name,$is_service);
			
			return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - loadmodifierlist - ".$e);
			return false; 
		}
	}
	public function getRecmodifier($data)
	{
		
		try
		{
			$this->log->logIt($this->module." - getRecmodifier");
			
			$ObjmodifierDao = new \database\menu_modifierdao();
			$data = $ObjmodifierDao->getRecmodifier($data);
			return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - getRecmodifier - ".$e);
			return false; 
		}
	}
	// End  modifier
	
	
	// for Combo Product
	 public function loadcomboproductlist($data)
	{
		try{
			$this->log->logIt($this->module." - loadcomboproductlist");
			$limit = $data['limit'];
			$offset = $data['offset'];
			$name = $data['nm'];
			
			$is_service = '';
			if(isset($data['is_service']))
				$is_service = $data['is_service'];	
			
			$ObjUserDao = new \database\combo_productdao();
			$data = $ObjUserDao->comboproductlist($limit,$offset,$name,$is_service);
			
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loadcomboproductlist - ".$e);
			return false; 
		}
	}
	
	public function addComboProduct($data){
		try{
			$this->log->logIt($this->module." - addComboProduct");
			
			$ObjComboProductDao = new \database\combo_productdao();
			$data = $ObjComboProductDao->addComboProduct($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - addComboProduct - ".$e);
			return false;
		
		}
	}
	
	public function getComboRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getComboRec");
			$ObjComboDao = new \database\combo_productdao();
			$data = $ObjComboDao->getComboRec($data);
			return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - getComboRec - ".$e);
			return false; 
		}
	}
	
	public function getChkboxRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getChkboxRec");
			$ObjComboDao = new \database\combo_productdao();
			$data = $ObjComboDao->getChkboxRec($data);
			return $data;
			
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - getChkboxRec - ".$e);
			return false; 
		}
	}
	
	// End Combo Product
	
	
	
}
?>