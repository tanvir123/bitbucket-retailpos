<?php

class auditlog
{
	public $module = 'Service_auditlog';
	public $log;
	public $dbconnection;
	public $encdec;
	public $account_code;
	public $dbname;
	public $dbconnect;	

	public function __construct()
	{
			$this->log=new \util\logger();
	}
    
    public function loadindividualaudit($data)
	{
		try{
			$this->log->logIt($this->module." - loadindividualaudit");
			$id = isset($data['id'])?$data['id']:'';
			$module = isset($data['module'])?$data['module']:'';
			$masterfield = isset($data['masterfield'])?$data['masterfield']:'';
			$pd = isset($data['pd'])?$data['pd']:'';
			
			$ObjUserDao = new \database\auditlogdao();
			$data = $ObjUserDao->loadindividualauditlogs($id,$module,$masterfield,$pd);
			
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loadindividualaudit - ".$e);
			return false; 
		}
	}
    public function loaddisplaysettingslogs($data)
    {
        try{
            $this->log->logIt($this->module." - loaddisplaysettingslogs");
            $module = isset($data['module'])?$data['module']:'';
            $keyname = isset($data['keyname'])?$data['keyname']:'';
            $ObjUserDao = new \database\auditlogdao();
            $data = $ObjUserDao->loaddisplaysettingslogs($module,$keyname);
            return $data;
        }
        catch(Exception $e){
            $this->log->logIt($this->module." - loaddisplaysettingslogs - ".$e);
            return false;
        }
    }
   
}
?>