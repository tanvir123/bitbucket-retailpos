<?php

class storelistforpms
{
    public $module = 'storelistforpms';
    public $log;
    public $dbconnection;

    public function __construct()
    {
        $this->log=new \util\logger();
    }
    public function storelist($data)
    {
        try {
            $this->log->logIt($this->module . ' - storelist');
            $dao = new \dao;
            $appid = $data['appid'];
            $appsecret= $data['appsecret'];

            $strSql = "SELECT * FROM syscompany where app_id=:app_id AND app_secret=:app_secret ";
            $dao->initCommand($strSql);
            $dao->addParameter(':app_id', $appid);
            $dao->addParameter(':app_secret', $appsecret);
            $company = $dao->executeQuery();

            if (count($company) > 0 && isset($company[0]['syscompanyid'])) {
                $dbname = $company[0]['databasename'];
                $companyid = $company[0]['syscompanyid'];
                $is_store = self::check_store_available($dbname,$companyid);
                if($is_store){
                    return $is_store;
                }
                else {
                    $Obj = new \database\commondao();
                    define('CONFIG_DBN', $dbname);
                    define('CONFIG_CID', $companyid);
                    $result = $Obj->getallstorelist();
                    return $result;
                }
            }
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . ' - storelist - ' . $e);
        }
    }

    public function storewisecategorylist($data)
    {
        try {
            
            $this->log->logIt($this->module . ' - storewisecategorylist');
            $dao = new \dao;
            $appid = $data['appid'];
            $appsecret= $data['appsecret'];

            $strSql = "SELECT * FROM syscompany where app_id=:app_id AND app_secret=:app_secret ";
            $dao->initCommand($strSql);
            $dao->addParameter(':app_id', $appid);
            $dao->addParameter(':app_secret', $appsecret);
            $company = $dao->executeQuery();

            if (count($company) > 0 && isset($company[0]['syscompanyid'])) {
                $dbname = $company[0]['databasename'];
                $companyid = $company[0]['syscompanyid'];
                $is_store = self::check_store_available($dbname,$companyid);
                if($is_store){
                    return $is_store;
                }
                else {
                    $Obj = new \database\raw_categorydao();
                    define('CONFIG_DBN', $dbname);
                    define('CONFIG_CID', $companyid);
                    define('CONFIG_SID', $data['store_id']);
                    $result = $Obj->getAllCategory(1);
                    return $result;
                }
            }
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . ' - storewisecategorylist - ' . $e);
        }
    }

    public function categorywiseitemlist($data)
    {
        try {

            $this->log->logIt($this->module . ' - categorywiseitemlist');
            $dao = new \dao;
            $appid = $data['appid'];
            $appsecret= $data['appsecret'];

            if($data['category_id']==''){
                die('Category Not Found');
            }

            $strSql = "SELECT * FROM syscompany where app_id=:app_id AND app_secret=:app_secret ";
            $dao->initCommand($strSql);
            $dao->addParameter(':app_id', $appid);
            $dao->addParameter(':app_secret', $appsecret);
            $company = $dao->executeQuery();

            if (count($company) > 0 && isset($company[0]['syscompanyid'])) {
                $dbname = $company[0]['databasename'];
                $companyid = $company[0]['syscompanyid'];
                $is_store = self::check_store_available($dbname,$companyid);
                if($is_store){
                    return $is_store;
                }
                else {
                    $Obj = new \database\indentrequestdao();
                    define('CONFIG_DBN', $dbname);
                    define('CONFIG_CID', $companyid);
                    $senddata = [];
                    $senddata['rawcatid'] = $data['category_id'];
                    $result = $Obj->getrawmaterialoncategory($senddata);
                    return $result;
                }
            }
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . ' - categorywiseitemlist - ' . $e);
        }
    }

    public function itemwiseunitlist($data)
    {
        try {

            $this->log->logIt($this->module . ' - itemwiseunitlist');
            $dao = new \dao;
            $appid = $data['appid'];
            $appsecret= $data['appsecret'];

            if($data['item_id']==''){
                die('Item Not Found');
            }

            $strSql = "SELECT * FROM syscompany where app_id=:app_id AND app_secret=:app_secret ";
            $dao->initCommand($strSql);
            $dao->addParameter(':app_id', $appid);
            $dao->addParameter(':app_secret', $appsecret);
            $company = $dao->executeQuery();

            if (count($company) > 0 && isset($company[0]['syscompanyid'])) {
                $dbname = $company[0]['databasename'];
                $companyid = $company[0]['syscompanyid'];
                $is_store = self::check_store_available($dbname,$companyid);
                if($is_store){
                    return $is_store;
                }
                else {
                    $Obj = new \database\indentrequestdao();
                    define('CONFIG_DBN', $dbname);
                    define('CONFIG_CID', $companyid);
                    $senddata = [];
                    $senddata['rawid'] = $data['item_id'];
                    $senddata['locationid'] = $data['store_id'];
                    $result = $Obj->getUnitRate($senddata);
                    return $result;
                }
            }
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . ' - itemwiseunitlist - ' . $e);
        }
    }
    public function check_store_available($dbname,$companyid)
    {
        try {
            $storeSql = "SELECT store_available FROM ".$dbname.".cfcompany WHERE companyid=:companyid ";
            $dao = new \dao;
            $dao->initCommand($storeSql);
            $dao->addParameter(':companyid',$companyid);
            $store_rec = $dao->executeRow();
            $store_available=$store_rec['store_available'];
            if($store_available==0){
                return json_encode(array("Success"=>"False","Message"=>"Store not available!"));
            }
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . ' - check_store_available - ' . $e);
        }
    }

    public function UpdateInventroy($data)
    {
        try {

            $this->log->logIt($this->module . ' - UpdateInventroy');
            $dao = new \dao;
            $appid = $data['appid'];
            $appsecret= $data['appsecret'];


            $strSql = "SELECT * FROM syscompany where app_id=:app_id AND app_secret=:app_secret ";
            $dao->initCommand($strSql);
            $dao->addParameter(':app_id', $appid);
            $dao->addParameter(':app_secret', $appsecret);
            $company = $dao->executeQuery();

            if (count($company) > 0 && isset($company[0]['syscompanyid'])) {
                $dbname = $company[0]['databasename'];
                $companyid = $company[0]['syscompanyid'];
                $is_store = self::check_store_available($dbname,$companyid);
                if($is_store){
                    return $is_store;
                }
                else {
                    $Obj = new \database\indentrequestdao();
                    define('CONFIG_DBN', $dbname);
                    define('CONFIG_CID', $companyid);
                    define('CONFIG_SID', $data['store_id']);
                    $senddata = [];
                    if(isset($data['items']) && count($data['items'])>0){
                        foreach ($data['items'] as $key=>$val){
                            $senddata[$key]['rawid'] = $val['item_id'];
                            $senddata[$key]['unit_id'] = $val['unit_id'];
                            $senddata[$key]['unit_rate'] = $val['unit_rate'];
                            $senddata[$key]['item_price'] = $val['item_price'];
                            $senddata[$key]['quantity'] = $val['quantity'];
                            $senddata[$key]['crdb'] = $val['crdb'];
                        }
                        $result = $Obj->updateInventoryForPMS($senddata);
                        return $result;
                    }else{
                        return json_encode(array('Success'=>'False','Message'=>"Please used any item!"));
                    }

                }
            }
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . ' - UpdateInventroy - ' . $e);
        }
    }
}
?>
