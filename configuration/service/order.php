<?php
include("login.php");

class order
{
    public $module = 'order';
    public $log;
    public $dbconnection;

    public function __construct()
    {
        $this->log = new \util\logger();
    }

    public function orderlist($data)
    {
        try {
            $this->log->logIt($this->module . ' - orderlist');




            $dao = new \dao();
            $strSql = "SELECT * FROM sysuser where username=:username AND password=:password AND companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':username', $data['username']);
            $dao->addParameter(':password', md5($data['password']));
            $dao->addParameter(':companyid', $data['companyid']);
            $rec = $dao->executeQuery();
            if (count($rec) > 0 && isset($rec[0]['userunkid'])) {
                $strSql1 = "SELECT * FROM `syscompany` WHERE syscompanyid=:syscompanyid";
                $dao->initCommand($strSql1);
                $dao->addParameter(':syscompanyid', $data['companyid']);
                $company = $dao->executeQuery();
                $dbname = $company[0]['databasename'];

                $str = "SELECT locationid FROM " . $dbname . ".cfparameter WHERE keyname='tabinsta_locationid' AND companyid='" . $data['companyid'] . "' AND keyvalue='" . $data['locationid'] . "'";
                $dao->initCommand($str);
                $loc = $dao->executeRow();

                if (isset($loc['locationid'])) {
                    define('CONFIG_DBN', $dbname);
                    define('CONFIG_CID', $data['companyid']);
                    define('CONFIG_LID', $loc['locationid']);
                    define('CONFIG_UID', $rec[0]['userunkid']);
                    define('CONFIG_UNM', $data['username']);
                    $obj = new \database\synctabinstadao();
                    $beforeorder = $obj->beforeorderamttesting($data);
                    if ($beforeorder == 1) {
                        $Obj = new \database\synctabinstadao();
                        $result = $Obj->orderfromtabinsta($data);
                        return $result;
                    } else {
                        return json_encode(array("Success" => "False", "Message" => "Order Amount Mismatch"));
                    }
                }
                else{
                    return json_encode(array("Success" => "False", "Message" => "Location not match"));
                }
            }
            else{
                return json_encode(array("Success" => "False", "Message" => "Authentication Failed"));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - orderlist - ' . $e);
        }
    }
}
?>




