<?php

class location
{
    public $module = 'location';
    public $log;
    public $dbconnection;
    public $encdec;
    public $account_code;
    public $dbname;

    public $dbconnect;

    public function __construct()
    {
        $this->log = new \util\logger();
    }

    public function locationlist($data)
    {
        try
        {
            $this->log->logIt($this->module.'-locationlist');


            $this->log->logIt($this->module . ' - reservnolist');
            $dao = new \dao;
            $username = $data['username'];
            $pwd= $data['password'];
            $cid= $data['companyid'];

            $strSql = "SELECT * FROM sysuser where username=:username AND password=:password AND companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':username', $username);
            $dao->addParameter(':password', md5($pwd));
            $dao->addParameter(':companyid', $cid);
            $rec = $dao->executeQuery();

            if (count($rec) > 0 && isset($rec[0]['userunkid'])) {
                $strSql1 = "SELECT * FROM `syscompany` WHERE syscompanyid=:syscompanyid";
                $dao->initCommand($strSql1);
                $dao->addParameter(':syscompanyid',$cid);
                $company = $dao->executeQuery();
                $dbname = $company[0]['databasename'];

                $Obj = new \database\locationdao();
                define('CONFIG_DBN',$dbname);
                $result = $Obj->getlocation($data);
                return $result;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-locationlist -'.$e);
        }
    }
}
?>

