<?php

class request
{
        public $module = 'Service_request';
        public $log;
        public $dbconnection;
        public $encdec;
        public $account_code;
        public $dbname;
        public $dbconnect;

		public function __construct()
		{
				$this->log=new \util\logger();
		}
        
		public function loaddashboard()
		{
				try
				{
					$this->log->logIt($this->module.' - loaddashboard');

				    $ObjDashboard = new \database\dashboarddao();
					$retval = $ObjDashboard->getaccountdetail();
					return $retval;
				}
				catch(Exception $e)
				{
					$this->log->logIt($this->module.' - loaddashboard - '.$e);
				}
		}
		public function auth_dash()
		{
				try
				{
					$this->log->logIt($this->module.' - auth_dash');
								
					$ObjDashboard = new \database\dashboarddao();
					$retval = $ObjDashboard->getaccountdetail();
					return $retval;
				}
				catch(Exception $e)
				{
					$this->log->logIt($this->module.' - auth_dash- '.$e);
				}
		}
        public function authentication()
        {
            try
            {
                $this->log->logIt($this->module.' - authentication');
                $ObjAuth = new \database\authdao();
                $retval = $ObjAuth->generateToken();
                return $retval;
            }
            catch(Exception $e)
            {
                $this->log->logIt($this->module.' - authentication - '.$e);
            }
        }
		public function toggleststus($data)
		{
			try
            {
                $this->log->logIt($this->module.' - toggleststus');
                $ObjCommonDao = new \database\commondao();
				$res = $ObjCommonDao->toggleststus($data);
				return $res;
			}
            catch(Exception $e)
            {
                $this->log->logIt($this->module.' - toggleststus - '.$e);
            }	
		}

        public function toggleststusBycompany($data)
        {
            try
            {
                $this->log->logIt($this->module.' - toggleststusBycompany');
                $ObjCommonDao = new \database\commondao();
                $res = $ObjCommonDao->toggleststusBycompany($data);
                return $res;
            }
            catch(Exception $e)
            {
                $this->log->logIt($this->module.' - toggleststusBycompany - '.$e);
            }
        }

		public function remove($data)
		{
			try
            {
                $this->log->logIt($this->module.' - toggleststus');
                $ObjCommonDao = new \database\commondao();
				$res = $ObjCommonDao->remove($data);
				return $res;
			}
            catch(Exception $e)
            {
                $this->log->logIt($this->module.' - toggleststus - '.$e);
            }	
		}
        public function removeByCompany($data)
        {
            try
            {
                $this->log->logIt($this->module.' - removeByCompany');
                $ObjCommonDao = new \database\commondao();
                $res = $ObjCommonDao->removeByCompany($data);
                return $res;
            }
            catch(Exception $e)
            {
                $this->log->logIt($this->module.' - removeByCompany - '.$e);
            }
        }
}
?>