<?php

session_start();

require(dirname(__FILE__).'/../common/dao.php');
require(dirname(__FILE__).'/../config/config.php');
require(dirname(__FILE__).'/../config/dbconnect.php');

spl_autoload_register(function ($className) {
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
   $fileName .= str_replace('', DIRECTORY_SEPARATOR, $className) . '.php';
   require_once dirname(__DIR__).'/'.$fileName;
});
$ObjConn = new dbconnect();
$ObjConn->config_connect();

?>