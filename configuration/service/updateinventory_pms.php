<?php

class updateinventory_pms
{
    public $module = 'updateinventory_pms';
    public $log;
    public $dbconnection;

    public function __construct()
    {
        $this->log=new \util\logger();
    }
    public function updateinventory($data)
    {
        try {
            $this->log->logIt($this->module . ' - updateinventory');
            $dao = new \dao;
            $appid = $data['appid'];
            $appsecret= $data['appsecret'];

            $strSql = "SELECT * FROM syscompany where app_id=:app_id AND app_secret=:app_secret ";
            $dao->initCommand($strSql);
            $dao->addParameter(':app_id', $appid);
            $dao->addParameter(':app_secret', $appsecret);
            $company = $dao->executeQuery();

            if (count($company) > 0 && isset($company[0]['syscompanyid'])) {
                $dbname = $company[0]['databasename'];
                $companyid = $company[0]['syscompanyid'];
                $Obj = new \database\indentrequestdao();
                define('CONFIG_DBN',$dbname);
                define('CONFIG_CID',$companyid);

                $result = $Obj->getallstorelist();
                return $result;
            }
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . ' - updateinventory - ' . $e);
        }
    }


    public function storewisecategorylist($data)
    {
        try {
            
            $this->log->logIt($this->module . ' - storewisecategorylist');
            $dao = new \dao;
            $appid = $data['appid'];
            $appsecret= $data['appsecret'];

            if($data['store_id']==''){
                die('Store Not Found');
            }

            $strSql = "SELECT * FROM syscompany where app_id=:app_id AND app_secret=:app_secret ";
            $dao->initCommand($strSql);
            $dao->addParameter(':app_id', $appid);
            $dao->addParameter(':app_secret', $appsecret);
            $company = $dao->executeQuery();

            if (count($company) > 0 && isset($company[0]['syscompanyid'])) {
                $dbname = $company[0]['databasename'];
                $companyid = $company[0]['syscompanyid'];
                $Obj = new \database\raw_categorydao();
                define('CONFIG_DBN',$dbname);
                define('CONFIG_CID',$companyid);
                $result = $Obj->getAllCategory();
                return $result;
            }
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . ' - storewisecategorylist - ' . $e);
        }
    }
}
?>
