<?php

class configuration
{
	public $module = 'Service_request';
	public $log;
	public $dbconnection;
	public $encdec;
	public $account_code;
	public $dbname;
	public $dbconnect;	

	public function __construct()
	{
			$this->log=new \util\logger();
	}
    // Payment Type
	public function loadpaymenttypelist($data)
	{
		try
		{
			$this->log->logIt($this->module." - loadpaymenttypelist");
			$limit = $data['limit'];
			$offset = $data['offset'];
			$name = $data['nm'];
			
			$is_service = '';
			if(isset($data['is_service']))
				$is_service = $data['is_service'];	
			
			$ObjUserDao = new \database\paymenttypedao();
			$data = $ObjUserDao->paymenttypelist($limit,$offset,$name);
			
			return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - loadpaymenttypelist - ".$e);
			return false; 
		}
	}
	public function addPaymentType($data)
	{
		try
		{
			$this->log->logIt($this->module." - addPaymentType");
			
			$ObjPaymentTypeDao = new \database\paymenttypedao();
			$data = $ObjPaymentTypeDao->addPaymentType($data);
			return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - addPaymentType - ".$e);
			return false; 
		}
	}
	public function getPaymentTypeRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getPaymentTypeRec");
			
			$ObjPaymentTypeDao = new \database\paymenttypedao();
			$data = $ObjPaymentTypeDao->getPaymentTypeRec($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getPaymentTypeRec - ".$e);
			return false; 
		}
	}
	
	
	
	// Tax------------------------
	
	public function loadtaxlist($data)
	{
		try{
			$this->log->logIt($this->module." - loadtaxlist");
			$limit = $data['limit'];
			$offset = $data['offset'];
			$name = $data['nm'];
			
			$is_service = '';
			if(isset($data['is_service']))
				$is_service = $data['is_service'];	
			
			$ObjUserDao = new \database\taxdao();
			$data = $ObjUserDao->taxlist($limit,$offset,$name,$is_service);
			
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loadtaxlist - ".$e);
			return false; 
		}
	}
	public function addTax($data){
		try{
			$this->log->logIt($this->module." - addTax");
			
			$ObjPaymentTypeDao = new \database\taxdao();
			$data = $ObjPaymentTypeDao->addTax($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - addTax - ".$e);
			return false; 
		}
	}
	public function getTaxRec($data)
	{
		
		try
		{
			$this->log->logIt($this->module." - getTaxRec");
			
			$ObjPaymentTypeDao = new \database\taxdao();
			$data = $ObjPaymentTypeDao->getTaxRec($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getTaxRec - ".$e);
			return false; 
		}
	}
	public function getDetailsRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getDetailsRec");
			


			$ObjPaymentTypeDao = new \database\taxdao();
			$data = $ObjPaymentTypeDao->getDetailsRec($data);


			
			return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - getDetailsRec - ".$e);
			return false; 
		}
	}
	
	
	// Tax------------------------
	
	public function loaddisclist($data)
	{
		try{
			$this->log->logIt($this->module." - loaddisclist");
			$limit = $data['limit'];
			$offset = $data['offset'];
			$name = $data['nm'];
			
			$is_service = '';
			if(isset($data['is_service']))
				$is_service = $data['is_service'];	
			
			$ObjDao = new \database\discountdao();
			$data = $ObjDao->disclist($limit,$offset,$name,$is_service);
			
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loaddisclist - ".$e);
			return false; 
		}
	}
	public function addDiscount($data){
		try{
			$this->log->logIt($this->module." - addDiscount");
			
			$ObjDao = new \database\discountdao();
			$data = $ObjDao->addDiscount($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - addDiscount - ".$e);
			return false; 
		}
	}
	public function getDiscRec($data)
	{
		
		try
		{
			$this->log->logIt($this->module." - getDiscRec");
			
			$ObjDao = new \database\discountdao();
			$data = $ObjDao->getDiscRec($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getDiscRec - ".$e);
			return false; 
		}
	}
	// Display Setting
	public function loaddisplaysettingslist($data)
	{
		try
		{
			$this->log->logIt($this->module." - loaddisplaysettingslist");
			
			$ObjDao = new \database\displaysettingsdao();
			$data = $ObjDao->loaddisplaysettingslist($data);
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loaddisplaysettingslist - ".$e);
			return false; 
		}
	}
	
	public function editDisplaysettings($data)
	{
		try
		{
			$this->log->logIt($this->module." - editDisplaysettings");
			
			$ObjDao = new \database\displaysettingsdao();
			$data = $ObjDao->editDisplaysettings($data);
			return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - editDisplaysettings - ".$e);
			return false; 
		}
	}
	
	
	//Company Information
	public function loadcompanyinformationlist($data)
	{
		try
		{
			$this->log->logIt($this->module." - loadcompanyinformationlist");
			
			$ObjDao = new \database\companyinformationdao();
			$data = $ObjDao->loadcompanyinformationlist($data);
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loadcompanyinformationlist - ".$e);
			return false; 
		}
	}
	public function editCompanyInformation($data)
	{
		try
		{
			$this->log->logIt($this->module." - editCompanyInformation");
			
			$ObjDao = new \database\companyinformationdao();
			$data = $ObjDao->editCompanyInformation($data);
			return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - editCompanyInformation - ".$e);
			return false; 
		}
	}
	
	public function loadcountrylist($data)
	{
		try{
			$this->log->logIt($this->module." - loadcountrylist");
			
			$ObjUserDao = new \database\companyinformationdao();
			$data = $ObjUserDao->countrylist();
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loadcountrylist - ".$e);
			return false; 
		}
	}
	// End of Company Information

	//user 
	public function loaduserlist($data)
	{
		try{
			
			$this->log->logIt($this->module." - loaduserlist");
			$limit = $data['limit'];
			$offset = $data['offset'];
			$name = $data['nm'];
			
			$is_service = '';
			if(isset($data['is_service']))
				$is_service = $data['is_service'];	
			
			$ObjDao = new \database\userdao();
			$data = $ObjDao->userlist($limit,$offset,$name,$is_service);
			return $data;
		}
		catch(Exception $e){
			$this->log->logIt($this->module." - loaduserlist - ".$e);
			return false; 
		}
	}

	public function getUserRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getUserRec");
			
			$ObjDao = new \database\userdao();
			$data = $ObjDao->getUserRec($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getUserRec - ".$e);
			return false; 
		}
	}
	public function addUser($data){
		try{
			$this->log->logIt($this->module." - addUser");
			
			$ObjDao = new \database\userdao();
			$data = $ObjDao->addUser($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - addUser - ".$e);
			return false; 
		}
	}
	
	//Order
	public function addcontactdetail($data){
		try{
			$this->log->logIt($this->module." - addcontactdetail");
			
			$ObjOrderDao = new \database\ordersdao();
			$data = $ObjOrderDao->addcontactdetail($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - addcontactdetail - ".$e);
			return false; 
		}
	}
	
	public function getmenuitems($data){
		try{
			$this->log->logIt($this->module." - getmenuitems");
			
			$ObjOrderDao = new \database\ordersdao();
			$data = $ObjOrderDao->getmenuitems($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getmenuitems - ".$e);
			return false; 
		}
	}

	// For Dashboard
	public function loadreservationlist($data)
	{
		try
		{
			$this->log->logIt($this->module." - loadreservationlists");
			
			$ObjdashDao = new \database\dashboarddao();
			$data = $ObjdashDao->reservationlist();



			return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - loadreservationlists - ".$e);
			return false; 
		}
	}
	
}
?>