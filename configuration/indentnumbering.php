<?php

class indentnumbering
{
    public $module = 'indentnumbering';
    public $log;
    private $language,$lang_arr,$default_lang_arr,$Reset_type;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->Reset_type = new \util\language('store_Reset');
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(64,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(64);
            $ObjDocDao = new \database\indentnumberingdao();
            $docnumdata = $ObjDocDao->doclist();

            $reset_lang_arr = \common\staticlang::$store_Reset;
            $reset = $this->Reset_type->loadlanguage($reset_lang_arr);
            $resetArr=html_entity_decode(json_encode($reset),ENT_QUOTES);
            $reset_arr = (array)json_decode($resetArr);
            $data = json_decode($docnumdata, true);
            $this->loadLang();
            $template = $twig->loadTemplate('indentnumbering.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['datalist'] = $data[0]['data'];
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['module'] = $this->module;
            $senderarr['reset'] = $reset_arr;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];

            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function docnum($data)
    {
        try {
            $this->log->logIt($this->module . " - docnum");
            $this->loadLang();
            $data['module'] = $this->module;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $data['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $data['default_langlist'] = json_decode($defaultlanguageArr);
            $ObjDocDao = new \database\indentnumberingdao();
            $id = $ObjDocDao->docupdate($data);
            if ($id == "" || $id == 0)
                return json_encode(array("Success" => "False", 'Data' => $id));
            else
                return json_encode(array("Success" => "True", 'Data' => $id));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - docnum - " . $e);

        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$indentnumbering;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }
}

?>