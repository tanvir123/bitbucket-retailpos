<?php

class physicalinventorytracking
{
    public $module = 'physicalinventorytracking';
    public $log;
    private $language, $lang_arr, $default_lang_arr;
    public $ObjPurchaseDao;
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->ObjPurchaseDao = new \database\physicalinventorytrackingdao();
    }

    public function load()
    {
        try {
        	
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(59,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(59);
            $arr_purchase_orders = json_encode(array(array("cnt" => 0, "data" => [])));
            $template = $twig->loadTemplate('physicalinventorytracking.html');

            $ObjCategoryDao = new \database\raw_categorydao();
            $catData = $ObjCategoryDao->getAllCategory();
            $cat_list = json_decode($catData, 1);
            $js_date = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $rawMaterialDao = new \database\physicalinventorytrackingdao();
            
			$dateformat = \database\parameter::getParameter('dateformat');
			$ObjUtil = new \util\util;
			$cudate = date($dateformat);
			
			$fromdate = $ObjUtil->convertDateToMySql($cudate);
			
			$data = [];
			$data['date'] = $cudate;
			$storeRawMaterials = $rawMaterialDao->storeWiseRawMaterials($data);
			$round_off = \database\parameter::getParameter('digitafterdecimal');
			
            $this->loadLang();
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['login_type'] = CONFIG_LOGINTYPE;
            $sender_arr['module'] = $this->module;
            $sender_arr['datalist'] = $storeRawMaterials;
            $sender_arr['currentdate'] = $fromdate;
            $sender_arr['displaycurrentdate'] = $cudate;
            $sender_arr['category'] = $cat_list['Data'];
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $sender_arr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $sender_arr['default_langlist'] = json_decode($defaultlanguageArr);
            $sender_arr['jsdateformat'] = $js_date;
            $sender_arr['roundoff'] = $round_off;
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            $sender_arr['lang_type'] = CONFIG_CUSTOM_LANG;
            $sender_arr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];

            echo $template->render($sender_arr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }
    
    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$physicalinventorytracking;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
    
    public function updatephysicalinventory($data)
	{
		try{
			$this->log->logIt($this->module.' - updatephysicalinventory');
			$flag = \util\validate::check_notnull($data,array('current_date'));
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr=json_decode($defaultlanguageArr);
            if($flag=='true')
            {
                $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
                $languageArr=json_decode($languageArr);
				$ObjUnitDao = new \database\physicalinventorytrackingdao();
				$data = $ObjUnitDao->updatePhysicalInventory($data,$languageArr);
				return $data;
			}else
				return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->Missing_Field));
		}catch (Exception $e)
		{
			$this->log->logIt($this->module . " - updatephysicalinventory - " . $e);
			return json_encode(array("Success"=>"True", "Message" => $e->getMessage()));
		}
	}
	
	public function getinventorylist($data)
	{
		try{
			$this->log->logIt($this->module.' - getinventorylist');
			$flag = \util\validate::check_notnull($data,array('search_date'));
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);
			if($flag=='true')
			{
				
				$rawMaterialDao = new \database\physicalinventorytrackingdao();
				
				$dateformat = \database\parameter::getParameter('dateformat');
				$mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
				$ObjUtil = new \util\util;
				$cudate = $data['search_date'];
				
				$fromdate = $ObjUtil->convertDateToMySql($cudate);;
				$data['date'] = $fromdate;
				$storeRawMaterials = $rawMaterialDao->storeWiseRawMaterials($data);
				if(empty($storeRawMaterials) || !$storeRawMaterials)
				{
					$storeRawMaterials = [];
				}
				return json_encode(array('Success'=>'True','Data'=>$storeRawMaterials));
			}else
				return json_encode(array('Success'=>'False','Message'=>$default_langlist->Missing_Field));
		}catch (Exception $e)
		{
			$this->log->logIt($this->module . " - updatephysicalinventory - " . $e);
			return json_encode(array("Success"=>"True", "Message" => $e->getMessage()));
		}
	}
}

?>