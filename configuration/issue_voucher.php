<?php

class issue_voucher
{
    public $module = 'issue_voucher';
    public $log;
    private $language, $lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(54, $this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(54);

            $ObjVoucherDao = new \database\issuevoucherdao();
            $ObjStoreDao = new \database\indentrequestdao();

            $arr_indent = $ObjVoucherDao->createdVoucherList(50,'0' ,'', '', '', '');

            $this->loadLang('ISSUE_VOUCHER_LIST');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $storeData = $ObjStoreDao->getallstorelist();
            $store_list = json_decode($storeData, 1);
            $ObjCategoryDao = new \database\raw_categorydao();
            $arr_category   = $ObjCategoryDao->getAllCategory();
            $category_list  = json_decode($arr_category, 1);

            $js_date_format = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $this->language = new \util\language('issue_voucher_list');
            $this->loadLang("ISSUE_VOUCHER_LIST");
            $template = $twig->loadTemplate('issue_voucher_list.html');

            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $sender_arr['login_type'] = CONFIG_LOGINTYPE;
            $sender_arr['module'] = $this->module;
            $sender_arr['datalist'] = $arr_indent;
            $sender_arr['storelist'] = $store_list['Data'];
            $sender_arr['category'] = $category_list['Data'];
            $sender_arr['langlist'] = json_decode($languageArr);
            $sender_arr['jsdateformat'] = $js_date_format;
            $sender_arr['roundoff'] = $round_off;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $sender_arr['default_langlist'] = json_decode($defaultlanguageArr);
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            $sender_arr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($sender_arr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function callissuevoucher($data)
    {
        try {
            $this->log->logIt($this->module . " - load");
            global $twig;

            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(54, $this->module);

            $ObjCommonDao = new \database\commondao();
            $privilegeList = $ObjCommonDao->getuserprivongroup(54);


            $ObjIdent = new \database\indentrequestdao();
            $storeData = $ObjIdent->getallstorelist();
            $storelist = json_decode($storeData, 1);
            $ObjCommonDao = new \database\commondao();
            $currencysign = $ObjCommonDao->getCurrencySignCompany();
            $Objcategorydao = new \database\raw_categorydao();
            $catData = $Objcategorydao->getAllCategory();
            $catlist = json_decode($catData, 1);
            $indentNo = $ObjCommonDao->getIndentDocumentNumbering('issue_voucher', '');
            $round_off = \database\parameter::getParameter('digitafterdecimal');
 
            $jsdate = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $template = $twig->loadTemplate('issue_voucher.html');

            $Objpuchasedao = new \database\purchaseorderdao();
            $applicable_taxes = $Objpuchasedao->getAppliedTax();
            $issueIndentDetail = '';

            $this->language = new \util\language('Generatevoucher');
            $this->loadLang("GENERATE_VOUCHER");
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $issueIndentDetail;
            $senderarr['indent_id'] = $data['indent_id'];
            $senderarr['issueid'] = $data['issueid'];
            $senderarr['applicable_taxes'] = $applicable_taxes;
            $senderarr['currencysign'] = $currencysign;
            $senderarr['voucherno'] = $indentNo;
            $senderarr['round_off'] = $round_off;
            $senderarr['category'] = $catlist['Data'];
            $senderarr['storelist'] = $storelist['Data'];
            $senderarr['langlist'] = json_decode($languageArr);
            $senderarr['jsdateformat'] = $jsdate;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            echo json_encode(array("Success" => "True", "Data" => $template->render($senderarr)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - callissuevoucher - " . $e);
            return false;
        }
    }

    public function loadLang($operation)
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr='';
            if ($operation == 'ISSUE_VOUCHER_LIST') {
                    $this->language = new \util\language($this->module);
                    $default_lang_arr = \common\staticlang::$issue_voucher;
            }
            if ($operation == "GENERATE_VOUCHER") {
                $this->language = new \util\language('Generatevoucher');
                $default_lang_arr = \common\staticlang::$Generatevoucher;
            }
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }

    public function addissueindent($data)
    {
        try {
            $this->log->logIt($this->module . ' - addissueindent');
           // $this->log->logIt($data);

            $req_arr = array(
                "po_items" => $data['po_items'],
                "total_amount" => $data['total_amount'],
                "issue_date" => $data['issue_date'],
                "pstoreid" => $data['pstoreid'],
                "premarks" => $data['po_remarks'],
                "id" => $data['id'],
                "Indent_id" => $data['Indent_id'],
                "ISSUEID" => $data['ISSUEID'],
                "module" => $this->module
            );
            $ObjUnitDao = new \database\indentdao();
            $this->loadLang('GENERATE_VOUCHER');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);
            $data = $ObjUnitDao->addissueindent($req_arr,$languageArr,$defaultlanguageArr);
            return $data;

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addissueindent - ' . $e);
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');
            $limit = 50;
            $offset = 0;
            $from_date = "";
            $to_date = "";
            $voucher = "";
            $store_id = "";

            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['fromdate']) && $data['fromdate'] != "")
                $from_date = $data['fromdate'];
            if (isset($data['todate']) && $data['todate'] != "")
                $to_date = $data['todate'];
            if (isset($data['voucher']) && $data['voucher'] != "")
                $voucher = $data['voucher'];
            if (isset($data['storeid']) && $data['storeid'] != "")
                $store_id = $data['storeid'];

            $ObjVoucherDao = new \database\issuevoucherdao();
            $result = $ObjVoucherDao->createdVoucherList($limit, $offset, $from_date, $to_date, $voucher, $store_id);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function printInvoice($data)
    {
        try {
            $this->log->logIt($this->module . ' - printInvoice');
            $id = $data['id'];

            $ObjIndentDao = new \database\issuevoucherdao();
            $this->loadLang('GENERATE_VOUCHER');
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $headerData = $ObjIndentDao->getissuedetail($id,$defaultlanguageArr);
            if($headerData){
                $headerData['default_langlist'] = json_decode($defaultlanguageArr);         $senderarr['user_type'] = CONFIG_USR_TYPE;
                $indentlanguage = new \util\language('AuthorizeReceivedindent');
                $llang_arr = \common\staticlang::$AuthorizeReceivedindent;
                $languageArr=html_entity_decode(json_encode($indentlanguage->loadlanguage($llang_arr)),ENT_QUOTES);
                $headerData['langlist'] = json_decode($languageArr);
            }
            $PrintReport = new \common\printreport();

            $PrintReport->addTemplate('issuedvoucher');
            $width = '793px';
            $height = '1122px';

            $PrintReport->addRecord($headerData);
            $data = $PrintReport->Request();
            return json_encode(array("Success" => "True", "Data" => $data, "Height" => $height, "Width" => $width));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - printInvoice - ' . $e);
        }
    }

    public function getIssueIndentRecordsonID($data)
    {
        try {
            $this->log->logIt($this->module . " - getIssueIndentRecordsonID");
            $ObjIssueIndentDao = new \database\indentdao();
            $issueIndentDetail = '';
            if ($data['id'] != 0) {
                $issueIndentDetail = $ObjIssueIndentDao->getIssueIndentRecordsonID($data['id']);
            } else {
                if($data['id'] =='0')
                {
                    $issueIndentDetail = $ObjIssueIndentDao->getIssueRecordsonID($data['Issueid']);
                }
            }


            return $issueIndentDetail;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getIssueIndentRecordsonID - " . $e);
            return false;
        }
    }
}

?>