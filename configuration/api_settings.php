<?php
class api_settings
{
    private $module = 'api_settings';
    private $log;
    private $language, $lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_api_settings');
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module. ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(30,$this->module);
            $ObjDao = new \database\apisettingsdao();
            $data = $ObjDao->loadCompanyInfo();
            $data_arr = json_decode($data, true);
            $dta_arr = $data_arr['Data'];

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(30);


            $tbloc = $dta_arr['company_info'];
            $tbapp = $tbloc['tabinsta_app_id'];
            $tbsec = $tbloc['tabinsta_app_secret'];

            $arr = array(
                'app_id' => $tbapp,
                'app_secret' => $tbsec,
            );
           // $list = $this->tabinstalocation($arr);
           // $listtab = json_decode($list, true);


            $template = $twig->loadTemplate('api_settings.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['datalist'] = $dta_arr;
            //$senderarr['tab'] = $listtab['payloalisttabinstalocationd'];
            $languageArr=html_entity_decode(json_encode($this->lang_arr));
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            
            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function editApiInfo($data)
    {
        try {
            $this->log->logIt($this->module . ' - editApiInfo');
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
            $default_langlist = json_decode($defaultlanguageArr);
            $flag1=$flag3='true';
           /* if($data['enable_tabinsta']=='1') {
                if(!isset($data['tb_insta_app_id']) ||  $data['tb_insta_app_id']==''){
                    $flag1 = 'false';
                }
                if(!isset($data['tb_insta_app_secret']) ||  $data['tb_insta_app_secret']==''){
                    $flag1 = 'false';
                }
                //$flag1 = \util\validate::check_notnull($data, array('tb_insta_app_id', 'tb_insta_app_secret'));
            }*/

            if(isset($data['enable_loyalty']) && $data['enable_loyalty']=='1') {
                if(!isset($data['loyalty_appid']) ||  $data['loyalty_appid']==''){
                    $flag1 = 'false';
                }
                if(!isset($data['loyalty_appsecret']) ||  $data['loyalty_appsecret']==''){
                    $flag1 = 'false';
                }
            }

            if(isset($data['enable_pms']) && $data['enable_pms']=='1') {
                //$flag3 = \util\validate::check_notnull($data, array('pms_username', 'pms_password'));
                if(!isset($data['pms_username']) ||  $data['pms_username']==''){
                    $flag3 = 'false';
                }
                if(!isset($data['pms_password']) ||  $data['pms_password']==''){
                    $flag3 = 'false';
                }
            }
            $flag2 = 'true';

            /*if (isset($data['tabinstalocation']) && count($data['tabinstalocation']) > 0) {
                foreach ($data['tabinstalocation'] as $value) {
                    if ($value == '')
                        $flag2 = 'false';
                }
            }*/

            if (isset($data['pms_hotel_id']) && count($data['pms_hotel_id']) > 0) {
                foreach ($data['pms_hotel_id'] as $value) {
                    if ($value == '')
                        $flag2 = 'false';
                }
            }
            
            if ($flag1 == 'true' && $flag2 == 'true' && $flag3=='true') {

                $ObjDao = new \database\apisettingsdao();
                $rec = $ObjDao->UpdateApiSettings($data);
                return $rec;
            } else {
                return json_encode(array('Success' => 'False', 'Message' => $default_langlist->PLEASE_FILL_NECESSARY_INFORMATION));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - editApiInfo - ' . $e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_api_settings;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }


    public function tabinstalocation($data)
    {
        try {
            $this->log->logIt($this->module . '-listtabinstalocation');
            $tabinsta_app_id = $data['app_id'];
            $tabinsta_app_secret = $data['app_secret'];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, CONFIG_LOCATIONSYNC);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type:application/json",
                "app-id:$tabinsta_app_id",
                "app-secret:$tabinsta_app_secret",
            ));
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $res = curl_exec($ch);
            curl_close($ch);
            $data = trim($res);
            return $data;

        } catch (Exception $e) {
            $this->log->logIt($this->module . '-listtabinstalocation -' . $e);
        }
    }


}

?>