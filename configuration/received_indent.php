<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 13/4/18
 * Time: 3:06 PM
 */
class received_indent
{
    public $module = 'received_indent';
    public $log;
    private $language, $lang_arr, $default_lang_arr;
    public $ObjIndentDao;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->ObjIndentDao = new \database\indentdao();
    }

    public function load()
    {
        try
        {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(28, $this->module);

            $ObjCommonDao = new \database\commondao();
            $ObjRECIndentDao = new \database\indentdao();
            $ObjIndentDao = new \database\indentrequestdao();
            
            $arr_indent = $ObjRECIndentDao->receivedIndentList(50,'0' ,'', '', '', '', '');
            $this->loadLang('RECEIVED_INDENT');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $template = $twig->loadTemplate('received_indent.html');

            $currencysign =$ObjCommonDao->getCurrencySignCompany();

            $storeData = $ObjIndentDao->getallstorelist();
            $storelist = json_decode($storeData, 1);

            $ObjCategoryDao = new \database\raw_categorydao();
            $arr_category = $ObjCategoryDao->getAllCategory();
            $category_list = json_decode($arr_category, 1);
            
            $js_date_format = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['login_type'] = CONFIG_LOGINTYPE;
            $sender_arr['module'] = $this->module;
            $sender_arr['datalist'] = $arr_indent;
            $sender_arr['currencysign'] = $currencysign;
            $sender_arr['storelist'] = $storelist['Data'];
            $sender_arr['category'] = $category_list['Data'];
            $sender_arr['langlist'] = json_decode($languageArr);
            $sender_arr['jsdateformat'] = $js_date_format;
            $sender_arr['roundoff'] = $round_off;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $sender_arr['default_langlist'] = json_decode($defaultlanguageArr);
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            $sender_arr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($sender_arr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }
    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');
            $limit = 50;
            $offset = 0;
            $from_date = "";
            $to_date = "";
            $indent = "";
            $store_id = "";
            $status = "";

            if (isset($data['limit']) && $data['limit']!= "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset']!= "")
                $offset = $data['offset'];
            if (isset($data['fromdate']) && $data['fromdate']!= "")
                $from_date = $data['fromdate'];
            if (isset($data['todate']) && $data['todate']!= "")
                $to_date = $data['todate'];
            if (isset($data['indent']) && $data['indent']!= "")
                $indent = $data['indent'];
            if (isset($data['locationid']) && $data['locationid']!= "")
                $store_id = $data['locationid'];
            if (isset($data['statusid']) && $data['statusid']!= "")
                $status = $data['statusid'];
            $ObjUserDao = new \database\indentdao();
            $result = $ObjUserDao->receivedIndentList($limit, $offset,$from_date,$to_date,$indent,$store_id,$status);


            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }
    public function authorize_indent($data){
        try{
            $this->log->logIt($this->module . ' - authorize_indent');
            $this->language = new \util\language('AuthorizeReceivedindent');
            $this->loadLang("AUTHORIZE_RECEIVED_INDENT");
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);
            $result = $this->ObjIndentDao->authorizeReceivedIndent($data,$defaultlanguageArr);
            return $result;
        }catch (Exception $e) {
            $this->log->logIt($this->module . ' - authorize_indent - ' . $e);
        }
    }
    public function loadAuthorizeIndentItem($data){
        try{
            $this->log->logIt($this->module . ' - loadAuthorizeIndentItem');
            global $twig;
            $indentDetail = $this->ObjIndentDao->getIndentDetail($data['indent_id']);
            $ObjIndentRequest = new \database\indentrequestdao();
            $indentItems = $ObjIndentRequest->getIndentItemsToIssue($data['indent_id']);


            $template = $twig->loadTemplate('authindentqty.html');
            $this->language = new \util\language('AuthorizeReceivedindent');
            $this->loadLang("AUTHORIZE_RECEIVED_INDENT");
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['indentDetail'] = $indentDetail;
            $sender_arr['indentItems'] = $indentItems;
            $sender_arr['langlist'] = json_decode($languageArr);
            $sender_arr['default_langlist'] = json_decode($defaultlanguageArr);
            echo json_encode(array("Success" => "True","Data" => $template->render($sender_arr)));
        }catch (Exception $e) {
            $this->log->logIt($this->module . ' - loadAuthorizeIndentItem - ' . $e);
        }
    }
    public function loadLang($operation)
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr='';
            if($operation=='RECEIVED_INDENT'){
                $this->language = new \util\language($this->module);
                $default_lang_arr = \common\staticlang::$received_indent;
            }
            if($operation=='AUTHORIZE_RECEIVED_INDENT'){
                $this->language = new \util\language('AuthorizeReceivedindent');
                $default_lang_arr = \common\staticlang::$AuthorizeReceivedindent;
            }

            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
    public function authorizeIndentItems($data){
        try{
            $this->log->logIt($this->module.' - authorizeIndentItems');
            $ObjIndentRequest = new \database\indentrequestdao();
            $this->loadLang('AUTHORIZE_RECEIVED_INDENT');
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $result = $ObjIndentRequest->authorizeIndentItemsQty($data,$default_langlist);
            return $result;
        }catch (Exception $e) {
            $this->log->logIt($this->module.' - authorizeIndentItems - '.$e);
        }
    }
    public function ReceivedIndentprintInvoice($data)
    {
        try
        {
            $this->log->logIt($this->module.' - ReceivedIndentprintInvoice');
            $id = $data['id'];

            $ObjIndentDao = new \database\indentdao();
            $headerData = $ObjIndentDao->getreceviedindentdetail($id);
            if($headerData){
                $this->loadLang('AUTHORIZE_RECEIVED_INDENT');
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
                $headerData['default_langlist'] = json_decode($defaultlanguageArr);         $senderarr['user_type'] = CONFIG_USR_TYPE;
                $indentlanguage = new \util\language('AuthorizeReceivedindent');
                $llang_arr = \common\staticlang::$AuthorizeReceivedindent;
                $languageArr=html_entity_decode(json_encode($indentlanguage->loadlanguage($llang_arr)),ENT_QUOTES);
                $headerData['langlist'] = json_decode($languageArr);
            }
            $PrintReport = new \common\printreport();

            $PrintReport->addTemplate('receivedindentvoucher');
            $width = '793px';
            $height = '1122px';

            $PrintReport->addRecord($headerData);
            $data = $PrintReport->Request();
            return json_encode(array("Success"=>"True","Data"=>$data, "Height"=>$height, "Width"=>$width));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - ReceivedIndentprintInvoice - '.$e);
        }
    }

}
?>