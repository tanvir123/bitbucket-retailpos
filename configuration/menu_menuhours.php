<?php
class menu_menuhours   
{
    public $module='menu_menuhours';
    public $log;
    private $language,$lang_arr,$default_lang_arr;
    public $jsdateformat;
    
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $jstime =  \common\staticarray::$jstimeformat[\database\parameter::getParameter('timeformat')];

            $ObjUserDao = new \database\menu_menuhoursdao();
            $data = $ObjUserDao->menuhourslist(50,'0','','');

            $template = $twig->loadTemplate('menu_menuhours.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['datalist'] = $data;
            $senderarr['module'] = $this->module;
            $senderarr['jsdateformat'] = $jsdate;
            $senderarr['jstime'] = $jstime;
            $senderarr['langlist'] = $this->lang_arr;
            $senderarr['default_langlist'] = $this->default_lang_arr;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
            $flag = \util\validate::check_notnull($data,array('txtname','txtopentime','txtclosetime'));
            if($flag=='true')
            {
               $data = array(
                            "name" =>$data['txtname'],
                            "opentime" =>$data['txtopentime'],
                            "closetime"=>$data['txtclosetime'],
                            "dayofweek" =>$data['weekdays'],
                            "id"=>$data['id'],
                            "rdo_status"=>$data['rdo_status'],
                            "module" => $this->module
                        );
                $ObjmenuhourDao = new \database\menu_menuhoursdao();
                $data = $ObjmenuhourDao->addmenuhour($data);
                return $data;  
            }
            else
                return json_encode(array('Success'=>'False','Message'=>'Some field is missing'));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }

    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            $ObjmenuhourDao = new \database\menu_menuhoursdao();
            $data = $ObjmenuhourDao->menuhourslist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    public function getRecmenuhour($data)
	{
		try
		{
			$this->log->logIt($this->module." - getRecmenuhour");
			
			$ObjmenuhourDao = new \database\menu_menuhoursdao();
			$data = $ObjmenuhourDao->getRecmenuhour($data);
			return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - getRecmenuhour - ".$e);
			return false; 
		}
	}
     public function loadLang()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $arr = array(
                "LANG1" => "Add Menu Hours",
                "LANG2" => "Name",
                "LANG3" => "Enter Name",
                "LANG4" => "Open time",
                "LANG5" => "Close time",
                "LANG6" => "Time",
                "LANG7" => "To",
                "LANG8" => " Days of Week"
            );
            $this->lang_arr = $this->language->loadlanguage($arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
    
}
?>