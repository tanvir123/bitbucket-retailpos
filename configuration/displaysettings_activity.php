<?php

class displaysettings_activity
{
    private $module='displaysettings_activity';
    private $log;
    private $encdec;
    private $language,$lang_arr,$default_lang_arr,$languageArr,$defaultlanguageArr;
    
    function __construct()
    {
        try
        {
            $this->log = new \util\logger();
            if(CONFIG_LOGINTYPE == 1)
            {
                $this->language = new \util\language('config_displaysettingslogs');
                $this->default_lang_arr = \common\staticlang::$config_displaysettingslogs;
            }
            else{
                $this->language = new \util\language('displaysettings');
                $this->default_lang_arr = \common\staticlang::$displaysettings;
            }

            $this->lang_arr = $this->language->loadlanguage($this->default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
            $this->languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $this->defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
    public function load($data)
    {
        try
        {
            $this->log->logIt($this->module." - load");
            global $twig;
            $flag = 0;
            if(!isset($data) || !isset($data['module']))
            {
                $flag==1;
            }
            else
            {
                $module = $data['module'];
                $ObjStaticArray =new \common\staticarray();
                $settings_arr = $ObjStaticArray->$module;
            }
            $display_setting_drop=json_decode($this->languageArr);
            $template = $twig->loadTemplate('displaysettings_activity.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['module'] = $module;
            $senderarr['settings'] = (array)$display_setting_drop;
            $senderarr['default_langlist'] = json_decode($this->defaultlanguageArr);
            $rec = $template->render($senderarr);
            return json_encode(array("Data"=>$rec));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - load - ".$e);
        }
    }

    public function displayactivitylog($data)
    {
        try
        {
            $this->log->logIt($this->module." - displayactivitylog");
            if(!isset($data['keyname']) || !isset($data['module']))
            {
                return json_encode(array("Success"=>"False","Message"=>"Please select Key"));
            }
            else
            {
                $module = $data['module'];
                $keyname = $data['keyname'];
                $ObjAuditDao = new \database\auditlogdao();
                $result = $ObjAuditDao->loaddisplaysettingslogs($module,$keyname);
                return $result;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - displayactivitylog - ".$e);
        }
    }
}
?>