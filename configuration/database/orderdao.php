<?php

namespace database;
class orderdao
{
    public $module = 'DB_orderdao';
    public $log;
    public $dbconnect;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->mongo_con = new \MongoDB\Driver\Manager(CONFIG_MONGO_HOST);
    }

    public function orderslist($limit, $offset, $name, $from_date = "", $to_date = "", $folio_no = "",$defaultlanguageArr='',$today='',$module='')
    {
        try {
            $this->log->logIt($this->module . ' - orderslist - ' . $name);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $timeformat = \common\staticarray::$mysqltimeformat[\database\parameter::getParameter('timeformat')];
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $ObjUtil = new \util\util;
            $strSql = "SELECT TRC.contacttype,FM.hashkey as fm_hashkey,FM.foliounkid,IFNULL(FM.foliono,'') as foliono,IF(FM.isfolioclosed=1,'".$defaultlanguageArr->YES."','".$defaultlanguageArr->NO."') AS isfolioclosed,TRC.name,FM.foliostatus,FM.ispostedtopms,
             IFNULL(DATE_FORMAT(FM.servedate,'" . $mysqlformat . "'),'') AS servedate,
              IFNULL(TIME_FORMAT(FM.servedate,'" . $timeformat . "'),'') as sreveTime,FM.ordertype,
                      IFNULL(DATE_FORMAT(FM.opendate,'" . $mysqlformat . "'),'') AS orderDate,
                       IFNULL(TIME_FORMAT(FM.opendate,'" . $timeformat . "'),'') as orderTime,
                       FM.no_of_person,                      
                       IFNULL(ROUND((SUM(CASE WHEN FASM.mastertypeunkid IN (1,2,3,4,8,9,10) and FD.isvoid=0 THEN FD.baseamount*FASM.crdr*FD.quantity ELSE 0 END)-IFNULL(SUM(BFM.meta_value),0)),".$round_off."),0) AS orderAmt,
                       IFNULL(ROUND(SUM(CASE WHEN (FASM.mastertypeunkid IN (7,13) and FD.isrefund=0 and FD.isvoid=0 )THEN (FD.baseamount*FD.quantity*IF(FASM.mastertypeunkid=13,(1),(-1)))  ELSE 0 END),$round_off),0) AS paidAmt,
                       IFNULL(ROUND(SUM(CASE WHEN (FASM.mastertypeunkid IN (7,13) and FD.isrefund=1 and FD.isvoid=2 )THEN (FD.baseamount*FD.quantity*IF(FASM.mastertypeunkid=13,(1),(-1)))  ELSE 0 END),$round_off),0) AS refundAmt,
                      ".
                "CASE 
                       WHEN TRC.contacttype=1 THEN ''
                       WHEN TRC.contacttype=4 THEN '(".$defaultlanguageArr->NO_CHARGE_USER.")'
                       ELSE ''
                       END As usertype,IFNULL(TBLBOOK.is_confirm,' ') as is_confirm ".
                       " FROM " . CONFIG_DBN . ".trcontact AS TRC 
                       INNER JOIN " . CONFIG_DBN . ".fasfoliomaster AS FM ON TRC.contactunkid=FM.lnkcontactid and TRC.companyid=:companyid AND FM.isvoid=0 AND FM.locationid=:locationid
                      
                     
                        LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail FD ON FM.foliounkid=FD.foliounkid
                       LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid = FD.masterunkid AND FASM.companyid=:companyid AND FASM.locationid=:locationid
                       
                       
                       LEFT JOIN ".CONFIG_DBN.".bmfoliodetail_meta AS BFM ON FD.detailunkid = BFM.lnkdetailunkid AND BFM.companyid=:companyid AND BFM.locationid=:locationid AND FD.isvoid = 0
                       LEFT JOIN " . CONFIG_DBN . ".cfcompany AS cmp ON cmp.companyid =  :companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cflocation AS cfl ON cfl.locationid = :locationid 
                       LEFT JOIN ".CONFIG_DBN.".cftable_booking AS TBLBOOK ON TBLBOOK.foliolnkid = FM.foliounkid AND TBLBOOK.locationid = :locationid AND TBLBOOK.companyid= :companyid
                       WHERE TRC.companyid=:companyid AND TRC.locationid=:locationid AND FM.isvalid=1 ";
            if($module == 'dash'){
                $strSql .= " AND FM.canceluserunkid=0 AND FM.isfolioclosed=1 ";
            }
            if ($name != "") {
                $strSql .= " AND TRC.name LIKE '%" . $name . "%'";
            }
            if ($folio_no != "") {
                $strSql .= " AND FM.foliono LIKE '%" . $folio_no . "%'";
            }
            if ($from_date != "" && $to_date != "") {
                $strSql .= " AND CAST(FM.opendate AS DATE) BETWEEN :from_date AND :to_date";
            }
            $strSql .= " AND FM.isvalid=1 GROUP BY FM.foliounkid ORDER BY FM.foliounkid DESC";
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            if ($from_date != "" && $to_date != "") {
                $dao->addParameter(':from_date', $ObjUtil->convertDateToMySql($from_date));
                $dao->addParameter(':to_date', $ObjUtil->convertDateToMySql($to_date));
            }
            $data = $dao->executeQuery();

            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $dao->initCommand($strSql . $strSqllmt);
            } else {
                $dao->initCommand($strSql);
            }

            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            if ($from_date != "" && $to_date != "") {
                $dao->addParameter(':from_date', $ObjUtil->convertDateToMySql($from_date));
                $dao->addParameter(':to_date', $ObjUtil->convertDateToMySql($to_date));
            }

            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return html_entity_decode(json_encode($retvalue));
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - orderslist - ' . $e);
        }
    }

    public function addfolio($data)
    {
        try {
            $this->log->logIt($this->module . ' - addfolio');

            $localdatetime = \util\util::getLocalDateTime();
            $year = \util\util::getYearCode();
            $dao = new \dao;
            $invno = "";
            $invno = $this->getInvoiceNumber('inc');

            $ObjAuditDao = new \database\orderlogdao();

            if ($data['contactid'] == "" || $data['contactid'] == "0") {
                $hashkey = \util\util::gethash();
                $strSql = "INSERT INTO " . CONFIG_DBN . ".trcontact SET salutation=:salutation, name=:name" .
                    " , business_name=:business_name, address=:address, state=:state, city=:city" .
                    " , zip=:zip, country=:country, phone=:phone, contacttype=:contacttype" .
                    " , mobile=:mobile, email=:email, fax=:fax";

                if ($data['info_birth_date'] != "")
                    $strSql .= " ,dateofbirth='" . \util\util::convertDateToMySql($data['info_birth_date']) . "'";
                if ($data['info_weddinganniversary'] != "")
                    $strSql .= " ,Aniversary_date='" . \util\util::convertDateToMySql($data['info_weddinganniversary']) . "'";
                if ($data['info_spousebirthdate'] != "")
                    $strSql .= " ,spouse_bdate='" . \util\util::convertDateToMySql($data['info_spousebirthdate']) . "'";

                $strSql .= "  ,hashkey=:hashkey" .
                    " , is_foliocreated=:is_foliocreated, companyid=:companyid,locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':salutation', $data['salutation']);
                $dao->addParameter(':name', $data['info_name']);
                $dao->addParameter(':business_name', $data['info_company']);
                $dao->addParameter(':address', $data['info_address']);
                $dao->addParameter(':state', $data['info_state']);
                $dao->addParameter(':city', $data['info_city']);
                $dao->addParameter(':zip', $data['info_zip']);
                $dao->addParameter(':country', $data['info_select_country']);
                $dao->addParameter(':contacttype', 1);
                $dao->addParameter(':phone', $data['info_phone']);
                $dao->addParameter(':mobile', $data['info_mobile']);
                $dao->addParameter(':email', $data['info_email']);
                $dao->addParameter(':fax', $data['info_fax']);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':is_foliocreated', '1');
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                $id = $dao->getLastInsertedId();
            } else {
                $ObjCommonDao = new \database\commondao();
                $id = $ObjCommonDao->getprimarykey('trcontact', $data['contactid'], 'contactunkid');
                $hashkey = $data['contactid'];
                $strSql = "UPDATE " . CONFIG_DBN . ".trcontact SET salutation=:salutation, name=:name" .
                    " , business_name=:business_name, address=:address, state=:state, city=:city" .
                    " , zip=:zip, country=:country, phone=:phone" .
                    " , mobile=:mobile, email=:email, fax=:fax ";
                if ($data['info_birth_date'] != "")
                    $strSql .= " ,dateofbirth='" . \util\util::convertDateToMySql($data['info_birth_date']) . "'";
                else
                    $strSql .= " ,dateofbirth=NULL";
                if ($data['info_weddinganniversary'] != "")
                    $strSql .= " ,Aniversary_date='" . \util\util::convertDateToMySql($data['info_weddinganniversary']) . "'";
                else
                    $strSql .= " ,Aniversary_date=NULL";
                if ($data['info_spousebirthdate'] != "")
                    $strSql .= " ,spouse_bdate='" . \util\util::convertDateToMySql($data['info_spousebirthdate']) . "'";
                else
                    $strSql .= " ,spouse_bdate=NULL";

                $strSql .= " WHERE hashkey=:hashkey and companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':salutation', $data['salutation']);
                $dao->addParameter(':name', $data['info_name']);
                $dao->addParameter(':business_name', $data['info_company']);
                $dao->addParameter(':address', $data['info_address']);
                $dao->addParameter(':state', $data['info_state']);
                $dao->addParameter(':city', $data['info_city']);
                $dao->addParameter(':zip', $data['info_zip']);
                $dao->addParameter(':country', $data['info_select_country']);
                $dao->addParameter(':phone', $data['info_phone']);
                $dao->addParameter(':mobile', $data['info_mobile']);
                $dao->addParameter(':email', $data['info_email']);
                $dao->addParameter(':fax', $data['info_fax']);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
            }
            if ($data['folioid'] == "" || $data['folioid'] == "0") {
                $tableid=implode(',',$data['info_tableid']);
                $this->log->logIt($tableid);

                $hashkey1 = \util\util::gethash();
                $strFolio = "INSERT INTO " . CONFIG_DBN . ".fasfoliomaster SET foliostatus=1,companyid=:companyid,locationid=:locationid,year_code=:year_code, lnkcontactid=:lnkcontactid, " .
                    " opendate=:opendate,lnktableunkid=:lnktableunkid, foliotypeunkid=:foliotypeunkid, docno=:docno, foliono=:foliono, hashkey=:hashkey, folioopenuserunkid=:folioopenuserunkid";
                $dao->initCommand($strFolio);
                $dao->addParameter(':year_code', $year);
                $dao->addParameter(':lnkcontactid', $id);
                $dao->addParameter(':opendate', $localdatetime);
                $dao->addParameter(':lnktableunkid', $tableid);
                $dao->addParameter(':foliotypeunkid', 1);
                $dao->addParameter(':docno', '');
                $dao->addParameter(':foliono', $invno);
                $dao->addParameter(':hashkey', $hashkey1);
                $dao->addParameter(':folioopenuserunkid', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                $fid = $dao->getLastInsertedId();
                $ObjAuditDao->addOrderLog($fid, 'ADD_ORDER', $data, $invno);
                /*foreach($data['info_tableid']as $tableiid => $tablevalue) {
                    $str = "INSERT INTO " . CONFIG_DBN . ".cftablesession SET lnkfolioid=:lnkfolioid,
                                                                      lnktableunkid=:lnktableunkid,
                                                                      startdate=:startdate,
                                                                      isreleased=:isreleased,
                                                                      hashkey=:hashkey,
                                                                      companyid=:companyid,
                                                                      locationid=:locationid";
                    $dao->initCommand($str);
                    $dao->addParameter(':lnkfolioid', $fid);
                    $dao->addParameter(':lnktableunkid', $tablevalue);
                    $dao->addParameter(':startdate', $localdatetime);
                    $dao->addParameter(':isreleased', 0);
                    $dao->addParameter(':hashkey', $hashkey1);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $dao->executeNonQuery();
                }*/

            } else {
                $hashkey1 = $data['folioid'];
                $id = $ObjCommonDao->getprimarykey('trcontact', $data['contactid'], 'contactunkid');
                $str = "UPDATE " . CONFIG_DBN . ".fasfoliomaster  SET lnktableunkid=:lnktableunkid WHERE lnkcontactid=:lnkcontactid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($str);
                $dao->addParameter(':lnktableunkid', $data['info_tableid']);
                $dao->addParameter(':lnkcontactid', $id);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();

                $str = "SELECT foliounkid FROM " . CONFIG_DBN . ".fasfoliomaster WHERE lnkcontactid=:lnkcontactid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($str);
                $dao->addParameter(':lnkcontactid', $id);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $folioid = $dao->executeRow();


                /*$str = "UPDATE " . CONFIG_DBN . ".cftablesession  SET lnktableunkid=:lnktableunkid WHERE lnkfolioid=:lnkfolioid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($str);
                $dao->addParameter(':lnktableunkid', $data['info_tableid']);
                $dao->addParameter(':lnkfolioid', $folioid['foliounkid']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();*/


            }
            $retarr = array(
                "contact" => $hashkey,
                "inv" => $invno,
                "foliono" => $hashkey1
            );

            return html_entity_decode(json_encode(array('Success' => 'True', 'Data' => $retarr)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addfolio - ' . $e);
            return html_entity_decode(json_encode(array('Success' => 'False')));
        }
    }

    public function getInvoiceNumber($action = '')
    {
        try {
            $this->log->logIt($this->module . ' - getInvoiceNumber');
            $ObjDocNumberDao = new \database\document_numberingdao();
            $invoicenumbering = $ObjDocNumberDao->getInvoiceNumbering();
            $todaysdate = \util\util::getLocalDate();
            $reset_type = $invoicenumbering['reset_type'];
            $flag = 0;
            $dao = new \dao;
            if ($reset_type == '1') {
                $strSql = "SELECT count(foliounkid) AS cnt FROM " . CONFIG_DBN . ".fasfoliomaster WHERE companyid=:companyid AND locationid=:locationid AND CAST(opendate AS DATE) = :todaysdate";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->addParameter(':todaysdate', $todaysdate);
                $data = $dao->executeRow();
                if ($data['cnt'] == '0')
                    $flag = 1;
            } elseif ($reset_type == '2') {
                $date = \DateTime::createFromFormat("Y-m-d", $todaysdate);
                $day = $date->format("d");
                $month = $date->format("m");
                if ($day == '01') {
                    $strSql = "SELECT count(foliounkid) AS cnt FROM " . CONFIG_DBN . ".fasfoliomaster WHERE companyid=:companyid AND locationid=:locationid AND CAST(opendate AS DATE) = :todaysdate";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $dao->addParameter(':todaysdate', $todaysdate);
                    $data = $dao->executeRow();
                    if ($data['cnt'] == '0')
                        $flag = 1;
                }
            } elseif ($reset_type == '3') {
                $date = \DateTime::createFromFormat("Y-m-d", $todaysdate);
                $day = $date->format("d");
                $month = $date->format("m");
                if ($day == '01' && $month == '04') {
                    $strSql = "SELECT count(foliounkid) AS cnt FROM " . CONFIG_DBN . ".fasfoliomaster WHERE companyid=:companyid AND locationid=:locationid AND CAST(opendate AS DATE) = :todaysdate";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $dao->addParameter(':todaysdate', $todaysdate);
                    $data = $dao->executeRow();
                    if ($data['cnt'] == '0')
                        $flag = 1;
                }
            }
            if ($flag == 0)
                $no = $invoicenumbering['prefix'] . $invoicenumbering['startno'];
            else
                $no = $invoicenumbering['prefix'] . '1';

            if ($action == 'inc') {
                $updatedno = $invoicenumbering['startno'] + 1;
                $strSql = "UPDATE " . CONFIG_DBN . ".cfdocnumber SET startno=:startno WHERE companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->addParameter(':startno', $updatedno);
                $dao->executeNonQuery();
            }
            return $no;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getInvoiceNumber - ' . $e);
        }
    }

    public function addorder($data,$languageArr,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - addorder');
            $dao = new \dao();

            $ObjAuditDao = new \database\orderlogdao();
            if ($data['folioid'] != "" && $data['contactid'] != "") {
                $ObjCommonDao = new \database\commondao();
                $folio_id = $ObjCommonDao->getprimarykey('fasfoliomaster', $data['folioid'], 'foliounkid');
                if ($data['type'] == 1 || $data['type'] == 2) {
                    $ObjTaxDao = new\database\taxdao();
                    $arr_tax = $ObjTaxDao->getAppliedTax();
                    $resBaseItem = $this->addOrderItem($data, $arr_tax, $folio_id);
                    if ($data['customized_item'] != "") {
                        $resOrderModifiers = $this->addOrderModifiers($data, $arr_tax, $folio_id, $resBaseItem['Id']);
                    } else {
                        $resOrderModifiers['Total_Modifier_Price'] = 0;
                    }
                    $final_price = $resBaseItem['Base_Amount'] + $resOrderModifiers['Total_Modifier_Price'];
                    $this->calculateTotalDiscount($data, $final_price, $resBaseItem['Id'], $folio_id);
                }
                if ($data['type'] == 3) {
                    $this->addAdjustment($data, $folio_id);
                }
                if ($data['type'] == 4) {
                    $this->addTotalDiscount($data, $folio_id);
                }
                if ($data['type'] == 5) {
                    $result = $this->postPayment($data, $folio_id,$languageArr,$defaultlanguageArr);
                    return $result;
                }
                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $languageArr->LANG58)));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addorder - ' . $e);
        }
    }

    public function addOrderItem($order_data, $tax_data, $folio_id)
    {
        try {
            $this->log->logIt($this->module . ' - addOrderItem');
            $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $current_date = \util\util::getLocalDateTime();
            $year = \util\util::getYearCode();
            $hash_key = \util\util::gethash();
            $ObjCommonDao = new \database\commondao();
            $ObjInventoryDao = new \database\inventorydao();
            if ($order_data['type'] == 1) {
                $item_id = $ObjCommonDao->getprimarykey('cfmenu_items', $order_data['item'], 'itemunkid');
                $master_type = $ObjCommonDao->getmastertype('8');
            }
            if ($order_data['type'] == 2) {
                $item_id = $ObjCommonDao->getprimarykey('cfmenu_combo', $order_data['item'], 'combounkid');
                $master_type = $ObjCommonDao->getmastertype('1');
            }

            /*Add Base Item*/
            $item_amount = $order_data['amount'];
            if ($order_data['taxinc'] == 1) {
                $item_amount = $this->calculateReverseTax($order_data['amount'], $tax_data, $order_data['select_discount'], $order_data['discount']);
            }
            $taxable_amt = $item_amount * $order_data['quantity'];
            $strSql = "INSERT INTO ".CONFIG_DBN.".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                " , masterunkid=:masterunkid,year_code=:year_code,quantity=:quantity,trandate=:trandate, baseamount=:baseamount,parentid=:parentid" .
                " , description=:description,lnkmappingunkid=:lnkmappingunkid,originatedfrom=0,taxexempt=:taxexempt, hashkey=:hashkey";
            $dao->initCommand($strSql);
            $dao->addParameter(':foliounkid', $folio_id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->addParameter(":postinguserunkid", CONFIG_UID);
            $dao->addParameter(':masterunkid', $master_type['MasterId']);
            $dao->addParameter(':year_code', $year);
            $dao->addParameter(':baseamount', round($item_amount, $round_off));
            $dao->addParameter(':parentid', 0);
            $dao->addParameter(':quantity', $order_data['quantity']);
            $dao->addParameter(':description', $order_data['Comment']);
            $dao->addParameter(':lnkmappingunkid', $item_id);
            $dao->addParameter(':trandate', $current_date);
            $dao->addParameter(':taxexempt', 0);
            $dao->addParameter(':hashkey', $hash_key);
            $dao->executeNonQuery();
            $id = $dao->getLastInsertedId();
            $this->log->logIt('Item Tax');
            $this->addItemTax($order_data, $taxable_amt, $tax_data, $folio_id, $id);

            //Deduct Inventory
               $unitratio = 100;
               $operation = 0;//Decrease
               $isvoid = 0;
            if(CONFIG_IS_STORE == 1) {
                $ObjInventoryDao->updateItemInventory($item_id, $order_data['quantity'], $unitratio, $order_data['type'], $operation,$folio_id,$id,$isvoid);
            }
            //

            /*Add Base Item*/
            $resObj = array("Success" => "True", "Item_Id" => $item_id, "Base_Amount" => $taxable_amt, "Id" => $id);
            return $resObj;
        } catch (Exception $e) {
            //$dao->releaseTransaction(false);
            $this->log->logIt($this->module . ' - addOrderItem - ' . $e);
        }
    }

    public function calculateReverseTax($item_amt, $tax_data, $lstdis, $disamt)
    {
        try {
            $this->log->logIt($this->module . ' - calculateReverseTax');
            $dao = new \dao();
            $tax_per_beforedisc = 0;
            $tax_flat = 0;
            $tax_per_afterdisc = 0;
            $x = 1;
            foreach ($tax_data as $value) {
                if ($value['taxapplyafter'] == 0) {
                    if ($value['postingrule'] == 1) {
                        $tax_per_beforedisc = $tax_per_beforedisc + $value['amount'];
                    } else {
                        $tax_flat = $tax_flat + $value['amount'];
                    }
                } else {
                    if ($value['postingrule'] == 1) {
                        $tax_per_afterdisc = $tax_per_afterdisc + $value['amount'];
                    } else {
                        $tax_flat = $tax_flat + $value['amount'];
                    }
                }
            }
            $rate = $item_amt - $tax_flat;
            if ($tax_per_afterdisc == 0 && $tax_per_beforedisc != 0) {
                $rate = $item_amt - ($rate * $tax_per_beforedisc / (100 + $tax_per_beforedisc));
            } else if ($tax_per_afterdisc != 0 && $tax_per_beforedisc == 0) {
                if ($lstdis != "" && $lstdis != 0)
                    $discount = $this->getDiscount($disamt, $lstdis);
                else {
                    $discount['type'] = 1;
                    $discount['value'] = 0;
                }
                if ($discount['type'] == 1) {
                    $disc = $discount['value'];
                    if ($disc == 0) {
                        $rate = $item_amt - ($rate * $tax_per_afterdisc / (100 + $tax_per_afterdisc));
                    } else {
                        $actual_per = $tax_per_afterdisc - ($tax_per_afterdisc * $disc / 100);
                        $rate = $item_amt - ($rate * $actual_per / (100 + $actual_per));
                    }
                } else {
                    $disc = $discount['value'];
                    $Totalafterdisc = (($rate - $disc) * $tax_per_afterdisc) / (100 + $tax_per_afterdisc);
                    $rate = $item_amt - $Totalafterdisc;
                }
            } else if ($tax_per_afterdisc != 0 && $tax_per_beforedisc != 0) {
                if ($lstdis != "" && $lstdis != 0)
                    $discount = $this->getDiscount($disamt, $lstdis);
                else {
                    $discount['type'] = 1;
                    $discount['value'] = 0;
                }
                if ($discount['type'] == 1) {
                    $disc = $discount['value'];
                    if ($disc == 0) {
                        $actual_per = $tax_per_afterdisc + $tax_per_beforedisc;
                    } else {
                        $actual_per = $tax_per_afterdisc - ($tax_per_afterdisc * $disc / 100);
                        $actual_per = $actual_per + $tax_per_beforedisc;
                    }
                    $rate = $item_amt - ($rate * $actual_per) / (100 + $actual_per);
                } else {
                    $disc = $discount['value'];
                    $Totalbeforedisc = ($rate * $tax_per_beforedisc) / (100 + $tax_per_beforedisc + $tax_per_afterdisc);
                    $Totalafterdisc = (($rate - $disc) * $tax_per_afterdisc) / (100 + $tax_per_beforedisc + $tax_per_afterdisc);
                    $rate = $item_amt - ($Totalbeforedisc + $Totalafterdisc);
                }
            }
            return $rate;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - calculateReverseTax - ' . $e);
        }
    }

    public function getDiscount($discount_amt, $id)
    {
        try {
            $this->log->logIt($this->module . " - getDiscount");
            $dao = new \dao();
            $strSql = " SELECT shortcode,discount,postingrule,isopendiscount,value,isactive FROM " . CONFIG_DBN . ".cfdiscount WHERE hashkey=:discountunkid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->addParameter(':discountunkid', $id);
            $res = $dao->executeRow();
            $arr = array();
            $arr['type'] = $res['postingrule'];
            if ($res['isopendiscount'] == 1) {
                $arr['value'] = $discount_amt;
            } else {
                $arr['value'] = $res['value'];
            }
            return $arr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getDiscount - " . $e);
            return false;
        }
    }

    public function addItemTax($item_data, $item_amt, $tax_data, $folio_id, $parent_id)
    {
        try {
            $this->log->logIt($this->module . ' - addItemTax');
            $dao = new \dao();
            $current_date = \util\util::getLocalDateTime();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $year = \util\util::getYearCode();
            $discount_amt = 0;
            if ($item_data['select_discount'] != "" && $item_data['select_discount'] != 0) {
                $discount_amt = $this->calculateDiscount($item_amt, $item_data['discount'], $item_data['select_discount']);
            }
            foreach ($tax_data as $value) {
                $tax_amt = 0;
                if ($value['postingrule'] == 1) {
                    if ($value['taxapplyafter'] == 0) {
                        $tax_amt = ($item_amt * $value['amount']) / 100;
                    } else {
                        $exempt_discount = $item_amt - $discount_amt;
                        if ($exempt_discount > 0)
                            $tax_amt = ($exempt_discount * $value['amount']) / 100;
                    }
                } else {
                    $tax_amt = $value['amount'];
                }
                $hash_key = \util\util::gethash();
                $strSql = "INSERT INTO " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                    " , masterunkid=:masterunkid,year_code=:year_code,quantity=:quantity,trandate=:trandate, baseamount=:baseamount,parentid=:parentid" .
                    " , lnkmappingunkid=:lnkmappingunkid,originatedfrom=:originatedfrom,taxexempt=:taxexempt, hashkey=:hashkey";
                $dao->initCommand($strSql);
                $dao->addParameter(':foliounkid', $folio_id);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->addParameter(":postinguserunkid", CONFIG_UID);
                $dao->addParameter(':masterunkid', $value['lnkmasterunkid']);
                $dao->addParameter(':year_code', $year);
                $dao->addParameter(':baseamount', round($tax_amt, $round_off));
                $dao->addParameter(':quantity', 1);
                $dao->addParameter(':parentid', $parent_id);
                $dao->addParameter(':lnkmappingunkid', $value['taxdetailunkid']);
                $dao->addParameter(':originatedfrom', $value['taxunkid']);
                $dao->addParameter(':trandate', $current_date);
                $dao->addParameter(':taxexempt', 0);
                $dao->addParameter(':hashkey', $hash_key);
                $dao->executeNonQuery();
            }
            return 1;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addItemTax - ' . $e);
        }
    }

    public function calculateDiscount($item_amt, $discount_amt, $id)
    {
        try {
            $this->log->logIt($this->module . " - calculateDiscount");
            $dao = new \dao();
            $strSql = " SELECT shortcode,discount,postingrule,isopendiscount,value,isactive FROM " . CONFIG_DBN . ".cfdiscount WHERE hashkey=:discountunkid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->addParameter(':discountunkid', $id);
            $res = $dao->executeRow();
            if ($res['isopendiscount'] == 1) {
                $val = $discount_amt;
            } else {
                $val = $res['value'];
            }
            if ($res['postingrule'] == 1) {
                $discount_amt = ($item_amt * $val) / 100;
            } else {
                $discount_amt = $val;
            }
            return $discount_amt;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - calculateDiscount - " . $e);
            return false;
        }
    }

    public function addOrderModifiers($order_data, $tax_data, $folio_id, $parent_id)
    {
        try {
            $this->log->logIt($this->module . ' - addOrderModifiers');


            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $ObjInventoryDao = new\database\inventorydao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $current_date = \util\util::getLocalDateTime();
            $year = \util\util::getYearCode();
            $modifiers = $order_data['customized_item'];
            $modifiers_base_amt = array();
            $modifiers_total_amt = 0;
            $item_id = $ObjCommonDao->getprimarykey('cfmenu_items', $order_data['item'], 'itemunkid');
            $master_modifier_item_type = $ObjCommonDao->getmastertype('9');
            $master_modifier_type = $ObjCommonDao->getmastertype('10');
            if (is_array($modifiers) && count($modifiers) > 0) {
                foreach ($modifiers as $ordermodifier) {
                    if ($ordermodifier['quentity'] > 0) { //Add modifier only if quantity is greater than zero

                        if($ordermodifier['type']==9)
                        {
                            $recipe_type= 3;
                            $hash_key = \util\util::gethash();
                            $modifier_item_id = $ObjCommonDao->getprimarykey('cfmenu_modifier_items', $ordermodifier['modifier_item'], 'modifieritemunkid');
                            $strSql1 = "SELECT lnkmodifierrelid,sale_amount from ".CONFIG_DBN.".fdmodifier_item_relation WHERE lnkitemid=:itemid and lnkmodifieritemid=:modifieritemid AND is_deleted=0 and companyid=:companyid AND locationid=:locationid";
                            $dao->initCommand($strSql1);
                            $dao->addParameter(':itemid', $item_id);
                            $dao->addParameter(":modifieritemid", $modifier_item_id);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $res = $dao->executeRow();
                            $origin_Id = $res['lnkmodifierrelid'];
                            $posted_from = $modifier_item_id;
                            $modifier_item_price = $res['sale_amount'];
                            $modifier_item_amt = $modifier_item_price;
                            $masterId = $master_modifier_item_type['MasterId'];
                        }
                        if($ordermodifier['type']==10){

                            $recipe_type= 2;
                            $hash_key = \util\util::gethash();
                            $modifier_item_id = $ObjCommonDao->getprimarykey('cfmenu_modifiers', $ordermodifier['modifier_item'], 'modifierunkid');
                            $strSql1 = "SELECT lnkmodifierid,sale_amount from ".CONFIG_DBN.".fditem_modifier_relation 
                            WHERE lnkitemid=:itemid AND lnkmodifierid=:lnkmodifierid  AND is_deleted=0 and companyid=:companyid AND locationid=:locationid";
                            $dao->initCommand($strSql1);
                            $dao->addParameter(':itemid', $item_id);
                            $dao->addParameter(":lnkmodifierid", $modifier_item_id);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $res = $dao->executeRow();
                            $origin_Id = $res['lnkmodifierrelid'];
                            $posted_from = $modifier_item_id;
                            $modifier_item_price = $res['sale_amount'];
                            $modifier_item_amt = $modifier_item_price;
                            $masterId = $master_modifier_type['MasterId'];
                        }

                        if ($order_data['taxinc'] == 1) {
                            $modifier_item_amt = $this->calculateReverseTax($modifier_item_price, $tax_data, $order_data['select_discount'], $order_data['discount']);
                        }
                        $taxable_amt = $modifier_item_amt * ($ordermodifier['quentity'] * $order_data['quantity']);
                        $modifiers_base_amt[] = $taxable_amt;
                        $strSql = "INSERT INTO " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                            " , masterunkid=:masterunkid,year_code=:year_code, quantity=:quantity,description=:description,trandate=:trandate, baseamount=:baseamount,parentid=:parentid" .
                            " , lnkmappingunkid=:lnkmappingunkid,originatedfrom=:originatedfrom,postedfrom=:postedfrom,taxexempt=:taxexempt, hashkey=:hashkey";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':foliounkid', $folio_id);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->addParameter(":postinguserunkid", CONFIG_UID);
                        $dao->addParameter(':masterunkid', $masterId);
                        $dao->addParameter(':year_code', $year);
                        $dao->addParameter(':baseamount', round($modifier_item_amt, $round_off));
                        $dao->addParameter(':parentid', $parent_id);
                        $dao->addParameter(':quantity', $ordermodifier['quentity'] * $order_data['quantity']);
                        $dao->addParameter(':description', $order_data['Comment']);
                        $dao->addParameter(':lnkmappingunkid', $modifier_item_id);
                        $dao->addParameter(':originatedfrom', $origin_Id);
                        $dao->addParameter(':postedfrom', $posted_from);
                        $dao->addParameter(':trandate', $current_date);
                        $dao->addParameter(':taxexempt', 0);
                        $dao->addParameter(':hashkey', $hash_key);
                        $dao->executeNonQuery();
                        $id = $dao->getLastInsertedId();
                        $this->addItemTax($order_data, $taxable_amt, $tax_data, $folio_id, $id);

                        //Deduct inventory
                        $total_quantity = $ordermodifier['quentity'] * $order_data['quantity'];
                        $operation=0;//Decrease
                        $isvoid = 0;
                        if(CONFIG_IS_STORE == 1) {
                            $ObjInventoryDao->updateModifierInventory($modifier_item_id, $total_quantity, $recipe_type, $operation,$folio_id,$id,$isvoid);
                        }
                        //
                    }
                }
            }
            $modifiers_total_amt = array_sum($modifiers_base_amt);
            $resObj = array("Success" => "True", "Total_Modifier_Price" => $modifiers_total_amt);
            return $resObj;
        } catch (Exception $e) {
            //$dao->releaseTransaction(false);
            $this->log->logIt($this->module . ' - addOrderModifiers - ' . $e);
        }
    }

    public function calculateTotalDiscount($data, $amount, $parent_id, $folio_id)
    {
        try {
            $this->log->logIt($this->module . " - calculateTotalDiscount");
            $dao = new \dao();
            $discount_amt = 0;
            $current_date = \util\util::getLocalDateTime();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $year = \util\util::getYearCode();
            if ($data['select_discount'] != "" && $data['select_discount'] != 0) {
                $strSql = " SELECT shortcode,discount,postingrule,isopendiscount,value,isactive FROM " . CONFIG_DBN . ".cfdiscount WHERE hashkey=:discountunkid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->addParameter(':discountunkid', $data['select_discount']);
                $res = $dao->executeRow();
                if ($res['isopendiscount'] == 1) {
                    $val = $data['discount'];
                } else {
                    $val = $res['value'];
                }
                if ($res['postingrule'] == 1) {
                    $discount_amt = ($amount * $val) / 100;
                } else {
                    $discount_amt = $val;
                }
            }
            if ($discount_amt > 0) {
                $hash_key = \util\util::gethash();
                $ObjCommonDao = new \database\commondao();
                $discountunkid = $ObjCommonDao->getprimarykey('cfdiscount', $data['select_discount'], 'discountunkid');
                $masterunkid = $ObjCommonDao->getprimarykey('cfdiscount', $data['select_discount'], 'lnkmasterunkid');
                $strSql = "INSERT INTO " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                    " , masterunkid=:masterunkid,year_code=:year_code,quantity=:quantity,trandate=:trandate, baseamount=:baseamount,parentid=:parentid" .
                    " , description=:description,lnkmappingunkid=:lnkmappingunkid,originatedfrom=0,taxexempt=:taxexempt, hashkey=:hashkey";
                $dao->initCommand($strSql);
                $dao->addParameter(':foliounkid', $folio_id);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->addParameter(":postinguserunkid", CONFIG_UID);
                $dao->addParameter(':masterunkid', $masterunkid);
                $dao->addParameter(':year_code', $year);
                $dao->addParameter(':baseamount', round($discount_amt, $round_off));
                $dao->addParameter(':quantity', 1);
                $dao->addParameter(':parentid', $parent_id);
                $dao->addParameter(':description', $data['Comment']);
                $dao->addParameter(':lnkmappingunkid', $discountunkid);
                $dao->addParameter(':trandate', $current_date);
                $dao->addParameter(':taxexempt', 0);
                $dao->addParameter(':hashkey', $hash_key);
                $dao->executeNonQuery();
            }
            return $discount_amt;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - calculateTotalDiscount - " . $e);
            return false;
        }
    }

    public function addAdjustment($data, $folio_id)
    {
        try {
            $this->log->logIt($this->module . ' - addAdjustment');
            $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $current_date = \util\util::getLocalDateTime();
            $year = \util\util::getYearCode();
            $hash_key = \util\util::gethash();
            $ObjCommonDao = new \database\commondao();
            $master_type = $ObjCommonDao->getmastertype('4');
            $amount = $data['amount'];
            $strSql = "INSERT INTO " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                " , masterunkid=:masterunkid,year_code=:year_code,quantity=:quantity,trandate=:trandate, baseamount=:baseamount,parentid=:parentid" .
                " , description=:description,lnkmappingunkid=:lnkmappingunkid, taxexempt=:taxexempt, hashkey=:hashkey";
            $dao->initCommand($strSql);
            $dao->addParameter(':foliounkid', $folio_id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->addParameter(":postinguserunkid", CONFIG_UID);
            $dao->addParameter(':masterunkid', $master_type['MasterId']);
            $dao->addParameter(':year_code', $year);
            $dao->addParameter(':baseamount', round($amount, $round_off));
            $dao->addParameter(':parentid', 0);
            $dao->addParameter(':quantity', 1);
            $dao->addParameter(':description', $data['Comment']);
            $dao->addParameter(':lnkmappingunkid', 0);
            $dao->addParameter(':trandate', $current_date);
            $dao->addParameter(':taxexempt', 0);
            $dao->addParameter(':hashkey', $hash_key);
            $dao->executeNonQuery();
            $id = $dao->getLastInsertedId();
            $resObj = array("Success" => "True", "Id" => $id);
            return $resObj;
        } catch (Exception $e) {
            //$dao->releaseTransaction(false);
            $this->log->logIt($this->module . ' - addAdjustment - ' . $e);
        }
    }

    public function addTotalDiscount($data, $folio_id)
    {
        try {
            $this->log->logIt($this->module . ' - addTotalDiscount');
            $dao = new \dao();
            $rec = $this->getamount($data['folioid']);
            $amount = $rec['orderAmt'];
            $discount_amt = 0;
            $current_date = \util\util::getLocalDateTime();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $year = \util\util::getYearCode();
            if ($data['select_discount'] != "" && $data['select_discount'] != 0) {
                $strSql = " SELECT shortcode,discount,postingrule,isopendiscount,value,isactive FROM " . CONFIG_DBN . ".cfdiscount WHERE hashkey=:discountunkid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->addParameter(':discountunkid', $data['select_discount']);
                $res = $dao->executeRow();
                if ($res['isopendiscount'] == 1) {
                    $val = $data['discount'];
                } else {
                    $val = $res['value'];
                }
                if ($res['postingrule'] == 1) {
                    $discount_amt = ($amount * $val) / 100;
                } else {
                    $discount_amt = $val;
                }
            }
            if ($discount_amt > 0) {
                $hash_key = \util\util::gethash();
                $ObjCommonDao = new \database\commondao();
                $discountunkid = $ObjCommonDao->getprimarykey('cfdiscount', $data['select_discount'], 'discountunkid');
                $masterunkid = $ObjCommonDao->getprimarykey('cfdiscount', $data['select_discount'], 'lnkmasterunkid');
                $strSql = "INSERT INTO " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                    " , masterunkid=:masterunkid,year_code=:year_code,quantity=:quantity,trandate=:trandate, baseamount=:baseamount" .
                    " , description=:description,lnkmappingunkid=:lnkmappingunkid,originatedfrom=0,parentid=0, taxexempt=:taxexempt, hashkey=:hashkey";
                $dao->initCommand($strSql);
                $dao->addParameter(':foliounkid', $folio_id);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->addParameter(":postinguserunkid", CONFIG_UID);
                $dao->addParameter(':masterunkid', $masterunkid);
                $dao->addParameter(':year_code', $year);
                $dao->addParameter(':baseamount', round($discount_amt, $round_off));
                $dao->addParameter(':quantity', 1);
                $dao->addParameter(':description', $data['Comment']);
                $dao->addParameter(':lnkmappingunkid', $discountunkid);
                $dao->addParameter(':trandate', $current_date);
                $dao->addParameter(':taxexempt', 0);
                $dao->addParameter(':hashkey', $hash_key);
                $dao->executeNonQuery();
            }
            return $discount_amt;

        } catch (Exception $e) {

            $this->log->logIt($this->module . ' - addTotalDiscount - ' . $e);
        }
    }

    public function getamount($folio_id)
    {
        try {
            $this->log->logIt($this->module . ' - getamount');
            $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $strSql = "SELECT IFNULL(ROUND(SUM(ffd.baseamount*fm.crdr*ffd.quantity)," . $round_off . "),0) AS orderAmt,TRC.contacttype, ffm.foliounkid, ffm.foliono ,ffm.kot_no as kotno
                       FROM " . CONFIG_DBN . ".fasfoliomaster AS ffm
                       LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS ffd ON ffd.foliounkid = ffm.foliounkid AND ffd.companyid=:companyid AND ffd.locationid=:locationid
                        LEFT JOIN ".CONFIG_DBN.".trcontact AS TRC ON TRC.contactunkid = ffm.lnkcontactid AND TRC.companyid = :companyid AND TRC.locationid = :locationid
                       LEFT JOIN " . CONFIG_DBN . ".fasmaster AS fm ON fm.masterunkid = ffd.masterunkid AND fm.companyid=:companyid AND fm.locationid=:locationid
                       WHERE ffm.hashkey=:hashkey AND ffm.companyid=:companyid AND ffm.locationid=:locationid AND ffd.isvoid=0";
            $dao->initCommand($strSql);
            $dao->addParameter(":hashkey", $folio_id);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeRow();
            return $res;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . '- getamount -' . $e);

        }
    }

    public function calculateTotalTax($item_data, $tax_data)
    {
        try {
            $this->log->logIt($this->module . ' - calculateTotalTax');
            $dao = new \dao();
            $discount_amt = $this->calculateDiscount($item_data['amount'], $item_data['discount'], $item_data['select_discount']);
            $arr_tax = array();
            $total_tax = 0;
            foreach ($tax_data as $value) {
                if ($value['postingrule'] == 1) {
                    if ($value['taxapplyafter'] == 0) {
                        $tax_amt = ($item_data['amount'] * $value['amount']) / 100;
                    } else {
                        $exempt_discount = $item_data['amount'] - $discount_amt;
                        $tax_amt = ($exempt_discount * $value['amount']) / 100;
                    }
                } else {
                    $tax_amt = $value['amount'];
                }
                $arr_tax[] = $tax_amt;
            }
            $total_tax = array_sum($arr_tax);
            return $total_tax;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - calculateTotalTax - ' . $e);
        }
    }

    public function loadorderitems($data)
    {
        try {
            $this->log->logIt($this->module . ' - loadorderitems');
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $ObjCommonDao = new \database\commondao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $folio_id = $ObjCommonDao->getprimarykey('fasfoliomaster', $data['folio'], 'foliounkid');

            $strSql = "SELECT FASFD.detailunkid,FASFD.hashkey,CASE WHEN FASM.mastertypeunkid=1 THEN 
            CFCOMBO.comboname WHEN FASM.mastertypeunkid=4 THEN 'Adjustment'
             WHEN FASM.mastertypeunkid=8 THEN CFI.itemname 
             WHEN FASM.mastertypeunkid=10 THEN CMM.modifiername 
              WHEN FASM.mastertypeunkid=9 THEN CFMI.itemname  ELSE FASM.name END AS particular" .
                " ,IFNULL(FASFD.description,'') as comment,FASFD.isvoid,ROUND((FASFD.baseamount*FASM.crdr*FASFD.quantity)," . $round_off . ") AS Baseamt,ROUND(FASFD.quantity) as quantity" .
                " ,IFNULL(DATE_FORMAT(trandate,'" . $mysqlformat . "'),'') as orderdate,FASFD.parentid,FASM.mastertypeunkid FROM " . CONFIG_DBN . ".fasfoliodetail AS FASFD" .
                " INNER JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid" .
                " AND FASM.companyid=:companyid AND FASM.locationid=:locationid LEFT JOIN " . CONFIG_DBN . ".cfmenu_items AS CFI" .
                " ON CFI.itemunkid=FASFD.lnkmappingunkid AND CFI.companyid=:companyid AND CFI.locationid=:locationid" .
                " LEFT JOIN " . CONFIG_DBN . ".cfmenu_combo AS CFCOMBO ON CFCOMBO.combounkid=FASFD.lnkmappingunkid AND CFCOMBO.companyid=:companyid AND CFCOMBO.locationid=:locationid" .
                " LEFT JOIN " . CONFIG_DBN . ".cfmenu_modifier_items AS CFMI ON CFMI.modifieritemunkid=FASFD.lnkmappingunkid AND CFMI.locationid=:locationid AND CFMI.companyid=:companyid" .
                " LEFT JOIN " . CONFIG_DBN . ".cfmenu_modifiers AS CMM ON CMM.modifierunkid=FASFD.lnkmappingunkid AND CMM.locationid=:locationid AND CMM.companyid=:companyid" .
                " WHERE FASFD.foliounkid=:folioid AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid ORDER BY FASFD.detailunkid ASC";

            $dao->initCommand($strSql);
            $dao->addParameter(':folioid', $folio_id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success" => 'True', "Cnt" => count($res), "Data" => $res)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - loadorderitems - ' . $e);
        }
    }

    public function voiditem($data,$languageArr,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - voiditem');
            $dao = new \dao();
            $current_date = \util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjInventoryDao = new\database\inventorydao();

            if ($data['items'] != "" && count($data['items']) > 0 && $data['folio'] != "") {
                $operation = 1;//Increase
                $unitratio = 100;
                $isvoid = 1;

                $folio_id = $ObjCommonDao->getprimarykey("fasfoliomaster", $data['folio'], "foliounkid");

                foreach ($data['items'] as $value) {

                    /*Remove Item Tax & modifiers*/
                    $strSql1 = "SELECT FD.detailunkid,FD.lnkmappingunkid,FD.quantity,FASM.mastertypeunkid FROM " . CONFIG_DBN . ".fasfoliodetail AS FD " .
                        " INNER JOIN " . CONFIG_DBN . ".fasmaster FASM ON FD.masterunkid=FASM.masterunkid AND FASM.companyid=:companyid WHERE FD.hashkey='" . $value . "' AND FD.companyid=:companyid AND FD.locationid=:locationid";
                    $dao->initCommand($strSql1);
                    $dao->addParameter(":companyid", CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $res = $dao->executeRow();

                    if ($res['mastertypeunkid'] == 1 || $res['mastertypeunkid'] == 8 || $res['mastertypeunkid'] == 9 || $res['mastertypeunkid'] == 10) {
                        /*Remove modifiers Tax*/

                        if($res['mastertypeunkid'] == 1){
                            $itemtype = 2;//Combo product
                            if(CONFIG_IS_STORE == 1) {
                                $ObjInventoryDao->updateItemInventory($res['lnkmappingunkid'], $res['quantity'], $unitratio, $itemtype, $operation,$folio_id,$res['detailunkid'],$isvoid);
                            }
                        }
                        //Update Whole item
                        if ($res['mastertypeunkid'] == 8) {

                            $strSql3 = "SELECT IFNULL(GROUP_CONCAT(FASFD.detailunkid),'') AS detailid FROM " . CONFIG_DBN . ".fasfoliodetail AS FASFD INNER JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid = FASFD.masterunkid WHERE FASM.mastertypeunkid IN (9,10) AND FASFD.parentid=:parentid AND FASFD.foliounkid=:foliounkid AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid";
                            $dao->initCommand($strSql3);
                            $dao->addParameter(":parentid", $res['detailunkid']);
                            $dao->addParameter(":foliounkid", $folio_id);
                            $dao->addParameter(":companyid", CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $resObj = $dao->executeRow();

                            $itemtype = 1;
                            if(CONFIG_IS_STORE == 1) {
                                $ObjInventoryDao->updateItemInventory($res['lnkmappingunkid'], $res['quantity'], $unitratio, $itemtype, $operation,$folio_id,$res['detailunkid'],$isvoid);
                            }
                            if ($resObj['detailid'] != "") {

                            //Update inventory of modifier
                                $detailIds = explode(',', $resObj['detailid']);
                                foreach ($detailIds as $detailId) {
                                    $strSql1 = "SELECT FD.lnkmappingunkid,FD.quantity,FASM.mastertypeunkid FROM " . CONFIG_DBN . ".fasfoliodetail AS FD " .
                                        " INNER JOIN " . CONFIG_DBN . ".fasmaster FASM ON FD.masterunkid=FASM.masterunkid AND FASM.companyid=:companyid WHERE FD.detailunkid='" . $detailId . "' AND FD.isvoid=0 AND FD.companyid=:companyid AND FD.locationid=:locationid";
                                    $dao->initCommand($strSql1);
                                    $dao->addParameter(":companyid", CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $resmodi = $dao->executeRow();
                                    if ($resmodi['mastertypeunkid'] == 9) {
                                        $recipe_type = 3; //Modifier item
                                        if(CONFIG_IS_STORE == 1) {
                                            $ObjInventoryDao->updateModifierInventory($resmodi['lnkmappingunkid'], $resmodi['quantity'], $recipe_type, $operation,$folio_id,$detailId,$isvoid);
                                        }
                                    }
                                    if ($resmodi['mastertypeunkid'] == 10) {

                                        $recipe_type = 2; //modifier
                                        if(CONFIG_IS_STORE == 1) {
                                            $ObjInventoryDao->updateModifierInventory($resmodi['lnkmappingunkid'], $resmodi['quantity'], $recipe_type, $operation,$folio_id,$detailId,$isvoid);
                                        }
                                    }

                                }
                            }
                            if($resObj['detailid'] != ""){ // Added (Daisy) : 15/03/2018 - check if detailid is not empty
                                $strSql4 = "UPDATE " . CONFIG_DBN . ".fasfoliodetail SET isvoid=1,voiduserunkid=:voiduserunkid,voiddatetime=:voiddatetime WHERE parentid IN (" . $resObj['detailid'] . ") AND foliounkid=:foliounkid AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql4);
                                //$this->log->logIt($strSql4);
                                $dao->addParameter(":foliounkid", $folio_id);
                                $dao->addParameter(":voiduserunkid", CONFIG_UID);
                                $dao->addParameter(":voiddatetime", $current_date);
                                $dao->addParameter(":companyid", CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $dao->executeNonQuery();
                            }

                            //
                        }

                        if ($res['mastertypeunkid'] == 9 || $res['mastertypeunkid'] == 10) {

                                $strSql1 = "SELECT FD.lnkmappingunkid,FD.quantity,FASM.mastertypeunkid FROM " . CONFIG_DBN . ".fasfoliodetail AS FD " .
                                    " INNER JOIN " . CONFIG_DBN . ".fasmaster FASM ON FD.masterunkid=FASM.masterunkid AND FASM.companyid=:companyid WHERE FD.detailunkid='" . $res['detailunkid'] . "' AND FD.isvoid=0 AND FD.companyid=:companyid AND FD.locationid=:locationid";
                                $dao->initCommand($strSql1);
                                $dao->addParameter(":companyid", CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $resmodi = $dao->executeRow();
                                if ($resmodi['mastertypeunkid'] == 9) {
                                    $recipe_type = 3; //Modifier item
                                    if(CONFIG_IS_STORE == 1) {
                                        $ObjInventoryDao->updateModifierInventory($resmodi['lnkmappingunkid'], $resmodi['quantity'], $recipe_type, $operation,$folio_id,$res['detailunkid'],$isvoid);
                                    }
                                    }
                                if ($resmodi['mastertypeunkid'] == 10) {
                                    $recipe_type = 2; //modifier
                                    if(CONFIG_IS_STORE == 1) {
                                        $ObjInventoryDao->updateModifierInventory($resmodi['lnkmappingunkid'], $resmodi['quantity'], $recipe_type, $operation,$folio_id,$res['detailunkid'],$isvoid);
                                    }
                                    }
                        }
                        $strSql2 = "UPDATE " . CONFIG_DBN . ".fasfoliodetail SET isvoid=1,voiduserunkid=:voiduserunkid,voiddatetime=:voiddatetime WHERE parentid=:id AND foliounkid=:foliounkid AND companyid=:companyid AND locationid=:locationid";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(":id", $res['detailunkid']);
                        $dao->addParameter(":foliounkid", $folio_id);
                        $dao->addParameter(":voiduserunkid", CONFIG_UID);
                        $dao->addParameter(":voiddatetime", $current_date);
                        $dao->addParameter(":companyid", CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                    }
                    /*Remove Selected Item*/
                    $strSql1 = "UPDATE " . CONFIG_DBN . ".fasfoliodetail SET isvoid=1,voiduserunkid=:voiduserunkid,voiddatetime=:voiddatetime WHERE hashkey=:id AND foliounkid=:foliounkid AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql1);
                    $dao->addParameter(":id", $value);
                    $dao->addParameter(":foliounkid", $folio_id);
                    $dao->addParameter(":voiduserunkid", CONFIG_UID);
                    $dao->addParameter(":voiddatetime", $current_date);
                    $dao->addParameter(":companyid", CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $dao->executeNonQuery();
                    /*Remove Selected Item*/
                }
                return html_entity_decode(json_encode(array("Success" => "True", "Message" => $defaultlanguageArr->REM_REC_SUC)));
            } else
                return html_entity_decode(json_encode(array("Success" => "False", "Message" => $defaultlanguageArr->INTERNAL_ERROR)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - voiditem - ' . $e);
        }
    }

    public function getuserdetails($data)
    {
        try {
            $this->log->logIt($this->module . " - getuserdetails");
            $dao = new \dao();


            $id = (isset($data['folio'])) ? $data['folio'] : "";


            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = "SELECT TRC.hashkey as contactid,foliono,isfolioclosed,lnktableunkid,
            classunkid,salutation,name,address,state,zip,city,country,phone,mobile,email" .
                " ,isfolioclosed,fax,business_name,
                IFNULL(DATE_FORMAT(dateofbirth,'" . $mysqlformat . "'),'') as birth_date" .
                " ,IFNULL(DATE_FORMAT(Aniversary_date,'" . $mysqlformat . "'),'') as annv_date" .
                " ,IFNULL(DATE_FORMAT(spouse_bdate,'" . $mysqlformat . "'),'') as spouse_bdate" .
                " FROM " . CONFIG_DBN . ".trcontact AS TRC 
             INNER JOIN " . CONFIG_DBN . ".fasfoliomaster AS FM ON TRC.contactunkid=FM.lnkcontactid AND FM.companyid=:companyid AND FM.locationid=:locationid 
                       LEFT JOIN " . CONFIG_DBN . ".cfclasstable AS cfct ON cfct.tableunkid=FM.lnktableunkid
                      WHERE FM.hashkey=:hashkey AND TRC.companyid=:companyid AND TRC.locationid=:locationid";
            $dao->initCommand($strSql);

            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->addParameter(':hashkey', $id);
            $res = $dao->executeRow();



            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getuserdetails - " . $e);
            return false;
        }
    }
    ///usr/lib/cups/filter/rastertopos58

    public function getinvoicerec($id,$template='',$languageArr='',$defaultlanguageArr='')
    {
        try {
            $this->log->logIt($this->module . ' - getinvoicerec');
            $liquor_data = '';
            if($template==4){
                $liquor_data = $this->getInvoiceDetailForLiquor($id,$template='',$languageArr='',$defaultlanguageArr='');
            }
            if($languageArr){
                $languageArr=json_decode($languageArr);
            }if($defaultlanguageArr){
                $defaultlanguageArr=json_decode($defaultlanguageArr);
            }
            $dao = new \dao();

            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $ObjCommonDao = new \database\commondao();
            $folio_id = $ObjCommonDao->getprimarykey('fasfoliomaster', $id, 'foliounkid');
            $post_amount = $this->getamount($id);
            $todaysdate = \util\util::getTodaysDateTime();
            $liquor_ids = '';
            if($liquor_data!='' && isset($liquor_data['itemarr']) && count($liquor_data['itemarr'])>0){
                $liquor_ids = array_keys($liquor_data['itemarr']);
            }

            $strSql = "SELECT IF(FASM.mastertypeunkid=8,IU.name,'') as unitname,FASM.name as Uname,FFM.ordertype,BFM.meta_value as discount,FASFD.hashkey,FASFD.detailunkid,CASE WHEN FASM.mastertypeunkid=1 THEN CFCOMBO.comboname WHEN FASM.mastertypeunkid=8 THEN CFI.itemname  WHEN FASM.mastertypeunkid=9 THEN CFMI.itemname WHEN FASM.mastertypeunkid=10 THEN CMM.modifiername ELSE FASM.name END AS particular" .
                " ,IFNULL(FASFD.description,'') as comment,FASFD.isvoid,ROUND((FASFD.baseamount*FASM.crdr*FASFD.quantity)," . $round_off . ") AS Baseamt,ROUND(FASFD.quantity) as quantity" .
                " ,IFNULL(DATE_FORMAT(trandate,'" . $mysqlformat . "'),'') as orderdate,FASFD.parentid,FASM.mastertypeunkid,FASFD.masterunkid,IF(FASM.mastertypeunkid=3 AND CFD.category_id!='',1,0) as is_categoty_discount FROM " . CONFIG_DBN . ".fasfoliodetail AS FASFD" .
                " INNER JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid" .
                " AND FASM.companyid=:companyid AND FASM.locationid=:locationid LEFT JOIN " . CONFIG_DBN . ".cfmenu_items AS CFI" .
                " ON CFI.itemunkid=FASFD.lnkmappingunkid AND CFI.companyid=:companyid AND CFI.locationid=:locationid" .
                " LEFT JOIN " . CONFIG_DBN . ".cfmenu_combo AS CFCOMBO ON CFCOMBO.combounkid=FASFD.lnkmappingunkid AND CFCOMBO.companyid=:companyid AND CFCOMBO.locationid=:locationid" .
                " LEFT JOIN " . CONFIG_DBN . ".cfmenu_modifier_items AS CFMI ON CFMI.modifieritemunkid=FASFD.lnkmappingunkid AND CFMI.companyid=:companyid AND CFMI.locationid=:locationid" .
                " LEFT JOIN " . CONFIG_DBN . ".cfmenu_modifiers AS CMM ON CMM.modifierunkid=FASFD.lnkmappingunkid AND CMM.locationid=:locationid AND CMM.companyid=:companyid" .
                " LEFT JOIN " . CONFIG_DBN . ".bmfoliodetail_meta AS BFM ON BFM.lnkfoliounkid=FASFD.foliounkid AND BFM.lnkdetailunkid=FASFD.detailunkid AND BFM.meta_key like 'discount' AND BFM.companyid=:companyid AND BFM.locationid=:locationid" .
                " LEFT JOIN " . CONFIG_DBN . ".fasfoliomaster AS FFM ON FFM.foliounkid=FASFD.foliounkid AND FFM.companyid=:companyid AND FFM.locationid=:locationid".
                " LEFT JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS IU ON FASFD.lnkunitid=IU.unitunkid AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid".
                " LEFT JOIN " . CONFIG_DBN . ".cfdiscount AS CFD ON CFD.lnkmasterunkid=FASM.masterunkid AND CFD.companyid=:companyid AND CFD.locationid=:locationid".
                " WHERE FASFD.foliounkid=:folioid AND FASFD.isvoid=0 AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid ";
            if($liquor_ids!=''){
                $strSql .= " AND FASFD.detailunkid NOT IN (".implode(',',$liquor_ids).") AND FASFD.parentid NOT IN (".implode(',',$liquor_ids).") ";
            }
            $strSql .= " ORDER BY FASFD.detailunkid ASC";
            $dao->initCommand($strSql);

            $dao->addParameter(':folioid', $folio_id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeQuery();

            $resarr = array();
            $retArr = array();
            $tracarr = array();
            //$cnt = 0;
            $gratuity = $loyalty =$tender=$adjustment = $subamt = $amt = $tax = $disc = $pytype =$price = $category_discount = 0;
            foreach ($res AS $val) {
                if ($val['parentid'] == 0 && ($val['mastertypeunkid'] == 1 || $val['mastertypeunkid'] == 8)) {
                    //$cnt++;
                    $retArr[$val['detailunkid']]['item'] = $val;
                    $tracarr[$val['detailunkid']] = $ObjCommonDao->getprimarykey('fasfoliodetail', $val['hashkey'], 'detailunkid');
                    $retArr[$val['detailunkid']]['item']['total'] = $val['Baseamt'];
                }
                if(isset($tracarr[$val['parentid']])){
                    if ($val['parentid'] == $tracarr[$val['parentid']] && ($val['mastertypeunkid'] == 9 || $val['mastertypeunkid'] == 10)) {
                        $retArr[$val['parentid']]['modifier'][] = $val;
                        $retArr[$val['parentid']]['item']['total'] = $retArr[$val['parentid']]['item']['total'] + $val['Baseamt'];
                    }
                    if ($val['parentid'] == $tracarr[$val['parentid']] && $val['mastertypeunkid'] == 3) {
                        $retArr[$val['parentid']]['discount'][] = $val;
                    }
                }

                if ($val['mastertypeunkid'] == 4)
                    $adjustment = $adjustment + $val['Baseamt'];
                if ($val['mastertypeunkid'] == 11)
                    $gratuity = $gratuity + $val['Baseamt'];
                if ($val['mastertypeunkid'] == 13)
                    $loyalty= $loyalty + $val['Baseamt'];
                if ($val['mastertypeunkid'] == 12)
                    $tender = $tender + $val['Baseamt'];
                if (isset($val['is_categoty_discount']) && $val['is_categoty_discount'] == 1) {
                    if ($val['ordertype'] == 2) {
                        $category_discount = $category_discount + (-$val['discount']);
                    } else {
                        $category_discount = $category_discount + $val['Baseamt'];
                    }
                }
                if ($val['mastertypeunkid'] == 3 && $val['ordertype']!=2)
                    $disc = $disc + $val['Baseamt'];
                if ($val['ordertype'] ==2)
                    $disc = $disc + (-$val['discount']);
                if ($val['mastertypeunkid'] == 8 || $val['mastertypeunkid'] == 9 || $val['mastertypeunkid'] == 10 || $val['mastertypeunkid'] == 1)
                    $amt = $amt + $val['Baseamt'];
                if ($val['mastertypeunkid'] == 2)
                    $tax = $tax + $val['Baseamt'];

            }
            $resarr['itemarr'] = $retArr;
            $resarr['amt'] = $amt;
            $resarr['disc'] = $disc;
            $resarr['category_disc'] = $category_discount;
            $resarr['tax'] = $tax;
            $resarr['adjustment'] = $adjustment;
            $resarr['gratuity'] = $gratuity;
            $resarr['loyalty'] = $loyalty;
            $resarr['tender'] = $tender;
            $resarr['pytype'] = $pytype;
            $resarr['paymentytpe'] = isset($val['Uname'])?$val['Uname']:'';

            // $p = number_format($price,$round_off);
            //  $resarr['total'] = number_format($disc + $amt + $tax + $adjustment, $round_off);

            /*Get Applied Tax Name */
            if(isset($res) && count($res)>0) {
                /*Get posted payments Name */
                if (isset($liquor_data['itemarr']) && count($liquor_data['itemarr']) > 0) {
                    $strSql1 = "SELECT FASFD.detailunkid,FASFD.parentid,ctx.is_gst, FASM.name AS applied_tax,SUM((FASFD.baseamount*FASM.crdr*FASFD.quantity)) AS amt" .
                        " FROM " . CONFIG_DBN . ".fasfoliodetail AS FASFD" .
                        " INNER JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid" .
                        " AND FASM.companyid=:companyid AND FASM.locationid=:locationid" .
                        " LEFT JOIN " . CONFIG_DBN . ".cftax AS ctx ON ctx.lnkmasterunkid=FASM.masterunkid AND ctx.companyid=:companyid AND ctx.locationid=:locationid " .
                        " WHERE FASFD.foliounkid=:folioid AND FASM.mastertypeunkid=2 AND FASFD.isvoid=0 AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid";
                    if ($liquor_ids != '') {
                        $strSql1 .= " AND FASFD.parentid NOT IN (" . implode(',', $liquor_ids) . ") ";
                    }
                    $strSql1 .= " GROUP BY ctx.lnkmasterunkid ORDER BY ctx.lnkmasterunkid DESC";
                    $dao->initCommand($strSql1);
                    $dao->addParameter(':folioid', $folio_id);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $res1 = $dao->executeQuery();
                    if (count($res1) == 0) {
                        $totaltax = 1;
                    } else {
                        $totaltax = count($res1);
                    }

                    $liquorstrSql1 = "SELECT FASFD.detailunkid,FASFD.parentid,ctx.is_gst, FASM.name AS applied_tax,SUM((FASFD.baseamount*FASM.crdr*FASFD.quantity)) AS amt" .
                        " FROM " . CONFIG_DBN . ".fasfoliodetail AS FASFD" .
                        " INNER JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid" .
                        " AND FASM.companyid=:companyid AND FASM.locationid=:locationid" .
                        " LEFT JOIN " . CONFIG_DBN . ".cftax AS ctx ON ctx.lnkmasterunkid=FASM.masterunkid AND ctx.companyid=:companyid AND ctx.locationid=:locationid " .
                        " WHERE FASFD.foliounkid=:folioid AND FASM.mastertypeunkid=2 AND FASFD.isvoid=0 AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid";
                    if ($liquor_ids != '') {
                        $liquorstrSql1 .= " AND FASFD.parentid IN (" . implode(',', $liquor_ids) . ") ";
                    }
                    $liquorstrSql1 .= " GROUP BY ctx.lnkmasterunkid ORDER BY ctx.lnkmasterunkid DESC";
                    $dao->initCommand($liquorstrSql1);
                    $dao->addParameter(':folioid', $folio_id);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $liquorres1 = $dao->executeQuery();


                } else {
                    $strSql1 = "SELECT ctx.is_gst, FASM.name AS applied_tax,SUM((FASFD.baseamount*FASM.crdr*FASFD.quantity)) AS amt" .
                        " FROM " . CONFIG_DBN . ".fasfoliodetail AS FASFD" .
                        " INNER JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid" .
                        " AND FASM.companyid=:companyid AND FASM.locationid=:locationid" .
                        " LEFT JOIN " . CONFIG_DBN . ".cftax AS ctx ON ctx.lnkmasterunkid=FASM.masterunkid AND ctx.companyid=:companyid AND ctx.locationid=:locationid " .
                        " WHERE FASFD.foliounkid=:folioid AND FASM.mastertypeunkid=2 AND FASFD.isvoid=0 AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid";
                    $strSql1 .= " GROUP BY ctx.lnkmasterunkid ORDER BY ctx.lnkmasterunkid DESC";

                    $dao->initCommand($strSql1);

                    $dao->addParameter(':folioid', $folio_id);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $res1 = $dao->executeQuery();
                    if (count($res1) == 0) {
                        $totaltax = 1;
                    } else {
                        $totaltax = count($res1);
                    }
                }
            }

            /*Get posted payments Name */
            $str6 = "SELECT FASM.name,SUM(FASFD.baseamount) AS amt FROM " . CONFIG_DBN . ".fasfoliodetail AS FASFD 
                      LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid
                      AND FASM.companyid=:companyid AND FASM.locationid=:locationid WHERE 
                      FASFD.foliounkid=:folioid AND FASM.mastertypeunkid=7
                      AND FASFD.isvoid=0 AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid GROUP BY FASM.masterunkid ";
            $dao->initCommand($str6);
            $dao->addParameter(':folioid', $folio_id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $resPayment = $dao->executeQuery();
            foreach ($resPayment AS $key => $value) {
                $price += abs($value['amt']);
            }

            // 22/02/2018 - Add default count for tax applied , if any tax is not applied on order

            $resarr['tax_applied'] = isset($res1)?$res1:'';
            $taxvalue = $resarr['tax'] / isset($totaltax)?$totaltax:1;

            $resarr['txvl'] = $taxvalue;
            $resarr['rfrmt'] = $round_off;


            /*Get Applied Tax Name */
            /*$strSql = " SELECT CFC.companyname, CONCAT(CFC.address1,',',CFC.address2,',',CFC.city,',',CFC.state) AS address, countrylist.countryName, CFC.currency_sign, CFC.phone, CFC.fax, CFC.website, CFC.logo FROM ".CONFIG_DBN.".cfcompany AS CFC ".
                        " INNER JOIN ".CONFIG_DBN.".countrylist ON countrylist.id = CFC.country WHERE CFC.companyid=:companyid";
           */
            $strSql = " SELECT companyname,currency_sign,IFNULL(logo,'') as logo,registration_no1 FROM " . CONFIG_DBN . ".cfcompany WHERE companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $rec1 = $dao->executeRow();

            $strSql2 = " SELECT CFL.locationname, CONCAT(CFL.address1,',',CFL.address2,',',CFL.city,',',CFL.state) AS address, 
                     VWC.countryName, CFL.currency_sign, CFL.phone, CFL.fax, 
                     CFL.website FROM " . CONFIG_DBN . ".cflocation AS CFL
                     INNER JOIN " . CONFIG_DBN . ".vwcountry VWC ON VWC.id = CFL.country WHERE CFL.companyid=:companyid AND CFL.locationid=:locationid";
            $dao->initCommand($strSql2);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $rec2 = $dao->executeRow();
            $resarr['companyinfo'] = $rec1;
            $resarr['locationinfo'] = $rec2;

            $strSql = " SELECT " .
                " FM.foliono,FM.ispostedtopms,IFNULL(DATE_FORMAT(FM.opendate,'" . $mysqlformat . "'),'') AS invoice_date," .
                " TRC.name ,IFNULL(FM.locationmetalnkid,0) AS locationmetalnkid,IFNULL(FM.no_of_person,'') AS no_of_person 
                  FROM " . CONFIG_DBN . ".fasfoliomaster AS FM " .
                " LEFT JOIN " . CONFIG_DBN . ".trcontact AS TRC ON TRC.contactunkid = FM.lnkcontactid " .

                " WHERE FM.foliounkid=:folioid AND FM.companyid=:companyid AND FM.locationid=:locationid";

            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->addParameter(':folioid', $folio_id);
            $rec3 = $dao->executeRow();
            $resarr['folioinfo'] = $rec3;

            if ($rec3['ispostedtopms'] == '1') {
                $resarr['post_amount'] = $post_amount['orderAmt'];
            } else {
                $resarr['post_amount'] = 0;
            }
	    
	    $total_disc = $disc;

            if($category_discount==0){
                $total_disc = 0;
                $resarr['category_disc'] = 0;
            }
	    
            $resarr['order_total'] = $total_disc + $amt + $tax + $loyalty;


            $resarr['total'] = $total_disc + $amt + $tax + $adjustment - $price + $loyalty + $gratuity + $tender - $resarr['post_amount'];
            $resarr['paymnttype'] = $resPayment;

            $strSql = "SELECT logo as company_logo FROM " . CONFIG_DBN . ".cfcompany WHERE companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $res = $dao->executeRow();
            $resarr['imgurl'] = CONFIG_BASE_URL . "companyadmin/assets/company_logo/";
            $resarr['company_logo'] = $res['company_logo'];
            $powered_by = '';

            $powered_by = CONFIG_POWERED_BY;
            if ($powered_by == '')
                $powered_by = 'Pure ITES';
            else
                $powered_by = CONFIG_POWERED_BY;

            $resarr['powered_by'] = $powered_by;
            if ($template == '3') {
                $strSql2 = "SELECT meta_value FROM " . CONFIG_DBN . ". cflocationtemplate_meta  WHERE
                            companyid=:companyid AND locationid=:locationid AND meta_key=:meta_key";
                if(isset($rec3) && $rec3['locationmetalnkid']!=0){
                    $strSql2.=" AND locationmetaunkid=:locationmetaunkid";
                }
                $strSql2 .= " ORDER BY locationmetaunkid DESC";
                $dao->initCommand($strSql2);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $dao->addParameter(':meta_key', "template".$template);
                if(isset($rec3) && $rec3['locationmetalnkid']!=0){
                $dao->addParameter(':locationmetaunkid', isset($rec3)?$rec3['locationmetalnkid']:0);
                }
                $template_detail= $dao->executeRow();
                if(isset($template_detail['meta_value']))
                {
                    $template_detail=(array)json_decode($template_detail['meta_value']);
                }
                $resarr['billing_station'] = CONFIG_UID;
                $resarr['user_name'] = CONFIG_UNM;
                $resarr['todays_date'] = $todaysdate;
                if($template_detail){
                    $resarr['NIT'] = $template_detail['nit_number'];
                    $reg_detail=$languageArr->LANG3." ".$languageArr->LANG18." ".
                        $languageArr->LANG37." ".$template_detail['dian_num']. " ".$defaultlanguageArr->OF.
                        " ".$template_detail['appliesfrom']." ".$languageArr->LANG38." ". $defaultlanguageArr->FROM. " ".$template_detail['main_start_no']." ";

                    if(isset($template_detail['endno']) && $template_detail['endno']!=''){
                        $reg_detail .=$defaultlanguageArr->TO." ".$template_detail['endno'];
                    }
                    $resarr['Registration_detail'] =$reg_detail;
                    $resarr['welcome_msg'] = $template_detail['welcomemsg'];
                    $resarr['note'] = $template_detail['note'];
                }

            }
            $resarr['grand_total'] = $resarr['order_total'];
            if (isset($liquor_data['itemarr']) && count($liquor_data['itemarr']) > 0) {
                $resarr['liquor_info'] = $liquor_data;
                $resarr['liquor_info']['tax_applied'] = isset($liquorres1) ? $liquorres1 : '';
                $tax_amount = array_sum(array_column($resarr['liquor_info']['tax_applied'], 'amt'));
                $resarr['liquor_info']['order_total'] = $resarr['liquor_info']['order_total'] + (($tax_amount != '') ? $tax_amount : 0);
                $resarr['liquor_info']['total'] = $resarr['liquor_info']['total'] + (($tax_amount != '') ? $tax_amount : 0);
                $resarr['grand_total'] = $resarr['grand_total'] + $resarr['liquor_info']['order_total'];
            }
            if($category_discount==0){
                $resarr['grand_total'] = $resarr['grand_total'] + $resarr['disc'];
            }
            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getinvoicerec - ' . $e);
        }

    }


    public function closefolio($data,$languageArr,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - closefolio');
            $dao = new \dao();
            $ObjAuditDao = new \database\orderlogdao();
            $datetime = \util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $rec = $this->getamount($data['id']);
            $amount = $rec['orderAmt'];
            if($amount==0){
                $strSql = "UPDATE ".CONFIG_DBN.".fasfoliomaster SET foliostatus=2,isfolioclosed=1,
                        closeuserunkid=:closeuserunkid,
                        folioclosedatetime=:folioclosedatetime 
                        WHERE hashkey=:id AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id", $data['id']);
                $dao->addParameter(':closeuserunkid', CONFIG_UID);
                $dao->addParameter(':folioclosedatetime', $datetime);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                $ObjAuditDao->addOrderLog($data['id'], 'CLOSE_FOLIO', $data);

                $folio_id = $ObjCommonDao->getprimarykey("fasfoliomaster", $data['id'], "foliounkid");

               /* $str = "UPDATE ".CONFIG_DBN.".cftablesession  SET isreleased=:isreleased,
                                                                  releasedate=:releasedate                                                              
                                                                WHERE lnkfolioid=:lnkfolioid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($str);
                $dao->addParameter(':isreleased', 1);
                $dao->addParameter(':releasedate', $datetime);
                $dao->addParameter(':lnkfolioid', $folio_id);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();*/
                return html_entity_decode(json_encode(array("Success" => "True", "Message" => $languageArr->LANG57)));
            }else{
                return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG59)));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - closefolio - ' . $e);
        }
    }

    public function closefolioOnPostCharge($data,$languageArr,$invoice)
    {
        try {
            $this->log->logIt($this->module . ' - closefolioOnPost');
            $dao = new \dao();
            $ObjAuditDao = new \database\orderlogdao();
            $ObjCommonDao =   new \database\commondao();
            $datetime = \util\util::getLocalDateTime();

            $strSql2 = "SELECT TRC.contacttype,IFNULL(fasfoliomaster.foliono,'') as foliono FROM " . CONFIG_DBN . ". fasfoliomaster LEFT JOIN " . CONFIG_DBN . ".trcontact AS TRC ON TRC.contactunkid = fasfoliomaster.lnkcontactid  WHERE
                            fasfoliomaster.companyid=:companyid AND fasfoliomaster.locationid=:locationid AND fasfoliomaster.foliounkid=:foliounkid ";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->addParameter(":foliounkid", $data['folioid']);
            $folioget= $dao->executeRow();


            $strSql = "UPDATE ".CONFIG_DBN.".fasfoliomaster SET ispostedtopms=1,foliostatus=2,isfolioclosed=1,";
            $genrate_invoice_nocharge = \database\parameter::getParameter('genrate_invoice_for_nochargeuser');
            if(isset($folioget) && $folioget['foliono']==''){

                if($folioget['contacttype'] == 4 && $genrate_invoice_nocharge == 0){
                    GOTO outinvoice;
                }else{

                    $prefixval=isset($invoice['prefix'])?$invoice['prefix']:'';
                    $startno=isset($invoice['startno'])?$invoice['startno']:'';
                    $inv_no = isset($invoice['invoice_no'])?$invoice['invoice_no']:'';

                $strSql.="foliono=:foliono,prefix=:prefix,real_no=:real_no,old_no=:real_no,";
                }
            }
            outinvoice:
            $strSql.="closeuserunkid=:closeuserunkid,folioclosedatetime=:folioclosedatetime WHERE foliounkid=:folioid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(":folioid", $data['folioid']);
            $dao->addParameter(':closeuserunkid', CONFIG_UID);
            $dao->addParameter(':folioclosedatetime', $datetime);

            if(isset($folioget) && $folioget['foliono']==''){
                if($folioget['contacttype'] == 4 && $genrate_invoice_nocharge == 0){
                    GOTO outdao;
                }else{
                $dao->addparameter(':foliono', isset($inv_no)?$inv_no:'');
                $dao->addparameter(':prefix', isset($prefixval)?$prefixval:'');
                $dao->addparameter(':real_no', isset($startno)?$startno:'');
                }

            }
            outdao:

            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->executeNonQuery();

            $hash =$ObjCommonDao->getFieldValue('fasfoliomaster','hashkey','foliounkid',$data['folioid']);

            $ObjAuditDao->addOrderLog($data['folioid'], 'CLOSE_FOLIO', $data);

            /*$str = "UPDATE ".CONFIG_DBN.".cftablesession  SET isreleased=:isreleased,
                                                              releasedate=:releasedate                                                              
                                                            WHERE lnkfolioid=:lnkfolioid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($str);
            $dao->addParameter(':isreleased', 1);
            $dao->addParameter(':releasedate', $datetime);
            $dao->addParameter(':lnkfolioid', $data['folioid']);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->executeNonQuery();*/
            return html_entity_decode(json_encode(array("Success" => "True", "Message" => $languageArr->LANG60)));

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - closefolioOnPost - ' . $e);
        }
    }

    public function searchGuest($data)
    {
        try {
            $this->log->logIt($this->module . ' - searchGuest -');
            $dao = new \dao();
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];

            $strSql = "SELECT hashkey,salutation,name,address,city,state,zip,mobile,phone" .
                " ,email,fax,business_name,IFNULL(DATE_FORMAT(dateofbirth,'" . $mysql_format . "'),'') as birth_date" .
                " ,IFNULL(DATE_FORMAT(Aniversary_date,'" . $mysql_format . "'),'') as annv_date" .
                " ,IFNULL(DATE_FORMAT(spouse_bdate,'" . $mysql_format . "'),'') as spouse_bdate" .
                " FROM " . CONFIG_DBN . ".trcontact where companyid=:companyid AND locationid=:locationid and contacttype=:contacttype";
            if (isset($data['name']) && $data['name'] != "") {
                $strSql .= " and name LIKE '%" . $data['name'] . "%'";
            }
            if (isset($data['mobile']) && $data['mobile'] != "") {
                $strSql .= " and mobile LIKE '%" . $data['mobile'] . "%'";
            }
            if (isset($data['name']) && isset($data['mobile']) && $data['name'] != "" && $data['mobile'] != "") {
                $strSql .= " and name LIKE '%" . $data['name'] . "%' and mobile LIKE '%" . $data['mobile'] . "%'";
            }
            if (isset($data['contactid']) && $data['contactid'] != "" && $data['contactid'] != 0) {
                $strSql .= " and hashkey='" . $data['contactid'] . "'";
            }
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->addParameter(":contacttype", 1);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success" => "True", "Cnt" => count($res), "Data" => $res)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - searchGuest - ' . $e);
        }
    }

    public function removefolio($data,$languageArr)
    {
        try {
            $this->log->logIt($this->module . ' - removefolio');
            $dao = new \dao();
            $current_date = \util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            if ($data['id'] != "") {
                $folio_id = $ObjCommonDao->getprimarykey("fasfoliomaster", $data['id'], "foliounkid");
                /*Delete Order*/
                $strSql1 = "UPDATE " . CONFIG_DBN . ".fasfoliomaster SET isvoid=1,voiduserunkid=:voiduserunkid,voiddatetime=:voiddatetime WHERE hashkey=:id AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql1);
                $dao->addParameter(":id", $data['id']);
                $dao->addParameter(":voiduserunkid", CONFIG_UID);
                $dao->addParameter(":voiddatetime", $current_date);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                /*Delete Order*/
                /*Delete order items*/
                $strSql1 = "UPDATE " . CONFIG_DBN . ".fasfoliodetail SET isvoid=1,voiduserunkid=:voiduserunkid,voiddatetime=:voiddatetime WHERE foliounkid=:foliounkid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql1);
                $dao->addParameter(":foliounkid", $folio_id);
                $dao->addParameter(":voiduserunkid", CONFIG_UID);
                $dao->addParameter(":voiddatetime", $current_date);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                /*Delete order items*/
            }
            return html_entity_decode(json_encode(array("Success" => "True", "Message" => $languageArr->LANG61)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - removefolio - ' . $e);
        }
    }

    public function takedataforreservation($locationid = '')
    {
        try {
            if ($locationid == "") {
                $locationid= CONFIG_LID;
            }
            $dao = new \dao();
            $strSql = "SELECT companyid,pms_username,pms_password FROM " . CONFIG_DBN . ".cflocation
                           WHERE locationid = " . $locationid . " ";
            $dao->initCommand($strSql);
            $data = $dao->executeRow();
            return $data;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . '-takedataforreservation -' . $e);
        }
    }

    public function updatesplitorder($data,$languageArr,$defaultlanguageArr)
    {
        try {

            $this->log->logIt($this->module . '-updatesplitorder');
            $dao = new \dao();
            $localdatetime = \util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\orderlogdao();
            $year = \util\util::getYearCode();
            $invno = "";
            $invno = $this->getInvoiceNumber('inc');

            $str = "SELECT foliono FROM " . CONFIG_DBN . ".fasfoliomaster WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid ";
            $dao->initCommand($str);
            $dao->addParameter(':hashkey', $data['chrgehash']);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $folio = $dao->executeRow();

            if ($data['ordercharge'] != '' && count($data['ordercharge']) > 0) {
                // $id = explode(',',$data['ordercharge']);

                $hashkey = \util\util::gethash();
                $strSqlinsrt = "INSERT INTO " . CONFIG_DBN . ".trcontact SET salutation=:salutation, name=:name" .
                    " , business_name=:business_name, address=:address, state=:state, city=:city" .
                    " , zip=:zip, country=:country, phone=:phone, contacttype=:contacttype" .
                    " , mobile=:mobile, email=:email, fax=:fax";

                if ($data['info_birth_date'] != "")
                    $strSqlinsrt .= " ,dateofbirth='" . \util\util::convertDateToMySql($data['info_birth_date']) . "'";
                if ($data['info_weddinganniversary'] != "")
                    $strSqlinsrt .= " ,Aniversary_date='" . \util\util::convertDateToMySql($data['info_weddinganniversary']) . "'";
                if ($data['info_spousebirthdate'] != "")
                    $strSqlinsrt .= " ,spouse_bdate='" . \util\util::convertDateToMySql($data['info_spousebirthdate']) . "'";

                $strSqlinsrt .= "  ,hashkey=:hashkey" .
                    " , is_foliocreated=:is_foliocreated, companyid=:companyid,locationid=:locationid";
                $dao->initCommand($strSqlinsrt);
                $dao->addParameter(':salutation', $data['salutation']);
                $dao->addParameter(':name', $data['info_name']);
                $dao->addParameter(':business_name', $data['info_company']);
                $dao->addParameter(':address', $data['info_address']);
                $dao->addParameter(':state', $data['info_state']);
                $dao->addParameter(':city', $data['info_city']);
                $dao->addParameter(':zip', $data['info_zip']);
                $dao->addParameter(':country', $data['info_select_country']);
                $dao->addParameter(':contacttype', 1);
                $dao->addParameter(':phone', $data['info_phone']);
                $dao->addParameter(':mobile', $data['info_mobile']);
                $dao->addParameter(':email', $data['info_email']);
                $dao->addParameter(':fax', $data['info_fax']);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':is_foliocreated', '1');
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                $id = $dao->getLastInsertedId();

                $hashkey1 = \util\util::gethash();
                $strFolio = "INSERT INTO " . CONFIG_DBN . ".fasfoliomaster SET companyid=:companyid,locationid=:locationid,year_code=:year_code, lnkcontactid=:lnkcontactid, " .
                    " opendate=:opendate, foliotypeunkid=:foliotypeunkid, docno=:docno, foliono=:foliono, hashkey=:hashkey, folioopenuserunkid=:folioopenuserunkid";
                $dao->initCommand($strFolio);
                $dao->addParameter(':year_code', $year);
                $dao->addParameter(':lnkcontactid', $id);
                $dao->addParameter(':opendate', $localdatetime);
                $dao->addParameter(':foliotypeunkid', 1);
                $dao->addParameter(':docno', '');
                $dao->addParameter(':foliono', $invno);
                $dao->addParameter(':hashkey', $hashkey1);
                $dao->addParameter(':folioopenuserunkid', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                $fid = $dao->getLastInsertedId();

                $new = explode(",", $data['ordercharge']);

                foreach ($new AS $value) {
                    $itemid = $ObjCommonDao->getprimarykey('fasfoliodetail', $value, 'detailunkid');

                    $str = "UPDATE " . CONFIG_DBN . ".fasfoliodetail as fd SET fd.foliounkid=" . $fid . ",
                                                                                       fd.description=:description
                            WHERE fd.detailunkid =" . $itemid . "
                           AND fd.companyid=" . CONFIG_CID . " AND fd.locationid=" . CONFIG_LID . " ";

                    $dao->initCommand($str);
                    $dao->addParameter(':description', "Split from " . $folio['foliono']);
                    $dao->executeNonQuery();

                    $strSql = "SELECT IFNULL(GROUP_CONCAT(FASFD.detailunkid),'') AS detailid FROM " . CONFIG_DBN . ".fasfoliodetail AS FASFD
                             WHERE  FASFD.parentid=:parentid  AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':parentid', $itemid);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $Obj = $dao->executeRow();

                    if ($Obj['detailid'] != "") {
                        $strSql2 = "UPDATE " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=" . $fid . ",
                                                                                     description=:description
                                 WHERE detailunkid IN (" . $Obj['detailid'] . ")  AND companyid=:companyid AND locationid=:locationid";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(':description', "Split from " . $folio['foliono']);
                        $dao->addParameter(":companyid", CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                    }

                    $str = "SELECT IFNULL(GROUP_CONCAT(FASFD.detailunkid),'') AS detailid FROM " . CONFIG_DBN . ".fasfoliodetail as FASFD
                        INNER JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid = FASFD.masterunkid WHERE parentid=:parentid AND FASM.mastertypeunkid=9 AND FASFD.foliounkid=:foliounkid AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid";


                    $dao->initCommand($str);
                    $dao->addParameter(':parentid', $itemid);
                    $dao->addParameter(':foliounkid', $fid);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':locationid', CONFIG_LID);
                    $objdata = $dao->executeRow();


                    if ($objdata['detailid'] != "") {
                        $strSql2 = "UPDATE " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid,
                                                                                     description=:description
                                 WHERE parentid IN (" . $objdata['detailid'] . ")  AND companyid=:companyid AND locationid=:locationid";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(':foliounkid', $fid);
                        $dao->addParameter(':description', "Split from " . $folio['foliono']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                    }


                }
                $ObjAuditDao->addOrderLog($fid, 'SPIT_INVOICE', $data, $invno);
                return html_entity_decode(json_encode(array("Success" => "True", "Message" => $languageArr->LANG62)));
            }
        } catch (\Exception $e) {
            $this->log->logIt($this->module . '- updatesplitorder -' . $e);
        }
    }

    public function getlistofguest($data)
    {
        try {
            $this->log->logIt($this->module . ' - getlistofguest -');
            $dao = new \dao();

            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $date_format = \database\parameter::getParameter('dateformat');
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $strSql = "SELECT IFNULL(ROUND(SUM(FD.baseamount*FASM.crdr*FD.quantity)," . $round_off . "),0) AS orderAmt,IFNULL(DATE_FORMAT(FM.opendate,'" . $mysqlformat . "'),'') AS orderDate,
                FM.foliono,FM.foliounkid,FD.hashkey AS hkey, FASM.crdr,FD.quantity,FD.baseamount, FM.hashkey as fm_hashkey,
                FM.foliono, FM.foliounkid,TCT.contactunkid, TCT.hashkey,TCT.salutation,TCT.name,TCT.address,TCT.city,TCT.state,
                TCT.zip,TCT.mobile,IFNULL(TCT.phone,'')as phone,IFNULL(TCT.email,'') as email,TCT.fax,TCT.business_name" .
                " FROM " . CONFIG_DBN . ".trcontact AS TCT " .
                "INNER JOIN " . CONFIG_DBN . ".fasfoliomaster AS FM ON TCT.contactunkid=FM.lnkcontactid and TCT.companyid=FM.companyid AND FM.isvoid=0 AND FM.locationid=TCT.locationid" .
                " LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail FD ON FM.foliounkid=FD.foliounkid AND FM.companyid=FD.companyid and FD.isvoid=0 AND FD.locationid=FM.locationid" .
                " LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid = FD.masterunkid AND FASM.companyid=:companyid AND FASM.locationid=FD.locationid" .
                " WHERE TCT.companyid=:companyid AND TCT.locationid=:locationid and TCT.contacttype=:contacttype";

            if (isset($data['name']) && $data['name'] != "") {
                $strSql .= " and TCT.name LIKE '%" . $data['name'] . "%'";
            }
            if (isset($data['email']) && $data['email'] != "") {
                $strSql .= " and TCT.email LIKE '%" . $data['email'] . "%'";
            }
            if (isset($data['mobile']) && $data['mobile'] != "") {
                $strSql .= " and TCT.mobile LIKE '%" . $data['mobile'] . "%'";
            }
            if (isset($data['foliono']) && $data['foliono'] != "") {
                $strSql .= " and FM.foliono LIKE '%" . $data['foliono'] . "%'";
            }
            if (isset($data['name']) && isset($data['mobile']) && isset($data['foliono']) && $data['name'] != "" && $data['mobile'] != "" && $data['foliono'] != "") {
                $strSql .= " and TCT.name LIKE '%" . $data['name'] . "%' and TCT.mobile LIKE '%" . $data['mobile'] . "%' and FM.foliono LIKE '%.";
            }
            if (isset($data['contactid']) && $data['contactid'] != "" && $data['contactid'] != 0) {
                $strSql .= " and hashkey='" . $data['contactid'] . "'";
            }
            $strSql .= " GROUP BY FM.foliounkid ORDER BY FM.foliounkid DESC";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->addParameter(":contacttype", 1);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success" => "True", "Cnt" => count($res), "Data" => $res)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getlistofguest' . $e);
        }
    }

    public function invoicebysearch($data,$languageArr,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . '-invoicebysearch');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\orderlogdao();

            if ($data['srcfolio'] != '' && $data['destfolio'] != '' && $data['fcharge'] != '') {
                $str = "SELECT ffm.foliounkid,ffm.foliono FROM " . CONFIG_DBN . ".fasfoliomaster AS ffm 
                    WHERE ffm.hashkey=:hashkey AND ffm.companyid=:companyid AND ffm.locationid=:locationid";

                $dao->initCommand($str);
                $dao->addParameter(':hashkey', $data['destfolio']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $res = $dao->executeRow();

                $detail = $data['fcharge'];

                $str = "SELECT foliono,foliounkid,lnkcontactid FROM " . CONFIG_DBN . ".fasfoliomaster 
                    WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($str);
                $dao->addParameter(':hashkey', $data['srcfolio']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $srcfolio = $dao->executeRow();

                $ObjAuditDao->addOrderLog($srcfolio['foliounkid'], 'SPLIT_TO', $res['foliono']);


                foreach ($detail AS $value) {
                    $item = $ObjCommonDao->getprimarykey('fasfoliodetail', $value, 'detailunkid');

                    $str = " UPDATE " . CONFIG_DBN . ".fasfoliodetail AS ffd  SET ffd.foliounkid=:foliounkid,
                                                                                 ffd.description=:description
                                WHERE ffd.detailunkid=:detailunkid AND ffd.companyid=:companyid AND ffd.locationid=:locationid";

                    $dao->initCommand($str);
                    $dao->addParameter(':foliounkid', $res['foliounkid']);
                    $dao->addParameter(':detailunkid', $item);
                    $dao->addParameter(':description', "Split from " . $srcfolio['foliono']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':locationid', CONFIG_LID);
                    $dao->executeNonQuery();

                    $strSql = "SELECT IFNULL(GROUP_CONCAT(FASFD.detailunkid),'') AS detailid FROM " . CONFIG_DBN . ".fasfoliodetail AS FASFD
                             WHERE  FASFD.parentid=:parentid  AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid";


                    $dao->initCommand($strSql);
                    $dao->addParameter(':parentid', $item);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $Obj = $dao->executeRow();

                    if ($Obj['detailid'] != "") {
                        $strSql2 = "UPDATE " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid,
                                                                                 description=:description
                                 WHERE detailunkid IN (" . $Obj['detailid'] . ")  AND companyid=:companyid AND locationid=:locationid";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(':foliounkid', $res['foliounkid']);
                        $dao->addParameter(':description', "Split from " . $srcfolio['foliono']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                    }

                    $str = "SELECT IFNULL(GROUP_CONCAT(FASFD.detailunkid),'') AS detailid FROM " . CONFIG_DBN . ".fasfoliodetail as FASFD
                        INNER JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid = FASFD.masterunkid WHERE parentid=:parentid AND FASM.mastertypeunkid=9 AND FASFD.foliounkid=:foliounkid AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid";


                    $dao->initCommand($str);
                    $dao->addParameter(':parentid', $item);
                    $dao->addParameter(':foliounkid', $res['foliounkid']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':locationid', CONFIG_LID);
                    $objdata = $dao->executeRow();

                    if ($objdata['detailid'] != "") {
                        $strSql2 = "UPDATE " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid,
                                                                                 description=:description
                                 WHERE parentid IN (" . $objdata['detailid'] . ")  AND companyid=:companyid AND locationid=:locationid";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(':foliounkid', $res['foliounkid']);
                        $dao->addParameter(':description', "Split from " . $srcfolio['foliono']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                    }
                }
                return html_entity_decode(json_encode(array("Success" => "True", "Message" =>$languageArr->LANG61)));
            } else {
                return html_entity_decode(json_encode(array("Success" => "False", "Message" => $defaultlanguageArr->INTERNAL_ERROR)));
            }

        } catch (\Exception $e) {
            $this->log->logIt($this->module . '-invoicebysearch' . $e);
        }
    }

    public function orderlistformerge($data)
    {
        try {
            $this->log->logIt($this->module . '-orderlistformerge');
            $dao = new \dao;
            $id = $data['folio'];

            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $timeformat = \common\staticarray::$mysqltimeformat[\database\parameter::getParameter('timeformat')];
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $ObjUtil = new \util\util;
            $strSql = "SELECT FM.hashkey as fm_hashkey,FM.foliono,TRC.name,IFNULL(DATE_FORMAT(FM.opendate,'" . $mysqlformat . "'),'') AS orderDate,
                           IFNULL(TIME_FORMAT(FM.opendate,'" . $timeformat . "'),'') as orderTime,
                           IFNULL(ROUND(SUM(FD.baseamount*FASM.crdr*FD.quantity)," . $round_off . "),0) AS orderAmt
                           FROM " . CONFIG_DBN . ".trcontact AS TRC 
                           INNER JOIN " . CONFIG_DBN . ".fasfoliomaster AS FM ON TRC.contactunkid=FM.lnkcontactid and TRC.companyid=FM.companyid AND FM.isvoid=0 AND FM.locationid=TRC.locationid
                           LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail FD ON FM.foliounkid=FD.foliounkid AND FM.companyid=FD.companyid and FD.isvoid=0 AND FD.locationid=FM.locationid
                           LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid = FD.masterunkid AND FASM.companyid=:companyid AND FASM.locationid=FD.locationid
                           LEFT JOIN " . CONFIG_DBN . ".cfcompany AS cmp ON cmp.companyid =  TRC.companyid
                           LEFT JOIN " . CONFIG_DBN . ".cflocation AS cfl ON cfl.locationid =  TRC.locationid
                           WHERE TRC.companyid=:companyid AND TRC.locationid=:locationid AND FM.hashkey NOT IN('$id') ";

            $strSql .= " GROUP BY FM.foliounkid ORDER BY FM.foliounkid DESC  ";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $data = $dao->executeQuery();

            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $data)));

        } catch (\Exception $e) {
            $this->log->logIt($this->module . '-orderlistformerge -' . $e);
        }
    }

    public function mergeorder($data,$languageArr,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . '-mergeorder');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\orderlogdao();
            $mergeuserdata = $data['order'];

            foreach ($mergeuserdata as $umdata) {

                $mid = $ObjCommonDao->getprimarykey('fasfoliomaster', $umdata, 'foliounkid');

                $strupdte = "UPDATE " . CONFIG_DBN . ".fasfoliomaster AS ffm SET ffm.isvoid=:isvoid
                            WHERE ffm.companyid=:companyid AND ffm.locationid=:locationid AND ffm.foliounkid=:foliounkid";

                $dao->initCommand($strupdte);
                $dao->addParameter(':isvoid', 1);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $dao->addParameter(':foliounkid', $mid);
                $dao->executeNonQuery();


                $str = "SELECT IFNULL(GROUP_CONCAT(ffd.detailunkid),'') AS detailid,ffm.foliono,ffm.foliounkid FROM " . CONFIG_DBN . ".fasfoliomaster  AS ffm 
                        LEFT JOIN " . CONFIG_DBN . ". fasfoliodetail AS ffd ON ffd.foliounkid= ffm.foliounkid
                        WHERE ffm.foliounkid=:foliounkid AND ffm.companyid=:companyid AND ffm.locationid=:locationid ";
                $dao->initCommand($str);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $dao->addParameter(':foliounkid', $mid);
                $folio = $dao->executeRow();

                if ($folio['detailid'] > 0) {
                    $oid = $ObjCommonDao->getprimarykey('fasfoliomaster', $data['folio'], 'foliounkid');


                    $str = " UPDATE " . CONFIG_DBN . ".fasfoliodetail AS ffd  SET ffd.foliounkid=:foliounkid,
                                                                                   ffd.description=:description
                                WHERE ffd.detailunkid IN (" . $folio['detailid'] . ") AND ffd.companyid=:companyid AND ffd.locationid=:locationid";

                    $dao->initCommand($str);
                    $dao->addParameter(':foliounkid', $oid);
                    $dao->addParameter(':description', "Merged from " . $folio['foliono']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':locationid', CONFIG_LID);
                    $dao->executeNonQuery();
                }
                $ObjAuditDao->addOrderLog($oid, 'MERGE_FROM', $folio);
                return (json_encode(array("Success" => "True", "Message" => $languageArr->LANG63)));
            }
        } catch (\Exception $e) {
            $this->log->logIt($this->module . '-mergeorder-' . $e);
        }
    }

    public function getlistofguestdata($data)
    {
        try {
            $this->log->logIt($this->module . ' - getlistofguestdata -');
            $dao = new \dao();
            $id = $data['folio'];
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $date_format = \database\parameter::getParameter('dateformat');
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $strSql = "SELECT IFNULL(ROUND(SUM(FD.baseamount*FASM.crdr*FD.quantity)," . $round_off . "),0) AS orderAmt,IFNULL(DATE_FORMAT(FM.opendate,'" . $mysqlformat . "'),'') AS orderDate,FM.foliono,FM.foliounkid,FD.hashkey AS hkey, FASM.crdr,FD.quantity,FD.baseamount, FM.hashkey as fm_hashkey,FM.foliono, FM.foliounkid,TCT.contactunkid, TCT.hashkey,TCT.salutation,TCT.name,TCT.address,TCT.city,TCT.state,TCT.zip,TCT.mobile,TCT.phone" .
                " ,TCT.email,TCT.fax,TCT.business_name" .
                " FROM " . CONFIG_DBN . ".trcontact AS TCT " .
                "INNER JOIN " . CONFIG_DBN . ".fasfoliomaster AS FM ON TCT.contactunkid=FM.lnkcontactid and TCT.companyid=FM.companyid AND FM.isvoid=0 AND FM.locationid=TCT.locationid" .
                " LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail FD ON FM.foliounkid=FD.foliounkid AND FM.companyid=FD.companyid and FD.isvoid=0 AND FD.locationid=FM.locationid" .
                " LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid = FD.masterunkid AND FASM.companyid=:companyid AND FASM.locationid=FD.locationid" .
                " WHERE TCT.companyid=:companyid AND TCT.locationid=:locationid and TCT.contacttype=:contacttype AND FM.hashkey NOT IN('$id')  ";

            if (isset($data['name']) && $data['name'] != "") {
                $strSql .= " and TCT.name LIKE '%" . $data['name'] . "%'";
            }
            if (isset($data['email']) && $data['email'] != "") {
                $strSql .= " and TCT.email LIKE '%" . $data['email'] . "%'";
            }
            if (isset($data['mobile']) && $data['mobile'] != "") {
                $strSql .= " and TCT.mobile LIKE '%" . $data['mobile'] . "%'";
            }
            if (isset($data['name']) && isset($data['mobile']) && $data['name'] != "" && $data['mobile'] != "") {
                $strSql .= " and TCT.name LIKE '%" . $data['name'] . "%' and TCT.mobile LIKE '%" . $data['mobile'] . "%'";
            }
            if (isset($data['contactid']) && $data['contactid'] != "" && $data['contactid'] != 0) {
                $strSql .= " and hashkey='" . $data['contactid'] . "'";
            }
            $strSql .= " GROUP BY FM.foliounkid ORDER BY FM.foliounkid DESC";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->addParameter(":contacttype", 1);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success" => "True", "Cnt" => count($res), "Data" => $res)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getlistofguestdata' . $e);
        }
    }

    public function pmsint()
    {
        $dao = new \dao();
/*
        $str = "SELECT pms_integration FROM " . CONFIG_DBN . ".cflocation WHERE locationid=:locationid";

        $dao->initCommand($str);
        $dao->addParameter(':locationid', CONFIG_LID);
        $data = $dao->executeRow();
        return $data;*/
    }

    public function removeorder($data,$languageArr,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . '-removeorder');
            $dao = new\dao();
            $datetime = \util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjInventoryDao = new\database\inventorydao();
            $id = $ObjCommonDao->getprimarykey('fasfoliomaster', $data['id'], 'foliounkid');

            $operation = 1;//Increase
            $unitratio = 100;
            $isvoid = 1;

            $strSql1 = "SELECT FD.detailunkid,FD.lnkmappingunkid,FD.quantity,FASM.mastertypeunkid FROM " . CONFIG_DBN . ".fasfoliodetail AS FD " .
                " INNER JOIN " . CONFIG_DBN . ".fasmaster FASM ON FD.masterunkid=FASM.masterunkid AND FASM.companyid=:companyid 
                 WHERE FD.foliounkid=:foliounkid AND isvoid=:isvoid AND FD.companyid=:companyid AND FD.locationid=:locationid AND FASM.mastertypeunkid IN(1,8,9,10)";
            $dao->initCommand($strSql1);
            $dao->addParameter(":foliounkid", $id);
            $dao->addParameter(":isvoid", 0);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeQuery();
            if(CONFIG_IS_STORE == 1) {
                foreach ($res as $val) {

                    if ($val['mastertypeunkid'] == 1 || $val['mastertypeunkid'] == 8 || $val['mastertypeunkid'] == 9 || $val['mastertypeunkid'] == 10) {

                        if ($val['mastertypeunkid'] == 1) {
                            $itemtype = 2;//Combo product
                            $ObjInventoryDao->updateItemInventory($val['lnkmappingunkid'], $val['quantity'], $unitratio, $itemtype, $operation,$id,$val['detailunkid'],$isvoid);
                        } elseif ($val['mastertypeunkid'] == 8) {
                            $itemtype = 1; //item
                            $ObjInventoryDao->updateItemInventory($val['lnkmappingunkid'], $val['quantity'], $unitratio, $itemtype, $operation,$id,$val['detailunkid'],$isvoid);
                        } elseif ($val['mastertypeunkid'] == 9) {
                            $recipe_type = 3; //Modifier item
                            $ObjInventoryDao->updateModifierInventory($val['lnkmappingunkid'], $val['quantity'], $recipe_type, $operation,$id,$val['detailunkid'],$isvoid);
                        } else {
                            $recipe_type = 2; //modifier
                            $ObjInventoryDao->updateModifierInventory($val['lnkmappingunkid'], $val['quantity'], $recipe_type, $operation,$id,$val['detailunkid'],$isvoid);
                        }
                    }
                }
            }

            $strSql = "UPDATE " . CONFIG_DBN . ".fasfoliomaster SET foliostatus=4,isvoid=:isvoid,
                                                                    modified_user=:modified_user,
                                                                    modifieddatetime=:modifieddatetime 
                                                                    WHERE hashkey=:id AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(":id", $data['id']);
            $dao->addParameter(":isvoid", 1);
            $dao->addParameter(':modified_user', CONFIG_UID);
            $dao->addParameter(':modifieddatetime', $datetime);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->executeNonQuery();


            $strSql = "UPDATE " . CONFIG_DBN . ".fasfoliodetail SET isvoid=1, voiduserunkid=:voiduserunkid, voiddatetime=:voiddatetime WHERE locationid=:locationid AND companyid=:companyid AND foliounkid=:foliounkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":foliounkid", $id);
            $dao->addParameter(':voiduserunkid', CONFIG_UID);
            $dao->addParameter(':voiddatetime', $datetime);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->executeNonQuery();

            /*$str = "SELECT sessionunkid,lnkfolioid,isreleased FROM " . CONFIG_DBN . ".cftablesession
                        WHERE lnkfolioid=:lnkfolioid AND companyid=:companyid AND locationid=:locationid";

            $dao->initCommand($str);
            $dao->addParameter(':lnkfolioid', $id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $releaseddata = $dao->executeRow();

            if (isset($releaseddata['sessionunkid']) && $releaseddata['isreleased'] == 0) {
                $str = "UPDATE " . CONFIG_DBN . ".cftablesession SET releasedate=:releasedate,
                                                                     isreleased=:isreleased
                                                                     WHERE lnkfolioid=:lnkfolioid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($str);
                $dao->addParameter(':releasedate', $datetime);
                $dao->addParameter(':isreleased', 1);
                $dao->addParameter(':lnkfolioid', $id);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
            }*/
            return html_entity_decode(json_encode(array("Success" => "True", "Message" => $defaultlanguageArr->REM_REC_SUC)));

        } catch (\Exception $e) {
            $this->log->logIt($this->module . '-removeorder-' . $e);
        }
    }

    public function getItemList()
    {
        try {
            $this->log->logIt($this->module . '-getItemList');
            $dao = new\dao();
            $strSql = "SELECT itemunkid, itemname, categoryunkid, sku, sale_amount, hashkey, type FROM  " . CONFIG_DBN . ".cfmenu_items WHERE companyid=:companyid AND locationid=:locationid AND is_active=1 AND is_deleted=0";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $data = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $data)));

        } catch (\Exception $e) {
            $this->log->logIt($this->module . '-getItemList-' . $e);
        }
    }

    public function postPayment($data,$folio_id,$languageArr,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - postPayment');
            $dao = new \dao();
            $current_date = \util\util::getLocalDateTime();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $year = \util\util::getYearCode();
            $rec = $this->getamount($data['folioid']);
            $amount = $rec['orderAmt'];
            if($data['amount']!=0 && $data['amount']!=''){
                if($data['amount']>$amount){
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG64)));
                }else{
                    /*Get payment detail*/
                        $strSql1 = "SELECT paymenttypeunkid,lnkmasterunkid FROM ".CONFIG_DBN.".cfpaymenttype WHERE ".
                            "companyid=:companyid AND locationid=:locationid AND paymenttypeunkid=:paymenttypeunkid";
                        $dao->initCommand($strSql1);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->addParameter(':paymenttypeunkid', $data['select_payment']);
                        $resObj1 = $dao->executeRow();
                    /*Get payment detail*/
                    /*Post payment*/
                        $hash_key = \util\util::gethash();
                        $strSql2 = "INSERT INTO ".CONFIG_DBN.".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid".
                            ",masterunkid=:masterunkid,year_code=:year_code,quantity=:quantity,trandate=:trandate,baseamount=:baseamount".
                            ",lnkmappingunkid=:lnkmappingunkid,originatedfrom=0,parentid=0,hashkey=:hashkey";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(':foliounkid',$folio_id);
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $dao->addparameter(':locationid',CONFIG_LID);
                        $dao->addParameter(":postinguserunkid",CONFIG_UID);
                        $dao->addParameter(':masterunkid',$resObj1['lnkmasterunkid']);
                        $dao->addParameter(':year_code',$year);
                        $dao->addParameter(':baseamount',round((-1)*$data['amount'], $round_off));
                        $dao->addParameter(':quantity',1);
                        $dao->addParameter(':lnkmappingunkid',$resObj1['paymenttypeunkid']);
                        $dao->addParameter(':trandate',$current_date);
                        $dao->addParameter(':hashkey',$hash_key);
                        $dao->executeNonQuery();
                    /*Post payment*/
                    return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$languageArr->LANG65)));
                }
            }
        }catch(Exception $e){
            $this->log->logIt($this->module.' - postPayment - '.$e);
        }
    }

    public function cancelOrder($data,$languageArr,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . '- cancelOrder');
            $dao = new\dao();
            $datetime = \util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $id = $ObjCommonDao->getprimarykey('fasfoliomaster', $data['id'], 'foliounkid');
            $strSql1 = "UPDATE ".CONFIG_DBN.".fasfoliomaster SET foliostatus=3,canceluserunkid=:cancel_user,canceldatetime=:cancel_datetime".
                " WHERE foliounkid=:id AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql1);
            $dao->addParameter(":id", $id);
            $dao->addParameter(':cancel_user',CONFIG_UID);
            $dao->addParameter(':cancel_datetime',$datetime);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->executeNonQuery();

            $strSql2 = "UPDATE ".CONFIG_DBN.".fasfoliodetail SET isvoid=1,voiduserunkid=:void_user, voiddatetime=:void_datetime WHERE locationid=:locationid AND companyid=:companyid AND foliounkid=:foliounkid";
            $dao->initCommand($strSql2);
            $dao->addParameter(":foliounkid", $id);
            $dao->addParameter(':void_user', CONFIG_UID);
            $dao->addParameter(':void_datetime', $datetime);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->executeNonQuery();

           /* $strSql3 = "DELETE FROM ".CONFIG_DBN.".cftablesession WHERE lnkfolioid=:lnkfolioid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql3);
            $dao->addParameter(':lnkfolioid', $id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->executeNonQuery();*/

            return html_entity_decode(json_encode(array("Success" => "True", "Message" => $languageArr->LANG66)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . '-cancelOrder-' . $e);
        }
    }

    public function getTagrecords()
    {
        try {
            $this->log->logIt($this->module . " - getTagrecords");
            $dao = new \dao();
            $strSql2 = "SELECT meta_value FROM " . CONFIG_DBN . ". cflocationtemplate_meta  WHERE
                            companyid=:companyid AND locationid=:locationid AND meta_key=:meta_key ";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->addParameter(':meta_key', "tags_value");
            $res= $dao->executeRow();

            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getTagrecords - " . $e);
            return false;
        }
    }

    public function getExchangeRateDetails($id,$locationinfo)
    {
        try {
            $this->log->logIt($this->module . " - getExchangeRateDetails");
            $dao = new \dao();
            $CommonObj=new \database\commondao;
            $id=$CommonObj->getprimarykey('fasfoliomaster',$id,'foliounkid');
            $strSql2 = "SELECT GROUP_CONCAT(DISTINCT currencyunkid) as currencyids,GROUP_CONCAT(DISTINCT currencyamount) as currencyamount FROM " . CONFIG_DBN . ". fasfoliodetail  WHERE
                            isvoid=0 AND companyid=:companyid AND locationid=:locationid AND foliounkid=:foliounkid ";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->addParameter(':foliounkid', $id);
            $res= $dao->executeRow();
            $changecurrency=array();
            if($res){
                $check_currency=explode(',',$res['currencyids']);
                $check_currency=array_values($check_currency);

                $check_currency_rate=explode(',',$res['currencyamount']);
                $check_currency_rate=array_values($check_currency_rate);

                if(isset($check_currency) && count($check_currency)>1){
                    $changecurrency['currency_sign']=$locationinfo['currency_sign'];
                    $changecurrency['currency_rate']=1;
                }else{
                    if(isset($check_currency) && in_array('0',$check_currency)){
                        $changecurrency['currency_sign']=$locationinfo['currency_sign'];
                        $changecurrency['currency_rate']=1;
                    }else{
                        $strSql2 = "SELECT currency_rate,currency_sign FROM " . CONFIG_DBN . ". cfcurrencylist  WHERE
                            companyid=:companyid AND locationid=:locationid AND currencyunkid=:currencyunkid ";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':locationid', CONFIG_LID);
                        $dao->addParameter(':currencyunkid', $check_currency[0]);
                        $rescurrency= $dao->executeRow();

                        $changecurrency['currency_sign']=$rescurrency['currency_sign'];
                        $changecurrency['currency_rate']=$check_currency_rate[0];
                    }
                }

            }
            return $changecurrency;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getExchangeRateDetails - " . $e);
            return false;
        }
    }

    public function getInvoiceDetailForLiquor($id,$template='',$languageArr='',$defaultlanguageArr='')
    {
        try {
            $this->log->logIt($this->module . ' - getInvoiceDetail');
            if($languageArr){
                $languageArr=json_decode($languageArr);
            }if($defaultlanguageArr){
                $defaultlanguageArr=json_decode($defaultlanguageArr);
            }
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $todaysdate = \util\util::getTodaysDateTime();

            $folio_id = $id;
            $hash =$ObjCommonDao->getFieldValue('fasfoliomaster','hashkey','foliounkid',$folio_id);
            $folio_id =$ObjCommonDao->getprimarykey('fasfoliomaster',$folio_id,'foliounkid');

            $post_amount = self::getamount($hash);
            $liquer_id = 'Liquor';
            if(defined('LIQUOR_IDS') && LIQUOR_IDS!='') {
                $LIQUOR_IDS = LIQUOR_IDS;
                if(isset($LIQUOR_IDS[CONFIG_CID.'_'.CONFIG_LID])){
                    $liquer_id =  $LIQUOR_IDS[CONFIG_CID.'_'.CONFIG_LID];
                }
            }

            // for liquer
            $strSql = "SELECT IF(FASM.mastertypeunkid IN(8,1),IU.name,'') as unitname,FASM.name as Uname,FFM.ordertype,FASFD.hashkey,FASFD.detailunkid,FASFD.lnkmappingunkid,CASE WHEN FASM.mastertypeunkid=1 THEN CFCOMBO.comboname WHEN FASM.mastertypeunkid=8 THEN CFI.itemname ELSE FASM.name END AS particular".
                " ,IFNULL(FASFD.description,'') as comment,FASFD.isvoid,ROUND((FASFD.baseamount*FASM.crdr*FASFD.quantity),".$round_off.") AS Baseamt,ROUND(FASFD.quantity) as quantity".
                " ,IFNULL(DATE_FORMAT(trandate,'".$mysql_format."'),'') as orderdate,FASFD.parentid,FASM.mastertypeunkid,FASFD.masterunkid FROM ".CONFIG_DBN.".fasfoliodetail AS FASFD".
                " INNER JOIN ".CONFIG_DBN.".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid ".
                " AND FASM.companyid=:companyid AND FASM.locationid=:locationid LEFT JOIN ".CONFIG_DBN.".cfmenu_items AS CFI".
                " ON CFI.itemunkid=FASFD.lnkmappingunkid AND CFI.companyid=:companyid AND CFI.locationid=:locationid".
                " LEFT JOIN ".CONFIG_DBN.".cfmenu_combo AS CFCOMBO ON CFCOMBO.combounkid=FASFD.lnkmappingunkid AND CFCOMBO.companyid=:companyid AND CFCOMBO.locationid=:locationid" .
                " LEFT JOIN " . CONFIG_DBN . ".fasfoliomaster AS FFM ON FFM.foliounkid=FASFD.foliounkid AND FFM.companyid=:companyid AND FFM.locationid=:locationid".
                " LEFT JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS IU ON FASFD.lnkunitid=IU.unitunkid AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid".
                " LEFT JOIN " . CONFIG_DBN . ".cfmenu_categories AS CFC ON CFC.categoryunkid=CFI.categoryunkid AND CFC.companyid=:companyid AND CFC.locationid=:locationid".
                " LEFT JOIN " . CONFIG_DBN . ".cfmenu AS CFM ON CFM.menuunkid=CFC.menuunkid AND CFM.companyid=:companyid AND CFM.locationid=:locationid".
                " WHERE FASFD.foliounkid=:folioid AND FASFD.isvoid=0 AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid AND FASFD.parentid=0 AND CFM.name = '".$liquer_id."' ORDER BY FASFD.detailunkid ASC ";
            $dao->initCommand($strSql);
            $dao->addParameter(':folioid', $folio_id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $data = $dao->executeQuery();
            $resarr = array();
            if(isset($data) && count($data)>0) {
                foreach ($data as $key => $val) {
                    /* COMBO PRODUCT */                                        // CHANGE BY TANVIR
                    if ($val['mastertypeunkid'] == 1) {
                        $strSql = "SELECT IF(FASM.mastertypeunkid IN(8,1),IU.name,'') as unitname,FASM.name as Uname,FASFD.lnkmappingunkid,FFM.ordertype,FASFD.hashkey,FASFD.detailunkid,FASM.name AS particular" .
                            " ,IFNULL(FASFD.description,'') as comment,FASFD.isvoid,ROUND((FASFD.baseamount*FASM.crdr*FASFD.quantity)," . $round_off . ") AS Baseamt,ROUND(FASFD.quantity) as quantity" .
                            " ,IFNULL(DATE_FORMAT(trandate,'" . $mysql_format . "'),'') as orderdate,FASFD.parentid,FASM.mastertypeunkid,FASFD.masterunkid FROM " . CONFIG_DBN . ".fasfoliodetail AS FASFD " .
                            " INNER JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid AND FASM.mastertypeunkid !=8 " .
                            " AND FASM.companyid=:companyid AND FASM.locationid=:locationid " .
                            " LEFT JOIN " . CONFIG_DBN . ".cfmenu_combo AS CFCOMBO ON CFCOMBO.combounkid=FASFD.lnkmappingunkid AND CFCOMBO.companyid=:companyid AND CFCOMBO.locationid=:locationid" .
                            " LEFT JOIN " . CONFIG_DBN . ".fasfoliomaster AS FFM ON FFM.foliounkid=FASFD.foliounkid AND FFM.companyid=:companyid AND FFM.locationid=:locationid" .
                            " LEFT JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS IU ON FASFD.lnkunitid=IU.unitunkid AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid" .
                            " WHERE FASFD.foliounkid=:folioid AND FASFD.isvoid=0 AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid AND FASFD.parentid = " . $val['detailunkid'] . " ORDER BY FASFD.detailunkid ASC ";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':folioid', $folio_id);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $newdata1 = $dao->executeQuery();

                        $data = array_merge($data, $newdata1);
                    }

                    /* ITEM */
                    if ($val['mastertypeunkid'] == 8) {

                        $strSql = "SELECT IF(FASM.mastertypeunkid IN(8,1),IU.name,'') as unitname,FASM.name as Uname,FASFD.lnkmappingunkid,FFM.ordertype,FASFD.hashkey,FASFD.detailunkid,CASE WHEN FASM.mastertypeunkid=9 THEN CFMI.itemname WHEN FASM.mastertypeunkid=10 THEN CMM.modifiername ELSE FASM.name END AS particular" .
                            " ,IFNULL(FASFD.description,'') as comment,FASFD.isvoid,ROUND((FASFD.baseamount*FASM.crdr*FASFD.quantity)," . $round_off . ") AS Baseamt,ROUND(FASFD.quantity) as quantity" .
                            " ,IFNULL(DATE_FORMAT(trandate,'" . $mysql_format . "'),'') as orderdate,FASFD.parentid,FASM.mastertypeunkid,FASFD.masterunkid FROM " . CONFIG_DBN . ".fasfoliodetail AS FASFD" .
                            " INNER JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid " .
                            " AND FASM.companyid=:companyid AND FASM.locationid=:locationid " .
                            " LEFT JOIN " . CONFIG_DBN . ".cfmenu_modifier_items AS CFMI ON CFMI.modifieritemunkid=FASFD.lnkmappingunkid AND CFMI.companyid=:companyid AND CFMI.locationid=:locationid" .
                            " LEFT JOIN " . CONFIG_DBN . ".cfmenu_modifiers AS CMM ON CMM.modifierunkid=FASFD.lnkmappingunkid AND CMM.locationid=:locationid AND CMM.companyid=:companyid" .
                            " LEFT JOIN " . CONFIG_DBN . ".fasfoliomaster AS FFM ON FFM.foliounkid=FASFD.foliounkid AND FFM.companyid=:companyid AND FFM.locationid=:locationid" .
                            " LEFT JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS IU ON FASFD.lnkunitid=IU.unitunkid AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid" .
                            " WHERE FASFD.foliounkid=:folioid AND FASFD.isvoid=0 AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid AND FASFD.parentid = " . $val['detailunkid'] . " ORDER BY FASFD.detailunkid ASC ";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':folioid', $folio_id);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $newdata2 = $dao->executeQuery();

                        $data = array_merge($data, $newdata2);

                        if (isset($newdata2) && count($newdata2) > 0) {
                            foreach ($newdata2 as $nkey => $nval) {
                                if ($nval['mastertypeunkid'] == 9 || $nval['mastertypeunkid'] == 10) {


                                    $strSql = "SELECT IF(FASM.mastertypeunkid IN(8,1),IU.name,'') as unitname,FASM.name as Uname,FFM.ordertype,FASFD.hashkey,FASFD.detailunkid,FASM.name AS particular" .
                                        " ,IFNULL(FASFD.description,'') as comment,FASFD.isvoid,FASFD.lnkmappingunkid,ROUND((FASFD.baseamount*FASM.crdr*FASFD.quantity)," . $round_off . ") AS Baseamt,ROUND(FASFD.quantity) as quantity" .
                                        " ,IFNULL(DATE_FORMAT(trandate,'" . $mysql_format . "'),'') as orderdate,FASFD.parentid,FASM.mastertypeunkid,FASFD.masterunkid FROM " . CONFIG_DBN . ".fasfoliodetail AS FASFD " .
                                        " INNER JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid" .
                                        " AND FASM.companyid=:companyid AND FASM.locationid=:locationid " .
                                        " LEFT JOIN " . CONFIG_DBN . ".fasfoliomaster AS FFM ON FFM.foliounkid=FASFD.foliounkid AND FFM.companyid=:companyid AND FFM.locationid=:locationid" .
                                        " LEFT JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS IU ON FASFD.lnkunitid=IU.unitunkid AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid" .
                                        " WHERE FASFD.foliounkid=:folioid AND FASFD.isvoid=0 AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid AND FASFD.parentid = " . $nval['detailunkid'] . " ORDER BY FASFD.detailunkid ASC";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':folioid', $folio_id);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $newdata3 = $dao->executeQuery();

                                    $data = array_merge($data, $newdata3);
                                }
                            }
                        }
                    }
                }

                $res = \util\util::msort($data, 'detailunkid');
                $retArr = array();
                $tracarr = array();
                $subamt = $amt = $price = 0;
                foreach ($res AS $val) {
                    if ($val['parentid'] == 0 && ($val['mastertypeunkid'] == 1 || $val['mastertypeunkid'] == 8)) {
                        $retArr[$val['detailunkid']]['item'] = $val;
                        $tracarr[$val['detailunkid']] = $ObjCommonDao->getprimarykey('fasfoliodetail', $val['hashkey'], 'detailunkid');
                        $retArr[$val['detailunkid']]['item']['total'] = $val['Baseamt'];
                    }
                    if ($val['parentid'] != 0 && ($val['mastertypeunkid'] == 9 || $val['mastertypeunkid'] == 10)) {
                        $retArr[$val['parentid']]['modifier'][] = $val;
                        $retArr[$val['parentid']]['item']['total'] = $retArr[$val['parentid']]['item']['total'] + $val['Baseamt'];
                    }
                    if ($val['parentid'] != 0 && $val['mastertypeunkid'] == 3) {
                        $retArr[$val['parentid']]['discount'][] = $val;
                    }
                    if ($val['mastertypeunkid'] == 8 || $val['mastertypeunkid'] == 9 || $val['mastertypeunkid'] == 10 || $val['mastertypeunkid'] == 1)
                        $amt = $amt + $val['Baseamt'];

                }
                $resarr['itemarr'] = $retArr;
                $resarr['amt'] = $amt;
                /*Get posted payments Name */

                $resarr['order_total'] = $amt;
                $resarr['total'] = $amt - $price - $resarr['post_amount'];
            }
            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module.' - getInvoiceDetail - '.$e);
        }
    }
}
?>