<?php

namespace database;

class goodsreturndao
{
    public $module = 'DB_goodsreturndao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function goodsReturnList($limit, $offset, $from_date, $to_date, $order_no, $vendor,$voucherno)
    {
        try {
            $this->log->logIt($this->module. ' - goodsReturnList');
            $ObjUtil = new \util\util;
            $fromDate = $ObjUtil->convertDateToMySql($from_date);
            $toDate = $ObjUtil->convertDateToMySql($to_date);
            $dao = new \dao;
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $strSql = "SELECT CFGR.grid,CFGR.lnkgrnid,CFGR.voucher_no,CFGR.gr_doc_num,CFGR.remarks,ROUND(CFGR.totalamount,$round_off) as totalamount,TRC.business_name,".
                      "IFNULL(DATE_FORMAT(CFGR.gr_date,'".$mysql_format."'),'') AS gr_date,IFNULL(CFU1.username,'') AS createduser,".
                      "IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CFGR.createddatetime,'".$mysql_format."'),'') AS created_date,".
                      "IFNULL(DATE_FORMAT(CFGR.modifieddatetime,'".$mysql_format."'),'') AS modified_date ".
                      "FROM ".CONFIG_DBN.".cfgoodsreturn AS CFGR ".
                      "LEFT JOIN ".CONFIG_DBN.".cfuser AS CFU1 ON CFGR.createduser=CFU1.userunkid AND CFU1.companyid=CFU1.companyid ".
                      "LEFT JOIN ".CONFIG_DBN.".cfuser AS CFU2 ON CFGR.modifieduser=CFU2.userunkid AND CFU1.companyid=CFU2.companyid ".
                      "LEFT JOIN ".CONFIG_DBN.".cfvandor AS CV ON CFGR.lnkvendorid=CV.vandorunkid AND CV.companyid=:companyid ".
                      "LEFT JOIN ".CONFIG_DBN.".trcontact AS TRC ON CV.lnkcontactid=TRC.contactunkid AND TRC.companyid=:companyid ".
                      "WHERE CFGR.companyid=:companyid AND CFGR.storeid=:storeid AND CFGR.is_deleted=0";
            if ($order_no!= "") {
                $strSql .= " AND CFGR.gr_doc_num LIKE '%".$order_no."%'";
            }
            if ($vendor != 0) {
                $strSql .= " AND CFGR.lnkvendorid =".$vendor."";
            }
            if ($voucherno != "") {
                $strSql .= " AND CFGR.voucher_no LIKE '%" . $voucherno . "%'";
            }
            
            if ($fromDate != "" && $toDate!= "") {
                $strSql .= " AND CFGR.gr_date BETWEEN '".$fromDate."' AND '".$toDate."' ";
            }
            $strSql .= " ORDER BY CFGR.grid DESC";
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqlLmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $data = $dao->executeQuery();
            if ($limit != "" && ($offset != "" || $offset != 0)){
                $dao->initCommand($strSql.$strSqlLmt);
            }else{
                $dao->initCommand($strSql);
            }
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $rec = $dao->executeQuery();
        
            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return html_entity_decode(json_encode($retvalue),ENT_QUOTES);
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue),ENT_QUOTES);
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - goodsReturnList - ' . $e);
        }
    }
    public function addGR($data,$defaultlanguageArr='')
    {
        try {
            $this->log->logIt($this->module.' - addGR - ');
            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $gr_array = $data['gr_items'];
            $ObjUtil = new \util\util;
            $gr_date = $ObjUtil->convertDateToMySql($data['gr_date']);

            $ObjCommonDao = new \database\commondao();
            if ($data['id'] == '0' || $data['id'] == '') {
                $gr_doc_num = $ObjCommonDao->getIndentDocumentNumbering('goods_return', 'inc');
                $gr_vc_num = $ObjCommonDao->getIndentDocumentNumbering('gr_voucher_no', 'inc');

                $strSql1 = "INSERT into ".CONFIG_DBN.".cfgoodsreturn SET gr_date=:gr_date,gr_doc_num=:gr_doc_num,voucher_no=:voucher_no,
                            lnkvendorid=:lnkvendorid,companyid=:companyid,storeid=:storeid,createddatetime=:createddatetime,
                            createduser=:createduser,totalamount=:total_amount,remarks=:remarks";
                $dao->initCommand($strSql1);
                $dao->addParameter(':gr_date', $gr_date);
                $dao->addParameter(':gr_doc_num', $gr_doc_num);
                $dao->addParameter(':voucher_no', $gr_vc_num);
                $dao->addParameter(':lnkvendorid', $data['gr_sel_vendor']);
                $dao->addParameter(':total_amount', $data['total_amount']);
                $dao->addParameter(':remarks', $data['gr_remarks']);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->addParameter(':createduser', CONFIG_UID);
                $dao->executeNonQuery();
                $grId = $dao->getLastInsertedId();
                foreach ($gr_array AS $key => $value) {
                    if(is_array($value['item_tax_det']) && count($value['item_tax_det'])>0){
                        $tax_description = json_encode($value['item_tax_det']);
                    }else{
                        $tax_description = '';
                    }
                    $strSql2 = "INSERT into ".CONFIG_DBN.".cfgoodsreturndetail SET 
                                lnkgrid=:lnkgrid,lnkrawid=:lnkrawid,
                                lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,
                                qty=:qty,rate=:rate,discount=:discount,discount_amount=:discount_amount,tax_amount=:tax_amount,
                                tax_description=:tax_description,total=:total,storeid=:storeid,companyid=:companyid";
                    $dao->initCommand($strSql2);
                    $dao->addParameter(':lnkgrid', $grId);
                    $dao->addParameter(':lnkrawid', $value['item_id']);
                    $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                    $dao->addParameter(':lnkunitid', $value['item_unit']);
                    $dao->addParameter(':qty', $value['item_qty']);
                    $dao->addParameter(':rate', $value['item_rpu']);
                    $dao->addParameter(':discount', $value['item_disc_per']);
                    $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                    $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                    $dao->addParameter(':tax_description', $tax_description);
                    $dao->addParameter(':total',$value['item_total_amt']);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->executeNonQuery();
                }
                return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_ADD_SUC));
            } else {
                $strSql3 = "SELECT grdetailunkid  FROM ".CONFIG_DBN.".cfgoodsreturndetail 
                            WHERE lnkgrid=:lnkgrid AND companyid=:companyid AND storeid=:storeid";
                $dao->initCommand($strSql3);
                $dao->addParameter(':lnkgrid', $data['id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $resObj3 = $dao->executeQuery();

                $result3 = \util\util::getoneDarray($resObj3, 'grdetailunkid');

                $gr_del_arr = [];
                foreach ($gr_array as $key => $value) {
                    if(is_array($value['item_tax_det']) && count($value['item_tax_det'])>0){
                        $tax_description = json_encode($value['item_tax_det']);
                    }else{
                        $tax_description = '';
                    }
                    if (in_array($value['itm_detId'], $result3)) {
                        $strSql4 = "UPDATE ".CONFIG_DBN.".cfgoodsreturndetail SET 
                                    lnkrawid=:lnkrawid,lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,
                                    qty=:qty,rate=:rate,discount=:discount,discount_amount=:discount_amount,tax_amount=:tax_amount,
                                    tax_description=:tax_description,total=:total
                                    WHERE grdetailunkid=:grdetailunkid AND lnkgrid=:lnkgrid AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strSql4);
                        $dao->addParameter(':grdetailunkid', $value['itm_detId']);
                        $dao->addParameter(':lnkgrid', $data['id']);
                        $dao->addParameter(':lnkrawid', $value['item_id']);
                        $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                        $dao->addParameter(':lnkunitid', $value['item_unit']);
                        $dao->addParameter(':qty', $value['item_qty']);
                        $dao->addParameter(':rate', $value['item_rpu']);
                        $dao->addParameter(':discount', $value['item_disc_per']);
                        $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                        $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                        $dao->addParameter(':tax_description', $tax_description);
                        $dao->addParameter(':total',$value['item_total_amt']);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->executeNonQuery();
                        $gr_del_arr[] = $value['itm_detId'];
                    } else {
                        $strSql5 = "INSERT into ".CONFIG_DBN.".cfgoodsreturndetail SET lnkgrid=:lnkgrid,lnkrawid=:lnkrawid,
                                    lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,qty=:qty,rate=:rate,
                                    discount=:discount,discount_amount=:discount_amount,tax_amount=:tax_amount,
                                    tax_description=:tax_description,total=:total,storeid=:storeid,companyid=:companyid";
                        $dao->initCommand($strSql5);
                        $dao->addParameter(':lnkgrid', $data['id']);
                        $dao->addParameter(':lnkrawid', $value['item_id']);
                        $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                        $dao->addParameter(':lnkunitid', $value['item_unit']);
                        $dao->addParameter(':qty', $value['item_qty']);
                        $dao->addParameter(':rate', $value['item_rpu']);
                        $dao->addParameter(':discount', $value['item_disc_per']);
                        $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                        $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                        $dao->addParameter(':tax_description', $tax_description);
                        $dao->addParameter(':total',$value['item_total_amt']);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->executeNonQuery();
                        $gr_del_arr[] = $dao->getLastInsertedId();
                    }
                }
                $strSql6 = "DELETE FROM ".CONFIG_DBN.".cfgoodsreturndetail              
                            WHERE grdetailunkid NOT IN ('".implode("','",$gr_del_arr)."') 
                            AND lnkgrid=:lnkgrid AND companyid=:companyid AND storeid=:storeid";
                $dao->initCommand($strSql6);
                $dao->addParameter(':lnkgrid', $data['id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->executeNonQuery();

                $strSql7 = "UPDATE ".CONFIG_DBN.".cfgoodsreturn SET totalamount=:totalamount,remarks=:remarks,
                            modifieddatetime=:modifieddatetime,modifieduser=:modifieduser WHERE grid=:grid AND companyid=:companyid AND storeid=:storeid";
                $dao->initCommand($strSql7);
                $dao->addParameter(':grid', $data['id']);
                $dao->addParameter(':remarks', $data['gr_remarks']);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':modifieduser', CONFIG_UID);
                $dao->addParameter(':totalamount', $data['total_amount']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->executeNonQuery();
                return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_UP_SUC));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module.' - addGR - '.$e);
        }
    }
    public function addGRFromGRN($data,$languageArr='')
    {
        try {
            $this->log->logIt($this->module.' - addGRFromGRN - ');
            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $gr_items_array = $data['gr_items'];
            $ObjUtil = new \util\util;
            $gr_date = $ObjUtil->convertDateToMySql($data['gr_date']);

            $ObjCommonDao = new \database\commondao();
            if ($data['grn_id']!= '0' && $data['grn_id'] != '') {
                $gr_doc_num = $ObjCommonDao->getIndentDocumentNumbering('goods_return', 'inc');
                $gr_vc_num = $ObjCommonDao->getIndentDocumentNumbering('gr_voucher_no', 'inc');

                $strSql1 = "INSERT into ".CONFIG_DBN.".cfgoodsreturn SET gr_date=:gr_date,gr_doc_num=:gr_doc_num,voucher_no=:voucher_no,
                            lnkgrnid=:grn_id,lnkvendorid=:lnkvendorid,companyid=:companyid,storeid=:storeid,createddatetime=:createddatetime,
                            createduser=:createduser,totalamount=:total_amount,remarks=:remarks";
                $dao->initCommand($strSql1);
                $dao->addParameter(':grn_id', $data['grn_id']);
                $dao->addParameter(':gr_date', $gr_date);
                $dao->addParameter(':gr_doc_num', $gr_doc_num);
                $dao->addParameter(':voucher_no', $gr_vc_num);
                $dao->addParameter(':lnkvendorid', $data['gr_sel_vendor']);
                $dao->addParameter(':total_amount', $data['total_amount']);
                $dao->addParameter(':remarks', $data['gr_remarks']);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->addParameter(':createduser', CONFIG_UID);
                $dao->executeNonQuery();
                $grId = $dao->getLastInsertedId();
                if(is_array($gr_items_array) && count($gr_items_array)>0){
                    foreach ($gr_items_array AS $key => $value) {
                        if(is_array($value['item_tax_det']) && count($value['item_tax_det'])>0){
                            $tax_description = json_encode($value['item_tax_det']);
                        }else{
                            $tax_description = '';
                        }
                        $strSql2 = "INSERT INTO ".CONFIG_DBN.".cfgoodsreturndetail SET lnkgrid=:lnkgrid,lnkgrndetailid=:det_id,
                                    lnkrawid=:lnkrawid,lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,qty=:qty,rate=:rate,
                                    discount=:discount,discount_amount=:discount_amount,tax_amount=:tax_amount,
                                    tax_description=:tax_description,total=:total,storeid=:storeid,companyid=:companyid";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(':lnkgrid', $grId);
                        $dao->addParameter(':det_id', $value['itm_detId']);
                        $dao->addParameter(':lnkrawid', $value['item_id']);
                        $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                        $dao->addParameter(':lnkunitid', $value['item_unit']);
                        $dao->addParameter(':qty', $value['item_qty']);
                        $dao->addParameter(':rate', $value['item_rpu']);
                        $dao->addParameter(':discount', $value['item_disc_per']);
                        $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                        $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                        $dao->addParameter(':tax_description', $tax_description);
                        $dao->addParameter(':total',$value['item_total_amt']);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->executeNonQuery();
                        $gr_det_id = $dao->getLastInsertedId();
                        $strSql3 = "UPDATE ".CONFIG_DBN.".cfgrndetail SET lnkgrdetailid=:gr_det_id WHERE lnkgrnid=:grn_id AND grndetailunkid=:detail_id
                                    AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strSql3);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':gr_det_id', $gr_det_id);
                        $dao->addParameter(':detail_id', $value['itm_detId']);
                        $dao->addParameter(':grn_id', $data['grn_id']);
                        $dao->executeNonQuery();

                        //Update Inventory
                        $amount = $value['item_total_amt'] + $value['item_disc_amt'] - $value['item_tax_amt'];
                        $inventory = $value['item_qty'] * $value['item_unit_val'];

                        $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc 
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strrawLoc);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':item_id', $value['item_id']);
                        $resrawLoc = $dao->executeRow();

                        $dao->initCommand("SELECT u.unitunkid FROM ".CONFIG_DBN.".cfrawmaterial CFR 
					    INNER JOIN ".CONFIG_DBN.".cfunit u ON u.measuretype = CFR.measuretype AND u.is_systemdefined=1 AND u.companyid=:companyid
					    WHERE CFR.id = :id AND CFR.companyid=:companyid ");
                        $dao->addParameter(':id',$value['item_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);

                        $rawMaterialRec = $dao->executeRow();
                        if(!$resrawLoc)
                        {
                            $insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
                            $dao->initCommand($insertInventorySql);
                            $dao->addParameter(':lnkrawmaterialid',$value['item_id']);
                            $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                            $dao->addParameter(':storeid',CONFIG_SID);
                            $dao->addParameter(':companyid',CONFIG_CID);
                            $dao->executeNonQuery();
                            $resrawLoc = [];
                            $resrawLoc['id'] = $dao->getLastInsertedId();
                            $resrawLoc['rateperunit'] = 0;
                            $resrawLoc['inventory'] = 0;
                        }

                        $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,lnkvendorid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:lnkvendorid)";
                        $dao->initCommand($strinventory);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':rawlocid', $resrawLoc['id']);
                        $dao->addParameter(':inventory', $inventory);
                        $dao->addParameter(':rate', $amount);
                        $dao->addParameter(':lnkvendorid', $data['gr_sel_vendor']);
                        $dao->addParameter(':total_rate', $value['item_total_amt']);
                        $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                        $dao->addParameter(':ref_id',$gr_det_id);
                        $dao->addParameter(':invstatus',INV_STATUS_GR);
                        $dao->addParameter(':crdb','db');
                        $dao->addParameter(':createddatetime',$datetime);
                        $dao->addParameter(':created_user',CONFIG_UID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->executeNonQuery();

                        $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                        $total_inventory = $resrawLoc['inventory'] - $inventory ;
                        $totalPrice = $old_price - $value['item_total_amt'];
                        $rateperunit = $totalPrice / $total_inventory;
                        $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                        $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                                                WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                                                and storeid=:storeid";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':inventory', $total_inventory);
                        $dao->addParameter(':rateperunit', $rateperunit);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->executeNonQuery();
                        //Update Inventory//
                    }
                }
                /*Check if all grn items are returned*/
                    $strSql4 = "SELECT COUNT(grndetailunkid) as cnt FROM ".CONFIG_DBN.".cfgrndetail WHERE lnkgrnid=:grn_id ".
                               "AND companyid=:companyid AND storeid=:storeid AND lnkgrdetailid=0";
                    $dao->initCommand($strSql4);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':grn_id', $data['grn_id']);
                    $resObj4 = $dao->executeRow();
                    if($resObj4['cnt']==0){
                        $strSql5 = "UPDATE ".CONFIG_DBN.".cfgrn SET is_returned=1 WHERE grnid=:grn_id AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strSql5);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':grn_id', $data['grn_id']);
                        $dao->executeNonQuery();
                    }
                /*Check if all grn items are returned*/


                return json_encode(array('Success' => 'True', 'Message' => $languageArr->LANG43));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module.' - addGRFromGRN - '.$e);
        }
    }
    public function getGrRecordsByID($data)
    {
        try {
            $this->log->logIt($this->module." - getGrRecordsByID");
            $dao = new \dao();
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $strSql1 = "SELECT CGRN.grnid,CGRN.grn_doc_num,CGRN.voucher_no,IFNULL(CGRN.remarks,'') as remarks,ROUND(CGRN.totalamount,$round_off) as totalamount,
                        CGRN.lnkvendorid,IFNULL(DATE_FORMAT(CGRN.grn_date,'".$mysql_format."'),'') as grn_date
                        FROM ".CONFIG_DBN.".cfgrn AS CGRN WHERE CGRN.grnid=:grnid and CGRN.companyid=:companyid and CGRN.storeid=:storeid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addParameter(':grnid', $data['id']);
            $resObj1 = $dao->executeRow();

            $strSql2 = "SELECT CR.name AS storeitem,CU.name as unit,CU.unit as unit_val,CGRND.lnkgrnid,CGRND.grndetailunkid,CGRND.lnkrawid,CGRND.lnkrawcatid,".
                "CGRND.lnkunitid,ROUND(CGRND.qty,$round_off) as qty,ROUND(CGRND.rate,$round_off) as rate,".
                "CGRND.discount_per,ROUND(CGRND.discount_amount,$round_off) as discount_amount,ROUND(CGRND.tax_amount,$round_off) as tax_amount,".
                "CGRND.tax_description,ROUND(CGRND.total,$round_off) as total FROM ".CONFIG_DBN.".cfgrndetail AS CGRND ".
                "LEFT JOIN ".CONFIG_DBN.".cfrawmaterial AS CR ON CGRND.lnkrawid=CR.id AND CR.companyid=:companyid ".
                "LEFT JOIN ".CONFIG_DBN.".cfunit CU ON CGRND.lnkunitid=CU.unitunkid AND CU.companyid=:companyid AND CU.is_active=1 and CU.is_deleted=0 ".
                "WHERE CGRND.lnkgrnid=:lnkgrnid and CGRND.lnkgrdetailid=0 and CGRND.companyid=:companyid and CGRND.storeid=:storeid";
            $dao->initCommand($strSql2);
            $dao->addParameter(':lnkgrnid',  $data['id']);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $resObj2 = $dao->executeQuery();

            return json_encode(array("Success" => "True", "good_return" => $resObj1, "returnDetail" => $resObj2));
        } catch (Exception $e) {
            $this->log->logIt($this->module." - getGrRecordsByID - ".$e);
            return false;
        }
    }
    public function getAllVendorList()
    {
        try {
            $this->log->logIt($this->module . '-getAllVendorList');
            $dao = new \dao();
            $strSql = "SELECT CFV.vandorunkid, TC.business_name FROM ".CONFIG_DBN.".cfvandor CFV INNER JOIN ".CONFIG_DBN.".trcontact TC ON TC.contactunkid = CFV.lnkcontactid WHERE CFV.companyid=:companyid AND  CFV.is_active=1 AND  CFV.is_deleted=0";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $rec = $dao->executeQuery();
            return $rec;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . '-getAllVendorList -' . $e);
        }
    }
    public function getVendorCategory($data)
    {
        try {
            $this->log->logIt($this->module . ' - getVendorCategory');
            $categories = [];
            if (isset($data['vendorid']) && $data['vendorid'] > 0) {
                $dao = new \dao();
                $dao->initCommand("SELECT DISTINCT lnkcategoryid,CTRC.name AS category_name FROM " . CONFIG_DBN . ".cfrawmaterial CFR
              LEFT JOIN ".CONFIG_DBN.". cfinventorydetail ID ON CFR.id= ID.lnkrawmaterialid AND ID.lnkvendorid=:lnkvandorid
                 INNER JOIN " . CONFIG_DBN . ".cfrawmaterial_category CTRC 
				    ON CTRC.id = CFR.lnkcategoryid AND CTRC.is_deleted=0 AND CTRC.companyid = :companyid
				    WHERE  CFR.companyid = :companyid AND CFR.is_deleted=0 AND CFR.is_active=1 ORDER BY CTRC.name ASC ");
                $dao->addParameter(':lnkvandorid', $data['vendorid']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $categories = $dao->executeQuery();
            }
            return json_encode(array('Success' => 'True', 'Message' => 'Vendor categories', 'categories' => $categories));
        } catch (\Exception $e) {
            $this->log->logIt($this->module . ' - getVendorCategory - ' . $e);
        }
    }
    public function getRawMaterialFromCategory($data)
    {
        try {
            $this->log->logIt($this->module . " - getRawMaterialFromCategory");
            $dao = new \dao();

            $strSql = " SELECT id,name,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial
            WHERE is_active=1 and is_deleted=0 AND lnkcategoryid=:lnkcategoryid AND companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':lnkcategoryid', $data['rawcatid']);
            $res = $dao->executeQuery();

            return json_encode(array("Success" => "True", "Data" => $res));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getRawMaterialFromCategory - " . $e);
            return false;
        }
    }

	public function getAppliedTax()
	{
		try
		{
			$this->log->logIt($this->module.' - getAppliedTax');
			$todaysdate = \util\util::getLocalDate();
			$dao = new \dao();

			$strSql = " SELECT CASE WHEN CFTD.postingrule=1 THEN '%' WHEN CFTD.postingrule=2
                       THEN 'FLAT' END as pytype,CFTD.amount,CFT.taxunkid,CFT.lnkmasterunkid,CFTD.taxdetailunkid,CFT.tax,CFTD.postingrule,CFTD.taxapplyafter
                        FROM ".CONFIG_DBN.".cftax AS CFT
                        INNER JOIN ".CONFIG_DBN.".cftaxdetail CFTD
                          ON CFT.taxunkid = CFTD.taxunkid AND
                             CFTD.taxdetailunkid = (SELECT taxdetailunkid FROM ".CONFIG_DBN.".cftaxdetail WHERE taxunkid=CFT.taxunkid ORDER BY entrydatetime DESC LIMIT 1)
                        WHERE CFT.companyid =:companyid  AND CFT.isactive=1 AND CFT.is_deleted=0 AND CAST(CFTD.taxdate AS DATE)<=:todaysdate ";
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$strSql .= " AND CFT.locationid = :locationid ";
			}

			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$strSql .= " AND CFT.storeid = :storeid ";
			}

			$strSql .=" ORDER BY CFT.taxunkid";
			$dao->initCommand($strSql);
			$dao->addParameter(':todaysdate',$todaysdate);
			$dao->addParameter(':companyid',CONFIG_CID);
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$dao->addparameter(':locationid',CONFIG_LID);
			}

			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$dao->addparameter(':storeid',CONFIG_SID);
			}
			$res = $dao->executeQuery();
			return $res;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module.' - getAppliedTax - '.$e);
		}
	}
    public function getGrnDetailForReturn($data)
    {
        try {
            $this->log->logIt($this->module." - getGrnDetailForReturn");
            $dao = new \dao();
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $strSql1 = "SELECT CGRN.grnid,CGRN.grn_doc_num,CGRN.voucher_no,IFNULL(CGRN.remarks,'') as remarks,ROUND(CGRN.totalamount,$round_off) as totalamount,
                        CGRN.lnkvendorid,IFNULL(DATE_FORMAT(CGRN.grn_date,'".$mysql_format."'),'') as grn_date
                        FROM ".CONFIG_DBN.".cfgrn AS CGRN WHERE CGRN.grnid=:grnid and CGRN.companyid=:companyid and CGRN.storeid=:storeid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addParameter(':grnid', $data['grn_id']);
            $resObj1 = $dao->executeRow();

            $strSql2 = "SELECT CR.name AS storeitem,CU.name as unit,CU.unit as unit_val,CGRND.lnkgrnid,CGRND.grndetailunkid,CGRND.lnkrawid,CGRND.lnkrawcatid,".
                       "CGRND.lnkunitid,ROUND(CGRND.qty,$round_off) as qty,ROUND(CGRND.rate,$round_off) as rate,".
                       "CGRND.discount_per,ROUND(CGRND.discount_amount,$round_off) as discount_amount,ROUND(CGRND.tax_amount,$round_off) as tax_amount,".
                       "CGRND.tax_description,ROUND(CGRND.total,$round_off) as total FROM ".CONFIG_DBN.".cfgrndetail AS CGRND ".
                       "LEFT JOIN ".CONFIG_DBN.".cfrawmaterial AS CR ON CGRND.lnkrawid=CR.id AND CR.companyid=:companyid ".
                       "LEFT JOIN ".CONFIG_DBN.".cfunit CU ON CGRND.lnkunitid=CU.unitunkid AND CU.companyid=:companyid AND CU.is_active=1 and CU.is_deleted=0 ".
                       "WHERE CGRND.lnkgrnid=:lnkgrnid and CGRND.lnkgrdetailid=0 and CGRND.companyid=:companyid and CGRND.storeid=:storeid";
            $dao->initCommand($strSql2);
            $dao->addParameter(':lnkgrnid',  $data['grn_id']);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $resObj2 = $dao->executeQuery();
            return json_encode(array("Success" => "True", "good_receipt" => $resObj1, "grnDetail" => $resObj2));
        } catch (Exception $e) {
            $this->log->logIt($this->module." - getGrnDetailForReturn - ".$e);
            return false;
        }
    }
    public function printGoodsReturn($id,$defaultlanguageArr='')
    {
        try {
            $this->log->logIt($this->module . ' - printGoodsReturn');
            $dao = new \dao();


            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $resarr = array();

            $strSql = "SELECT C.companyname,C.address1 AS company_address,C.phone AS company_phone,
                        C.currency_sign,C.companyemail,C.fax,IFNULL(C.logo,'') as company_logo,
                        TC.name AS vendor_name,TC.address AS vendor_address,TC.mobile AS vendor_mobile,TC.business_name AS Vendor_business,
                        TC.fax AS Vendor_fax,TC.email AS Vendor_email,
                        GR.gr_doc_num AS gr_voucher_num, GR.remarks,GR.voucher_no AS voucher_num,
                         IFNULL(DATE_FORMAT(GR.gr_date,'" . $mysql_format . "'),'')  AS Voucher_Date, 
                        ROUND(IFNULL(GR.totalamount,0),$round_off) as totalamount,
                        VC.countryName AS Company_Country                        
                        FROM " . CONFIG_DBN . ".cfgoodsreturn AS GR                         
                        LEFT JOIN " . CONFIG_DBN . ".cfcompany AS C ON GR.companyid= C.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfvandor AS V ON GR.lnkvendorid=V.vandorunkid AND V.companyid=GR.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".trcontact AS TC ON V.lnkcontactid=TC.contactunkid AND TC.companyid=V.companyid
                        LEFT JOIN " . CONFIG_DBN . ".vwcountry AS VC ON C.country=VC.id                       
                        WHERE GR.companyid=:companyid AND GR.grid=:grid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':grid', $id);
            $grrecords = $dao->executeRow();


            $resarr['company_name'] = $grrecords['companyname'];
            $resarr['address'] = $grrecords['company_address'];
            $resarr['phone'] = $grrecords['company_phone'];
            $resarr['company_logo'] =!empty($grrecords['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$grrecords['company_logo']:'';
            $resarr['email'] = $grrecords['companyemail'];
            $resarr['fax'] = $grrecords['fax'];
            $resarr['Company_Country'] = $grrecords['Company_Country'];
            $resarr['currency_sign'] = $grrecords['currency_sign'];
            $resarr['gr_voucher_num'] = $grrecords['gr_voucher_num'];
            $resarr['voucher_num'] = $grrecords['voucher_num'];
            $resarr['Vendor_business'] = $grrecords['Vendor_business'];
            $resarr['remarks'] = $grrecords['remarks'];
            $resarr['Voucher_Date'] = $grrecords['Voucher_Date'];
            $resarr['totalamount'] = $grrecords['totalamount'];
            $resarr['vendor_name'] = $grrecords['vendor_name'];
            $resarr['print_by'] = CONFIG_UNM;
            $resarr['vendor_mobile'] = $grrecords['vendor_mobile'];
            $resarr['vendor_email'] = $grrecords['Vendor_email'];
            $resarr['current_date'] = \util\util::convertMySqlDate($current_date);

            $strPODetail = "SELECT CR.name AS storeitem,CU.shortcode AS unit,CPD.lnkgrid,CPD.lnkrawid,CPD.lnkrawcatid," .
                "CPD.lnkunitid,ROUND(CPD.qty,$round_off) AS qty,ROUND(CPD.rate,$round_off) AS rate," .
                "CPD.discount,ROUND(CPD.discount_amount,$round_off) AS discount_amount,ROUND(CPD.tax_amount,$round_off) AS tax_amount," .
                "CPD.tax_description,ROUND(CPD.total,$round_off) AS total FROM " . CONFIG_DBN . ".cfgoodsreturndetail AS CPD " .
                "LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CPD.lnkrawid=CR.id " .
                "LEFT JOIN " . CONFIG_DBN . ".cfunit CU ON CPD.lnkunitid=CU.unitunkid AND " .
                "CU.companyid=:companyid AND CU.is_active=1 AND CU.is_deleted=0 " .
                "WHERE CPD.lnkgrid=:lnkgrid AND CPD.companyid=:companyid AND CPD.storeid=:storeid";
            $dao->initCommand($strPODetail);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addparameter(':lnkgrid', $id);
            $grdetail = $dao->executeQuery();

            $taxTOtal = 0;

            if(isset($grdetail)){
                foreach ($grdetail as $key => $value) {
                    $taxname = array();
                    $grtaxdetail = json_decode($value['tax_description'], 1);
                    $taxeslist_dis='';
                    if($grtaxdetail){
                        foreach ($grtaxdetail as $key2 => $value2) {
                            $Tax_NAME='';
                            $posting_type=isset($grrecords['currency_sign'])?$grrecords['currency_sign']:'Flat';
                            if(isset($value2['posting_rule']) && $value2['posting_rule']==1){
                                $posting_type='%';
                            }
                            if(empty($value2['tax_name']))
                            {
                                $str = "SELECT tax FROM " . CONFIG_DBN . ".cftax
                 WHERE  companyid=:companyid AND storeid=:storeid  AND taxunkid=:taxunkid";
                                $dao->initCommand($str);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addParameter(':storeid', CONFIG_SID);
                                $dao->addParameter(':taxunkid', $value2['tax_id']);
                                $qtaxname = $dao->executeRow();
                                $Tax_NAME=$qtaxname['tax'];
                            }
                            else{
                                $Tax_NAME=$value2['tax_name'];
                            }
                            $taxeslist_dis .= "<b>".$Tax_NAME."</b> : (".number_format($value2['tax_value'],$round_off,'.','')." ".$posting_type.") : ".number_format($value2['tax_amt'], $round_off,'.','')." <br>";
                            $taxname[$value2['tax_id']] = $value2['tax_amt'];

                        }
                    }
                    $taxTOtal = ($taxTOtal+$value['tax_amount']);
                    $grdetail[$key]['tax_description'] = isset($taxeslist_dis)?$taxeslist_dis:'';
                }
            }

            $resarr['grdetail'] = $grdetail;
            $resarr['Tax_Total'] =  number_format($taxTOtal,$round_off,'.','');

            $Discounttotal = $Dto = 0;
            foreach ($grdetail AS $val) {

                $to = $val['discount_amount'];
                $Discounttotal += $to;
            }

            $resarr['Discounttotal'] = number_format($Discounttotal, $round_off,'.','');
            $numberinwords = \util\util::convertNumber($grrecords['totalamount'],'','','',$defaultlanguageArr);

            $resarr['numberinwords'] = $numberinwords;

            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - printGoodsReturn - ' . $e);
        }
    }
}

?>

