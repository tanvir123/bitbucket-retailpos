<?php
namespace database;

class raw_categorydao
{
    public $module = 'DB_raw_category';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function addCategory($data,$languageArr,$defaultlanguageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - addCategory');
            $dao = new \dao();
            $datetime=\util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $ObjDependencyDao = new \database\dependencydao();
            $tablename =  "cfrawmaterial_category";
            $arr_log = array(
                'Category Name'=>$data['name'],
                'Sortkey'=>$data['sortkey'],
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive'
            );
            $json_data = html_entity_decode(json_encode($arr_log));

            if($data['id']==0)
            {
                $chk_name = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"name",$data['name']);
                if($chk_name==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG7)));
                }
                /*$chk_scode = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"sortkey",$data['sortkey']);
                if($chk_scode==1)
                {
                    return json_encode(array('Success'=>'False','Message'=>'Sort key already Exists'));
                }*/
                $title = "Add Record";
                $hashkey = \util\util::gethash();

                $strSql = "INSERT INTO ".CONFIG_DBN.".cfrawmaterial_category(`name`, 
                                                                    sortkey, 
                                                                    companyid,
                                                                    storeid,
                                                                    createddatetime, 
                                                                    created_user, 
                                                                    is_active, 
                                                                    hashkey)
                                                            VALUE(  :name, 
                                                                    :sortkey, 
                                                                    :companyid,
                                                                    :storeid, 
                                                                    :createddatetime, 
                                                                    :created_user, 
                                                                    :rdo_status, 
                                                                    :hashkey)";
                
                $dao->initCommand($strSql);
                $dao->addParameter(':name',$data['name']);
                $dao->addParameter(':sortkey',$data['sortkey']);
                $dao->addParameter(':rdo_status',$data['rdo_status']);
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':created_user',CONFIG_UID);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':storeid',CONFIG_SID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_ADD_SUC)));
           }
           else
           {
               $id =$ObjCommonDao->getprimaryBycompany('cfrawmaterial_category',$data['id'],'id');

               $chk_name = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"name",$data['name'],$id);
               if($chk_name==1)
               {
                   return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG7)));
               }
              /* $chk_scode = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"sortkey",$data['sortkey'],$id);
               if($chk_scode==1)
               {
                   return json_encode(array('Success'=>'False','Message'=>'Sort key already Exists'));
               }*/

                $title = "Edit Record";
                $strSql = "UPDATE ".CONFIG_DBN.".cfrawmaterial_category SET name=:name,
                                                                    sortkey=:sortkey,
                                                                    modifieddatetime=:modifieddatetime,
                                                                    modified_user=:modified_user
                                                                    WHERE hashkey=:id AND companyid=:companyid";
                $dao->initCommand($strSql);
                $dao->addParameter(':id',$data['id']);
                $dao->addParameter(':name',$data['name']);
                $dao->addParameter(':sortkey',$data['sortkey']);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_UP_SUC)));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addCategory - '.$e);
        }
    }

    public function categorylist($limit,$offset,$name,$isactive=0)
    {
        try
        {
            $this->log->logIt($this->module.' - raw-categorylist - '.$name);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT CRC.name,CRC.hashkey,CRC.sortkey,CRC.is_active,IFNULL(CFU1.username,'') AS createduser,
                       IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CRC.createddatetime,'".$mysqlformat."'),'') as created_date,
                       IFNULL(DATE_FORMAT(CRC.modifieddatetime,'".$mysqlformat."'),'') as modified_date
                       FROM ".CONFIG_DBN.".cfrawmaterial_category AS CRC
                       LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CRC.created_user=CFU1.userunkid AND CRC.companyid=CFU1.companyid
                       LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON CRC.modified_user=CFU2.userunkid AND CRC.modified_user=CFU2.userunkid AND CRC.companyid=CFU2.companyid
                       WHERE CRC.companyid=:companyid AND CRC.is_deleted=0";

            if($name!="")
                $strSql .= " AND name LIKE '%".$name."%'";
            if($isactive==1)
                $strSql .= " AND is_active=1";
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $rec = $dao->executeQuery();

            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue));
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - raw-categorylist - '.$e);
        }
    }

    public function getrelatedRecords($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getRelatedRecords');
            $dao = new \dao();
            $result = array();
            $ObjCommonDao = new \database\commondao();
            if($data['id']!="")
            {
                $id = $ObjCommonDao->getprimaryBycompany('cfrawmaterial_category',$data['id'],'id');

                //Check Rawmaterial
                $strSql = "SELECT GROUP_CONCAT(CR.name) AS subject FROM ".CONFIG_DBN.".cfrawmaterial CR
                            INNER JOIN ".CONFIG_DBN.".cfrawmaterial_category CRC ON CRC.id=CR.lnkcategoryid
                            WHERE CRC.id =:id AND CRC.is_deleted=0 AND CR.is_deleted=0 
                            AND CRC.companyid=:companyid and CR.companyid=:companyid 
                            ORDER BY CR.id";

                $dao->initCommand($strSql);
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addParameter(":id",$id);
                $resobj = $dao->executeRow();
                $arr_item = array();
                if($resobj['subject']!=""){
                    $arr_item['subject'] = $resobj['subject'];
                    $arr_item['module'] = \common\staticarray::$getmodulename['cfrawmaterial']['Display_Name'];
                    $result[] = $arr_item;
                }

                return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$result)));
            }
            else{
                return html_entity_decode(json_encode(array("Success"=>"False","Data"=>"")));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getRelatedRecords - '.$e);
        }
    }
    public function getCategoryRec($data)
    {
        try
        {
            $this->log->logIt($this->module." - getCategoryRec");
            $dao = new \dao();
            $id=$data['id'];
            $strSql = "SELECT name,hashkey,sortkey,is_active FROM ".CONFIG_DBN.".cfrawmaterial_category WHERE hashkey=:id AND companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':id',$id);
            $dao->addParameter(':companyid',CONFIG_CID);
            $res = $dao->executeRow();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getCategoryRec - ".$e);
            return false;
        }
    }
    public function getAllCategory($get=0)
    {
        try
        {
            $this->log->logIt($this->module." - getAllCategory");
            $dao = new \dao();

            if($get==1){
                $strSql = "SELECT keyvalue FROM ".CONFIG_DBN.".cfparameter WHERE keyname = 'digitafterdecimal' AND companyid = ".CONFIG_CID." AND storeid=".CONFIG_SID." AND  type=2 ";
                $dao->initCommand($strSql);
                $round_off=$dao->executeRow();

                $round_off=isset($round_off['keyvalue'])?$round_off['keyvalue']:1;
            }

            $strSql = "SELECT name,id FROM ".CONFIG_DBN.".cfrawmaterial_category WHERE companyid=:companyid
             and is_deleted=0 and is_active=1";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $res = $dao->executeQuery();

            $result=array();
            if($res && $get==1){
                $result['round_off']=isset($round_off)?$round_off:1;
                $result['category_list']=$res;
            }else{
                $result=$res;
            }
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$result)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getAllCategory - ".$e);
            return false;
        }
    }
}
?>