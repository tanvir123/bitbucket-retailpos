<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 4/8/17
 * Time: 12:13 PM
 */

namespace database;
class orderlogdao
{
    public $module = 'DB_orderlogdao';
    public $log;
    public $dbconnect;
    private $lang_arr, $lang_activity;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('orders_logs');
        $this->default_lang_arr = new \util\language('orders_operations');
        $this->loadLanguage();
    }



    public function addOrderLog($fid,$operation,$data,$invno="",$orderdata="",$tbledata="")
    {
        try
        {
            $this->log->logIt($this->module.'-addOrderLog');
            $datetime = \util\util::getLocalDateTime();
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $userip = \util\util::VisitorIP();
            $arr_detail = array();

            if($invno!="")
            {
                $invno = $invno;
            }


            if($orderdata!="")
            {
                $orderdata = $orderdata;
            }

            if($tbledata!="")
            {
                $tbledata = $tbledata;
            }


            if($operation=='ADD_ORDER')
            {

                if(isset($orderdata) && $orderdata!='')
                {
                    $arr_detail['FOLIO_NO'] = $invno;
                    $arr_detail['NAME'] = $orderdata['full_name'];
                    $arr_detail['TABLE_NAME'] = $tbledata;
                    $arr_detail['DESCRIPTION'] = "Order From tabinsta";

                }
                else {

                    $str = "SELECT tablename FROM " . CONFIG_DBN . ".cfclasstable WHERE tableunkid=:tableunkid AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($str);
                    $dao->addParameter(':tableunkid', $data['info_tableid']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':locationid', CONFIG_LID);
                    $table = $dao->executeRow();
                    $arr_detail['FOLIO_NO'] = $invno;
                    $arr_detail['NAME'] = $data['salutation'] . " " . $data['info_name'];
                    $arr_detail['TABLE_NAME'] = $table['tablename'];
                }
            }

            if($operation=='ADD_ITEM')
            {
                $str= "SELECT itemname,sale_amount FROM ".CONFIG_DBN.".cfmenu_items WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($str);
                $dao->addParameter(':hashkey',$data['item']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addParameter(':locationid',CONFIG_LID);
                $item = $dao->executeRow();


                if($data['select_discount']>0)
                {
                    $str = "SELECT discount,postingrule,value as disvlue FROM ".CONFIG_DBN.".cfdiscount WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($str);
                    $dao->addParameter(':hashkey',$data['select_discount']);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addParameter(':locationid',CONFIG_LID);
                    $discount = $dao->executeRow();
                    $discname = $discount['discount'];
                    if($discount['postingrule'] == 1)
                    {
                        $disamt = $discount['disvlue'];
                    }
                    else
                    {
                        $disamt = $data['discount'];
                    }
                }
                else{
                    $discname = '';
                    $disamt = '';

                }
                $arr_detail['ITEM_NAME'] = $item['itemname'];
                $arr_detail['AMOUNT'] = $item['sale_amount'];
                $arr_detail['QUANTITY'] = $data['quantity'];
                $arr_detail['DISCOUNT_NAME'] = $discname;
                $arr_detail['DISCOUNT_PRICE'] = $disamt;
            }

            if($operation=='CLOSE_FOLIO')
            {
                $str  = "SELECT ffm.foliounkid,trcntct.name,trcntct.salutation,ffm.lnkcontactid, ffm.foliono,ffm.lnkcontactid FROM ".CONFIG_DBN.". fasfoliomaster as ffm
                 LEFT JOIN ".CONFIG_DBN.".trcontact as trcntct ON trcntct.contactunkid=ffm.lnkcontactid
                 WHERE ffm.hashkey=:hashkey AND ffm.companyid=:companyid AND ffm.locationid=:locationid ";
                $dao->initCommand($str);
                $dao->addParameter(':hashkey',$data['chrgehash']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addParameter(':locationid',CONFIG_LID);
                $detail= $dao->executeRow();
                $fid = $detail['foliounkid'];

                $arr_detail['FOLIO_NO'] = $detail['foliono'];
                $arr_detail['NAME'] = $detail['salutation'] . " " . $detail['name'];

            }


            if($operation=='POST_TO_PMS_FOLIO')
            {
                $str  = "SELECT ffm.foliounkid,trcntct.name,trcntct.salutation,ffm.lnkcontactid, ffm.foliono,ffm.lnkcontactid FROM ".CONFIG_DBN.". fasfoliomaster as ffm
                 LEFT JOIN ".CONFIG_DBN.".trcontact as trcntct ON trcntct.contactunkid=ffm.lnkcontactid
                 WHERE ffm.hashkey=:hashkey AND ffm.companyid=:companyid AND ffm.locationid=:locationid ";
                $dao->initCommand($str);
                $dao->addParameter(':hashkey',$data['chrgehash']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addParameter(':locationid',CONFIG_LID);
                $detail= $dao->executeRow();
                $fid = $detail['foliounkid'];

                $arr_detail['FOLIO_NO'] = $detail['foliono'];
                $arr_detail['NAME'] = $detail['salutation'] . " " . $detail['name'];

            }

            if($operation=='SPLIT_TO')
            {

                $arr_detail['FOLIO_NO'] = $data;
            }



            if($operation=='MERGE_FROM')
            {
                $arr_detail['FOLIO_NO'] = $data['foliono'];

            }

            if($operation=='SPIT_INVOICE')
            {
                $str = "SELECT foliono FROM ".CONFIG_DBN.".fasfoliomaster WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid ";
                $dao->initCommand($str);
                $dao->addParameter(':hashkey',$data['chrgehash']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addParameter(':locationid',CONFIG_LID);
                $folio = $dao->executeRow();

                $arr_detail['NAME'] = $data['salutation']." ".$data['info_name'];
                $arr_detail['FOLIO_NO'] = $invno;
                $arr_detail['SPLIT_INVOICE_FROM'] = $folio['foliono'];
            }


            if($arr_detail!='' && count($arr_detail)>0) {
                $arr_json = json_encode($arr_detail);
            }else{
                $arr_json = '';
            }
            $strSql = "INSERT INTO ".CONFIG_DBN.". cfauditlog SET
                       lnkfolioid=:lnkfolioid,operation=:operation,description=:description,
                       userid=:userid,user_ip=:user_ip,username=:username,datetime=:datetime,
                       companyid=:companyid,locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':lnkfolioid',$fid);
            $dao->addParameter(':operation',$operation);
            $dao->addParameter(':description',$arr_json);
            $dao->addParameter(':userid',CONFIG_UID);
            $dao->addParameter(':user_ip',$userip);
            $dao->addParameter(':username',CONFIG_UNM);
            $dao->addParameter(':datetime',$datetime);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',CONFIG_LID);
            $dao->executeNonQuery();
            return 1;

        }
        catch(\Exception $e)
        {
            $this->log->logIt($this->module.'-addOrderLog-'.$e);
        }
    }



    public function listOrderLogs($fid)
    {
        try
        {
            $this->log->logIt($this->module.' - listOrderLogs');
            $dao = new \dao();

            $audit_log_arr = $this->lang_arr;
           // $activity_arr = $this->lang_activity;


            $mysql_format =  \common\staticarray::$mysqldateformat[\database\parameter::getParameter('dateformat')];
            $time_format = \common\staticarray::$mysqltimeformat[\database\parameter::getParameter('timeformat')];
            $strSql = "SELECT lnkfolioid,operation,description,user_ip,username,DATE_FORMAT(datetime,'".$mysql_format."') AS date,
                       TIME_FORMAT(datetime,'".$time_format."') AS time FROM ".CONFIG_DBN.".cfauditlog 
                       WHERE lnkfolioid=:folioid AND companyid=:companyid AND locationid=:locationid ORDER BY datetime DESC";
            $dao->initCommand($strSql);
            $dao->addParameter(':folioid',$fid);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',CONFIG_LID);
            $resObj = $dao->executeQuery();

            foreach($resObj as $key=>$value){


                $resObj[$key]['activity'] = $value['operation'];

                $new_arr = array();
                if($value['description']!=''){
                    $arr_desc = json_decode($value['description'],1);
                    foreach($arr_desc as $key1=>$value1){
                        $new_key = $audit_log_arr[$key1];
                        $new_arr[$new_key] = $value1;
                    }
                }
                $resObj[$key]['info'] = $new_arr;
            }
            return json_encode(array("Success"=>"True","Data"=>$resObj));
            //return $resObj;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - listOrderLogs');
        }
    }

    public function loadLanguage()
    {
        try
        {
            $this->log->logIt($this->module.'-loadLanguage');
            $arr1 = array(
                "KOT_NO" => "KOT No",
                "FOLIO_NO" => "Folio No",
                "NAME" => "Name",
                "CLASS_NAME" => "Table Class",
                "TABLE_NAME" => "Table Name",
                "ITEM_NAME" => "Item Name",
                "AMOUNT" => "Amount",
                "QUANTITY" => "Quantity",
                "DISCOUNT_NAME" => "Discount Name",
                "DISCOUNT_PRICE" => "Discount Price",
                "SPLIT_INVOICE_FROM" => "Split Invoice From",
                "Detail" => "Detail",
                "DESCRIPTION" => "Description",
                "MERGE_FROM" => "Merge From Folios",
            );
            $arr2 = array(
                "ADD_ORDER" => "Add Order",
                "ADD_ITEM" => "Add Item",
                "CLOSE_FOLIO" => "Close Folio",
                "SPLIT_TO" => "Split To",
                "MERGE_FROM" => "Merge From",
                "MERGE_ORDER" => "Merge Order",
                "SPIT_INVOICE" => "Split Invoice",
                "SPLIT_BY_FOLIO" => "Split By Folio",
                "SPLIT_BY_NAME" => "Split By Name",
                "CREATE_FOLIO_FROM_SPLIT" => "Folio created on split",
                "TRANSFER_KOT" => "Order Transfer",
                "KOT_ITEM_TRANSFER" => "Item Transfer",
            );
            $this->lang_arr = $this->language->loadlanguage($arr1);
            $this->lang_activity = $this->language->loadlanguage($arr2);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-loadLanguage -'.$e);
        }
    }
}
    ?>