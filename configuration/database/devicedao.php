<?php
namespace database;

class devicedao
{
    public $module = 'DB_devicedao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function adddevice($data)
    {
        try
        {
            $this->log->logIt($this->module.' - adddevice');

            $dao = new \dao();


            $ObjAuditDao = new \database\auditlogdao();
            $tablename =  "cfdevice";

            $datetime=\util\util::getLocalDateTime();

            $ObjCommonDao = new \database\commondao();

            $ObjDependencyDao = new \database\dependencydao();

            $arr_log = array(
                'Device Name'=>$data['devicename'],
                'Type'=>$data['type'],
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive',
            );

            $json_data = json_encode($arr_log);

            if($data['id']=='0' || $data['id']==0)
            {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"devicename",$data['devicename']);

                if($chk_name==1){
                    return json_encode(array('Success'=>'False','Message'=>'Device already Exists'));
                }
                $title = "Add Record";
                $hashkey = \util\util::gethash();

                $strSql1 = "INSERT INTO ".CONFIG_DBN.".cfdevice SET 
                            type=:type,
                            devicename=:devicename,
                            is_active=:is_active,
                            createddatetime=:createddatetime,
                            created_user=:created_user,
                            companyid=:companyid,
                            locationid=:locationid,
                            hashkey=:hashkey";
                $dao->initCommand($strSql1);
                $dao->addparameter(':type',$data['type']);
                $dao->addparameter(':devicename',$data['devicename']);
                $dao->addparameter(':is_active',$data['rdo_status']);
                $dao->addparameter(':createddatetime',$datetime);
                $dao->addparameter(':created_user',CONFIG_UID);
                $dao->addparameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->addparameter(':hashkey',$hashkey);
                //$dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                return json_encode(array('Success'=>'True','Message'=>'Device saved successfully'));
            }
            else
            {
                $id = $ObjCommonDao->getprimarykey('cfdevice',$data['id'],'deviceunkid');
$this->log->logIt($id);
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"devicename",$data['devicename'],$id);
                $this->log->logIt($chk_name);
                if($chk_name==1){
                    return json_encode(array('Success'=>'False','Message'=>'Device already Exists'));
                }

                $title = "Edit Record";
                $strSql = "UPDATE ".CONFIG_DBN.".cfdevice SET
                 type=:type,
                 devicename=:devicename,
                 is_active=:is_active,
                 modifieddatetime=:modifieddatetime,
                 modified_user=:modified_user
                 WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid";

                $dao->initCommand($strSql);
                $dao->addparameter(':type',$data['type']);
                $dao->addparameter(':devicename',$data['devicename']);
                $dao->addparameter(':is_active',$data['rdo_status']);;
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':hashkey',$data['id']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                return json_encode(array('Success'=>'True','Message'=>'Device updated successfully'));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - adddevice - '.$e);
            return 0;
        }
    }
    public function devicelist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - devicelist - '.$limit.' - '.$offset.' - '.$name);
            $dao = new \dao();
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT CD.*,IFNULL(CFU1.username,'') AS created_user,IFNULL(CFU2.username,'') AS modified_user,
       (CASE when CD.type = 1 then 'KOT' WHEN CD.type =2 THEN 'Receipt' END) AS TYPE,
                        IFNULL(DATE_FORMAT(CD.createddatetime,'".$mysqlformat."'),'') AS created_date,
                        IFNULL(DATE_FORMAT(CD.modifieddatetime,'".$mysqlformat."'),'') AS modified_date
                        FROM ".CONFIG_DBN.".cfdevice AS CD
                        
                        LEFT JOIN ".CONFIG_DBN.".cfuser AS CFU1 ON CD.created_user=CFU1.userunkid AND CD.companyid=CFU1.companyid
                        LEFT JOIN ".CONFIG_DBN.".cfuser AS CFU2 ON CD.modified_user=CFU2.userunkid AND CD.modified_user=CFU2.userunkid AND CD.companyid=CFU2.companyid
                        WHERE CD.companyid=:companyid AND CD.locationid=:locationid AND CD.is_deleted=0";
            if($name!="")
            {
                $strSql .= " and CD.devicename LIKE '%".$name."%' ";
            }
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',CONFIG_LID);
            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',CONFIG_LID);
            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue),ENT_QUOTES);
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return html_entity_decode(json_encode($retvalue),ENT_QUOTES);
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - devicelist - '.$e);
        }
    }
    public function editdevicelist($data)
    {
        try
        {
            $this->log->logIt($this->module." - editdevicelist");
            $dao = new \dao();
            $id=(isset($data['id']))?$data['id']:"";
            $strSql = " SELECT devicename,type,is_active FROM ".CONFIG_DBN.".cfdevice
  WHERE hashkey=:deviceunkid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->addParameter(':deviceunkid',$id);
            $res = $dao->executeRow();

            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)),ENT_QUOTES);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - editdevicelist - ".$e);
            return false;
        }
    }


    public function countrylist()
    {
        try
        {
            $this->log->logIt($this->module.' - countrylist');

            $dao = new \dao();
            $strSql = "SELECT * FROM ".CONFIG_DBN.".vwcountry ORDER BY countryCode ASC";
            $dao->initCommand($strSql);
            $cntdata = $dao->executeQuery();

            $data = $dao->executeQuery();
            if(count($data)==0)
            {
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return html_entity_decode(json_encode($retvalue),ENT_QUOTES);
            }
            else
            {
                return json_encode(array(array("cnt"=>count($cntdata),"data"=>$data)));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - countrylist - '.$e);
        }
    }


}
?>