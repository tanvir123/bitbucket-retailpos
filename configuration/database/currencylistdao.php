<?php
namespace database;

class currencylistdao
{
    public $module = 'DB_currencylist';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addcurrency($data,$default_langlist='')
    {
        try
        {
            $this->log->logIt($this->module.' - addcurrency');
            $dao = new \dao();
            $strSql2 = "SELECT countryName FROM ".CONFIG_DBN.".vwcountry where id=:selectcountry";
            $dao->initCommand($strSql2);
            $dao->addParameter(':selectcountry',$data['countryId']);
            $res1=$dao->executeRow();
            $ObjAuditDao = new \database\auditlogdao();
            $tablename =  "cfcurrencylist";
            $datetime=\util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjDependencyDao = new \database\dependencydao();

            $arr_log = array(
                'Currency Code'=>$data['currencyCode'],
                'Country Name'=>$res1['countryName'],
                'Exchange Rate'=>$data['currencyRate'],
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive',
            );

            $json_data = json_encode($arr_log);
            if($data['id']=='0' || $data['id']==0)
            {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"countryid",$data['selectcountry']);
                if($chk_name==1){
                    return json_encode(array('Success'=>'False','Message'=>$default_langlist->RECORD_ALREADY_EXISTS));
                }
                $title = "Add Record";
                $hashkey = \util\util::gethash();
                $strSql1 = "INSERT INTO ".CONFIG_DBN.".cfcurrencylist SET 
                            countryid=:countryid,currency_code=:currency_code,currency_sign=:currency_sign,
                            currency_sign_pos=:currency_sign_pos,currency_rate=:currency_rate,
                            is_active=:is_active,createddatetime=:createddatetime,created_user=:created_user,
                            currency_is_default=:currency_is_default,companyid=:companyid,locationid=:locationid,hashkey=:hashkey,qb_currencyaccountid=:qb_currencyaccountid,qb_currencyaccountname=:qb_currencyaccountname";
                $dao->initCommand($strSql1);
                $dao->addparameter(':countryid',$data['selectcountry']);
                $dao->addparameter(':currency_code',$data['code']);
                $dao->addparameter(':currency_sign',$data['symbol']);
                $dao->addparameter(':currency_sign_pos',$data['symbol_pos']);
                $dao->addparameter(':currency_rate',$data['rate']);
                $dao->addparameter(':is_active',$data['rdo_status']);
                $dao->addparameter(':createddatetime',$datetime);
                $dao->addparameter(':created_user',CONFIG_UID);
                $dao->addparameter(':currency_is_default',0);
                $dao->addparameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->addparameter(':hashkey',$hashkey);
                $dao->addparameter(':qb_currencyaccountid',$data['qb_currencyaccountid']);
                $dao->addparameter(':qb_currencyaccountname',$data['qb_currencyaccountname']);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                return json_encode(array('Success'=>'True','Message'=>$default_langlist->REC_ADD_SUC));
            }
            else
            {
                $id = $ObjCommonDao->getprimarykey('cfcurrencylist',$data['id'],'currencyunkid');
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"countryid",$data['selectcountry'],$id);
                if($chk_name==1){
                    return json_encode(array('Success'=>'False','Message'=>$default_langlist->RECORD_ALREADY_EXISTS));
                }

                $strSql = "UPDATE ".CONFIG_DBN.".cflocation SET currency_sign=:currency_sign WHERE currencylnkid=:currencylnkid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->addParameter(':currencylnkid',$id);
                $dao->addParameter(':currency_sign',$data['symbol']);
                $dao->executeNonQuery();


                $title = "Edit Record";
                $strSql = "UPDATE ".CONFIG_DBN.".cfcurrencylist SET ";
                if($data['isdefault']==0){
                    $strSql .=" countryid=:countryid,currency_code=:currency_code,currency_rate=:currency_rate, ";
                }
                $strSql .= "currency_sign=:currency_sign,currency_sign_pos=:currency_sign_pos,
                            modifieddatetime=:modifieddatetime,modified_user=:modified_user,qb_currencyaccountid=:qb_currencyaccountid,qb_currencyaccountname=:qb_currencyaccountname
                            WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid";

                $dao->initCommand($strSql);
                if($data['isdefault']==0){
                    $dao->addparameter(':countryid',$data['selectcountry']);
                    $dao->addparameter(':currency_code',$data['code']);
                    $dao->addparameter(':currency_rate',$data['rate']);
                }
                $dao->addparameter(':currency_sign',$data['symbol']);
                $dao->addparameter(':currency_sign_pos',$data['symbol_pos']);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':hashkey',$data['id']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->addparameter(':qb_currencyaccountid',$data['qb_currencyaccountid']);
                $dao->addparameter(':qb_currencyaccountname',$data['qb_currencyaccountname']);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                return json_encode(array('Success'=>'True','Message'=>$default_langlist->REC_UP_SUC));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addcurrency - '.$e);
            return 0;
        }
    }
    public function currencylist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - currencylist - '.$limit.' - '.$offset.' - '.$name);
            $dao = new \dao();
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT CCL.*,IFNULL(CFU1.username,'') AS created_user,IFNULL(CFU2.username,'') AS modified_user,VWC.countryName,
                        IFNULL(DATE_FORMAT(CCL.createddatetime,'".$mysqlformat."'),'') AS created_date,
                        IFNULL(DATE_FORMAT(CCL.modifieddatetime,'".$mysqlformat."'),'') AS modified_date
                        FROM ".CONFIG_DBN.".cfcurrencylist AS CCL
                        INNER JOIN ".CONFIG_DBN.".vwcountry AS VWC ON CCL.countryid=VWC.id
                        LEFT JOIN ".CONFIG_DBN.".cfuser AS CFU1 ON CCL.created_user=CFU1.userunkid AND CCL.companyid=CFU1.companyid
                        LEFT JOIN ".CONFIG_DBN.".cfuser AS CFU2 ON CCL.modified_user=CFU2.userunkid AND CCL.modified_user=CFU2.userunkid AND CCL.companyid=CFU2.companyid
                        WHERE CCL.companyid=:companyid AND CCL.locationid=:locationid AND CCL.is_deleted=0";
            if($name!="")
            {
                $strSql .= " and VWC.countryName LIKE '%".$name."%' ";
            }
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',CONFIG_LID);
            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',CONFIG_LID);
            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue));
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - currencylist - '.$e);
        }
    }
    public function editcurrencylist($data)
    {
        try
        {
            $this->log->logIt($this->module." - editcurrencylist");
            $dao = new \dao();
            $id=(isset($data['id']))?$data['id']:"";
            $strSql = " SELECT countryid,currency_code,currency_sign,currency_sign_pos,currency_rate,is_active,currency_is_default,qb_currencyaccountid,qb_currencyaccountname FROM ".CONFIG_DBN.".cfcurrencylist WHERE hashkey=:currencyunkid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->addParameter(':currencyunkid',$id);
            $res = $dao->executeRow();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - editcurrencylist - ".$e);
            return false;
        }
    }


    public function countrylist()
    {
        try
        {
            $this->log->logIt($this->module.' - countrylist');

            $dao = new \dao();
            $strSql = "SELECT * FROM ".CONFIG_DBN.".vwcountry ORDER BY countryCode ASC";
            $dao->initCommand($strSql);
            $cntdata = $dao->executeQuery();

            $data = $dao->executeQuery();
            if(count($data)==0)
            {
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return html_entity_decode(json_encode($retvalue));
            }
            else
            {
                return json_encode(array(array("cnt"=>count($cntdata),"data"=>$data)));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - countrylist - '.$e);
        }
    }


    public function getbasecurrency()
    {
        try
        {
            $this->log->logIt($this->module.' - getbasecurrency');
            $dao = new \dao();
            $strSql = "SELECT countryid,currency_code,currency_sign,currency_sign_pos,currency_rate,is_active FROM ".CONFIG_DBN.".cfcurrencylist WHERE currency_is_default=:currency_is_default AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':currency_is_default',1);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $data = $dao->executeRow();
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getbasecurrency - '.$e);
        }
    }
}
?>