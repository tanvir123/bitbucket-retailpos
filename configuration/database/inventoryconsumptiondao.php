<?php

namespace database;

use function PHPSTORM_META\type;
	
	class inventoryconsumptiondao
{
    public $module = 'DB_inventoryconsumptiondao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }
	
	public function storeWiseRawMaterials($data)
	{
		try{
			$this->log->logIt($this->module.' - storeWiseRawMaterials');
			$dao = new \dao();
			$ObjUtil = new \util\util;
			$dateformat = \database\parameter::getParameter('dateformat');
			$cudate = date($dateformat);
			$currentDate = $ObjUtil->convertDateToMySql($cudate);;
			$stock_date = $data['date'];
			
			$round_off = \database\parameter::getParameter('digitafterdecimal');
			
			if($stock_date < $currentDate)
			{
				$strSql = " SELECT RMA.computer_stock,'' as physical_stock,'' as variances, '' as reason,RMA.inv_unitid,RMA.audit_date, RMA.created_at, CFR.name,CFR.code, CFU.name as unit_name, RMA.inv_unitid,RMA.lnkrawmaterialid as inventory_id  FROM  ".CONFIG_DBN.".rawmaterial_audit RMA INNER JOIN ".CONFIG_DBN.".cfrawmaterial CFR ON CFR.id = RMA.lnkrawmaterialid INNER JOIN ".CONFIG_DBN.".cfunit CFU ON CFU.unitunkid = RMA.inv_unitid  WHERE RMA.companyid = :companyid AND RMA.storeid = :storeid AND RMA.audit_date = :audit_date AND RMA.is_deleted = 0 ";
				
			}else{
				
				$strSql = "SELECT   CFRL.inventory as computer_stock,CFRL.lnkrawmaterialid as inventory_id,CFR.name,CFR.code, CFU.name as unit_name,CFRL.inv_unitid , '' as physical_stock, '' as reason , '' as variances FROM ".CONFIG_DBN.".cfrawmaterial_loc CFRL INNER JOIN ".CONFIG_DBN.".cfrawmaterial CFR ON CFR.id = CFRL.lnkrawmaterialid INNER JOIN ".CONFIG_DBN.".cfunit CFU ON CFU.unitunkid = CFRL.inv_unitid LEFT JOIN ".CONFIG_DBN.".rawmaterial_audit RMA ON RMA.lnkrawmaterialid = CFRL.lnkrawmaterialid AND audit_date =:audit_date AND RMA.companyid = :companyid AND RMA.storeid = :storeid WHERE CFRL.is_deleted = 0 AND CFRL.companyid = :companyid AND CFRL.storeid = :storeid ";
			}
			
			if(isset($data['category']) && $data['category'] > 0)
			{
				$strSql .= " AND CFR.lnkcategoryid = :lnkcategoryid ";
			}
			if(isset($data['itemname']) && $data['itemname'] != '')
			{
				$strSql .= " AND CFR.name LIKE '%".$data['itemname']."%' ";
			}
			
			$dao->initCommand($strSql);
			$dao->addParameter(':companyid',CONFIG_CID);
			$dao->addParameter(':storeid',CONFIG_SID);
			$dao->addParameter(':audit_date',$stock_date);
			if(isset($data['category']) && $data['category'] > 0)
			{
				$dao->addParameter(':lnkcategoryid',$data['category']);
			}
			
			
			$data = $dao->executeQuery();
			if(!$data || empty($data))
			{
				$data = [];
			}
			return $data;
			
		}catch (\Exception $e)
		{
			$this->log->logIt($this->module.' - storeWiseRawMaterials - '.$e->getMessage().' ==>'.$e->getLine().'===>'.$e->getFile().'===>'.$e->getCode());
		}
	}
	
	public function updatePhysicalInventory($data,$languageArr='')
    {
        try{
			$this->log->logIt($this->module.' - updatePhysicalInventory');
			$dao = new \dao();
			$datetime=\util\util::getLocalDateTime();
			$ObjUtil = new \util\util;
			$localdate = $ObjUtil->getLocalDate();
			$dateformat = \database\parameter::getParameter('dateformat');
			$cudate = date($dateformat,strtotime($localdate));
			//$dao->bindTransaction();
			if(isset($data['items']) && count($data['items']) > 0)
            {
                $bulkSql = '';
                $i = 0;
                
                foreach($data['items'] as $key => $item)
				{
                	if($item['physical_stock'] == '')
					{
						continue;
					}
					
					
					$round_off = \database\parameter::getParameter('digitafterdecimal');
					
					
					$dao->initCommand("SELECT * FROM ".CONFIG_DBN.".rawmaterial_audit  WHERE companyid = :companyid AND storeid = :storeid AND lnkrawmaterialid = :lnkrawmaterialid AND audit_date = :audit_date AND inv_unitid = :inv_unitid  ");
					//$tempDate = date($dateformat,strtotime($data['current_date']));
					$tempDate = $data['current_date'];
					$currentDatemain = $ObjUtil->convertDateToMySql($tempDate);
					
					$dao->addParameter(':companyid',CONFIG_CID);
					$dao->addParameter(':storeid',CONFIG_SID);
					$dao->addParameter(':inv_unitid',$item['inv_unitid']);
					$dao->addParameter(':audit_date',$currentDatemain);
					$dao->addParameter(':lnkrawmaterialid',$item['inventory_id']);
					$checkRow = $dao->executeRow();
					
					$new_stock = $computer_stock = $item['computer_stock'];
					
					if($checkRow)
					{
						$new_stock = $checkRow['computer_stock'] - $item['physical_stock'];
						$bulkSql .=" UPDATE  ".CONFIG_DBN.".rawmaterial_audit SET computer_stock = '".$new_stock."' wHERE id = '".$checkRow['id']."'; ";
						$computer_stock = $checkRow['computer_stock'];
					}else{
						$new_stock = $item['computer_stock'] - $item['physical_stock'];
						$computer_stock = $item['computer_stock'];
						$bulkSql .= " INSERT INTO  ".CONFIG_DBN.".rawmaterial_audit (companyid,storeid,inv_unitid,audit_date,lnkrawmaterialid,system_computer_stock,computer_stock,created_at,created_by) VALUES ('".CONFIG_CID."','".CONFIG_SID."','".$item['inv_unitid']."','".$currentDatemain."','".$item['inventory_id']."','".$computer_stock."','".$new_stock."','".$datetime."','".CONFIG_UID."'); ";
					}
					//$tempDate = date($dateformat,strtotime($data['current_date']));
					$tempDate = $data['current_date'];
					$cudatestrotime = strtotime($ObjUtil->convertDateToMySql($tempDate));
					$tempDate = date($dateformat,strtotime($cudate));
					$realdatestrotime = strtotime($ObjUtil->convertDateToMySql($tempDate));
					$difference = -$item['physical_stock'];
					
					if($cudatestrotime<$realdatestrotime)
					{
						
						$loopdate = date('Y-m-d',strtotime("+1 day",$cudatestrotime));
						
						while (strtotime($loopdate) <= $realdatestrotime) {
							
							$dao->initCommand("SELECT * FROM ".CONFIG_DBN.".rawmaterial_audit  WHERE companyid = :companyid AND storeid = :storeid AND lnkrawmaterialid = :lnkrawmaterialid AND audit_date = :audit_date AND inv_unitid = :inv_unitid  ");
							
							$dateformat = \database\parameter::getParameter('dateformat');
							$cudate = date($dateformat,strtotime($loopdate));
							$currentDate = $ObjUtil->convertDateToMySql($cudate);
							
							$dao->addParameter(':companyid',CONFIG_CID);
							$dao->addParameter(':storeid',CONFIG_SID);
							$dao->addParameter(':inv_unitid',$item['inv_unitid']);
							$dao->addParameter(':audit_date',$currentDate);
							$dao->addParameter(':lnkrawmaterialid',$item['inventory_id']);
							$checkRow = $dao->executeRow();
							$old_computer_stock = 0;
							if($checkRow)
							{
								$new_computer_stock = $checkRow['computer_stock'] + $difference;
								$old_computer_stock = $checkRow['computer_stock'];
								$bulkSql .=" UPDATE  ".CONFIG_DBN.".rawmaterial_audit SET computer_stock = '".$new_computer_stock."' wHERE id = '".$checkRow['id']."'; ";
								
							}else
							{
								$new_computer_stock =  $difference;
								$bulkSql .= " INSERT INTO  ".CONFIG_DBN.".rawmaterial_audit (companyid,storeid,inv_unitid,audit_date,lnkrawmaterialid,system_computer_stock,computer_stock,created_at,created_by) VALUES ('".CONFIG_CID."','".CONFIG_SID."','".$item['inv_unitid']."','".$currentDate."','".$item['inventory_id']."','".$new_computer_stock."','".$new_computer_stock."','".$datetime."','".CONFIG_UID."'); ";
							}
							
							$loopdate = date('Y-m-d',strtotime("+1 day",strtotime($loopdate)));
							
							$bulkSql .=" INSERT INTO  ".CONFIG_DBN.".rawmaterial_audit_log (`audit_date`,`lnkrawmaterialid`,`type`,`old_value`,`new_value`,`reason`,`inv_unitid`,`storeid`,`companyid`,`created_at`,`created_by`) VALUES ('".$currentDate."','".$item['inventory_id']."',3,'".$old_computer_stock."','".$item['physical_stock']."','".$item['reason']."','".$item['inv_unitid']."','".CONFIG_SID."','".CONFIG_CID."','".$datetime."','".CONFIG_UID."');  ";
						}
					}
					
					$dao->initCommand("SELECT * FROM ".CONFIG_DBN.".cfrawmaterial_loc  WHERE companyid = :companyid AND storeid = :storeid AND lnkrawmaterialid = :lnkrawmaterialid  AND inv_unitid = :inv_unitid  ");
					$tempDate = date($dateformat,strtotime($data['current_date']));
					$currentDatemain = $ObjUtil->convertDateToMySql($tempDate);;
					
					$dao->addParameter(':companyid',CONFIG_CID);
					$dao->addParameter(':storeid',CONFIG_SID);
					$dao->addParameter(':inv_unitid',$item['inv_unitid']);
					$dao->addParameter(':lnkrawmaterialid',$item['inventory_id']);
					$checkRowLoc = $dao->executeRow();
					
					if($checkRowLoc)
					{
						$newlocstock = $checkRowLoc['inventory'] + $difference ;
						$bulkSql .= " UPDATE ".CONFIG_DBN.".cfrawmaterial_loc SET inventory = '".$newlocstock."' WHERE id = '".$checkRowLoc['id']."'; ";
					}
					
					$bulkSql .=" INSERT INTO  ".CONFIG_DBN.".rawmaterial_audit_log (`audit_date`,`lnkrawmaterialid`,`type`,`old_value`,`new_value`,`reason`,`inv_unitid`,`storeid`,`companyid`,`created_at`,`created_by`) VALUES ('".$currentDatemain."','".$item['inventory_id']."',3,'".$computer_stock."','".$item['physical_stock']."','".$item['reason']."','".$item['inv_unitid']."','".CONFIG_SID."','".CONFIG_CID."','".$datetime."','".CONFIG_UID."');  ";
                    $i++;
                }
                
                if($bulkSql!='')
                {
                
                	$dao->initCommand($bulkSql);
					$dao->executeNonQuery();
                }
                
            }
           // $dao->releaseTransaction(TRUE);
			return json_encode(array('Success'=>'True','Message'=>$languageArr->LANG40));
        }catch (\Exception $e)
        {
        	//$dao->releaseTransaction(FALSE);
			$this->log->logIt($this->module.' - updatePhysicalInventory - '.$e);
        }
    }
    
}

?>

