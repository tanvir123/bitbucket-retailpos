<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 30/1/18
 * Time: 4:11 PM
 */
namespace database;
class inventorydao
{
    public $module = 'DB_inventorydao';
    public $log;
    public $dbconnect;
    private $ObjCommonDao;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->mongo_con = new \MongoDB\Driver\Manager(CONFIG_MONGO_HOST);
        $this->ObjCommonDao = new \database\commondao();
    }

    public function updateItemInventory($item_id, $itemqty,$unitratio,$itemtype,$operation,$folio_id,$detailid,$isvoid)
    {
        /*
        1 = Combo
        8 = Item
        9 = Modifier Item
        10 = Modifier
        */

        $this->log->logIt($this->module.' - updateItemInventory');
        $dao = new \dao();
        $recipe_type = 1;
        if ($itemtype == 1) {

            $strSql = "CALL " . CONFIG_DBN . ".SP_updateinventory(" . $item_id . "," . $itemqty . "," . $recipe_type . "," . $unitratio . ",".$operation.",".$folio_id.",".$detailid.",".$isvoid."," . CONFIG_CID . ",'" . CONFIG_LID . "',@final_status);";
            $dao->initCommand($strSql);
            $dao->executeNonQuery();
            $strSql = "SELECT @final_status as finalstatus";
            $dao->initCommand($strSql);
            $result = $dao->executeRow();
            $this->log->logIt('Final status item :: ' . $result['finalstatus']);
        }

        //Combo product deduct inventory
        if ($itemtype == 2) {

            $strSql = "SELECT lnkitemid,quantity FROM " . CONFIG_DBN . ".cfcombo_item_rel WHERE lnkcomboid=:itemid and companyid=:companyid and locationid=:locationid and is_deleted=0";
            $dao->initCommand($strSql);
            $dao->addParameter(':itemid', $item_id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $combolist = $dao->executeQuery();

            if (!empty($combolist)) {
                foreach ($combolist as $val) {
                    $item = $val['lnkitemid'];
                    $quantity = $val['quantity'];
                    $totalQty = $quantity * $itemqty;
                    $strSql = "CALL " . CONFIG_DBN . ".SP_updateinventory(" . $item . "," . $totalQty . "," . $recipe_type . "," . $unitratio . ",".$operation.",".$folio_id.",".$detailid.",".$isvoid."," . CONFIG_CID . ",'" . CONFIG_LID . "',@final_status);";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();
                    $strSql = "SELECT @final_status as finalstatus";
                    $dao->initCommand($strSql);
                    $result = $dao->executeRow();
                    $this->log->logIt('Final status combo:: ' . $result['finalstatus']);
                }
            }
        }
    }

    public function addItemInventory($rawhash, $itemid, $unit, $item, $unitqty, $itemqty)
    {
        $ObjUtil = new \util\util;
        $localdate = $ObjUtil->getLocalDate();
        $con = $this->mongo_con;
        if (isset($con)) {

            $filter = [
                'rawhash' => $rawhash,
                'itemid' => $itemid,
                'todaydate' => $localdate,
                'companyid' => CONFIG_CID,
                "locationid" => CONFIG_LID,
            ];

            $query = new \MongoDB\Driver\Query($filter);
            $cursor = $con->executeQuery(CONFIG_DBN . '.item_invenory', $query);
            $cnt = $itemcnt = $unitcnt = 0;

            foreach ($cursor as $document) {
                $itemcnt = $document->itemcount;
                $unitcnt = $document->unitcount;
                $cnt++;
            }
            $itemqty = $itemcnt + $itemqty;
            $unitqty = $unitcnt + $unitqty;
            $array = array(
                "rawhash" => $rawhash,
                "itemid" => $itemid,
                "item" => $item,
                "itemcount" => $itemqty,
                "unitcount" => $unitqty,
                "unit" => $unit,
                "todaydate" => $localdate,
                "companyid" => CONFIG_CID,
                "locationid" => CONFIG_LID,
            );
            if ($cnt > 0) {
                $bulk = new \MongoDB\Driver\BulkWrite;
                $bulk->update($filter, $array);
                $con->executeBulkWrite(CONFIG_DBN . ".item_invenory", $bulk);
            } else {
                $bulk = new \MongoDB\Driver\BulkWrite;
                $bulk->insert($array);
                $con->executeBulkWrite(CONFIG_DBN . ".item_invenory", $bulk);
            }
        }
    }

    public function updateModifierInventory($modifier_id, $total_quantity,$recipe_type,$operation,$folio_id,$detailid,$isvoid)
    {
        $this->log->logIt($this->module.' - updateModifierInventory');
        $dao = new \dao();
        $unitratio = 100;
        $strSql = "CALL " . CONFIG_DBN . ".SP_updateinventory(" . $modifier_id . "," . $total_quantity . "," . $recipe_type . "," . $unitratio . ",".$operation.",".$folio_id.",".$detailid.",".$isvoid."," . CONFIG_CID . ",'" . CONFIG_LID . "',@final_status);";
        $dao->initCommand($strSql);
        $dao->executeNonQuery();
        $strSql = "SELECT @final_status as finalstatus";
        $dao->initCommand($strSql);
        $result = $dao->executeRow();
        $this->log->logIt('Final status modifier:: ' . $result['finalstatus']);
    }

    public function addModifierInventory($rawhash, $modifierid, $unit, $modifier, $unitqty, $itemqty)
    {
        $ObjUtil = new \util\util;
        $localdate = $ObjUtil->getLocalDate();
        $con = $this->mongo_con;
        if (isset($con)) {

            $filter = [
                'rawhash' => $rawhash,
                'modifierid' => $modifierid,
                'todaydate' => $localdate,
                'companyid' => CONFIG_CID,
                "locationid" => CONFIG_LID,
            ];

            $query = new \MongoDB\Driver\Query($filter);
            $cursor = $con->executeQuery(CONFIG_DBN . '.modifier_invenory', $query);
            $cnt = $itemcnt = $unitcnt = 0;

            foreach ($cursor as $document) {
                $itemcnt = $document->itemcount;
                $unitcnt = $document->unitcount;
                $cnt++;
            }
            $itemqty = $itemcnt + $itemqty;
            $unitqty = $unitcnt + $unitqty;

            $array = array(
                "rawhash" => $rawhash,
                "modifierid" => $modifierid,
                "modifier" => $modifier,
                "itemcount" => $itemqty,
                "unitcount" => $unitqty,
                "unit" => $unit,
                "todaydate" => $localdate,
                "companyid" => CONFIG_CID,
                "locationid" => CONFIG_LID,
            );
            if ($cnt > 0) {
                $bulk = new \MongoDB\Driver\BulkWrite;
                $bulk->update($filter, $array);
                $con->executeBulkWrite(CONFIG_DBN . ".modifier_invenory", $bulk);
            } else {
                $bulk = new \MongoDB\Driver\BulkWrite;
                $bulk->insert($array);
                $con->executeBulkWrite(CONFIG_DBN . ".modifier_invenory", $bulk);
            }
        }
    }

}
?>