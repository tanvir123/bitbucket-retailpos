<?php
namespace database;
class discountdao
{
    public $module = 'DB_discountdao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addDiscount($data,$languageArr,$defaultlanguageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - addDiscount');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $datetime=\util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();
            $tablename =  "cfdiscount";
            $ObjDependencyDao = new \database\dependencydao();
            $catrgoty_names = '';
            $catrgoty_IDS = [];
            if(!empty($data['cat_discount']) && count($data['cat_discount'])>0){
                $selCat = "SELECT categoryunkid,categoryname FROM ".CONFIG_DBN.".cfmenu_categories WHERE is_active=1 AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid AND hashkey IN ('".implode("','",$data['cat_discount'])."')";
                $dao->initCommand($selCat);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $categorylist = $dao->executeQuery();
                if(!empty($categorylist) && count($categorylist)>0){
                    $catrgoty_names = \util\util::array_pluck($categorylist,'categoryname');
                    $catrgoty_IDS = \util\util::array_pluck($categorylist,'categoryunkid');
                }
            }

            $arr_log = array(
                'Short Name'=>$data['shortname'],
                'Discount'=>$data['discount'],
                'Posting Type'=>($data['postrule']==1)?'Percentage':'Flat',
                'Open Discount'=>($data['opendisc']==1)?'Yes':'No',
                'Value'=>$data['value'],
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive',
            );
            if($catrgoty_names!=''){
                $arr_log['Category'] = implode(',',$catrgoty_names);
            }
            $json_data = json_encode($arr_log);
            if($data['id']=='0' || $data['id']==0){
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"discount",$data['discount']);
                if($chk_name==1)
                {
                    return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->RECORD_ALREADY_EXISTS));
                }

                $title = "Add Record";
                $hashkey = \util\util::gethash();
                $strSql = "INSERT INTO ".CONFIG_DBN.".cfdiscount (shortcode, discount, companyid,locationid, fasmastertype, postingrule, value, isopendiscount, createddatetime, createduser, isactive, hashkey, category_id)
                           VALUE(:shortcode, :discount, :companyid,:locationid, :fasmastertype, :postingrule, :value, :isopendiscount, :createddatetime, :createduser, :isactive, :hashkey, :category_id)";
                $dao->initCommand($strSql);
                $dao->addParameter(':shortcode',$data['shortname']);
                $dao->addParameter(':discount',$data['discount']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->addParameter(':fasmastertype','discount');
                $dao->addParameter(':postingrule',$data['postrule']);
                $dao->addParameter(':value',$data['value']);
                $dao->addParameter(':isopendiscount',$data['opendisc']);
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':createduser',CONFIG_UID);
                $dao->addParameter(':isactive',$data['rdo_status']);
                $dao->addParameter(':hashkey',$hashkey);
                $dao->addParameter(':category_id',($catrgoty_IDS != '')?implode(',',$catrgoty_IDS):'');
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                return json_encode(array('Success'=>'True','Message'=>'REC_ADD_SUC'));
            }
            else{
                $id = $ObjCommonDao->getprimarykey('cfdiscount',$data['id'],'discountunkid');
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"discount",$data['discount'],$id);
                if($chk_name==1)
                {
                    return json_encode(array('Success'=>'False','Message'=>'RECORD_ALREADY_EXISTS'));
                }

                $title = "Edit Record";
                $did = $data['id'];
                $strSql = "UPDATE ".CONFIG_DBN.".cfdiscount SET shortcode=:shortcode, discount=:discount, postingrule=:postingrule, value=:value, isopendiscount=:isopendiscount , modifieddatetime=:modifieddatetime, modified_user=:modified_user, category_id=:category_id WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':shortcode',$data['shortname']);
                $dao->addParameter(':discount',$data['discount']);
                $dao->addParameter(':postingrule',$data['postrule']);
                $dao->addParameter(':value',$data['value']);
                $dao->addParameter(':isopendiscount',$data['opendisc']);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':hashkey',$did);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->addParameter(':category_id',($catrgoty_IDS != '')?implode(',',$catrgoty_IDS):'');
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                return json_encode(array('Success'=>'True','Message'=>'REC_UP_SUC'));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addDiscount - '.$e);
        }
    }

    public function disclist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - disclist - '.$name);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = "SELECT CFD.*,IFNULL(CFU1.username,'') AS createduser,IFNULL(CFU2.username,'') AS modified_user,IFNULL(DATE_FORMAT(CFD.createddatetime,'".$mysqlformat."'),'') as created_date,IFNULL(DATE_FORMAT(CFD.modifieddatetime,'".$mysqlformat."'),'') as modified_date FROM ".CONFIG_DBN.".cfdiscount AS CFD LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CFD.createduser=CFU1.userunkid AND CFD.companyid=CFU1.companyid LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON CFD.modified_user=CFU2.userunkid AND CFD.modified_user=CFU2.userunkid AND CFD.companyid=CFU2.companyid WHERE CFD.companyid=:companyid AND CFD.locationid=:locationid AND CFD.is_deleted=0";
            if($name!=""){
                $strSql .= " AND CFD.discount LIKE '%".$name."%'";
            }
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0)){
                $dao->initCommand($strSql.$strSqllmt);
            }
            else{
                $dao->initCommand($strSql);
            }
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue),ENT_QUOTES);
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - disclist - '.$e);
        }
    }

    public function getDiscRec($data)
    {
		try
        {
			$this->log->logIt($this->module." - getDiscRec");
			$dao = new \dao();
            $id=(isset($data['id']))?$data['id']:"";
            $strSql = " SELECT shortcode,discount,postingrule,isopendiscount,value,isactive,category_id FROM ".CONFIG_DBN.".cfdiscount WHERE hashkey=:discountunkid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':discountunkid',$id);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeRow();
            if(!empty($res)){
                if($res['category_id']!='') {
                    $selCatSql = "SELECT hashkey FROM " . CONFIG_DBN . ".cfmenu_categories WHERE categoryunkid IN (".$res['category_id'].") AND companyid=:companyid AND locationid=:locationid AND is_deleted=0";
                    $dao->initCommand($selCatSql);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $categoryData = $dao->executeQuery();
                    if(!empty($categoryData) && count($categoryData)>0){
                        $res['category_id'] = \util\util::array_pluck($categoryData,'hashkey');
                    }
                }else{
                    $res['category_id'] = [];
                }
            }

            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)),ENT_QUOTES);
        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getDiscRec - ".$e);
			return false;
		}
	}

    public function getActiveDiscList()
    {
		try
        {
			$this->log->logIt($this->module." - getDiscRec");
			$dao = new \dao();

            $strSql = " SELECT hashkey, discount,postingrule FROM ".CONFIG_DBN.".cfdiscount WHERE companyid=:companyid AND isactive=1 AND is_deleted=0 AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $Data = $dao->executeQuery();
            return $Data;
        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getDiscRec - ".$e);
			return false;
		}
	}





}

?>