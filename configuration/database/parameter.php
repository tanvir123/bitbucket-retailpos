<?php
namespace database;
class parameter
{
    public $module = 'DB_parameter';
    public $log;
    public $dbconnect;
    
    function __construct()
    {
       // $this->log = new \util\logger();
    }
    
    public function getParameter($keyname,$companyid='',$loginid='')
    {
        try
        {
            //$this->log->logIt($this->module.' - getParameter = '.$keyname);
            if($companyid=="")
            {
                $companyid=CONFIG_CID;
            }
            $type=CONFIG_LOGINTYPE;
            $dao = new \dao();
            if($type==2){
                if($loginid=="")
                {
                    $loginid=CONFIG_SID;
                }
                $strSql = "SELECT * FROM ".CONFIG_DBN.".cfparameter WHERE keyname = '".$keyname."' AND companyid = ".$companyid." AND storeid = ".$loginid." and type=".$type." ";
            }
            else {
                if($loginid=="")
                {
                    $loginid = CONFIG_LID;
                }
                $strSql = "SELECT * FROM ".CONFIG_DBN.".cfparameter WHERE keyname = '".$keyname."' AND companyid = ".$companyid." AND locationid = ".$loginid." and type=".$type." ";
            }
            $dao->initCommand($strSql);
            $data = $dao->executeRow();
            return $data['keyvalue'];
        }
        catch(Exception $e)
        {
            //$this->log->logIt($this->module.' - getParameter - '.$e);
        }
    }
    public function setParameter($keyname,$keyvalue,$companyid='',$loginid='')
    {
        try
        {
            //$this->log->logIt($this->module.' - setParameter = '.$keyname.' = '.$keyvalue);
            if($companyid=="")
            {
                $companyid=CONFIG_CID;
            }

            if($keyname!='invoice_display_lbl' && ($keyname==="" || $keyvalue===""))
            {
                return 0;
            }
            $type=CONFIG_LOGINTYPE;
            $dao = new \dao();
            $ObjAuditDao = new \database\auditlogdao();
            $current_value = self::getParameter($keyname,$companyid);
            if($keyname=='pms_hotelid'){
                if($type==2) {
                    if ($loginid == "") {
                        $loginid = CONFIG_SID;
                    }
                    $strSql = "UPDATE ".CONFIG_DBN.".cfparameter SET keyvalue='".$keyvalue."' WHERE keyname = '".$keyname."' AND companyid = ".$companyid." AND storeid = ".$loginid." and type=" . $type . " ;";
                }
                else{
                    if ($loginid == "") {
                        $loginid = CONFIG_LID;
                    }
                    $strSql = "UPDATE ".CONFIG_DBN.".cfparameter SET keyvalue='".$keyvalue."' WHERE keyname = '".$keyname."' AND companyid = ".$companyid." AND locationid = ".$loginid." and type=" . $type . " ;";

                }
                $dao->initCommand($strSql);
                $dao->executeNonQuery();
                $ObjAuditDao->addsettingslog($keyname,$current_value,$keyvalue);
            }else{
                if($current_value!=$keyvalue)
                {
                    if($type==2) {
                        if ($loginid == "") {
                            $loginid = CONFIG_SID;
                        }
                        $strSql = "UPDATE ".CONFIG_DBN.".cfparameter SET keyvalue='".$keyvalue."' WHERE keyname = '".$keyname."' AND companyid = ".$companyid." AND storeid = ".$loginid." and type=" . $type . " ;";
                    }
                    else{
                        if ($loginid == "") {
                            $loginid = CONFIG_LID;
                        }
                        $strSql = "UPDATE ".CONFIG_DBN.".cfparameter SET keyvalue='".$keyvalue."' WHERE keyname = '".$keyname."' AND companyid = ".$companyid." AND locationid = ".$loginid." and type=" . $type . " ;";

                    }
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();
                    $ObjAuditDao->addsettingslog($keyname,$current_value,$keyvalue);
                }
            }

            return 1;
        }
        catch(Exception $e)
        {
           // $this->log->logIt($this->module.' - setParameter - '.$e);
        }
    }
}
?>