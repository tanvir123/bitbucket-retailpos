<?php
namespace database;

class paymenttypedao
{
    public $module = 'DB_paymenttype';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function addPaymentType($data,$languageArr,$defaultlanguageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - addPaymentType');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $tablename =  "cfpaymenttype";
            $datetime=\util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();
            $ObjDependencyDao = new \database\dependencydao();
            $arr_log = array(
                'Short Code'=>$data['shortcode'],
                'Payment Type'=>$data['paymentmethod'],
                'Status'=>($data['active']==1)?'Active':'Inactive',
            );
            $json_data = json_encode($arr_log);
            if($data['paymenttypeid']=='0' || $data['paymenttypeid']==0)
            {
				$chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"paymenttype",$data['paymentmethod']);
				if($chk_name==1){
					return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG10));
				}
				
                $chk_shortcode = $ObjDependencyDao->checkduplicaterecord($tablename,"shortcode",$data['shortcode']);
                if($chk_shortcode==1){
                    return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG11));
                }
                $title = "Add Record";
                $hashkey = \util\util::gethash();
                $strSql = "INSERT INTO ".CONFIG_DBN.".cfpaymenttype (shortcode, paymenttype, type, isactive, createddatetime, fasmastertype, created_user, companyid,locationid, hashkey)
                            VALUES(:shortcode, :paymentmethod, :type, :status, :datetime, :fasmastertype, :userid, :companyid,:locationid, :hashkey)";
                $dao->initCommand($strSql);
                $dao->addParameter(':shortcode',$data['shortcode']);
                $dao->addParameter(':paymentmethod',$data['paymentmethod']);
                $dao->addParameter(':type',$data['type']);                
                $dao->addParameter(':status',$data['active']);
                $dao->addParameter(':datetime',$datetime);
                $dao->addParameter(':fasmastertype','paymenttype');
                $dao->addParameter(':userid',CONFIG_UID);
                $dao->addParameter(':hashkey',$hashkey);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();
                $id = $dao->getLastInsertedId();
                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                return json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_ADD_SUC));
            }
            else{
                $id = $ObjCommonDao->getprimarykey('cfpaymenttype',$data['paymenttypeid'],'paymenttypeunkid');
                $title = "Edit Record";
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"paymenttype",$data['paymentmethod'],$id);
                if($chk_name==1){
                    return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG10));
                }
	
				$chk_shortcode = $ObjDependencyDao->checkduplicaterecord($tablename,"shortcode",$data['shortcode'],$id);
				if($chk_shortcode==1){
                    return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG11));
				}
				
                $strSql = "UPDATE ".CONFIG_DBN.".cfpaymenttype SET shortcode=:shortcode, paymenttype=:paymenttype,
                            type=:type,  modifieddatetime=:modifieddatetime, modified_user=:modified_user";
                $strSql .= " WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid";
                 
                $dao->initCommand($strSql);
                $dao->addParameter(':shortcode',$data['shortcode']);
                $dao->addParameter(':paymenttype',$data['paymentmethod']);
                $dao->addParameter(':type',$data['type']);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':hashkey',$data['paymenttypeid']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$data['paymenttypeid'],$json_data);
                return json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_UP_SUC));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addPaymentType - '.$e);
        }
    }

    public function paymenttypelist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - paymenttypelist - '.$name);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = "SELECT CFP.paymenttypeunkid,CFP.shortcode,CFP.paymenttype,CFP.type,CFP.fasmastertype,CFP.lnkmasterunkid,CFP.isactive,CFP.systemdefined,CFP.createddatetime,CFP.created_user,CFP.modifieddatetime,CFP.modified_user,CFP.is_deleted,CFP.hashkey,CFP.companyid,CFP.locationid,IFNULL(DATE_FORMAT(CFP.createddatetime,'".$mysqlformat."'),'') as created_date,IFNULL(DATE_FORMAT(CFP.modifieddatetime,'".$mysqlformat."'),'') as modified_date
                        FROM ".CONFIG_DBN.".cfpaymenttype AS CFP
                        LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CFP.created_user=CFU1.userunkid AND CFP.companyid=CFU1.companyid
                        LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON CFP.modified_user=CFU2.userunkid AND CFP.modified_user=CFU2.userunkid AND CFP.companyid=CFU2.companyid
                        WHERE CFP.companyid=:companyid AND CFP.locationid=:locationid AND CFP.is_deleted=0";
            if($name!=""){
                $strSql .= " AND paymenttype LIKE '%".$name."%'";
            }
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue));
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - userdetail - '.$e);
        }
    }
    public function getPaymentTypeRec($data)
    {
		try
        {
			$this->log->logIt($this->module." - getPaymentTypeRec");
			$dao = new \dao();
            $paymenttypeid=(isset($data['paymenttypeid']))?$data['paymenttypeid']:"";
            $strSql = "SELECT shortcode,paymenttype,type,isactive 
                        FROM ".CONFIG_DBN.".cfpaymenttype 
                        WHERE hashkey=:paymenttypeunkid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':paymenttypeunkid',$paymenttypeid);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeRow();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getPaymentTypeRec - ".$e);
			return false; 
		}
	}
    public function getPaymentTypes($data)
    {
        try
        {
            $this->log->logIt($this->module." - getPaymentTypes");
            $dao = new \dao();
            $strSql = "SELECT paymenttypeunkid,shortcode,paymenttype FROM ".CONFIG_DBN.".cfpaymenttype WHERE type=:type AND companyid=:companyid AND locationid=:locationid AND is_deleted=0 AND isactive=1";
            $dao->initCommand($strSql);
            $dao->addParameter(':type',$data['payment_mode']);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $resObj = $dao->executeQuery();
            return json_encode(array("Success"=>"True","Data"=>$resObj));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getPaymentTypes - ".$e);
            return false;
        }
    }
    public function getAllPaymentTypes()
    {
        try
        {
            $this->log->logIt($this->module." - getAllPaymentTypes");
            $dao = new \dao();
            $strSql = "SELECT paymenttypeunkid,paymenttype FROM ".CONFIG_DBN.".cfpaymenttype WHERE companyid=:companyid AND locationid=:locationid AND is_deleted=0 AND isactive=1";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $resObj = $dao->executeQuery();
            return json_encode(array("Success"=>"True","Data"=>$resObj));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getAllPaymentTypes - ".$e);
            return false;
        }
    }
}

?>