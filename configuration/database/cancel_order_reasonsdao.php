<?php

namespace database;

class cancel_order_reasonsdao
{
    public $module = 'DB_cancel_order_reasonsdao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addcancelorderreasonRec($data, $languageArr, $defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - addcancelorderreasonRec');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();

            $datetime = \util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();
            $tablename = "cfreason";
            $ObjDependencyDao = new \database\dependencydao();

            $arr_log = array(
                'Name' => $data['name'],
                'Remarks' => $data['remarks'],
                'Status' => ($data['rdo_status'] == 1) ? 'Active' : 'Inactive',
            );
            $json_data = json_encode($arr_log);
            if ($data['id'] == 0) {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "name", $data['name']);
                if ($chk_name == 1) {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->RECORD_ALREADY_EXISTS));
                }
                $title = "Add Record";
                $hashkey = \util\util::gethash();
                $strSql = "INSERT INTO " . CONFIG_DBN . ".cfreason 
                (companyid,locationid, name,remarks, createddatetime, created_user, is_active, hashkey)
               VALUE(:companyid,:locationid, :name, :remarks, :createddatetime, :created_user, :is_active, :hashkey)";
                $dao->initCommand($strSql);
                $dao->addParameter(':name', $data['name']);
                $dao->addParameter(':remarks', $data['remarks']);
                $dao->addParameter(':is_active', $data['rdo_status']);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->addParameter(':created_user', CONFIG_UID);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
               $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'], $title, $hashkey, $json_data);
                return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_ADD_SUC));
            } else {
                $id = $ObjCommonDao->getprimarykey('cfreason', $data['id'], 'reasonunkid');
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "name", $data['name'], $id);
                if ($chk_name == 1) {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->RECORD_ALREADY_EXISTS));
                }
                $title = "Edit Record";
                $strSql = "UPDATE " . CONFIG_DBN . ".cfreason SET name=:name,remarks=:remarks, 
               modifieddatetime=:modifieddatetime, modified_user=:modified_user 
                 WHERE hashkey=:reasonunkid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':reasonunkid', $data['id']);
                $dao->addParameter(':remarks', $data['remarks']);
                $dao->addParameter(':name', $data['name']);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'], $title, $data['id'], $json_data);
                return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_UP_SUC));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addcancelorderreasonRec - ' . $e);
        }
    }

    public function cancelreasonslist($limit, $offset, $name, $isactive = 0)
    {
        try {
            $this->log->logIt($this->module . ' - cancelreasonslist - ' . $name);

            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = "SELECT D.*,IFNULL(CFU1.username,'') AS createduser,
                                    IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(D.createddatetime,'" . $mysqlformat . "'),'') as created_date,
                                    IFNULL(DATE_FORMAT(D.modifieddatetime,'" . $mysqlformat . "'),'') as modified_date
                                    FROM " . CONFIG_DBN . ".cfreason AS D
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU1 ON D.created_user=CFU1.userunkid AND D.companyid=CFU1.companyid
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU2 ON D.modified_user=CFU2.userunkid AND D.modified_user=CFU2.userunkid AND D.companyid=CFU2.companyid
                                    WHERE D.companyid=:companyid AND D.locationid=:locationid AND D.is_deleted=0";

            if ($name != "")
                $strSql .= " AND D.name LIKE '%" . $name . "%'";
            if ($isactive == 1) {
                $strSql .= " AND D.is_active=1 ";
            }
            $strSql .= " ORDER BY D.reasonunkid DESC";
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $data = $dao->executeQuery();

            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $rec = $dao->executeQuery();


            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return html_entity_decode(json_encode($retvalue));
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - cancelreasonslist - ' . $e);
        }
    }

    public function getcancelorderreasonRec($data)
    {
        try {
            $this->log->logIt($this->module . " - getcancelorderreasonRec");
            $dao = new \dao();
            $id = (isset($data['id'])) ? $data['id'] : "";
            $strSql = "SELECT name,remarks, is_active  FROM " . CONFIG_DBN . ".cfreason 
            WHERE hashkey=:reasonunkid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':reasonunkid', $id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeRow();
            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res)));

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getcancelorderreasonRec - " . $e);
            return false;
        }
    }






}

?>