<?php
namespace database;

class dashboarddao
{
    public $module = 'DB_dashboarddao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
    
    public function getdashboardheaderdata()
    {
        try
        {
            $this->log->logIt($this->module.' - getdashboardheaderdata');
            //$todaydate = \util\util::getLocalDate();

            $todaydate = \database\parameter::getParameter('todaysdate');
            if(!$todaydate || $todaydate == '')
            {
                $todaydate = \util\util::getlocalDate();
                \database\parameter::setParameter('todaysdate',$todaydate);
            }
            $retArr = array();
            $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $strSql = " SELECT count(FM.foliounkid) AS cnt FROM ".CONFIG_DBN.".fasfoliomaster As FM".
                " INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC ON TRC.contactunkid = FM.lnkcontactid AND FM.companyid=:companyid AND FM.locationid=:locationid ".
                        " WHERE FM.companyid=:companyid AND FM.locationid=:locationid AND TRC.contacttype != 4 AND CAST(FM.servedate AS DATE)=:date AND FM.isvoid=0 AND FM.canceluserunkid = 0 AND FM.isvalid = 1";
			
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid",CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->addParameter(":date",$todaydate);
            $rec = $dao->executeRow();
            $retArr['orders'] = $rec['cnt'];

            $strSql = " SELECT count(FASFD.detailunkid) AS cnt FROM ".CONFIG_DBN.".fasfoliodetail AS FASFD".
                " INNER JOIN ".CONFIG_DBN.".fasfoliomaster AS FFM ON FFM.foliounkid = FASFD.foliounkid ".
                " INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC ON TRC.contactunkid = FFM.lnkcontactid AND FFM.companyid=:companyid AND FFM.locationid=:locationid ".
                " INNER JOIN ".CONFIG_DBN.".fasmaster AS FASM ON FASFD.masterunkid = FASM.masterunkid AND FASFD.companyid=:companyid AND FASM.locationid=:locationid".
                " WHERE (FASM.mastertypeunkid=1 OR (FASM.mastertypeunkid=8 AND FASFD.parentid=0)) AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid AND FASFD.isvoid=0 AND FASFD.isrefund=0 AND CAST(FFM.servedate AS DATE) =:date AND TRC.contacttype != 4 AND FFM.isvoid = 0 AND FFM.canceluserunkid = 0 AND FFM.isvalid = 1";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid",CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->addParameter(":date",$todaydate);
            $rec1 = $dao->executeRow();
            $retArr['items'] = $rec1['cnt'];

            $strSql = "SELECT ROUND(IFNULL(SUM(CASE WHEN FASM.mastertypeunkid IN (1,2,3,8,9,10) THEN FASFD.quantity * FASFD.baseamount * FASM.crdr ELSE 0 END),0)-IFNULL(SUM(BFM.meta_value),0),".$round_off.") AS total
             FROM ".CONFIG_DBN.".fasfoliodetail AS FASFD".
                " INNER JOIN ".CONFIG_DBN.".fasfoliomaster AS FFM ON FFM.foliounkid = FASFD.foliounkid ".
                " INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC ON TRC.contactunkid = FFM.lnkcontactid AND FFM.companyid=:companyid AND FFM.locationid=:locationid ".
                " INNER JOIN ".CONFIG_DBN.".fasmaster AS FASM ON FASFD.masterunkid = FASM.masterunkid AND FASFD.companyid=:companyid AND FASM.locationid=:locationid".
                " LEFT JOIN ".CONFIG_DBN.".bmfoliodetail_meta AS BFM ON FASFD.detailunkid = BFM.lnkdetailunkid AND BFM.companyid=:companyid AND BFM.locationid=:locationid AND FASFD.isvoid = 0".
                " WHERE  FASFD.companyid=:companyid AND FASFD.locationid=:locationid AND FASFD.isvoid=0 AND FASFD.isrefund=0 AND CAST(FFM.servedate AS DATE) =:date AND FFM.isvoid = 0 AND TRC.contacttype != 4 AND FFM.canceluserunkid = 0 AND FFM.isvalid = 1";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid",CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->addParameter(":date",$todaydate);
            $rec2 = $dao->executeRow();
            $retArr['revenue'] = number_format($rec2['total'],$round_off);
            return $retArr;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getdashboardheaderdata - '.$e);
        }
    }
    public function getrevenuedays($fromdays,$todays)
    {
        try
        {
           $this->log->logIt($this->module.' - getrevenuedays');
            $todaydate = \database\parameter::getParameter('todaysdate');
            if(!$todaydate || $todaydate == '')
            {
                $todaydate = \util\util::getlocalDate();
                \database\parameter::setParameter('todaysdate',$todaydate);
            }
            $retArr = array();
            $dao = new \dao();

            $strSql = "SELECT IFNULL(CAST(FFM.servedate AS DATE),'') AS date, IFNULL(SUM(CASE WHEN FASM.mastertypeunkid IN (1,2,3,8,9,10) THEN FASFD.quantity * FASFD.baseamount * FASM.crdr ELSE 0 END),0) -IFNULL(SUM(BFM.meta_value),0) AS total".
                " FROM ".CONFIG_DBN.".fasfoliodetail AS FASFD INNER JOIN".
                " ".CONFIG_DBN.".fasmaster AS FASM ON FASFD.masterunkid = FASM.masterunkid AND FASFD.companyid=:companyid AND FASM.locationid=:locationid".
                " INNER JOIN ".CONFIG_DBN.".fasfoliomaster AS FFM ON FFM.foliounkid = FASFD.foliounkid ".
                " INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC ON TRC.contactunkid = FFM.lnkcontactid AND FFM.companyid=:companyid AND FFM.locationid=:locationid ".
                " LEFT JOIN ".CONFIG_DBN.".bmfoliodetail_meta AS BFM ON FASFD.detailunkid = BFM.lnkdetailunkid AND BFM.companyid=:companyid AND BFM.locationid=:locationid AND FASFD.isvoid = 0".
                " WHERE FASFD.companyid=:companyid AND FASFD.locationid=:locationid AND FASFD.isvoid=0 AND FASFD.isrefund=0 AND TRC.contacttype != 4 AND CAST(FFM.servedate AS DATE) BETWEEN :fromdate AND :todate AND FFM.isvoid = 0 AND FFM.canceluserunkid = 0 AND FFM.isvalid = 1 ".
                " GROUP BY CAST(FFM.servedate AS DATE)";

            $dao->initCommand($strSql);
            $dao->addParameter(":companyid",CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->addParameter(":todate",$todays);
            $dao->addParameter(":fromdate",$fromdays);
            $rec = $dao->executeQuery();

            foreach($rec AS $key=>$val){
               $retArr[$val['date']]  = $val['total'];
            }
            return $retArr;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getrevenuedays - '.$e);
        }
    }
    public function gettopsalingItem()
    {
        try
        {
            $this->log->logIt($this->module.' - gettopsalingItem');
            $dao = new \dao();
            $strSql = "SELECT CFI.itemname,IF(CFI.image IS NULL or CFI.image = '', '".CONFIG_ASSETS_URL."noimage.png', CFI.image) AS image, CFI.short_desc ,count(CFI.itemunkid) AS cnt".
                " FROM ".CONFIG_DBN.".fasfoliodetail AS FASFD INNER JOIN".
                " ".CONFIG_DBN.".fasmaster AS FASM ON FASFD.masterunkid = FASM.masterunkid AND FASFD.companyid=:companyid AND FASM.locationid=:locationid".
                " LEFT JOIN ".CONFIG_DBN.".cfmenu_items AS CFI ON CFI.itemunkid = FASFD.lnkmappingunkid AND CFI.is_deleted=0 AND CFI.companyid=:companyid AND CFI.locationid=:locationid".
                " WHERE FASFD.companyid=:companyid AND FASFD.locationid=:locationid AND FASFD.isvoid=0 AND FASFD.isrefund=0 AND FASM.mastertypeunkid=8 GROUP BY CFI.itemunkid ORDER BY cnt DESC LIMIT 5";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid",CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $rec = $dao->executeQuery();
            return $rec;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - gettopsalingItem - '.$e);
        }
    }

}

?>