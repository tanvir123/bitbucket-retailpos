<?php
namespace database;
class document_numberingdao
{
    public $module = 'DB_document_numberingdao';
    public $log;
    public $dbconnect;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
    
    public function doclist()
    {
        try
        {
            $this->log->logIt($this->module.' - doclist - ');
            $dao = new \dao();
            $strSql = "SELECT keyname,prefix,startno,IFNULL(endno,'') AS endno,reset_type,hashkey from ".CONFIG_DBN.".cfdocnumber where companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $cntdata = $dao->executeQuery();
            $data = $dao->executeQuery();
            if(count($data)==0)
            {
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return html_entity_decode(json_encode($retvalue),ENT_QUOTES);
            }
            else
            {
                return json_encode(array(array("cnt"=>count($cntdata),"data"=>$data)));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - doclist - '.$e);
        }
    }

    public function docupdate($data,$languageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - docupdate');
            $dao = new \dao();
            $ObjAuditDao = new \database\auditlogdao();
            $strSql = "";
            $title = "Edit Record";

           // $getinv = $this->getInvoiceNumbering();
           // $main_start_number= $getinv['startno'];
            foreach($data['reset'] as $key=>$value)
            {
                if($key!="")
                {
                   // $ObjCommonDao = new \database\commondao();
                    //$id=$ObjCommonDao->getprimarykey('cfdocnumber',$key,'id');
                   // $old_value = $ObjCommonDao->getFieldValue('cfdocnumber', 'main_start_no', 'id', $id);
                    $strSql = "UPDATE ".CONFIG_DBN.".cfdocnumber SET                     
                         prefix='".$data['prefix'][$key]."',
                         startno='".$data['startno'][$key]."',
                         endno='".$data['endno'][$key]."',
                         reset_type='".$data['reset'][$key]."'";

                   // if($data['startno'][$key] != $main_start_number)
                    //{
                        $strSql .=" ,main_start_no ='".$data['startno'][$key]."' ";
                    //}

                   $strSql .=" WHERE hashkey='".$key."' AND locationid='".CONFIG_LID."' AND companyid='".CONFIG_CID."'";
                    if($data['reset'][$key]==1){
                        $reset_type="Daily";
                    }
                    if($data['reset'][$key]==2){
                        $reset_type="Monthly";
                    }
                    if($data['reset'][$key]==3){
                        $reset_type="Yearly";
                    }
                    if(isset($data['endno'][$key]) && $data['endno'][$key]!=''){
                        if($data['endno'][$key] <= $data['startno'][$key]){
                            return $languageArr->LANG7;
                        }
                    }
                    $arr_log = array(
                        "Prefix"=>isset($data['prefix'][$key])?$data['prefix'][$key]:'',
                        "Start No."=>$data['startno'][$key],
                        "End No."=>$data['endno'][$key],
                        "Reset Type"=>isset($reset_type)?$reset_type:'',
                    );


                    $template = \database\parameter::getParameter('invoice');
                    $meta_key="template".$template;
                    $strSql2 = "SELECT meta_value FROM " . CONFIG_DBN . ". cflocationtemplate_meta  WHERE
                            companyid=:companyid AND locationid=:locationid AND meta_key=:meta_key 
                            ORDER BY locationmetaunkid DESC LIMIT 1";
                    $dao->initCommand($strSql2);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':locationid', CONFIG_LID);
                    $dao->addParameter(':meta_key', $meta_key);
                    $res= $dao->executeRow();
                    $decode_value = (array) json_decode($res['meta_value']);
                    $locationdetal_array = array();
                    $locationdetal_array['dian_num'] = isset($decode_value['dian_num'])?$decode_value['dian_num']:'';
                    $locationdetal_array['nit_number'] = isset($decode_value['nit_number'])?$decode_value['nit_number']:'';
                    $locationdetal_array['appliesfrom'] = isset($decode_value['appliesfrom'])?$decode_value['appliesfrom']:'';
                    $locationdetal_array['main_start_no'] = $data['startno'][$key];
                    $locationdetal_array['endno'] = $data['endno'][$key];
                    $locationdetal_array['welcomemsg'] = isset($decode_value['welcomemsg'])?$decode_value['welcomemsg']:'';
                    $locationdetal_array['note'] = isset($decode_value['note'])?$decode_value['note']:'';

                    //this will work only if template 3 is selected ! Just used currently for Columbian client requirement.
                    $strSql2 = "INSERT INTO " . CONFIG_DBN . ". cflocationtemplate_meta SET  meta_value=:meta_value,meta_key=:meta_key,
                            companyid=:companyid,locationid=:locationid,description=:description";
                    $dao->initCommand($strSql2);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':locationid', CONFIG_LID);
                    $dao->addParameter(':meta_key', $meta_key);
                    $dao->addParameter(':description', 'it contains DIAN number for registering their folio number, NIT num : Fiscal restuarant number, welcome message, Invoice note which will display at the end of template3');
                    $dao->addParameter(':meta_value', json_encode($locationdetal_array));
                    $dao->executeNonQuery();

                    $json_data = json_encode($arr_log);
                   $ObjAuditDao->addactivitylog($data['module'],$title,$key,$json_data);
                }
            }


            $dao->initCommand($strSql);
            $dao->executeNonQuery();


            return 1;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - docupdate - '.$e);
            return 0;
        }
    }

    public function getInvoiceNumbering()
    {
        try
        {
            $this->log->logIt($this->module.' - getInvoiceNumbering');
            $dao = new \dao();
            $strSql = "SELECT prefix, startno,IFNULL(endno,'') AS endno,main_start_no,reset_type FROM ".CONFIG_DBN.".cfdocnumber WHERE keyname='INVOICE' AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $rec = $dao->executeRow();
            return $rec;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getInvoiceNumbering - '.$e);
            return 0;
        }
    }
}

?>