<?php
namespace database;

class menu_itemdao
{
    public $module = 'DB_menu_itemdao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function additem($res,$languageArr,$defaultlanguageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - additem');
            $dao = new \dao();
            $tablename =  "cfmenu_items";
            $datetime=\util\util::getLocalDateTime();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $ObjDependencyDao = new \database\dependencydao();
            $arr_log = array(
                'Item Name'=>$res['name'],
                'SKU'=>$res['sku'],
                'Spicy Type'=>$res['spicy_type'],
                'Long Description'=>$res['longdesc'],
                'Status'=>($res['rdo_status']==1)?'Active':'Inactive',
                'Item Type'=>($res['item_type']==0)?'Simple':'Variable',
            );
            $json_data = json_encode($arr_log);
            if($res['id']==0)
            {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"itemname",$res['name']);
                if($chk_name==1){
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG48)));
                }
                $chk_scode = $ObjDependencyDao->checkduplicaterecord($tablename,"sku",$res['sku']);
                if($chk_scode==1){
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG47)));
                }

                $title = "Add Record";
                $hashkey = \util\util::gethash();

                $category = $ObjCommonDao->getprimarykey('cfmenu_categories',$res['cat'],'categoryunkid');
                $itemtype = $ObjCommonDao->getprimarykey('cfmenu_itemtype',$res['itemtype'],'typeunkid');
                $serveunit = $ObjCommonDao->getprimarykey('cfmenu_itemunit',$res['serve_unit'],'unitunkid');
                $strSql = "INSERT INTO ".CONFIG_DBN.".cfmenu_items( 
                           companyid,locationid,itemname,sku,spicy_type,sale_amount,categoryunkid,itemtypeunkid,long_desc,
                           image,serve_unit,created_user,is_active,createddatetime,hashkey,type,tax_apply_rate)
                           VALUE(:companyid,:locationid,:txtname,:txtsku,:spicy_type,:txtsaleamount,:txtcategory,:itemtype,
                                 :txtlongscription,:upload_img,:serve_unit,:created_user, 
                                 :isactive,:createddatetime,:hashkey,:item_type,:rates)";
                $dao->initCommand($strSql);
                $dao->addParameter(':txtname',$res['name']);
                $dao->addParameter(':txtsku',$res['sku']);
                $dao->addParameter(':spicy_type',$res['spicy_type']);
                $dao->addParameter(':txtsaleamount',$res['saleamount']);
                $dao->addParameter(':txtcategory',$category);
                $dao->addParameter(':itemtype',$itemtype);
                $dao->addParameter(':txtlongscription',$res['longdesc']);
                $dao->addParameter(':upload_img', ($res['image'] != '' ? $res['image'] : NULL));
                $dao->addParameter(':isactive',$res['rdo_status']);
                $dao->addParameter(':serve_unit',$serveunit);
                $dao->addParameter(':created_user',CONFIG_UID);
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':hashkey',$hashkey);
                $dao->addParameter(':item_type',$res['item_type']);
                $dao->addParameter(':rates',$res['rates']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();
                $itemunkid = $dao->getLastInsertedId();

                //Bind unit
                if(count($res['bind_unit'])>0)
                {
                    $bind_unit = $res['bind_unit'];
                    foreach($bind_unit AS $ukey=>$sub_unit) {

                        $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu_itemunit_rel(lnkitemid,lnkitemunitid,rate1,rate2,rate3,rate4,default_rate,created_user, createddatetime,recipe_type,companyid,locationid)";
                        $strSql .= " VALUE (:lnkitemid, :lnkitemunitid, :rate1,:rate2,:rate3,:rate4,:defaultrate,:created_user, :createddatetime,:recipe_type,:companyid, :locationid)";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':lnkitemid', $itemunkid);
                        $dao->addParameter(':lnkitemunitid', $ukey);
                        $dao->addParameter(':rate1', $sub_unit['rate1']);
                        $dao->addParameter(':rate2',$sub_unit['rate2']);
                        $dao->addParameter(':rate3',$sub_unit['rate3']);
                        $dao->addParameter(':rate4',$sub_unit['rate4']);
                        $dao->addParameter(':defaultrate',$sub_unit['defaultrate']);
                        $dao->addParameter(':created_user', CONFIG_UID);
                        $dao->addParameter(':createddatetime', $datetime);
                        $dao->addParameter(':recipe_type', '1');
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                        $relunitid = $dao->getLastInsertedId();

                        //Modifier
                        if(isset($sub_unit['modifier']) && count($sub_unit['modifier'])>0)
                        {
                            $bind_modifier = $sub_unit['modifier'];
                            foreach($bind_modifier AS $val)
                            {
                                $strSql = " INSERT INTO ".CONFIG_DBN.".fditem_modifier_relation (lnkitemid,lnkitemunitid,lnkmodifierid, min, max,lnkmodiunitid,sale_amount, created_user,createddatetime,companyid,locationid)";
                                $strSql .= " VALUE (:lnkitemid,:lnkitemunitid, :lnkmodifierid, :min, :max,:lnkmodiunitid,:sale_amount, :created_user, :createddatetime, :companyid,:locationid)";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':lnkitemid',$itemunkid);
                                $dao->addParameter(':lnkitemunitid', $relunitid);
                                $dao->addParameter(':lnkmodifierid',$val['modid']);
                                $dao->addParameter(':min',$val['min']);
                                $dao->addParameter(':max',$val['max']);
                                $dao->addParameter(':lnkmodiunitid',$val['mounit']);
                                $dao->addParameter(':sale_amount',$val['sale_amount']);
                                $dao->addParameter(':created_user',CONFIG_UID);
                                $dao->addParameter(':createddatetime',$datetime);
                                $dao->addParameter(':companyid',CONFIG_CID);
                                $dao->addparameter(':locationid',CONFIG_LID);
                                $dao->executeNonQuery();
                                $relmodifierid = $dao->getLastInsertedId();
                                $bind_modifier_item = isset($val['modifier_item'])?$val['modifier_item']:array();
                                if(isset($bind_modifier_item)){
                                    foreach($bind_modifier_item AS $key=>$sub_val)
                                    {
                                        $strSql = "INSERT INTO ".CONFIG_DBN.".fdmodifier_item_relation (lnkmodifierrelid,lnkitemid,lnkitemunitid, lnkmodifierid,lnkmodifieritemid, lnkmodiunitid, sale_amount, description, created_user, createddatetime, companyid,locationid)";
                                        $strSql .= " VALUE (:lnkmodifierrelid, :lnkitemid,:lnkitemunitid, :lnkmodifierid, :lnkmodifieritemid,:lnkmodiunitid, :sale_amount, :description, :created_user, :createddatetime, :companyid,:locationid)";
                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':lnkmodifierrelid',$relmodifierid);
                                        $dao->addParameter(':lnkitemid',$itemunkid);
                                        $dao->addParameter(':lnkitemunitid', $relunitid);
                                        $dao->addParameter(':lnkmodifierid',$val['modid']);
                                        $dao->addParameter(':lnkmodifieritemid',$key);
                                        $dao->addParameter(':lnkmodiunitid',$sub_val['mounit']);
                                        $dao->addParameter(':sale_amount',$sub_val['sale_amount']);
                                        $dao->addParameter(':description',$sub_val['description']);
                                        $dao->addParameter(':created_user',CONFIG_UID);
                                        $dao->addParameter(':createddatetime',$datetime);
                                        $dao->addParameter(':companyid',CONFIG_CID);
                                        $dao->addparameter(':locationid',CONFIG_LID);
                                        $dao->executeNonQuery();
                                    }
                                }
                            }
                        }

                        //Recipe
                        if(isset($sub_unit['recipe']) && count($sub_unit['recipe'])>0) {
                            $bind_recipe = $sub_unit['recipe'];
                            foreach ($bind_recipe AS $rkey => $sub_recipe) {

                                $unit = $sub_recipe['unitval'] * $sub_recipe['quantity'];

                                $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu_item_recipe (lnkitemid,lnkrawmaterialid,unit,lnkunitid,lnkstoreid,recipe_type,lnkitemunitid,created_user, createddatetime, companyid,locationid)";
                                $strSql .= "VALUE (:lnkitemid, :lnkrawmaterialid, :unit,:lnkunitid,:lnkstoreid,:recipe_type, :lnkitemunitid,:created_user, :createddatetime, :companyid, :locationid)";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':lnkitemid', $itemunkid);
                                $dao->addParameter(':lnkrawmaterialid', $rkey);
                                $dao->addParameter(':unit', $unit);
                                $dao->addParameter(':lnkunitid', $sub_recipe['unit']);
                                $dao->addParameter(':lnkstoreid', $sub_recipe['store']);
                                $dao->addParameter(':recipe_type', '1');
                                $dao->addParameter(':lnkitemunitid', $relunitid);
                                $dao->addParameter(':created_user', CONFIG_UID);
                                $dao->addParameter(':createddatetime', $datetime);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $dao->executeNonQuery();
                            }
                        }//Recipe
                    }
                }
                //

                //Bind Tax
                  if(isset($res['bind_tax']) && count($res['bind_tax'])>0 && $res['bind_tax']!='') {
                      $bind_tax = $res['bind_tax'];

                      foreach($bind_tax AS $tkey=>$tvalue) {
                          if(isset($tvalue['taxid']) && $tvalue['taxid']!='') {
                              $strSqlTax = "SELECT postingrule FROM ".CONFIG_DBN.".cftaxdetail WHERE taxunkid=:taxunkid ORDER BY entrydatetime DESC LIMIT 1";
                              $dao->initCommand($strSqlTax);
                              $dao->addParameter(':taxunkid', $tvalue['taxid']);
                              $tax_pr=$dao->executeRow();

                              $strSql = "INSERT INTO " . CONFIG_DBN . ".cfitemtax_rel(lnkitemid,lnktaxunkid,amount,taxapplyafter,postingrule,item_type,created_user, createddatetime,companyid,locationid)";
                              $strSql .= " VALUE (:lnkitemid, :lnktaxunkid, :amount,:taxapplyafter,:postingrule,:item_type,:created_user, :createddatetime,:companyid, :locationid)";
                              $dao->initCommand($strSql);
                              $dao->addParameter(':lnkitemid', $itemunkid);
                              $dao->addParameter(':lnktaxunkid', $tkey);
                              $dao->addParameter(':amount', $tvalue['amount']);
                              $dao->addParameter(':taxapplyafter', $tvalue['applytax']);
                              $dao->addParameter(':postingrule', $tax_pr['postingrule']);
                              $dao->addParameter(':item_type', '1');
                              $dao->addParameter(':created_user', CONFIG_UID);
                              $dao->addParameter(':createddatetime', $datetime);
                              $dao->addParameter(':companyid', CONFIG_CID);
                              $dao->addparameter(':locationid', CONFIG_LID);
                              $dao->executeNonQuery();
                          }
                      }
                  }
                //

                $ObjAuditDao->addactivitylog($res['module'],$title,$hashkey,$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_ADD_SUC)));
            }
            else
            {
                $title = "Edit Record";
                $category = $ObjCommonDao->getprimarykey('cfmenu_categories',$res['cat'],'categoryunkid');
                $itemtype = $ObjCommonDao->getprimarykey('cfmenu_itemtype',$res['itemtype'],'typeunkid');
                $serveunit = $ObjCommonDao->getprimarykey('cfmenu_itemunit',$res['serve_unit'],'unitunkid');

                $id = $ObjCommonDao->getprimarykey('cfmenu_items',$res['id'],'itemunkid');
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"itemname",$res['name'],$id);
                if($chk_name==1){
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG48)));
                }
                $chk_scode = $ObjDependencyDao->checkduplicaterecord($tablename,"sku",$res['sku'],$id);
                if($chk_scode==1){
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG47)));
                }

                $itemid = $ObjCommonDao->getprimarykey('cfmenu_items',$res['id'],'itemunkid');

                //  Check Combo Dependency
                if(count($res['bind_unit'])>0)
                {
                    $strSql = "SELECT lnkitemunitid FROM ".CONFIG_DBN.".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':lnkitemid',$itemid);
                    $dao->addParameter(':recipe_type', '1');
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $particuler_unit = $dao->executeQuery();

                    $unit_arr = \util\util::getoneDarray($particuler_unit,'lnkitemunitid');
                    $bind_unit = $res['bind_unit'];

                    if(count($bind_unit)>0)
                    {
                        foreach($bind_unit AS $ukey=>$sub_unit)
                        {
                            $strSql = "SELECT itmunitunkid FROM ".CONFIG_DBN.".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND recipe_type=:recipe_type AND lnkitemunitid=:lnkitemunitid AND companyid=:companyid AND locationid=:locationid";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':lnkitemid',$itemid);
                            $dao->addParameter(':recipe_type', '1');
                            $dao->addParameter(':lnkitemunitid',$ukey);
                            $dao->addParameter(':companyid',CONFIG_CID);
                            $dao->addparameter(':locationid',CONFIG_LID);
                            $particuler_unit = $dao->executeRow();

                            if (in_array($ukey, $unit_arr))
                            {
                                if(($unkey = array_search($ukey, $unit_arr)) !== false) {

                                    unset($unit_arr[$unkey]);
                                    $relunitid = $particuler_unit['itmunitunkid'];
                                }

                                //Modifier

                                $strSql = " SELECT lnkmodifierid,lnkmodiunitid FROM ".CONFIG_DBN.".fditem_modifier_relation WHERE lnkitemid=:itemid AND lnkitemunitid=:lnkitemunitid AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':itemid',$itemid);
                                $dao->addParameter(':lnkitemunitid',$relunitid);
                                $dao->addParameter(':companyid',CONFIG_CID);
                                $dao->addparameter(':locationid',CONFIG_LID);
                                $item_modifier = $dao->executeQuery();

                                $mod_arr = array();
                                $modunit_arr = array();
                                if(isset($item_modifier) && count($item_modifier) >0)
                                {
                                    $mod_arr = \util\util::getoneDarray($item_modifier,'lnkmodifierid');
                                    $modunit_arr = \util\util::getoneDarray($item_modifier,'lnkmodiunitid');
                                }

                                if(isset($sub_unit['modifier']) && count($sub_unit['modifier'])>0)
                                {
                                    $bind_modifier = $sub_unit['modifier'];
                                    foreach($bind_modifier AS $val)
                                    {
                                        $modid = $val['modid'];
                                        $mounit = 0;
                                        if($val['mounit'] != '') {
                                            $mounit = $val['mounit'];
                                        }

                                        $strSql = "SELECT relationunkid FROM ".CONFIG_DBN.".fditem_modifier_relation WHERE lnkitemid=:itemid AND lnkitemunitid=:lnkitemunitid AND lnkmodifierid=:lnkmodifierid AND companyid=:companyid AND locationid=:locationid";
                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':itemid',$itemid);
                                        $dao->addParameter(':lnkitemunitid',$relunitid);
                                        $dao->addParameter(':lnkmodifierid',$modid);
                                        $dao->addParameter(':companyid',CONFIG_CID);
                                        $dao->addparameter(':locationid',CONFIG_LID);
                                        $perticuler_mod = $dao->executeRow();

                                            if (in_array($modid, $mod_arr) && in_array($mounit, $modunit_arr)) {
                                                if (($key = array_search($modid, $mod_arr)) !== false) {
                                                    unset($mod_arr[$key]);
                                                }
                                                if (($mounit = array_search($mounit, $modunit_arr)) !== false) {
                                                    unset($modunit_arr[$mounit]);
                                                }
                                                $relmodifierid = $perticuler_mod['relationunkid'];

                                            }

                                        $strSql = "SELECT lnkmodiunitid,lnkmodifieritemid FROM ".CONFIG_DBN.".fdmodifier_item_relation WHERE lnkmodifierrelid=:lnkmodifierrelid AND lnkitemid=:lnkitemid AND lnkitemunitid=:lnkitemunitid AND companyid=:companyid AND locationid=:locationid";
                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':lnkitemid',$itemid);
                                        $dao->addParameter(':lnkitemunitid',$relunitid);
                                        $dao->addParameter(':lnkmodifierrelid',$relmodifierid);
                                        $dao->addParameter(':companyid',CONFIG_CID);
                                        $dao->addparameter(':locationid',CONFIG_LID);
                                        $perticuler_mod_item = $dao->executeQuery();

                                        $mod_item_arr = array();
                                        $mod_item_unit_arr = array();
                                        if(isset($perticuler_mod_item) && count($perticuler_mod_item) >0){

                                            $mod_item_arr = \util\util::getoneDarray($perticuler_mod_item,'lnkmodifieritemid');
                                            $mod_item_unit_arr = \util\util::getoneDarray($perticuler_mod_item,'lnkmodiunitid');

                                        }

                                        $bind_modifier_item = isset($val['modifier_item'])?$val['modifier_item']:array();
                                        if(count($bind_modifier_item)>0)
                                        {
                                            foreach($bind_modifier_item AS $key=>$sub_val)
                                            {
                                                $mo_item_unit = $sub_val['mounit'];
                                                if (in_array($key, $mod_item_arr) && in_array($mo_item_unit, $mod_item_unit_arr))
                                                {
                                                    if(($key = array_search($key, $mod_item_arr)) !== false) {
                                                        unset($mod_item_arr[$key]);
                                                    }
                                                    if(($key = array_search($mo_item_unit, $mod_item_unit_arr)) !== false) {
                                                        unset($mod_item_unit_arr[$key]);
                                                    }
                                                }
                                            }
                                        }
                                        if(count($mod_item_arr)>0){

                                            $strid = implode(",",$mod_item_arr);

                                            if(count($mod_item_unit_arr)>0) {
                                                $strunitid = implode(",", $mod_item_unit_arr);

                                                $check = $ObjDependencyDao->checkModifiersdependency($id, $ukey, $modid, '', $strid, $strunitid);

                                                if ($check > 0) {
                                                    return json_encode(array("Success" => "False", "Message" => $defaultlanguageArr->PLZ_REM_REL_RECORD));
                                                }
                                            }

                                        }
                                    }
                                    if(count($mod_arr)>0){

                                        $strid = implode(",",$mod_arr);

                                        if(count($modunit_arr)>0) {
                                            $strunitid = implode(",", $modunit_arr);

                                            $check = $ObjDependencyDao->checkModifiersdependency($id, $ukey, $strid, $strunitid, '', '');
                                            if ($check > 0) {
                                                return json_encode(array("Success" => "False", "Message" => $defaultlanguageArr->PLZ_REM_REL_RECORD));
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if(count($mod_arr)>0){

                                        $strid = implode(",",$mod_arr);

                                        if(count($modunit_arr)>0) {
                                            $strunitid = implode(",", $modunit_arr);

                                            $check = $ObjDependencyDao->checkModifiersdependency($id, $ukey, $strid, $strunitid, '', '');
                                            if ($check > 0) {
                                                return json_encode(array("Success" => "False", "Message" => $defaultlanguageArr->PLZ_REM_REL_RECORD));
                                            }
                                        }
                                    }
                                }
                                //End of unit
                            }

                        }
                    }//End of unit if

                    if(count($unit_arr)>0){

                        $strid = implode(",",$unit_arr);

                        $check = $ObjDependencyDao->checkItemUnitdependency($id,$strid);

                        if ($check > 0) {
                           return json_encode(array("Success" => "False", "Message" => $defaultlanguageArr->PLZ_REM_REL_RECORD));
                        }

                    }
                }
                //  Check Combo Dependency


                $strSql = "UPDATE ".CONFIG_DBN.".cfmenu_items SET itemname=:txtname,
                           categoryunkid=:txtcategory,itemtypeunkid=:itemtype,sale_amount=:txtsaleamount,
                           sku=:txtsku,spicy_type=:spicy_type,long_desc=:txtlongdescription,
                           image=:upload_img,serve_unit=:serve_unit,modifieddatetime=:modifieddatetime,
                           modified_user=:modified_user,type=:type,tax_apply_rate=:rates
                           WHERE hashkey=:itemunkid AND companyid=:companyid AND locationid=:locationid";

                $dao->initCommand($strSql);
                $dao->addParameter(':itemunkid',$res['id']);
                $dao->addParameter(':txtname',$res['name']);
                $dao->addParameter(':txtcategory',$category);
                $dao->addParameter(':itemtype',$itemtype);
                $dao->addParameter(':txtsaleamount',$res['saleamount']);
                $dao->addParameter(':txtsku',$res['sku']);
                $dao->addParameter(':spicy_type',$res['spicy_type']);
                $dao->addParameter(':txtlongdescription',$res['longdesc']);
                $dao->addParameter(':upload_img', ($res['image'] != '' ? $res['image'] : NULL));
                $dao->addParameter(':serve_unit',$serveunit);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':type',$res['item_type']);
                $dao->addParameter(':rates',$res['rates']);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();
                

                //Bind Tax
                if(isset($res['bind_tax']) && count($res['bind_tax'])>0 && $res['bind_tax']!='') {
                    $strSql = "SELECT itmtaxrelid FROM ".CONFIG_DBN.".cfitemtax_rel WHERE lnkitemid=:lnkitemid AND item_type=:item_type AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':lnkitemid',$itemid);
                    $dao->addParameter(':item_type', '1');
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $particuler_tax = $dao->executeQuery();

                    $tax_arr = \util\util::getoneDarray($particuler_tax,'itmtaxrelid');
                    $bind_tax = $res['bind_tax'];

                    if(count($bind_tax)>0) {
                        foreach ($bind_tax AS $tkey => $tvalue) {
                            if (isset($tvalue['taxid']) && $tvalue['taxid'] != '') {

                            $strSqlTax = "SELECT postingrule FROM ".CONFIG_DBN.".cftaxdetail WHERE taxunkid=:taxunkid  ORDER BY entrydatetime DESC LIMIT 1";
                            $dao->initCommand($strSqlTax);
                            $dao->addParameter(':taxunkid', $tvalue['taxid']);
                            $tax_pr=$dao->executeRow();

                                $strSql = "SELECT itmtaxrelid FROM " . CONFIG_DBN . ".cfitemtax_rel WHERE lnkitemid=:lnkitemid AND lnktaxunkid=:lnktaxunkid AND item_type=:item_type AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':lnkitemid', $itemid);
                                $dao->addParameter(':lnktaxunkid', $tvalue['taxid']);
                                $dao->addParameter(':item_type', '1');
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $particuler_tax = $dao->executeRow();

                                if (in_array($particuler_tax['itmtaxrelid'], $tax_arr)) {

                                    $strSql = "UPDATE " . CONFIG_DBN . ".cfitemtax_rel SET amount=:amount, taxapplyafter=:taxapplyafter,item_type=:item_type,modified_user=:modified_user,postingrule=:postingrule,modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND item_type=:item_type AND lnktaxunkid=:lnktaxunkid
                                           AND locationid=:locationid AND lnkitemid=:lnkitemid";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':lnkitemid', $itemid);
                                    $dao->addParameter(':lnktaxunkid', $tvalue['taxid']);
                                    $dao->addParameter(':amount', $tvalue['amount']);
                                    $dao->addParameter(':taxapplyafter', $tvalue['applytax']);
                                    $dao->addParameter(':postingrule', $tax_pr['postingrule']);
                                    $dao->addParameter(':item_type', '1');
                                    $dao->addParameter(':modified_user', CONFIG_UID);
                                    $dao->addParameter(':modifieddatetime', $datetime);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $dao->executeNonQuery();
                                    if (($tkey = array_search($particuler_tax['itmtaxrelid'], $tax_arr)) !== false) {
                                        unset($tax_arr[$tkey]);
                                    }
                                } else {
                                    $strSql = "INSERT INTO " . CONFIG_DBN . ".cfitemtax_rel(lnkitemid,lnktaxunkid,amount,taxapplyafter,postingrule,item_type,created_user, createddatetime,companyid,locationid)";
                                    $strSql .= " VALUE (:lnkitemid, :lnktaxunkid, :amount,:taxapplyafter,:postingrule,:item_type,:created_user, :createddatetime,:companyid, :locationid)";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':lnkitemid', $itemid);
                                    $dao->addParameter(':lnktaxunkid', $tvalue['taxid']);
                                    $dao->addParameter(':amount', $tvalue['amount']);
                                    $dao->addParameter(':taxapplyafter', $tvalue['applytax']);
                                    $dao->addParameter(':postingrule', $tax_pr['postingrule']);
                                    $dao->addParameter(':item_type', '1');
                                    $dao->addParameter(':created_user', CONFIG_UID);
                                    $dao->addParameter(':createddatetime', $datetime);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $dao->executeNonQuery();
                                }
                            }
                        }
                    }
                    if(count($tax_arr)>0){
                        $strid = implode(",",$tax_arr);
                        $strSql = "DELETE FROM ".CONFIG_DBN.".cfitemtax_rel WHERE lnkitemid=:lnkitemid AND itmtaxrelid IN (".$strid.") AND item_type=:item_type AND companyid=:companyid AND locationid=:locationid";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':lnkitemid',$itemid);
                        $dao->addParameter(':item_type', '1');
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $dao->addparameter(':locationid',CONFIG_LID);
                        $dao->executeNonQuery();
                    }
                }
                else{
                    $strSql = "DELETE FROM ".CONFIG_DBN.".cfitemtax_rel WHERE lnkitemid=:id AND item_type=:item_type AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":id",$itemid);
                    $dao->addParameter(':item_type', '1');
                    $dao->addParameter(":companyid",CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $dao->executeNonQuery();
                }
                //

                //Bind unit
                if(count($res['bind_unit'])>0)
                {
                    $strSql = "SELECT itmunitunkid FROM ".CONFIG_DBN.".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':lnkitemid',$itemid);
                    $dao->addParameter(':recipe_type', '1');
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $particuler_unit = $dao->executeQuery();

                    $unit_arr = \util\util::getoneDarray($particuler_unit,'itmunitunkid');
                    $bind_unit = $res['bind_unit'];

                    if(count($bind_unit)>0)
                    {
                        foreach($bind_unit AS $ukey=>$sub_unit)
                        {
                            $strSql = "SELECT itmunitunkid FROM ".CONFIG_DBN.".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND recipe_type=:recipe_type AND lnkitemunitid=:lnkitemunitid AND companyid=:companyid AND locationid=:locationid";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':lnkitemid',$itemid);
                            $dao->addParameter(':recipe_type', '1');
                            $dao->addParameter(':lnkitemunitid',$ukey);
                            $dao->addParameter(':companyid',CONFIG_CID);
                            $dao->addparameter(':locationid',CONFIG_LID);
                            $particuler_unit = $dao->executeRow();

                            if (in_array($particuler_unit['itmunitunkid'], $unit_arr))
                            {

                                $strSql = "UPDATE ".CONFIG_DBN.".cfmenu_itemunit_rel SET lnkitemid=:lnkitemid, lnkitemunitid=:lnkitemunitid, rate1=:rate1, rate2=:rate2,rate3=:rate3,rate4=:rate4,
                                           default_rate=:defaultrate,modified_user=:modified_user, modifieddatetime=:modifieddatetime WHERE recipe_type=:recipe_type AND companyid=:companyid AND itmunitunkid=:itmunitunkid
                                           AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':itmunitunkid',$particuler_unit['itmunitunkid']);
                                $dao->addParameter(':lnkitemid', $itemid);
                                $dao->addParameter(':lnkitemunitid', $ukey);
                                $dao->addParameter(':rate1', $sub_unit['rate1']);
                                $dao->addParameter(':rate2', $sub_unit['rate2']);
                                $dao->addParameter(':rate3', $sub_unit['rate3']);
                                $dao->addParameter(':rate4', $sub_unit['rate4']);
                                $dao->addParameter(':defaultrate', $sub_unit['defaultrate']);
                                $dao->addParameter(':modified_user',CONFIG_UID);
                                $dao->addParameter(':modifieddatetime',$datetime);
                                $dao->addParameter(':recipe_type', '1');
                                $dao->addParameter(':companyid',CONFIG_CID);
                                $dao->addparameter(':locationid',CONFIG_LID);
                                $dao->executeNonQuery();
                                $relunitid = $particuler_unit['itmunitunkid'];
                                if(($ukey = array_search($particuler_unit['itmunitunkid'], $unit_arr)) !== false) {
                                    unset($unit_arr[$ukey]);
                                }
                            }
                            else {

                                $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu_itemunit_rel(lnkitemid,lnkitemunitid,rate1,rate2,rate3,rate4,default_rate,created_user, createddatetime,recipe_type,companyid,locationid)";
                                $strSql .= " VALUE (:lnkitemid, :lnkitemunitid, :rate1,:rate2,:rate3,:rate4,:defaultrate,:created_user, :createddatetime,:recipe_type,:companyid, :locationid)";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':lnkitemid', $itemid);
                                $dao->addParameter(':lnkitemunitid', $ukey);
                                $dao->addParameter(':rate1', $sub_unit['rate1']);
                                $dao->addParameter(':rate2', $sub_unit['rate2']);
                                $dao->addParameter(':rate3', $sub_unit['rate3']);
                                $dao->addParameter(':rate4', $sub_unit['rate4']);
                                $dao->addParameter(':defaultrate', $sub_unit['defaultrate']);
                                $dao->addParameter(':created_user', CONFIG_UID);
                                $dao->addParameter(':createddatetime', $datetime);
                                $dao->addParameter(':recipe_type', '1');
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $dao->executeNonQuery();
                                $relunitid = $dao->getLastInsertedId();
                            }// Unit

                            //Modifier
                            if(isset($sub_unit['modifier']) && count($sub_unit['modifier'])>0)
                            {
                                $strSql = " SELECT relationunkid FROM ".CONFIG_DBN.".fditem_modifier_relation WHERE lnkitemid=:itemid AND lnkitemunitid=:lnkitemunitid AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':itemid',$itemid);
                                $dao->addParameter(':lnkitemunitid',$relunitid);
                                $dao->addParameter(':companyid',CONFIG_CID);
                                $dao->addparameter(':locationid',CONFIG_LID);
                                $item_modifier = $dao->executeQuery();
                                $mod_arr = \util\util::getoneDarray($item_modifier,'relationunkid');
                                $bind_modifier = $sub_unit['modifier'];
                                foreach($bind_modifier AS $val)
                                {
                                    $modid = $val['modid'];
                                    $strSql = "SELECT relationunkid FROM ".CONFIG_DBN.".fditem_modifier_relation WHERE lnkitemid=:itemid AND lnkitemunitid=:lnkitemunitid AND lnkmodifierid=:lnkmodifierid AND companyid=:companyid AND locationid=:locationid";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':itemid',$itemid);
                                    $dao->addParameter(':lnkitemunitid',$relunitid);
                                    $dao->addParameter(':lnkmodifierid',$modid);
                                    $dao->addParameter(':companyid',CONFIG_CID);
                                    $dao->addparameter(':locationid',CONFIG_LID);
                                    $perticuler_mod = $dao->executeRow();
                                    if (in_array($perticuler_mod['relationunkid'], $mod_arr))
                                    {
                                        $strSql = " UPDATE ".CONFIG_DBN.".fditem_modifier_relation SET lnkitemid=:lnkitemid,lnkitemunitid=:lnkitemunitid, lnkmodifierid=:lnkmodifierid, min=:min, max=:max,lnkmodiunitid=:lnkmodiunitid,sale_amount=:sale_amount, modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND relationunkid=:relationunkid AND locationid=:locationid";

                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':relationunkid',$perticuler_mod['relationunkid']);
                                        $dao->addParameter(':lnkitemid',$itemid);
                                        $dao->addParameter(':lnkitemunitid',$relunitid);
                                        $dao->addParameter(':lnkmodifierid',$modid);
                                        $dao->addParameter(':min',$val['min']);
                                        $dao->addParameter(':max',$val['max']);
                                        $dao->addParameter(':lnkmodiunitid',$val['mounit']);
                                        $dao->addParameter(':sale_amount',$val['sale_amount']);
                                        $dao->addParameter(':modified_user',CONFIG_UID);
                                        $dao->addParameter(':modifieddatetime',$datetime);
                                        $dao->addParameter(':companyid',CONFIG_CID);
                                        $dao->addparameter(':locationid',CONFIG_LID);
                                        $dao->executeNonQuery();

                                        $relmodifierid = $perticuler_mod['relationunkid'];
                                        if(($key = array_search($relmodifierid, $mod_arr)) !== false) {
                                            unset($mod_arr[$key]);
                                        }
                                    }
                                    else{
                                        $strSql = " INSERT INTO ".CONFIG_DBN.".fditem_modifier_relation (lnkitemid,lnkitemunitid, lnkmodifierid, min, max,lnkmodiunitid,sale_amount,created_user,createddatetime, companyid,locationid)";
                                        $strSql .= " VALUE (:lnkitemid,:lnkitemunitid,:lnkmodifierid, :min, :max,:lnkmodiunitid, :sale_amount, :created_user, :createddatetime, :companyid,:locationid)";
                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':lnkitemid',$itemid);
                                        $dao->addParameter(':lnkitemunitid',$relunitid);
                                        $dao->addParameter(':lnkmodifierid',$modid);
                                        $dao->addParameter(':min',$val['min']);
                                        $dao->addParameter(':max',$val['max']);
                                        $dao->addParameter(':lnkmodiunitid',$val['mounit']);
                                        $dao->addParameter(':sale_amount',$val['sale_amount']);
                                        $dao->addParameter(':created_user',CONFIG_UID);
                                        $dao->addParameter(':createddatetime',$datetime);
                                        $dao->addParameter(':companyid',CONFIG_CID);
                                        $dao->addparameter(':locationid',CONFIG_LID);
                                        $dao->executeNonQuery();
                                        $relmodifierid = $dao->getLastInsertedId();
                                    }

                                    $strSql = "SELECT relationunkid FROM ".CONFIG_DBN.".fdmodifier_item_relation WHERE lnkmodifierrelid=:lnkmodifierrelid AND lnkitemid=:lnkitemid AND lnkitemunitid=:lnkitemunitid AND companyid=:companyid AND locationid=:locationid";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':lnkitemid',$itemid);
                                    $dao->addParameter(':lnkitemunitid',$relunitid);
                                    $dao->addParameter(':lnkmodifierrelid',$relmodifierid);
                                    $dao->addParameter(':companyid',CONFIG_CID);
                                    $dao->addparameter(':locationid',CONFIG_LID);
                                    $perticuler_mod_item = $dao->executeQuery();

                                    $mod_item_arr = \util\util::getoneDarray($perticuler_mod_item,'relationunkid');

                                    $bind_modifier_item = isset($val['modifier_item'])?$val['modifier_item']:array();
                                    if(count($bind_modifier_item)>0)
                                    {
                                        foreach($bind_modifier_item AS $key=>$sub_val)
                                        {
                                            $strSql = "SELECT relationunkid FROM ".CONFIG_DBN.".fdmodifier_item_relation WHERE lnkmodifierrelid=:lnkmodifierrelid AND lnkitemid=:lnkitemid AND lnkitemunitid=:lnkitemunitid AND lnkmodifieritemid=:lnkmodifieritemid AND companyid=:companyid AND locationid=:locationid";
                                            $dao->initCommand($strSql);
                                            $dao->addParameter(':lnkitemid',$itemid);
                                            $dao->addParameter(':lnkitemunitid',$relunitid);
                                            $dao->addParameter(':lnkmodifierrelid',$relmodifierid);
                                            $dao->addParameter(':lnkmodifieritemid',$key);
                                            $dao->addParameter(':companyid',CONFIG_CID);
                                            $dao->addparameter(':locationid',CONFIG_LID);
                                            $perticuler_itm_mod = $dao->executeRow();

                                            if (in_array($perticuler_itm_mod['relationunkid'], $mod_item_arr))
                                            {
                                                $strSql = "UPDATE ".CONFIG_DBN.".fdmodifier_item_relation SET lnkmodifierrelid=:lnkmodifierrelid, lnkitemid=:lnkitemid,lnkitemunitid=:lnkitemunitid, lnkmodifierid=:lnkmodifierid, lnkmodifieritemid=:lnkmodifieritemid,lnkmodiunitid=:lnkmodiunitid,sale_amount=:sale_amount, description=:description, modified_user=:modified_user, modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND relationunkid=:relationunkid AND locationid=:locationid";
                                                $dao->initCommand($strSql);
                                                $dao->addParameter(':relationunkid',$perticuler_itm_mod['relationunkid']);
                                                $dao->addParameter(':lnkmodifierrelid',$relmodifierid);
                                                $dao->addParameter(':lnkitemid',$itemid);
                                                $dao->addParameter(':lnkitemunitid',$relunitid);
                                                $dao->addParameter(':lnkmodifierid',$modid);
                                                $dao->addParameter(':lnkmodifieritemid',$key);
                                                $dao->addParameter(':lnkmodiunitid',$sub_val['mounit']);
                                                $dao->addParameter(':sale_amount',$sub_val['sale_amount']);
                                                $dao->addParameter(':description',$sub_val['description']);
                                                $dao->addParameter(':modified_user',CONFIG_UID);
                                                $dao->addParameter(':modifieddatetime',$datetime);
                                                $dao->addParameter(':companyid',CONFIG_CID);
                                                $dao->addparameter(':locationid',CONFIG_LID);
                                                $dao->executeNonQuery();
                                                if(($key = array_search($perticuler_itm_mod['relationunkid'], $mod_item_arr)) !== false) {
                                                    unset($mod_item_arr[$key]);
                                                }
                                            }
                                            else{
                                                $strSql = "INSERT INTO ".CONFIG_DBN.".fdmodifier_item_relation (lnkmodifierrelid,lnkitemid,lnkitemunitid, lnkmodifierid,lnkmodifieritemid, lnkmodiunitid,sale_amount, description, created_user, createddatetime, companyid,locationid)";
                                                $strSql .= " VALUE (:lnkmodifierrelid, :lnkitemid,:lnkitemunitid, :lnkmodifierid, :lnkmodifieritemid,:lnkmodiunitid,:sale_amount, :description, :created_user, :createddatetime, :companyid,:locationid)";
                                                $dao->initCommand($strSql);
                                                $dao->addParameter(':lnkmodifierrelid',$relmodifierid);
                                                $dao->addParameter(':lnkitemid',$itemid);
                                                $dao->addParameter(':lnkitemunitid',$relunitid);
                                                $dao->addParameter(':lnkmodifierid',$modid);
                                                $dao->addParameter(':lnkmodifieritemid',$key);
                                                $dao->addParameter(':lnkmodiunitid',$sub_val['mounit']);
                                                $dao->addParameter(':sale_amount',$sub_val['sale_amount']);
                                                $dao->addParameter(':description',$sub_val['description']);
                                                $dao->addParameter(':created_user',CONFIG_UID);
                                                $dao->addParameter(':createddatetime',$datetime);
                                                $dao->addParameter(':companyid',CONFIG_CID);
                                                $dao->addparameter(':locationid',CONFIG_LID);
                                                $dao->executeNonQuery();
                                            }
                                        }
                                    }
                                    if(count($mod_item_arr)>0){
                                        $strid = implode(",",$mod_item_arr);
                                        $strSql = "DELETE FROM ".CONFIG_DBN.".fdmodifier_item_relation WHERE lnkitemid=:lnkitemid AND lnkitemunitid=:lnkitemunitid AND lnkmodifierid=:lnkmodifierid AND relationunkid IN (".$strid.") AND companyid=:companyid AND locationid=:locationid";
                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':lnkitemid',$itemid);
                                        $dao->addParameter(':lnkitemunitid',$relunitid);
                                        $dao->addParameter(':lnkmodifierid',$modid);
                                        $dao->addParameter(':companyid',CONFIG_CID);
                                        $dao->addparameter(':locationid',CONFIG_LID);
                                        $dao->executeNonQuery();
                                    }
                                }
                                if(count($mod_arr)>0){

                                    $strid = implode(",",$mod_arr);
                                    $strSql = "DELETE FROM ".CONFIG_DBN.".fditem_modifier_relation WHERE lnkitemid=:lnkitemid AND lnkitemunitid=:lnkitemunitid AND relationunkid IN (".$strid.") AND companyid=:companyid AND locationid=:locationid";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':lnkitemid',$itemid);
                                    $dao->addParameter(':lnkitemunitid',$relunitid);
                                    $dao->addParameter(':companyid',CONFIG_CID);
                                    $dao->addparameter(':locationid',CONFIG_LID);
                                    $dao->executeNonQuery();
                                    $strSql = "DELETE FROM ".CONFIG_DBN.".fdmodifier_item_relation WHERE lnkitemid=:lnkitemid AND lnkitemunitid=:lnkitemunitid AND lnkmodifierrelid IN (".$strid.") AND companyid=:companyid AND locationid=:locationid";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':lnkitemid',$itemid);
                                    $dao->addParameter(':lnkitemunitid',$relunitid);
                                    $dao->addParameter(':companyid',CONFIG_CID);
                                    $dao->addparameter(':locationid',CONFIG_LID);
                                    $dao->executeNonQuery();
                                }
                            }
                            else
                            {
                                /*Delete From Item- Modifier Relation*/
                                $strSql = "DELETE FROM ".CONFIG_DBN.".fditem_modifier_relation WHERE lnkitemid=:id AND lnkitemunitid=:lnkitemunitid AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(":id",$itemid);
                                $dao->addParameter(':lnkitemunitid',$relunitid);
                                $dao->addParameter(":companyid",CONFIG_CID);
                                $dao->addparameter(':locationid',CONFIG_LID);
                                $dao->executeNonQuery();
                                /*Delete From Item- Modifier Relation*/
                                /*Delete From Item- Modifier Item Relation*/
                                $strSql = "DELETE FROM ".CONFIG_DBN.".fdmodifier_item_relation WHERE lnkitemid=:id AND lnkitemunitid=:lnkitemunitid AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(":id",$itemid);
                                $dao->addParameter(':lnkitemunitid',$relunitid);
                                $dao->addParameter(":companyid",CONFIG_CID);
                                $dao->addparameter(':locationid',CONFIG_LID);
                                $dao->executeNonQuery();
                                /*Delete From Item- Modifier Item Relation*/
                            }
                            //End of unit

                            //Recipe
                            if(isset($sub_unit['recipe']) && count($sub_unit['recipe'])>0)
                            {
                                $strSql = "SELECT recipeunkid FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:lnkitemid AND lnkitemunitid=:lnkitemunitid AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':lnkitemid',$itemid);
                                $dao->addParameter(':lnkitemunitid',$relunitid);
                                $dao->addParameter(':recipe_type', '1');
                                $dao->addParameter(':companyid',CONFIG_CID);
                                $dao->addparameter(':locationid',CONFIG_LID);
                                $particuler_recipe = $dao->executeQuery();

                                $recipe_arr = \util\util::getoneDarray($particuler_recipe,'recipeunkid');
                                $bind_recipe = $sub_unit['recipe'];

                                if(count($bind_recipe)>0)
                                {
                                    foreach($bind_recipe AS $rkey=>$sub_recipe)
                                    {
                                        $strSql = "SELECT recipeunkid FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:lnkitemid AND lnkitemunitid=:lnkitemunitid AND recipe_type=:recipe_type AND lnkrawmaterialid=:lnkrawmaterialid AND companyid=:companyid AND locationid=:locationid";
                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':lnkitemid',$itemid);
                                        $dao->addParameter(':lnkitemunitid',$relunitid);
                                        $dao->addParameter(':lnkrawmaterialid',$rkey);
                                        $dao->addParameter(':recipe_type', '1');
                                        $dao->addParameter(':companyid',CONFIG_CID);
                                        $dao->addparameter(':locationid',CONFIG_LID);
                                        $particuler_recipe = $dao->executeRow();

                                        $unit=$sub_recipe['unitval']*$sub_recipe['quantity'];

                                        if (in_array($particuler_recipe['recipeunkid'], $recipe_arr))
                                        {

                                            $strSql = "UPDATE ".CONFIG_DBN.".cfmenu_item_recipe SET lnkitemid=:lnkitemid,lnkitemunitid=:lnkitemunitid,lnkrawmaterialid=:lnkrawmaterialid, unit=:unit, lnkunitid=:lnkunitid,lnkstoreid=:lnkstoreid, modified_user=:modified_user, modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND recipeunkid=:recipeunkid AND locationid=:locationid";
                                            $dao->initCommand($strSql);
                                            $dao->addParameter(':recipeunkid',$particuler_recipe['recipeunkid']);
                                            $dao->addParameter(':lnkitemid',$itemid);
                                            $dao->addParameter(':lnkitemunitid',$relunitid);
                                            $dao->addParameter(':lnkrawmaterialid',$rkey);
                                            $dao->addParameter(':unit', $unit);
                                            $dao->addParameter(':lnkunitid',$sub_recipe['unit']);
                                            $dao->addParameter(':lnkstoreid',$sub_recipe['store']);
                                            $dao->addParameter(':modified_user',CONFIG_UID);
                                            $dao->addParameter(':modifieddatetime',$datetime);
                                            $dao->addParameter(':companyid',CONFIG_CID);
                                            $dao->addparameter(':locationid',CONFIG_LID);
                                            $dao->executeNonQuery();
                                            if(($rkey = array_search($particuler_recipe['recipeunkid'], $recipe_arr)) !== false) {
                                                unset($recipe_arr[$rkey]);
                                            }
                                        }
                                        else{

                                            $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu_item_recipe (lnkitemid,lnkitemunitid,lnkrawmaterialid,unit,lnkunitid,lnkstoreid,recipe_type,created_user, createddatetime, companyid,locationid)";
                                            $strSql .= "VALUE (:lnkitemid,:lnkitemunitid,:lnkrawmaterialid, :unit,:lnkunitid,:lnkstoreid,:recipe_type,:created_user, :createddatetime, :companyid, :locationid)";
                                            $dao->initCommand($strSql);
                                            $dao->addParameter(':lnkitemid', $itemid);
                                            $dao->addParameter(':lnkitemunitid',$relunitid);
                                            $dao->addParameter(':lnkrawmaterialid', $rkey);
                                            $dao->addParameter(':unit', $unit);
                                            $dao->addParameter(':lnkunitid',$sub_recipe['unit']);
                                            $dao->addParameter(':lnkstoreid',$sub_recipe['store']);
                                            $dao->addParameter(':recipe_type', '1');
                                            $dao->addParameter(':created_user', CONFIG_UID);
                                            $dao->addParameter(':createddatetime', $datetime);
                                            $dao->addParameter(':companyid', CONFIG_CID);
                                            $dao->addparameter(':locationid', CONFIG_LID);
                                            $dao->executeNonQuery();
                                        }
                                    }
                                }
                                    if(count($recipe_arr)>0){
                                        $strid = implode(",",$recipe_arr);
                                        $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:lnkitemid AND lnkitemunitid=:lnkitemunitid AND recipeunkid IN (".$strid.") AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':lnkitemid',$itemid);
                                        $dao->addParameter(':lnkitemunitid',$relunitid);
                                        $dao->addParameter(':recipe_type', '1');
                                        $dao->addParameter(':companyid',CONFIG_CID);
                                        $dao->addparameter(':locationid',CONFIG_LID);
                                        $dao->executeNonQuery();
                                    }
                                }
                            else
                                {
                                //Delete From Item- Recipe
                                $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:id AND lnkitemunitid=:lnkitemunitid AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(":id",$itemid);
                                $dao->addParameter(':lnkitemunitid',$relunitid);
                                $dao->addParameter(':recipe_type', '1');
                                $dao->addParameter(":companyid",CONFIG_CID);
                                $dao->addparameter(':locationid',CONFIG_LID);
                                $dao->executeNonQuery();
                                //Delete From Item- Recipe
                            }
                            //End of Recipe
                        }
                    }//End of unit if

                    if(count($unit_arr)>0){

                        $strid = implode(",",$unit_arr);
                        $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND itmunitunkid IN (".$strid.") AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':lnkitemid',$itemid);
                        $dao->addParameter(':recipe_type', '1');
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $dao->addparameter(':locationid',CONFIG_LID);
                        $dao->executeNonQuery();

                        $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:lnkitemid AND lnkitemunitid IN (".$strid.") AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':lnkitemid',$itemid);
                        $dao->addParameter(':recipe_type', '1');
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $dao->addparameter(':locationid',CONFIG_LID);
                        $dao->executeNonQuery();
                    }
                }
                else
                {
                    //Delete From Item- Unit
                    $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":lnkitemid",$itemid);
                    $dao->addParameter(':recipe_type', '1');
                    $dao->addParameter(":companyid",CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $dao->executeNonQuery();
                    //Delete From Item- Unit

                    //Delete From Item- Recipe
                    $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:id AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":id",$itemid);
                    $dao->addParameter(':recipe_type', '1');
                    $dao->addParameter(":companyid",CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $dao->executeNonQuery();
                    //Delete From Item- Recipe
                }
                // End of unit

                $ObjAuditDao->addactivitylog($res['module'],$title,$res['id'],$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_UP_SUC)));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - additem - '.$e);
        }
    }

    public function itemlist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - itemlist - '.$name);
            $dao = new \dao;
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT CFI.hashkey,CFI.itemname,CFI.is_active,IFNULL(CFU1.username,'') AS createduser,IFNULL(CFU2.username,'') AS modifieduser,
                        IFNULL(CASE 
                        WHEN CIUR.default_rate=1 THEN ROUND(CIUR.rate1,$round_off)
                        WHEN CIUR.default_rate=2 THEN ROUND(CIUR.rate2,$round_off)
                        WHEN CIUR.default_rate=3 THEN ROUND(CIUR.rate3,$round_off)
                        ELSE ROUND(CIUR.rate4,$round_off)
                        END ,'')as sale_amount,
                        IFNULL(DATE_FORMAT(CFI.createddatetime,'".$mysqlformat."'),'') as created_date,
                        IFNULL(DATE_FORMAT(CFI.modifieddatetime,'".$mysqlformat."'),'') as modified_date
                        FROM ".CONFIG_DBN.".cfmenu_items AS CFI                    
                         LEFT JOIN ".CONFIG_DBN.".cfmenu_itemunit_rel as CIUR ON CFI.itemunkid=CIUR.lnkitemid AND CFI.serve_unit=CIUR.lnkitemunitid AND CIUR.recipe_type=1
                        LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 
                        ON CFI.created_user=CFU1.userunkid AND CFI.companyid=CFU1.companyid 
                        LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON CFI.modified_user=CFU2.userunkid 
                        AND CFI.modified_user=CFU2.userunkid AND CFI.companyid=CFU2.companyid 
                        WHERE CFI.companyid=:companyid AND CFI.locationid=:locationid AND CFI.is_deleted=0";

            if($name!="")
                $strSql .= " AND itemname LIKE '%".$name."%'";
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $strSql.=" GROUP BY itemunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);          
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $rec = $dao->executeQuery();
            if(count($data) != 0){               
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue));
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - itemlist - '.$e);
        }
    }

    public function getrec($data)
    {
        try
        {
            $this->log->logIt($this->module." - getrec") ;
            $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $bucket='';
            $hashkey=(isset($data['id']))?$data['id']:"";
            
            $strSql = "SELECT CFI.itemname,CFI.long_desc,IFNULL(CFI.image,'') as image,IFNULL(CIT.hashkey,0) as typeid,CFI.tax_apply_rate,CFI.sale_amount,
                       CIU.hashkey as serve_unit,CFI.sku,CFI.spicy_type,CFI.is_active,CFI.type,CFC.hashkey AS catid";
            $strSql .= " FROM ";
            $strSql .= CONFIG_DBN.".cfmenu_items AS CFI 
                        INNER JOIN ".CONFIG_DBN.".cfmenu_categories AS CFC ON CFC.categoryunkid = CFI.categoryunkid AND CFC.companyid=:companyid
                        LEFT JOIN ".CONFIG_DBN.".cfmenu_itemunit AS CIU ON CIU.unitunkid = CFI.serve_unit AND CIU.companyid=:companyid and CIU.locationid=:locationid
                        LEFT JOIN ".CONFIG_DBN.".cfmenu_itemtype AS CIT ON CIT.typeunkid = CFI.itemtypeunkid AND CIT.companyid=:companyid
                        WHERE CFI.hashkey=:hashkey AND CFI.companyid=:companyid AND CFI.locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':hashkey',$hashkey);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeRow();

            $response_array = array();
            $response_array['itmdata'] = $res;
            $response_array['bind_unit'] = array();
            $response_array['bind_tax'] = array();
            $response_arr['bind_modifier'] = array();

            $ObjCommonDao = new \database\commondao;
            $itemunkid = $ObjCommonDao->getprimarykey('cfmenu_items',$hashkey,'itemunkid');

            $strSql = "SELECT CIU.hashkey as unithash,CIUR.itmunitunkid,
                  IFNULL(ROUND(CIUR.rate1,$round_off),'') as rate1,IFNULL(ROUND(CIUR.rate2,$round_off),'') as rate2,
                  IFNULL(ROUND(CIUR.rate3,$round_off),'') as rate3,IFNULL(ROUND(CIUR.rate4,$round_off),'') as rate4,
                  CIUR.default_rate,CIU.name,CIUR.lnkitemunitid
                  FROM ".CONFIG_DBN.".cfmenu_itemunit_rel as CIUR
                  INNER JOIN ".CONFIG_DBN.".cfmenu_itemunit AS CIU
                  ON CIU.unitunkid=CIUR.lnkitemunitid                
                  WHERE CIUR.lnkitemid=:itemid AND CIUR.recipe_type=:recipe_type AND CIUR.companyid=:companyid AND CIUR.locationid=:locationid";

            $dao->initCommand($strSql);
            $dao->addParameter(':itemid',$itemunkid);
            $dao->addParameter(':recipe_type', '1');
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $unitdata = $dao->executeQuery();
            foreach ($unitdata as $key=>$val){

                    $strSql="SELECT FDIM.min, FDIM.max,FDIM.lnkmodiunitid,ROUND(FDIM.sale_amount,$round_off)as sale_amount,CFM.is_included,CFM.modifiername,CFM.modifierunkid AS modifier_hash,FDMI.lnkmodiunitid AS mi_modiunitid, ROUND(FDMI.sale_amount,$round_off) as mi_sale_amount, FDMI.description AS mi_description, CFMI.modifieritemunkid AS mi_hashkey";
                    $strSql.="  FROM ".CONFIG_DBN.".fditem_modifier_relation AS FDIM ";
                    $strSql.="  INNER JOIN ".CONFIG_DBN.".cfmenu_modifiers AS CFM ON FDIM.lnkmodifierid = CFM.modifierunkid AND CFM.companyid = :companyid AND CFM.locationid=:locationid";
                    $strSql.="  LEFT JOIN ".CONFIG_DBN.".fdmodifier_item_relation AS FDMI ON FDMI.lnkmodifierrelid = FDIM.relationunkid AND FDMI.lnkitemid = FDIM.lnkitemid AND FDIM.companyid = :companyid AND FDIM.locationid=:locationid";
                    $strSql.="  LEFT JOIN ".CONFIG_DBN.".cfmenu_modifier_items AS CFMI ON CFMI.modifieritemunkid = FDMI.lnkmodifieritemid AND CFMI.companyid = :companyid AND CFMI.locationid=:locationid";
                    $strSql.="  WHERE FDIM.lnkitemid = :itemid AND FDIM.lnkitemunitid=:lnkitemunitid AND FDIM.companyid = :companyid AND FDIM.locationid=:locationid";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':itemid',$itemunkid);
                    $dao->addParameter(':lnkitemunitid',$val['itmunitunkid']);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $data = $dao->executeQuery();

                    $modifier_arr=array();
                    $arr_maintain = array();
                    $cnt = 0;
                    /*  foreach($data as $mkey=>$mvalue){

                        $this->log->logIt("Mkey ::".$mkey);
                        $this->log->logIt("Mvalue ::".json_encode($mvalue));

                        if($mkey==0) {
                            $modifier_arr[$cnt]['mod_name'] = $mvalue['modifiername'];
                            $modifier_arr[$cnt]['mod_min'] = $mvalue['min'];
                            $modifier_arr[$cnt]['mod_max'] = $mvalue['max'];
                            $modifier_arr[$cnt]['mod_sale_amount'] = $mvalue['sale_amount'];
                            $modifier_arr[$cnt]['mod_is_included'] = $mvalue['is_included'];
                            $modifier_arr[$cnt]['mod_hash'] = $mvalue['modifier_hash'];
                            $arr_maintain[$mvalue['modifier_hash']] = $cnt;
                            $modifier_arr[$cnt]['mi_hashkeys'] = "";
                        }
                        else if($mkey!=0 && isset($arr_maintain[$mkey['modifier_hash']])!=1)
                        {
                            $cnt++;
                            $modifier_arr[$cnt]['mod_name'] = $mvalue['modifiername'];
                            $modifier_arr[$cnt]['mod_min'] = $mvalue['min'];
                            $modifier_arr[$cnt]['mod_max'] = $mvalue['max'];
                            $modifier_arr[$cnt]['mod_sale_amount'] = $mvalue['sale_amount'];
                            $modifier_arr[$cnt]['mod_is_included'] = $mvalue['is_included'];
                            $modifier_arr[$cnt]['mod_hash'] = $mvalue['modifier_hash'];
                            $arr_maintain[$mvalue['modifier_hash']] = $cnt;
                            $modifier_arr[$cnt]['mi_hashkeys'] = "";
                        }

                        $index = $arr_maintain[$mvalue['modifier_hash']];
                        if($modifier_arr[$index]['mi_hashkeys']=="")
                            $modifier_arr[$index]['mi_hashkeys'] .=$mvalue['mi_hashkey'];

                        else
                            $modifier_arr[$index]['mi_hashkeys'] .= ",".$mvalue['mi_hashkey'];

                        if($mvalue['mi_hashkey']!="" && $mvalue['mi_itemname']!="" && $mvalue['mi_sale_amount']!="")
                        {
                            $modifier_arr[$index]['modifieritem'][] = array(
                                "mi_hashkey"=>$mvalue['mi_hashkey'],
                                "mi_itemname"=> $mvalue['mi_itemname'],
                                "mi_sale_amount"=> $mvalue['mi_sale_amount'],
                                "mi_description"=> $mvalue['mi_description'],
                            );
                        }
                        else{
                            $modifier_arr[$index]['modifieritem'] = array();
                        }
                    }*/

                for($i=0; $i<count($data); $i++)
                {
                    if($i==0)
                    {
                        $modifier_arr[$cnt]['mod_name'] = $data[$i]['modifiername'];
                        $modifier_arr[$cnt]['mod_min'] = $data[$i]['min'];
                        $modifier_arr[$cnt]['mod_max'] = $data[$i]['max'];
                        $modifier_arr[$cnt]['mod_unit'] = $data[$i]['lnkmodiunitid'];
                        $modifier_arr[$cnt]['mod_sale_amount'] = $data[$i]['sale_amount'];
                        $modifier_arr[$cnt]['mod_is_included'] = $data[$i]['is_included'];
                        $modifier_arr[$cnt]['mod_hash'] = $data[$i]['modifier_hash'];
                        $arr_maintain[$data[$i]['modifier_hash']] = $cnt;
                        $modifier_arr[$cnt]['mi_hashkeys'] = "";
                    }
                    else if($i!=0 && isset($arr_maintain[$data[$i]['modifier_hash']])!=1)
                    {
                        $cnt++;
                        $modifier_arr[$cnt]['mod_name'] = $data[$i]['modifiername'];
                        $modifier_arr[$cnt]['mod_min'] = $data[$i]['min'];
                        $modifier_arr[$cnt]['mod_max'] = $data[$i]['max'];
                        $modifier_arr[$cnt]['mod_unit'] = $data[$i]['lnkmodiunitid'];
                        $modifier_arr[$cnt]['mod_sale_amount'] = $data[$i]['sale_amount'];
                        $modifier_arr[$cnt]['mod_is_included'] = $data[$i]['is_included'];
                        $modifier_arr[$cnt]['mod_hash'] = $data[$i]['modifier_hash'];
                        $arr_maintain[$data[$i]['modifier_hash']] = $cnt;
                        $modifier_arr[$cnt]['mi_hashkeys'] = "";
                    }
                    $index = $arr_maintain[$data[$i]['modifier_hash']];

                    if($modifier_arr[$index]['mi_hashkeys']=="")
                        $modifier_arr[$index]['mi_hashkeys'] .=$data[$i]['mi_hashkey'];
                    else
                        $modifier_arr[$index]['mi_hashkeys'] .= ",".$data[$i]['mi_hashkey'];

                    if($data[$i]['mi_hashkey']!="" && $data[$i]['mi_modiunitid']!="" && $data[$i]['mi_sale_amount']!="")
                    {
                        //$mi_description = \util\util::removeptag($data[$i]['mi_description']);
                        $modifier_arr[$index]['modifieritem'][] = array(
                            "mi_hashkey"=>$data[$i]['mi_hashkey'],
                            "mi_unit"=> $data[$i]['mi_modiunitid'],
                            "mi_sale_amount"=> $data[$i]['mi_sale_amount'],
                            "mi_description"=> $data[$i]['mi_description'],
                        );
                    }
                    else{
                        $modifier_arr[$index]['modifieritem'] = array();
                    }
                }
                $unitdata[$key]['bind_modifier'] = $modifier_arr;

                $strRSql = "SELECT ROUND((CIR.unit/CU.unit),$round_off)as quantity,ROUND((CIR.unit/CUR.conversation_rate),$round_off)as raw_quantity,
                  CU.unitunkid,CIR.lnkstoreid,CU.hashkey,CR.id as rawhash,CR.name,CU.name as unit_name,CU.unit
                  FROM ".CONFIG_DBN.".cfmenu_item_recipe as CIR
                  INNER JOIN ".CONFIG_DBN.".cfrawmaterial AS CR ON CR.id=CIR.lnkrawmaterialid                 
                  INNER JOIN ".CONFIG_DBN.".cfunit AS CU ON CU.unitunkid=CIR.lnkunitid     
                  LEFT JOIN ".CONFIG_DBN.".cfrawmaterial_unit_rel AS CUR ON CUR.lnkrawid=CIR.lnkrawmaterialid AND CUR.lnkunitid=CIR.lnkunitid AND CUR.companyid=:companyid
                  WHERE CIR.lnkitemid=:itemid AND CIR.lnkitemunitid=:lnkitemunitid AND CIR.recipe_type=:recipe_type AND CIR.companyid=:companyid AND CIR.locationid=:locationid";

                $dao->initCommand($strRSql);
                $dao->addParameter(':itemid',$itemunkid);
                $dao->addParameter(':lnkitemunitid',$val['itmunitunkid']);
                $dao->addParameter(':recipe_type', '1');
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $recipe_data = $dao->executeQuery();
                $unitdata[$key]['bind_recipe'] = $recipe_data;
            }
            $response_array['bind_unit'] = $unitdata;

            $strSql = "SELECT CITR.lnktaxunkid,
                  ROUND(CITR.amount,$round_off) as amount,CITR.taxapplyafter
                  FROM ".CONFIG_DBN.".cfitemtax_rel as CITR         
                  WHERE CITR.lnkitemid=:itemid AND CITR.item_type=:item_type AND CITR.companyid=:companyid AND CITR.locationid=:locationid";

            $dao->initCommand($strSql);
            $dao->addParameter(':itemid',$itemunkid);
            $dao->addParameter(':item_type', '1');
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $taxdata = $dao->executeQuery();
            $response_array['bind_tax'] = $taxdata;

            return json_encode(array("Success"=>"True","Data"=>$response_array));
        }   
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getrec - ".$e);
            return false; 
        }
    }
    public function getItemList($data)
    {
        try
        {
            $this->log->logIt($this->module." - getItemList");
            $dao = new \dao();
            $strSql = "SELECT CFI.hashkey, CFI.itemname FROM ".CONFIG_DBN.".cfmenu_items AS CFI INNER JOIN ".CONFIG_DBN.".cfmenu_categories AS CFC ON CFC.categoryunkid = CFI.categoryunkid AND CFC.companyid=:companyid AND CFC.locationid=:locationid";
            $strSql .= " WHERE CFC.hashkey=:hashkey AND CFI.companyid=:companyid AND CFI.locationid=:locationid AND CFI.is_active=1 AND CFI.is_deleted=0";
            $dao->initCommand($strSql);
            $dao->addParameter(':hashkey',$data['id']);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array('Success'=>'True', 'Data'=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getItemList - ".$e);
            return html_entity_decode(json_encode(array('Success'=>'False')));
        }
    }

    public function getrec_itemonly($data)
    {
        try
        {
            $this->log->logIt($this->module." - getrec");
            $dao = new \dao();
            $hashkey=(isset($data['id']))?$data['id']:"";
            $strSql = "SELECT CFI.itemname,CFI.long_desc,CFI.image,CFI.sale_amount,CFI.sku,CFI.spicy_type,CFI.is_active,CFI.type,CFC.hashkey AS catid";
            $strSql .= " FROM ";
            $strSql .= CONFIG_DBN.".cfmenu_items AS CFI INNER JOIN ".CONFIG_DBN.".cfmenu_categories AS CFC ON CFC.categoryunkid = CFI.categoryunkid AND CFC.companyid=:companyid AND CFC.locationid=:locationid WHERE CFI.hashkey=:hashkey AND CFI.companyid=:companyid AND CFI.locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':hashkey',$hashkey);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeRow();
            return html_entity_decode(json_encode(array('Success'=>'True', 'Data'=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getrec - ".$e);
            return html_entity_decode(json_encode(array('Success'=>'False')));
        }
    }
    public function removeitem($data)
    {
        try
        {   
            $this->log->logIt($this->module.' - remove');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $Objstaticarray =new \common\staticarray();
            $datetime=\util\util::getLocalDateTime();
            if(count($data)>0 && isset($data['module']) && isset($data['id']))
            {
                $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $id = $ObjCommonDao->getprimarykey("cfmenu_items",$data['id'],"itemunkid");
                $strSql = "SELECT combounkid FROM ".CONFIG_DBN.".cfmenu_combo WHERE FIND_IN_SET('".$id."',itemunkid) AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $resobj = $dao->executeQuery();
                if(count($resobj)>0){
                    return html_entity_decode(json_encode(array("Success"=>"False","Message"=>"This Item is used as part of combo product")));
                }
                /*Delete Item*/
                    $strSql = "UPDATE ".CONFIG_DBN.".cfmenu_items SET is_deleted=1,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE hashkey=:id AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":id",$data['id']);
                    $dao->addParameter(':modified_user',CONFIG_UID);
                    $dao->addParameter(':modifieddatetime',$datetime);
                    $dao->addParameter(":companyid",CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $dao->executeNonQuery();
                /*Delete Item*/
                /*Delete From Item- Modifier Relation*/
                    $strSql = "DELETE FROM ".CONFIG_DBN.".fditem_modifier_relation WHERE lnkitemid=:id AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":id",$id);
                    $dao->addParameter(":companyid",CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $dao->executeNonQuery();
                /*Delete From Item- Modifier Relation*/
                /*Delete From Item- Modifier Item Relation*/
                    $strSql = "DELETE FROM ".CONFIG_DBN.".fdmodifier_item_relation WHERE lnkitemid=:id AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":id",$id);
                    $dao->addParameter(":companyid",CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $dao->executeNonQuery();
                /*Delete From Item- Modifier Item Relation*/

                //Delete Recipe
                $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:id AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id",$id);
                $dao->addParameter(':recipe_type', '1');
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();

                $strSql = "DELETE FROM ".CONFIG_DBN.".cfitemtax_rel WHERE lnkitemid=:id AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id",$id);
                $dao->addParameter(':recipe_type', '1');
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'],'Delete Record',$data['id'],'');
                return html_entity_decode(json_encode(array("Success"=>"True","Message"=>"Record removed successfully")));
            }
            else
                return html_entity_decode(json_encode(array("Success"=>"False","Message"=>"Internal Error")));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - remove - '.$e);
        }
    }
    public function getitem_modifier_rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getitem_modifier_rec');
            $dao = new \dao();
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getitem_modifier_rec - '.$e);
        }
    }
    public function getrelateditems($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getrelateditems');
            $dao = new \dao();
            $result = array();
            $ObjCommonDao = new \database\commondao();
            if($data['id']!="")
            {
                $id = $ObjCommonDao->getprimarykey('cfmenu_items',$data['id'],'itemunkid');
                /*$strSql = "SELECT GROUP_CONCAT(CFCOMBO.comboname) AS subject FROM ".CONFIG_DBN.".cfmenu_combo CFCOMBO INNER JOIN ".CONFIG_DBN.".cfcombo_item_rel as REL on REL.lnkcomboid=CFCOMBO.combounkid
                            WHERE FIND_IN_SET('".$id."',REL.lnkitemid) AND CFCOMBO.is_deleted=0 AND CFCOMBO.companyid=:companyid AND CFCOMBO.locationid=:locationid 
                            AND REL.is_deleted=0 AND REL.companyid=:companyid AND REL.locationid=:locationid
                            ORDER BY CFCOMBO.combounkid";*/

                $strSql = "SELECT GROUP_CONCAT(DISTINCT CFCOMBO.comboname) AS subject FROM ".CONFIG_DBN.".cfmenu_combo CFCOMBO INNER JOIN ".CONFIG_DBN.".comboitemgroups_items_relation as REL on REL.comboid=CFCOMBO.combounkid 
                            WHERE FIND_IN_SET('".$id."',REL.itemid) AND CFCOMBO.is_deleted=0 AND CFCOMBO.companyid=:companyid AND CFCOMBO.locationid=:locationid 
                            AND REL.is_deleted=0 AND REL.companyid=:companyid AND REL.locationid=:locationid
                            ORDER BY CFCOMBO.combounkid";


                                                                     // change relation table for item use in combo      change BY Tanvir

                $dao->initCommand($strSql);
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $resobj = $dao->executeRow();
                $arr_item = array();
                if($resobj['subject']!=""){
                    $arr_item['subject'] = $resobj['subject'];
                    $arr_item['module'] = \common\staticarray::$getmodulename['cfmenu_combo']['Display_Name'];
                    $result[] = $arr_item;
                }
                return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$result)));
            }
            else{
                return html_entity_decode(json_encode(array("Success"=>"False","Data"=>"")));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getrelateditems - '.$e);
        }
    }
    public function geItemByType($data)
    {
        try
        {
            $this->log->logIt($this->module." - geItemByType");
            $dao = new \dao();
            $itemtype = $data['itemtype'];
            if($itemtype == 1){
                $strSql = "SELECT itemname,hashkey FROM ".CONFIG_DBN.".cfmenu_items 
                           WHERE is_deleted=0 AND is_active=1
                           AND companyid=:companyid AND locationid=:locationid";
            }
            elseif($itemtype == 2){
                $strSql = "SELECT modifiername as itemname,hashkey FROM ".CONFIG_DBN.".cfmenu_modifiers 
                           WHERE is_deleted=0 AND is_active=1
                           AND companyid=:companyid AND locationid=:locationid";
            }
            else{
                $strSql = "SELECT itemname,hashkey FROM ".CONFIG_DBN.".cfmenu_modifier_items 
                           WHERE is_deleted=0 AND is_active=1
                           AND companyid=:companyid AND locationid=:locationid";
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array('Success'=>'True', 'Data'=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - geItemByType - ".$e);
            return html_entity_decode(json_encode(array('Success'=>'False')));
        }
    }


    public function checktaxavailableornot($data)
    {
        try
        {
            $this->log->logIt($this->module." - checktaxavailableornot");
            $is_tax_available=0;
            if($data){
                foreach ($data as $key=>$val){
                    if(array_key_exists('taxid',$val)){
                        $is_tax_available=1;
                    }
                }

            }
            return $is_tax_available;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - checktaxavailableornot - ".$e);
            return html_entity_decode(json_encode(array('Success'=>'False')));
        }
    }

}


?>

