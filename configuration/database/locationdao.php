<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 23/8/17
 * Time: 10:13 AM
 */
namespace database;
use function r\add;

class locationdao
{
    public $module = 'DB_locationdao';
    public $log;
    public $dbconnect;

    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function loadLocationInfo()
    {
        try
        {
            $this->log->logIt($this->module.' - loadLocationInfo');
            $dao = new \dao();
            $strSql = "SELECT * FROM ".CONFIG_DBN.".cflocation WHERE companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',CONFIG_LID);
            $res = $dao->executeRow();
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loadLocationInfo - '.$e);
        }
    }
    public function editLocationInformation($data)
    {
        try
        {
            $this->log->logIt($this->module.' - editLocationInformation');
            $dao = new \dao();
            $hashkey = \util\util::gethash();
            $ObjAuditDao = new \database\auditlogdao();
            $strSql1 = "SELECT currencylnkid from ".CONFIG_DBN.".cflocation where companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',CONFIG_LID);
            $rec1 = $dao->executeRow();


            if(!isset($data['txtzipcode']) && !empty($data['txtzipcode'])){
                $lat_log=$this->extractLatLngFromAdd($data['txtzipcode']);
            }else{
                $lat_log=$this->extractLatLngFromAdd($data['txtaddress1']);
            }
            $currency_sign=isset($data['txtsymbol'])?$data['txtsymbol']:'';

            if($data['currencylnkid'] != $rec1['currencylnkid']) {
                $strSql2 = "Update ".CONFIG_DBN.".cfcurrencylist SET currency_is_default=0 where companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql2);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();

                $strSql3 = "SELECT COUNT(countryid) AS cnt,currency_sign,currencyunkid,hashkey from ".CONFIG_DBN.".cfcurrencylist where currencyunkid=:currencyunkid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql3);
                $dao->addParameter(':currencyunkid', $data['currencylnkid']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid',CONFIG_LID);
                $rec3 = $dao->executeRow();


                if($rec3){
                    $currency_sign=$rec3['currency_sign'];
                }

                /* if ($rec3['cnt'] == 0) {
                     $strSql4 = "SELECT currencyCode,countryName from ".CONFIG_DBN.".vwcountry where id=:countryCode";
                     $dao->initCommand($strSql4);
                     $dao->addParameter(':countryCode', $data['select_country']);
                     $rec4 = $dao->executeRow();
                     $arr_log = array(
                         'Currency Code'=>$rec4['currencyCode'],
                         'Country Name'=>$rec4['countryName'],
                         'Exchange Rate'=>'1.00',
                         'Status'=>'Active',
                     );
                     $title = "Add Record";
                     $key = $hashkey;
                     $strSql5 = "INSERT INTO ".CONFIG_DBN.".cfcurrencylist SET
                                 countryid=:countryid,currency_code=:currency_code,
                                 currency_sign=:currency_sign,currency_sign_pos=0,
                                 currency_rate=1,currency_is_default=1,companyid=:companyid,
                                 locationid=:locationid,hashkey=:hashkey";
                     $dao->initCommand($strSql5);
                     $dao->addparameter(':countryid', $data['select_country']);
                     $dao->addParameter(':currency_sign', $rec4['currencyCode']);
                     $dao->addParameter(':currency_code', $rec4['currencyCode']);
                     $dao->addparameter(':hashkey', $hashkey);
                     $dao->addparameter(':companyid', CONFIG_CID);
                     $dao->addParameter(':locationid',CONFIG_LID);
                     $dao->executeNonQuery();
                 }
                 else
                 {*/
                   // $title = 'Edit Record';
                    //$arr_log = array('Exchange Rate'=>'1.00',);
                   // $key = $rec3['hashkey'];
                    $strSql6 = "Update ".CONFIG_DBN.".cfcurrencylist SET currency_rate=1,
                                currency_is_default=1 where companyid=:companyid AND currencyunkid=:currencyunkid
                                AND locationid=:locationid";
                    $dao->initCommand($strSql6);
                    $dao->addparameter(':currencyunkid',$rec3['currencyunkid']);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addParameter(':locationid',CONFIG_LID);
                    $dao->executeNonQuery();
                //}
                //$json_data = json_encode($arr_log);
                //$ObjAuditDao->addactivitylog('currencylist',$title,$key,$json_data);
            }
            $strSql7 = "UPDATE ".CONFIG_DBN.".cflocation SET 
                       locationname=:locationname,address1=:address1,address2=:address2,
                       country=:country,state=:state,city=:city,zipcode=:zipcode,phone=:phone,
                       reservation_contact_no=:reservation_contact_no,fax=:fax,latitude=:latitude,                       longitude=:longitude,image=:image,locationemail=:locationemail,                                  website=:website,currency_sign=:currency_sign,currencylnkid=:currencylnkid,amenities=:amenities
                       WHERE companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql7);
            $dao->addParameter(':locationname',$data['txtlocationname']);
            $dao->addParameter(':address1',$data['txtaddress1']);
            $dao->addParameter(':address2',$data['txtaddress2']);
            $dao->addParameter(':country',$data['select_country']);
            $dao->addParameter(':state',$data['txtstate']);
            $dao->addParameter(':city',$data['txtcity']);
            $dao->addParameter(':zipcode',isset($data['txtzipcode'])?$data['txtzipcode']:'');
            $dao->addParameter(':phone',$data['txtphoneno']);
            $dao->addParameter(':reservation_contact_no',$data['txtrescontactno']);
            $dao->addParameter(':fax',$data['txtfaxno']);
            $dao->addParameter(':latitude',isset($lat_log['latitude'])?$lat_log['latitude']:'');
            $dao->addParameter(':longitude',isset($lat_log['longitude'])?$lat_log['longitude']:'');
            $dao->addParameter(':image',$data['image']);
            $dao->addParameter(':locationemail',$data['txtemail']);
            $dao->addParameter(':website',$data['txtwebsite']);
            $dao->addParameter(':currency_sign',$currency_sign);
            $dao->addParameter(':currencylnkid',$data['currencylnkid']);
            $dao->addParameter(':amenities',isset($data['amenities'])?$data['amenities']:'');
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',CONFIG_LID);
            $dao->executeNonQuery();
            $strSql8 = "Update syslocation SET locationname=:locationname,locationemail=:locationemail where companyid=:syscompanyid AND syslocationid=:syslocationid";
            $dao->initCommand($strSql8);
            $dao->addParameter(':locationname',$data['txtlocationname']);
            $dao->addParameter(':locationemail',$data['txtemail']);
            $dao->addParameter(':syscompanyid',CONFIG_CID);
            $dao->addParameter(':syslocationid',CONFIG_LID);
            $dao->executeNonQuery();
            return 1;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - editLocationInformation - '.$e);
            return 0;
        }
    }


    public  function extractLatLngFromAdd($address) {
        try {

            if(!isset($address) || empty($address)) {
                return json_encode(array('Success'=>'False','Message'=>'INTERNAL_ERROR'));
                //throw new exception("Address is not set.");
            }

            $geodata =   json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key=AIzaSyBguvbb-ZqSLY8fXmdX94A9_5OvYeKA7ag'));
            if(!$geodata) {
                return json_encode(array('Success'=>'False','Message'=>'INTERNAL_ERROR'));
                //throw new exception("Error to extract geodata.");
            }
// extract latitude and longitude
            $result['latitude']  = isset($geodata->results[0]->geometry->location)?$geodata->results[0]->geometry->location->lat:'';
            $result['longitude'] = isset($geodata->results[0]->geometry->location)?$geodata->results[0]->geometry->location->lng:'';
            if(empty($result)) {
                return json_encode(array('Success'=>'False','Message'=>'INTERNAL_ERROR'));
                //throw new exception("Couldn't extract latitude and longitude, Plz try to input different address.");
            }
            return $result;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }



    public function getLocationList()
    {
        try
        {
            $this->log->logIt($this->module.' - getLocationList');
            $dao = new \dao();
            $strSql = "SELECT syslocationid, locationname FROM syslocation WHERE is_deleted=0 AND companyid=:companyid ORDER BY syslocationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $rec = $dao->executeQuery();
            return $rec;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getLocationList - '.$e);
        }
    }


    public function getlocation($data)
    {
        try
        {
            $this->log->logIt($this->module.'-getlocation');
            $dao = new \dao();
            $strSql = "SELECT syslocationid, locationname FROM syslocation WHERE is_deleted=0 AND companyid=:companyid ";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',$data['companyid']);
            $rec = $dao->executeQuery();
            return json_encode(array("Success"=>"True","Message"=>"Location List","Data"=>$rec));
        }
        catch(\Exception $e)
        {
            $this->log->logIt($this->module.'-getlocation -'.$e);
        }
    }

    public function getAllLocation()
    {
        try
        {
            $this->log->logIt($this->module.'-getAllLocation');
            $dao = new \dao();
            $strSql = "SELECT locationid,locationname FROM ".CONFIG_DBN.".cflocation WHERE companyid=:companyid AND locationid!=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',CONFIG_LID);
            $rec = $dao->executeQuery();
            return json_encode(array("Success"=>"True","Data"=>$rec));
        }
        catch(\Exception $e)
        {
            $this->log->logIt($this->module.'-getAllLocation -'.$e);
        }
    }
}
?>