<?php

namespace database;

use common\staticarray;

class dependencydao
{
    public $module = 'DB_dependencydao';
    public $log;
    private $language, $default_lang_arr, $defaultlanguageArr;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language();
        $this->default_lang_arr = $this->language->loaddefaultlanguage();
        $this->defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
        $this->defaultlanguageArr = json_decode($this->defaultlanguageArr);
    }

    public function loadrelatedrecords($id = "", $module = "")
    {
        try {
            $this->log->logIt($this->module . ' - loadrelatedrecords');
            $dao = new \dao();
            $data = array();
            $ObjStaticArray = new \common\staticarray();
            if ($module != "" && $id != "") {
                $rel = isset($ObjStaticArray->relatedtables[$module]) ? $ObjStaticArray->relatedtables[$module] : "";
                if ($rel != "" && count($rel) > 0) {
                    if (CONFIG_LOGINTYPE == 2) {
                        $loginid = CONFIG_SID;
                    } else {
                        $loginid = CONFIG_LID;
                    }
                    foreach ($rel as $key => $value) {
                        $modulename = \common\staticarray::$getmodulename[$key]['Module_Name'];
                        $masterfield = $ObjStaticArray->auditlogmodules[$modulename]['masterfield'];
                        $PrimaryKey = $ObjStaticArray->auditlogmodules[$modulename]['pd'];
                        if (CONFIG_LOGINTYPE == 2) {
                            $strSql = "SELECT GROUP_CONCAT(" . $masterfield . ") AS subject FROM " . CONFIG_DBN . "." . $key . " WHERE " . $value . " = '" . $id . "' AND is_deleted=0 AND companyid=:companyid AND storeid=:locationid ORDER BY " . $PrimaryKey . " DESC LIMIT 3";
                        } else {
                            $strSql = "SELECT GROUP_CONCAT(" . $masterfield . ") AS subject FROM " . CONFIG_DBN . "." . $key . " WHERE " . $value . " = '" . $id . "' AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid ORDER BY " . $PrimaryKey . " DESC LIMIT 3";
                        }
                        $dao->initCommand($strSql);
                        $dao->addParameter(":companyid", CONFIG_CID);
                        $dao->addparameter(':locationid', $loginid);
                        $resobj = $dao->executeRow();
                        $data[$key]['subject'] = $resobj['subject'];
                        $data[$key]['module'] = \common\staticarray::$getmodulename[$key]['Display_Name'];
                    }
                }
                return json_encode(array("Success" => "True", "Data" => $data));
            } else {
                return json_encode(array("Success" => "False", "Data" => ""));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - loadrelatedrecords - ' . $e);
        }
    }

    public function checkdependency($module, $id, $return_type)
    {
        try {
            $this->log->logIt($this->module . ' - checkdependency');
            $dao = new \dao;
            $ObjStaticArray = new \common\staticarray();
            $rel = isset($ObjStaticArray->dependency_check_method[$module]) ? $ObjStaticArray->dependency_check_method[$module] : "";
            $data = "";
            if ($rel != "") {
                $data = $this->$rel($module,$id,$return_type,0);
            }
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkdependency - ' . $e);
        }
    }


    public function checkdependencyBYcompany($module, $id)
    {
        try {
            $this->log->logIt($this->module . ' - checkdependencyBYcompany');
            $dao = new \dao;
            $ObjStaticArray = new \common\staticarray();
            $rel = isset($ObjStaticArray->relatedtables[$module]) ? $ObjStaticArray->relatedtables[$module] : "";
            $flag = 0;
            if ($rel != "" && count($rel) > 0) {
                foreach ($rel as $key => $value) {
                    $strSql = "SELECT * FROM " . CONFIG_DBN . "." . $key . " WHERE " . $value . " = '" . $id . "' AND is_deleted=0 AND companyid=:companyid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":companyid", CONFIG_CID);
                    $resobj = $dao->executeQuery();
                    if (count($resobj) > 0) {
                        $flag++;
                    }
                }
            }
            return $flag;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkdependency - ' . $e);
        }
    }

    public function checkcommondependency($module, $id, $return_type, $active = 0)
    {
        try {
            $this->log->logIt($this->module . ' - checkcommondependency');
            $dao = new \dao;
            $ObjStaticArray = new \common\staticarray();
            $rel = isset($ObjStaticArray->relatedtables[$module]) ? $ObjStaticArray->relatedtables[$module] : "";
            $flag = 0;
            $data = array();
            if ($rel != "" && count($rel) > 0) {
                foreach ($rel as $key => $value) {
                    $modulename = \common\staticarray::$getmodulename[$key]['Module_Name'];
                    $masterfield = $ObjStaticArray->auditlogmodules[$modulename]['masterfield'];
                    $PrimaryKey = $ObjStaticArray->auditlogmodules[$modulename]['pd'];
                    $cn = $ObjStaticArray->auditlogmodules[$modulename]['status_cn'];
                    if (CONFIG_LOGINTYPE == 2) {
                        $loginid = CONFIG_SID;
                        $strSql = "SELECT $PrimaryKey FROM " . CONFIG_DBN . "." . $key . " WHERE " . $value . " = '" . $id . "' AND is_deleted=0 AND companyid=:companyid and storeid=:locationid";
                    } else {
                        $loginid = CONFIG_LID;
                        if($masterfield=='discount'){
                            $strSql = "SELECT $PrimaryKey FROM " . CONFIG_DBN . "." . $key . " WHERE FIND_IN_SET(".$id.",`".$value ."`) > 0 AND is_deleted=0 AND companyid=:companyid and locationid=:locationid";
                        }else{
                            $strSql = "SELECT $PrimaryKey FROM " . CONFIG_DBN . "." . $key . " WHERE " . $value . " = '" . $id . "' AND is_deleted=0 AND companyid=:companyid and locationid=:locationid";
                        }
                    }
                    if ($active == 1) {
                        $strSql .= " AND " . $cn . "=1";
                    }
                    $dao->initCommand($strSql);
                    $dao->addParameter(":companyid", CONFIG_CID);
                    $dao->addParameter(":locationid", $loginid);
                    $resObj = $dao->executeQuery();
                    if (count($resObj) > 0) {
                        $flag++;
                    }
                    if ($return_type == 2) {
                        if (CONFIG_LOGINTYPE == 2) {
                            $loginid = CONFIG_SID;
                            $strSql2 = "SELECT COUNT($PrimaryKey) as cnt,GROUP_CONCAT(" . $masterfield . ") AS subject FROM " . CONFIG_DBN . "." . $key . " WHERE " . $value . " = '" . $id . "' AND is_deleted=0 AND companyid=:companyid AND storeid=:locationid ORDER BY " . $PrimaryKey . " DESC LIMIT 3";
                        } else {
                            $loginid = CONFIG_LID;
                            if($masterfield=='discount'){
                                $strSql2 = "SELECT COUNT($PrimaryKey) as cnt,GROUP_CONCAT(" . $masterfield . ") AS subject FROM " . CONFIG_DBN . "." . $key . " WHERE FIND_IN_SET(".$id.",`".$value."`) AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid ORDER BY " . $PrimaryKey . " DESC LIMIT 3";
                            }else {
                                $strSql2 = "SELECT COUNT($PrimaryKey) as cnt,GROUP_CONCAT(" . $masterfield . ") AS subject FROM " . CONFIG_DBN . "." . $key . " WHERE " . $value . " = '" . $id . "' AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid ORDER BY " . $PrimaryKey . " DESC LIMIT 3";
                            }
                        }
                        $dao->initCommand($strSql2);
                        $dao->addParameter(":companyid", CONFIG_CID);
                        $dao->addParameter(":locationid", $loginid);
                        $resObj2 = $dao->executeRow();
                        if ($resObj2['cnt'] > 0) {
                            $data[$key]['subject'] = $resObj2['subject'];
                            $data[$key]['module'] = \common\staticarray::$getmodulename[$key]['Display_Name'];
                        }
                    }
                }
            }
            if ($return_type == 1) {
                return $flag;
            }
            if ($return_type == 2) {
                return json_encode(array("Success" => "True", "Data" => $data));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkcommondependency - ' . $e);
        }
    }

    public function checkdependencytable($module, $id)
    {
        try {
            $this->log->logIt($this->module . ' - checkdependencydata');
            $dao = new \dao;
            $ObjStaticArray = new \common\staticarray();
            $rel = isset($ObjStaticArray->relatedtables[$module]) ? $ObjStaticArray->relatedtables[$module] : "";
            $flag = 0;
            if ($rel != "" && count($rel) > 0) {
                foreach ($rel as $key => $value) {
                    $strSql = "SELECT * FROM " . CONFIG_DBN . "." . $key . " WHERE " . $value . " = '" . $id . "' AND isvoid=0 AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":companyid", CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $resobj = $dao->executeQuery();
                    if (count($resobj) > 0) {
                        $flag++;
                    }
                }
            }
            return $flag;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkdependencydata - ' . $e);
        }
    }

    public function checkduplicaterecord($table, $field, $value, $id = "")
    {
        try {
            $this->log->logIt($this->module . ' - checkduplicaterecord');
            $dao = new \dao;
            $ObjStaticArray = new \common\staticarray();
            $modulename = \common\staticarray::$getmodulename[$table]['Module_Name'];
            $PrimaryKey = $ObjStaticArray->auditlogmodules[$modulename]['pd'];
            if (CONFIG_LOGINTYPE == 2) {
                $loginid = CONFIG_SID;
                $strSql = "SELECT " . $PrimaryKey . " FROM " . CONFIG_DBN . "." . $table . " WHERE " . $field . "='" . $value . "' and is_deleted=0 AND companyid=:companyid AND storeid=:locationid";
            } else {
                $loginid = CONFIG_LID;
                $strSql = "SELECT " . $PrimaryKey . " FROM " . CONFIG_DBN . "." . $table . " WHERE " . $field . "='" . $value . "' and is_deleted=0 AND companyid=:companyid AND locationid=:locationid";
            }

            if ($id != "") {
                $strSql .= " AND " . $PrimaryKey . "!=" . $id . "";
            }
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', $loginid);
            $resobj = $dao->executeQuery();
            if (count($resobj) > 0) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkduplicaterecord - ' . $e);
        }
    }

    public function checkduplicaterecordBycompany($table, $field, $value, $id = "")
    {
        try {
            $this->log->logIt($this->module . ' - checkduplicaterecordBycompany');
            $dao = new \dao;
            $ObjStaticArray = new \common\staticarray();
            $modulename = \common\staticarray::$getmodulename[$table]['Module_Name'];
            $PrimaryKey = $ObjStaticArray->auditlogmodules[$modulename]['pd'];
            $strSql = "SELECT " . $PrimaryKey . " FROM " . CONFIG_DBN . "." . $table . " WHERE " . $field . "='" . $value . "' and is_deleted=0 AND companyid=:companyid";
            if ($id != "") {
                $strSql .= " AND " . $PrimaryKey . "!=" . $id . "";
            }
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $resobj = $dao->executeQuery();
            if (count($resobj) > 0) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkduplicaterecordBycompany - ' . $e);
        }
    }

    public function checkstatusdependency($module, $id, $return_type, $active)
    {
        try {
            $this->log->logIt($this->module . ' - checkstatusdependency');
            $dao = new \dao;
            $ObjStaticArray = new \common\staticarray();
            $rel = isset($ObjStaticArray->dependency_check_method[$module]) ? $ObjStaticArray->dependency_check_method[$module] : "";
            $data = "";
            if ($rel != "") {
                $data = $this->$rel($module, $id, $return_type, $active);
            }
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkstatusdependency - ' . $e);
        }
    }

    public function checkclassdependency($module, $id, $return_type, $active = 0)
    {
        try {
            $this->log->logIt($this->module . ' - checkclassdependency');
            $dao = new \dao;
            $flag = 0;
            $data = array();
            $strSql = "SELECT COUNT(tableunkid) as cnt,IFNULL(GROUP_CONCAT(tablename),'') as subject 
                      FROM " . CONFIG_DBN . ". cfclasstable WHERE FIND_IN_SET($id,classunkid) AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid";


            if ($active == 1) {
                $strSql .= " AND is_active=1";
            }
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 1) {
                return $flag;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfclasstable']['Display_Name'];
                }
                return json_encode(array("Success" => "True", "Data" => $data));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkclassdependency - ' . $e);
        }
    }

    public function checkunitdependency($module, $id, $return_type, $active = 0)
    {
        try {
            $this->log->logIt($this->module . ' - checkunitdependency');
            $dao = new \dao;
            $flag = 0;
            $data = array();
            $result = array();
            $strSql = "SELECT COUNT(DISTINCT CI.itemunkid) as cnt,GROUP_CONCAT(DISTINCT CI.itemname) AS subject FROM " . CONFIG_DBN . ".cfmenu_items CI
                            INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit_rel CIR ON CI.itemunkid=CIR.lnkitemid and recipe_type=:recipetype
                            INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit CIU ON CIU.unitunkid=CIR.lnkitemunitid
                            WHERE CIU.unitunkid =:id AND CI.is_deleted=0 AND CIR.is_deleted=0 
                            AND CIU.companyid=:companyid and CIR.companyid=:companyid 
                            AND CIU.locationid=:locationid AND CIR.locationid=:locationid";

            if ($active == 1) {
                $strSql .= " AND CI.is_active=1";
            }
            $strSql .= " ORDER BY CI.itemunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $dao->addParameter(":recipetype", '1');
            $dao->addParameter(":id", $id);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_items']['Display_Name'];
                    $result[] = $data[$module];
                }
            }
            //Modifier
            $strSql = "SELECT COUNT(DISTINCT CFMOD.modifierunkid) as cnt,GROUP_CONCAT(DISTINCT CFMOD.modifiername) AS subject FROM " . CONFIG_DBN . ".cfmenu_modifiers CFMOD
                            INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit_rel CIR ON CFMOD.modifierunkid=CIR.lnkitemid and recipe_type=:recipetype
                            INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit CIU ON CIU.unitunkid=CIR.lnkitemunitid
                            WHERE CIU.unitunkid =:id AND CFMOD.is_deleted=0 AND CIR.is_deleted=0 
                            AND CIU.companyid=:companyid and CIR.companyid=:companyid 
                            AND CIU.locationid=:locationid AND CIR.locationid=:locationid";

            if ($active == 1) {
                $strSql .= " AND CFMOD.is_active=1";
            }
            $strSql .= "  ORDER BY CFMOD.modifierunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $dao->addParameter(":recipetype", '2');
            $dao->addParameter(":id", $id);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_modifiers']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            //Modifier item
            $strSql = "SELECT COUNT(DISTINCT CFMI.modifieritemunkid) as cnt,GROUP_CONCAT(DISTINCT CFMI.itemname) AS subject FROM " . CONFIG_DBN . ".cfmenu_modifier_items CFMI 
                            INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit_rel CIR ON CFMI.modifieritemunkid=CIR.lnkitemid and recipe_type=:recipetype
                            INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit CIU ON CIU.unitunkid=CIR.lnkitemunitid
                            WHERE CIU.unitunkid =:id AND CFMI.is_deleted=0 AND CIR.is_deleted=0 
                            AND CFMI.companyid=:companyid and CIR.companyid=:companyid 
                            AND CFMI.locationid=:locationid AND CIR.locationid=:locationid";

            if ($active == 1) {
                $strSql .= " AND CFMI.is_active=1";
            }
            $strSql .= " ORDER BY CFMI.modifieritemunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $dao->addParameter(":recipetype", '3');
            $dao->addParameter(":id", $id);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_modifier_items']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            //Combo Product

            $strSql = "SELECT COUNT(DISTINCT CFCOMBO.combounkid) as cnt,GROUP_CONCAT(DISTINCT CFCOMBO.comboname) AS subject FROM " . CONFIG_DBN . ".cfmenu_combo CFCOMBO
                            INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit_rel CIR ON CFCOMBO.combounkid=CIR.lnkitemid and recipe_type=:recipetype AND CIR.companyid=:companyid AND CIR.locationid=:locationid AND CIR.is_deleted=0   
                            INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit CIU ON CIU.unitunkid=CIR.lnkitemunitid AND CIU.companyid=:companyid AND CIU.locationid=:locationid WHERE CIU.unitunkid =:id AND CFCOMBO.is_deleted=0 
                             ";

            if ($active == 1) {
                $strSql .= " AND CFCOMBO.is_active=1";
            }
            $strSql .= " ORDER BY CFCOMBO.combounkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $dao->addParameter(":recipetype", '4');
            $dao->addParameter(":id", $id);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_combo']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            if ($return_type == 1) {
                return $flag;
            }
            if ($return_type == 2) {
                return json_encode(array("Success" => "True", "Data" => $result));
            }

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkunitdependency - ' . $e);
        }
    }

    public function checkrawMaterialdependency($module, $id, $return_type, $active = 0)
    {
        try {
            $this->log->logIt($this->module . ' - checkrawMaterialdependency');
            $dao = new \dao;
            $flag = 0;
            $data = array();
            $result = array();
            $strSql = "SELECT COUNT(CFITEM.itemunkid) as cnt,GROUP_CONCAT(CFITEM.itemname) AS subject,CL.locationname as location FROM " . CONFIG_DBN . ".cfmenu_items CFITEM 
                            INNER JOIN " . CONFIG_DBN . ".cfmenu_item_recipe CIR ON CFITEM.itemunkid=CIR.lnkitemid 
                            INNER JOIN " . CONFIG_DBN . ".cfrawmaterial CR ON CIR.lnkrawmaterialid=CR.id
                            INNER JOIN " . CONFIG_DBN . ".cflocation CL ON CL.locationid=CFITEM.locationid
                            WHERE CR.id =:id AND CR.is_deleted=0 AND CFITEM.is_deleted=0 
                            AND CIR.recipe_type=:recipe_type
                            AND CFITEM.companyid=:companyid and CIR.companyid=:companyid 
                            GROUP BY CFITEM.locationid";

            if ($active == 1) {
                $strSql .= " AND CFITEM.is_active=1";
            }
            $strSql .= " ORDER BY CFITEM.itemunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $dao->addParameter(":recipe_type", '1');
            $dao->addParameter(":id", $id);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['location'] = $resObj['location'];
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_items']['Display_Name'];
                    $result[] = $data[$module];
                }
            }
            //Modifier
            $strSql = "SELECT COUNT(CFMOD.modifierunkid) as cnt,GROUP_CONCAT(CFMOD.modifiername) AS subject,CL.locationname as location FROM " . CONFIG_DBN . ".cfmenu_modifiers CFMOD 
                            INNER JOIN " . CONFIG_DBN . ".cfmenu_item_recipe CIR ON CFMOD.modifierunkid=CIR.lnkitemid 
                            INNER JOIN " . CONFIG_DBN . ".cfrawmaterial CR ON CIR.lnkrawmaterialid=CR.id
                            INNER JOIN " . CONFIG_DBN . ".cflocation CL ON CL.locationid=CFMOD.locationid
                            WHERE CR.id =:id  AND CR.is_deleted=0 AND CFMOD.is_deleted=0
                            AND CIR.recipe_type=:recipe_type
                            AND CFMOD.companyid=:companyid and CIR.companyid=:companyid 
                            GROUP BY CFMOD.locationid";

            if ($active == 1) {
                $strSql .= " AND CFMOD.is_active=1";
            }
            $strSql .= "  ORDER BY CFMOD.modifierunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $dao->addParameter(":recipe_type", '2');
            $dao->addParameter(":id", $id);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['location'] = $resObj['location'];
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_modifiers']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            //Modifier item
            $strSql = "SELECT COUNT(CFMI.modifieritemunkid) as cnt,GROUP_CONCAT(CFMI.itemname) AS subject,CL.locationname as location FROM " . CONFIG_DBN . ".cfmenu_modifier_items CFMI 
                            INNER JOIN " . CONFIG_DBN . ".cfmenu_item_recipe CIR ON CFMI.modifieritemunkid=CIR.lnkitemid 
                            INNER JOIN " . CONFIG_DBN . ".cfrawmaterial CR ON CIR.lnkrawmaterialid=CR.id
                            INNER JOIN " . CONFIG_DBN . ".cflocation CL ON CL.locationid=CFMI.locationid
                            WHERE CR.id =:id AND CR.is_deleted=0 AND CFMI.is_deleted=0
                            AND CIR.recipe_type=:recipe_type
                            AND CFMI.companyid=:companyid and CIR.companyid=:companyid  
                            GROUP BY CFMI.locationid";

            if ($active == 1) {
                $strSql .= " AND CFMI.is_active=1";
            }
            $strSql .= " ORDER BY CFMI.modifieritemunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $dao->addParameter(":recipe_type", '3');
            $dao->addParameter(":id", $id);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['location'] = $resObj['location'];
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_modifier_items']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            if ($return_type == 1) {
                return $flag;
            }
            if ($return_type == 2) {
                return json_encode(array("Success" => "True", "Data" => $result));
            }

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkrawMaterialdependency - ' . $e);
        }
    }

    public function checkModiferdependency($module, $id, $return_type, $active = 0)
    {
        try {
            $this->log->logIt($this->module . ' - checkModiferdependency');
            $dao = new \dao;
            $flag = 0;
            $data = array();
            $result = array();

            $strSql = "SELECT COUNT(DISTINCT CFITEM.itemunkid) as cnt,GROUP_CONCAT(DISTINCT CFITEM.itemname) AS subject FROM " . CONFIG_DBN . ".cfmenu_items CFITEM 
                        INNER JOIN " . CONFIG_DBN . ".fditem_modifier_relation CFREL ON CFITEM.itemunkid=CFREL.lnkitemid AND CFREL.is_deleted=0
                        WHERE CFREL.lnkmodifierid = '" . $id . "' AND CFITEM.is_deleted=0 AND CFITEM.companyid=:companyid AND CFITEM.locationid=:locationid and CFREL.companyid=:companyid 
                        and CFREL.locationid=:locationid";

            if ($active == 1) {
                $strSql .= " AND CFITEM.is_active=1";
            }
            $strSql .= " ORDER BY CFITEM.itemunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_items']['Display_Name'];
                    $result[] = $data[$module];
                }
            }
            //Modifier item

            $strSql = "SELECT COUNT(DISTINCT CFMITEM.modifieritemunkid) as cnt,GROUP_CONCAT(CFMITEM.itemname) AS subject 
                        FROM " . CONFIG_DBN . ".cfmenu_modifier_items CFMITEM WHERE CFMITEM.modifierunkid = '" . $id . "' 
                        AND CFMITEM.is_deleted=0 AND CFMITEM.companyid=:companyid AND CFMITEM.locationid=:locationid";

            if ($active == 1) {
                $strSql .= " AND CFMITEM.is_active=1";
            }
            $strSql .= " ORDER BY CFMITEM.modifieritemunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_modifier_items']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            // Combo Products

            $strSql = "SELECT COUNT(DISTINCT CFCOMBO.combounkid) as cnt,GROUP_CONCAT(DISTINCT CFCOMBO.comboname) AS subject FROM " . CONFIG_DBN . ".cfmenu_combo CFCOMBO 
                        INNER JOIN " . CONFIG_DBN . ".comboitem_mod_relation CFREL ON CFCOMBO.combounkid=CFREL.comboid AND CFREL.companyid=:companyid AND CFREL.locationid=:locationid AND CFREL.is_deleted=0 
                        WHERE CFREL.modifierid = '" . $id . "' AND CFCOMBO.is_deleted=0 AND CFCOMBO.companyid=:companyid AND CFCOMBO.locationid=:locationid ";


            if ($active == 1) {
                $strSql .= " AND CFCOMBO.is_active=1";
            }
            $strSql .= " ORDER BY CFCOMBO.combounkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_combo']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            if ($return_type == 1) {
                return $flag;
            }
            if ($return_type == 2) {
                return json_encode(array("Success" => "True", "Data" => $result));
            }

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkModiferdependency - ' . $e);
        }
    }

    public function checkModifierItemdependency($module, $id, $return_type, $active = 0)
    {
        try {
            $this->log->logIt($this->module . ' - checkModifierItemdependency');
            $dao = new \dao;
            $flag = 0;
            $data = array();
            $result = array();

            $strSql = "SELECT COUNT(DISTINCT CFITEM.itemunkid) as cnt,GROUP_CONCAT(DISTINCT CFITEM.itemname) AS subject FROM " . CONFIG_DBN . ".cfmenu_items 
                       CFITEM INNER JOIN " . CONFIG_DBN . ".fdmodifier_item_relation CFREL ON CFITEM.itemunkid=CFREL.lnkitemid
                       AND CFITEM.companyid=CFREL.companyid WHERE CFREL.lnkmodifieritemid = '" . $id . "' AND 
                       CFITEM.is_deleted=0 AND CFREL.is_deleted=0 AND CFITEM.companyid=:companyid AND 
                       CFITEM.locationid=:locationid";
            if ($active == 1) {
                $strSql .= " AND CFITEM.is_active=1";
            }
            $strSql .= " ORDER BY CFITEM.itemunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $resObj = $dao->executeRow();

            if ($resObj['cnt'] > 0) {
                $flag++;
            }

            if ($return_type == 2) {

                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_items']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            // Combo Products

            $strSql = "SELECT COUNT(DISTINCT CFCOMBO.combounkid) as cnt,GROUP_CONCAT(DISTINCT CFCOMBO.comboname) AS subject FROM " . CONFIG_DBN . ".cfmenu_combo CFCOMBO
             INNER JOIN " . CONFIG_DBN . ".combomod_item_relation CFREL ON CFCOMBO.combounkid=CFREL.comboid AND CFREL.companyid=:companyid AND CFREL.locationid=:locationid AND CFREL.is_deleted=0
                        WHERE CFREL.modifieritemid = '" . $id . "' AND CFCOMBO.is_deleted=0 AND CFCOMBO.companyid=:companyid AND CFCOMBO.locationid=:locationid ";

            if ($active == 1) {
                $strSql .= " AND CFCOMBO.is_active=1";
            }
            $strSql .= " ORDER BY CFCOMBO.combounkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $resObj = $dao->executeRow();

            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {

                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_combo']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            if ($return_type == 1) {
                return $flag;
            }
            if ($return_type == 2) {
                return json_encode(array("Success" => "True", "Data" => $result));
            }

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkModifierItemdependency - ' . $e);
        }
    }

    public function checkTaxdependency($module, $id, $return_type, $active = 0)
    {
        try {
            $this->log->logIt($this->module . ' - checkTaxdependency');
            $dao = new \dao;
            $flag = 0;
            $data = array();
            $result = array();


            /*  $strSql = "SELECT COUNT(taxunkid) as cnt,tax AS subject from ".CONFIG_DBN.".cftax
               WHERE companyid=:companyid and locationid=:locationid and is_deleted=0 AND storeid=0 AND isactive=1";
              $dao->initCommand($strSql);
              $dao->addParameter(":companyid", CONFIG_CID);
              $dao->addParameter(":locationid", CONFIG_LID);

              $resObj = $dao->executeRow();
              if ($resObj['cnt'] >=4) {
                  $flag++;
              }
              if ($return_type == 2) {

                  if ($resObj['cnt'] > 0) {
                      $data[$module]['subject'] = $resObj['subject'];
                      $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_items']['Display_Name'];
                      $result[] = $data[$module];
                  }
              }*/

            //this will be called in Display settings module to check whether tax is binded with gratuity or not.
            $str = "SELECT keyvalue FROM " . CONFIG_DBN . ".cfparameter WHERE companyid=:companyid AND
             locationid=:locationid AND keyname='gratuity_tax_detail'";
            $dao->initCommand($str);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $result1 = $dao->executeRow();

            if ($result1) {
                if ($result1['keyvalue'] != '') {
                    $decoded_value = (array)json_decode($result1['keyvalue']);
                    $tax_id = array_keys($decoded_value);
                    if (in_array($id, $tax_id)) {
                        if($return_type == 1)
                        {
                            return 1;
                        }
                        if ($return_type == 2) {
                            $data[$module]['subject'] = $this->defaultlanguageArr->GRATUITY." ".$this->defaultlanguageArr->TAX;
                            $data[$module]['module'] = $this->defaultlanguageArr->DISPLAY_SETTINGS;
                            $result[] = $data[$module];
                        }
                    }
                }
            }



            $strSql = "SELECT COUNT(DISTINCT CFITEM.itemunkid) as cnt,GROUP_CONCAT(DISTINCT CFITEM.itemname) AS subject FROM " . CONFIG_DBN . ".cfmenu_items CFITEM
                       INNER JOIN " . CONFIG_DBN . ".cfitemtax_rel CTREL ON CFITEM.itemunkid=CTREL.lnkitemid AND item_type=:item_type
                       WHERE CTREL.lnktaxunkid = '" . $id . "' AND 
                       CFITEM.is_deleted=0 AND CTREL.is_deleted=0 AND CFITEM.companyid=:companyid AND 
                       CFITEM.locationid=:locationid";
            if ($active == 1) {
                $strSql .= " AND CFITEM.is_active=1";
            }
            $strSql .= " ORDER BY CFITEM.itemunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $dao->addParameter(":item_type", '1');
            $resObj = $dao->executeRow();

            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {

                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_items']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            //Modifier
            $strSql = "SELECT COUNT(DISTINCT CFMOD.modifierunkid) as cnt,GROUP_CONCAT(DISTINCT CFMOD.modifiername) AS subject FROM " . CONFIG_DBN . ".cfmenu_modifiers CFMOD
                       INNER JOIN " . CONFIG_DBN . ".cfitemtax_rel CTREL ON CFMOD.modifierunkid=CTREL.lnkitemid AND item_type=:item_type
                       WHERE CTREL.lnktaxunkid = '" . $id . "' AND 
                       CFMOD.is_deleted=0 AND CTREL.is_deleted=0 AND CFMOD.companyid=:companyid AND 
                       CFMOD.locationid=:locationid";

            if ($active == 1) {
                $strSql .= " AND CFMOD.is_active=1";
            }
            $strSql .= "  ORDER BY CFMOD.modifierunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $dao->addParameter(":item_type", '2');
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_modifiers']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            //Modifier item
            $strSql = "SELECT COUNT(DISTINCT CFMI.modifieritemunkid) as cnt,GROUP_CONCAT(DISTINCT CFMI.itemname) AS subject FROM " . CONFIG_DBN . ".cfmenu_modifier_items CFMI
                       INNER JOIN " . CONFIG_DBN . ".cfitemtax_rel CTREL ON CFMI.modifieritemunkid=CTREL.lnkitemid AND item_type=:item_type
                       WHERE CTREL.lnktaxunkid = '" . $id . "' AND 
                       CFMI.is_deleted=0 AND CTREL.is_deleted=0 AND CFMI.companyid=:companyid AND 
                       CFMI.locationid=:locationid";

            if ($active == 1) {
                $strSql .= " AND CFMI.is_active=1";
            }
            $strSql .= " ORDER BY CFMI.modifieritemunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $dao->addParameter(":item_type", '3');
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_modifier_items']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            // Combo Product

            $strSql = "SELECT COUNT(DISTINCT CFCOMBO.combounkid) as cnt,GROUP_CONCAT(DISTINCT CFCOMBO.comboname) AS subject FROM " . CONFIG_DBN . ".cfmenu_combo CFCOMBO
                       INNER JOIN " . CONFIG_DBN . ".cfitemtax_rel CTREL ON CFCOMBO.combounkid=CTREL.lnkitemid AND item_type=:item_type
                       WHERE CTREL.lnktaxunkid = '" . $id . "' AND 
                       CFCOMBO.is_deleted=0 AND CTREL.is_deleted=0 AND CFCOMBO.companyid=:companyid AND 
                       CFCOMBO.locationid=:locationid";
            if ($active == 1) {
                $strSql .= " AND CFCOMBO.is_active=1";
            }
            $strSql .= " ORDER BY CFCOMBO.combounkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $dao->addParameter(":item_type", '4');
            $resObj = $dao->executeRow();

            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {

                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_combo']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            //after tax check

            $strSql="SELECT GROUP_CONCAT(tax) as subject FROM ".CONFIG_DBN.".cftax WHERE FIND_IN_SET(".$id.",after_tax) AND companyid=".CONFIG_CID;

            if ($active == 1) {
                $strSql .= " AND isactive=1";
            }
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $resObj = $dao->executeRow();
            if (isset($resObj['subject']) && $resObj['subject']!=''){
                $flag++;
            }
            if ($return_type == 2) {
                if (isset($resObj['subject']) && $resObj['subject']!='') {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cftax']['Display_Name'];
                    $result[] = $data[$module];
                }
            }
            if ($return_type == 1) {
                return $flag;
            }
            if ($return_type == 2) {
                return json_encode(array("Success" => "True", "Data" => $result));
            }

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkTaxdependency - ' . $e);
        }
    }

    public function checktaxdependencywithclass($module, $id, $return_type, $active = 0)
    {
        try {
            $this->log->logIt($this->module . ' - checkclassdependency');
            $dao = new \dao;
            $flag = 0;
            $data = array();
            $strSql = "SELECT T.tax, COUNT(CTR.lnktaxunkid) AS cnt , GROUP_CONCAT(C.classname) AS subject
 FROM " . CONFIG_DBN . ".cftax AS T
 LEFT JOIN " . CONFIG_DBN . ".cfclasstaxrelation AS CTR ON CTR.lnktaxunkid=T.taxunkid 
 LEFT JOIN " . CONFIG_DBN . ".cfclass AS C ON CTR.lnkclassid=C.classunkid
  Where T.companyid=:companyid and T.locationid=:locationid AND T.isactive=1 AND T.is_deleted=0 AND CTR.lnktaxunkid= " . $id . "";
            if ($active == 1) {
                $strSql .= " AND is_active=1";
            }
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $resObj = $dao->executeRow();

            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 1) {
                return $flag;
            }
            if ($return_type == 2) {

                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfclass']['Display_Name'];
                }
                return json_encode(array("Success" => "True", "Data" => $data));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkclassdependency - ' . $e);
        }
    }

    public function checkItemdependency($module, $id, $return_type, $active = 0)
    {
        try {
            $this->log->logIt($this->module . ' - checkItemdependency');
            $dao = new \dao;
            $flag = 0;
            $data = array();
            $result = array();

            // Combo Products

            $strSql = "SELECT COUNT(DISTINCT CFCOMBO.combounkid) as cnt,GROUP_CONCAT(DISTINCT CFCOMBO.comboname) AS subject FROM " . CONFIG_DBN . ".cfmenu_combo CFCOMBO INNER JOIN " . CONFIG_DBN . ".comboitemgroups_items_relation as REL on REL.comboid=CFCOMBO.combounkid 
                            WHERE FIND_IN_SET('" . $id . "',REL.itemid) AND CFCOMBO.is_deleted=0 AND CFCOMBO.companyid=:companyid AND CFCOMBO.locationid=:locationid 
                            AND REL.is_deleted=0 AND REL.companyid=:companyid AND REL.locationid=:locationid ";

            if ($active == 1) {
                $strSql .= " AND CFCOMBO.is_active=1";
            }
            $strSql .= " ORDER BY CFCOMBO.combounkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":locationid", CONFIG_LID);
            $resObj = $dao->executeRow();

            if ($resObj['cnt'] > 0) {
                $flag++;
            }

            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $data[$module]['module'] = \common\staticarray::$getmodulename['cfmenu_combo']['Display_Name'];
                    $result[] = $data[$module];
                }
            }

            if ($return_type == 2) {
                return json_encode(array("Success" => "True", "Data" => $result));
            }

            if ($return_type == 1) {
                return $flag;
            }


        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkItemdependency - ' . $e);
        }
    }

    public function checkItemUnitdependency($id , $unitid)
    {
        try {
            $this->log->logIt($this->module . ' - checkItemUnitdependency');
            $dao = new \dao;
            $flag = 0;

            // Item Unit

            if($unitid != '')
            {
                $strSql = "SELECT COUNT(DISTINCT CFMITEM.itemunitrelationunkid) as cnt FROM " . CONFIG_DBN . ".comboitem_unit_relation  CFMITEM WHERE CFMITEM.itemid = '" . $id . "' AND CFMITEM.itemunitid IN('" . $unitid . "')  AND CFMITEM.is_deleted=0 AND CFMITEM.companyid=:companyid AND CFMITEM.locationid=:locationid ";

                $strSql .= " ORDER BY CFMITEM.itemunitrelationunkid";
                $dao->initCommand($strSql);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->addParameter(":locationid", CONFIG_LID);
                $resObj = $dao->executeRow();

                if ($resObj['cnt'] > 0) {
                    $flag++;
                }
            }
            return $flag;

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkItemUnitdependency - ' . $e);
        }
    }

    public function checkModifiersdependency($id,$unitid,$modid,$modunitid,$moditemid,$moditemunitid)
    {
        try {
            $this->log->logIt($this->module . ' - checkModifiersdependency');
            $dao = new \dao;
            $flag = 0;

            $this->log->logIt($flag);

            if($modid != '' &&  $modunitid !='')
            {
                // Modifier WITH Unit

                $strSql = "SELECT COUNT(DISTINCT CFMITEM.modifierid) as cnt FROM " . CONFIG_DBN . ".comboitem_mod_relation CFMITEM WHERE CFMITEM.modifierid IN('" . $modid . "') AND CFMITEM.itemid = '" . $id . "' AND CFMITEM.itemunitid = '" . $unitid . "' AND CFMITEM.modifierunitid IN('" . $modunitid . "') AND CFMITEM.is_deleted=0 AND CFMITEM.companyid=:companyid AND CFMITEM.locationid=:locationid ";

                $strSql .= " ORDER BY CFMITEM.itemmodrelationunkid";
                $dao->initCommand($strSql);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->addParameter(":locationid", CONFIG_LID);
                $resObj = $dao->executeRow();

                if ($resObj['cnt'] > 0) {
                    $flag++;
                }
            }


            if($modid != '' && $moditemid != '' && $moditemunitid ) {

                // Modifier item With Unit

                $strSql = "SELECT COUNT(DISTINCT CFMITEM.modifieritemid) as cnt FROM " . CONFIG_DBN . ".combomod_item_relation CFMITEM
                INNER JOIN " . CONFIG_DBN . ".comboitem_mod_relation CFM ON CFMITEM.modifierid = CFM.modifierid AND CFM.itemid = '" . $id . "' AND CFM.itemunitid = '" . $unitid . "' AND CFM.is_deleted=0 AND CFM.companyid=:companyid AND CFM.locationid=:locationid
                        WHERE CFMITEM.modifierid = '" . $modid . "' AND CFMITEM.modifieritemid = '" . $moditemid . "'  AND CFMITEM.modifieritemunitid = '" . $moditemunitid . "'
                        AND CFMITEM.is_deleted=0 AND CFMITEM.companyid=:companyid AND CFMITEM.locationid=:locationid";

                $strSql .= " ORDER BY CFMITEM.moditemrelationunkid";
                $dao->initCommand($strSql);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->addParameter(":locationid", CONFIG_LID);
                $resObj = $dao->executeRow();

                if ($resObj['cnt'] > 0) {
                    $flag++;
                }
            }

            return $flag;

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkModifiersdependency - ' . $e);
        }
    }

}

?>