<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 20/5/17
 * Time: 10:28 AM
 */

namespace database;

class storereportsdao
{
    public $module = 'DB_storereportsdao';
    public $log;
    public $dbconnect;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->mongo_con = new \MongoDB\Driver\Manager(CONFIG_MONGO_HOST);
    }

    public function getGRNRecords($data, $languageArr = '')
    {
        try {
            $this->log->logIt($this->module . ' - getGRNRecords');
            $dao = new \dao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $resarr = array();
            $Companyinfo = self::loadcompanyinformation();
            $strSql = "SELECT IFNULL(DATE_FORMAT(CGRN.grn_date,'" . $mysql_format . "'),'') AS grn_date,CGRN.voucher_no,
                        CGRN.grn_doc_num,ROUND(CGRN.totalamount,$round_off)as totalamount,
                        TRC.name AS Vendor_name,S.storename AS Store_name, 
                        IFNULL(CFU1.username,'') AS CU,
                        IFNULL(CFU2.username,'') AS MU,
						CGRN.grnid,CGRN.lnkorderid  ,
						GRD.discount_per AS DP,GRD.discount_amount AS DA,GRD.tax_amount  AS TA					                            
                        FROM " . CONFIG_DBN . ".cfgrn AS CGRN                        
                        LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU1 ON CGRN.createduser=CFU1.userunkid AND CGRN.companyid=CFU1.companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU2 ON CGRN.modifieduser=CFU2.userunkid AND CGRN.modifieduser=CFU2.userunkid AND CGRN.companyid=CFU2.companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfvandor AS CV ON CGRN.lnkvendorid=CV.vandorunkid AND CV.companyid=:companyid
                        LEFT JOIN " . CONFIG_DBN . ".trcontact AS TRC ON CV.lnkcontactid=TRC.contactunkid AND TRC.companyid=:companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS S ON S.storeunkid=CGRN.storeid AND S.companyid=:companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfgrndetail AS GRD ON GRD.lnkgrnid=CGRN.grnid AND GRD.companyid=:companyid AND GRD.storeid=:companyid                        
                        WHERE CGRN.companyid=:companyid AND CGRN.storeid=:storeid AND CGRN.is_deleted=0  ";
            if ($data['grnno'] != "") {

                $strSql .= " AND CGRN.grn_doc_num LIKE '%" . $data['grnno'] . "%'";
            }
            if ($data['voucher_no'] != "") {
                $strSql .= " AND CGRN.voucher_no LIKE '%" . $data['voucher_no'] . "%'";
            }
            if ($data['vendorid'] != 0) {
                $strSql .= " AND CGRN.lnkvendorid LIKE '%" . $data['vendorid'] . "%'";
            }
            if ($data['fromdate'] != "" && $data['todate'] != "") {
                $strSql .= " AND CAST(CGRN.grn_date AS DATE) BETWEEN '" . $data['fromdate'] . "' AND '" . $data['todate'] . "'";
            }
            if ($data['fromdate'] != "" && $data['todate'] == "") {
                $strSql .= " AND CAST(CGRN.grn_date AS DATE) BETWEEN '" . $data['fromdate'] . "' AND '" . $current_date . "'";
            }
            $strSql .= " GROUP BY CGRN.grnid  ORDER BY CGRN.grnid DESC";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $grrecords = $dao->executeQuery();

            $GT = $GRandTotal = $T = $TAXTotal = $D = $DisTOtal = 0;

            foreach ($grrecords AS $val) {
                $GT = $val['totalamount'];
                $GRandTotal += $GT;

            }

            foreach ($grrecords AS $val) {
                $T = $val['TA'];
                $TAXTotal += $T;

            }
            foreach ($grrecords AS $val) {
                $D = $val['DA'];
                $DisTOtal += $D;

            }
            //$resarr['Tax_Total'] = $total['TAX_TOTAL'];
            $resarr['Tax_Total'] = number_format($TAXTotal, $round_off);
            $resarr['DIS_Total'] = number_format($DisTOtal, $round_off);

            $resarr['company_name'] = $Companyinfo['companyname'];
            $resarr['address'] = $Companyinfo['company_address'];
            $resarr['phone'] = $Companyinfo['company_phone'];
            $resarr['email'] = $Companyinfo['companyemail'];
            $resarr['company_logo'] = !empty($Companyinfo['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$Companyinfo['company_logo']:'';
            $resarr['fax'] = $Companyinfo['fax'];
            $resarr['currency_sign'] = $Companyinfo['currency_sign'];
            $resarr['grnRecords'] = $grrecords;
            $resarr['print_by'] = CONFIG_UNM;

            $resarr['Grand_Total'] = $GRandTotal;

            $resarr['current_date'] = \util\util::convertMySqlDate($current_date);

            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getGRNRecords - ' . $e);
        }
    }

    public function loadcompanyinformation()
    {
        try {
            $this->log->logIt($this->module . ' - loadcompanyinformation');
            $dao = new \dao();
            $strSql = "SELECT  C.companyname,C.address1 AS company_address,C.phone AS company_phone,IFNULL(C.logo,'') as company_logo,country AS Company_Country,
                        C.currency_sign,C.companyemail,C.fax FROM " . CONFIG_DBN . ".cfcompany AS C  WHERE companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $res = $dao->executeRow();
            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - loadcompanyinformation - ' . $e);
        }
    }

    public function createdVoucherList($data, $languageArr = '')
    {
        try {
            $this->log->logIt($this->module . ' - createdVoucherList');
            $resarr = array();

            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $Companyinfo = self::loadcompanyinformation();
            $current_date = \util\util::getLocalDate();
            $ObjUtil = new \util\util;
            $from_date = $ObjUtil->convertDateToMySql($data['ifromdate']);
            $to_date = $ObjUtil->convertDateToMySql($data['itodate']);
            $dao = new \dao;
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $strSql = "SELECT CFIV.lnkindentid,CFIV.issueid,CFIV.issue_doc_num,DATE_FORMAT(CFIV.issue_date,'" . $mysql_format . "') as issueDate,IFNULL(CFRI.indent_doc_num,'') as indentNo," .
                "CFSM.storename,IFNULL(CFU1.username,'') AS created_user,CFIV.totalamount                FROM " . CONFIG_DBN . ".cfissuevoucher CFIV " .
                "LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS CFSM ON CFSM.storeunkid=CFIV.relstoreid AND CFSM.companyid =:companyid " .
                "LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU1 ON CFIV.createduser=CFU1.userunkid AND CFU1.companyid =:companyid " .
                "LEFT JOIN " . CONFIG_DBN . ".cfrequestindent AS CFRI ON CFRI.indentid=CFIV.lnkindentid AND CFRI.companyid=:companyid AND CFRI.storeid = CFIV.relstoreid " .
                "WHERE CFIV.companyid=:companyid AND CFIV.storeid=:storeid AND CFIV.is_deleted=0";
            if ($data['ivoucher_no'] != "") {
                $strSql .= " AND CFIV.issue_doc_num LIKE '%" . $data['ivoucher_no'] . "%'";
            }
            if ($data['istoreid'] != 0) {
                $strSql .= " AND CFIV.relstoreid = '" . $data['istoreid'] . "'";
            }
            if ($from_date != "" && $to_date != "") {
                $strSql .= " AND CFIV.issue_date BETWEEN '" . $from_date . "' AND '" . $to_date . "' ";
            }
            if ($from_date != "" && $to_date == "") {
                $strSql .= " AND CAST(CFIV.issue_date AS DATE) BETWEEN '" . $from_date . "' AND '" . $current_date . "'";
            }
            $strSql .= " ORDER BY CFIV.issueid DESC";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $issue = $dao->executeQuery();
            $GT = $GRandTotal = 0;
            foreach ($issue AS $val) {
                $GT = $val['totalamount'];
                $GRandTotal += $GT;
            }

            $resarr['current_date'] = \util\util::convertMySqlDate($current_date);
            $resarr['company_name'] = $Companyinfo['companyname'];
            $resarr['address'] = $Companyinfo['company_address'];
            $resarr['phone'] = $Companyinfo['company_phone'];
            $resarr['email'] = $Companyinfo['companyemail'];
            $resarr['company_logo'] = !empty($Companyinfo['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$Companyinfo['company_logo']:'';
            $resarr['fax'] = $Companyinfo['fax'];
            $resarr['Company_Country'] = $Companyinfo['Company_Country'];
            $resarr['currency_sign'] = $Companyinfo['currency_sign'];

            $resarr['issuedetail'] = $issue;
            $resarr['Grand_total'] = number_format($GRandTotal, $round_off);

            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - createdVoucherList - ' . $e);
        }
    }

    public function itemStockledger($data, $languageArr = '')
    {
        try {
            $this->log->logIt($this->module . ' - itemStockledger');
            $resarr = array();
            $itemarr = array();
            $typearr = array();
            $typearr2 = array();
            $typearr3 = array();
            $Companyinfo = self::loadcompanyinformation();
            $current_date = \util\util::getLocalDate();
            $ObjUtil = new \util\util;
            $from_date = $ObjUtil->convertDateToMySql($data['lfromdate']);
            $to_date = $ObjUtil->convertDateToMySql($data['ltodate']);
            $dao = new \dao;
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];

            $str = "SELECT RM.id,RM.name AS ITEM, RC.name AS Cat_Nm, U.shortcode AS Unit
              FROM " . CONFIG_DBN . ".cfrawmaterial AS RM
                 LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial_category AS RC ON RM.lnkcategoryid=RC.id AND RC.companyid=:companyid 
                 LEFT JOIN " . CONFIG_DBN . ".cfunit AS U on RM.measuretype=U.measuretype AND U.is_systemdefined=1 AND U.companyid=:companyid 
                  WHERE RM.companyid=:companyid AND RM.is_active=1 AND RM.is_deleted=0";
            if ($data['lcat'] != 0) {
                $str .= " AND RM.lnkcategoryid = '" . $data['lcat'] . "'";
            }
            if ($data['litem'] != 0) {
                $str .= " AND RM.id = '" . $data['litem'] . "'";
            }
            $str .=" ORDER BY " . CONFIG_DBN . ".RM.name ASC";
            $dao->initCommand($str);
            $dao->addParameter(':companyid', CONFIG_CID);

            $item = $dao->executeQuery();

            $strSql = " SELECT CFID.id AS INV_ID,CFID.lnkrawmaterialid AS RMLID,
                        TC.name AS Vendor_name,TC.business_name AS Vendor_business,CFID.inventory,CFID.rate AS TotalAmount, 
                        RML.inventory AS LOCTotalInventory,
                        S.storename AS Store_name ,SR.storename AS REL_Storename,CFID.crdb,
                        (CASE WHEN CFID.inv_status =1 THEN  '" . $languageArr->LANG28 . "'
                        WHEN CFID.inv_status =2 THEN '" . $languageArr->LANG29 . "'
                        WHEN CFID.inv_status =3 THEN '" . $languageArr->LANG19 . "'
                        WHEN CFID.inv_status =4 THEN '" . $languageArr->LANG21 . "'                        
                        WHEN CFID.inv_status =5 THEN '" . $languageArr->LANG30 . "' END)  AS Stat,
                        IFNULL(DATE_FORMAT(CFID.createddatetime,'" . $mysql_format . "'),'') as INV_Date,CFID.inv_status,
                        ROUND(CFID.total_rate,$round_off) as Rate             
                        FROM " . CONFIG_DBN . ".cfinventorydetail AS CFID
                        LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial_loc AS RML ON CFID.lnkrawmateriallocid=RML.id AND RML.companyid=:companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS RM ON CFID.lnkrawmaterialid=RM.id AND RM.companyid=:companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfvandor AS V ON V.vandorunkid=CFID.lnkvendorid AND CFID.companyid=:companyid
                        LEFT JOIN " . CONFIG_DBN . ".trcontact AS TC ON  TC.contactunkid=V.lnkcontactid AND TC.companyid=:companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS SR ON SR.storeunkid=CFID.rel_storeid AND SR.companyid=:companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS S ON S.storeunkid=CFID.storeid AND S.companyid=:companyid
                         
                        WHERE CFID.companyid=:companyid AND CFID.storeid=:storeid AND CFID.is_deleted=0";
            if ($from_date == $to_date) {
                $strSql .= " AND DATE(CFID.createddatetime) = '" . $from_date . "' ";
            } elseif ($from_date != "" && $to_date != "") {
                $strSql .= " AND CFID.createddatetime BETWEEN '" . $from_date . "' AND '" . $to_date . "' ";
            } elseif ($from_date != "" && $to_date == "") {
                $strSql .= " AND CAST(CFID.createddatetime AS DATE) BETWEEN '" . $from_date . "' AND '" . $current_date . "'";
            }

            $strSql .= " ORDER BY CFID.id DESC";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $itemdetail = $dao->executeQuery();

            if ($to_date == '') $to_date = $current_date;
            if ($from_date == '') $from_date = $current_date;

            $strt1 = "SELECT RL.audit_date,RL.type,RL.new_value,RL.lnkrawmaterialid AS RLID,RL.id AS id1 
                    FROM  " . CONFIG_DBN . ".cfrawmaterial AS RM
                    LEFT JOIN  " . CONFIG_DBN . ".rawmaterial_audit_log AS RL ON RL.lnkrawmaterialid = RM.id
                     AND RL.storeid =:storeid AND RL.companyid =:companyid AND RL.id =(
                    SELECT    id  FROM    " . CONFIG_DBN . ".rawmaterial_audit_log  WHERE 
                       lnkrawmaterialid = RM.id AND companyid =:companyid AND storeid =:storeid AND type=1 
                       AND DATE(audit_date)='" . $from_date . "' ORDER BY created_at DESC  LIMIT 1)
                    WHERE RL.companyid =:companyid  AND RL.is_deleted = 0  ORDER BY RL.id DESC";
            $dao->initCommand($strt1);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $type1 = $dao->executeQuery();
            if(isset($type1) && !empty($type1))
            {
                foreach ($type1 as $tkey1 => $tvalue1) {
                    $typearr[$tvalue1['id1']]['RLID'] = $tvalue1['RLID'];
                    $typearr[$tvalue1['id1']]['new_value'] = $tvalue1['new_value'];
                    $typearr[$tvalue1['id1']]['type'] = $tvalue1['type'];
                    $typearr[$tvalue1['id1']]['audit_date'] = $tvalue1['audit_date'];
                }
            }


            $strt2 = "SELECT RL.audit_date,RL.type,  RL.new_value,RL.lnkrawmaterialid AS RLID,RL.id AS id2 FROM  " . CONFIG_DBN . ".cfrawmaterial AS RM
                    LEFT JOIN  " . CONFIG_DBN . ".rawmaterial_audit_log AS RL ON RL.lnkrawmaterialid = RM.id
                     AND RL.storeid =:storeid AND RL.companyid =:companyid AND RL.id =(
                    SELECT    id  FROM    " . CONFIG_DBN . ".rawmaterial_audit_log  WHERE 
                       lnkrawmaterialid = RM.id AND companyid =:companyid AND storeid =:storeid AND type=2 AND 
                       DATE(audit_date)='" . $from_date . "' ORDER BY created_at DESC  LIMIT 1)
                    WHERE RL.companyid =:companyid  AND RL.is_deleted = 0   ORDER BY RL.id DESC";
            $dao->initCommand($strt2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $type2 = $dao->executeQuery();
            if(isset($type2) && !empty($type2))
            {
                foreach ($type2 as $tkey2 => $tvalue2) {
                    $typearr2[$tvalue2['id2']]['RLID'] = $tvalue2['RLID'];
                    $typearr2[$tvalue2['id2']]['new_value'] = $tvalue2['new_value'];
                    $typearr2[$tvalue2['id2']]['type'] = $tvalue2['type'];
                    $typearr2[$tvalue2['id2']]['audit_date'] = $tvalue2['audit_date'];
                }
            }

            $strt3 = "SELECT RL.audit_date,RL.type,RL.new_value,RL.lnkrawmaterialid AS RLID,RL.id AS id3 FROM  " . CONFIG_DBN . ".cfrawmaterial AS RM
                    LEFT JOIN  " . CONFIG_DBN . ".rawmaterial_audit_log AS RL ON RL.lnkrawmaterialid = RM.id
                     AND RL.storeid =:storeid AND RL.companyid =:companyid AND RL.id =(
                    SELECT    id  FROM    " . CONFIG_DBN . ".rawmaterial_audit_log  WHERE 
                       lnkrawmaterialid = RM.id AND companyid =:companyid AND storeid =:storeid AND type=3 AND 
                       DATE(audit_date)='" . $from_date . "' ORDER BY created_at DESC  LIMIT 1)
                    WHERE RL.companyid =:companyid  AND RL.is_deleted = 0   ORDER BY RL.id DESC";
            $dao->initCommand($strt3);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $type3 = $dao->executeQuery();
            if(isset($type3) && !empty($type3))
            {
                foreach ($type3 as $tkey3 => $tvalue3) {
                    $typearr3[$tvalue3['id3']]['RLID'] = $tvalue3['RLID'];
                    $typearr3[$tvalue3['id3']]['new_value'] = $tvalue3['new_value'];
                    $typearr3[$tvalue3['id3']]['type'] = $tvalue3['type'];
                    $typearr3[$tvalue3['id3']]['audit_date'] = $tvalue3['audit_date'];
                }
            }

            if (isset($item) && !empty($item)) {
                foreach ($item as $key2 => $value2) {
                    $itemarr[$value2['id']]['ItemName'] = $value2['ITEM'];
                    $itemarr[$value2['id']]['Cat_name'] = $value2['Cat_Nm'];
                    $itemarr[$value2['id']]['Unit_name'] = $value2['Unit'];
                    if (isset($itemdetail) && !empty($itemdetail)) {
                        foreach ($itemdetail as $key => $value) {
                            if(isset($value2['id']))
                            {
                                if ($value2['id'] == $value['RMLID']) {
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['RMLID'] = $value['RMLID'];
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['INV_ID'] = $value['INV_ID'];
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['inventory'] = $value['inventory'];
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['Vendor_name'] = $value['Vendor_name'];
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['Vendor_business'] = $value['Vendor_business'];
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['LOCTotalInventory'] = $value['LOCTotalInventory'];
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['Store_name'] = $value['Store_name'];
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['REL_Storename'] = $value['REL_Storename'];
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['crdb'] = $value['crdb'];
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['Stat'] = $value['Stat'];
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['inv_status'] = $value['inv_status'];
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['INV_Date'] = $value['INV_Date'];
                                    $itemarr[$value2['id']]['ItemDetail'][$key]['Rate'] = $value['Rate'];
                                }
                            }
                        }
                    }


                    foreach ($typearr as $keyT1 => $valueT1) {
                        if(isset($value2['id'])) {
                            if ($value2['id'] == $valueT1['RLID']) {
                                $itemarr[$value2['id']]['ItemDetail'][$keyT1]['NewValue'] = $valueT1['new_value'];
                                $itemarr[$value2['id']]['ItemDetail'][$keyT1]['Type'] = $valueT1['type'];
                                $itemarr[$value2['id']]['ItemDetail'][$keyT1]['date'] = $valueT1['audit_date'];
                                $itemarr[$value2['id']]['ItemDetail'][$keyT1]['TypeName'] = $languageArr->LANG31;
                            }
                        }
                    }
                    foreach ($typearr2 as $keyT2 => $valueT2) {
                        if(isset($value2['id'])) {
                            if ($value2['id'] == $valueT2['RLID']) {
                                $itemarr[$value2['id']]['ItemDetail'][$keyT2]['NewValue2'] = $valueT2['new_value'];
                                $itemarr[$value2['id']]['ItemDetail'][$keyT2]['Type2'] = $valueT2['type'];
                                $itemarr[$value2['id']]['ItemDetail'][$keyT2]['date2'] = $valueT2['audit_date'];
                                $itemarr[$value2['id']]['ItemDetail'][$keyT2]['TypeName2'] = $languageArr->LANG32;
                            }
                        }
                    }
                    foreach ($typearr3 as $keyT3 => $valueT3) {
                        if(isset($value2['id'])) {
                            if ($value2['id'] == $valueT3['RLID']) {
                                $itemarr[$value2['id']]['ItemDetail'][$keyT3]['NewValue3'] = $valueT3['new_value'];
                                $itemarr[$value2['id']]['ItemDetail'][$keyT3]['Type3'] = $valueT3['type'];
                                $itemarr[$value2['id']]['ItemDetail'][$keyT3]['date3'] = $valueT3['audit_date'];
                                $itemarr[$value2['id']]['ItemDetail'][$keyT3]['TypeName3'] = $languageArr->LANG33;
                            }
                        }
                    }
                }
            }
            if (isset($itemdetail))
            {
                $GT = $GRandTotal = 0;
                foreach ($itemdetail AS $val) {
                    $GT = $val['Rate'];
                    $GRandTotal += $GT;
                }
            }

            $resarr['current_date'] = \util\util::convertMySqlDate($current_date);
            $resarr['company_name'] = $Companyinfo['companyname'];
            $resarr['address'] = $Companyinfo['company_address'];
            $resarr['company_logo'] = !empty($Companyinfo['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$Companyinfo['company_logo']:'';
            $resarr['phone'] = $Companyinfo['company_phone'];
            $resarr['email'] = $Companyinfo['companyemail'];
            $resarr['fax'] = $Companyinfo['fax'];
            $resarr['currency_sign'] = $Companyinfo['currency_sign'];
            $resarr['issuedetail'] = $itemdetail;
            $resarr['Item'] = $itemarr;
            $resarr['Grand_total'] = number_format(isset($GRandTotal) ? $GRandTotal : '0', $round_off);
            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - itemStockledger - ' . $e);
        }
    }

}

?>



