<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 20/5/17
 * Time: 10:28 AM
 */
namespace database;
class reportsdao
{
    public $module = 'DB_reportsdao';
    public $log;
    public $dbconnect;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->mongo_con = new \MongoDB\Driver\Manager(CONFIG_MONGO_HOST);
    }
    public function getmonthlyrec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getmonthlyrec');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $Companyinfo = self::loadcompanyinformation();
            $locationinfo = $ObjCommonDao->getLocationInfo();

            $current_date = \util\util::getLocalDate();
            $arr_date = explode('-',$data['date']);
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $default_val = number_format(0,$round_off);
            $res_days=\util\util::getmonth($arr_date[1],$arr_date[0]);
            $s_date = $res_days['year']."-".$res_days['mon']."-01";
            $e_date = $res_days['year']."-".$res_days['mon']."-".$res_days['last_mday'];
            $strSql = "SELECT IFNULL(CAST(FM.servedate AS DATE),'') as order_date,COUNT(DISTINCT FM.foliounkid) as total_order".
                      " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 8 AND FD.parentid = 0 THEN FD.quantity WHEN FASM.mastertypeunkid = 1 THEN FD.quantity ELSE 0 END)) AS itemsold".
                      " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 1 OR FASM.mastertypeunkid = 8 OR FASM.mastertypeunkid = 9 OR FASM.mastertypeunkid = 10 THEN FD.quantity * FD.baseamount ELSE 0 END),".$round_off.") AS itemprice".
                      " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 3 THEN FD.quantity * FD.baseamount ELSE 0 END),".$round_off.") AS discamt".
                      " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 2 THEN FD.quantity * FD.baseamount ELSE 0 END),".$round_off.") AS taxamt".
                      " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid IN (1,2,3,8,9,10) THEN FD.quantity * FD.baseamount * FASM.crdr ELSE 0 END),".$round_off.") AS finalamt".
                      " FROM ".CONFIG_DBN.".fasfoliomaster AS FM".
                      " INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC ON TRC.contactunkid = FM.lnkcontactid AND FM.companyid=TRC.companyid AND FM.locationid=TRC.locationid ".
                      " LEFT JOIN ".CONFIG_DBN.".fasfoliodetail AS FD ON FM.foliounkid = FD.foliounkid AND FM.companyid=FD.companyid AND FD.locationid=FM.locationid AND FD.isvoid = 0 AND FD.isrefund = 0".
                      " LEFT JOIN ".CONFIG_DBN.".fasmaster AS FASM ON FD.masterunkid = FASM.masterunkid AND FD.companyid=FASM.companyid AND FASM.locationid=FD.locationid".
                   	  " WHERE FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.isvalid = 1 AND FM.canceluserunkid = 0 AND TRC.contacttype != 4 AND FASM.mastertypeunkid NOT IN (4,7) AND CAST(FM.servedate AS DATE) BETWEEN '".$s_date."' AND '".$e_date."' AND FM.companyid=:companyid AND FM.locationid=:locationid GROUP BY CAST(FM.servedate AS DATE)";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeQuery();
            $final_arr = array();
            $arr_days = \util\util::generateDateRange($s_date,$e_date);
            $arr_res = \util\util::getkeyfieldarray($res,'order_date');
            $cnt = $total_items = $total_ordr = $total_tax = $total_item_price = $total_discount = $total_order_amt = 0;
            foreach($arr_days AS $val){
                if(isset($arr_res[$val])){
                    $final_arr[$cnt] = $arr_res[$val];
                }else{
                    $final_arr[$cnt] = array("total_order"=>"0","itemsold"=>"0","itemprice"=>$default_val,"discamt"=>$default_val,"taxamt"=>$default_val,"finalamt"=>$default_val);
                }
                $total_ordr += $final_arr[$cnt]['total_order'];
                $total_items += $final_arr[$cnt]['itemsold'];
                $total_item_price += $final_arr[$cnt]['itemprice'];
                $total_tax += $final_arr[$cnt]['taxamt'];
                $total_discount += $final_arr[$cnt]['discamt'];
                $total_order_amt += $final_arr[$cnt]['finalamt'];
                $final_arr[$cnt]['order_date'] =  \util\util::convertMySqlDate($val);
                $cnt++;
            }
            $result['data'] = $final_arr;
            $result['total_order'] = $total_ordr;
            $result['items_sold'] = $total_items;
            $result['item_price'] = number_format($total_item_price,$round_off);
            $result['item_tax'] = number_format($total_tax,$round_off);
            $result['item_discount'] = number_format($total_discount,$round_off);
            $result['order_amount'] = number_format($total_order_amt,$round_off);
            $result['report_date'] = \util\util::convertMySqlDate($current_date);
            $result['month'] = date('F,Y',strtotime($data['date']));
            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();
            $result['loc_name'] = $locationinfo['locationname'];
            $result['loc_email'] = $locationinfo['locationemail'];
            $result['loc_phone'] = $locationinfo['phone'];
            $result['company_logo'] = !empty($Companyinfo['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$Companyinfo['company_logo']:'';
            return $result;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getmonthlyrec - '.$e);
        }
    }
    public function getdailyrec($data,$languageArr='')
    {
        try
        {
            $this->log->logIt($this->module.' - getdailyrec');
            $dao = new \dao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');            
            $form_date = $to_date = '';
            if(isset($data['todate'])){
                $to_date = \util\util::convertDateToMySql($data['todate']);
            }
            if(isset($data['fromdate'])){
                $form_date = \util\util::convertDateToMySql($data['fromdate']);
            }
            $ObjCommonDao = new \database\commondao();
            $Companyinfo = self::loadcompanyinformation();
            $locationinfo = $ObjCommonDao->getLocationInfo();

            $dateformat=\database\parameter::getParameter('dateformat');
            $dateformat_dis=\common\staticarray::$mysqldateformat[$dateformat];


            $timeformat=\database\parameter::getParameter('timeformat');
            $timeformat_dis=\common\staticarray::$mysqltimeformat[$timeformat];

            $strSql = "SELECT FM.foliono".
                      ",CASE WHEN FM.ispostedtopms=1 THEN '".$languageArr->LANG5."'  ELSE '' END AS ispostedtopms" .
                      " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 1 OR FASM.mastertypeunkid = 8 THEN FD.quantity * FD.baseamount ELSE 0 END),".$round_off.") AS itemprice".
                      " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid IN (9,10) THEN FD.quantity * FD.baseamount ELSE 0 END),".$round_off.") AS moditemprice".
                      " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 3 THEN FD.quantity * FD.baseamount ELSE 0 END),".$round_off.") AS discamt".
                      " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 2 THEN FD.quantity * FD.baseamount ELSE 0 END),".$round_off.") AS taxamt".
                      ",DATE_FORMAT(FM.servedate,'".$dateformat_dis." ".$timeformat_dis."') as trandate ,ROUND(IFNULL(SUM(CASE WHEN FASM.mastertypeunkid IN (1,2,3,8,9,10) THEN FD.quantity * FD.baseamount * FASM.crdr ELSE 0 END),0),".$round_off.") AS finalamt".
                      " FROM ".CONFIG_DBN.".fasfoliomaster AS FM".
                      " INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC ON TRC.contactunkid = FM.lnkcontactid AND FM.companyid=TRC.companyid AND FM.locationid=TRC.locationid ".
                      " LEFT JOIN ".CONFIG_DBN.".fasfoliodetail AS FD ON FM.foliounkid = FD.foliounkid AND FM.companyid=FD.companyid AND FM.locationid=FD.locationid AND FD.isvoid = 0 AND FD.isrefund = 0".
                      " LEFT JOIN ".CONFIG_DBN.".fasmaster AS FASM ON FD.masterunkid = FASM.masterunkid AND FD.companyid=FASM.companyid AND FD.locationid=FASM.locationid".
                      " WHERE FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.isvalid = 1 AND FM.canceluserunkid = 0 AND TRC.contacttype != 4 AND FM.companyid=:companyid AND FM.locationid=:locationid ";
            if ($form_date != "" && $to_date != "") {
                $strSql .= " AND CAST(FM.servedate AS DATE) BETWEEN '" . $form_date . "' AND '" . $to_date . "' ";
            }else if($form_date != "" && $to_date == "") {
                $strSql .= " AND CAST(FM.servedate AS DATE)='" . $form_date . "' ";
            }
            if(isset($data['order_type']) && !empty($data['order_type']) && count($data['order_type'])>0){
                $strSql .= " AND FM.ordertype IN (".implode(',',$data['order_type']).") ";
            }
            if($form_date != ""){
                $strSql.= " GROUP BY FM.foliounkid";
            }
            $dao->initCommand($strSql);

            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeQuery();
            $total_tax = $total_item_price = $total_mod_item_price = $total_discount = $total_order_amt = 0;
            foreach($res AS $val){
                $total_item_price += $val['itemprice'];
                $total_mod_item_price += $val['moditemprice'];
                $total_tax += $val['taxamt'];
                $total_discount += $val['discamt'];
               // $total_order_amt += $val['finalamt'];
                $total_order_amt += $val['itemprice'] + $val['moditemprice'] + $val['taxamt'] - $val['discamt'];

            }
            $result['data'] = $res;
            $result['item_price'] = number_format($total_item_price,$round_off);
            $result['modifier_item_price'] = number_format($total_mod_item_price,$round_off);
            $result['item_tax'] = number_format($total_tax,$round_off);
            $result['item_discount'] = number_format($total_discount,$round_off);
            $result['order_amount'] = number_format($total_order_amt,$round_off);
            $result['report_date'] = !empty($to_date)?\util\util::convertMySqlDate($form_date).' to '.\util\util::convertMySqlDate($to_date):\util\util::convertMySqlDate($form_date);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();
            $result['loc_name'] = $locationinfo['locationname'];
            $result['loc_email'] = $locationinfo['locationemail'];
            $result['loc_phone'] = $locationinfo['phone'];
            $result['company_logo'] = !empty($Companyinfo['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$Companyinfo['company_logo']:'';
            return $result;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getdailyrec - '.$e);
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getitemsales($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getitemsales');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $from_date = \util\util::convertDateToMySql($data['fromdate']);
            $to_date = \util\util::convertDateToMySql($data['todate']);
            $Companyinfo = self::loadcompanyinformation();
            $locationinfo = $ObjCommonDao->getLocationInfo();
            $menu_id = '';
            if(isset($data['menu']))
            {
                $menu_id = $ObjCommonDao->getprimarykey('cfmenu',$data['menu'],'menuunkid');
            }
            $category_id = $ObjCommonDao->getprimarykey('cfmenu_categories',$data['category'],'categoryunkid');
            $item_id = $ObjCommonDao->getprimarykey('cfmenu_items',$data['item'],'itemunkid');
            /*Get item price,mod item price,item tax,discount*/
            $strSql = "SELECT tbl.itemname,SUM(tbl.quantity) as quantity,IFNULL(tbl.name,'') AS unitName,tbl.lnkmappingunkid,tbl.categoryname,ROUND(SUM(tbl.itemprice),".$round_off.") AS itemprice,ROUND(SUM(tbl.moditemprice),".$round_off.") AS moditemprice,ROUND(SUM(tbl.itemdiscount),".$round_off.") AS itemdiscount,ROUND(SUM(tbl.itemtax),".$round_off.") AS itemtax ".
                " FROM (SELECT CFI.itemname,FD1.quantity as quantity,CFC.categoryname,CMU.name,CMU.unitunkid,FD1.detailunkid,FD1.lnkmappingunkid".
                " ,ROUND((CASE WHEN FASM1.mastertypeunkid=8 THEN FD1.quantity * FD1.baseamount * FASM1.crdr ELSE 0 END),".$round_off.") AS itemprice".
                " ,ROUND(SUM(CASE WHEN FASM2.mastertypeunkid IN (9,10) THEN FD2.quantity * FD2.baseamount * FASM2.crdr ELSE 0 END),".$round_off.") AS moditemprice".
                " ,ROUND(SUM(CASE WHEN FASM2.mastertypeunkid=3 THEN FD2.quantity * FD2.baseamount ELSE 0 END),".$round_off.") AS itemdiscount".
                " ,ROUND(SUM(CASE WHEN FASM2.mastertypeunkid=2 THEN FD2.quantity * FD2.baseamount * FASM2.crdr ELSE 0 END),".$round_off.") AS itemtax".
                " FROM ".CONFIG_DBN.".fasfoliomaster AS FM
                               INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC 
                                    ON TRC.contactunkid = FM.lnkcontactid AND TRC.locationid =:locationid       
                                LEFT JOIN ".CONFIG_DBN.".fasfoliodetail AS FD1 
                                    ON FM.foliounkid = FD1.foliounkid AND FD1.isvoid = 0 AND FD1.isrefund = 0 AND FD1.locationid=:locationid AND FD1.parentid = 0
                                INNER JOIN ".CONFIG_DBN.".cfmenu_items AS CFI 
                                    ON FD1.lnkmappingunkid = CFI.itemunkid AND CFI.locationid=:locationid
                                INNER JOIN ".CONFIG_DBN.".cfmenu_categories AS CFC 
                                    ON CFI.categoryunkid = CFC.categoryunkid AND CFC.locationid=:locationid
                                LEFT JOIN ".CONFIG_DBN.".fasmaster AS FASM1 
                                    ON FD1.masterunkid = FASM1.masterunkid  AND FASM1.locationid=:locationid
                                LEFT JOIN ".CONFIG_DBN.".fasfoliodetail AS FD2 
                                    ON FM.foliounkid = FD2.foliounkid AND FD1.detailunkid=FD2.parentid AND FD2.isvoid = 0 AND FD2.isrefund = 0 AND FD2.locationid=:locationid
                                    LEFT JOIN ".CONFIG_DBN.".cfmenu_itemunit AS CMU
                                    ON CMU.unitunkid=FD1.lnkunitid and CMU.locationid=:locationid
                                LEFT JOIN ".CONFIG_DBN.".fasmaster AS FASM2 
                                    ON FD2.masterunkid = FASM2.masterunkid AND FASM2.locationid=:locationid
                                WHERE FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.isvalid = 1 AND FM.canceluserunkid = 0 AND TRC.contacttype != 4 AND FM.companyid=:companyid AND FM.locationid=:locationid AND FD1.companyid=:companyid  AND FASM1.mastertypeunkid=8";
            if($data['category']!="" && $data['category']!=0){
                $strSql .= " AND CFI.categoryunkid='".$category_id."'";
            }
            if(isset($menu_id) && $menu_id != '')
            {
                $strSql .= " AND CFC.menuunkid = '".$menu_id."' ";
            }
            if($data['item']!="" && $data['item']!=0){
                $strSql .= " AND FD1.lnkmappingunkid='".$item_id."'";
            }
            if($data['fromdate']!="" && $data['todate']!=""){
                $strSql .= " AND CAST(FM.servedate AS DATE) BETWEEN '".$from_date."' AND '".$to_date."'";
            }
            $strSql .= " GROUP BY FD1.detailunkid) AS tbl GROUP BY tbl.lnkmappingunkid,tbl.unitunkid";

            $dao->initCommand($strSql);

            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeQuery();
            if($res){
                $res=json_decode(html_entity_decode(json_encode($res)),1);
            }
            /*Get item price,mod item price,item tax,discount*/
            /*Get modifier item tax*/
            $strSql1  = "SELECT FD1.lnkmappingunkid".
                " ,ROUND(SUM(CASE WHEN FASM3.mastertypeunkid=2 THEN FD3.quantity * FD3.baseamount * FASM3.crdr ELSE 0 END),".$round_off.") AS moditemtax".
                " FROM ".CONFIG_DBN.".fasfoliomaster AS FM
                            INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC 
                                ON TRC.contactunkid = FM.lnkcontactid AND TRC.locationid =:locationid
                            LEFT JOIN ".CONFIG_DBN.".fasfoliodetail AS FD1 
                                ON FM.foliounkid = FD1.foliounkid AND FD1.isvoid = 0 AND FD1.isrefund = 0 AND FD1.locationid=:locationid AND FD1.parentid = 0
                            INNER JOIN ".CONFIG_DBN.".cfmenu_items AS CFI 
                                ON FD1.lnkmappingunkid = CFI.itemunkid AND CFI.locationid=:locationid
                            INNER JOIN ".CONFIG_DBN.".cfmenu_categories AS CFC 
                                ON CFI.categoryunkid = CFC.categoryunkid AND CFC.locationid=:locationid
                            LEFT JOIN ".CONFIG_DBN.".fasmaster AS FASM1 
                                ON FD1.masterunkid = FASM1.masterunkid AND FASM1.locationid=:locationid AND FASM1.mastertypeunkid=8
                            LEFT JOIN ".CONFIG_DBN.".fasfoliodetail AS FD2 
                                ON FM.foliounkid = FD2.foliounkid AND FD2.isvoid = 0 AND FD2.isrefund = 0 AND FD2.parentid=FD1.detailunkid AND FD2.locationid=:locationid
                            LEFT JOIN ".CONFIG_DBN.".fasmaster AS FASM2 
                                ON FD2.masterunkid = FASM2.masterunkid AND FASM2.locationid=:locationid
                            LEFT JOIN ".CONFIG_DBN.".fasfoliodetail AS FD3 
                                ON FM.foliounkid = FD3.foliounkid AND FD3.parentid=FD2.detailunkid AND FD3.locationid=:locationid
                            LEFT JOIN ".CONFIG_DBN.".fasmaster AS FASM3 
                                ON FD3.masterunkid = FASM3.masterunkid AND FD3.parentid=FD2.detailunkid AND FASM3.locationid=:locationid
                            WHERE FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.isvalid = 1 AND FM.canceluserunkid = 0 AND TRC.contacttype != 4 AND FM.companyid=:companyid AND FM.locationid=:locationid AND FD1.companyid=:companyid  AND FASM1.mastertypeunkid=8";
            if($data['category']!="" && $data['category']!=0){
                $strSql1 .= " AND CFI.categoryunkid='".$category_id."'";
            }
            if($data['item']!="" && $data['item']!=0){
                $strSql1 .= " AND FD1.lnkmappingunkid='".$item_id."'";
            }
            if($data['fromdate']!="" && $data['todate']!=""){
                $strSql1 .= " AND CAST(FM.servedate AS DATE) BETWEEN '".$from_date."' AND '".$to_date."'";
            }
            $strSql1 .= " GROUP BY FD1.lnkmappingunkid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res1 = $dao->executeQuery();
            /*Get modifier item tax*/
            $mod_item_tax = \util\util::getkeyfieldarray($res1,'lnkmappingunkid');
            $cnt = $total_tax = $total_item_price = $total_mod_item_price = $total_discount = $total_item_amt = 0;


            foreach($res AS $val){
                $val['moditemtax'] = (isset($mod_item_tax[$val['lnkmappingunkid']]))?$mod_item_tax[$val['lnkmappingunkid']]['moditemtax']:0;
                $total_item_price += $val['itemprice'];
                $total_mod_item_price += $val['moditemprice'];
                $total_tax += $val['itemtax']+$val['moditemtax'];
                $res[$cnt]['taxamt'] = $val['itemtax']+$val['moditemtax'];
                $res[$cnt]['itemamt'] = $val['itemprice']+$val['moditemprice']+$val['itemtax']+$val['moditemtax']-$val['itemdiscount'];
                $total_discount +=$val['itemdiscount'];
                $total_item_amt +=$res[$cnt]['itemamt'];
                $cnt++;
            }
            $result['data'] = $res;
            $result['item_price'] = number_format($total_item_price,$round_off);
            $result['modifier_item_price'] = number_format($total_mod_item_price,$round_off);
            $result['item_tax'] = number_format($total_tax,$round_off);
            $result['item_discount'] = number_format($total_discount,$round_off);
            $result['order_amount'] = number_format($total_item_amt,$round_off);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            $result['from_date'] = \util\util::convertMySqlDate($from_date);
            $result['to_date'] = \util\util::convertMySqlDate($to_date);
            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();
            $result['loc_name'] = $locationinfo['locationname'];
            $result['loc_email'] = $locationinfo['locationemail'];
            $result['loc_phone'] = $locationinfo['phone'];
            $result['company_logo'] = !empty($Companyinfo['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$Companyinfo['company_logo']:'';

            return $result;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getitemsales - '.$e);
        }
    }

//
//    public function getcombosales($data)
//    {
//        try
//        {
//            $this->log->logIt($this->module.' - getitemsales');
//            $dao = new \dao();
//            $ObjCommonDao = new \database\commondao();
//            $current_date = \util\util::getLocalDate();
//            $round_off = \database\parameter::getParameter('digitafterdecimal');
//            $from_date = \util\util::convertDateToMySql($data['fromdate']);
//            $to_date = \util\util::convertDateToMySql($data['todate']);
//            $combo_id = $ObjCommonDao->getprimarykey('cfmenu_combo',$data['combo'],'combounkid');
//            $strSql = "SELECT tbl.comboname,tbl.lnkmappingunkid,ROUND(SUM(tbl.base_price),".$round_off.") AS base_price,ROUND(SUM(tbl.discount),".$round_off.") AS discount,ROUND(SUM(tbl.tax),".$round_off.") AS tax ".
//                      " FROM (SELECT CFC.comboname,FD1.lnkmappingunkid".
//                      " ,ROUND((CASE WHEN FASM1.mastertypeunkid=1 THEN FD1.quantity * FD1.baseamount * FASM1.crdr ELSE 0 END),".$round_off.") AS base_price".
//                      " ,ROUND(SUM(CASE WHEN FASM2.mastertypeunkid=3 THEN FD2.quantity * FD2.baseamount ELSE 0 END),".$round_off.") AS discount".
//                      " ,ROUND(SUM(CASE WHEN FASM2.mastertypeunkid=2 THEN FD2.quantity * FD2.baseamount * FASM2.crdr ELSE 0 END),".$round_off.") AS tax".
//                      " FROM ".CONFIG_DBN.".fasfoliomaster AS FM
//                                LEFT JOIN ".CONFIG_DBN.".fasfoliodetail AS FD1
//                                    ON FM.foliounkid = FD1.foliounkid AND FD1.isvoid = 0 AND FD1.locationid=:locationid
//                                INNER JOIN ".CONFIG_DBN.".cfmenu_combo AS CFC
//                                    ON FD1.lnkmappingunkid = CFC.combounkid AND CFC.locationid=:locationid
//                                LEFT JOIN ".CONFIG_DBN.".fasmaster AS FASM1
//                                    ON FD1.masterunkid = FASM1.masterunkid AND FASM1.locationid=:locationid
//                                LEFT JOIN ".CONFIG_DBN.".fasfoliodetail AS FD2
//                                    ON FM.foliounkid = FD2.foliounkid AND FD1.detailunkid=FD2.parentid AND FD2.isvoid = 0 AND FD2.locationid=:locationid
//                                LEFT JOIN ".CONFIG_DBN.".fasmaster AS FASM2
//                                    ON FD2.masterunkid = FASM2.masterunkid AND FASM2.locationid=:locationid
//                                WHERE FM.isvoid = 0 AND FM.companyid=:companyid AND FM.locationid=:locationid AND FD1.companyid=:companyid AND FD2.companyid=:companyid AND FASM1.mastertypeunkid=1";
//            if($data['combo']!="" && $data['combo']!=0){
//                $strSql .= " AND FD1.lnkmappingunkid='".$combo_id."'";
//            }
//            if($data['fromdate']!="" && $data['todate']!=""){
//                $strSql .= " AND CAST(FD1.trandate AS DATE) BETWEEN '".$from_date."' AND '".$to_date."'";
//            }
//            $strSql .= " GROUP BY FD1.detailunkid) AS tbl GROUP BY tbl.lnkmappingunkid";
//            $dao->initCommand($strSql);
//            $dao->addParameter(':companyid',CONFIG_CID);
//            $dao->addparameter(':locationid',CONFIG_LID);
//            $res = $dao->executeQuery();
//
//            $cnt = $total_tax = $total_combo_price = $total_discount = $total_amt = 0;
//            foreach($res AS $val){
//                $total_combo_price += $val['base_price'];
//                $total_tax += $val['tax'];
//                $res[$cnt]['base_amt'] = number_format($val['base_price']+$val['tax']-$val['discount'],$round_off);
//                $base_amt = $val['base_price']+$val['tax']-$val['discount'];
//                $total_discount += $val['discount'];
//                $total_amt += $base_amt;
//                $cnt++;
//            }
//
//            $result['data'] = $res;
//            $result['total_base_price'] = number_format($total_combo_price,$round_off);
//            $result['total_tax'] = number_format($total_tax,$round_off);
//            $result['total_discount'] = number_format($total_discount,$round_off);
//            $result['total_order_amount'] = number_format($total_amt,$round_off);
//            $result['from_date'] = \util\util::convertMySqlDate($from_date);
//            $result['to_date'] = \util\util::convertMySqlDate($to_date);
//            $result['current_date'] = \util\util::convertMySqlDate($current_date);
//            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();
//            $result['numberformat'] = $round_off;
//
//            return $result;
//        }
//        catch(Exception $e)
//        {
//            $this->log->logIt($this->module.' - getitemsales - '.$e);
//        }
//    }

    public function getcombosales($data)
    {
        try {
            $this->log->logIt($this->module . ' - getcombosales');
            $dao = new \dao();

            $form_date = $to_date='';
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            if(isset($data['todate'])){
                $to_date = \util\util::convertDateToMySql($data['todate']);
            }
            if(isset($data['fromdate'])){
                $from_date = \util\util::convertDateToMySql($data['fromdate']);
            }
            $ObjCommonDao = new \database\commondao();
            $Companyinfo = self::loadcompanyinformation();
            $locationinfo = $ObjCommonDao->getLocationInfo();

            $combo_id = '';

            if (isset($data['combo']) && $data['combo'] != 0) {
                $combo_id =$ObjCommonDao->getprimarykey('cfmenu_combo', $data['combo'], 'combounkid');
            }

            $strSql = "SELECT tbl.comboname,tbl.lnkmappingunkid,ROUND(SUM(tbl.base_amount)," . $round_off . ") AS base_amount,ROUND(SUM(tbl.tax)," . $round_off . ") AS tax,ROUND(SUM(tbl.qty)) AS qty" .
                " FROM (SELECT CFC.comboname,FD1.lnkmappingunkid" .
                " ,ROUND((CASE WHEN FASM1.mastertypeunkid=1 THEN FD1.quantity * FD1.baseamount * FASM1.crdr ELSE 0 END)," . $round_off . ") AS base_amount,ROUND((CASE WHEN FASM1.mastertypeunkid=1 THEN FD1.quantity ELSE 0 END)) AS qty" .
                " ,ROUND(SUM(CASE WHEN FASM2.mastertypeunkid=2 THEN FD2.quantity * FD2.baseamount * FASM2.crdr ELSE 0 END)," . $round_off . ") AS tax,FM.foliono" .
                " FROM " . CONFIG_DBN . ".fasfoliomaster AS FM
                                LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS FD1 
                                    ON FM.foliounkid = FD1.foliounkid AND FD1.isvoid = 0 AND FD1.isrefund = 0 AND FD1.companyid=:companyid AND FD1.locationid=:locationid 
                                INNER JOIN " . CONFIG_DBN . ".cfmenu_combo AS CFC 
                                    ON FD1.lnkmappingunkid = CFC.combounkid AND CFC.companyid=:companyid AND CFC.locationid=:locationid
                                LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM1 
                                    ON FD1.masterunkid = FASM1.masterunkid AND FASM1.companyid=:companyid AND FASM1.locationid=:locationid AND FASM1.mastertypeunkid=1
                                LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS FD2 
                                    ON FM.foliounkid = FD2.foliounkid AND FD1.detailunkid=FD2.parentid AND FD2.isvoid = 0 AND FD2.isrefund = 0 AND FD2.companyid=:companyid AND FD2.locationid=:locationid
                                     INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC ON TRC.contactunkid = FM.lnkcontactid AND FM.companyid=TRC.companyid AND FM.locationid=TRC.locationid
                                LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM2 
                                    ON FD2.masterunkid = FASM2.masterunkid AND FASM2.companyid=:companyid AND FASM2.locationid=:locationid AND FASM2.mastertypeunkid=2
                                WHERE FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.isvalid = 1 AND FM.canceluserunkid = 0 AND TRC.contacttype != 4 AND FM.companyid=:companyid AND FM.locationid=:locationid ";
            if (isset($data['combo']) && $data['combo'] != "" && $data['combo'] != 0) {
                $strSql .= " AND FD1.lnkmappingunkid='" . $combo_id . "'";
            }

            if ($from_date != "" && $to_date != "") {
                $strSql .= " AND CAST(FD1.trandate AS DATE) BETWEEN '" . $from_date . "' AND '" . $to_date . "' ";
            }else if($from_date != "" && $to_date == "") {
                $strSql .= " AND CAST(FD1.trandate AS DATE)='" . $from_date . "' ";
            }

            $strSql .= " GROUP BY FD1.detailunkid) AS tbl GROUP BY tbl.lnkmappingunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeQuery();
            $cnt = $total_tax = $total_combo_price = $total_order_amount = $total_qty = 0;

            foreach ($res AS $val) {
                $total_combo_price += $val['base_amount'];
                $total_tax += $val['tax'];
                $total_qty += $val['qty'];
                $res[$cnt]['order_amount'] = number_format($val['base_amount'] + $val['tax'], $round_off);
                $order_amount = $val['base_amount'] + $val['tax'];
                $total_order_amount += $order_amount;
                $cnt++;
            }

            $result = array();
            $result['data'] = $res;
            $result['total_base_amount'] = number_format($total_combo_price, $round_off);
            $result['total_tax'] = number_format($total_tax, $round_off);
            $result['total_order_amount'] = number_format($total_order_amount, $round_off);
            $result['total_qty'] = $total_qty;
            $result['report_date'] = !empty($to_date)?\util\util::convertMySqlDate($from_date).' to '.\util\util::convertMySqlDate($to_date):\util\util::convertMySqlDate($form_date);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            $result['from_date'] = \util\util::convertMySqlDate($from_date);
            $result['to_date'] = \util\util::convertMySqlDate($to_date);
            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();
            $result['loc_name'] = $locationinfo['locationname'];
            $result['loc_email'] = $locationinfo['locationemail'];
            $result['loc_phone'] = $locationinfo['phone'];
            $result['company_logo'] = !empty($Companyinfo['company_logo']) ? CONFIG_BASE_URL . "companyadmin/assets/company_logo/" . $Companyinfo['company_logo'] : '';

            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getcombosales - ' . $e);
        }
    }

    public function loadcompanyinformation()
    {
        try
        {
            $this->log->logIt($this->module.' - loadcompanyinformation');
            $dao = new \dao();
            $strSql = "SELECT companyname,currency_sign,companyemail,phone,IFNULL(logo,'') as company_logo FROM ".CONFIG_DBN.".cfcompany WHERE companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $res = $dao->executeRow();
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loadcompanyinformation - '.$e);
        }
    }

    public function getitemlocationwise($data)
    {
        try
        {
            $this->log->logIt($this->module.'-getitemlocationwise');
            $dao = new \dao();
            $order_date = \util\util::convertDateToMySql($data['fromdate']);
            $current_date = \util\util::getLocalDate();
            $ObjCommonDao = new \database\commondao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $str = "SELECT locationid,locationname FROM ".CONFIG_DBN.".cflocation  WHERE companyid=:companyid";
            $dao->initCommand($str);
            $dao->addParameter(':companyid',CONFIG_CID);
            $locationdata = $dao->executeQuery();
            $arraylocation = \util\util::getkeyfieldarray($locationdata,'locationid');
            $location = array_column($locationdata, 'locationid');

            $loc = implode(',', $location);

            $strSql = "SELECT FM.foliono,FM.locationid,cfl.locationname" .
                    " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 1 OR FASM.mastertypeunkid = 8 THEN FD.quantity * FD.baseamount ELSE 0 END)," . $round_off . ") AS itemprice" .
                    " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid IN (9,10) THEN FD.quantity * FD.baseamount ELSE 0 END)," . $round_off . ") AS moditemprice" .
                    " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 3 THEN FD.quantity * FD.baseamount ELSE 0 END)," . $round_off . ") AS discamt" .
                    " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 2 THEN FD.quantity * FD.baseamount ELSE 0 END)," . $round_off . ") AS taxamt" .
                " ,ROUND(IFNULL(SUM(CASE WHEN FASM.mastertypeunkid IN (1,2,3,8,9,10) THEN FD.quantity * FD.baseamount * FASM.crdr ELSE 0 END),0),".$round_off.") AS finalamt".
                    " FROM " . CONFIG_DBN . ".fasfoliomaster AS FM ".
                " INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC ON TRC.contactunkid = FM.lnkcontactid AND FM.companyid = TRC.companyid AND FM.locationid=TRC.locationid ".
                    " LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS FD ON FM.foliounkid = FD.foliounkid AND FM.companyid=FD.companyid AND FM.locationid=FD.locationid AND FD.isvoid = 0 AND FD.isrefund = 0" .
                    " LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FD.masterunkid = FASM.masterunkid AND FD.companyid=FASM.companyid AND FD.locationid=FASM.locationid" .
                    " LEFT JOIN " . CONFIG_DBN . ". cflocation AS cfl ON cfl.locationid=FM.locationid AND cfl.companyid=:companyid" .
                    " WHERE FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.isvalid = 1 AND FM.canceluserunkid = 0 AND TRC.contacttype != 4 AND CAST(FM.servedate AS DATE)='" . $order_date . "' AND FASM.mastertypeunkid NOT IN (4,7) AND FM.companyid=:companyid AND FM.locationid IN (" . $loc . ")    GROUP BY FM.foliounkid ORDER BY FM.locationid ";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $res = $dao->executeQuery();

            $total_tax = $total_item_price = $total_mod_item_price = $total_discount = $total_order_amt = 0;
            foreach($res AS $val){
                $total_item_price += $val['itemprice'];
                $total_mod_item_price += $val['moditemprice'];
                $total_tax += $val['taxamt'];
                $total_discount += $val['discamt'];
                //$total_order_amt += $val['finalamt'];
                $total_order_amt += $val['itemprice'] + $val['moditemprice'] + $val['taxamt'] - $val['discamt'];

            }
            $result['data'] = $res;
            $result['item_price'] = number_format($total_item_price,$round_off);
            $result['modifier_item_price'] = number_format($total_mod_item_price,$round_off);
            $result['item_tax'] = number_format($total_tax,$round_off);
            $result['item_discount'] = number_format($total_discount,$round_off);
            $result['order_amount'] = number_format($total_order_amt,$round_off);
            $result['report_date'] = \util\util::convertMySqlDate($order_date);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();

            $arr_item = array();

            foreach($result['data'] as $key => $item)
            {
                $result[$item['locationid']][] = $item;
                $arr_item[$item['locationid']][] = $item;
                $arraylocation[$item['locationid']]['items'] = $arr_item[$item['locationid']];
            }
            $result['location'] = $arraylocation;

            ksort($result, SORT_NUMERIC);

            return $result;

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-getitemlocationwise-'.$e);
        }
    }

    public function getrawmaterial($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getrawmaterial');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $catid = $ObjCommonDao->getprimaryBycompany('cfrawmaterial_category',$data['rawcat'],'id');

            $strSql = "SELECT CR.name,CR.code,
                      IF(RL.inventory<=RL.min_limit AND RL.min_limit!=0,'YES','NO') as min_limit,
                      RC.name as category,
                      ROUND(RL.inventory,$round_off) as inventory,
                      CU.shortcode as inv_unit,
                      ROUND(RL.rateperunit,$round_off) as rateperunit,
                      TRC.name as vendor_name
                      FROM ".CONFIG_DBN.".cfrawmaterial as CR   
                      LEFT JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS RL
                        ON RL.lnkrawmaterialid=CR.id and RL.locationid=:locationid
                      LEFT JOIN ".CONFIG_DBN.".cfrawmaterial_category AS RC
                        ON RC.id=CR.lnkcategoryid 
                      INNER JOIN ".CONFIG_DBN.".cfvandor AS CV
                      ON CV.vandorunkid=CR.lnkvandorid 
                      LEFT JOIN ".CONFIG_DBN.".cfunit AS CU
                        ON CU.unitunkid=RL.inv_unitid 
                      LEFT JOIN ".CONFIG_DBN.".trcontact AS TRC
                        ON CV.lnkcontactid=TRC.contactunkid 
                      WHERE CR.companyid=:companyid AND CR.is_deleted=0
                      AND CR.is_active=1";

            if($data['rawcat']!="" && $data['rawcat']!=0){
                $strSql .= " AND CR.lnkcategoryid='".$catid."'";
            }
            if($data['rawmaterial']!="" && $data['rawmaterial']!=0){
                $strSql .= " AND CR.hashkey='".$data['rawmaterial']."'";
            }
            if($data['shortage']!="" && $data['shortage']==1){
                $strSql .= " AND RL.inventory<=RL.min_limit AND RL.min_limit!=0";
            }
            if($data['shortage']!="" && $data['shortage']==0){
                $strSql .= " AND RL.inventory>RL.min_limit";
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $resobj = $dao->executeQuery();
            $result['data'] = $resobj;
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            return $result;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getrawmaterial - '.$e);
        }
    }

    public function getvendor($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getvendor');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $current_date = \util\util::getLocalDate();
            $contactid = $ObjCommonDao->getprimaryBycompany('trcontact',$data['vendor'],'contactunkid');

            $strSql = "SELECT CR.name AS rawmaterial,TRC.name ,
                            IF(RL.inventory<=RL.min_limit AND RL.min_limit!=0,'YES','NO') as min_limit
                            FROM ".CONFIG_DBN.".cfrawmaterial CR
                            INNER JOIN ".CONFIG_DBN.".cfvandor CV 
                            ON CV.vandorunkid=CR.lnkvandorid
                            INNER JOIN ".CONFIG_DBN.".trcontact TRC 
                            ON TRC.contactunkid=CV.lnkcontactid
                            LEFT JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS RL
                            ON RL.lnkrawmaterialid=CR.id and RL.locationid=:locationid
                            WHERE CV.is_deleted=0 AND CR.is_deleted=0 
                            AND CV.companyid=:companyid  
                            and CR.companyid=:companyid";

            if($data['vendor']!="" && $data['vendor']!=0){
                $strSql .= " AND CV.lnkcontactid='".$contactid."'";
            }
            if($data['shortage']!="" && $data['shortage']==1){
                $strSql .= " AND RL.inventory<=RL.min_limit AND RL.min_limit!=0";
            }
            if($data['shortage']!="" && $data['shortage']==0){
                $strSql .= " AND RL.inventory>RL.min_limit";
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $resobj = $dao->executeQuery();
            $result['data'] = $resobj;
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            return $result;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getvendor - '.$e);
        }
    }

    public function getrecipe($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getrecipe');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $current_date = \util\util::getLocalDate();
            $itemtype = $data['itemtype'];
            $resultData = array();
            if($itemtype ==1){
                $itemid = $ObjCommonDao->getprimaryBycompany('cfmenu_items',$data['item'],'itemunkid');
                $strSql = "SELECT CFI.itemname,CFI.itemunkid,
                      IFNULL(sum(ROUND((CIR.unit*CRL.rateperunit),$round_off)),'') as total_cost
                      FROM ".CONFIG_DBN.".cfmenu_item_recipe as CIR
                      INNER JOIN ".CONFIG_DBN.".cfmenu_items AS CFI
                        ON CFI.itemunkid=CIR.lnkitemid
                      INNER JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS CRL
                        ON CRL.lnkrawmaterialid=CIR.lnkrawmaterialid AND CRL.companyid=:companyid AND CRL.locationid=:locationid 
                      WHERE CIR.companyid=:companyid AND CIR.locationid=:locationid
                      AND CIR.recipe_type=$itemtype";

                if($data['item']!="" && $data['item']!=0){
                    $strSql .= " AND CIR.lnkitemid='".$itemid."'";
                }
                $strSql .= " group by CFI.itemunkid";

                $dao->initCommand($strSql);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $res = $dao->executeQuery();

                foreach ($res as $val){
                    $strSqlr = "SELECT ROUND((CIR.unit/CU.unit),$round_off)as quantity,
                      ROUND(CRL.rateperunit,$round_off) as rateperunit,
                      CR.name as raw_name,CU.shortcode,ROUND(CU.unit,$round_off) as unit,
                      ROUND((CIR.unit*CRL.rateperunit),$round_off)as base_cost
                      FROM ".CONFIG_DBN.".cfmenu_item_recipe as CIR      
                      INNER JOIN ".CONFIG_DBN.".cfrawmaterial AS CR
                        ON CR.id=CIR.lnkrawmaterialid
                      INNER JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS CRL
                        ON CRL.lnkrawmaterialid=CIR.lnkrawmaterialid AND CRL.companyid=:companyid AND CRL.locationid=:locationid 
                      INNER JOIN ".CONFIG_DBN.".cfunit AS CU
                        ON CU.unitunkid=CIR.lnkunitid
                      WHERE CIR.recipe_type=$itemtype AND lnkitemid='".$val['itemunkid']."'";

                    if($data['item']!="" && $data['item']!=0){
                        $strSqlr .= " AND CIR.lnkitemid='".$itemid."'";
                    }
                    $dao->initCommand($strSqlr);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $resobj = $dao->executeQuery();
                    $result[$val['itemunkid']]['data'] = $resobj;
                    $resultData[$val['itemunkid']]['itemname']=$val['itemname'];
                    $resultData[$val['itemunkid']]['totalcost']=$val['total_cost'];
                    $resultData[$val['itemunkid']]['rawdata']= $result[$val['itemunkid']]['data'];
                }
                $result['data'] = $resultData;
                $result['current_date'] = \util\util::convertMySqlDate($current_date);
            }
            elseif($itemtype ==2){
                $itemid = $ObjCommonDao->getprimaryBycompany('cfmenu_modifiers',$data['item'],'modifierunkid');
                $strSql = "SELECT CFMODI.modifiername as itemname,CFMODI.modifierunkid,
                      IFNULL(sum(ROUND((CIR.unit*CRL.rateperunit),$round_off)),'') as total_cost
                      FROM ".CONFIG_DBN.".cfmenu_item_recipe as CIR
                      INNER JOIN ".CONFIG_DBN.".cfmenu_modifiers AS CFMODI
                        ON CFMODI.modifierunkid=CIR.lnkitemid
                      INNER JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS CRL
                        ON CRL.lnkrawmaterialid=CIR.lnkrawmaterialid AND CRL.companyid=:companyid AND CRL.locationid=:locationid 
                      WHERE CIR.companyid=:companyid AND CIR.locationid=:locationid
                      AND CIR.recipe_type=$itemtype ";

                if($data['item']!="" && $data['item']!=0){
                    $strSql .= " AND CIR.lnkitemid='".$itemid."'";
                }
                $strSql .= " group by CFMODI.modifierunkid";

                $dao->initCommand($strSql);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $res = $dao->executeQuery();

                foreach ($res as $val){
                    $strSqlr = "SELECT ROUND((CIR.unit/CU.unit),$round_off)as quantity,
                      ROUND(CRL.rateperunit,$round_off) as rateperunit,
                      CR.name as raw_name,CU.shortcode,ROUND(CU.unit,$round_off) as unit,
                      ROUND((CIR.unit*CRL.rateperunit),$round_off)as base_cost
                      FROM ".CONFIG_DBN.".cfmenu_item_recipe as CIR   
                      INNER JOIN ".CONFIG_DBN.".cfrawmaterial AS CR
                        ON CR.id=CIR.lnkrawmaterialid
                      INNER JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS CRL
                        ON CRL.lnkrawmaterialid=CIR.lnkrawmaterialid AND CRL.companyid=:companyid AND CRL.locationid=:locationid 
                      INNER JOIN ".CONFIG_DBN.".cfunit AS CU
                        ON CU.unitunkid=CIR.lnkunitid
                      WHERE CIR.recipe_type=$itemtype AND lnkitemid='".$val['modifierunkid']."'";

                    if($data['item']!="" && $data['item']!=0){
                        $strSqlr .= " AND CIR.lnkitemid='".$itemid."'";
                    }
                    $dao->initCommand($strSqlr);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $resobj = $dao->executeQuery();
                    $result[$val['modifierunkid']]['data'] = $resobj;
                    $resultData[$val['modifierunkid']]['itemname']=$val['itemname'];
                    $resultData[$val['modifierunkid']]['totalcost']=$val['total_cost'];
                    $resultData[$val['modifierunkid']]['rawdata']= $result[$val['modifierunkid']]['data'];
                }
                $result['data'] = $resultData;
                $result['current_date'] = \util\util::convertMySqlDate($current_date);
            }
            else{
                $itemid = $ObjCommonDao->getprimaryBycompany('cfmenu_modifier_items',$data['item'],'modifieritemunkid');
                $strSql = "SELECT CFI.itemname,CFI.modifieritemunkid,
                      IFNULL(sum(ROUND((CIR.unit*CRL.rateperunit),$round_off)),'') as total_cost
                      FROM ".CONFIG_DBN.".cfmenu_item_recipe as CIR
                      INNER JOIN ".CONFIG_DBN.".cfmenu_modifier_items AS CFI
                        ON CFI.modifieritemunkid=CIR.lnkitemid                     
                      INNER JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS CRL
                        ON CRL.lnkrawmaterialid=CIR.lnkrawmaterialid AND CRL.companyid=:companyid AND CRL.locationid=:locationid 
                      WHERE CIR.companyid=:companyid AND CIR.locationid=:locationid
                      AND CIR.recipe_type=$itemtype ";

                if($data['item']!="" && $data['item']!=0){
                    $strSql .= " AND CIR.lnkitemid='".$itemid."'";
                }
                $strSql .= " group by CFI.modifieritemunkid";

                $dao->initCommand($strSql);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $res = $dao->executeQuery();

                foreach ($res as $val){
                    $strSqlr = "SELECT ROUND((CIR.unit/CU.unit),$round_off)as quantity,
                      ROUND(CRL.rateperunit,$round_off) as rateperunit,
                      CR.name as raw_name,CU.shortcode,ROUND(CU.unit,$round_off) as unit,
                      ROUND((CIR.unit*CRL.rateperunit),$round_off)as base_cost
                      FROM ".CONFIG_DBN.".cfmenu_item_recipe as CIR
                      INNER JOIN ".CONFIG_DBN.".cfrawmaterial AS CR
                        ON CR.id=CIR.lnkrawmaterialid
                      INNER JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS CRL
                        ON CRL.lnkrawmaterialid=CIR.lnkrawmaterialid AND CRL.companyid=:companyid AND CRL.locationid=:locationid 
                      INNER JOIN ".CONFIG_DBN.".cfunit AS CU
                        ON CU.unitunkid=CIR.lnkunitid
                      WHERE CIR.recipe_type=$itemtype AND lnkitemid='".$val['modifieritemunkid']."'";

                    if($data['item']!="" && $data['item']!=0){
                        $strSqlr .= " AND CIR.lnkitemid='".$itemid."'";
                    }
                    $dao->initCommand($strSqlr);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $resobj = $dao->executeQuery();
                    $result[$val['modifieritemunkid']]['data'] = $resobj;
                    $resultData[$val['modifieritemunkid']]['itemname']=$val['itemname'];
                    $resultData[$val['modifieritemunkid']]['totalcost']=$val['total_cost'];
                    $resultData[$val['modifieritemunkid']]['rawdata']= $result[$val['modifieritemunkid']]['data'];
                }
                $result['data'] = $resultData;
                $result['current_date'] = \util\util::convertMySqlDate($current_date);
            }
            return $result;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getrecipe - '.$e);
        }
    }

   /* public function getInventory($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getInventory');
            $current_date = \util\util::getLocalDate();
            $rawhash = $data['rawmaterial'];
            $fromdate = \util\util::convertDateToMySql($data['fromdate']);
            $todate = \util\util::convertDateToMySql($data['todate']);

            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $rawid =$ObjCommonDao->getprimaryBycompany('cfrawmaterial',$data['rawmaterial'],'id');

            $strSql = "SELECT `name` FROM ".CONFIG_DBN.". cfrawmaterial WHERE hashkey=:rawhash and companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':rawhash',$rawhash);
            $dao->addParameter(':companyid', CONFIG_CID);
            $rawlist = $dao->executeRow();
            $result['rawmaterial']=$rawlist['name'];

            $strSql = "SELECT inventory FROM ".CONFIG_DBN.". cfrawmaterial_loc WHERE lnkrawmaterialid=:rawid and companyid=:companyid and locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':rawid',$rawid);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $invlist = $dao->executeRow();
            $result['reduceinventory']=$invlist['inventory'];

            $resultData = array();
            $resultModi=array();
            $total_unit= $total_order_unit=$total_modi_unit=$total_inv_unit=$total_wastage_unit=0;
            $order_unit = $modi_unit = $inv_unit = $wastage_unit= '';
            $con = $this->mongo_con;

            //Item Inventory
            if (isset($con)) {
                $filter = [
                    'rawhash' => $rawhash,
                    'companyid' => CONFIG_CID,
                    "locationid" => CONFIG_LID,
                ];
                if($fromdate!="" && $todate!=""){
                    $datefilter=[
                        '$gte' => $fromdate, '$lte' => $todate,
                    ];
                    $filter['todaydate'] = $datefilter;
                }
                $cnt = 0;
                $query = new \MongoDB\Driver\Query($filter);
                $cursor = $con->executeQuery(CONFIG_DBN . '.item_invenory', $query);
                foreach ($cursor as $document) {
                    $total_unit+= $document->unitcount;
                    $total_order_unit += $document->unitcount;
                    $order_unit=$document->unit;
                    $resultData[$cnt]['rawhash'] = $document->rawhash;
                    $resultData[$cnt]['item'] = $document->item;
                    $resultData[$cnt]['itemcount'] = $document->itemcount;
                    $resultData[$cnt]['unitcount'] = $document->unitcount;
                    $resultData[$cnt]['unit'] = $document->unit;
                    $resultData[$cnt]['date'] = \util\util::convertMySqlDate($document->todaydate);
                    $cnt++;
                }
            }

            //Modifier Inventory
            if (isset($con)) {
                $filter = [
                    'rawhash' => $rawhash,
                    'companyid' => CONFIG_CID,
                    "locationid" => CONFIG_LID,
                ];
                if($fromdate!="" && $todate!=""){
                    $datefilter=[
                        '$gte' => $fromdate, '$lte' => $todate,
                    ];
                    $filter['todaydate'] = $datefilter;
                }
                $cnt = 0;
                $query = new \MongoDB\Driver\Query($filter);
                $cursor = $con->executeQuery(CONFIG_DBN . '.modifier_invenory', $query);
                foreach ($cursor as $document) {
                    $total_unit+= $document->unitcount;
                    $total_modi_unit += $document->unitcount;
                    $modi_unit=$document->unit;
                    $resultModi[$cnt]['rawhash'] = $document->rawhash;
                    $resultModi[$cnt]['modifier'] = $document->modifier;
                    $resultModi[$cnt]['itemcount'] = $document->itemcount;
                    $resultModi[$cnt]['unitcount'] = $document->unitcount;
                    $resultModi[$cnt]['unit'] = $document->unit;
                    $resultModi[$cnt]['date'] = \util\util::convertMySqlDate($document->todaydate);
                    $cnt++;
                }
            }

            //Wastage inventory
            $strSql = "SELECT ROUND((CW.inventory/CU.unit),$round_off) as inventory,
                        CW.inventory as base_inventory,
                        CW.description,CU.shortcode ,
                        DATE_FORMAT(CW.createddatetime,'".$mysqlformat."') as date,
                        CU1.shortcode as base_code
                        FROM ".CONFIG_DBN.".cfwastage as CW
                        INNER JOIN ".CONFIG_DBN.".cfunit AS CU
                        ON CU.unitunkid=CW.inv_unitid
                        INNER JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS CL   
                        ON CW.lnkrawmaterialid=CL.lnkrawmaterialid AND CL.companyid=:companyid AND CL.locationid=:locationid
                        INNER JOIN ".CONFIG_DBN.".cfunit AS CU1
                        ON CL.inv_unitid=CU1.unitunkid 
                        WHERE CW.companyid=:companyid AND CW.locationid=:locationid 
                        AND CW.lnkrawmaterialid=:id AND CW.is_deleted=0";

            if($fromdate!="" && $todate!=""){
                $strSql .= " AND DATE_FORMAT(CW.createddatetime,'%Y-%m-%d') BETWEEN '".$fromdate."' AND '".$todate."'";
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->addParameter(':id',$rawid);
            $res = $dao->executeQuery();
            foreach ($res as $val) {
                $total_unit += $val['base_inventory'];
                $total_wastage_unit += $val['base_inventory'];
                $wastage_unit = $val['base_code'];
            }

          //Transfer inventory
           $invstatus=2;
           $strSql = "SELECT ROUND(((CI.inventory/CU.unit)*-1),$round_off) as inventory,
                        (CI.inventory*-1) as base_inventory,
                        IFNULL(LOC.locationname,'') as location,ROUND(CI.rate,$round_off)as rate,
                        CU.shortcode, DATE_FORMAT(CI.createddatetime,'".$mysqlformat."') as date,
                        CU1.shortcode as base_code
                        FROM ".CONFIG_DBN.".cfinventorydetail as CI                       
                        INNER JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS CL
                        ON CI.lnkrawmateriallocid=CL.id AND CL.companyid=:companyid AND CL.locationid=:locationid
                        INNER JOIN ".CONFIG_DBN.".cfunit AS CU
                        ON CU.unitunkid=CI.inv_unitid
                        INNER JOIN ".CONFIG_DBN.".cfrawmaterial AS CR
                        ON CI.lnkrawmaterialid=CR.id                       
                        LEFT JOIN ".CONFIG_DBN.".cflocation LOC
                        ON LOC.locationid=CI.rel_locationid   
                        INNER JOIN ".CONFIG_DBN.".cfunit AS CU1
                        ON CL.inv_unitid=CU1.unitunkid 
                        WHERE CI.companyid=:companyid AND CI.locationid =:locationid 
                        AND CI.lnkrawmaterialid=:id AND CI.inv_status=:invstatus AND CI.is_deleted=0";

            if($fromdate!="" && $todate!=""){
                $strSql .= " AND DATE_FORMAT(CI.createddatetime,'%Y-%m-%d') BETWEEN '".$fromdate."' AND '".$todate."'";
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->addparameter(':invstatus',$invstatus);
            $dao->addParameter(':id',$rawid);
            $resdata = $dao->executeQuery();
            foreach ($resdata as $val) {
                $total_unit+=$val['base_inventory'];
                $total_inv_unit+=$val['base_inventory'];
                $inv_unit = $val['base_code'];
            }

            $result['inventory'] = $resultData;
            $result['modifier'] = $resultModi;
            $result['wastage']=$res;
            $result['transfer']=$resdata;
            $result['order_unit']=$order_unit;
            $result['total_order_unit'] = number_format($total_order_unit,$round_off);
            $result['modi_unit']=$modi_unit;
            $result['total_modi_unit'] = number_format($total_modi_unit,$round_off);
            $result['wastage_unit']=$wastage_unit;
            $result['total_wastage_unit'] = number_format($total_wastage_unit,$round_off);
            $result['inv_unit']=$inv_unit;
            $result['total_inv_unit'] = number_format($total_inv_unit,$round_off);
            $result['total_unit']=number_format($total_unit,$round_off);
            $result['frominventory']=number_format(($total_unit+$invlist['inventory']),$round_off);
            $result['fromdate'] = \util\util::convertMySqlDate($fromdate);
            $result['todate'] = \util\util::convertMySqlDate($todate);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            return $result;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getInventory - '.$e);
        }
    }*/

    public function getInventory($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getInventory');
            $current_date = \util\util::getLocalDate();
            $rawhash = $data['rawmaterial'];
            $fromdate = \util\util::convertDateToMySql($data['fromdate']);
            $todate = \util\util::convertDateToMySql($data['todate']);

            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $rawid =$ObjCommonDao->getprimarykey('cfrawmaterial',$data['rawmaterial'],'id');

            $strSql = "SELECT `name` FROM ".CONFIG_DBN.". cfrawmaterial WHERE hashkey=:rawhash and companyid=:companyid and locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':rawhash',$rawhash);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $rawlist = $dao->executeRow();
            $result['rawmaterial']=$rawlist['name'];

            $strSql = "SELECT RL.inventory,CU.shortcode FROM ".CONFIG_DBN.". cfrawmaterial_loc as RL
             INNER JOIN ".CONFIG_DBN.".cfunit as CU on CU.unitunkid=RL.inv_unitid
             WHERE RL.lnkrawmaterialid=:rawid and RL.companyid=:companyid and RL.locationid=:locationid
             and CU.companyid=:companyid and CU.is_systemdefined=:is_systemdefined";

            $dao->initCommand($strSql);
            $dao->addParameter(':rawid',$rawid);
            $dao->addParameter(':is_systemdefined',1);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $invlist = $dao->executeRow();
            $result['reduceinventory']=$invlist['inventory'];
            $result['order_unit']=$invlist['shortcode'];

            $total_unit= $total_item_unit= $total_modi_unit = $total_modiitm_unit=$total_inv_unit=$total_wastage_unit=0;

            //Item Inventory

            $strItem = "SELECT sum(ROUND((REL.inventory),$round_off)) as inventory,CMI.itemname as item,
              sum(ROUND((REL.quantity),$round_off)) as itemcount,DATE_FORMAT(REL.createddatetime,'%d/%m/%Y')as date
              FROM ".CONFIG_DBN.". cforder_inventory_rel as REL
             INNER JOIN ".CONFIG_DBN.".cfmenu_items as CMI on CMI.itemunkid=REL.lnkitemid
             WHERE REL.lnkrawmaterialid=:rawid and REL.companyid=:companyid and REL.locationid=:locationid
             and REL.recipe_type=:recipe
             and CMI.companyid=:companyid and CMI.locationid=:locationid";

            if($fromdate!="" && $todate!=""){
                $strItem .= " AND DATE_FORMAT(REL.createddatetime,'%Y-%m-%d') BETWEEN '".$fromdate."' AND '".$todate."'";
            }

            $strItem .= "GROUP BY REL.lnkitemid,DATE_FORMAT(REL.createddatetime,'%Y/%m/%d')";
            $dao->initCommand($strItem);
            $dao->addParameter(':rawid',$rawid);
            $dao->addParameter(':recipe',1);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $itemlist = $dao->executeQuery();
            foreach ($itemlist as $val) {
                $total_unit += $val['inventory'];
                $total_item_unit += $val['inventory'];
            }

            //Modifier Inventory

            $strModi = "SELECT sum(ROUND((REL.inventory),$round_off)) as inventory,CMI.modifiername as item,
              sum(ROUND((REL.quantity),$round_off)) as itemcount,DATE_FORMAT(REL.createddatetime,'%d/%m/%Y')as date
              FROM ".CONFIG_DBN.". cforder_inventory_rel as REL
             INNER JOIN ".CONFIG_DBN.".cfmenu_modifiers as CMI on CMI.modifierunkid=REL.lnkitemid
             WHERE REL.lnkrawmaterialid=:rawid and REL.companyid=:companyid and REL.locationid=:locationid
             and REL.recipe_type=:recipe
             and CMI.companyid=:companyid and CMI.locationid=:locationid";

            if($fromdate!="" && $todate!=""){
                $strModi .= " AND DATE_FORMAT(REL.createddatetime,'%Y-%m-%d') BETWEEN '".$fromdate."' AND '".$todate."'";
            }

            $strModi .= "GROUP BY REL.lnkitemid,DATE_FORMAT(REL.createddatetime,'%Y/%m/%d')";
            $dao->initCommand($strModi);
            $dao->addParameter(':rawid',$rawid);
            $dao->addParameter(':recipe',2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $modilist = $dao->executeQuery();
            foreach ($modilist as $val) {
                $total_unit += $val['inventory'];
                $total_modi_unit += $val['inventory'];
            }

            //Modifier item inventory

            $strmoItem = "SELECT sum(ROUND((REL.inventory),$round_off)) as inventory,CMI.itemname as item,
              sum(ROUND((REL.quantity),$round_off)) as itemcount,DATE_FORMAT(REL.createddatetime,'%d/%m/%Y')as date
              FROM ".CONFIG_DBN.". cforder_inventory_rel as REL
             INNER JOIN ".CONFIG_DBN.".cfmenu_modifier_items as CMI on CMI.modifieritemunkid=REL.lnkitemid
             WHERE REL.lnkrawmaterialid=:rawid and REL.companyid=:companyid and REL.locationid=:locationid
             and REL.recipe_type=:recipe
             and CMI.companyid=:companyid and CMI.locationid=:locationid";

            if($fromdate!="" && $todate!=""){
                $strmoItem .= " AND DATE_FORMAT(REL.createddatetime,'%Y-%m-%d') BETWEEN '".$fromdate."' AND '".$todate."'";
            }

            $strmoItem .= "GROUP BY REL.lnkitemid,DATE_FORMAT(REL.createddatetime,'%Y/%m/%d')";
            $dao->initCommand($strmoItem);
            $dao->addParameter(':rawid',$rawid);
            $dao->addParameter(':recipe',3);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $moitemlist = $dao->executeQuery();
            foreach ($moitemlist as $val) {
                $total_unit += $val['inventory'];
                $total_modiitm_unit += $val['inventory'];
            }

            //Wastage inventory
            $strSql = "SELECT ROUND((CW.inventory/CU.unit),$round_off) as inventory,
                        CW.inventory as base_inventory,
                        CW.description,CU.shortcode ,
                        DATE_FORMAT(CW.createddatetime,'".$mysqlformat."') as date,
                        CU1.shortcode as base_code
                        FROM ".CONFIG_DBN.".cfwastage as CW
                        INNER JOIN ".CONFIG_DBN.".cfunit AS CU
                        ON CU.unitunkid=CW.inv_unitid
                        INNER JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS CL   
                        ON CW.lnkrawmaterialid=CL.lnkrawmaterialid AND CL.companyid=:companyid AND CL.locationid=:locationid
                        INNER JOIN ".CONFIG_DBN.".cfunit AS CU1
                        ON CL.inv_unitid=CU1.unitunkid 
                        WHERE CW.companyid=:companyid AND CW.locationid=:locationid 
                        AND CW.lnkrawmaterialid=:id AND CW.is_deleted=0";

            if($fromdate!="" && $todate!=""){
                $strSql .= " AND DATE_FORMAT(CW.createddatetime,'%Y-%m-%d') BETWEEN '".$fromdate."' AND '".$todate."'";
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->addParameter(':id',$rawid);
            $res = $dao->executeQuery();
            foreach ($res as $val) {
                $total_unit += $val['base_inventory'];
                $total_wastage_unit += $val['base_inventory'];
            }

            //Transfer inventory
            $invstatus=2;
            $strSql = "SELECT ROUND(((CI.inventory/CU.unit)*-1),$round_off) as inventory,
                        (CI.inventory*-1) as base_inventory,
                        IFNULL(LOC.locationname,'') as location,ROUND(CI.rate,$round_off)as rate,
                        CU.shortcode, DATE_FORMAT(CI.createddatetime,'".$mysqlformat."') as date,
                        CU1.shortcode as base_code
                        FROM ".CONFIG_DBN.".cfinventorydetail as CI                       
                        INNER JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS CL
                        ON CI.lnkrawmateriallocid=CL.id AND CL.companyid=:companyid AND CL.locationid=:locationid
                        INNER JOIN ".CONFIG_DBN.".cfunit AS CU
                        ON CU.unitunkid=CI.inv_unitid
                        INNER JOIN ".CONFIG_DBN.".cfrawmaterial AS CR
                        ON CI.lnkrawmaterialid=CR.id                       
                        LEFT JOIN ".CONFIG_DBN.".cflocation LOC
                        ON LOC.locationid=CI.rel_locationid   
                        INNER JOIN ".CONFIG_DBN.".cfunit AS CU1
                        ON CL.inv_unitid=CU1.unitunkid 
                        WHERE CI.companyid=:companyid AND CI.locationid =:locationid 
                        AND CI.lnkrawmaterialid=:id AND CI.inv_status=:invstatus AND CI.is_deleted=0";

            if($fromdate!="" && $todate!=""){
                $strSql .= " AND DATE_FORMAT(CI.createddatetime,'%Y-%m-%d') BETWEEN '".$fromdate."' AND '".$todate."'";
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->addparameter(':invstatus',$invstatus);
            $dao->addParameter(':id',$rawid);
            $resdata = $dao->executeQuery();
            foreach ($resdata as $val) {
                $total_unit+=$val['base_inventory'];
                $total_inv_unit+=$val['base_inventory'];
            }

            $result['item'] = $itemlist;
            $result['modifier'] = $modilist;
            $result['modifieritm'] = $moitemlist;
            $result['wastage']=$res;
            $result['transfer']=$resdata;
            $result['total_item_unit'] = number_format($total_item_unit,$round_off);
            $result['total_modi_unit'] = number_format($total_modi_unit,$round_off);
            $result['total_modiitm_unit'] = number_format($total_modiitm_unit,$round_off);
            $result['total_wastage_unit'] = number_format($total_wastage_unit,$round_off);
            $result['total_inv_unit'] = number_format($total_inv_unit,$round_off);
            $result['total_unit']=number_format($total_unit,$round_off);
            $result['frominventory']=number_format(($total_unit+$invlist['inventory']),$round_off);
            $result['fromdate'] = \util\util::convertMySqlDate($fromdate);
            $result['todate'] = \util\util::convertMySqlDate($todate);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            return $result;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getInventory - '.$e);
        }
    }
    public function getdailystatistics($data){
        try
        {
            $this->log->logIt($this->module.' - getdailystatistics');

            $dao = new \dao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $order_date = \util\util::convertDateToMySql($data['date']);
            $ObjCommonDao = new \database\commondao();
            $Companyinfo = self::loadcompanyinformation();
            $locationinfo = $ObjCommonDao->getLocationInfo();
            $strSql = "SELECT COUNT(DISTINCT FM.foliounkid) as total_order,FM.foliono".
                " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 1 OR FASM.mastertypeunkid = 8 THEN FD.quantity * FD.baseamount ELSE 0 END),".$round_off.") AS itemprice".
                " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid IN (9,10) THEN FD.quantity * FD.baseamount ELSE 0 END),".$round_off.") AS moditemprice".
                " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 3 THEN FD.quantity * FD.baseamount ELSE 0 END),".$round_off.") AS discamt".
                " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 2 THEN FD.quantity * FD.baseamount ELSE 0 END),".$round_off.") AS taxamt".
                " ,ROUND(IFNULL(SUM(FD.quantity * FD.baseamount * FASM.crdr),0),".$round_off.") AS finalamt".
                " ,ROUND(SUM(CASE WHEN FASM.mastertypeunkid = 8 AND FD.parentid = 0 THEN FD.quantity WHEN FASM.mastertypeunkid = 1 THEN FD.quantity ELSE 0 END)) AS itemsold".
                " FROM ".CONFIG_DBN.".fasfoliomaster AS FM ".
                " INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC ON TRC.contactunkid = FM.lnkcontactid AND TRC.companyid =:companyid AND TRC.locationid =:locationid ".
                "LEFT JOIN ".CONFIG_DBN.".fasfoliodetail AS FD ON FM.foliounkid = FD.foliounkid AND FM.companyid=FD.companyid AND FM.locationid=FD.locationid AND FD.isvoid = 0 AND FD.isrefund = 0".
                " LEFT JOIN ".CONFIG_DBN.".fasmaster AS FASM ON FD.masterunkid = FASM.masterunkid AND FD.companyid=FASM.companyid AND FD.locationid=FASM.locationid".
                " WHERE FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.isvalid = 1 AND FM.canceluserunkid = 0 AND TRC.contacttype != 4 AND CAST(FM.servedate AS DATE)='".$order_date."' AND FASM.mastertypeunkid NOT IN (4,7) AND FM.companyid=:companyid AND FM.locationid=:locationid GROUP BY FM.foliounkid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeQuery();
            $total_tax = $total_item_price = $total_mod_item_price = $total_discount = $total_order_amt = $total_items = $total_order =0;
            foreach($res AS $val){
                $total_item_price += $val['itemprice'];
                $total_mod_item_price += $val['moditemprice'];
                $total_tax += $val['taxamt'];
                $total_discount += $val['discamt'];
               // $total_order_amt += $val['finalamt'];
                $total_items += $val['itemsold'];
                $total_order += $val['total_order'];
                $total_order_amt += $val['itemprice'] + $val['moditemprice'] + $val['taxamt'] - $val['discamt'];
            }
            $result['data'] = $res;
            $result['total_order'] = $total_order;
            $result['items_sold'] = $total_items;
            $result['item_price'] = number_format($total_item_price,$round_off);
            $result['modifier_item_price'] = number_format($total_mod_item_price,$round_off);
            $result['item_tax'] = number_format($total_tax,$round_off);
            $result['item_discount'] = number_format($total_discount,$round_off);
            $result['order_amount'] = number_format($total_order_amt,$round_off);
            $result['report_date'] = \util\util::convertMySqlDate($order_date);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();
            $result['loc_name'] = $locationinfo['locationname'];
            $result['loc_email'] = $locationinfo['locationemail'];
            $result['loc_phone'] = $locationinfo['phone'];
            return $result;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getdailystatistics - '.$e);
        }

    }
    public function GetPaymentTypeReport($data,$languageArr='')
    {
        try {
            $this->log->logIt($this->module . ' - GetPaymentTypeReport');
            $dao = new \dao();

            $current_date = \util\util::getLocalDate();
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $from_date = \util\util::convertDateToMySql($data['fromdate']);
            $to_date = \util\util::convertDateToMySql($data['todate']);
            $ObjCommonDao = new \database\commondao();
            $Companyinfo = self::loadcompanyinformation();
            $locationinfo = $ObjCommonDao->getLocationInfo();
            $paymentdetailarr = array();
            $Final_Order_Amount = array();
            $GrandTotal=0;

            $str = "SELECT FASM.foliono,FASM.foliounkid,IFNULL(DATE_FORMAT(FASM.servedate,'".$mysqlformat."'),'') as opendate
                    FROM " . CONFIG_DBN . ".fasfoliomaster AS FASM          
            WHERE FASM.companyid=:companyid and FASM.locationid=:locationid AND FASM.isvalid = 1 AND FASM.canceluserunkid = 0 AND FASM.isvoid = 0 AND FASM.isfolioclosed = 1";

            if ($data['fromdate'] != "" && $data['todate'] != "") {
                $str .= " AND CAST(FASM.servedate AS DATE) BETWEEN '" . $from_date . "' AND '" . $to_date . "'";
            }

            $dao->initCommand($str);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $result1 = $dao->executeQuery();
            if ($result1) {
                foreach ($result1 as $key => $value) {
                    $strSql1 = "SELECT FASM.foliono,FASM.foliounkid,FSM.name,ABS(FASD.baseamount) as baseamount,FSM.mastertypeunkid
                                FROM " . CONFIG_DBN . ".fasfoliomaster AS FASM 
                                LEFT JOIN " . CONFIG_DBN . ". fasfoliodetail AS FASD ON FASM.foliounkid=FASD.foliounkid AND FASD.isvoid = 0 AND FASD.isrefund = 0 
                                LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FSM ON FASD.masterunkid=FSM.masterunkid 
                            LEFT JOIN " . CONFIG_DBN . ".cfpaymenttype AS PT ON FSM.masterunkid=PT.lnkmasterunkid
                            where FASM.foliounkid=:foliounkid AND FASM.companyid=:companyid AND FASM.locationid=:locationid 
                            AND FSM.mastertypeunkid IN (7,12,11)";
                    if($data['paymenttype'] != "" && $data['paymenttype']!=0)
                    {
                        $strSql1 .= " AND FSM.masterunkid = ".$data['paymenttype']." ";
                    }
                    if($data['payment'] != '0'){
                        $strSql1 .= " AND PT.type = '" . $data['payment'] . "'";
                    }
                    if ($data['fromdate'] != "" && $data['todate'] != "") {
                        $strSql1 .= " AND CAST(FASM.servedate AS DATE) BETWEEN '" . $from_date . "' AND '" . $to_date . "'";
                    }
                    $dao->initCommand($strSql1);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $dao->addparameter(':foliounkid', $value['foliounkid']);
                    $result2= $dao->executeQuery();

                    $gratuity = $tender = $adjustment = $payment_amount = $final_amount = 0;
                    foreach ($result2 as $key2 => $value2) {
                        if ($value2['mastertypeunkid'] == 7)
                            $payment_amount = $payment_amount + $value2['baseamount'];
                        if ($value2['mastertypeunkid'] == 11)
                            $gratuity = $gratuity + $value2['baseamount'];
                        if ($value2['mastertypeunkid'] == 12)
                            $tender = $tender + $value2['baseamount'];

                        $final_amount = $payment_amount - $gratuity - $tender;
                        $paymentdetailarr[$value['foliounkid']]['FolioName'] = $value['foliono'];
                        $paymentdetailarr[$value['foliounkid']]['Date'] = $value['opendate'];
                        $paymentdetailarr[$value['foliounkid']]['Final_Amount'] = $final_amount;
                        if($value2['name']=='Gratuity')
                            $paymentdetailarr[$value2['foliounkid']]['PaymenDetail'][$key2]['FolioNo'] = $languageArr->LANG5;
                        elseif($value2['name']=='Tender')
                            $paymentdetailarr[$value2['foliounkid']]['PaymenDetail'][$key2]['FolioNo'] = $languageArr->LANG6;
                        else
                            $paymentdetailarr[$value2['foliounkid']]['PaymenDetail'][$key2]['FolioNo'] = $value2['name'];
                        $paymentdetailarr[$value2['foliounkid']]['PaymenDetail'][$key2]['BaseAmount'] = number_format($value2['baseamount'], $round_off);
                        $Final_Order_Amount[$value2['foliounkid']]['Order_amount'] = $final_amount;

                    }
                }
            }
            $Final_Amount = array_column($Final_Order_Amount, 'Order_amount');
            $GrandTotal = array_sum($Final_Amount);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();
            $result['loc_name'] = $locationinfo['locationname'];
            $result['loc_email'] = $locationinfo['locationemail'];
            $result['loc_phone'] = $locationinfo['phone'];
            $result['company_logo'] = !empty($Companyinfo['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$Companyinfo['company_logo']:'';
            $result['Paymentdetail'] = $paymentdetailarr;
            $result['PaymentTotal'] = number_format($GrandTotal,$round_off);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - GetPaymentTypeReport - ' . $e);
        }
    }
    public function getPaymentTypes($data)
    {
        try
        {
            $this->log->logIt($this->module." - getPaymentTypeList");
            $dao = new \dao();
            $strSql = "SELECT lnkmasterunkid,paymenttype FROM ".CONFIG_DBN.".cfpaymenttype WHERE type=:type AND companyid=:companyid AND locationid=:locationid AND is_deleted=0";
            $dao->initCommand($strSql);
            $dao->addParameter(':type',$data['id']);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $resObj = $dao->executeQuery();
            return json_encode(array("Success"=>"True","Data"=>$resObj));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getPaymentTypeList - ".$e);
            return false;
        }
    }

    public function GetNoChargeReport($data,$languageArr)
    {
        try {
            $this->log->logIt($this->module . ' - GetNoChargeReport');
            $dao = new \dao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $form_date = $to_date='';
            if(isset($data['todate'])){
                $to_date = \util\util::convertDateToMySql($data['todate']);
            }
            if(isset($data['fromdate'])){
                $from_date = \util\util::convertDateToMySql($data['fromdate']);
            }
            $ObjCommonDao = new \database\commondao();
            $Companyinfo = self::loadcompanyinformation();
            $locationinfo = $ObjCommonDao->getLocationInfo();

            $dateformat=\database\parameter::getParameter('dateformat');
            $dateformat_dis=\common\staticarray::$mysqldateformat[$dateformat];

            $timeformat=\database\parameter::getParameter('timeformat');
            $timeformat_dis=\common\staticarray::$mysqltimeformat[$timeformat];

            $nochargeuserid = '';

            if (isset($data['nochargeuserid']) && $data['nochargeuserid'] != 0) {
                $nochargeuserid = $ObjCommonDao->getprimarykey('cfnocharge', $data['nochargeuserid'], 'nochargeunkid');
            }

            $strSql2 = "SELECT CF.nochargeunkid FROM " . CONFIG_DBN . ".cfnocharge AS CF ".
                " WHERE CF.companyid=:companyid AND CF.locationid=:locationid ";

            if ($nochargeuserid != '') {
                $strSql2 .= " AND CF.nochargeunkid='".$nochargeuserid."'";
            }

            $strSql2 .= "  ORDER BY CF.nochargeunkid ASC";

            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $resuserid = $dao->executeQuery();

            $strSql = "SELECT CFN.baseamount As FolioAmt,CFN.creditlimitamount As LmtAmt,CF.name As NochargeName,CF.nochargeunkid".
                " ,DATE_FORMAT(CFN.createddatetime,'".$dateformat_dis."') as FolioDate,".
                " CASE WHEN CF.credit_reset_type=1 THEN '".$languageArr->LANG10."'
                       WHEN CF.credit_reset_type=2 THEN '".$languageArr->LANG8."'
                       WHEN CF.credit_reset_type=3 THEN '".$languageArr->LANG9."'
                  ELSE ''
                      END As credit_reset_type ".
                " FROM " . CONFIG_DBN . ".fasfoliomaster AS FM ".
                " INNER JOIN " . CONFIG_DBN . ".cfnochargefolio AS CFN ON FM.foliounkid = CFN.foliolnkid AND FM.companyid=CFN.companyid AND FM.locationid=CFN.locationid ".
                " INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC ON TRC.contactunkid = FM.lnkcontactid AND FM.companyid=TRC.companyid AND FM.locationid=TRC.locationid ".
                " INNER JOIN " . CONFIG_DBN . ".cfnocharge AS CF ON TRC.contactpersonunkid = CF.nochargeunkid AND TRC.companyid=CF.companyid AND TRC.locationid=CF.locationid ".
                " WHERE FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.canceluserunkid = 0 AND FM.isvalid = 1 AND TRC.contacttype = 4 AND FM.companyid=:companyid AND FM.locationid=:locationid ";

            if ($from_date != "" && $to_date != "") {
                $strSql .= " AND CAST(FM.servedate AS DATE) BETWEEN '" . $from_date . "' AND '" . $to_date . "' ";
            }else if($from_date != "" && $to_date == "") {
                $strSql .= " AND CAST(FM.servedate AS DATE)='" . $from_date . "' ";
            }

            if ($nochargeuserid != '') {
                $strSql .= " AND CF.nochargeunkid ='".$nochargeuserid."' ";
            }

            $strSql .= "  ORDER BY CF.nochargeunkid ASC,CFN.nochargefoliounkid ASC";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeQuery();

            $folio = array();

            $FolioDate2 = $NochargeName2 = $credit_reset_type2 ='';

            $TotalFolioAmt = 0;
            $LmtAmt = $totalfolioamtuserwise = $LmtAmt2 = $cnt = 0;

            foreach ($resuserid AS $uid)
            {
                $credit_reset_type = $FolioDate = $NochargeName = '';
                $LmtAmt = 0;

                foreach ($res AS $val) {


                    if($val['nochargeunkid'] == $uid['nochargeunkid'])
                    {

                        if($val['FolioDate'] != $FolioDate)
                        {
                            $FolioDate = $val['FolioDate'];
                            $FolioDate2 = $val['FolioDate'];
                        }
                        else
                        {
                            $FolioDate2 = '';
                        }

                        if($val['NochargeName'] != $NochargeName)
                        {
                            $NochargeName = $val['NochargeName'];
                            $NochargeName2 = $val['NochargeName'];
                        }
                        else
                        {
                            $NochargeName2 = '';
                        }

                        if($val['LmtAmt'] != $LmtAmt)
                        {
                            $LmtAmt = $val['LmtAmt'];
                            $LmtAmt2 = $val['LmtAmt'];
                        }
                        else
                        {
                            $LmtAmt2 = '';
                        }
                        if($val['credit_reset_type'] != $credit_reset_type)
                        {
                            $credit_reset_type = $val['credit_reset_type'];
                            $credit_reset_type2 = $val['credit_reset_type'];
                        }
                        else
                        {
                            $credit_reset_type2 = '';
                        }

                        $folio[$cnt]['FolioDate'] = $FolioDate2;
                        $folio[$cnt]['NochargeName'] = $NochargeName2;
                        $folio[$cnt]['LmtAmt'] = $LmtAmt2;
                        $folio[$cnt]['FolioAmt'] = $val['FolioAmt'];
                        $folio[$cnt]['credit_reset_type'] = $credit_reset_type2;
                        $cnt++;
                    }

                }
            }

            foreach ($res AS $val) {
                if($val['nochargeunkid'])
                {
                    $TotalFolioAmt += $val['FolioAmt'];
                }
            }

            $result = array();
            $result['data'] = $folio;
            $result['total_folioamt'] = number_format($TotalFolioAmt, $round_off);
            $result['report_date'] = !empty($to_date)?\util\util::convertMySqlDate($from_date).' to '.\util\util::convertMySqlDate($to_date):\util\util::convertMySqlDate($form_date);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            $result['from_date'] = \util\util::convertMySqlDate($from_date);
            $result['to_date'] = \util\util::convertMySqlDate($to_date);
            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();
            $result['loc_name'] = $locationinfo['locationname'];
            $result['loc_email'] = $locationinfo['locationemail'];
            $result['loc_phone'] = $locationinfo['phone'];
            $result['company_logo'] = !empty($Companyinfo['company_logo']) ? CONFIG_BASE_URL . "companyadmin/assets/company_logo/" . $Companyinfo['company_logo'] : '';
            return $result;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . ' - GetNoChargeReport - ' . $e);
        }
    }

    public function GetCashDrawerReport($data)
    {
        try {
            $this->log->logIt($this->module . ' - GetCashDrawerReport');
            $dao = new \dao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $form_date = $to_date='';
            if(isset($data['todate'])){
                $to_date = \util\util::convertDateToMySql($data['todate']);
            }
            if(isset($data['fromdate'])){
                $from_date = \util\util::convertDateToMySql($data['fromdate']);
            }
            $ObjCommonDao = new \database\commondao();
            $Companyinfo = self::loadcompanyinformation();
            $locationinfo = $ObjCommonDao->getLocationInfo();

            $dateformat=\database\parameter::getParameter('dateformat');
            $dateformat_dis=\common\staticarray::$mysqldateformat[$dateformat];

            $timeformat=\database\parameter::getParameter('timeformat');
            $timeformat_dis=\common\staticarray::$mysqltimeformat[$timeformat];

            $strSql = "SELECT ROUND(IFNULL(US.closingbalance,0),".$round_off.") As closingbalance,ROUND(IFNULL(US.openingbalance,0),".$round_off.")  As openingbalance, IFNULL(US.bank_cash_balance,'') As bank_cash_balance,CFU.username,US.userid ".
                " ,DATE_FORMAT(US.timein,'".$dateformat_dis." ".$timeformat_dis."') as timein ".
                " ,DATE_FORMAT(US.timeout,'".$dateformat_dis." ".$timeformat_dis."') as timeout ".
                " FROM " . CONFIG_DBN . ".usershifts AS US ".
                " INNER JOIN " . CONFIG_DBN . ".cfuser AS CFU ON US.userid = CFU.userunkid AND CFU.companyid=:companyid".
                " WHERE US.companyid=:companyid AND US.locationid=:locationid";

            if ($from_date != "" && $to_date != "") {
                $strSql .= " AND CAST(US.timein AS DATE) BETWEEN '" . $from_date . "' AND '" . $to_date . "' ";
            }else if($from_date != "" && $to_date == "") {
                $strSql .= " AND CAST(US.timein AS DATE)='" . $from_date . "' ";
            }

            $strSql .= " ORDER BY US.timein ASC";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeQuery();

            $cnt = 0;
            foreach ($res as $re)
            {
                $total_bank_cash = 0 ;
                $bank_cash_balance = '';
                if($re['bank_cash_balance'] != '')
                {
                    $bank_cash_balance = json_decode($re['bank_cash_balance']);

                    if(count($bank_cash_balance) >0)
                    {
                        for ($i=0;$i<count($bank_cash_balance);$i++)
                        {

                            $total_bank_cash += $bank_cash_balance[$i]->Baseamt;
                            str_replace(',', '', $total_bank_cash);
                        }

                    }
                }
                $res[$cnt]['total_bank_cash'] = str_replace(',', '', number_format($total_bank_cash,$round_off));
                $cnt++;
            }

            $openingbalance = $closingbalance = $bankcashbalance = 0;

            foreach ($res AS $val) {
                if($val['openingbalance'])
                {
                    $openingbalance += $val['openingbalance'];
                }
                if($val['closingbalance'])
                {
                    $closingbalance += $val['closingbalance'];
                }
                if($val['total_bank_cash'])
                {
                    $bankcashbalance += $val['total_bank_cash'];
                }
            }

            $result = array();
            $result['data'] = $res;
            $result['total_openingbalance'] = number_format($openingbalance, $round_off);
            $result['total_closingbalance'] = number_format($closingbalance, $round_off);
            $result['total_bankcashbalance'] = number_format($bankcashbalance, $round_off);
            $result['report_date'] = !empty($to_date)?\util\util::convertMySqlDate($from_date).' to '.\util\util::convertMySqlDate($to_date):\util\util::convertMySqlDate($form_date);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            $result['from_date'] = \util\util::convertMySqlDate($from_date);
            $result['to_date'] = \util\util::convertMySqlDate($to_date);
            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();
            $result['loc_name'] = $locationinfo['locationname'];
            $result['loc_email'] = $locationinfo['locationemail'];
            $result['loc_phone'] = $locationinfo['phone'];
            $result['company_logo'] = !empty($Companyinfo['company_logo']) ? CONFIG_BASE_URL . "companyadmin/assets/company_logo/" . $Companyinfo['company_logo'] : '';
            return $result;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . ' - GetCashDrawerReport - ' . $e);
        }
    }

    public function GetGratuityReport($data,$languageArr)
    {
        try {
            $this->log->logIt($this->module . ' - GetGratuityReport');
            $dao = new \dao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $form_date = $to_date='';
            if(isset($data['todate'])){
                $to_date = \util\util::convertDateToMySql($data['todate']);
            }
            if(isset($data['fromdate'])){
                $from_date = \util\util::convertDateToMySql($data['fromdate']);
            }
            $ObjCommonDao = new \database\commondao();
            $Companyinfo = self::loadcompanyinformation();
            $locationinfo = $ObjCommonDao->getLocationInfo();

            $dateformat=\database\parameter::getParameter('dateformat');
            $dateformat_dis=\common\staticarray::$mysqldateformat[$dateformat];

//            $waiterid = '';
//
//            if (isset($data['waiterid']) && $data['waiterid'] != 0 && $data['waiterid'] != '') {
//                $waiterid = $ObjCommonDao->getprimarykey('waiter_mst', $data['waiterid'], 'z_waiterid_pk');
//            }

           /* $strSql2 = "SELECT WTR.z_waiterid_pk AS waiterid FROM " . CONFIG_DBN . ".waiter_mst AS WTR ".
                " WHERE WTR.companyid=:companyid AND WTR.locationid=:locationid ";*/

//            if ($waiterid != '') {
//                $strSql2 .= " AND WTR.z_waiterid_pk='".$waiterid."'";
//            }

            /*$strSql2 .= "  ORDER BY WTR.z_waiterid_pk ASC";

            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $resuserid = $dao->executeQuery();*/

            array_push($resuserid,array('waiterid'=>0));

            $strSql = "SELECT FM.foliono,".
                " DATE_FORMAT(FM.servedate,'".$dateformat_dis."') as FolioDate,IFNULL(FD.baseamount,0) AS tipamount,".
                " FROM " . CONFIG_DBN . ".fasfoliomaster AS FM ".
                " LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS FD ON FM.foliounkid = FD.foliounkid AND FM.companyid=FD.companyid AND FM.locationid=FD.locationid AND FD.isvoid = 0 AND FD.isrefund = 0".
                " LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM ON FASM.masterunkid = FD.masterunkid AND FD.companyid=FASM.companyid AND FD.locationid=FASM.locationid AND FASM.mastertypeunkid = 11".

                " WHERE FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.canceluserunkid = 0 AND FM.isvalid = 1 AND FM.companyid=:companyid AND FM.locationid=:locationid AND FASM.mastertypeunkid = 11";

            if ($from_date != "" && $to_date != "") {
                $strSql .= " AND CAST(FM.servedate AS DATE) BETWEEN '" . $from_date . "' AND '" . $to_date . "' ";
            }else if($from_date != "" && $to_date == "") {
                $strSql .= " AND CAST(FM.servedate AS DATE)='" . $from_date . "' ";
            }

//            if ($waiterid != '') {
//                $strSql .= " AND FM.lnkwaiterid ='".$waiterid."' ORDER BY WTR.z_waiterid_pk ASC ,FM.servedate ASC";
//            }
//            else
//            {
            $strSql .= " ORDER BY FM.servedate ASC";
            //}

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeQuery();

            $folio = array();

            $FolioDate2 = $waitername2 = '';

            $TotaltipAmt = 0;
            $cnt = 0;

            foreach ($resuserid AS $uid)
            {
                $FolioDate = $waitername = '';

                foreach ($res AS $val) {

                    if($val['waiterid'] == $uid['waiterid'])
                    {

                        if($val['FolioDate'] != $FolioDate)
                        {
                            $FolioDate = $val['FolioDate'];
                            $FolioDate2 = $val['FolioDate'];
                        }
                        else
                        {
                            $FolioDate2 = '';
                        }

//                        if($val['waitername'] != $waitername)
//                        {
//                            $waitername = $val['waitername'];
//                            $waitername2 = $val['waitername'];
//                        }
//                        else
//                        {
//                            $waitername2 = '';
//                        }


                        //$folio[$cnt]['waitername'] = $val['waitername'];
                        $folio[$cnt]['FolioDate'] = $FolioDate2;
                        $folio[$cnt]['foliono'] = $val['foliono'];
                        $folio[$cnt]['tipamount'] = $val['tipamount'];
                        $cnt++;
                    }

                }
            }

            foreach ($res AS $val) {

                if($val['tipamount'])
                {
                    $TotaltipAmt += $val['tipamount'];
                }
            }

            $result = array();
            $result['data'] = $folio;
            $result['total_tipamount'] = number_format($TotaltipAmt, $round_off);
            $result['report_date'] = !empty($to_date)?\util\util::convertMySqlDate($from_date).' to '.\util\util::convertMySqlDate($to_date):\util\util::convertMySqlDate($form_date);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            $result['from_date'] = \util\util::convertMySqlDate($from_date);
            $result['to_date'] = \util\util::convertMySqlDate($to_date);
            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();
            $result['loc_name'] = $locationinfo['locationname'];
            $result['loc_email'] = $locationinfo['locationemail'];
            $result['loc_phone'] = $locationinfo['phone'];
            $result['company_logo'] = !empty($Companyinfo['company_logo']) ? CONFIG_BASE_URL . "companyadmin/assets/company_logo/" . $Companyinfo['company_logo'] : '';
            return $result;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . ' - GetGratuityReport - ' . $e);
        }
    }

    public function GetComplementaryReport($data)
    {
        try {
            $this->log->logIt($this->module . ' - GetComplementaryReport');
            $dao = new \dao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $form_date = $to_date='';
            if(isset($data['todate'])){
                $to_date = \util\util::convertDateToMySql($data['todate']);
            }
            if(isset($data['fromdate'])){
                $from_date = \util\util::convertDateToMySql($data['fromdate']);
            }
            $ObjCommonDao = new \database\commondao();
            $Companyinfo = self::loadcompanyinformation();
            $locationinfo = $ObjCommonDao->getLocationInfo();

            $dateformat=\database\parameter::getParameter('dateformat');
            $dateformat_dis=\common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT DATE_FORMAT(FM.servedate,'".$dateformat_dis."') as FolioDate,IFNULL(FM.foliono,'') AS foliono,ROUND(SUM(CASE WHEN FASM1.mastertypeunkid IN (1,2,8,9,10) THEN FD1.quantity * FD1.orderamount * FASM1.crdr ELSE 0 END)," . $round_off . ") AS baseamount,".
                "ROUND(SUM(CASE WHEN FASM1.mastertypeunkid=8 THEN FD1.quantity ELSE 0 END)) AS totalitemqty,ROUND(SUM(CASE WHEN FASM1.mastertypeunkid IN(9,10) THEN FD1.quantity ELSE 0 END)) AS totalmodqty".
                " FROM " . CONFIG_DBN . ".fasfoliomaster AS FM LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS FD1 
                                    ON FM.foliounkid = FD1.foliounkid AND FD1.isvoid = 0 AND FD1.isrefund = 0 AND FD1.locationid=:locationid                                                   
                                    LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM1 
                                    ON FD1.masterunkid = FASM1.masterunkid  AND FASM1.locationid=:locationid                                     
                                    LEFT JOIN " . CONFIG_DBN . ".cfmenu_items AS CFI 
                                    ON FD1.lnkmappingunkid = CFI.itemunkid AND CFI.locationid=:locationid AND FASM1.mastertypeunkid = 8
                                    
                                    LEFT JOIN " . CONFIG_DBN . ".cfmenu_modifiers AS MO 
                                    ON FD1.lnkmappingunkid = MO.modifierunkid AND MO.locationid=:locationid AND FASM1.mastertypeunkid = 10
                                    
                                    LEFT JOIN " . CONFIG_DBN . ".cfmenu_modifier_items AS MODI 
                                    ON FD1.lnkmappingunkid = MODI.modifieritemunkid AND MODI.locationid=:locationid AND FASM1.mastertypeunkid = 9  
                                                      
                                    WHERE FM.companyid=:companyid AND FM.locationid=:locationid AND FD1.is_complimentary = 1 AND FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.canceluserunkid = 0 AND FM.isvalid = 1 ";

            if ($from_date != "" && $to_date != "") {
                $strSql .= " AND CAST(FM.servedate AS DATE) BETWEEN '" . $from_date . "' AND '" . $to_date . "' ";
            }else if($from_date != "" && $to_date == "") {
                $strSql .= " AND CAST(FM.servedate AS DATE)='" . $from_date . "' ";
            }


            $strSql .= " GROUP BY FM.foliounkid ORDER BY FM.foliounkid ASC,FM.servedate ASC";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeQuery();

            $folio = array();
            $cnt = 0;
            $FolioDate = '';

            foreach ($res AS $val) {


                if($val['FolioDate'] != $FolioDate)
                {
                    $FolioDate = $val['FolioDate'];
                    $FolioDate2 = $val['FolioDate'];
                }
                else
                {
                    $FolioDate2 = '';
                }

                $folio[$cnt]['FolioDate'] = $FolioDate2;
                $folio[$cnt]['foliono'] = $val['foliono'];
                $folio[$cnt]['baseamount'] = $val['baseamount'];
                $folio[$cnt]['totalitemqty'] = $val['totalitemqty'];
                $folio[$cnt]['totalmodqty'] = $val['totalmodqty'];
                $cnt++;
            }

            $TotalFolioAmt = $TotalItemQty = $TotalModQty = 0;
            foreach ($res AS $val) {
                if($val['baseamount'])
                {
                    $TotalFolioAmt += $val['baseamount'];
                    $TotalItemQty  += $val['totalitemqty'];
                    $TotalModQty   += $val['totalmodqty'];
                }
            }

            $result = array();
            $result['data'] = $folio;
            $result['total_folioamt'] = number_format($TotalFolioAmt, $round_off);
            $result['total_itemqty'] = number_format($TotalItemQty);
            $result['total_modqty'] = number_format($TotalModQty);
            $result['report_date'] = !empty($to_date)?\util\util::convertMySqlDate($from_date).' to '.\util\util::convertMySqlDate($to_date):\util\util::convertMySqlDate($form_date);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            $result['from_date'] = \util\util::convertMySqlDate($from_date);
            $result['to_date'] = \util\util::convertMySqlDate($to_date);
            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();
            $result['loc_name'] = $locationinfo['locationname'];
            $result['loc_email'] = $locationinfo['locationemail'];
            $result['loc_phone'] = $locationinfo['phone'];
            $result['company_logo'] = !empty($Companyinfo['company_logo']) ? CONFIG_BASE_URL . "companyadmin/assets/company_logo/" . $Companyinfo['company_logo'] : '';
            return $result;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . ' - GetComplementaryReport - ' . $e);
        }
    }

    public function GetWaiterSaleReport($data,$languageArr)
    {
        try {
            $this->log->logIt($this->module . ' - GetWaiterSaleReport');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $from_date = \util\util::convertDateToMySql($data['fromdate']);
            $to_date = \util\util::convertDateToMySql($data['todate']);
            $Companyinfo = self::loadcompanyinformation();
            $locationinfo = $ObjCommonDao->getLocationInfo();
            if (isset($data['waiterid'])) {
                $waiter_id = $ObjCommonDao->getprimarykey('waiter_mst', $data['waiterid'], 'z_waiterid_pk');
            }

            /*Get item price,mod item price,item tax,discount*/
            $strSql = "SELECT tbl.lnkwaiterid,tbl.waiter_name,tbl.itemname,SUM(tbl.quantity) as quantity,IFNULL(tbl.name,'') AS unitName,tbl.lnkmappingunkid,ROUND(SUM(tbl.itemprice)," . $round_off . ") AS itemprice,ROUND(SUM(tbl.moditemprice)," . $round_off . ") AS moditemprice,ROUND(SUM(tbl.itemdiscount)," . $round_off . ") AS itemdiscount,ROUND(SUM(tbl.itemtax)," . $round_off . ") AS itemtax " .
                " FROM (SELECT FM.lnkwaiterid,WM.name as waiter_name,CFI.itemname,FD1.quantity as quantity,CMU.name,CMU.unitunkid,FD1.detailunkid,FD1.lnkmappingunkid" .
                " ,ROUND((CASE WHEN FASM1.mastertypeunkid = 8 THEN FD1.quantity * FD1.baseamount * FASM1.crdr ELSE 0 END)," . $round_off . ") AS itemprice" .
                " ,ROUND(SUM(CASE WHEN FASM2.mastertypeunkid IN (9,10) THEN FD2.quantity * FD2.baseamount * FASM2.crdr ELSE 0 END)," . $round_off . ") AS moditemprice" .
                " ,ROUND(SUM(CASE WHEN FASM2.mastertypeunkid=3 THEN FD2.quantity * FD2.baseamount ELSE 0 END)," . $round_off . ") AS itemdiscount" .
                " ,ROUND(SUM(CASE WHEN FASM2.mastertypeunkid=2 THEN FD2.quantity * FD2.baseamount * FASM2.crdr ELSE 0 END)," . $round_off . ") AS itemtax" .
                " FROM " . CONFIG_DBN . ".fasfoliomaster AS FM".
                " INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC 
                                    ON TRC.contactunkid = FM.lnkcontactid AND TRC.locationid =:locationid ".
                "LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS FD1 
                                    ON FM.foliounkid = FD1.foliounkid AND FD1.isvoid = 0 AND FD1.isrefund = 0 AND FD1.locationid=:locationid AND FD1.parentid=0
                                INNER JOIN " . CONFIG_DBN . ".cfmenu_items AS CFI 
                                    ON FD1.lnkmappingunkid = CFI.itemunkid AND CFI.locationid=:locationid
                               
                                LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM1 
                                    ON FD1.masterunkid = FASM1.masterunkid  AND FASM1.locationid=:locationid AND FASM1.mastertypeunkid = 8    
                                LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS FD2 
                                    ON FM.foliounkid = FD2.foliounkid AND FD1.detailunkid=FD2.parentid AND FD2.isvoid = 0 AND FD2.isrefund = 0 AND FD2.locationid=:locationid
                                    LEFT JOIN ".CONFIG_DBN.".cfmenu_itemunit AS CMU
                                    ON CMU.unitunkid=FD1.lnkunitid and CMU.locationid=:locationid
                                LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM2 
                                    ON FD2.masterunkid = FASM2.masterunkid AND FASM2.locationid=:locationid 
                                LEFT JOIN " . CONFIG_DBN . ".waiter_mst AS WM 
                                    ON FM.lnkwaiterid = WM.z_waiterid_pk AND WM.locationid=:locationid
                                WHERE FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.isvalid = 1 AND FM.canceluserunkid = 0 AND TRC.contacttype != 4 AND FM.companyid=:companyid AND FM.locationid=:locationid AND FD1.companyid=:companyid AND FM.lnkwaiterid > 0 AND FM.lnkwaiterid IS NOT NULL ";


            if (isset($waiter_id) && $waiter_id != '') {
                $strSql .= " AND FM.lnkwaiterid='" . $waiter_id . "'";
            }

            if ($data['fromdate'] != "" && $data['todate'] != "") {
                $strSql .= " AND CAST(FM.servedate AS DATE) BETWEEN '" . $from_date . "' AND '" . $to_date . "'";
            }
            $strSql .= " GROUP BY FD1.detailunkid) AS tbl GROUP BY tbl.lnkmappingunkid,tbl.unitunkid,tbl.lnkwaiterid";

            $strSql .= " UNION ";
            $strSql .= " SELECT tbl.lnkwaiterid,tbl.waiter_name,tbl.itemname,SUM(tbl.quantity) as quantity,IFNULL(tbl.name,'') AS unitName,tbl.lnkmappingunkid,ROUND(SUM(tbl.itemprice)," . $round_off . ") AS itemprice,ROUND(SUM(tbl.moditemprice)," . $round_off . ") AS moditemprice,ROUND(SUM(tbl.itemdiscount)," . $round_off . ") AS itemdiscount,ROUND(SUM(tbl.itemtax)," . $round_off . ") AS itemtax " .
                " FROM (SELECT FM.lnkwaiterid,WM.name as waiter_name,FD1.quantity as quantity,CMU.name,CMU.unitunkid,FD1.detailunkid,FD1.lnkmappingunkid" .
                " ,CASE WHEN FASM1.mastertypeunkid=1 THEN CFCOMBO.comboname END AS itemname" .
                " ,ROUND((CASE WHEN FASM1.mastertypeunkid = 1 THEN FD1.quantity * FD1.baseamount * FASM1.crdr ELSE 0 END)," . $round_off . ") AS itemprice" .
                " ,ROUND(SUM(CASE WHEN FASM2.mastertypeunkid IN (9,10) THEN FD2.quantity * FD2.baseamount * FASM2.crdr ELSE 0 END)," . $round_off . ") AS moditemprice" .
                " ,ROUND(SUM(CASE WHEN FASM2.mastertypeunkid=3 THEN FD2.quantity * FD2.baseamount ELSE 0 END)," . $round_off . ") AS itemdiscount" .
                " ,ROUND(SUM(CASE WHEN FASM2.mastertypeunkid=2 THEN FD2.quantity * FD2.baseamount * FASM2.crdr ELSE 0 END)," . $round_off . ") AS itemtax" .
                " FROM " . CONFIG_DBN . ".fasfoliomaster AS FM".
                " INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC 
                                    ON TRC.contactunkid = FM.lnkcontactid AND TRC.locationid =:locationid ".
                "LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS FD1 
                                    ON FM.foliounkid = FD1.foliounkid AND FD1.isvoid = 0 AND FD1.isrefund = 0 AND FD1.locationid=:locationid AND FD1.parentid=0
                             
                                LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM1 
                                    ON FD1.masterunkid = FASM1.masterunkid  AND FASM1.locationid=:locationid AND FASM1.mastertypeunkid = 1 
                                    
                                INNER JOIN " . CONFIG_DBN . ".cfmenu_combo AS CFCOMBO 
                                    ON FD1.lnkmappingunkid = CFCOMBO.combounkid AND CFCOMBO.locationid=:locationid
                                        
                                LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS FD2 
                                    ON FM.foliounkid = FD2.foliounkid AND FD1.detailunkid=FD2.parentid AND FD2.isvoid = 0 AND FD2.isrefund = 0 AND FD2.locationid=:locationid
                                    LEFT JOIN ".CONFIG_DBN.".cfmenu_itemunit AS CMU
                                    ON CMU.unitunkid=FD1.lnkunitid and CMU.locationid=:locationid
                                LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM2 
                                    ON FD2.masterunkid = FASM2.masterunkid AND FASM2.locationid=:locationid AND FASM1.mastertypeunkid != 8 
                                LEFT JOIN " . CONFIG_DBN . ".waiter_mst AS WM 
                                    ON FM.lnkwaiterid = WM.z_waiterid_pk AND WM.locationid=:locationid
                                WHERE FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.isvalid = 1 AND FM.canceluserunkid = 0 AND TRC.contacttype != 4 AND FM.companyid=:companyid AND FM.locationid=:locationid AND FD1.companyid=:companyid AND FM.lnkwaiterid > 0 AND FM.lnkwaiterid IS NOT NULL ";

            if (isset($waiter_id) && $waiter_id != '') {
                $strSql .= " AND FM.lnkwaiterid='" . $waiter_id . "'";
            }

            if ($data['fromdate'] != "" && $data['todate'] != "") {
                $strSql .= " AND CAST(FM.servedate AS DATE) BETWEEN '" . $from_date . "' AND '" . $to_date . "'";
            }
            $strSql .= " GROUP BY FD1.detailunkid) AS tbl GROUP BY tbl.lnkmappingunkid,tbl.unitunkid,tbl.lnkwaiterid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeQuery();

            if($res){
                $res=json_decode(html_entity_decode(json_encode($res)),1);
            }

            /*Get item price,mod item price,item tax,discount*/
            /*Get modifier item tax*/
            $strSql1 = "SELECT FD1.lnkmappingunkid" .
                " ,ROUND(SUM(CASE WHEN FASM3.mastertypeunkid=2 THEN FD3.quantity * FD3.baseamount * FASM3.crdr ELSE 0 END)," . $round_off . ") AS moditemtax" .
                " FROM " . CONFIG_DBN . ".fasfoliomaster AS FM
                            INNER JOIN " . CONFIG_DBN . ".trcontact AS TRC 
                                ON TRC.contactunkid = FM.lnkcontactid AND TRC.locationid =:locationid
                            LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS FD1 
                                ON FM.foliounkid = FD1.foliounkid AND FD1.isvoid = 0 AND FD1.isrefund = 0 AND FD1.locationid=:locationid
                            INNER JOIN " . CONFIG_DBN . ".cfmenu_items AS CFI 
                                ON FD1.lnkmappingunkid = CFI.itemunkid AND CFI.locationid=:locationid                          
                            LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM1 
                                ON FD1.masterunkid = FASM1.masterunkid AND FASM1.locationid=:locationid
                            LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS FD2 
                                ON FM.foliounkid = FD2.foliounkid AND FD2.isvoid = 0 AND FD2.isrefund = 0 AND FD2.parentid=FD1.detailunkid AND FD2.locationid=:locationid
                            LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM2 
                                ON FD2.masterunkid = FASM2.masterunkid AND FASM2.locationid=:locationid
                            LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS FD3 
                                ON FM.foliounkid = FD3.foliounkid AND FD3.parentid=FD2.detailunkid AND FD3.locationid=:locationid
                            LEFT JOIN " . CONFIG_DBN . ".fasmaster AS FASM3 
                                ON FD3.masterunkid = FASM3.masterunkid AND FD3.parentid=FD2.detailunkid AND FASM3.locationid=:locationid
                            WHERE FM.isvoid = 0 AND FM.isfolioclosed = 1 AND FM.isvalid = 1 AND FM.canceluserunkid = 0 AND TRC.contacttype != 4 AND FM.companyid=:companyid AND FM.locationid=:locationid AND FD1.companyid=:companyid  AND FASM1.mastertypeunkid=8 AND FM.lnkwaiterid > 0 AND FM.lnkwaiterid IS NOT NULL";
            if (isset($waiter_id) && $waiter_id != "") {
                $strSql1 .= " AND FM.lnkwaiterid='" . $waiter_id . "'";
            }

            if ($data['fromdate'] != "" && $data['todate'] != "") {
                $strSql1 .= " AND CAST(FM.servedate AS DATE) BETWEEN '" . $from_date . "' AND '" . $to_date . "'";
            }
            $strSql1 .= " GROUP BY FD1.lnkmappingunkid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res1 = $dao->executeQuery();
            /*Get modifier item tax*/
            $mod_item_tax = \util\util::getkeyfieldarray($res1, 'lnkmappingunkid');
            $cnt = $total_tax = $total_item_price = $total_mod_item_price = $total_discount = $total_item_amt = 0;


            foreach ($res AS $val) {
                $val['moditemtax'] = (isset($mod_item_tax[$val['lnkmappingunkid']])) ? $mod_item_tax[$val['lnkmappingunkid']]['moditemtax'] : 0;
                $total_item_price += $val['itemprice'];
                $total_mod_item_price += $val['moditemprice'];
                $total_tax += $val['itemtax'] + $val['moditemtax'];
                $res[$cnt]['taxamt'] = $val['itemtax'] + $val['moditemtax'];
                $res[$cnt]['itemamt'] = $val['itemprice'] + $val['moditemprice'] + $val['itemtax'] + $val['moditemtax'] - $val['itemdiscount'];
                $total_discount += $val['itemdiscount'];
                $total_item_amt += $res[$cnt]['itemamt'];
                $cnt++;
            }
            $result['data'] = $res;
            $result['item_price'] = number_format($total_item_price, $round_off);
            $result['modifier_item_price'] = number_format($total_mod_item_price, $round_off);
            $result['item_tax'] = number_format($total_tax, $round_off);
            $result['item_discount'] = number_format($total_discount, $round_off);
            $result['order_amount'] = number_format($total_item_amt, $round_off);
            $result['current_date'] = \util\util::convertMySqlDate($current_date);
            $result['from_date'] = \util\util::convertMySqlDate($from_date);
            $result['to_date'] = \util\util::convertMySqlDate($to_date);
            $result['currency_symbol'] = $ObjCommonDao->getCurrencySign();
            $result['loc_name'] = $locationinfo['locationname'];
            $result['loc_email'] = $locationinfo['locationemail'];
            $result['loc_phone'] = $locationinfo['phone'];
            $result['company_logo'] = !empty($Companyinfo['company_logo']) ? CONFIG_BASE_URL . "companyadmin/assets/company_logo/" . $Companyinfo['company_logo'] : '';

            return $result;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . ' - GetWaiterSaleReport - ' . $e);
        }
    }
}
?>