<?php
namespace database;

class taxdao
{
    public $module = 'DB_taxdao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function addTax($data,$languageArr,$defaultlanguageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - addTax');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();

            $ObjAuditDao = new \database\auditlogdao();
            $datetime=\util\util::getLocalDateTime();
            $objutil = new \util\util;
            $getdate = $objutil->convertDateToMySql($data['appliesfrom']);
            $tablename =  "cftax";
            $ObjDependencyDao = new \database\dependencydao();
            $is_gst = (isset($data['is_gst'])) ? $data['is_gst'] : 0;

            $arr_log = array(
                'Short Name'=>$data['shortname'],
                'Tax Name'=>$data['tax'],
                'Applies From'=>$getdate,
                'Posting Type'=>($data['taxrule']==1)?'Percentage':'Flat',
                'Amount'=>$data['amount'],
                'Apply Tax'=>($data['applytax']==0)?'Before Discount':'After Discount',
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive',
            );
            $json_data = html_entity_decode(json_encode($arr_log));

            if($data['id']=='0' || $data['id']==0)
            {

             $strSql2 = "SELECT COUNT(taxunkid) as total_tax from ".CONFIG_DBN.".cftax
             WHERE companyid=:companyid  and is_deleted=0  ";

                if(CONFIG_LID !=0)
                {
                    $strSql2 .= " AND locationid=:locationid AND storeid=0";}
                else
                {
                    $strSql2 .= " AND locationid=0 AND storeid=:storeid";

                }
                $dao->initCommand($strSql2);
                if(CONFIG_LID !=0)
                {

                    $dao->addParameter(":locationid",CONFIG_LID);
                }
                else
                {
                    $dao->addParameter(":storeid",CONFIG_SID);
                }

                $dao->addParameter(":companyid",CONFIG_CID);

                $rec2 = $dao->executeRow();

                if($rec2['total_tax']>=4){
                    return html_entity_decode(json_encode(array("Success"=>"False","Message"=>$defaultlanguageArr->NO_TAX_CAN_BE_ADDED)));
                }
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"tax",$data['tax']);
                if($chk_name==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG24)));
                }

                $title = "Add Record";
                $hashkey = \util\util::gethash();
                $strSql = "INSERT INTO ".CONFIG_DBN.".cftax (companyid,locationid, shortcode, tax,is_gst,is_child,after_tax, fasmastertype, createddatetime, created_user, isactive, hashkey,storeid)
                            VALUE(:companyid,:locationid, :shortcode, :tax, :is_gst, :is_child,:after_tax,:fasmastertype, :createddatetime, :created_user, :isactive, :hashkey,:storeid)";
                $dao->initCommand($strSql);
                $dao->addParameter(':shortcode',$data['shortname']);
                $dao->addParameter(':tax',$data['tax']);
                $dao->addParameter(':is_gst',$is_gst);
                $dao->addParameter(':fasmastertype','tax');
                $dao->addParameter(':isactive',$data['rdo_status']);
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':created_user',CONFIG_UID);
                $dao->addParameter(':hashkey',$hashkey);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->addparameter(':storeid',CONFIG_SID);
                if(isset($data['aftertaxlist']) && count($data['aftertaxlist'])>0){
                    $dao->addParameter(':after_tax',isset($data['aftertaxlist'])?implode(',',$data['aftertaxlist']):'');
                    $dao->addParameter(':is_child',1);
                }else{
                    $dao->addParameter(':after_tax','');
                    $dao->addParameter(':is_child',0);
                }
                $dao->executeNonQuery();
                $taxid = $dao->getLastInsertedId();
                
                $strSql = "INSERT INTO ".CONFIG_DBN.".cftaxdetail (taxunkid, taxdate, amount, postingrule, slab, taxapplyafter, fiscal_tax,zomato_tax)
                            VALUE(:taxunkid, :taxdate, :amount, :postingrule, :slab, :taxapplyafter ,:fiscal_tax,:zomato_tax);";
                $dao->initCommand($strSql);
                $dao->addParameter(':taxunkid', $taxid);
                $dao->addParameter(':taxdate',$getdate);
                $dao->addParameter(':amount',$data['amount']);
                $dao->addParameter(':postingrule',$data['taxrule']);
                $dao->addParameter(':slab','');
                $dao->addParameter(':taxapplyafter',$data['applytax']);
                $dao->addparameter(':fiscal_tax',isset($data['fiscal_tax'])?$data['fiscal_tax']:'');
                $dao->addparameter(':zomato_tax',isset($data['zomato_tax'])?$data['zomato_tax']:'');
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_ADD_SUC)));
            }
            else
            {
                $id = $ObjCommonDao->getprimarykey('cftax',$data['id'],'taxunkid');
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"tax",$data['tax'],$id);
                if($chk_name==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG24)));
                }

                $title = "Edit Record";
                $strSql = "UPDATE ".CONFIG_DBN.".cftax SET shortcode=:shortcode, tax=:tax,is_gst=:is_gst,after_tax=:after_tax,is_child=:is_child,modifieddatetime=:modifieddatetime, modified_user=:modified_user WHERE hashkey=:hashkey AND companyid=:companyid ";

                if(defined('CONFIG_LID') && CONFIG_LID > 0)
				{
					$strSql .= " AND locationid=:locationid ";
				}
	
				if(defined('CONFIG_SID') && CONFIG_SID > 0)
				{
					$strSql .= " AND storeid=:storeid ";
				}

                $dao->initCommand($strSql);
                $dao->addParameter(':hashkey',$data['id']);
                $dao->addParameter(':shortcode',$data['shortname']);
                $dao->addParameter(':tax',$data['tax']);
                $dao->addParameter(':is_gst',$is_gst);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);

                if(isset($data['aftertaxlist']) && count($data['aftertaxlist'])>0){
                    $dao->addParameter(':after_tax',isset($data['aftertaxlist'])?implode(',',$data['aftertaxlist']):'');
                    $dao->addParameter(':is_child',1);
                }else{
                    $dao->addParameter(':after_tax','');
                    $dao->addParameter(':is_child',0);
                }

				if(defined('CONFIG_LID') && CONFIG_LID > 0)
				{
					$dao->addparameter(':locationid',CONFIG_LID);
				}
	
				if(defined('CONFIG_SID') && CONFIG_SID > 0)
				{
					$dao->addparameter(':storeid',CONFIG_SID);
				}
                $dao->executeNonQuery();

                $strSql = "INSERT INTO ".CONFIG_DBN.".cftaxdetail (taxunkid, taxdate, amount, postingrule, slab, taxapplyafter,fiscal_tax,zomato_tax)
                            VALUE(:taxunkid, :taxdate, :amount, :postingrule, :slab, :taxapplyafter, :fiscal_tax, :zomato_tax);";
                $dao->initCommand($strSql);
                $dao->addParameter(':taxunkid', $id);
                $dao->addParameter(':taxdate',$getdate);
                $dao->addParameter(':amount',$data['amount']);
                $dao->addParameter(':postingrule',$data['taxrule']);
                $dao->addParameter(':slab','');
                $dao->addParameter(':taxapplyafter',$data['applytax']);
                $dao->addParameter(':fiscal_tax',isset($data['fiscal_tax'])?$data['fiscal_tax']:'');
                $dao->addParameter(':zomato_tax',isset($data['zomato_tax'])?$data['zomato_tax']:'');
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_UP_SUC)));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addTax - '.$e);
        }
    }

    public function taxlist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - taxlist - '.$name);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = "SELECT CFT.*,IFNULL(CFU1.username,'') AS createduser,IFNULL(CFU2.username,'') AS modifieduser,
            IFNULL(DATE_FORMAT(CFT.createddatetime,'".$mysqlformat."'),'') as created_date,
            IFNULL(DATE_FORMAT(CFT.modifieddatetime,'".$mysqlformat."'),'') as modified_date FROM ".CONFIG_DBN.".cftax AS CFT 
            LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CFT.created_user=CFU1.userunkid AND CFT.companyid=CFU1.companyid 
            LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON CFT.modified_user=CFU2.userunkid AND CFT.modified_user=CFU2.userunkid AND CFT.companyid=CFU2.companyid
            WHERE CFT.companyid=:companyid  AND CFT.is_deleted=0 ";
            if($name!="")
                $strSql .= " AND tax LIKE '%".$name."%'";
	
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$strSql .= " AND CFT.locationid = :locationid ";
			}
            
            if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$strSql .= " AND CFT.storeid = :storeid ";
			}
            
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
           
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$dao->addparameter(':locationid',CONFIG_LID);
			}
	
			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$dao->addparameter(':storeid',CONFIG_SID);
			}
			
            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);          
            $dao->addParameter(':companyid',CONFIG_CID);
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$dao->addparameter(':locationid',CONFIG_LID);
			}
	
			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$dao->addparameter(':storeid',CONFIG_SID);
			}
            $rec = $dao->executeQuery();
            
            if(count($data) != 0){               
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue));
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - taxlist - '.$e);
        }
    }

    public function getTaxRec($data)
    {
		try
        {
			$this->log->logIt($this->module." - getTaxRec");
			$dao = new \dao();
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = " SELECT CFT.taxunkid,CFTD.taxdetailunkid,CFT.shortcode,CFT.tax,CFT.is_gst,CFT.isactive,CFT.after_tax,
            CFTD.taxdate,CFTD.postingrule,CFTD.amount,CFTD.taxapplyafter,CFTD.fiscal_tax,CFTD.zomato_tax,
            DATE_FORMAT(CFTD.taxdate,'".$mysqlformat."') as date 
                            FROM ".CONFIG_DBN.".cftax AS CFT  
                            INNER JOIN ".CONFIG_DBN.".cftaxdetail AS CFTD 
                            ON CFT.taxunkid=CFTD.taxunkid
                            WHERE CFT.hashkey=:hashkey AND CFT.companyid=:companyid ";
	
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$strSql .= " AND CFT.locationid = :locationid ";
			}
	
			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$strSql .= " AND CFT.storeid = :storeid ";
			}
            
            $strSql .=" ORDER BY CFTD.taxdetailunkid DESC LIMIT 1";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$dao->addparameter(':locationid',CONFIG_LID);
			}
	
			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$dao->addparameter(':storeid',CONFIG_SID);
			}
            
            $dao->addParameter(':hashkey',$data['id']);
            $res = $dao->executeRow();

            if($res){
                $strSql="SELECT taxunkid FROM ".CONFIG_DBN.".cftax WHERE FIND_IN_SET(".$res['taxunkid'].",after_tax) AND companyid=".CONFIG_CID;
                $dao->initCommand($strSql);
                $res1 = $dao->executeRow();
                if($res1){
                    $res['istaxavailable']=0;
                }else{
                    $res['istaxavailable']=1;
                }

            }
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getTaxRec - ".$e);
			return false; 
		}
	}
    
    public function getDetailsRec($data)
    {
        try
        {
            $this->log->logIt($this->module." - getDetailsRec");
            $dao = new \dao();
            $id=(isset($data['id']))?$data['id']:"";
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $timeformat = \database\parameter::getParameter('timeformat');
            $mysqltimeformat =  \common\staticarray::$mysqltimeformat[$timeformat];
            $strSql = " SELECT TD.*,T.hashkey AS taxdetailhashkey,DATE_FORMAT(taxdate,'".$mysqlformat."') AS tax_date,DATE_FORMAT(entrydatetime,'".$mysqlformat."') AS entry_date,DATE_FORMAT(entrydatetime,'".$mysqltimeformat."') AS entry_time,
                               IF(postingrule = '1', 'Percentage' , 'Flat') AS PT,
                               IF(taxapplyafter = '0', 'Before Discount' , 'After Discount') AS TA 
                        FROM ".CONFIG_DBN.".cftaxdetail AS TD
                        INNER JOIN ".CONFIG_DBN.".cftax AS T
                        ON TD.taxunkid=T.taxunkid
                        WHERE T.hashkey=:taxunkid AND T.companyid=:companyid ";
	
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$strSql .= " AND T.locationid = :locationid ";
			}
	
			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$strSql .= " AND T.storeid = :storeid ";
			}
            $dao->initCommand($strSql);
            $dao->addParameter(':taxunkid',$id);
            $dao->addParameter(':companyid',CONFIG_CID);
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$dao->addparameter(':locationid',CONFIG_LID);
			}
	
			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$dao->addparameter(':storeid',CONFIG_SID);
			}
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getDetailsRec - ".$e);
            return false; 
        }
    }

    public function getAppliedTax()
    {
        try
        {
            $this->log->logIt($this->module.' - getAppliedTax');
            $todaysdate = \util\util::getLocalDate();
            $dao = new \dao();


            /*( SELECT GROUP_CONCAT(TAX.tax) as tax_name from ".CONFIG_DBN.".cftax TAX where taxunkid IN (CFT.after_tax)) as taxlistname */

            $strSql = " SELECT CASE WHEN CFTD.postingrule=1 THEN '%' WHEN CFTD.postingrule=2
                       THEN 'FLAT' END as pytype,CFTD.amount,CFT.taxunkid,CFT.lnkmasterunkid,CFTD.taxdetailunkid,CFT.tax,CFTD.postingrule,CFTD.taxapplyafter,CFT.after_tax
                        FROM ".CONFIG_DBN.".cftax AS CFT
                        INNER JOIN ".CONFIG_DBN.".cftaxdetail CFTD 
                          ON CFT.taxunkid = CFTD.taxunkid AND 
                             CFTD.taxdetailunkid = (SELECT taxdetailunkid FROM ".CONFIG_DBN.".cftaxdetail WHERE taxunkid=CFT.taxunkid ORDER BY entrydatetime DESC LIMIT 1)
                        WHERE CFT.companyid =:companyid  AND CFT.isactive=1 AND CFT.is_deleted=0 AND CAST(CFTD.taxdate AS DATE)<=:todaysdate ";
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$strSql .= " AND CFT.locationid = :locationid ";
			}
			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$strSql .= " AND CFT.storeid = :storeid ";
			}
			
            $strSql .=" ORDER BY CFT.taxunkid";
            
            
            
            $dao->initCommand($strSql);
            $dao->addParameter(':todaysdate',$todaysdate);
            $dao->addParameter(':companyid',CONFIG_CID);
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$dao->addparameter(':locationid',CONFIG_LID);
			}
	
			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$dao->addparameter(':storeid',CONFIG_SID);
			}
            $res = $dao->executeQuery();
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getAppliedTax - '.$e);
        }
    }
    public function GetTaxCount()
    {
        try
        {
            $this->log->logIt($this->module.' - GetTaxCount');
            $dao = new \dao();
            $strSql2 = "SELECT COUNT(taxunkid) as total_tax from ".CONFIG_DBN.".cftax
             WHERE companyid=:companyid  and is_deleted=0  ";

            if(CONFIG_LID !=0)
            {
                $strSql2 .= " AND locationid=:locationid AND storeid=0";}
            else
            {
                $strSql2 .= " AND locationid=0 AND storeid=:storeid";

            }
            $dao->initCommand($strSql2);
            if(CONFIG_LID !=0)
            {

                $dao->addParameter(":locationid",CONFIG_LID);
            }
            else
            {
                $dao->addParameter(":storeid",CONFIG_SID);
            }

            $dao->addParameter(":companyid",CONFIG_CID);

            $rec2 = $dao->executeRow();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$rec2)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - GetTaxCount - '.$e);
        }
    }


    public function distaxlist()
    {
        try
        {
            $this->log->logIt($this->module." - distaxlist");
            $dao = new \dao();
            $strSql = " SELECT CFT.taxunkid,CFT.shortcode,CFT.tax,CFT.is_gst,CFT.isactive 
                            FROM ".CONFIG_DBN.".cftax AS CFT
                            WHERE  CFT.companyid=:companyid AND CFT.isactive=1 AND is_deleted=0 AND is_child=0";

            if(defined('CONFIG_LID') && CONFIG_LID > 0)
            {
                $strSql .= " AND CFT.locationid = :locationid ";
            }

            if(defined('CONFIG_SID') && CONFIG_SID > 0)
            {
                $strSql .= " AND CFT.storeid = :storeid ";
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            if(defined('CONFIG_LID') && CONFIG_LID > 0)
            {
                $dao->addparameter(':locationid',CONFIG_LID);
            }

            if(defined('CONFIG_SID') && CONFIG_SID > 0)
            {
                $dao->addparameter(':storeid',CONFIG_SID);
            }

            $res = $dao->executeQuery();

            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - distaxlist - ".$e);
            return false;
        }
    }
}

?>