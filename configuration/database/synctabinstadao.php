<?php
/**
 * Created by PhpStorm.
 * User: hemaxi
 * Date: 28/10/17
 * Time: 10:50 AM
 */
namespace database;

class synctabinstadao
{
    public $module = 'DB_synctabinstadao';
    public $log;

    function __construct()
    {

        $this->log = new \util\logger();
    }

    public function tabint()
    {
        $dao =new \dao();

        /*$str = "SELECT tabinsta_integration FROM ".CONFIG_DBN.".cfcompany WHERE companyid=:companyid";

        $dao->initCommand($str);
        $dao->addParameter(':companyid',CONFIG_CID);
        $data =$dao->executeRow();
        return $data;*/
    }



    public function updatediscountfromtabinsta($dataa, $modulename)
    {
        try {
            $this->log->logIt($this->module . '-updatediscountfromtabinsta-');
            $dao = new \dao();
            $ObjAuditDao = new \database\auditlogdao();
            if ($dataa['code'] == 200 && $dataa['status'] == 'ok') {
                $discount = $dataa['payload'];

                foreach ($discount AS $value) {

                    $hashkey = \util\util::gethash();
                    $arr_log = array(
                        'Short Name' => $value['title'],
                        'Discount' => $value['promocode'],
                        'Posting Type' => ($value['discounttype'] == 1) ? 'Percentage' : 'Flat',
                        'Open Discount' => 0,
                        'Value' => $value['discount'],
                        'Status' => ($value['status'] == 1) ? 'Active' : 'Inactive',
                    );
                    $json_data = json_encode($arr_log);

                    $str = "SELECT cfd.tabinstaid,cfd.hashkey FROM " . CONFIG_DBN . ".cfdiscount AS cfd WHERE cfd.tabinstaid=:tabinstaid AND cfd.is_deleted=0 AND cfd.companyid=:companyid AND cfd.locationid=:locationid";
                    $dao->initCommand($str);
                    $dao->addParameter(':tabinstaid', $value['promocode_id']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':locationid', CONFIG_LID);
                    $disid = $dao->executeRow();

                    if ($value['discounttype'] == 1) {
                        $postingrule = 1;
                    } else {
                        $postingrule = 2;
                    }

                    if (!$disid['tabinstaid'] == $value['promocode_id']) {
                        $title = "Sync from tabinsta(insert)";

                        $str = "INSERT INTO " . CONFIG_DBN . ".cfdiscount SET 
                                                                       shortcode=:shortcode,
                                                                       discount=:discount,
                                                                       fasmastertype=:fasmastertype,
                                                                       postingrule=:postingrule,
                                                                       value=:value,
                                                                       isopendiscount=:isopendiscount,
                                                                       createddatetime=:createddatetime,
                                                                       createduser=:createduser,
                                                                       isactive=:isactive,
                                                                       hashkey=:hashkey,
                                                                       companyid=:companyid,
                                                                       locationid=:locationid,
                                                                       tabinstaid=:tabinstaid";

                        $dao->initCommand($str);
                        $dao->addParameter(':shortcode', $value['title']);
                        $dao->addParameter(':discount', $value['promocode']);
                        $dao->addParameter(':fasmastertype', 'discount');
                        $dao->addParameter(':postingrule', $postingrule);
                        $dao->addParameter(':value', $value['discount']);
                        $dao->addParameter(':isopendiscount', 0);
                        $dao->addParameter(':createddatetime', $value['added_on']);
                        $dao->addParameter(':createduser', CONFIG_UID);
                        $dao->addParameter(':isactive', $value['status']);
                        $dao->addParameter(':hashkey', $hashkey);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':locationid', CONFIG_LID);
                        $dao->addParameter(':tabinstaid', $value['promocode_id']);
                        $dao->executeNonQuery();
                        $ObjAuditDao->addactivitylog($modulename, $title, $hashkey, $json_data);
                    } else {
                        $title = "Sync from tabinsta(update)";
                        $str = "UPDATE " . CONFIG_DBN . ".cfdiscount SET 
                                                               shortcode=:shortcode,
                                                               discount=:discount,
                                                               fasmastertype=:fasmastertype,
                                                               postingrule=:postingrule,
                                                               value=:value,
                                                               isopendiscount=:isopendiscount,
                                                               modifieddatetime=:modifieddatetime,
                                                               modified_user=:modified_user,
                                                               isactive=:isactive
                                                               WHERE tabinstaid=:tabinstaid	AND	companyid=:companyid AND locationid=:locationid";
                        $dao->initCommand($str);
                        $dao->addParameter(':shortcode', $value['title']);
                        $dao->addParameter(':discount', $value['promocode']);
                        $dao->addParameter(':fasmastertype', 'discount');
                        $dao->addParameter(':postingrule', $postingrule);
                        $dao->addParameter(':value', $value['discount']);
                        $dao->addParameter(':isopendiscount', 0);
                        $dao->addParameter(':modifieddatetime', $value['modified_on']);
                        $dao->addParameter(':modified_user', CONFIG_UID);
                        $dao->addParameter(':isactive', $value['status']);
                        $dao->addParameter(':tabinstaid', $value['promocode_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                        $ObjAuditDao->addactivitylog($modulename, $title, $disid['hashkey'], $json_data);
                    }
                }
                return html_entity_decode(json_encode(array("Success" => "True", "Message" => "Discount Sync Successfully")));
            }
            else
            {
                return html_entity_decode(json_encode(array("Success" => "False", "Message" => "Discount not Available")));
            }

        } catch (Exception $e) {
            $this->log->logIt($this->module . '-updatediscountfromtabinsta-' . $e);
        }
    }

    public function updatetaxfromtabinsta($dataa,$modulename)
    {
        try
        {
            $this->log->logIt($this->module.'-updatetaxfromtabinsta');
            $dao = new \dao();
            $datetime=\util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();

            if($dataa['code'] == 200 && $dataa['status'] == 'ok')
            {

                $data = $dataa['payload'];

                foreach ($data as $value) {


                    $arr_log = array(
                        'Short Name'=>$value['name'],
                        'Tax Name'=>$value['name'],
                        'Applies From'=>$value['added_on'],
                        'Posting Type'=>1,
                        'Amount'=>$value['rate'],
                        'Apply Tax'=>1
                    );
                    $json_data = json_encode($arr_log);

                    $str = "SELECT ct.tabinstaid as tabinstaid,ct.hashkey FROM " . CONFIG_DBN . ".cftax AS ct WHERE ct.tabinstaid=:taxid AND ct.is_deleted=0 AND ct.companyid=:companyid AND ct.locationid=:locationid";

                    $dao->initCommand($str);
                    $dao->addParameter(':taxid', $value['tax_id']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $restabid = $dao->executeRow();

                    if(!$restabid['tabinstaid'] == $value['tax_id'])
                    {
                        $title = "Sync from tabinsta(insert)";
                        $hashkey = \util\util::gethash();
                        $str = "INSERT INTO ".CONFIG_DBN.".cftax SET 
                                                            shortcode=:shortcode,
                                                            tax=:tax,
                                                            is_gst=:is_gst,
                                                            fasmastertype=:fasmastertype,
                                                            createddatetime=:createddatetime,
                                                            created_user=:created_user,
                                                            isactive=:isactive,
                                                            hashkey=:hashkey,
                                                            companyid=:companyid,
                                                            locationid=:locationid,
                                                            tabinstaid=:tabinstaid
                                                            ";
                        $dao->initCommand($str);
                        $dao->addParameter(':shortcode',$value['name']);
                        $dao->addParameter(':tax',$value['name']);
                        $dao->addParameter(':is_gst',$value['is_gst']);
                        $dao->addParameter(':fasmastertype','tax');
                        $dao->addParameter(':createddatetime',$datetime);
                        $dao->addParameter(':created_user',CONFIG_UID);
                        $dao->addParameter(':isactive',$value['is_active']);
                        $dao->addParameter(':hashkey',$hashkey);
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $dao->addParameter(':locationid',CONFIG_LID);
                        $dao->addParameter(':tabinstaid',$value['tax_id']);
                        $dao->executeNonQuery();
                        $taxpk = $dao->getLastInsertedId();

                        $str = "INSERT INTO ".CONFIG_DBN.".cftaxdetail SET 
                                                            taxunkid=:taxunkid,
                                                            taxdate=:taxdate,
                                                            amount=:amount,
                                                            postingrule=:postingrule,
                                                            slab=:slab,
                                                            taxapplyafter=:taxapplyafter
                                                            ";
                        $dao->initCommand($str);
                        $dao->addParameter(':taxunkid',$taxpk);
                        $dao->addParameter(':taxdate',$value['added_on']);
                        $dao->addParameter(':amount',$value['rate']);
                        $dao->addParameter(':postingrule',1);
                        $dao->addParameter(':slab','');
                        $dao->addParameter(':taxapplyafter',1);
                        $dao->executeNonQuery();
                        $ObjAuditDao->addactivitylog($modulename,$title,$hashkey,$json_data);
                    }
                    else
                    {

                        $title = "Sync from tabinsta(update)";
                        $str = "UPDATE ".CONFIG_DBN.".cftax AS ctx SET
                                                    shortcode=:shortcode,
                                                    tax=:tax,
                                                    is_gst=:is_gst,
                                                    isactive=:isactive,
                                                    modifieddatetime=:modifieddatetime, 
                                                    modified_user=:modified_user
                                                    WHERE ctx.tabinstaid=:tabinstaid AND ctx.companyid=:companyid AND ctx.locationid=:locationid";

                        $dao->initCommand($str);
                        $dao->addParameter(':shortcode',$value['name']);
                        $dao->addParameter(':tax',$value['name']);
                        $dao->addParameter(':is_gst',$value['is_gst']);
                        $dao->addParameter(':isactive',$value['is_active']);
                        $dao->addParameter(':modifieddatetime',$datetime);
                        $dao->addParameter(':modified_user',CONFIG_UID);
                        $dao->addParameter(':tabinstaid',$value['tax_id']);
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $dao->addParameter(':locationid',CONFIG_LID);
                        $dao->executeNonQuery();
                        $id = $ObjCommonDao->getprimarykey('cftax',$restabid['hashkey'],'taxunkid');


                        $str = "INSERT INTO ".CONFIG_DBN.".cftaxdetail SET 
                                                            taxunkid=:taxunkid,
                                                            taxdate=:taxdate,
                                                            amount=:amount,
                                                            postingrule=:postingrule,
                                                            slab=:slab,
                                                            taxapplyafter=:taxapplyafter
                                                            ";
                        $dao->initCommand($str);
                        $dao->addParameter(':taxunkid',$id);
                        $dao->addParameter(':taxdate',$value['added_on']);
                        $dao->addParameter(':amount',$value['rate']);
                        $dao->addParameter(':postingrule',1);
                        $dao->addParameter(':slab','');
                        $dao->addParameter(':taxapplyafter',1);
                        $dao->executeNonQuery();
                        $ObjAuditDao->addactivitylog($modulename,$title,$restabid['hashkey'],$json_data);
                    }
                }

                return json_encode(array('Success' => 'True','Message'=>'Tax Sync Successfully'));
            }
            else
            {
                return json_encode(array('Success' => 'False','Message'=>'Tax not available'));
            }
        }
        catch(\Exception $e)
        {
            $this->log->logIt($this->module.'-updatetaxfromtabinsta -'.$e);
        }
    }

    public function updatemenufromtabinsta($dataa,$modulename)
    {
        try {
            $this->log->logIt($this->module . '-updatemenufromtabinsta');


            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();
            if ($dataa['code'] == 200 && $dataa['status'] == 'ok') {
                $list = $dataa['payload'];

                 if($list =="")
                 {
                     return  json_encode(array("Success"=>"False","Message"=>"Menu not available"));
                 }
                 else
                 {
                foreach ($list AS $key => $value) {


                    $arr_log = array(
                        'Menu Name' => $value['menu_name'],
                        'Description' => $value['menu_description'],
                        'Status' => ($value['is_active'] == 1) ? 'Active' : 'Inactive',
                    );
                    $json_data = json_encode($arr_log);

                    if ($value['menu_image'] > 0) {

                        $ig = $value['menu_image'];
                        $first = reset($ig);
                        $nam = basename($first);
                        $destdir = 'assets/menu_images/';
                        $img = file_get_contents($first);
                        file_put_contents($destdir . substr($first, strrpos($first, '/')), $img);
                    } else {
                        $nam = '';
                    }

                    $str = "SELECT cm.tabinstaid as tabinstaid,cm.hashkey FROM " . CONFIG_DBN . ".cfmenu AS cm WHERE cm.tabinstaid=:menu_id AND cm.is_deleted=0 AND cm.companyid=:companyid AND cm.locationid=:locationid";

                    $dao->initCommand($str);
                    $dao->addParameter(':menu_id', $value['menu_id']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $res = $dao->executeRow();


                    if (!$res['tabinstaid'] == $value['menu_id']) {
                        $title = "Sync from tabinsta(insert)";
                        $hashkey = \util\util::gethash();

                        $str = "INSERT INTO " . CONFIG_DBN . ".cfmenu SET name=:menuname,
                                                                  long_desc=:long_desc,
                                                                   is_active=:is_active,
                                                                  image=:image,
                                                                  tabinstaid=:tabinstaid,
                                                                  hashkey=:hashkey,
                                                                  createddatetime=:createddatetime,
                                                                  created_user=:created_user,
                                                                  companyid=:companyid,
                                                                  locationid=:locationid";
                        $dao->initCommand($str);
                        $dao->addParameter(':menuname', $value['menu_name']);
                        $dao->addParameter(':long_desc', $value['menu_description']);
                        $dao->addParameter(':is_active', $value['is_active']);
                        $dao->addParameter(':image', $nam);
                        $dao->addParameter(':tabinstaid', $value['menu_id']);
                        $dao->addParameter(':hashkey', $hashkey);
                        $dao->addParameter(':createddatetime', $datetime);
                        $dao->addParameter(':created_user', CONFIG_UID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                        $ObjAuditDao->addactivitylog($modulename, $title, $hashkey, $json_data);

                    } else {
                        $title = "Sync from tabinsta(update)";
                        $str = "UPDATE " . CONFIG_DBN . ".cfmenu AS cm SET cm.name=:menuname,
                                                         cm.long_desc=:long_desc, 
                                                         cm.image=:image, 
                                                         cm.is_active=:is_active,
                                                         cm.tabinstaid=:tabinstaid,
                                                         cm.modifieddatetime=:modifieddatetime,
                                                         cm.modified_user=:modified_user
                                                         WHERE cm.tabinstaid=:tabinstaid AND cm.companyid=:companyid AND cm.locationid=:locationid";
                        $dao->initCommand($str);
                        $dao->addParameter(':menuname', $value['menu_name']);
                        $dao->addParameter(':long_desc', $value['menu_description']);
                        $dao->addParameter(':image', $nam);
                        $dao->addParameter(':is_active', $value['is_active']);
                        $dao->addParameter(':tabinstaid', $value['menu_id']);
                        $dao->addParameter(':modifieddatetime', $datetime);
                        $dao->addParameter(':modified_user', CONFIG_UID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                        $ObjAuditDao->addactivitylog($modulename, $title, $res['hashkey'], $json_data);
                    }
                }
                return json_encode(array('Success' => 'True', 'Message' => 'Menu Sync successfully'));
            }
        }
            else
            {
                return json_encode(array('Success'=>'False','Message'=>'Something Wrong'));
            }

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-updatemenufromtabinsta-'.$e);
        }
    }


    public function updatecategoryfromtabinsta($dataa,$modulename)
    {
        try
        {
            $this->log->logIt($this->module.'-updatecategoryfromtabinsta');
            $datetime=\util\util::getLocalDateTime();
            $dao = new \dao();
            $ObjAuditDao = new \database\auditlogdao();
            if($dataa['code'] == 200  && $dataa['status'] == 'ok') {
                $data = $dataa['payload'];

                if ($data == "") {
                    return json_encode(array("Success" => "False", "Message" => "Category not available"));
                }
                else
                {
                    foreach ($data as $key => $menuid) {

                    $arr_log = array(
                        'Category Name' => $menuid['name'],
                        'SKU' => $menuid['sku'],
                        'Status' => ($menuid['is_active'] == 1) ? 'Active' : 'Inactive'
                    );
                    $json_data = json_encode($arr_log);

                    $menu = $menuid['menu_id'];

                    if ($menuid['image_url'] > 0) {
                        $ig = $menuid['image_url'];
                        $first = reset($ig);
                        $nam = basename($first);
                        $destdir = 'assets/menu_category_images/';
                        $img = file_get_contents($first);
                        file_put_contents($destdir . substr($first, strrpos($first, '/')), $img);
                    } else {
                        $nam = '';
                    }

                    $str = "SELECT cm.menuunkid AS menuid FROM " . CONFIG_DBN . ".cfmenu AS cm WHERE cm.tabinstaid=:tabinstaid AND cm.is_deleted=0  AND cm.companyid=:companyid AND cm.locationid=:locationid";

                    $dao->initCommand($str);
                    $dao->addParameter(':tabinstaid', $menu);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':locationid', CONFIG_LID);
                    $resmenuid = $dao->executeRow();

                    if (!$resmenuid['menuid']) {
                        return json_encode(array("Success" => "False", "Message" => "Something Wrong"));
                    } else {


                        $str = "SELECT cmc.tabinstaid as tabinstaid,cmc.hashkey FROM " . CONFIG_DBN . ".cfmenu_categories AS cmc WHERE cmc.tabinstaid=:category_id AND cmc.is_deleted=0 AND cmc.companyid=:companyid AND cmc.locationid=:locationid";

                        $dao->initCommand($str);
                        $dao->addParameter(':category_id', $menuid['category_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $restabid = $dao->executeRow();


                        if (!$restabid['tabinstaid'] == $menuid['category_id']) {
                            $title = "Sync from tabinsta(insert)";
                            $hashkey = \util\util::gethash();

                            $str = "INSERT INTO " . CONFIG_DBN . ".cfmenu_categories SET menuunkid=:menuunkid,
                                                                          categoryname=:categoryname,
                                                                          image=:image,
                                                                          sku=:sku,
                                                                          is_active=:is_active,
                                                                          createddatetime=:createddatetime,
                                                                          created_user=:created_user,
                                                                          hashkey=:hashkey,
                                                                          companyid=:companyid,
                                                                          locationid=:locationid,
                                                                          tabinstaid=:tabinstaid
                                                                          ";
                            $dao->initCommand($str);
                            $dao->addParameter(':menuunkid', $resmenuid['menuid']);
                            $dao->addParameter(':categoryname', $menuid['name']);
                            $dao->addParameter(':image', $nam);
                            $dao->addParameter(':sku', $menuid['sku']);
                            $dao->addParameter(':is_active', $menuid['is_active']);
                            $dao->addParameter(':createddatetime', $datetime);
                            $dao->addParameter(':created_user', CONFIG_UID);
                            $dao->addParameter(':hashkey', $hashkey);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $dao->addParameter(':tabinstaid', $menuid['category_id']);
                            $dao->executeNonQuery();
                            $ObjAuditDao->addactivitylog($modulename, $title, $hashkey, $json_data);
                        } else {
                            $title = "Sync from tabinsta(update)";
                            $str = "UPDATE " . CONFIG_DBN . ".cfmenu_categories SET menuunkid=:menuunkid,
                                                                           categoryname=:categoryname,
                                                                           image=:image,
                                                                           sku=:sku,
                                                                           is_active=:is_active,
                                                                           modifieddatetime=:modifieddatetime, 
                                                                           modified_user=:modified_user
                                                                           WHERE tabinstaid=:tabinstaid AND companyid=:companyid AND locationid=:locationid";
                            $dao->initCommand($str);
                            $dao->addParameter(':menuunkid', $resmenuid['menuid']);
                            $dao->addParameter(':categoryname', $menuid['name']);
                            $dao->addParameter(':image', $nam);
                            $dao->addParameter(':sku', $menuid['sku']);
                            $dao->addParameter(':is_active', $menuid['is_active']);
                            $dao->addParameter(':modifieddatetime', $datetime);
                            $dao->addParameter(':modified_user', CONFIG_UID);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $dao->addParameter(':tabinstaid', $menuid['category_id']);
                            $dao->executeNonQuery();
                            $ObjAuditDao->addactivitylog($modulename, $title, $restabid['hashkey'], $json_data);
                        }
                    }

                }
            }
                return json_encode(array('Success' => 'True', 'Message' => 'Category Sync Successfully'));

            }
            else
            {
                return json_encode(array('Success'=>'False','Message'=>'Category not available'));
            }

        }
        catch(\Exception $e)
        {
            $this->log->logIt($this->module.'-updatecategoryfromtabinsta-'.$e);
        }
    }

    public function updatemodifierfromtabinsta($dataa,$modulename)
    {
        try
        {
            $this->log->logIt($this->module.'-updatemodifierfromtabinsta -');
            $dao = new \dao();
            $datetime=\util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();
            if($dataa['code'] == 200 && $dataa['status'] == 'ok')
            {
                $list = $dataa['payload'];

                foreach ($list AS $value) {

                    $arr_log = array(
                        'Modifier Name'=>$value['name'],
                        'Long Description'=>$value['description'],
                        'Min'=>1,
                        'Max'=>100,
                        'Status'=>$value['is_active'],
                    );
                    $json_data = json_encode($arr_log);


                    if ($value['image_url'] > 0) {
                        $ig = $value['image_url'];
                        $first = reset($ig);
                        $nam = basename($first);
                        $destdir = 'assets/menu_modifier_images/';
                        $img=file_get_contents($first);
                        file_put_contents($destdir.substr($first, strrpos($first,'/')), $img);
                    }
                    else
                    {
                        $nam ='';
                    }
                    $str = "SELECT cmm.tabinstaid as tabinstaid,cmm.hashkey FROM " . CONFIG_DBN . ".cfmenu_modifiers AS cmm WHERE cmm.tabinstaid=:modifier_id AND cmm.is_deleted=0 AND cmm.companyid=:companyid AND cmm.locationid=:locationid";

                    $dao->initCommand($str);
                    $dao->addParameter(':modifier_id',$value['modifier_id']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $res = $dao->executeRow();

                    if (!$res['tabinstaid'] == $value['modifier_id']) {
                        $title = "Sync from tabinsta(insert)";
                        $hashkey = \util\util::gethash();

                        $str = "INSERT INTO " . CONFIG_DBN . ".cfmenu_modifiers SET modifiername=:modifiername,
                                                                  long_desc=:long_desc,
                                                                  image=:image,
                                                                  sku=:sku,
                                                                  min=:min,
                                                                  max=:max,
                                                                  tabinstaid=:tabinstaid,
                                                                  hashkey=:hashkey,
                                                                  is_active=:is_active,
                                                                  createddatetime=:createddatetime,
                                                                  created_user=:created_user,
                                                                  companyid=:companyid,
                                                                  locationid=:locationid";
                        $dao->initCommand($str);
                        $dao->addParameter(':modifiername', $value['name']);
                        $dao->addParameter(':long_desc', $value['description']);
                        $dao->addParameter(':image', $nam);
                        $dao->addParameter(':sku',$value['sku']);
                        $dao->addParameter(':min',1);
                        $dao->addParameter(':max',100);
                        $dao->addParameter(':tabinstaid', $value['modifier_id']);
                        $dao->addParameter(':hashkey', $hashkey);
                        $dao->addParameter(':is_active',$value['is_active']);
                        $dao->addParameter(':createddatetime',$datetime);
                        $dao->addParameter(':created_user',CONFIG_UID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                        $ObjAuditDao->addactivitylog($modulename,$title,$hashkey,$json_data);
                    } else {
                        $title = "Sync from tabinsta(update)";
                        $str = "UPDATE " . CONFIG_DBN . ".cfmenu_modifiers AS cmm SET cmm.modifiername=:modifiername,
                                                         cmm.long_desc=:long_desc, 
                                                         cmm.image=:image, 
                                                         cmm.sku=:sku,
                                                         cmm.min=:min,
                                                         cmm.max=:max,
                                                         cmm.tabinstaid=:tabinstaid,
                                                         cmm.is_active=:is_active,
                                                         cmm.modifieddatetime=:modifieddatetime,
                                                         cmm.modified_user=:modified_user
                                                         WHERE cmm.tabinstaid=:tabinstaid AND cmm.companyid=:companyid AND cmm.locationid=:locationid";
                        $dao->initCommand($str);
                        $dao->addParameter(':modifiername', $value['name']);
                        $dao->addParameter(':long_desc', $value['description']);
                        $dao->addParameter(':image', $nam);
                        $dao->addParameter(':sku',$value['sku']);
                        $dao->addParameter(':min',1);
                        $dao->addParameter(':max',100);
                        $dao->addParameter(':tabinstaid', $value['modifier_id']);
                        $dao->addParameter(':is_active',$value['is_active']);
                        $dao->addParameter(':modifieddatetime',$datetime);
                        $dao->addParameter(':modified_user',CONFIG_UID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                        $ObjAuditDao->addactivitylog($modulename,$title,$res['hashkey'],$json_data);
                    }
                }
                return json_encode(array('Success'=>'True','Message'=>'Modifier Sync successfully'));
            }
            else
            {
                return json_encode(array('Success'=>'False','Message'=>'Modifiet not available'));

            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-updatemodifierfromtabinsta -'.$e);
        }
    }


    public function updatemodifieritemfromtabinsta($dataa,$modulename)
    {
        try
        {
            $this->log->logIt($this->module.'-updatemodifieritemfromtabinsta');
            $ObjAuditDao = new \database\auditlogdao();
            $datetime=\util\util::getLocalDateTime();
            $dao = new \dao();
            if($dataa['code'] == 200  && $dataa['status'] == 'ok')
            {
                $data = $dataa['payload'];

                foreach ($data as $modifierid)
                {
                    $modifier = $modifierid['modifier_id'];

                    $arr_log = array(
                        'Item Name'=>$modifierid['name'],
                        'Sale Amount'=>0,
                        'Status'=>$modifierid['is_active'],
                    );
                    $json_data = json_encode($arr_log);


                    if($modifierid['image_url'] > 0)
                    {
                        $ig = $modifierid['image_url'];
                        $first = reset($ig);
                        $nam = basename($first);
                        $destdir = 'assets/modifier_item_images/';
                        $img=file_get_contents($first);
                        file_put_contents($destdir.substr($first, strrpos($first,'/')), $img);
                    }
                    else
                    {
                        $nam = '';
                    }

                    $str = "SELECT IFNULL(GROUP_CONCAT(cmm.modifierunkid),'') AS modifierid FROM ".CONFIG_DBN.".cfmenu_modifiers AS cmm WHERE cmm.tabinstaid=:tabinstaid AND cmm.is_deleted=0 AND cmm.companyid=:companyid AND cmm.locationid=:locationid";

                    $dao->initCommand($str);
                    $dao->addParameter(':tabinstaid',$modifier);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addParameter(':locationid',CONFIG_LID);
                    $resmodifierid= $dao->executeRow();


                    if(!$resmodifierid['modifierid'])
                    {
                        return json_encode(array("Success"=>"False","Message"=>"Something Wrong"));

                    }
                    else {


                        $str = "SELECT cmmi.tabinstaid as tabinstaid,cmmi.hashkey FROM " . CONFIG_DBN . ".cfmenu_modifier_items AS cmmi 
                    WHERE cmmi.tabinstaid=:modifieritemid AND cmmi.is_deleted=0 AND cmmi.companyid=:companyid AND cmmi.locationid=:locationid";

                        $dao->initCommand($str);
                        $dao->addParameter(':modifieritemid', $modifierid['modifieritem_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $restabid = $dao->executeRow();

                        if (!$restabid['tabinstaid'] == $modifierid['modifieritem_id']) {

                            $title = "Sync from tabinsta(insert)";
                            $hashkey = \util\util::gethash();

                            $str = "INSERT INTO " . CONFIG_DBN . ".cfmenu_modifier_items SET modifierunkid=:modifierunkid,
                                                                          itemname=:itemname,
                                                                          image=:image,
                                                                          is_active=:is_active,
                                                                          sale_amount=:sale_amount,
                                                                          createddatetime=:createddatetime,
                                                                          created_user=:created_user,
                                                                          hashkey=:hashkey,
                                                                          companyid=:companyid,
                                                                          locationid=:locationid,
                                                                          tabinstaid=:tabinstaid
                                                                          ";
                            $dao->initCommand($str);
                            $dao->addParameter(':modifierunkid', $resmodifierid['modifierid']);
                            $dao->addParameter(':itemname', $modifierid['name']);
                            $dao->addParameter(':image', $nam);
                            $dao->addParameter(':is_active', $modifierid['is_active']);
                            $dao->addParameter(':sale_amount', 0);
                            $dao->addParameter(':createddatetime', $datetime);
                            $dao->addParameter(':created_user', CONFIG_UID);
                            $dao->addParameter(':hashkey', $hashkey);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $dao->addParameter(':tabinstaid', $modifierid['modifieritem_id']);
                            $dao->executeNonQuery();
                            $ObjAuditDao->addactivitylog($modulename, $title, $hashkey, $json_data);
                        } else {
                            $title = "Sync from tabinsta(update)";
                            $str = "UPDATE " . CONFIG_DBN . ".cfmenu_modifier_items SET modifierunkid=:modifierunkid,
                                                                           itemname=:itemname,
                                                                           image=:image,
                                                                           is_active=:is_active,
                                                                           sale_amount=:sale_amount,
                                                                           modifieddatetime=:modifieddatetime, 
                                                                           modified_user=:modified_user
                                                                           WHERE tabinstaid=:tabinstaid AND companyid=:companyid AND locationid=:locationid";
                            $dao->initCommand($str);
                            $dao->addParameter(':modifierunkid', $resmodifierid['modifierid']);
                            $dao->addParameter(':itemname', $modifierid['name']);
                            $dao->addParameter(':image', $nam);
                            $dao->addParameter(':is_active', $modifierid['is_active']);
                            $dao->addParameter(':sale_amount', 0);
                            $dao->addParameter(':modifieddatetime', $datetime);
                            $dao->addParameter(':modified_user', CONFIG_UID);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $dao->addParameter(':tabinstaid', $modifierid['modifieritem_id']);
                            $dao->executeNonQuery();
                            $ObjAuditDao->addactivitylog($modulename, $title, $restabid['hashkey'], $json_data);
                        }
                    }
                }
                return json_encode(array('Success'=>'True','Message'=>'Modifier Item Sync Successfully'));
            }
            else
            {
                return json_encode(array('Success'=>'False','Message'=>'Modifier item not available'));
            }

        }
        catch(\Exception $e)
        {
            $this->log->logIt($this->module.'-updatemodifieritemfromtabinsta -'.$e);
        }
    }


    public function gettabmenulist()
    {
        try
        {
            $this->log->logIt($this->module.'-gettabmenulist -');
            $dao = new \dao();

            $str = "SELECT tabinstaid,name,menuunkid  FROM ".CONFIG_DBN.".cfmenu WHERE tabinstaid!='' AND is_deleted=0  AND companyid=:companyid AND locationid=:locationid";

            $dao->initCommand($str);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res= $dao->executeQuery();

            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));

        }
        catch(\Exception $e)
        {
            $this->log->logIt($this->module.'-gettabmenulist -'.$e);
        }

    }

    public function gettabcatlist()
    {
        try
        {
            $this->log->logIt($this->module.'-gettabcatlist');
            $dao = new \dao();

            $str = "SELECT categoryunkid,categoryname,tabinstaid FROM ".CONFIG_DBN.".cfmenu_categories WHERE tabinstaid!='' AND is_deleted=0  AND companyid=:companyid AND locationid=:locationid ";
            $dao->initCommand($str);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res= $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-gettabcatlist -'.$e);
        }

    }

    public function updateitemfromtabinsta($data,$opdata,$modulename)
    {
        try
        {
            $this->log->logIt($this->module.'-updateitemfromtabinsta');
            $ObjAuditDao = new \database\auditlogdao();
            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            if($data['code'] == 200 && $data['status'] == 'ok')
            {
                $item = $data['payload'];

                foreach ($item as $key => $value) {

                    if ($value['modifiers']) {
                        $type = 1;
                    } else {
                        $type = 0;
                    }
                    $arr_log = array(
                        'Item Name' => $value['name'],
                        'Sale Amount' => $value['price'],
                        'SKU' => $value['sku'],
                        /*'Description' => $value['description'],*/
                        'Item Type' => $type,
                    );
                    $json_data = json_encode($arr_log);

                    if ($value['image_url'] > 0) {

                        $ig = $value['image_url'];
                        $first = reset($ig);
                        $nam = basename($first);
                        $destdir = 'assets/menu_item_images/';
                        $img = file_get_contents($first);
                        file_put_contents($destdir . substr($first, strrpos($first, '/')), $img);
                    } else {
                        $nam = '';
                    }

                    $str = "SELECT cmi.tabinstaid as tabinstaid,cmi.hashkey FROM " . CONFIG_DBN . ".cfmenu_items AS cmi 
                            WHERE cmi.tabinstaid=:tabinstaid  AND cmi.companyid=:companyid AND cmi.locationid=:locationid AND is_deleted=:is_deleted";

                    $dao->initCommand($str);
                    $dao->addParameter(':tabinstaid', $value['item_id']);
                    $dao->addParameter(':is_deleted', 0);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $res = $dao->executeRow();

                    if (!$res['tabinstaid'] == $value['item_id']) {

                        $title = "Sync from tabinsta(insert)";
                        $hashkey = \util\util::gethash();


                        $strpk = "SELECT categoryunkid AS catid FROM " . CONFIG_DBN . ".cfmenu_categories WHERE
                         tabinstaid=:tabinstaid AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid";

                        $dao->initCommand($strpk);
                        $dao->addParameter(':tabinstaid', $opdata['category_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':locationid', CONFIG_LID);
                        $res = $dao->executeRow();

                        if(!$res['catid'])
                        {
                            return json_encode(array("Success"=>"False","Message"=>"Something Wrong"));
                        }
                        else {
                            $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu_items( 
                                                                companyid,
                                                                locationid,
                                                                itemname,
                                                                sku,
                                                                amount,
                                                                sale_amount,
                                                                categoryunkid,
                                                                long_desc,
                                                                short_desc,
                                                                image,
                                                                created_user,
                                                                is_active,
                                                                createddatetime,
                                                                hashkey,
                                                                tabinstaid,
                                                                type)
                                                        VALUE(:companyid,
                                                              :locationid,
                                                              :txtname,
                                                              :txtsku,
                                                              :txtamount,
                                                              :txtsaleamount,
                                                              :txtcategory,
                                                              :txtlongscription,
                                                              :txtshortdescription,
                                                              :upload_img,
                                                              :created_user, 
                                                              :isactive,
                                                              :createddatetime,
                                                              :hashkey,
                                                              :tabinstaid,
                                                              :item_type)";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':txtname', $value['name']);
                            $dao->addParameter(':txtsku', $value['sku']);
                            $dao->addParameter(':txtamount', '');
                            $dao->addParameter(':txtsaleamount', $value['price']);
                            $dao->addParameter(':txtcategory', $res['catid']);
                            $dao->addParameter(':txtlongscription', $value['description']);
                            $dao->addParameter(':txtshortdescription', '');
                            $dao->addParameter(':upload_img', $nam);
                            $dao->addParameter(':isactive', 1);
                            $dao->addParameter(':created_user', CONFIG_UID);
                            $dao->addParameter(':createddatetime', $datetime);
                            $dao->addParameter(':hashkey', $hashkey);
                            $dao->addParameter(':item_type', $type);
                            $dao->addParameter(':tabinstaid', $value['item_id']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $dao->executeNonQuery();
                            $itemunkid = $dao->getLastInsertedId();

                            /*  modifier modifier modifier modifier*/

                            if (isset($value['modifiers'])) {
                                $modifier = $value['modifiers'];

                                foreach ($modifier as $modi => $fier) {


                                    $strpk = "SELECT modifierunkid AS modid FROM " . CONFIG_DBN . ".cfmenu_modifiers WHERE 
                            tabinstaid=:tabinstaid AND companyid=:companyid AND locationid=:locationid AND is_deleted=0 ";
                                    $dao->initCommand($strpk);
                                    $dao->addParameter(':tabinstaid', $fier['modifier_id']);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addParameter(':locationid', CONFIG_LID);
                                    $mod = $dao->executeRow();

                                    $strSql = " INSERT INTO " . CONFIG_DBN . ".fditem_modifier_relation (lnkitemid,
                                                                                                lnkmodifierid, 
                                                                                                min,
                                                                                                max, 
                                                                                                created_user,
                                                                                                createddatetime, 
                                                                                                companyid,
                                                                                                locationid)";
                                    $strSql .= " VALUE (:lnkitemid,
                                                    :lnkmodifierid,
                                                    :min, 
                                                    :max, 
                                                    :created_user, 
                                                    :createddatetime, 
                                                    :companyid,
                                                    :locationid)";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':lnkitemid', $itemunkid);
                                    $dao->addParameter(':lnkmodifierid', $mod['modid']);
                                    $dao->addParameter(':min', $fier['min_item']);
                                    $dao->addParameter(':max', $fier['max_item']);
                                    $dao->addParameter(':created_user', CONFIG_UID);
                                    $dao->addParameter(':createddatetime', $datetime);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $dao->executeNonQuery();
                                    $relmodifierid = $dao->getLastInsertedId();

                                    /*modifier item modifier item modifier item*/

                                    if (isset($fier['items'])) {
                                        $moditem = $fier['items'];

                                        foreach ($moditem as $ky => $mitem) {


                                            $strpk = "SELECT modifieritemunkid AS moditemid FROM " . CONFIG_DBN . ".cfmenu_modifier_items WHERE tabinstaid=:tabinstaid AND companyid=:companyid AND locationid=:locationid AND is_deleted=0 ";
                                            $dao->initCommand($strpk);
                                            $dao->addParameter(':tabinstaid', $mitem['modifieritem_id']);
                                            $dao->addParameter(':companyid', CONFIG_CID);
                                            $dao->addParameter(':locationid', CONFIG_LID);
                                            $moditem = $dao->executeRow();

                                            $strSql = "INSERT INTO " . CONFIG_DBN . ".fdmodifier_item_relation (lnkmodifierrelid,lnkitemid, lnkmodifierid,lnkmodifieritemid, amount, sale_amount, description, created_user, createddatetime, companyid,locationid)";
                                            $strSql .= " VALUE (:lnkmodifierrelid, :lnkitemid, :lnkmodifierid, :lnkmodifieritemid, :amount, :sale_amount, :description, :created_user, :createddatetime, :companyid,:locationid)";
                                            $dao->initCommand($strSql);
                                            $dao->addParameter(':lnkmodifierrelid', $relmodifierid);
                                            $dao->addParameter(':lnkitemid', $itemunkid);
                                            $dao->addParameter(':lnkmodifierid', $mod['modid']);
                                            $dao->addParameter(':lnkmodifieritemid', $moditem['moditemid']);
                                            $dao->addParameter(':amount', '');
                                            $dao->addParameter(':sale_amount', $mitem['price']);
                                            $dao->addParameter(':description', '');
                                            $dao->addParameter(':created_user', CONFIG_UID);
                                            $dao->addParameter(':createddatetime', $datetime);
                                            $dao->addParameter(':companyid', CONFIG_CID);
                                            $dao->addparameter(':locationid', CONFIG_LID);
                                            $dao->executeNonQuery();
                                        }
                                    } else {

                                        continue;
                                    }
                                }
                            } else {

                                continue;
                            }
                        }
                            $ObjAuditDao->addactivitylog($modulename, $title, $hashkey, $json_data);


                    } else {
                        $title = "Sync from tabinsta(update)";

                        $strpk = "SELECT categoryunkid AS catid FROM " . CONFIG_DBN . ".cfmenu_categories WHERE
                         tabinstaid=:tabinstaid AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid";

                        $dao->initCommand($strpk);
                        $dao->addParameter(':tabinstaid', $opdata['category_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':locationid', CONFIG_LID);
                        $result = $dao->executeRow();

                        if (!$result['catid']) {
                            return json_encode(array("Success" => "False", "Message" => "Something Wrong"));
                        } else {
                            $strSql = "UPDATE " . CONFIG_DBN . ".cfmenu_items SET itemname=:txtname,
                           categoryunkid=:txtcategory,amount=:txtamount,sale_amount=:txtsaleamount,
                           sku=:txtsku,long_desc=:txtlongdescription,short_desc=:txtshortdescription,
                           image=:upload_img,modifieddatetime=:modifieddatetime,
                           modified_user=:modified_user,type=:type
                           WHERE tabinstaid=:tabinstaid AND companyid=:companyid AND locationid=:locationid";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':tabinstaid', $value['item_id']);
                            $dao->addParameter(':txtname', $value['name']);
                            $dao->addParameter(':txtcategory', $result['catid']);
                            $dao->addParameter(':txtamount', '');
                            $dao->addParameter(':txtsaleamount', $value['price']);
                            $dao->addParameter(':txtsku', $value['sku']);
                            $dao->addParameter(':txtlongdescription', $value['description']);
                            $dao->addParameter(':txtshortdescription', '');
                            $dao->addParameter(':upload_img', $nam);
                            $dao->addParameter(':modifieddatetime', $datetime);
                            $dao->addParameter(':type', $type);
                            $dao->addParameter(':modified_user', CONFIG_UID);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $dao->executeNonQuery();

                            $id = $ObjCommonDao->getprimarykey('cfmenu_items', $res['hashkey'], 'itemunkid');

                            $modifier = $value['modifiers'];


                            if (isset($modifier) && count($value['modifiers']) > 0) {

                                $strSql = " SELECT relationunkid FROM " . CONFIG_DBN . ".fditem_modifier_relation WHERE lnkitemid=:itemid AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':itemid', $id);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $item_modifier = $dao->executeQuery();
                                $mod_arr = \util\util::getoneDarray($item_modifier, 'relationunkid');
                                $bind_modifier = $value['modifiers'];
                                foreach ($bind_modifier AS $key => $val) {

                                    $str = "SELECT modifierunkid,modifiername  FROM " . CONFIG_DBN . ".cfmenu_modifiers WHERE tabinstaid=:tabinstaid AND companyid=:companyid AND locationid=:locationid AND is_deleted=0 ";
                                    $dao->initCommand($str);
                                    $dao->addParameter(':tabinstaid', $val['modifier_id']);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addParameter(':locationid', CONFIG_LID);
                                    $modid = $dao->executeRow();


                                    // $modid = $val['modifier_id'];
                                    $strSql = "SELECT relationunkid FROM " . CONFIG_DBN . ".fditem_modifier_relation WHERE lnkitemid=:itemid AND lnkmodifierid=:lnkmodifierid AND companyid=:companyid AND locationid=:locationid";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':itemid', $id);
                                    $dao->addParameter(':lnkmodifierid', $modid['modifierunkid']);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $perticuler_mod = $dao->executeRow();

                                    if (in_array($perticuler_mod['relationunkid'], $mod_arr)) {
                                        //$this->log->logIt("Modifier came to update - ".$modid);
                                        $strSql = " UPDATE " . CONFIG_DBN . ".fditem_modifier_relation SET lnkitemid=:lnkitemid, lnkmodifierid=:lnkmodifierid, min=:min, max=:max, modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND relationunkid=:relationunkid AND locationid=:locationid";

                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':relationunkid', $perticuler_mod['relationunkid']);
                                        $dao->addParameter(':lnkitemid', $id);
                                        $dao->addParameter(':lnkmodifierid', $modid['modifierunkid']);
                                        $dao->addParameter(':min', $val['min_item']);
                                        $dao->addParameter(':max', $val['max_item']);
                                        $dao->addParameter(':modified_user', CONFIG_UID);
                                        $dao->addParameter(':modifieddatetime', $datetime);
                                        $dao->addParameter(':companyid', CONFIG_CID);
                                        $dao->addparameter(':locationid', CONFIG_LID);
                                        $dao->executeNonQuery();

                                        $relmodifierid = $perticuler_mod['relationunkid'];
                                        if (($key = array_search($relmodifierid, $mod_arr)) !== false) {
                                            //$this->log->logIt("Modifier came to Unset - ".$modid);
                                            unset($mod_arr[$key]);
                                        }
                                    } else {
                                        $strSql = " INSERT INTO " . CONFIG_DBN . ".fditem_modifier_relation (lnkitemid, lnkmodifierid, min, max, created_user,createddatetime, companyid,locationid)";
                                        $strSql .= " VALUE (:lnkitemid, :lnkmodifierid, :min, :max, :created_user, :createddatetime, :companyid,:locationid)";
                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':lnkitemid', $id);
                                        $dao->addParameter(':lnkmodifierid', $modid['modifierunkid']);
                                        $dao->addParameter(':min', $val['min_item']);
                                        $dao->addParameter(':max', $val['max_item']);
                                        $dao->addParameter(':created_user', CONFIG_UID);
                                        $dao->addParameter(':createddatetime', $datetime);
                                        $dao->addParameter(':companyid', CONFIG_CID);
                                        $dao->addparameter(':locationid', CONFIG_LID);
                                        $dao->executeNonQuery();
                                        $relmodifierid = $dao->getLastInsertedId();
                                    }


                                    $strSql = "SELECT relationunkid FROM " . CONFIG_DBN . ".fdmodifier_item_relation WHERE lnkmodifierrelid=:lnkmodifierrelid AND lnkitemid=:lnkitemid AND companyid=:companyid AND locationid=:locationid";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':lnkitemid', $id);
                                    $dao->addParameter(':lnkmodifierrelid', $relmodifierid);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $perticuler_mod_item = $dao->executeQuery();
                                    $mod_item_arr = \util\util::getoneDarray($perticuler_mod_item, 'relationunkid');

                                    $bind_modifier_item = isset($val['items']) ? $val['items'] : array();

                                    if (count($bind_modifier_item) > 0) {
                                        foreach ($bind_modifier_item AS $key => $sub_val) {

                                            $strpk = "SELECT modifieritemunkid   FROM " . CONFIG_DBN . ".cfmenu_modifier_items WHERE tabinstaid=:tabinstaid AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid";
                                            $dao->initCommand($strpk);
                                            $dao->addParameter(':tabinstaid', $sub_val['modifieritem_id']);
                                            $dao->addParameter(':companyid', CONFIG_CID);
                                            $dao->addParameter(':locationid', CONFIG_LID);
                                            $moditem = $dao->executeRow();

                                            $strSql = "SELECT relationunkid FROM " . CONFIG_DBN . ".fdmodifier_item_relation WHERE lnkmodifierrelid=:lnkmodifierrelid AND lnkitemid=:lnkitemid AND lnkmodifieritemid=:lnkmodifieritemid AND companyid=:companyid AND locationid=:locationid";
                                            $dao->initCommand($strSql);
                                            $dao->addParameter(':lnkitemid', $id);
                                            $dao->addParameter(':lnkmodifierrelid', $relmodifierid);
                                            $dao->addParameter(':lnkmodifieritemid', $moditem['modifieritemunkid']);
                                            $dao->addParameter(':companyid', CONFIG_CID);
                                            $dao->addparameter(':locationid', CONFIG_LID);
                                            $perticuler_itm_mod = $dao->executeRow();

                                            if (in_array($perticuler_itm_mod['relationunkid'], $mod_item_arr)) {

                                                $strSql = "UPDATE " . CONFIG_DBN . ".fdmodifier_item_relation SET lnkmodifierrelid=:lnkmodifierrelid, lnkitemid=:lnkitemid, lnkmodifierid=:lnkmodifierid, lnkmodifieritemid=:lnkmodifieritemid, amount=:amount, sale_amount=:sale_amount, description=:description, modified_user=:modified_user, modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND relationunkid=:relationunkid AND locationid=:locationid";
                                                $dao->initCommand($strSql);
                                                $dao->addParameter(':relationunkid', $perticuler_itm_mod['relationunkid']);
                                                $dao->addParameter(':lnkmodifierrelid', $relmodifierid);
                                                $dao->addParameter(':lnkitemid', $id);
                                                $dao->addParameter(':lnkmodifierid', $modid['modifierunkid']);
                                                $dao->addParameter(':lnkmodifieritemid', $moditem['modifieritemunkid']);
                                                $dao->addParameter(':amount', '');
                                                $dao->addParameter(':sale_amount', $sub_val['price']);
                                                $dao->addParameter(':description', '');
                                                $dao->addParameter(':modified_user', CONFIG_UID);
                                                $dao->addParameter(':modifieddatetime', $datetime);
                                                $dao->addParameter(':companyid', CONFIG_CID);
                                                $dao->addparameter(':locationid', CONFIG_LID);
                                                $dao->executeNonQuery();
                                                if (($key = array_search($perticuler_itm_mod['relationunkid'], $mod_item_arr)) !== false) {
                                                    unset($mod_item_arr[$key]);
                                                }
                                            } else {

                                                $strSql = "INSERT INTO " . CONFIG_DBN . ".fdmodifier_item_relation (lnkmodifierrelid,lnkitemid, lnkmodifierid,lnkmodifieritemid, amount, sale_amount, description, created_user, createddatetime, companyid,locationid)";
                                                $strSql .= " VALUE (:lnkmodifierrelid, :lnkitemid, :lnkmodifierid, :lnkmodifieritemid, :amount, :sale_amount, :description, :created_user, :createddatetime, :companyid,:locationid)";
                                                $dao->initCommand($strSql);
                                                $dao->addParameter(':lnkmodifierrelid', $relmodifierid);
                                                $dao->addParameter(':lnkitemid', $id);
                                                $dao->addParameter(':lnkmodifierid', $modid['modifierunkid']);
                                                $dao->addParameter(':lnkmodifieritemid', $moditem['modifieritemunkid']);
                                                $dao->addParameter(':amount', $sub_val['price']);
                                                $dao->addParameter(':sale_amount', $sub_val['price']);
                                                $dao->addParameter(':description', '');
                                                $dao->addParameter(':created_user', CONFIG_UID);
                                                $dao->addParameter(':createddatetime', $datetime);
                                                $dao->addParameter(':companyid', CONFIG_CID);
                                                $dao->addparameter(':locationid', CONFIG_LID);
                                                $dao->executeNonQuery();
                                            }
                                        }
                                    } else {
                                        continue;
                                    }
                                    if (count($mod_item_arr) > 0) {
                                        $strid = implode(",", $mod_item_arr);
                                        $strSql = "DELETE FROM " . CONFIG_DBN . ".fdmodifier_item_relation WHERE lnkitemid=:lnkitemid AND lnkmodifierid=:lnkmodifierid AND relationunkid IN (" . $strid . ") AND companyid=:companyid AND locationid=:locationid";
                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':lnkitemid', $id);
                                        $dao->addParameter(':lnkmodifierid', $modid['modifierunkid']);
                                        $dao->addParameter(':companyid', CONFIG_CID);
                                        $dao->addparameter(':locationid', CONFIG_LID);
                                        $dao->executeNonQuery();
                                    }
                                }
                                if (count($mod_arr) > 0) {

                                    $strid = implode(",", $mod_arr);
                                    $strSql = "DELETE FROM " . CONFIG_DBN . ".fditem_modifier_relation WHERE lnkitemid=:lnkitemid AND relationunkid IN (" . $strid . ") AND companyid=:companyid AND locationid=:locationid";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':lnkitemid', $id);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $dao->executeNonQuery();
                                    $strSql = "DELETE FROM " . CONFIG_DBN . ".fdmodifier_item_relation WHERE lnkitemid=:lnkitemid AND lnkmodifierrelid IN (" . $strid . ") AND companyid=:companyid AND locationid=:locationid";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':lnkitemid', $id);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $dao->executeNonQuery();
                                }


                            } else {
                                /*Delete From Item- Modifier Relation*/

                                $strSql = "DELETE FROM " . CONFIG_DBN . ".fditem_modifier_relation WHERE lnkitemid=:id AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(":id", $id);
                                $dao->addParameter(":companyid", CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $dao->executeNonQuery();
                                /*Delete From Item- Modifier Relation*/
                                /*Delete From Item- Modifier Item Relation*/
                                $strSql = "DELETE FROM " . CONFIG_DBN . ".fdmodifier_item_relation WHERE lnkitemid=:id AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(":id", $id);
                                $dao->addParameter(":companyid", CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $dao->executeNonQuery();
                                continue;
                                /*Delete From Item- Modifier Item Relation*/
                            }
                            $ObjAuditDao->addactivitylog($modulename, $title, $res['hashkey'], $json_data);
                        }
                    }
                }
                return json_encode(array('Success' => 'True', 'Message' => 'Sync Item update successfully'));
            }
            else
            {
                return json_encode(array('Success'=>'False','Message'=>$data['message']));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-updateitemfromtabinsta-'.$e);
        }
    }

    public function orderfromtabinsta($data)
    {
        try
        {
            $this->log->logIt($this->module.'-orderfromtabinsta');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $orderdata = $data['order'];
            $hashkey = \util\util::gethash();
            $ObjAuditDao = new \database\orderlogdao();
            $datetime=\util\util::getLocalDateTime();


            $str = "INSERT INTO ".CONFIG_DBN.".trcontact SET
                    name=:name,
                    mobile=:mobile,
                    email=:email,
                    hashkey=:hashkey,
                    companyid=:companyid,
                    locationid=:locationid ";
            $dao->initCommand($str);
            $dao->addParameter(':name', $orderdata['full_name']);
            $dao->addParameter(':mobile', $orderdata['phoneno']);
            $dao->addParameter(':email', $orderdata['user_email']);
            $dao->addParameter(':hashkey', $hashkey);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->executeNonQuery();
            $trid = $dao->getLastInsertedId();
            $year = \util\util::getYearCode();
            $meta = $orderdata['meta'];
            $invno = "";
            $localdatetime = \util\util::getLocalDateTime();
            $invoiceObj = new \database\orderdao();
            $invno = $invoiceObj->getInvoiceNumber('inc');


            $table = isset($meta['_tables']) ? $meta['_tables'] : 0;

            if($table!= 0){
                $tabletabinstaid = $table[0]['table_id'];
            }else{
                $tabletabinstaid = 0;
            }
            $str = "SELECT tableunkid,tablename FROM ".CONFIG_DBN.".cfclasstable WHERE tabinstaid=:tabinstaid AND companyid=:companyid 
                    AND locationid=:locationid";
            $dao->initCommand($str);
            $dao->addParameter(':tabinstaid',$tabletabinstaid);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',CONFIG_LID);
            $tbleunkid = $dao->executeRow();
            $tbledata = $tbleunkid['tablename'];

            $strFolio = "INSERT INTO ".CONFIG_DBN.".fasfoliomaster SET 
                       companyid=:companyid,
                       locationid=:locationid,
                       year_code=:year_code,
                       lnkcontactid=:lnkcontactid,
                       opendate=:opendate,
                       foliotypeunkid=:foliotypeunkid,
                       isfolioclosed=:isfolioclosed,
                       docno=:docno,
                       lnktableunkid=:lnktableunkid,
                       foliono=:foliono, 
                       hashkey=:hashkey,
                      folioopenuserunkid=:folioopenuserunkid";
            $dao->initCommand($strFolio);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->addParameter(':year_code', $year);
            $dao->addParameter(':lnkcontactid', $trid);
            $dao->addParameter(':opendate', $orderdata['order_datetime']);
            $dao->addParameter(':isfolioclosed',1);
            $dao->addParameter(':foliotypeunkid', 1);
            $dao->addParameter(':docno', '');
            $dao->addParameter(':lnktableunkid', $tbleunkid['tableunkid']);
            $dao->addParameter(':foliono', $invno);
            $dao->addParameter(':hashkey', $hashkey);
            $dao->addParameter(':folioopenuserunkid', CONFIG_UID);
            $dao->executeNonQuery();
            $fmid = $dao->getLastInsertedId();

            $ObjAuditDao->addOrderLog($fmid,'ADD_ORDER',$data,$invno,$orderdata,$tbledata);


            if(!empty($tbleunkid) && count($tbleunkid)>0){
                $str = "INSERT INTO " . CONFIG_DBN . ".cftablesession SET lnkfolioid=:lnkfolioid,
                          lnktableunkid=:lnktableunkid,
                          startdate=:startdate,
                          releasedate=:releasedate,
                          isreleased=:isreleased,
                          hashkey=:hashkey,
                          companyid=:companyid,
                          locationid=:locationid";
                $dao->initCommand($str);
                $dao->addParameter(':lnkfolioid', $fmid);
                $dao->addParameter(':lnktableunkid', $tbleunkid['tableunkid']);
                $dao->addParameter(':startdate', $orderdata['block_time']);
                $dao->addParameter(':releasedate', $datetime);
                $dao->addParameter(':isreleased', 1);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
            }
            $items = $orderdata['items'];

            foreach ($items AS $item) {

                $str = "SELECT itemunkid FROM " . CONFIG_DBN . ".cfmenu_items WHERE tabinstaid=:tabinstaid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($str);
                $dao->addParameter(':tabinstaid', $item['item_id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $itemid = $dao->executeRow();
                $item_id = $itemid['itemunkid'];


                if(empty($item_id) && count($item_id)<0)
                {
                    return json_encode(array('Success' => 'False', 'Message' => 'Item not availble'));
                }
                else
                {

                    $hashkey1 = \util\util::gethash();

                    $tax = $meta['_taxes'];
                    $master_type = $ObjCommonDao->getmastertype('8');

                    $strSql = "INSERT INTO ".CONFIG_DBN.".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                            " , masterunkid=:masterunkid,year_code=:year_code,quantity=:quantity,trandate=:trandate, baseamount=:baseamount,parentid=:parentid" .
                            " , description=:description,lnkmappingunkid=:lnkmappingunkid, taxexempt=:taxexempt, hashkey=:hashkey";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':foliounkid', $fmid);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $dao->addParameter(":postinguserunkid", CONFIG_UID);
                    $dao->addParameter(':masterunkid', $master_type['MasterId']);
                    $dao->addParameter(':year_code', $year);
                    $dao->addParameter(':baseamount', $item['product_price']);
                    $dao->addParameter(':parentid', 0);
                    $dao->addParameter(':quantity', $item['product_quantity']);
                    $dao->addParameter(':description', '');
                    $dao->addParameter(':lnkmappingunkid', $item_id);
                    $dao->addParameter(':trandate', $orderdata['order_datetime']);
                    $dao->addParameter(':taxexempt', 0);
                    $dao->addParameter(':hashkey', $hashkey1);
                    $dao->executeNonQuery();
                    $id = $dao->getLastInsertedId();
                    $this->addtaxonorderfromtabinsta($tax, $item, $fmid, $id, $orderdata);
                }
            }
            return json_encode(array('Success' => 'True', 'Message' => 'Order Placed Successfully'));
        }
        catch(\Exception $e)
        {
            $this->log->logIt($this->module.'-orderfromtabinsta-'.$e);
        }
    }


    public function addtaxonorderfromtabinsta($tax,$item,$fmid,$id,$orderdata)
    {
        try {
            $this->log->logIt($this->module . '-addtaxonorderfromtabinsta');
            $dao = new \dao();
            $year = \util\util::getYearCode();

            $discount  =  $item['product_discount'];
            $discountprise = 0;
            $disdata = array();
            if($discount > 0)
            {
                $str = "SELECT discountunkid, postingrule,lnkmasterunkid,value FROM ".CONFIG_DBN.".cfdiscount WHERE tabinstaid=:tabinstaid AND companyid=:companyid AND locationid=:locationid ";
                $dao->initCommand($str);
                $dao->addParameter(':tabinstaid',$orderdata['promocode_id']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addParameter(':locationid',CONFIG_LID);
                $disdata = $dao->executeRow();
                $discountprise = $disdata['value'];
            }
            

            foreach ($tax AS $taxvalue) {


                $str = "SELECT ctxdetail.taxdetailunkid, ctxdetail.amount,ctxdetail.postingrule,ctxdetail.taxapplyafter,ctax.taxunkid,ctax.lnkmasterunkid FROM " . CONFIG_DBN . ".cftax AS ctax
                        INNER JOIN " . CONFIG_DBN . ".cftaxdetail AS ctxdetail ON ctxdetail.taxunkid = ctax.taxunkid AND 
                        ctxdetail.taxdetailunkid = (SELECT taxdetailunkid FROM " . CONFIG_DBN . ".cftaxdetail WHERE taxunkid=ctax.taxunkid ORDER BY entrydatetime DESC LIMIT 1)
                        WHERE ctax.tabinstaid=:tabinstaid AND ctax.companyid=:companyid AND ctax.locationid=:locationid ";

                $dao->initCommand($str);
                $dao->addParameter(':tabinstaid', $taxvalue['tax_id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $taxdata = $dao->executeRow();

                if($discountprise == 0) {
                    if ($taxdata['postingrule'] == 1) {
                        $orderAmt = $item['product_price'];
                        $taxamt = ($orderAmt * $taxdata['amount']) / 100;
                    }
                    else {
                        $taxdisc = $taxdata['amount'];
                        $orderAmt = $item['product_price'];
                        $taxamt = ($orderAmt - $taxdisc);
                    }
                }
                else
                {
                    if ($taxdata['postingrule'] == 1) {

                        if($disdata['postingrule'] == 1) {
    
                            $orderAmt = $item['product_price'];
                            $itmds = ($orderAmt * $discountprise) / 100;
                            $taxamt = ($orderAmt - $itmds);
                            $taxamt = ($taxamt * $taxdata['amount']) / 100;
                            $taxamt = ($taxamt * $item['product_quantity']);
                        }
                        else {
    
                            $orderAmt = $item['product_price'];
                            $taxamt = ($orderAmt - $discountprise);
                            $taxamt = ($taxamt * $taxdata['amount']) / 100;
                            $taxamt = ($taxamt * $item['product_quantity']);
                        }
    
                    } else {
                        if ($disdata['postingrule'] == 1) {
                            $taxdisc = $taxdata['amount'];
                            $orderAmt = $item['product_price'];
                            $itmds = ($orderAmt * $discountprise) / 100;
                            $taxamt = ($orderAmt - $itmds);
                            $taxamt = ($taxamt - $taxdisc);
                            $taxamt = ($taxamt * $item['product_quantity']);
    
                        } else {
    
                            $taxdisc = $taxdata['amount'];
                            $orderAmt = $item['product_price'];
                            $taxamt = ($orderAmt - $discountprise);
                            $taxamt = ($taxamt - $taxdisc);
                            $taxamt = ($taxamt * $item['product_quantity']);
    
                        }
                    }
                }

                $hashkey = \util\util::gethash();
                $strSql = "INSERT INTO " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                    " , masterunkid=:masterunkid,year_code=:year_code,quantity=:quantity,trandate=:trandate, baseamount=:baseamount,parentid=:parentid" .
                    " , description=:description,lnkmappingunkid=:lnkmappingunkid, taxexempt=:taxexempt, hashkey=:hashkey";
                $dao->initCommand($strSql);
                $dao->addParameter(':foliounkid', $fmid);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->addParameter(":postinguserunkid", CONFIG_UID);
                $dao->addParameter(':masterunkid', $taxdata['lnkmasterunkid']);
                $dao->addParameter(':year_code', $year);
                $dao->addParameter(':baseamount',$taxamt);
                $dao->addParameter(':parentid', $id);
                $dao->addParameter(':quantity', $item['product_quantity']);
                $dao->addParameter(':description', '');
                $dao->addParameter(':lnkmappingunkid', $taxdata['taxdetailunkid']);
                $dao->addParameter(':trandate', $orderdata['order_datetime']);
                $dao->addParameter(':taxexempt', 0);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->executeNonQuery();
            }
            $this->addmodifieranditem($tax,$id,$item,$fmid,$orderdata,$disdata,$discountprise);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-addtaxonorderfromtabinsta-'.$e);
        }
    }

    public function addmodifieranditem($tax,$id,$item,$fmid,$orderdata,$disdata,$discountprise)
    {
        try {
            $this->log->logIt($this->module . '-addmodifieranditem');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $year = \util\util::getYearCode();

            if (!isset($item['modifiers'])) {
                $modifieritemprice = 0;
                $qunti = 0;
                $this->addtotaldiscount($disdata, $modifieritemprice, $item, $fmid, $id, $orderdata, $qunti, $discountprise);
            }
            else
            {
                $data = $item['modifiers'];
                $itmqun = $item['product_quantity'];
                foreach ($data as $key => $moditem) {
                    if (isset($moditem['items'])) {
                        $itemdata = $moditem['items'];
    
                        foreach ($itemdata AS $itemm) {
    
                            $master_type = $ObjCommonDao->getmastertype('9');
    
                            $hash_key = \util\util::gethash();
    
                            $str = "SELECT modifieritemunkid FROM " . CONFIG_DBN . ".cfmenu_modifier_items WHERE tabinstaid=:tabinstaid AND companyid=:companyid AND locationid=:locationid  ";
                            $dao->initCommand($str);
                            $dao->addParameter(':tabinstaid', $itemm['modifieritem_id']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addParameter(':locationid', CONFIG_LID);
                            $moditemid = $dao->executeRow();
    
                            if ($itemm['is_free'] == 1) {
                                $max = $itemm['max_no'];
                                $qun = $itemm['quantity'];
                                $im = $qun - $max;
    
                                if ($im > 0) {
                                    $modifieritemprice = $itemm['price'];
                                    $itemm['quantity'] = $im;
                                } else {
                                    $modifieritemprice = 0;
                                }
                            } else {
                                $modifieritemprice = $itemm['price'];
    
                            }
                            $qunti = ($itemm['quantity'] * $itmqun);
    
                            $strSql = "INSERT INTO " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                                " , masterunkid=:masterunkid,year_code=:year_code, quantity=:quantity,description=:description,trandate=:trandate, baseamount=:baseamount,parentid=:parentid" .
                                " , lnkmappingunkid=:lnkmappingunkid, taxexempt=:taxexempt, hashkey=:hashkey";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':foliounkid', $fmid);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $dao->addParameter(":postinguserunkid", CONFIG_UID);
                            $dao->addParameter(':masterunkid', $master_type['MasterId']);
                            $dao->addParameter(':year_code', $year);
                            $dao->addParameter(':baseamount', $modifieritemprice);
                            $dao->addParameter(':parentid', $id);
                            $dao->addParameter(':quantity', $qunti);
                            $dao->addParameter(':description', '');
                            $dao->addParameter(':lnkmappingunkid', $moditemid['modifieritemunkid']);
                            $dao->addParameter(':trandate', $orderdata['order_datetime']);
                            $dao->addParameter(':taxexempt', 0);
                            $dao->addParameter(':hashkey', $hash_key);
                            $dao->executeNonQuery();
    
                            foreach ($tax AS $taxvalue) {
    
                                $str = "SELECT ctxdetail.taxdetailunkid, ctxdetail.amount,ctxdetail.postingrule,ctxdetail.taxapplyafter,ctax.taxunkid,ctax.lnkmasterunkid FROM " . CONFIG_DBN . ".cftax AS ctax
                                        INNER JOIN " . CONFIG_DBN . ".cftaxdetail AS ctxdetail ON ctxdetail.taxunkid = ctax.taxunkid AND 
                                        ctxdetail.taxdetailunkid = (SELECT taxdetailunkid FROM " . CONFIG_DBN . ".cftaxdetail WHERE taxunkid=ctax.taxunkid ORDER BY entrydatetime DESC LIMIT 1)
                                        WHERE ctax.tabinstaid=:tabinstaid AND ctax.companyid=:companyid AND ctax.locationid=:locationid ";
    
                                $dao->initCommand($str);
                                $dao->addParameter(':tabinstaid', $taxvalue['tax_id']);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addParameter(':locationid', CONFIG_LID);
                                $taxdata = $dao->executeRow();
    
                                if ($discountprise == 0) {
                                    if ($taxdata['postingrule'] == 1) {
                                        $moditemamt = $modifieritemprice;
                                        $taxamt = ($moditemamt * $taxdata['amount']) / 100;
    
                                    } else {
                                        $moditemamt = $modifieritemprice;
                                        $taxamt = ($moditemamt - $taxdata['amount']);
                                    }
                                } else {
    
                                    if ($taxdata['postingrule'] == 1) {
    
                                        if ($disdata['postingrule'] == 1) {
    
                                            $moditemamt = $modifieritemprice;
    
                                            $itmdi = ($moditemamt * $disdata['value']) / 100;
    
                                            $taxamt = ($moditemamt - $itmdi);
                                            $taxamt = ($taxamt * $taxdata['amount']) / 100;
    
                                        } else {
                                            $moditemamt = $modifieritemprice;
    
                                            $taxamt = ($moditemamt - $disdata['value']);
                                            $taxamt = ($taxamt * $taxdata['amount']) / 100;
    
                                        }
                                    } else {
    
                                        if ($disdata['postingrule'] == 1) {
                                            $moditemamt = $modifieritemprice;
                                            $itmdi = ($moditemamt * $disdata['value']) / 100;
                                            $taxamt = ($moditemamt - $itmdi);
                                            $taxamt = ($taxamt - $taxdata['amount']);
    
                                        } else {
    
                                            $moditemamt = $modifieritemprice;
                                            $taxamt = ($moditemamt - $disdata['value']);
                                            $taxamt = ($taxamt - $taxdata['amount']);
    
                                        }
                                    }
                                }
                                $hashkey = \util\util::gethash();
    
                                $strSql = "INSERT INTO " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                                    " , masterunkid=:masterunkid,year_code=:year_code,quantity=:quantity,trandate=:trandate, baseamount=:baseamount,parentid=:parentid" .
                                    " , description=:description,lnkmappingunkid=:lnkmappingunkid, taxexempt=:taxexempt, hashkey=:hashkey";
    
                                $dao->initCommand($strSql);
                                $dao->addParameter(':foliounkid', $fmid);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $dao->addParameter(":postinguserunkid", CONFIG_UID);
                                $dao->addParameter(':masterunkid', $taxdata['lnkmasterunkid']);
                                $dao->addParameter(':year_code', $year);
                                $dao->addParameter(':baseamount', $taxamt);
                                $dao->addParameter(':parentid', $id);
                                $dao->addParameter(':quantity', $qunti);
                                $dao->addParameter(':description', '');
                                $dao->addParameter(':lnkmappingunkid', $taxdata['taxdetailunkid']);
                                $dao->addParameter(':trandate', $orderdata['order_datetime']);
                                $dao->addParameter(':taxexempt', 0);
                                $dao->addParameter(':hashkey', $hashkey);
                                $dao->executeNonQuery();
                            }
                        }
                    } else {
    
                        $master_type = $ObjCommonDao->getmastertype('10');
    
                        $hash_key = \util\util::gethash();
    
                        $str = "SELECT modifierunkid FROM " . CONFIG_DBN . ".cfmenu_modifiers WHERE tabinstaid=:tabinstaid AND is_deleted = 0 AND companyid=:companyid AND locationid=:locationid  ";
                        $dao->initCommand($str);
                        $dao->addParameter(':tabinstaid', $moditem['modifier_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':locationid', CONFIG_LID);
                        $modifierid = $dao->executeRow();
    
    
                        if ($moditem['is_free'] == 1) {
                            $max = $moditem['max'];
                            $qun = $moditem['quantity'];
                            $im = $qun - $max;
    
                            if ($im > 0) {
                                $modifieritemprice = $moditem['price'];
                                $moditem['quantity'] = $im;
                            } else {
                                $modifieritemprice = 0;
                            }
                        } else {
                            $modifieritemprice = $moditem['price'];
    
                        }
                        $qunti = ($moditem['quantity'] * $itmqun);
    
    
                        $strSql = "INSERT INTO " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                            " , masterunkid=:masterunkid,year_code=:year_code, quantity=:quantity,description=:description,trandate=:trandate, baseamount=:baseamount,parentid=:parentid" .
                            " , lnkmappingunkid=:lnkmappingunkid, taxexempt=:taxexempt, hashkey=:hashkey";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':foliounkid', $fmid);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->addParameter(":postinguserunkid", CONFIG_UID);
                        $dao->addParameter(':masterunkid', $master_type['MasterId']);
                        $dao->addParameter(':year_code', $year);
                        $dao->addParameter(':baseamount', $modifieritemprice);
                        $dao->addParameter(':parentid', $id);
                        $dao->addParameter(':quantity', $qunti);
                        $dao->addParameter(':description', '');
                        $dao->addParameter(':lnkmappingunkid', $modifierid['modifierunkid']);
                        $dao->addParameter(':trandate', $orderdata['order_datetime']);
                        $dao->addParameter(':taxexempt', 0);
                        $dao->addParameter(':hashkey', $hash_key);
                        $dao->executeNonQuery();
    
                        foreach ($tax AS $taxvalue) {
    
                            $str = "SELECT ctxdetail.taxdetailunkid, ctxdetail.amount,ctxdetail.postingrule,ctxdetail.taxapplyafter,ctax.taxunkid,ctax.lnkmasterunkid FROM " . CONFIG_DBN . ".cftax AS ctax
                            INNER JOIN " . CONFIG_DBN . ".cftaxdetail AS ctxdetail ON ctxdetail.taxunkid = ctax.taxunkid AND 
                            ctxdetail.taxdetailunkid = (SELECT taxdetailunkid FROM " . CONFIG_DBN . ".cftaxdetail WHERE taxunkid=ctax.taxunkid ORDER BY entrydatetime DESC LIMIT 1)
                            WHERE ctax.tabinstaid=:tabinstaid AND ctax.companyid=:companyid AND ctax.locationid=:locationid ";
    
                            $dao->initCommand($str);
                            $dao->addParameter(':tabinstaid', $taxvalue['tax_id']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addParameter(':locationid', CONFIG_LID);
                            $taxdata = $dao->executeRow();
    
                            if ($discountprise == 0) {
                                if ($taxdata['postingrule'] == 1) {
                                    $modamt = $modifieritemprice;
                                    $taxamt = ($modamt * $taxdata['amount']) / 100;
                                } else {
                                    $modamt = $modifieritemprice;
                                    $taxamt = ($modamt - $taxdata['amount']);
                                }
                            } else {
    
                                if ($taxdata['postingrule'] == 1) {
    
                                    if ($disdata['postingrule'] == 1) {
    
                                        $modamt = $modifieritemprice;
    
                                        $mdi = ($modamt * $disdata['value']) / 100;
    
                                        $taxamt = ($modamt - $mdi);
                                        $taxamt = ($taxamt * $taxdata['amount']) / 100;
    
                                    } else {
                                        $modamt = $modifieritemprice;
    
                                        $taxamt = ($modamt - $disdata['value']);
                                        $taxamt = ($taxamt * $taxdata['amount']) / 100;
    
                                    }
                                } else {
    
                                    if ($disdata['postingrule'] == 1) {
                                        $modamt = $modifieritemprice;
                                        $mdi = ($modamt * $disdata['value']) / 100;
                                        $taxamt = ($modamt - $mdi);
                                        $taxamt = ($taxamt - $taxdata['amount']);
    
                                    } else {
    
                                        $modamt = $modifieritemprice;
                                        $taxamt = ($modamt - $disdata['value']);
                                        $taxamt = ($taxamt - $taxdata['amount']);
    
                                    }
                                }
                            }
                            $hashkey1 = \util\util::gethash();
    
                            $strSql = "INSERT INTO " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                                " , masterunkid=:masterunkid,year_code=:year_code,quantity=:quantity,trandate=:trandate, baseamount=:baseamount,parentid=:parentid" .
                                " , description=:description,lnkmappingunkid=:lnkmappingunkid, taxexempt=:taxexempt, hashkey=:hashkey";
    
                            $dao->initCommand($strSql);
                            $dao->addParameter(':foliounkid', $fmid);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $dao->addParameter(":postinguserunkid", CONFIG_UID);
                            $dao->addParameter(':masterunkid', $taxdata['lnkmasterunkid']);
                            $dao->addParameter(':year_code', $year);
                            $dao->addParameter(':baseamount', $taxamt);
                            $dao->addParameter(':parentid', $id);
                            $dao->addParameter(':quantity', $qunti);
                            $dao->addParameter(':description', '');
                            $dao->addParameter(':lnkmappingunkid', $taxdata['taxdetailunkid']);
                            $dao->addParameter(':trandate', $orderdata['order_datetime']);
                            $dao->addParameter(':taxexempt', 0);
                            $dao->addParameter(':hashkey', $hashkey1);
                            $dao->executeNonQuery();
                        }
    
                    }
                    $this->addtotaldiscount($disdata, $modifieritemprice, $item, $fmid, $id, $orderdata, $qunti, $discountprise);
                }
            }
            return json_encode(array('Success' => 'True', 'Message' => 'Order Placed Successfully'));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-addmodifieranditem-'.$e);
        }
    }


    public function addtotaldiscount($disdata,$modifieritemprice,$item,$fmid,$id,$orderdata,$qunti,$discountprise)
    {
        try {
            $this->log->logIt($this->module . '-addtotaldiscount');

            $dao = new \dao();
            $year = \util\util::getYearCode();

            if ($discountprise == 0) {
                return 1;
            } else {

                $itemprise = $item['product_price'];
                $modifieritem = ($modifieritemprice * $qunti);

                if ($disdata['postingrule'] == 1) {
                    $total = $itemprise + $modifieritem;
                    $finaldiscount = ($total * $disdata['value']) / 100;
                } else {
                    $total = $itemprise + $modifieritem;
                    $finaldiscount = $total - $disdata['value'];
                }
                if ($finaldiscount > 0) {

                    $hash_key = \util\util::gethash();

                    $strSql = "INSERT INTO " . CONFIG_DBN . ".fasfoliodetail SET foliounkid=:foliounkid, companyid=:companyid,locationid=:locationid,postinguserunkid=:postinguserunkid" .
                        " , masterunkid=:masterunkid,year_code=:year_code,quantity=:quantity,trandate=:trandate, baseamount=:baseamount,parentid=:parentid" .
                        " , description=:description,lnkmappingunkid=:lnkmappingunkid, taxexempt=:taxexempt, hashkey=:hashkey";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':foliounkid', $fmid);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $dao->addParameter(":postinguserunkid", CONFIG_UID);
                    $dao->addParameter(':masterunkid', $disdata['lnkmasterunkid']);
                    $dao->addParameter(':year_code', $year);
                    $dao->addParameter(':baseamount', $finaldiscount);
                    $dao->addParameter(':quantity', 1);
                    $dao->addParameter(':parentid', $id);
                    $dao->addParameter(':description', '');
                    $dao->addParameter(':lnkmappingunkid', $disdata['discountunkid']);
                    $dao->addParameter(':trandate', $orderdata['order_datetime']);
                    $dao->addParameter(':taxexempt', 0);
                    $dao->addParameter(':hashkey', $hash_key);
                    $dao->executeNonQuery();
                } else {
                    return 1;
                }
            }
        }
        catch(\Exception $e)
        {
            $this->log->logIt($this->module.'-addtotaldiscount-'.$e);
        }
    }


    public function  beforeorderamttesting($data)
    {
        try
        {
            $this->log->logIt($this->module.'-beforeorderamttesting');

            $orderdata = $data['order'];


            $ordertotalamount = $orderdata['order_amount'];
            $ordertotalamount = (int)$ordertotalamount;
            $items = $orderdata['items'];

            $itemsum = 0;
            $modifieritemsum = 0;

            foreach ($items AS $key=>$item) {
                $itemsum += ($item['product_price'] * $item['product_quantity']);

                if(!isset($item['modifiers'])) {
                    $modifieritemsum += 0;
                }
                else {
                    $modifier = $item['modifiers'];
                    $itemquantity = $item['product_quantity'];

                    foreach ($modifier as $key => $modifier) {
                        if (isset($modifier['items'])) {

                            $modifierit = $modifier['items'];

                            foreach ($modifierit AS $key => $modifieritem) {
                                if ($modifieritem['is_free'] == 1) {
                                    $mitemprise = $modifieritem['price'];
                                    $maxno = $modifieritem['max_no'];
                                    $quantity = $modifieritem['quantity'];
                                    $cntitem = ($quantity - $maxno);
                                    $modifieritemprise = ($cntitem * $mitemprise);
                                    $modifieritemprise = ($modifieritemprise * $itemquantity);
                                    $modifieritemsum += $modifieritemprise;

                                } else {
                                    $quantity = $modifieritem['quantity'];
                                    $mitemprise = $modifieritem['price'];
                                    $modifieritemprise = ($mitemprise * $quantity);
                                    $modifieritemprise = ($modifieritemprise * $itemquantity);
                                    $modifieritemsum += $modifieritemprise;

                                }
                            }
                        } else {
                            if ($modifier['is_free'] == 1) {
                                $mprise = $modifier['price'];
                                $maxno = $modifier['max'];
                                $quantity = $modifier['quantity'];
                                $cntitem = ($quantity - $maxno);
                                $modifieritemprise = ($cntitem * $mprise);
                                $modifieritemprise = ($modifieritemprise * $itemquantity);
                                $modifieritemsum += $modifieritemprise;
                            } else {
                                $quantity = $modifier['quantity'];
                                $mitemprise = $modifier['price'];
                                $modifieritemprise = ($mitemprise * $quantity);
                                $modifieritemprise = ($modifieritemprise * $itemquantity);
                                $modifieritemsum += $modifieritemprise;

                            }
                        }
                    }
                }

            }

            $orderamt = $this->total($itemsum,$modifieritemsum,$orderdata,$item);
            $orderant = $orderamt['amount'];
            $this->log->logIt($orderant);
            $this->log->logIt($ordertotalamount);

            if($orderant == $ordertotalamount)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
        catch(\Exception $e)
        {
            $this->log->logIt($this->module.'-beforeorderamttesting -'.$e);
        }
    }

public function total($itemsum,$modifieritemsum,$orderdata,$item)
{
    try
    {
        $this->log->logIt($this->module.'-total');

        $taxsum = 0;
        $totalamount = $itemsum + $modifieritemsum;


        if($item['product_discount'] > 0)
        {
            $discount = $orderdata['order_promocode_amount'];
            $amtwithdec = ($totalamount  - $discount);
            $amtwithdec = (int)$amtwithdec;
        }
        else
        {
            $amtwithdec = (int)$totalamount;
        }
        $meta = $orderdata['meta'];
        $tax = $meta['_taxes'];

        foreach ($tax AS $key => $tax)
        {

            $taxamt = ($amtwithdec * $tax['tax_rate']) /100;
            $taxsum += $taxamt;
        }

        $amtwithtax = $amtwithdec + $taxsum;
        $amtwithtax = (int)$amtwithtax;

        $resObj = array("Success"=>"True","amount"=>$amtwithtax);
        return $resObj;
    }
    catch(Exception $e)
    {
        $this->log->logIt($this->module.'-total-'.$e);
    }
}


            /*SYNC TABLE*/


    public function updatetablefromtabinsta($dataa,$modulename)
    {
        try
        {
            $this->log->logIt($this->module.'-updatetablefromtabinsta');
            $dao = new \dao();
            $ObjAuditDao = new \database\auditlogdao();
            $mn = 'table_class';
            if($dataa['code'] == 200 && $dataa['status'] == 'ok')
            {
                $data = $dataa['payload'];

                foreach($data AS $key=>$class) {

                    $str = "SELECT classunkid,hashkey,tabinstaid FROM " . CONFIG_DBN . ".cfclass WHERE tabinstaid=:tabinstaid 
                            AND companyid=:companyid AND is_deleted=0 AND locationid=:locationid";
                    $dao->initCommand($str);
                    $dao->addParameter(':tabinstaid', $class['tabletype_id']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':locationid', CONFIG_LID);
                    $classid = $dao->executeRow();


                    $arr_log = array(
                        'Class Name'=>$class['name'],
                        'Status' => ($class['is_active'] == 1) ? 'Active' : 'Inactive',
                    );
                    $json_data = json_encode($arr_log);

                    if (!isset($classid['tabinstaid']) == $class['tabletype_id']) {
                        $title = "Sync from tabinsta(insert)";
                        $hashkey = \util\util::gethash();

                        $str = "INSERT INTO " . CONFIG_DBN . ".cfclass SET classname=:classname,
                                                                        createddatetime=:createddatetime,
                                                                        created_user=:created_user,
                                                                        is_active=:is_active,
                                                                        hashkey=:hashkey,
                                                                        companyid=:companyid,
                                                                        locationid=:locationid,
                                                                        tabinstaid=:tabinstaid                                                                      
                                                                        ";
                        $dao->initCommand($str);
                        $dao->addParameter(':classname', $class['name']);
                        $dao->addParameter(':createddatetime', $class['added_on']);
                        $dao->addParameter(':created_user', CONFIG_UID);
                        $dao->addParameter(':is_active', $class['is_active']);
                        $dao->addParameter(':hashkey', $hashkey);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':locationid', CONFIG_LID);
                        $dao->addParameter(':tabinstaid', $class['tabletype_id']);
                        $dao->executeNonQuery();
                        $clspkid = $dao->getLastInsertedId();
                        $ObjAuditDao->addactivitylog($mn, $title, $hashkey, $json_data);
                        if (isset($class['tables'])) {
                            $table = $class['tables'];

                            foreach ($table AS $keyd => $table) {

                                $arr_log = array(
                                    'Table Name' => $table['name'],
                                    'Capacity' => $table['seat_limit'],
                                    'Sort Key' => $table['table_no'],
                                    'Status' => ($table['is_active'] == 1) ? 'Active' : 'Inactive',
                                );
                                $json_data = json_encode($arr_log);

                                $str = "INSERT INTO " . CONFIG_DBN . ".cfclasstable SET tablename=:tablename,
                                                                                    classunkid=:classunkid,
                                                                                    capacity=:capacity,
                                                                                    sortkey=:sortkey,
                                                                                    createddatetime=:createddatetime,
                                                                                    created_user=:created_user,
                                                                                    is_active=:is_active,
                                                                                    hashkey=:hashkey,
                                                                                    companyid=:companyid,
                                                                                    locationid=:locationid,
                                                                                    tabinstaid=:tabinstaid
                                                                                    ";
                                $dao->initCommand($str);
                                $dao->addParameter(':tablename', $table['name']);
                                $dao->addParameter(':classunkid', $clspkid);
                                $dao->addParameter(':capacity', $table['seat_limit']);
                                $dao->addParameter(':sortkey', $table['table_no']);
                                $dao->addParameter(':createddatetime', $table['added_on']);
                                $dao->addParameter(':created_user', CONFIG_UID);
                                $dao->addParameter(':is_active', $table['is_active']);
                                $dao->addParameter(':hashkey', $hashkey);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addParameter(':locationid', CONFIG_LID);
                                $dao->addParameter(':tabinstaid', $table['table_id']);
                                $dao->executeNonQuery();
                                $ObjAuditDao->addactivitylog($modulename, $title, $hashkey, $json_data);
                            }

                        }
                    }
                    else
                        {
                            $ti = "Sync from tabinsta(update)";
                        $str = "UPDATE " . CONFIG_DBN . ". cfclass SET
                                                                classname=:classname,
                                                                modifieddatetime=:modifieddatetime,
                                                                modified_user=:modified_user,
                                                                is_active=:is_active
                                                                WHERE companyid=:companyid AND locationid=:locationid AND tabinstaid=:tabinstaid ";
                        $dao->initCommand($str);
                        $dao->addParameter(':classname', $class['name']);
                        $dao->addParameter(':modifieddatetime', $class['modified_on']);
                        $dao->addParameter(':modified_user', CONFIG_UID);
                        $dao->addParameter(':is_active', $class['is_active']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':locationid', CONFIG_LID);
                        $dao->addParameter(':tabinstaid', $class['tabletype_id']);
                        $dao->executeNonQuery();
                        $ObjAuditDao->addactivitylog($mn, $ti, $classid['hashkey'], $json_data);

                        $tabledata = $class['tables'];

                        foreach ($tabledata AS $keyh => $value) {
                            $title = "Sync from tabinsta(update)";

                            $arr_log = array(
                                'Table Name' => $value['name'],
                                'Capacity' => $value['seat_limit'],
                                'Sort Key' => $value['table_no'],
                                'Status' => ($value['is_active'] == 1) ? 'Active' : 'Inactive',
                            );
                            $json_data = json_encode($arr_log);


                            $str = "SELECT tableunkid,hashkey,tabinstaid FROM " . CONFIG_DBN . ".cfclasstable WHERE tabinstaid=:tabinstaid AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid";
                            $dao->initCommand($str);
                            $dao->addParameter(':tabinstaid', $value['table_id']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addParameter(':locationid', CONFIG_LID);
                            $tbl = $dao->executeRow();

                            if (isset($tbl['tabinstaid']) == $value['table_id']) {



                                $str = "UPDATE " . CONFIG_DBN . ". cfclasstable SET tablename=:tablename,
                                                                                classunkid=:classunkid,
                                                                                capacity=:capacity,
                                                                                sortkey=:sortkey,
                                                                                modifieddatetime=:modifieddatetime,
                                                                                modified_user=:modified_user,
                                                                                is_active=:is_active
                                                                                WHERE companyid=:companyid AND locationid=:locationid AND tabinstaid=:tabinstaid ";
                                $dao->initCommand($str);
                                $dao->addParameter(':tablename', $value['name']);
                                $dao->addParameter(':classunkid', $classid['classunkid']);
                                $dao->addParameter(':capacity', $value['seat_limit']);
                                $dao->addParameter('sortkey', $value['table_no']);
                                $dao->addParameter(':modifieddatetime', $value['modified_on']);
                                $dao->addParameter(':modified_user', CONFIG_UID);
                                $dao->addParameter(':is_active', $value['is_active']);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addParameter(':locationid', CONFIG_LID);
                                $dao->addParameter(':tabinstaid', $tbl['tabinstaid']);
                                $dao->executeNonQuery();
                                $ObjAuditDao->addactivitylog($modulename, $title, $tbl['hashkey'], $json_data);
                            }
                            else
                            {
                                $title = "Sync from tabinsta(insert)";
                                $hashkey = \util\util::gethash();
                                $str = "INSERT INTO " . CONFIG_DBN . ".cfclasstable SET tablename=:tablename,
                                                                                    classunkid=:classunkid,
                                                                                    capacity=:capacity,
                                                                                    sortkey=:sortkey,
                                                                                    createddatetime=:createddatetime,
                                                                                    created_user=:created_user,
                                                                                    is_active=:is_active,
                                                                                    hashkey=:hashkey,
                                                                                    companyid=:companyid,
                                                                                    locationid=:locationid,
                                                                                    tabinstaid=:tabinstaid
                                                                                    ";
                                $dao->initCommand($str);
                                $dao->addParameter(':tablename', $value['name']);
                                $dao->addParameter(':classunkid', $classid['classunkid']);
                                $dao->addParameter(':capacity', $value['seat_limit']);
                                $dao->addParameter(':sortkey', $value['table_no']);
                                $dao->addParameter(':createddatetime', $value['added_on']);
                                $dao->addParameter(':created_user', CONFIG_UID);
                                $dao->addParameter(':is_active', $value['is_active']);
                                $dao->addParameter(':hashkey', $hashkey);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addParameter(':locationid', CONFIG_LID);
                                $dao->addParameter(':tabinstaid', $value['table_id']);
                                $dao->executeNonQuery();
                                $ObjAuditDao->addactivitylog($modulename, $title, $hashkey, $json_data);
                            }
                        }
                    }
                }

                return json_encode(array("Success"=>"True","Message"=>"Table Sync Successfully"));
            }
            else
            {
                return  json_encode(array("Success"=>"False","Message"=>"Table not available"));
            }

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-updatetablefromtabinsta-'.$e);
        }
    }







}
 ?>