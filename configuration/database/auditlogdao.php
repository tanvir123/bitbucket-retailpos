<?php

namespace database;

use function PHPSTORM_META\type;

class auditlogdao
{
    public $module = 'DB_auditlogdao';
    public $log;
    private $lang_arr, $lang_activity;
    private $language, $default_lang_arr, $defaultlanguageArr;


    function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_displaysettings');

        $this->mongo_con = new \MongoDB\Driver\Manager(CONFIG_MONGO_HOST);

    }

    public function addactivitylog($module, $title, $id, $json)
    {
        try {
            $this->log->logIt($this->module . ' - addactivitylog');
            $ObjUtil = new \util\util;
            $ObjStaticArray = new \common\staticarray();
            $ObjCommonDao = new \database\commondao();
            $localdatetime = $ObjUtil->getLocalDateTime_ISO();
            $userip = $ObjUtil->VisitorIP();
            $m = $this->mongo_con;
            $masterfield = $ObjStaticArray->auditlogmodules[$module]['masterfield'];
            $table = $ObjStaticArray->auditlogmodules[$module]['table'];
            $ObjStaticArray = new \common\staticarray();
            if (in_array($module, $ObjStaticArray->company_modules['store_modules'])) {
                $object = $ObjCommonDao->getprimaryBycompany($table, $id, $masterfield);
            } else {
                $object = $ObjCommonDao->getprimarykey($table, $id, $masterfield);
            }

            if ($table != "" && $title != "" && isset($m)) {
                if (CONFIG_LOGINTYPE == 2) {
                    $locationid = CONFIG_SID;
                } else {
                    $locationid = CONFIG_LID;
                }
                $array = array(
                    "module" => $module,
                    "tablename" => $table,
                    "object" => $object,
                    "userid" => CONFIG_UID,
                    "username" => CONFIG_UNM,
                    "unkid" => $id,
                    "title" => $title,
                    "jsondata" => $json,
                    "currenttime" => $localdatetime,
                    "companyid" => CONFIG_CID,
                    "locationid" => $locationid,
                    "userip" => $userip);
                $bulk = new \MongoDB\Driver\BulkWrite;
                $bulk->insert($array);
                $result = $m->executeBulkWrite(CONFIG_DBN . ".activity_logs", $bulk);
                return 1;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addactivitylog - ' . $e);
        }
    }

    public function loadindividualauditlogs($id = "", $module = "", $masterfield = "", $pd = "", $Old_lang_arr = "", $NewlanguageArr = "", $default_dis_languageArr = "")
    {
        try {
            $this->log->logIt($this->module . ' - loadindividualauditlogs');
            if ($module != "" && $masterfield != "" && $pd != "") {
                $dateformat = \database\parameter::getParameter('dateformat');
                $timeformat = \database\parameter::getParameter('timeformat');
                $con = $this->mongo_con;
                if (CONFIG_LOGINTYPE == 2) {
                    $locationid = CONFIG_SID;
                } else {
                    $locationid = CONFIG_LID;
                }
                $filter = [
                    'tablename' => $module,
                    'companyid' => CONFIG_CID,
                    "locationid" => $locationid,
                ];
                if ($id != "") {
                    $filter['unkid'] = $id;
                }
                $options = [
                    'sort' => ['currenttime' => -1],
                ];
                $cnt = 0;
                $query = new \MongoDB\Driver\Query($filter, $options);
                $cursor = $con->executeQuery(CONFIG_DBN . '.activity_logs', $query);
                foreach ($cursor as $document) {
                    $title = $document->title;
                    if ($document->title == 'Add Record') {
                        $title = $default_dis_languageArr['ADD_RECORD'];
                    } elseif ($document->title == 'Edit Record') {
                        $title = $default_dis_languageArr['EDIT_RECORD'];
                    } elseif ($document->title == 'Delete Record') {
                        $title = $default_dis_languageArr['DEL_RECORD'];
                    } elseif ($document->title == 'Change Status') {
                        $title = $default_dis_languageArr['CHANGE_STATUS'];
                    }
                    $resObj[$cnt]['date'] = gmdate($dateformat, $document->currenttime);
                    $resObj[$cnt]['time'] = gmdate($timeformat, $document->currenttime);
                    $resObj[$cnt]['objct'] = $document->object;
                    $resObj[$cnt]['title'] = $title;
                    $resObj[$cnt]['uname'] = $document->username;
                    $resObj[$cnt]['userip'] = $document->userip;
                    $info_arr = (array)json_decode($document->jsondata);
                    $new_array = [];
                    if ($info_arr) {
                        foreach ($info_arr as $key => $val) {
                            if ($key == "Status") {
                                if ($val == "Active") {
                                    $dis_status = $default_dis_languageArr['ACTIVE'];
                                } else {
                                    $dis_status = $default_dis_languageArr['INACTIVE'];
                                }
                                $status_dis = $default_dis_languageArr['STATUS'];
                                $new_array[$status_dis] = $dis_status;
                            } elseif ($key == "Old Value") {
                                if ($val == "Active") {
                                    $dis_status = $default_dis_languageArr['ACTIVE'];
                                } else {
                                    $dis_status = $default_dis_languageArr['INACTIVE'];
                                }
                                $status_dis = $default_dis_languageArr['OLD_VAL'];
                                $new_array[$status_dis] = $dis_status;
                            } elseif ($key == "New Value") {
                                if ($val == "Active") {
                                    $dis_status = $default_dis_languageArr['ACTIVE'];
                                } else {
                                    $dis_status = $default_dis_languageArr['INACTIVE'];
                                }
                                $status_dis = $default_dis_languageArr['NEW_VAL'];
                                $new_array[$status_dis] = $dis_status;
                            } else {
                                $old_key = array_search($key, $Old_lang_arr, true);
                                if ($old_key) {
                                    $new_key_name = $NewlanguageArr->$old_key;
                                    $new_array[$new_key_name] = $info_arr[$key];
                                }
                            }
                        }
                    }
                    $resObj[$cnt]['info'] = $new_array;
                    $cnt++;
                }
                return json_encode(array("Success" => "True", "Data" => isset($resObj)?$resObj:""));
            } else {
                return json_encode(array("Success" => "False", "Data" => ""));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - loadindividualauditlogs - ' . $e);
        }
    }

    public function addsettingslog($keyname, $old_value, $new_value)
    {
        try {
            $this->log->logIt($this->module . ' - addactivitylog');

            $ObjUtil = new \util\util;

            // $objCommondao = new \database\commondao();
            $localdatetime = $ObjUtil->getLocalDateTime_ISO();
            $userip = $ObjUtil->VisitorIP();
            $m = $this->mongo_con;
            $dao = new \dao();

            if ($keyname != "" && isset($m)) {
                if (CONFIG_LOGINTYPE == 2) {
                    $locationid = CONFIG_SID;
                } else {
                    $locationid = CONFIG_LID;
                }
                /*if ($keyname == "payment_type") {
                    $old_value = $objCommondao->getFieldValue('cfpaymenttype', 'paymenttype', 'paymenttypeunkid', $old_value);
                    $new_value = $objCommondao->getFieldValue('cfpaymenttype', 'paymenttype', 'paymenttypeunkid', $new_value);
                }*/
                /* if ($keyname == "nationality" || $keyname == "country") {
                     $old_value = $objCommondao->getFieldValue('vwcountry', 'countryName', 'id', $old_value);
                     $new_value = $objCommondao->getFieldValue('vwcountry', 'countryName', 'id', $new_value);
                 }*/
                $array = array(
                    "keyname" => $keyname,
                    "oldvalue" => $old_value,
                    "newvalue" => $new_value,
                    "userid" => CONFIG_UID,
                    "username" => CONFIG_UNM,
                    "companyid" => CONFIG_CID,
                    "locationid" => $locationid,
                    "currenttime" => $localdatetime,
                    "userip" => $userip
                );
                $bulk = new \MongoDB\Driver\BulkWrite;
                $bulk->insert($array);
                $result = $m->executeBulkWrite(CONFIG_DBN . ".settings_logs", $bulk);
                return 1;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addactivitylog - ' . $e);
        }
    }

    public function loaddisplaysettingslogs($module = "", $keyname = "")
    {
        try {
            $this->log->logIt($this->module . ' - loaddisplaysettingslogs');
            $this->loadLang();
            $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
            $defaultlanguageArr = $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            if ($module != "" && $keyname != "") {
                $resObj = array();
                $ObjCommonDao = new \database\commondao();
                $dateformat = \database\parameter::getParameter('dateformat');
                $timeformat = \database\parameter::getParameter('timeformat');
                $salutation_lang_arr = \common\staticlang::$config_salutation;
                $config_invoice_template = \common\staticlang::$config_invoice_template;
                $config_Month = \common\staticlang::$config_Month;
                $con = $this->mongo_con;
                if (CONFIG_LOGINTYPE == 2) {
                    $locationid = CONFIG_SID;
                } else {
                    $locationid = CONFIG_LID;
                }
                $filter = [
                    'keyname' => $keyname,
                    'companyid' => CONFIG_CID,
                    "locationid" => $locationid,
                ];
                $options = [
                    'sort' => ['currenttime' => -1],
                ];
                $cnt = 0;
                $query = new \MongoDB\Driver\Query($filter, $options);
                $cursor = $con->executeQuery(CONFIG_DBN . '.settings_logs', $query);
                $check_key = array("nationality", "country", "fncialyearstartmonth", "invoice",
                    "usermandatoryinfo", "payment_type", "salutation", "visibility_field", "closefolioonsettlement",
                    "display_unit_selection", "printonfinishorder", "monthly_statistics","item_display_type_in_menu","finish_and_print_print_kot_or_invoice", "consolidatemenuitemoption","display_unit_selection_for_combo","kot_print_preview_enable","kot_cancel_item_display","display_table_class_in_kot","refund_payment_and_show_refundorders","bill_to_company_account","item_replacement_option_for_combo_item","cancel_order_kot_generate","takeaway_nocharge_user_available","genrate_invoice_for_nochargeuser","cashdrawer_popup_at_login_logouttime",
                    "table_netlock","daily_revenue", "item_sale_statistics", "daily_satistics", "payment_type_report", "gratuity_tax_type","nochargeuser_report","complementary_report","gratuity_report","cashdrawer_report","combo_report","waiter_wise_sale_report",
                    "gratuity_tax_detail","use_multicurrency_on_settlement","waiting_list"
                );
                foreach ($cursor as $document) {
                    if (!in_array($keyname, $check_key)) {
                        $old_value = $document->oldvalue;
                        $new_value = $document->newvalue;

                    }
                    if ($keyname == "nationality" || $keyname == "country") {
                        $old_value = $ObjCommonDao->getFieldValue('vwcountry', 'countryName', 'id', $document->oldvalue);
                        $new_value = $ObjCommonDao->getFieldValue('vwcountry', 'countryName', 'id', $document->newvalue);
                    }
                    if ($keyname == "usermandatoryinfo" || $keyname == "visibility_field") {
                        if ($document->oldvalue != '' || $document->newvalue != '') {
                            $old_value = $ObjCommonDao->getusermandatoryfields($document->oldvalue);
                            $new_value = $ObjCommonDao->getusermandatoryfields($document->newvalue);
                        }
                    }
                    if ($keyname == "payment_type") {
                        $old_value = $ObjCommonDao->getFieldValue('cfpaymenttype', 'paymenttype', 'paymenttypeunkid', $document->oldvalue);
                        $new_value = $ObjCommonDao->getFieldValue('cfpaymenttype', 'paymenttype', 'paymenttypeunkid', $document->newvalue);
                    }
                    if ($keyname == "salutation") {
                        $old_value = $salutation_lang_arr[$document->oldvalue];
                        $new_value = $salutation_lang_arr[$document->newvalue];
                    }
                    if ($keyname == "invoice") {
                        $old_value = $config_invoice_template[$document->oldvalue];
                        $new_value = $config_invoice_template[$document->newvalue];
                    }
                    if ($keyname == "fncialyearstartmonth") {
                        $old_value = $config_Month[$document->oldvalue];
                        $new_value = $config_Month[$document->newvalue];
                    }
                    if ($keyname == "closefolioonsettlement" || $keyname == 'display_unit_selection' || $keyname == 'printonfinishorder' || $keyname == 'takeaway_nocharge_user_available' || $keyname == 'genrate_invoice_for_nochargeuser' || $keyname == 'cashdrawer_popup_at_login_logouttime' || $keyname == 'table_netlock' || $keyname == 'kot_print_preview_enable' || $keyname == 'item_display_type_in_menu' ||  $keyname == 'finish_and_print_print_kot_or_invoice' || $keyname == 'kot_cancel_item_display' || $keyname == 'display_table_class_in_kot' || $keyname == 'refund_payment_and_show_refundorders' || $keyname == 'bill_to_company_account' || $keyname == 'display_unit_selection_for_combo' || $keyname == 'item_replacement_option_for_combo_item' || $keyname == 'cancel_order_kot_generate'
                        || $keyname == 'consolidatemenuitemoption' || $keyname == 'monthly_statistics' || $keyname == 'daily_revenue'
                        || $keyname == 'item_sale_statistics' || $keyname == 'daily_satistics' || $keyname == 'payment_type_report' || $keyname == 'kot_display_with_other_info' || $keyname == 'is_preorder' || $keyname == 'waiting_list' || $keyname == 'use_multicurrency_on_settlement' || $keyname == 'nochargeuser_report' || $keyname == 'complementary_report' || $keyname == 'gratuity_report' || $keyname == 'cashdrawer_report' || $keyname == 'combo_report' || $keyname == 'waiter_wise_sale_report') {
                        if ($document->oldvalue == 0) {
                            $old_value = $defaultlanguageArr->NO;
                        } else {
                            $old_value = $defaultlanguageArr->YES;
                        }
                        if ($document->newvalue == 0) {
                            $new_value = $defaultlanguageArr->NO;
                        } else {
                            $new_value = $defaultlanguageArr->YES;
                        }
                    }
                    if ($keyname == "gratuity_tax_type") {
                        if ($document->oldvalue == 1) {
                            $old_value = $languageArr->LANG38;
                        } else {
                            $old_value = $languageArr->LANG37;
                        }
                        if ($document->newvalue == 1) {
                            $new_value = $languageArr->LANG38;
                        } else {
                            $new_value = $languageArr->LANG37;
                        }
                    }

                    if ($keyname == "choose_kot_format") {
                        if ($document->oldvalue == 2) {
                            $old_value = $languageArr->LANG58;
                        } elseif ($document->oldvalue == 3) {
                            $old_value = $languageArr->LANG59;
                        } else {
                            $old_value = $languageArr->LANG57;
                        }

                        if ($document->newvalue == 2) {
                            $new_value = $languageArr->LANG58;
                        }elseif ($document->newvalue == 3) {
                            $new_value = $languageArr->LANG59;
                        } else {
                            $new_value = $languageArr->LANG57;
                        }
                    }
                    if ($keyname == "choose_rate") {
                        if ($document->oldvalue == 2) {
                            $old_value = $languageArr->LANG88;
                        } elseif ($document->oldvalue == 3) {
                            $old_value = $languageArr->LANG89;
                        }elseif ($document->oldvalue == 4) {
                            $old_value = $languageArr->LANG90;
                        }elseif ($document->oldvalue == 1) {
                            $old_value = $languageArr->LANG87;
                        } else {
                            $old_value = $languageArr->LANG91;
                        }

                        if ($document->newvalue == 2) {
                            $new_value = $languageArr->LANG88;
                        }elseif ($document->newvalue == 3) {
                            $new_value = $languageArr->LANG89;
                        }elseif ($document->newvalue == 1) {
                            $new_value = $languageArr->LANG90;
                        }elseif ($document->newvalue == 4) {
                            $new_value = $languageArr->LANG87;
                        } else {
                            $new_value = $languageArr->LANG91;
                        }
                    }
                    if ($keyname == "gratuity_tax_detail") {
                        $old_array['old_value'] = '';
                        $new_array['new_value'] = '';
                        if ($document->oldvalue !='') {
                            $tax_detail_old = (array)json_decode($document->oldvalue);
                            if ($tax_detail_old) {
                                foreach ($tax_detail_old as $key => $value) {
                                    $old_value = $ObjCommonDao->getFieldValue('cftax', 'tax', 'taxunkid', $key);
                                    if ($old_value != '' &&  $value->amount!='') {
                                        $old_array['old_value'] .= $old_value . "->" . $value->amount . '<br/>';
                                    }
                                }
                            }
                            if($old_array)
                            $old_value = $old_array['old_value'];
                        }
                        if ($document->newvalue !='') {
                            $tax_detail_new = (array)json_decode($document->newvalue);
                            if ($tax_detail_new) {
                                foreach ($tax_detail_new as $key2 => $value2) {
                                    $new_value = $ObjCommonDao->getFieldValue('cftax', 'tax', 'taxunkid', $key2);
                                    if ($old_value != '' && $value2->amount!='') {
                                        $new_array['new_value'] .= $new_value . "->" . $value2->amount . '<br/>';
                                    }
                                }
                            }
                            if($new_array)
                            $new_value = $new_array['new_value'];
                        }
                    }

                    $resObj[$cnt]['date'] = gmdate($dateformat, $document->currenttime);
                    $resObj[$cnt]['time'] = gmdate($timeformat, $document->currenttime);
                    $resObj[$cnt]['oldvalue'] = $old_value;
                    $resObj[$cnt]['newvalue'] = $new_value;
                    $resObj[$cnt]['uname'] = $document->username;
                    $resObj[$cnt]['userip'] = $document->userip;
                    $cnt++;
                }
                return json_encode(array("Success" => "True", "Data" => $resObj));
            } else {
                return json_encode(array("Success" => "False", "Data" => ""));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - loaddisplaysettingslogs - ' . $e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_displaysettings;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }
}

?>