<?php

namespace database;

class purchaseorderdao
{
    public $module = 'DB_purchaseorder';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function purchaselist($limit, $offset, $fromdate, $todate, $orderno, $vendorid, $statusid)
    {
        try {

            $this->log->logIt($this->module . ' - purchaselist');

            $objutil = new \util\util;
            $fromdate = $objutil->convertDateToMySql($fromdate);
            $todate = $objutil->convertDateToMySql($todate);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $round_off = \database\parameter::getParameter('digitafterdecimal');


            $strSql = "SELECT CPO.orderid,CPO.order_doc_num,CPO.order_date,IFNULL(CPO.remarks,'') AS remarks,ROUND(CPO.totalamount,$round_off) AS totalamount,TRC.business_name,CPO.authstatus, 
                        CASE 
                        WHEN CPO.status=1 THEN 'Pending'
                        WHEN CPO.status=2 THEN 'Partially Completed'                       
                        ELSE  'Completed'
                        END AS po_status,CPO.status,
                        IFNULL(DATE_FORMAT(CPO.order_date,'" . $mysqlformat . "'),'') AS order_date,IFNULL(CFU1.username,'') AS createduser,
                        IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CPO.createddatetime,'" . $mysqlformat . "'),'') AS created_date,
                        IFNULL(DATE_FORMAT(CPO.modifieddatetime,'" . $mysqlformat . "'),'') AS modified_date                                   
                        FROM " . CONFIG_DBN . ".cfpurchaseorder AS CPO
                        LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU1 ON CPO.createduser=CFU1.userunkid AND CPO.companyid=CFU1.companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU2 ON CPO.modifieduser=CFU2.userunkid AND CPO.modifieduser=CFU2.userunkid AND CPO.companyid=CFU2.companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfvandor AS CV ON CPO.lnkvendorid=CV.vandorunkid AND CV.companyid=:companyid
                        LEFT JOIN " . CONFIG_DBN . ".trcontact AS TRC ON CV.lnkcontactid=TRC.contactunkid AND TRC.companyid=:companyid
                        WHERE CPO.companyid=:companyid AND CPO.storeid=:storeid AND CPO.is_deleted=0";

            if ($orderno != "") {
                $strSql .= " AND CPO.order_doc_num LIKE '%" . $orderno . "%'";
            }
            if ($vendorid != 0) {
                $strSql .= " AND CPO.lnkvendorid =" . $vendorid . "";
            }
            if ($statusid != 0) {
                $strSql .= " AND CPO.status =" . $statusid . "";
            }
            if ($fromdate != "" && $todate != "") {
                $strSql .= " AND CPO.order_date BETWEEN  '" . $fromdate . "' AND '" . $todate . "' ";
            }
            $strSql .= " ORDER BY CPO.orderid DESC";
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }
            /*echo CONFIG_CID."<br>".CONFIG_SID;
            echo $strSql;
            exit;*/
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $data = $dao->executeQuery();
            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
                $dao->initCommand($strSql);

            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return html_entity_decode(json_encode($retvalue));
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - purchaselist - ' . $e);
        }
    }

    public function addpurchase($data,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - addpurchase - ');
            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $poarray = $data['po_items'];
            $objutil = new \util\util;
            $po_date = $objutil->convertDateToMySql($data['po_date']);

            $ObjCommonDao = new \database\commondao();

            if ($data['id'] == '0' || $data['id'] == '') {
                $po_doc_num = $ObjCommonDao->getIndentDocumentNumbering('purchase_order', 'inc');

                $sql = "INSERT into " . CONFIG_DBN . ".cfpurchaseorder SET 
                order_date=:order_date,
                order_doc_num=:order_doc_num,
                lnkvendorid=:lnkvendorid,
                companyid=:companyid,
                storeid=:storeid,
                createddatetime=:createddatetime,
                createduser=:createduser,
                totalamount=:total_amount,
                remarks=:remarks";

                $dao->initCommand($sql);
                $dao->addParameter(':order_date', $po_date);
                $dao->addParameter(':order_doc_num', $po_doc_num);
                $dao->addParameter(':lnkvendorid', $data['po_sel_vendor']);
                $dao->addParameter(':total_amount', $data['total_amount']);
                $dao->addParameter(':remarks', $data['po_remarks']);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->addParameter(':createduser', CONFIG_UID);
                $dao->executeNonQuery();
                $poId = $dao->getLastInsertedId();
                foreach ($poarray AS $key => $value) {
                    if (is_array($value['item_tax_det']) && count($value['item_tax_det']) > 0) {
                        $tax_description = html_entity_decode(json_encode($value['item_tax_det']));
                    } else {
                        $tax_description = '';
                    }

                    $sql = "INSERT into " . CONFIG_DBN . ".cfpurchaseorderdetail SET 
                            lnkorderid=:lnkorderid,lnkrawid=:lnkrawid,
                            lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,
                            qty=:qty,rate=:rate,discount_per=:discount_per,discount_amount=:discount_amount,tax_amount=:tax_amount,
                            tax_description=:tax_description,total=:total,storeid=:storeid,companyid=:companyid";
                    $dao->initCommand($sql);
                    $dao->addParameter(':lnkorderid', $poId);
                    $dao->addParameter(':lnkrawid', $value['item_id']);
                    $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                    $dao->addParameter(':lnkunitid', $value['item_unit']);
                    $dao->addParameter(':qty', $value['item_qty']);
                    $dao->addParameter(':rate', $value['item_rpu']);
                    $dao->addParameter(':discount_per', $value['item_disc_per']);
                    $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                    $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                    $dao->addParameter(':tax_description', $tax_description);
                    $dao->addParameter(':total', $value['item_total_amt']);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->executeNonQuery();
                }
                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_ADD_SUC)));
            } else {

                $strforcheckkey = "SELECT orderdetailunkid  FROM " . CONFIG_DBN . ".cfpurchaseorderdetail 
                WHERE lnkorderid=:lnkorderid AND companyid=:companyid AND storeid=:storeid";
                $dao->initCommand($strforcheckkey);
                $dao->addParameter(':lnkorderid', $data['id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $resultforidcheck = $dao->executeQuery();

                $orderdetailunkid = \util\util::getoneDarray($resultforidcheck, 'orderdetailunkid');

                $poarr = [];
                foreach ($poarray AS $key => $value) {
                    if (is_array($value['item_tax_det']) && count($value['item_tax_det']) > 0) {
                        $tax_description = html_entity_decode(json_encode($value['item_tax_det']));
                    } else {
                        $tax_description = '';
                    }


                    if (in_array($value['itm_detId'], $orderdetailunkid)) {
                        $strsql = "UPDATE " . CONFIG_DBN . ".cfpurchaseorderdetail SET 
                            lnkrawid=:lnkrawid,lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,
                            qty=:qty,rate=:rate,discount_per=:discount_per,discount_amount=:discount_amount,tax_amount=:tax_amount,
                            tax_description=:tax_description,total=:total
                            WHERE orderdetailunkid=:orderdetailunkid AND lnkorderid=:lnkorderid AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strsql);
                        $dao->addParameter(':orderdetailunkid', $value['itm_detId']);
                        $dao->addParameter(':lnkorderid', $data['id']);
                        $dao->addParameter(':lnkrawid', $value['item_id']);
                        $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                        $dao->addParameter(':lnkunitid', $value['item_unit']);
                        $dao->addParameter(':qty', $value['item_qty']);
                        $dao->addParameter(':rate', $value['item_rpu']);
                        $dao->addParameter(':discount_per', $value['item_disc_per']);
                        $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                        $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                        $dao->addParameter(':tax_description', $tax_description);
                        $dao->addParameter(':total', $value['item_total_amt']);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->executeNonQuery();
                        $poarr[] = $value['itm_detId'];
                    } else {
                        $sql = "INSERT into " . CONFIG_DBN . ".cfpurchaseorderdetail SET 
                            lnkorderid=:lnkorderid,lnkrawid=:lnkrawid,
                            lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,
                            qty=:qty,rate=:rate,discount_per=:discount_per,discount_amount=:discount_amount,tax_amount=:tax_amount,
                            tax_description=:tax_description,total=:total,storeid=:storeid,companyid=:companyid";
                        $dao->initCommand($sql);
                        $dao->addParameter(':lnkorderid', $data['id']);
                        $dao->addParameter(':lnkrawid', $value['item_id']);
                        $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                        $dao->addParameter(':lnkunitid', $value['item_unit']);
                        $dao->addParameter(':qty', $value['item_qty']);
                        $dao->addParameter(':rate', $value['item_rpu']);
                        $dao->addParameter(':discount_per', $value['item_disc_per']);
                        $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                        $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                        $dao->addParameter(':tax_description', $tax_description);
                        $dao->addParameter(':total', $value['item_total_amt']);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->executeNonQuery();
                        $poarr[] = $dao->getLastInsertedId();
                    }
                }
                $strsql = "DELETE FROM " . CONFIG_DBN . ".cfpurchaseorderdetail              
                           WHERE orderdetailunkid NOT IN ('" . implode("','", $poarr) . "') 
                           AND lnkorderid=:lnkorderid AND companyid=:companyid AND storeid=:storeid";
                $dao->initCommand($strsql);
                $dao->addParameter(':lnkorderid', $data['id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->executeNonQuery();

                $sql = "UPDATE " . CONFIG_DBN . ".cfpurchaseorder SET remarks=:remarks,
                        totalamount=:totalamount,modifieddatetime=:modifieddatetime,
                        modifieduser=:modifieduser WHERE orderid=:orderid AND companyid=:companyid AND storeid=:storeid";
                $dao->initCommand($sql
                );
                $dao->addParameter(':orderid', $data['id']);
                $dao->addParameter(':remarks', $data['po_remarks']);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':modifieduser', CONFIG_UID);
                $dao->addParameter(':totalamount', $data['total_amount']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->executeNonQuery();
                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_UP_SUC)));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addpurchase - ' . $e);
        }
    }

    public function getPoRecordsByID($data)
    {
        try {
            $this->log->logIt($this->module . " - getPoRecordsByID");
            $dao = new \dao();
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $strPO = "SELECT CPO.orderid,CPO.order_doc_num,IFNULL(CPO.remarks,'') AS remarks,ROUND(CPO.totalamount,$round_off)AS totalamount,
                     CPO.lnkvendorid,IFNULL(DATE_FORMAT(CPO.order_date,'" . $mysql_format . "'),'') AS order_date
                     FROM " . CONFIG_DBN . ".cfpurchaseorder AS CPO 
                     WHERE CPO.orderid=:orderid AND CPO.companyid=:companyid AND CPO.storeid=:storeid ";
            $dao->initCommand($strPO);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addParameter(':orderid', $data['id']);
            $resPO = $dao->executeRow();

            $strPODetail = "SELECT CR.name AS storeitem,CU.name AS unit,CU.unit as unit_val,CPD.lnkorderid,CPD.orderdetailunkid,CPD.lnkrawid,CPD.lnkrawcatid," .
                "CPD.lnkunitid,ROUND(CPD.qty,$round_off) AS qty,ROUND(CPD.rate,$round_off) AS rate," .
                "CPD.discount_per,ROUND(CPD.discount_amount,$round_off) AS discount_amount,ROUND(CPD.tax_amount,$round_off) AS tax_amount," .
                "CPD.tax_description,ROUND(CPD.total,$round_off)AS total FROM " . CONFIG_DBN . ".cfpurchaseorderdetail AS CPD " .
                "LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CPD.lnkrawid=CR.id " .
                "LEFT JOIN " . CONFIG_DBN . ".cfunit CU ON CPD.lnkunitid=CU.unitunkid AND " .
                "CU.companyid=:companyid AND CU.is_active=1 AND CU.is_deleted=0 " .
                "WHERE CPD.lnkorderid=:lnkorderid AND CPD.companyid=:companyid AND CPD.storeid=:storeid AND CPD.lnkgrndetailid=0";

            $dao->initCommand($strPODetail);
            $dao->addParameter(':lnkorderid', $data['id']);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $resPoDetail = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success" => "True", "order" => $resPO, "orderDetail" => $resPoDetail)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getPoRecordsByID - " . $e);
            return false;
        }
    }

    public function getlastpono()
    {
        try {
            $this->log->logIt($this->module . " - getlastpono");
            $dao = new \dao();
            $strSql = "SELECT prefix,startno  FROM " . CONFIG_DBN . ".cfindentnumber WHERE 
            storeid=:storeid AND keyname='purchase_order'";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_LID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $res = $dao->executeRow();
            $indent_doc_number = $res['prefix'] . $res['startno'];

            return $indent_doc_number;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getlastpono - " . $e);
            return false;
        }
    }

    public function authorize_po($data,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - authorize_po');
            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $strSql = "UPDATE " . CONFIG_DBN . ".cfpurchaseorder SET authstatus=1,authorized_user=:userid,authorize_datetime=:datetime WHERE orderid=:id AND storeid=:storeid AND companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':userid', CONFIG_UID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':id', $data['orderid']);
            $dao->addParameter(':datetime', $datetime);
            $dao->executeNonQuery();
            return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->VOUCHER_AUT_SUC)));
        } catch (\Exception $e) {
            $this->log->logIt($this->module . ' - authorize_po - ' . $e);
        }
    }

    public function getAllVendorList()
    {
        try {
            $this->log->logIt($this->module . '-getAllVendorList');
            $dao = new \dao();
            $strSql = "SELECT CFV.vandorunkid, TC.business_name FROM " . CONFIG_DBN . ".cfvandor CFV 
            INNER JOIN " . CONFIG_DBN . ".trcontact TC ON TC.contactunkid = CFV.lnkcontactid
             WHERE CFV.companyid=:companyid AND  CFV.is_active=1 AND  CFV.is_deleted=0";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $rec = $dao->executeQuery();
            return $rec;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . '-getAllVendorList -' . $e);
        }
    }

    public function getVendorCategory($data)
    {
        try {
            $this->log->logIt($this->module . ' - getVendorCategory');
            $categories = [];
            if (isset($data['vendorid']) && $data['vendorid'] > 0) {
                $dao = new \dao();
                $dao->initCommand("SELECT DISTINCT lnkcategoryid,CTRC.name AS category_name FROM " . CONFIG_DBN . ".cfrawmaterial CFR
              LEFT JOIN ".CONFIG_DBN.". cfinventorydetail ID ON CFR.id= ID.lnkrawmaterialid AND ID.lnkvendorid=:lnkvandorid
                 INNER JOIN " . CONFIG_DBN . ".cfrawmaterial_category CTRC 
				    ON CTRC.id = CFR.lnkcategoryid AND CTRC.is_deleted=0 AND CTRC.companyid = :companyid
				    WHERE  CFR.companyid = :companyid AND CFR.is_deleted=0 AND CFR.is_active=1 ORDER BY CTRC.name ASC ");
                $dao->addParameter(':lnkvandorid', $data['vendorid']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $categories = $dao->executeQuery();
            }
            return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => 'Vendor categories', 'categories' => $categories)));
        } catch (\Exception $e) {
            $this->log->logIt($this->module . ' - getVendorCategory - ' . $e);
        }
    }

    public function getRawMaterialFromCategory($data)
    {
        try {
            $this->log->logIt($this->module . " - getRawMaterialFromCategory");
            $dao = new \dao();

            $strSql = " SELECT id,name,rateperunit FROM " . CONFIG_DBN . ".cfrawmaterial
            WHERE is_active=1 AND is_deleted=0 AND lnkcategoryid=:lnkcategoryid AND companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':lnkcategoryid', $data['rawcatid']);
            $res = $dao->executeQuery();

            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getRawMaterialFromCategory - " . $e);
            return false;
        }
    }

    public function getUnitRate($data)
    {
        try {
            $this->log->logIt($this->module . " - getUnitRate");
            $dao = new \dao();
            //$round_off = \database\parameter::getParameter('digitafterdecimal');
            $strUnit = "SELECT CRUREL.lnkunitid as unitunkid,CRUREL.conversation_rate as unit,CU.name,IFNULL(CRL.rateperunit,0) AS rateperunit
                  FROM ".CONFIG_DBN.".cfrawmaterial_unit_rel as CRUREL
                  INNER JOIN ".CONFIG_DBN.".cfunit as CU on CU.unitunkid=CRUREL.lnkunitid and CU.companyid=:companyid AND CU.is_active=1 and CU.is_deleted=0
                  LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial_loc CRL ON CRL.lnkrawmaterialid=CRUREL.lnkrawid AND CRL.storeid=:storeid AND CRL.companyid=:companyid
                  WHERE CRUREL.lnkrawid=:lnkrawid AND CRUREL.companyid=:companyid";
            $dao->initCommand($strUnit);
            $dao->addParameter(':lnkrawid',$data['itemid']);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':storeid',CONFIG_SID);
            $resObj = $dao->executeQuery();

            if(!$resObj) {
                $strSql = " SELECT U.name,U.unitunkid,U.unit,IFNULL(CRL.rateperunit,0) AS rateperunit
              FROM " . CONFIG_DBN . ".cfrawmaterial CRM 
              LEFT JOIN " . CONFIG_DBN . ".cfunit U ON CRM.measuretype=U.measuretype AND U.companyid=:companyid AND U.is_active=1 AND U.is_deleted=0
              LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial_loc CRL ON CRL.lnkrawmaterialid=CRM.id AND CRL.storeid=:storeid 
              WHERE CRM.id =:id AND CRM.companyid=:companyid ";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':id', $data['itemid']);
                $resObj = $dao->executeQuery();
            }

            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $resObj)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getUnitRate - " . $e);
            return false;
        }
    }

    public function getAppliedTax()
    {
        try {
            $this->log->logIt($this->module . ' - getAppliedTax');
            $todaysdate = \util\util::getLocalDate();
            $dao = new \dao();

            $strSql = " SELECT CASE WHEN CFTD.postingrule=1 THEN '%' WHEN CFTD.postingrule=2
                       THEN 'FLAT' END AS pytype,CFTD.amount,CFT.taxunkid,CFT.lnkmasterunkid,CFTD.taxdetailunkid,CFT.tax,CFTD.postingrule,CFTD.taxapplyafter
                        FROM " . CONFIG_DBN . ".cftax AS CFT
                        INNER JOIN " . CONFIG_DBN . ".cftaxdetail CFTD
                          ON CFT.taxunkid = CFTD.taxunkid AND
                             CFTD.taxdetailunkid = (SELECT taxdetailunkid FROM " . CONFIG_DBN . ".cftaxdetail WHERE taxunkid=CFT.taxunkid ORDER BY entrydatetime DESC LIMIT 1)
                        WHERE CFT.companyid =:companyid  AND CFT.isactive=1 AND CFT.is_deleted=0 AND CAST(CFTD.taxdate AS DATE)<=:todaysdate ";
            if (defined('CONFIG_LID') && CONFIG_LID > 0) {
                $strSql .= " AND CFT.locationid = :locationid ";
            }

            if (defined('CONFIG_SID') && CONFIG_SID > 0) {
                $strSql .= " AND CFT.storeid = :storeid ";
            }

            $strSql .= " ORDER BY CFT.taxunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(':todaysdate', $todaysdate);
            $dao->addParameter(':companyid', CONFIG_CID);
            if (defined('CONFIG_LID') && CONFIG_LID > 0) {
                $dao->addparameter(':locationid', CONFIG_LID);
            }

            if (defined('CONFIG_SID') && CONFIG_SID > 0) {
                $dao->addparameter(':storeid', CONFIG_SID);
            }
            $res = $dao->executeQuery();
            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getAppliedTax - ' . $e);
        }
    }

    public function printPO($id,$defaultlanguageArr='')
    {
        try {
            $this->log->logIt($this->module . ' - printPO');
            $dao = new \dao();

            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $resarr = array();
            $strSql = "SELECT C.companyname,C.address1 AS company_address,C.phone AS company_phone,IFNULL(C.logo,'') as company_logo,
                        C.currency_sign,C.companyemail,C.fax,
                        TC.name AS vendor_name,TC.address AS vendor_address,TC.mobile AS vendor_mobile,TC.fax AS Vendor_fax,TC.email AS Vendor_email,TC.business_name AS vendor_business,
                        PO.order_doc_num AS voucher_num, PO.remarks AS po_remarks,
                         IFNULL(DATE_FORMAT(PO.order_date,'" . $mysql_format . "'),'')  AS Voucher_Date, 
                        IFNULL(CFU1.username,'') AS createduser,ROUND(IFNULL(PO.totalamount,0),$round_off) as totalamount,
                        VC.countryName AS Company_Country                        
                        FROM " . CONFIG_DBN . ".cfpurchaseorder AS PO                         
                        LEFT JOIN " . CONFIG_DBN . ".cfcompany AS C ON PO.companyid= C.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU1 ON PO.createduser=CFU1.userunkid AND PO.companyid=CFU1.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU2 ON PO.authorized_user=CFU2.userunkid AND PO.companyid=CFU2.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfvandor AS V ON PO.lnkvendorid=V.vandorunkid AND V.companyid=PO.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".trcontact AS TC ON V.lnkcontactid=TC.contactunkid AND TC.companyid=V.companyid
                        LEFT JOIN " . CONFIG_DBN . ".vwcountry AS VC ON C.country=VC.id                       
                        WHERE PO.companyid=:companyid AND PO.orderid=:orderid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':orderid', $id);
            $porecords = $dao->executeRow();

            $resarr['company_name'] = $porecords['companyname'];
            $resarr['address'] = $porecords['company_address'];
            $resarr['phone'] = $porecords['company_phone'];
            $resarr['email'] = $porecords['companyemail'];
            $resarr['company_logo'] = !empty($porecords['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$porecords['company_logo']:'';
            $resarr['fax'] = $porecords['fax'];
            $resarr['Company_Country'] = $porecords['Company_Country'];
            $resarr['currency_sign'] = $porecords['currency_sign'];
            $resarr['voucher_num'] = $porecords['voucher_num'];
            $resarr['remarks'] = $porecords['po_remarks'];
            $resarr['Voucher_Date'] = $porecords['Voucher_Date'];
            $resarr['totalamount'] = $porecords['totalamount'];
            $resarr['vendor_name'] = $porecords['vendor_name'];
            $resarr['vendor_address'] = $porecords['vendor_address'];
            $resarr['vendor_mobile'] = $porecords['vendor_mobile'];
            $resarr['vendor_business'] = $porecords['vendor_business'];
            $resarr['vendor_fax'] = $porecords['Vendor_fax'];
            $resarr['vendor_email'] = $porecords['Vendor_email'];
            $resarr['createduser'] = $porecords['createduser'];
            $resarr['current_date'] = \util\util::convertMySqlDate($current_date);

            $strPODetail = "SELECT CR.name AS storeitem,CU.shortcode AS unit,CPD.lnkorderid,CPD.orderdetailunkid,CPD.lnkrawid,CPD.lnkrawcatid," .
                "CPD.lnkunitid,ROUND(CPD.qty,$round_off) AS qty,ROUND(CPD.rate,$round_off) AS rate," .
                "CPD.discount_per,ROUND(CPD.discount_amount,$round_off) AS discount_amount,ROUND(CPD.tax_amount,$round_off) AS tax_amount," .
                "CPD.tax_description,ROUND(CPD.total,$round_off) AS total FROM " . CONFIG_DBN . ".cfpurchaseorderdetail AS CPD " .
                "LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CPD.lnkrawid=CR.id " .
                "LEFT JOIN " . CONFIG_DBN . ".cfunit CU ON CPD.lnkunitid=CU.unitunkid AND " .
                "CU.companyid=:companyid AND CU.is_active=1 AND CU.is_deleted=0 " .
                "WHERE CPD.lnkorderid=:lnkorderid AND CPD.companyid=:companyid AND CPD.storeid=:storeid";
            $dao->initCommand($strPODetail);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addparameter(':lnkorderid', $id);
            $podetail = $dao->executeQuery();

            $taxTOtal = 0;

            if ($podetail) {
                foreach ($podetail as $key => $value) {
                    $taxname = array();
                    $potaxdetail = json_decode($value['tax_description'], 1);
                    $taxeslist_dis ='';
                    if($potaxdetail)
                    {
                        foreach ($potaxdetail as $key2 => $value2) {
                            $Tax_NAME ='';
                            $posting_type=isset($porecords['currency_sign'])?$porecords['currency_sign']:'Flat';
                            if(empty($value2['tax_name']))
                            {
                                $str = "SELECT tax FROM " . CONFIG_DBN . ".cftax
                            WHERE  companyid=:companyid AND storeid=:storeid  AND taxunkid=:taxunkid";
                                $dao->initCommand($str);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addParameter(':storeid', CONFIG_SID);
                                $dao->addParameter(':taxunkid', $value2['tax_id']);
                                $qtaxname = $dao->executeRow();
                                $Tax_NAME=$qtaxname['tax'];
                            }
                            else{
                                $Tax_NAME=$value2['tax_name'];
                            }
                            if(isset($value2['posting_rule']) && $value2['posting_rule'] == 1)
                            {
                                $posting_type='%';
                            }
                            $taxeslist_dis .= "<b>".$Tax_NAME."</b> : (".number_format($value2['tax_value'],$round_off,'.','')." ".$posting_type.") : ".number_format($value2['tax_amt'], $round_off,'.','')." <br>";
                            $taxname[$value2['tax_id']] = $value2['tax_amt'];
                        }
                    }
                    $taxTOtal = ($taxTOtal+$value['tax_amount']);
                    $podetail[$key]['tax_description'] = isset($taxeslist_dis)?$taxeslist_dis:'';
                }
            }

            $resarr['podetail'] = $podetail;
            $resarr['Tax_Total'] =  number_format($taxTOtal,$round_off,'.','');

            $Discounttotal = $Dto = 0;
            foreach ($podetail AS $val) {

                $to = $val['discount_amount'];
                $Discounttotal += $to;
            }

            $resarr['Discounttotal'] = number_format($Discounttotal,$round_off,'.','');

            /*User : Nikita. Date : 14/07/2018
             Reason :  hide total in words because after point values were not proper.
             filename -> reports/tmpl/PO.html
            $numberinwords = \util\util::convertNumber($porecords['totalamount'],'','','',$defaultlanguageArr);
           $resarr['numberinwords'] = $numberinwords;*/

            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - printPO - ' . $e);
        }
    }

}

?>

