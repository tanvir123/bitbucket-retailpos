<?php
namespace database;

class menu_modifierdao
{
    public $module = 'DB_menu_modifierdao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function addmodifier($data,$defaultlanguageArr,$languageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - addmodifier');
            $dao = new \dao();
            $datetime=\util\util::getLocalDateTime();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $tablename =  "cfmenu_modifiers";
            $ObjDependencyDao = new \database\dependencydao();
            $arr_log = array(
                'Modifier Name'=>$data['name'],
                'Min'=>$data['min'],
                'Max'=>$data['max'],
                'SKU'=>$data['sku'],
                'Long Description'=>$data['txtlongdesc'],
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive',
            );
            $json_data = json_encode($arr_log);
            if($data['id']=='0' || $data['id']==0)
            {   
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"modifiername",$data['name']);
                if($chk_name==1)
                {
                    return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->RECORD_ALREADY_EXISTS));
                }
                $chk_sku = $ObjDependencyDao->checkduplicaterecord($tablename,"sku",$data['sku']);
                if($chk_sku==1)
                {
                    return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG3));
                }
                $title = "Add Record";
                $hashkey = \util\util::gethash();
                $strSql = "INSERT INTO ".CONFIG_DBN.".cfmenu_modifiers
                                                    (
                                                    companyid,
                                                    locationid,
                                                    modifiername,
                                                    min,
                                                    max,
                                                    long_desc,
                                                    image,
                                                    sku,
                                                    sale_amount,
                                                    is_included,
                                                    is_active,
                                                    createddatetime,
                                                    created_user,
                                                    hashkey,
                                                    tax_apply_rate
                                                    )".
                                             " VALUE(
                                             :companyid,
                                             :locationid,
                                             :txtname,
                                             :txtmin,
                                             :txtmax,
                                             :txtlongdesc,
                                             :upload_img,
                                             :txtsku,                                             
                                             :txtsaleamount,
                                             :is_included,
                                             :is_active,
                                             :createddatetime,
                                             :created_user,
                                             :hashkey,
                                             :rates
                                             )";                
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->addParameter(':txtname',$data['name']);
                $dao->addParameter(':txtmin',$data['min']);
                $dao->addParameter(':txtmax',$data['max']);
                $dao->addParameter(':txtlongdesc',$data['txtlongdesc']);
                $dao->addParameter(':upload_img', ($data['image'] != '' ? $data['image'] : NULL));
                $dao->addParameter(':txtsku',$data['sku']);
                $dao->addParameter(':is_included',$data['is_included']);
                $dao->addParameter(':txtsaleamount',$data['txtsaleamount']);
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':created_user',CONFIG_UID);
                $dao->addParameter(':is_active',$data['rdo_status']);
                $dao->addParameter(':hashkey',$hashkey);
                $dao->addParameter(':rates',$data['rates']);
                $dao->executeNonQuery();
                $modifierunkid = $dao->getLastInsertedId();

               //Bind unit
                if(count($data['bind_unit'])>0)
                {
                    $bind_unit = $data['bind_unit'];

                    foreach($bind_unit AS $ukey=>$sub_unit) {

                        $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu_itemunit_rel(lnkitemid,lnkitemunitid,rate1,rate2,rate3,rate4,default_rate,created_user, createddatetime,recipe_type,companyid,locationid)";
                        $strSql .= " VALUE (:lnkitemid, :lnkitemunitid, :rate1,:rate2,:rate3,:rate4,:defaultrate,:created_user, :createddatetime,:recipe_type,:companyid, :locationid)";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':lnkitemid', $modifierunkid);
                        $dao->addParameter(':lnkitemunitid', $ukey);
                        $dao->addParameter(':rate1', $sub_unit['rate1']);
                        $dao->addParameter(':rate2', $sub_unit['rate2']);
                        $dao->addParameter(':rate3', $sub_unit['rate3']);
                        $dao->addParameter(':rate4', $sub_unit['rate4']);
                        $dao->addParameter(':defaultrate', $sub_unit['defaultrate']);
                        $dao->addParameter(':created_user', CONFIG_UID);
                        $dao->addParameter(':createddatetime', $datetime);
                        $dao->addParameter(':recipe_type', '2');
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                        $relunitid = $dao->getLastInsertedId();

                        if(isset($sub_unit['recipe']) && count($sub_unit['recipe'])>0)
                        {
                            $bind_recipe = $sub_unit['recipe'];
                            foreach ($bind_recipe AS $rkey => $sub_recipe) {

                                $unit = $sub_recipe['unitval'] * $sub_recipe['quantity'];

                                $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu_item_recipe (lnkitemid,lnkrawmaterialid,unit,lnkunitid,lnkstoreid,recipe_type,lnkitemunitid,created_user, createddatetime, companyid,locationid)";
                                $strSql .= "VALUE (:lnkitemid, :lnkrawmaterialid, :unit,:lnkunitid,:lnkstoreid,:recipe_type, :lnkitemunitid,:created_user, :createddatetime, :companyid, :locationid)";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':lnkitemid', $modifierunkid);
                                $dao->addParameter(':lnkrawmaterialid', $rkey);
                                $dao->addParameter(':unit', $unit);
                                $dao->addParameter(':lnkunitid', $sub_recipe['unit']);
                                $dao->addParameter(':lnkstoreid', $sub_recipe['store']);
                                $dao->addParameter(':recipe_type', '2');
                                $dao->addParameter(':lnkitemunitid', $relunitid);
                                $dao->addParameter(':created_user', CONFIG_UID);
                                $dao->addParameter(':createddatetime', $datetime);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $dao->executeNonQuery();
                        }
                    }

                    }
                }
                //

                //Bind Tax
                if(isset($data['bind_tax']) && count($data['bind_tax'])>0 && $data['bind_tax']!='') {
                    $bind_tax = $data['bind_tax'];

                    foreach($bind_tax AS $tkey=>$tvalue) {
                        if($tvalue['taxid']!='') {
                            $strSqlTax = "SELECT postingrule FROM ".CONFIG_DBN.".cftaxdetail WHERE taxunkid=:taxunkid ORDER BY entrydatetime DESC LIMIT 1";
                            $dao->initCommand($strSqlTax);
                            $dao->addParameter(':taxunkid', $tvalue['taxid']);
                            $tax_pr=$dao->executeRow();

                            $strSql = "INSERT INTO " . CONFIG_DBN . ".cfitemtax_rel(lnkitemid,lnktaxunkid,amount,postingrule,item_type,created_user, createddatetime,companyid,locationid)";
                            $strSql .= " VALUE (:lnkitemid, :lnktaxunkid, :amount,:postingrule,:item_type,:created_user, :createddatetime,:companyid, :locationid)";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':lnkitemid', $modifierunkid);
                            $dao->addParameter(':lnktaxunkid', $tkey);
                            $dao->addParameter(':amount', $tvalue['amount']);
                            $dao->addParameter(':postingrule', $tax_pr['postingrule']);
                            $dao->addParameter(':item_type', '2');
                            $dao->addParameter(':created_user', CONFIG_UID);
                            $dao->addParameter(':createddatetime', $datetime);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $dao->executeNonQuery();
                        }
                    }
                }
                //

                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                return json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_ADD_SUC));
            }
           else
           {
                $id = $ObjCommonDao->getprimarykey('cfmenu_modifiers',$data['id'],'modifierunkid');
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"modifiername",$data['name'],$id);
                if($chk_name==1)
                {
                    return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->RECORD_ALREADY_EXISTS));
                }
                $chk_sku = $ObjDependencyDao->checkduplicaterecord($tablename,"sku",$data['sku'],$id);
                if($chk_sku==1)
                {
                    return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG3));
                }
                $title = "Edit Record";
                $strSql = "UPDATE ".CONFIG_DBN.".cfmenu_modifiers SET
                                                        modifiername=:txtname,
                                                        min=:txtmin,
                                                        max=:txtmax,
                                                        long_desc=:txtlongdesc,
                                                        image=:upload_img,
                                                        sku=:txtsku, 
                                                        sale_amount=:txtsaleamount,
                                                        is_included=:is_included,
                                                        tax_apply_rate=:rates,
                                                        modifieddatetime=:modifieddatetime,
                                                        modified_user=:modified_user
                                                        WHERE
                                                        hashkey=:hashkey
                                                        AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':hashkey',$data['id']);
                $dao->addParameter(':txtname',$data['name']);
                $dao->addParameter(':txtmin',$data['min']);
                $dao->addParameter(':txtmax',$data['max']);
                $dao->addParameter(':txtlongdesc',$data['txtlongdesc']);
                $dao->addParameter(':upload_img', ($data['image'] != '' ? $data['image'] : NULL));
                $dao->addParameter(':txtsku',$data['sku']);
                $dao->addParameter(':is_included',$data['is_included']);
                $dao->addParameter(':txtsaleamount',$data['txtsaleamount']);
                $dao->addParameter(':rates',$data['rates']);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();
                $modifierid = $ObjCommonDao->getprimarykey('cfmenu_modifiers',$data['id'],'modifierunkid');

               //Bind Tax
               if(isset($data['bind_tax']) && count($data['bind_tax'])>0 && $data['bind_tax']!='') {
                   $strSql = "SELECT itmtaxrelid FROM ".CONFIG_DBN.".cfitemtax_rel WHERE lnkitemid=:lnkitemid AND item_type=:item_type AND companyid=:companyid AND locationid=:locationid";
                   $dao->initCommand($strSql);
                   $dao->addParameter(':lnkitemid',$modifierid);
                   $dao->addParameter(':item_type', '2');
                   $dao->addParameter(':companyid',CONFIG_CID);
                   $dao->addparameter(':locationid',CONFIG_LID);
                   $particuler_tax = $dao->executeQuery();

                   $tax_arr = \util\util::getoneDarray($particuler_tax,'itmtaxrelid');
                   $bind_tax = $data['bind_tax'];

                   if(count($bind_tax)>0) {
                       foreach ($bind_tax AS $tkey => $tvalue) {
                           if (isset($tvalue['taxid']) && $tvalue['taxid'] != '') {
                           $strSqlTax = "SELECT postingrule FROM ".CONFIG_DBN.".cftaxdetail WHERE taxunkid=:taxunkid  ORDER BY entrydatetime DESC LIMIT 1";
                           $dao->initCommand($strSqlTax);
                           $dao->addParameter(':taxunkid', $tvalue['taxid']);
                           $tax_pr=$dao->executeRow();

                               $strSql = "SELECT itmtaxrelid FROM " . CONFIG_DBN . ".cfitemtax_rel WHERE lnkitemid=:lnkitemid AND lnktaxunkid=:lnktaxunkid AND item_type=:item_type AND companyid=:companyid AND locationid=:locationid";
                               $dao->initCommand($strSql);
                               $dao->addParameter(':lnkitemid', $modifierid);
                               $dao->addParameter(':lnktaxunkid', $tvalue['taxid']);
                               $dao->addParameter(':item_type', '2');
                               $dao->addParameter(':companyid', CONFIG_CID);
                               $dao->addparameter(':locationid', CONFIG_LID);
                               $particuler_tax = $dao->executeRow();

                               if (in_array($particuler_tax['itmtaxrelid'], $tax_arr)) {

                                   $strSql = "UPDATE " . CONFIG_DBN . ".cfitemtax_rel SET amount=:amount,postingrule=:postingrule,
                                           item_type=:item_type,modified_user=:modified_user, modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND item_type=:item_type AND lnktaxunkid=:lnktaxunkid
                                           AND locationid=:locationid AND lnkitemid=:lnkitemid";
                                   $dao->initCommand($strSql);
                                   $dao->addParameter(':lnkitemid', $modifierid);
                                   $dao->addParameter(':lnktaxunkid', $tvalue['taxid']);
                                   $dao->addParameter(':amount', $tvalue['amount']);
                                   $dao->addParameter(':postingrule', $tax_pr['postingrule']);
                                   $dao->addParameter(':item_type', '2');
                                   $dao->addParameter(':modified_user', CONFIG_UID);
                                   $dao->addParameter(':modifieddatetime', $datetime);
                                   $dao->addParameter(':companyid', CONFIG_CID);
                                   $dao->addparameter(':locationid', CONFIG_LID);
                                   $dao->executeNonQuery();
                                   if (($tkey = array_search($particuler_tax['itmtaxrelid'], $tax_arr)) !== false) {
                                       unset($tax_arr[$tkey]);
                                   }
                               } else {
                                   $strSql = "INSERT INTO " . CONFIG_DBN . ".cfitemtax_rel(lnkitemid,lnktaxunkid,amount,postingrule,item_type,created_user, createddatetime,companyid,locationid)";
                                   $strSql .= " VALUE (:lnkitemid, :lnktaxunkid, :amount,:postingrule,:item_type,:created_user, :createddatetime,:companyid, :locationid)";
                                   $dao->initCommand($strSql);
                                   $dao->addParameter(':lnkitemid', $modifierid);
                                   $dao->addParameter(':lnktaxunkid', $tvalue['taxid']);
                                   $dao->addParameter(':amount', $tvalue['amount']);
                                   $dao->addParameter(':postingrule', $tax_pr['postingrule']);
                                   $dao->addParameter(':item_type', '2');
                                   $dao->addParameter(':created_user', CONFIG_UID);
                                   $dao->addParameter(':createddatetime', $datetime);
                                   $dao->addParameter(':companyid', CONFIG_CID);
                                   $dao->addparameter(':locationid', CONFIG_LID);
                                   $dao->executeNonQuery();
                               }
                           }
                       }
                   }
                   if(count($tax_arr)>0){
                       $strid = implode(",",$tax_arr);
                       $strSql = "DELETE FROM ".CONFIG_DBN.".cfitemtax_rel WHERE lnkitemid=:lnkitemid AND itmtaxrelid IN (".$strid.") AND item_type=:item_type AND companyid=:companyid AND locationid=:locationid";
                       $dao->initCommand($strSql);
                       $dao->addParameter(':lnkitemid',$modifierid);
                       $dao->addParameter(':item_type', '2');
                       $dao->addParameter(':companyid',CONFIG_CID);
                       $dao->addparameter(':locationid',CONFIG_LID);
                       $dao->executeNonQuery();
                   }
               }
               else{
                   $strSql = "DELETE FROM ".CONFIG_DBN.".cfitemtax_rel WHERE lnkitemid=:id AND item_type=:item_type AND companyid=:companyid AND locationid=:locationid";
                   $dao->initCommand($strSql);
                   $dao->addParameter(":id",$modifierid);
                   $dao->addParameter(':item_type', '2');
                   $dao->addParameter(":companyid",CONFIG_CID);
                   $dao->addparameter(':locationid',CONFIG_LID);
                   $dao->executeNonQuery();
               }
               //

               //Bind unit
               if(count($data['bind_unit'])>0)
               {
                   $strSql = "SELECT itmunitunkid FROM ".CONFIG_DBN.".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                   $dao->initCommand($strSql);
                   $dao->addParameter(':lnkitemid',$modifierid);
                   $dao->addParameter(':recipe_type', '2');
                   $dao->addParameter(':companyid',CONFIG_CID);
                   $dao->addparameter(':locationid',CONFIG_LID);
                   $particuler_unit = $dao->executeQuery();

                   $unit_arr = \util\util::getoneDarray($particuler_unit,'itmunitunkid');
                   $bind_unit = $data['bind_unit'];

                   if(count($bind_unit)>0)
                   {
                       foreach($bind_unit AS $ukey=>$sub_unit)
                       {
                           $strSql = "SELECT itmunitunkid FROM ".CONFIG_DBN.".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND recipe_type=:recipe_type AND lnkitemunitid=:lnkitemunitid AND companyid=:companyid AND locationid=:locationid";
                           $dao->initCommand($strSql);
                           $dao->addParameter(':lnkitemid',$modifierid);
                           $dao->addParameter(':recipe_type', '2');
                           $dao->addParameter(':lnkitemunitid',$ukey);
                           $dao->addParameter(':companyid',CONFIG_CID);
                           $dao->addparameter(':locationid',CONFIG_LID);
                           $particuler_unit = $dao->executeRow();

                           if (in_array($particuler_unit['itmunitunkid'], $unit_arr))
                           {

                               $strSql = "UPDATE ".CONFIG_DBN.".cfmenu_itemunit_rel SET lnkitemid=:lnkitemid, lnkitemunitid=:lnkitemunitid, rate1=:rate1, rate2=:rate2,rate3=:rate3,rate4=:rate4,
                                           default_rate=:defaultrate,modified_user=:modified_user, modifieddatetime=:modifieddatetime WHERE recipe_type=:recipe_type AND companyid=:companyid AND itmunitunkid=:itmunitunkid
                                           AND locationid=:locationid";
                               $dao->initCommand($strSql);
                               $dao->addParameter(':itmunitunkid',$particuler_unit['itmunitunkid']);
                               $dao->addParameter(':lnkitemid', $modifierid);
                               $dao->addParameter(':lnkitemunitid', $ukey);
                               $dao->addParameter(':rate1', $sub_unit['rate1']);
                               $dao->addParameter(':rate2', $sub_unit['rate2']);
                               $dao->addParameter(':rate3', $sub_unit['rate3']);
                               $dao->addParameter(':rate4', $sub_unit['rate4']);
                               $dao->addParameter(':defaultrate', $sub_unit['defaultrate']);
                               $dao->addParameter(':modified_user',CONFIG_UID);
                               $dao->addParameter(':modifieddatetime',$datetime);
                               $dao->addParameter(':recipe_type', '2');
                               $dao->addParameter(':companyid',CONFIG_CID);
                               $dao->addparameter(':locationid',CONFIG_LID);
                               $dao->executeNonQuery();
                               $relunitid = $particuler_unit['itmunitunkid'];

                               if(($ukey = array_search($particuler_unit['itmunitunkid'], $unit_arr)) !== false) { //If found
                                   unset($unit_arr[$ukey]);
                               }
                           }
                           else {

                               $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu_itemunit_rel(lnkitemid,lnkitemunitid,rate1,rate2,rate3,rate4,default_rate,created_user, createddatetime,recipe_type,companyid,locationid)";
                               $strSql .= " VALUE (:lnkitemid, :lnkitemunitid, :rate1,:rate2,:rate3,:rate4,:defaultrate,:created_user, :createddatetime,:recipe_type,:companyid, :locationid)";
                               $dao->initCommand($strSql);
                               $dao->addParameter(':lnkitemid', $modifierid);
                               $dao->addParameter(':lnkitemunitid', $ukey);
                               $dao->addParameter(':rate1', $sub_unit['rate1']);
                               $dao->addParameter(':rate2', $sub_unit['rate2']);
                               $dao->addParameter(':rate3', $sub_unit['rate3']);
                               $dao->addParameter(':rate4', $sub_unit['rate4']);
                               $dao->addParameter(':defaultrate', $sub_unit['defaultrate']);
                               $dao->addParameter(':created_user', CONFIG_UID);
                               $dao->addParameter(':createddatetime', $datetime);
                               $dao->addParameter(':recipe_type', '2');
                               $dao->addParameter(':companyid', CONFIG_CID);
                               $dao->addparameter(':locationid', CONFIG_LID);
                               $dao->executeNonQuery();
                               $relunitid = $dao->getLastInsertedId();
                           }
                           //Recipe
                           if(isset($sub_unit['recipe']) && count($sub_unit['recipe'])>0)
                           {
                               $strSql = "SELECT recipeunkid FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:lnkitemid AND lnkitemunitid=:lnkitemunitid AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                               $dao->initCommand($strSql);
                               $dao->addParameter(':lnkitemid',$modifierid);
                               $dao->addParameter(':lnkitemunitid',$relunitid);
                               $dao->addParameter(':recipe_type', '2');
                               $dao->addParameter(':companyid',CONFIG_CID);
                               $dao->addparameter(':locationid',CONFIG_LID);
                               $particuler_recipe = $dao->executeQuery();

                               $recipe_arr = \util\util::getoneDarray($particuler_recipe,'recipeunkid');
                               $bind_recipe = $sub_unit['recipe'];

                               if(count($bind_recipe)>0)
                               {
                                   foreach($bind_recipe AS $rkey=>$sub_recipe)
                                   {
                                       $strSql = "SELECT recipeunkid FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:lnkitemid AND lnkitemunitid=:lnkitemunitid AND recipe_type=:recipe_type AND lnkrawmaterialid=:lnkrawmaterialid AND companyid=:companyid AND locationid=:locationid";
                                       $dao->initCommand($strSql);
                                       $dao->addParameter(':lnkitemid',$modifierid);
                                       $dao->addParameter(':lnkitemunitid',$relunitid);
                                       $dao->addParameter(':lnkrawmaterialid',$rkey);
                                       $dao->addParameter(':recipe_type', '2');
                                       $dao->addParameter(':companyid',CONFIG_CID);
                                       $dao->addparameter(':locationid',CONFIG_LID);
                                       $particuler_recipe = $dao->executeRow();

                                       $unit=$sub_recipe['unitval']*$sub_recipe['quantity'];

                                       if (in_array($particuler_recipe['recipeunkid'], $recipe_arr))
                                       {

                                           $strSql = "UPDATE ".CONFIG_DBN.".cfmenu_item_recipe SET lnkitemid=:lnkitemid,lnkitemunitid=:lnkitemunitid,lnkrawmaterialid=:lnkrawmaterialid, unit=:unit, lnkunitid=:lnkunitid,lnkstoreid=:lnkstoreid, modified_user=:modified_user, modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND recipeunkid=:recipeunkid AND locationid=:locationid";
                                           $dao->initCommand($strSql);
                                           $dao->addParameter(':recipeunkid',$particuler_recipe['recipeunkid']);
                                           $dao->addParameter(':lnkitemid',$modifierid);
                                           $dao->addParameter(':lnkitemunitid',$relunitid);
                                           $dao->addParameter(':lnkrawmaterialid',$rkey);
                                           $dao->addParameter(':unit', $unit);
                                           $dao->addParameter(':lnkunitid',$sub_recipe['unit']);
                                           $dao->addParameter(':lnkstoreid',$sub_recipe['store']);
                                           $dao->addParameter(':modified_user',CONFIG_UID);
                                           $dao->addParameter(':modifieddatetime',$datetime);
                                           $dao->addParameter(':companyid',CONFIG_CID);
                                           $dao->addparameter(':locationid',CONFIG_LID);
                                           $dao->executeNonQuery();
                                           if(($rkey = array_search($particuler_recipe['recipeunkid'], $recipe_arr)) !== false) {
                                               unset($recipe_arr[$rkey]);
                                           }
                                       }
                                       else{

                                           $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu_item_recipe (lnkitemid,lnkitemunitid,lnkrawmaterialid,unit,lnkunitid,lnkstoreid,recipe_type,created_user, createddatetime, companyid,locationid)";
                                           $strSql .= "VALUE (:lnkitemid,:lnkitemunitid,:lnkrawmaterialid, :unit,:lnkunitid,:lnkstoreid,:recipe_type,:created_user, :createddatetime, :companyid, :locationid)";
                                           $dao->initCommand($strSql);
                                           $dao->addParameter(':lnkitemid', $modifierid);
                                           $dao->addParameter(':lnkitemunitid',$relunitid);
                                           $dao->addParameter(':lnkrawmaterialid', $rkey);
                                           $dao->addParameter(':unit', $unit);
                                           $dao->addParameter(':lnkunitid',$sub_recipe['unit']);
                                           $dao->addParameter(':lnkstoreid',$sub_recipe['store']);
                                           $dao->addParameter(':recipe_type', '2');
                                           $dao->addParameter(':created_user', CONFIG_UID);
                                           $dao->addParameter(':createddatetime', $datetime);
                                           $dao->addParameter(':companyid', CONFIG_CID);
                                           $dao->addparameter(':locationid', CONFIG_LID);
                                           $dao->executeNonQuery();
                                       }
                                   }
                               }
                               if(count($recipe_arr)>0){
                                   $strid = implode(",",$recipe_arr);
                                   $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:lnkitemid AND lnkitemunitid=:lnkitemunitid AND recipeunkid IN (".$strid.") AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                                   $dao->initCommand($strSql);
                                   $dao->addParameter(':lnkitemid',$modifierid);
                                   $dao->addParameter(':lnkitemunitid',$relunitid);
                                   $dao->addParameter(':recipe_type', '2');
                                   $dao->addParameter(':companyid',CONFIG_CID);
                                   $dao->addparameter(':locationid',CONFIG_LID);
                                   $dao->executeNonQuery();
                               }
                           }
                           else
                           {
                               //Delete From Item- Recipe
                               $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:id AND lnkitemunitid=:lnkitemunitid AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                               $dao->initCommand($strSql);
                               $dao->addParameter(":id",$modifierid);
                               $dao->addParameter(':lnkitemunitid',$relunitid);
                               $dao->addParameter(':recipe_type', '2');
                               $dao->addParameter(":companyid",CONFIG_CID);
                               $dao->addparameter(':locationid',CONFIG_LID);
                               $dao->executeNonQuery();
                               //Delete From Item- Recipe
                           }
                           //End of Recipe
                       }
                   }//End of unit if

                   if(count($unit_arr)>0){

                       $strid = implode(",",$unit_arr);
                       $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND itmunitunkid IN (".$strid.") AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                       $dao->initCommand($strSql);
                       $dao->addParameter(':lnkitemid',$modifierid);
                       $dao->addParameter(':recipe_type', '2');
                       $dao->addParameter(':companyid',CONFIG_CID);
                       $dao->addparameter(':locationid',CONFIG_LID);
                       $dao->executeNonQuery();

                       $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:lnkitemid AND lnkitemunitid IN (".$strid.") AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                       $dao->initCommand($strSql);
                       $dao->addParameter(':lnkitemid',$modifierid);
                       $dao->addParameter(':recipe_type', '2');
                       $dao->addParameter(':companyid',CONFIG_CID);
                       $dao->addparameter(':locationid',CONFIG_LID);
                       $dao->executeNonQuery();
                   }
               }
               else
               {
                   //Delete From Item- Unit
                   $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                   $dao->initCommand($strSql);
                   $dao->addParameter(":lnkitemid",$modifierid);
                   $dao->addParameter(':recipe_type', '2');
                   $dao->addParameter(":companyid",CONFIG_CID);
                   $dao->addparameter(':locationid',CONFIG_LID);
                   $dao->executeNonQuery();
                   //Delete From Item- Unit

                   //Delete From Item- Recipe
                   $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:id AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                   $dao->initCommand($strSql);
                   $dao->addParameter(":id",$modifierid);
                   $dao->addParameter(':recipe_type', '2');
                   $dao->addParameter(":companyid",CONFIG_CID);
                   $dao->addparameter(':locationid',CONFIG_LID);
                   $dao->executeNonQuery();
                   //Delete From Item- Recipe
               }
               // End of unit

               $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
               return json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_UP_SUC));
            }
        } catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addmodifier - '.$e);
        }
    }

    public function modifierlist($limit,$offset,$name,$isactive=0)
    {
        try
        {
            $this->log->logIt($this->module.' - modifierlist - '.$name);
            
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

           $strSql = "SELECT CFM.*,IFNULL(ROUND(CFM.sale_amount,$round_off),'') as sale_amount,IF(CFM.is_included=1,'Yes','No')as is_included,IFNULL(CFU1.username,'') AS createduser,IFNULL(CFU2.username,'') AS modifieduser,
            IFNULL(DATE_FORMAT(CFM.createddatetime,'".$mysqlformat."'),'') as created_date,
            IFNULL(DATE_FORMAT(CFM.modifieddatetime,'".$mysqlformat."'),'') as modified_date FROM ".CONFIG_DBN.".cfmenu_modifiers AS CFM
             LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CFM.created_user=CFU1.userunkid AND CFM.companyid=CFU1.companyid
             LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON CFM.modified_user=CFU2.userunkid AND CFM.modified_user=CFU2.userunkid AND CFM.companyid=CFU2.companyid      
             WHERE CFM.companyid=:companyid AND CFM.locationid=:locationid AND CFM.is_deleted=0";

           if($name!="")
            {
                $strSql .= " AND modifiername LIKE '%".$name."%'";
            }
            if($isactive == 1)
            {
                $strSql .= " and CFM.is_active=1 ";
            }
            $strSql.=" GROUP BY modifierunkid";
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $data = $dao->executeQuery();
           
           if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $rec = $dao->executeQuery();
            
            if(count($data) != 0){               
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue));
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - modifierlist - '.$e);
        }
    }
    
    public function getRecmodifier($data)
    {
        try
        {
            $this->log->logIt($this->module." - getRecmodifier");
            $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $id=(isset($data['id']))?$data['id']:"";
            $strSql = "SELECT modifiername,min,max,long_desc,is_included,sku,is_active,tax_apply_rate,IFNULL(image,'".CONFIG_ASSETS_URL."noimage.png') as image FROM ".CONFIG_DBN.".cfmenu_modifiers WHERE hashkey=:modifierunkid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':modifierunkid',$id);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeRow();

            $response_arr = array();
            $response_arr['itmdata'] =$res;
            $response_arr['bind_unit'] = array();
            $response_array['bind_tax'] = array();

            $ObjCommonDao = new \database\commondao;
            $modifierunkid = $ObjCommonDao->getprimarykey('cfmenu_modifiers',$id,'modifierunkid');

            $strSql = "SELECT CIU.hashkey as unithash,CIUR.itmunitunkid,
                  IFNULL(ROUND(CIUR.rate1,$round_off),'') as rate1,IFNULL(ROUND(CIUR.rate2,$round_off),'') as rate2,
                  IFNULL(ROUND(CIUR.rate3,$round_off),'') as rate3,IFNULL(ROUND(CIUR.rate4,$round_off),'') as rate4,
                  CIUR.default_rate,CIU.name,CIUR.lnkitemunitid
                  FROM ".CONFIG_DBN.".cfmenu_itemunit_rel as CIUR
                  INNER JOIN ".CONFIG_DBN.".cfmenu_itemunit AS CIU
                  ON CIU.unitunkid=CIUR.lnkitemunitid                
                  WHERE CIUR.lnkitemid=:itemid AND CIUR.recipe_type=:recipe_type AND CIUR.companyid=:companyid AND CIUR.locationid=:locationid";

            $dao->initCommand($strSql);
            $dao->addParameter(':itemid',$modifierunkid);
            $dao->addParameter(':recipe_type', '2');
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $unitdata = $dao->executeQuery();
            foreach ($unitdata as $key=>$val){

                $strRSql = "SELECT ROUND((CIR.unit/CU.unit),$round_off)as quantity,ROUND((CIR.unit/CUR.conversation_rate),$round_off)as raw_quantity,
                  CU.unitunkid,CIR.lnkstoreid,CU.hashkey,CR.id as rawhash,CR.name,CU.name as unit_name,CU.unit
                  FROM ".CONFIG_DBN.".cfmenu_item_recipe as CIR
                  INNER JOIN ".CONFIG_DBN.".cfrawmaterial AS CR ON CR.id=CIR.lnkrawmaterialid
                  INNER JOIN ".CONFIG_DBN.".cfunit AS CU ON CU.unitunkid=CIR.lnkunitid    
                  LEFT JOIN ".CONFIG_DBN.".cfrawmaterial_unit_rel AS CUR ON CUR.lnkrawid=CIR.lnkrawmaterialid AND CUR.lnkunitid=CIR.lnkunitid AND CUR.companyid=:companyid 
                  WHERE CIR.lnkitemid=:itemid AND CIR.lnkitemunitid=:lnkitemunitid AND CIR.recipe_type=:recipe_type AND CIR.companyid=:companyid AND CIR.locationid=:locationid";

                $dao->initCommand($strRSql);
                $dao->addParameter(':itemid',$modifierunkid);
                $dao->addParameter(':lnkitemunitid',$val['itmunitunkid']);
                $dao->addParameter(':recipe_type', '2');
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $recipe_data = $dao->executeQuery();
                $unitdata[$key]['bind_recipe'] = $recipe_data;
            }
            $response_arr['bind_unit'] = $unitdata;

            $strSql = "SELECT CITR.lnktaxunkid,
                  ROUND(CITR.amount,$round_off) as amount
                  FROM ".CONFIG_DBN.".cfitemtax_rel as CITR         
                  WHERE CITR.lnkitemid=:itemid AND CITR.item_type=:item_type AND CITR.companyid=:companyid AND CITR.locationid=:locationid";

            $dao->initCommand($strSql);
            $dao->addParameter(':itemid',$modifierunkid);
            $dao->addParameter(':item_type', '2');
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $taxdata = $dao->executeQuery();
            $response_arr['bind_tax'] = $taxdata;

            return json_encode(array("Success"=>"True","Data"=>$response_arr));
           
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getRecmodifier - ".$e);
            return false; 
        }
    }

    public function getmodifier_combo()
    {
        try
        {
            $this->log->logIt($this->module." - getmodifier_combo");
            $dao = new \dao;
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $strMSql = "SELECT modifiername,max,min,modifierunkid,is_included FROM ".CONFIG_DBN.".cfmenu_modifiers where is_active=1 AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strMSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $data['modifier'] = $dao->executeQuery();

            $strSql = "SELECT CMODI.modifierunkid,CIU.unitunkid,CIU.name as itemunit,
                        CASE 
                        WHEN CUREL.default_rate=1 THEN ROUND(CUREL.rate1,$round_off)
                        WHEN CUREL.default_rate=2 THEN ROUND(CUREL.rate2,$round_off)
                        WHEN CUREL.default_rate=3 THEN ROUND(CUREL.rate3,$round_off)
                        ELSE ROUND(CUREL.rate4,$round_off)
                        END as rate
                        FROM ".CONFIG_DBN.".cfmenu_modifiers as CMODI 
                        LEFT JOIN ".CONFIG_DBN.".cfmenu_itemunit_rel as CUREL on CUREL.lnkitemid=CMODI.modifierunkid and CUREL.recipe_type=:recipe_type and CMODI.is_included=0
                        LEFT JOIN ".CONFIG_DBN.".cfmenu_itemunit as CIU on CUREL.lnkitemunitid=CIU.unitunkid and CMODI.is_included=0
                        where CMODI.is_active=1 AND CMODI.is_deleted=0 AND CMODI.companyid=:companyid AND CMODI.locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':recipe_type',2);
            $dao->addparameter(':locationid',CONFIG_LID);
            $data['modifier_unit'] = $dao->executeQuery();
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getmodifier_combo - ".$e);
            return false; 
        }  
    }
    public function getModifier($data)
    {
        try
        {
            $this->log->logIt($this->module." - getModifier");
            $dao = new \dao;
            $strSql = "SELECT modifiername,hashkey FROM ".CONFIG_DBN.".cfmenu_modifiers where is_active=1 AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid AND is_included=:is_included";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->addparameter(':is_included',1);
            $data = $dao->executeQuery();
            return json_encode(array("Success"=>"True","Data"=>$data));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getModifier - ".$e);
            return false;
        }
    }

    public function getitemincluded($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getitemincluded');
            $ObjDependencyDao = new \database\dependencydao();
            $ObjcommonDao = new \database\commondao();
            $Objstaticarray =new \common\staticarray();
            $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
            $pd = $Objstaticarray->auditlogmodules[$data['module']]['pd'];
            $id = $ObjcommonDao->getprimarykey($table,$data['id'],$pd);
            $check = $ObjDependencyDao->checkdependency($data['module'],$id);
            if($check>0){
                return json_encode(array("Success"=>"False","Message"=>"Please remove from related item !"));
            }
            else{
                return json_encode(array("Success"=>"True"));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getitemincluded - '.$e);
        }
    }

    public function removemodifier($data)
    {
        try
        {
            $this->log->logIt($this->module.' - remove');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $ObjDependencyDao = new \database\dependencydao();
            $Objstaticarray =new \common\staticarray();
            $datetime=\util\util::getLocalDateTime();
            if(count($data)>0 && isset($data['module']) && isset($data['id']))
            {
                $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $id = $ObjCommonDao->getprimarykey("cfmenu_modifiers",$data['id'],"modifierunkid");

                $check = $ObjDependencyDao->checkdependency($data['module'],$id);
                if($check>0){
                    return json_encode(array("Success"=>"False","Message"=>"Please remove related records"));
                }
                //Delete modifier
                $strSql = "UPDATE ".CONFIG_DBN.".cfmenu_modifiers SET is_deleted=1,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE hashkey=:id AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id",$data['id']);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();

                /*Delete From Item- Modifier Relation*/
                $strSql = "DELETE FROM ".CONFIG_DBN.".fditem_modifier_relation WHERE fditem_modifier_relation=:id AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id",$id);
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();

                /*Delete From Item- Modifier Item Relation*/
                $strSql = "DELETE FROM ".CONFIG_DBN.".fdmodifier_item_relation WHERE lnkmodifierid=:id AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id",$id);
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();

                //Delete Recipe
                $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_item_recipe WHERE lnkitemid=:id AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id",$id);
                $dao->addParameter(':recipe_type', '2');
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();

                $strSql = "DELETE FROM ".CONFIG_DBN.".cfitemtax_rel WHERE lnkitemid=:id AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id",$id);
                $dao->addParameter(':recipe_type', '2');
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'],'Delete Record',$data['id'],'');
                return json_encode(array("Success"=>"True","Message"=>"Record removed successfully"));
            }
            else
                return json_encode(array("Success"=>"False","Message"=>"Internal Error"));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - remove - '.$e);
        }
    }


}
?>