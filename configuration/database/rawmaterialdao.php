<?php
namespace database;

class rawmaterialdao
{
    public $module = 'DB_rawmaterial';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addRawmaterial($data,$languageArr,$defaultlanguageArr)
    {
        try
        {   
            $this->log->logIt($this->module.' - addRawmaterial');
            $dao = new \dao();
            $datetime=\util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $ObjDependencyDao = new \database\dependencydao();
            $tablename =  "cfrawmaterial";

            $arr_log = array(
                'Name'=>$data['name'],
                'Code'=>$data['code'],
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive'
            );
            $json_data = html_entity_decode(json_encode($arr_log));
            $dao->bindTransaction();

            if($data['id']==0)
            {
                $chk_name = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"name",$data['name']);
                if($chk_name==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG25)));
                }
                $chk_code = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"code",$data['code']);
                if($chk_code==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG26)));
                }
                $title = "Add Record";
                $hashkey = \util\util::gethash();

                $strSql = "INSERT INTO ".CONFIG_DBN.".cfrawmaterial(`name`, 
                                                                    code, 
                                                                    lnkcategoryid, 
                                                                    measuretype,
                                                                    companyid,
                                                                    storeid,
                                                                    createddatetime, 
                                                                    created_user, 
                                                                    is_active, 
                                                                    hashkey,
                                                                    is_recipe)
                                                            VALUE(  :name, 
                                                                    :code, 
                                                                    :category, 
                                                                    :measuretype,
                                                                    :companyid,
                                                                    :storeid, 
                                                                    :createddatetime, 
                                                                    :created_user, 
                                                                    :rdo_status, 
                                                                    :hashkey,
                                                                    :is_recipe)";
                
                $dao->initCommand($strSql);
                $dao->addParameter(':name',$data['name']);
                $dao->addParameter(':code',$data['code']);
                $dao->addParameter(':category', $data['category']);
                $dao->addParameter(':measuretype', $data['measuretype']);
                $dao->addParameter(':rdo_status',$data['rdo_status']);
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':created_user',CONFIG_UID);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':storeid',CONFIG_SID);
                $dao->addparameter(':is_recipe',$data['is_recipe']);
                $dao->executeNonQuery();
                $lastid = $dao->getLastInsertedId();

                if($lastid) {
                    $minlimit= (float)$data['minlimit']*(float)$data['unitval'];
                    $dao->initCommand("SELECT u.unitunkid FROM ".CONFIG_DBN.".cfrawmaterial CFR 
					INNER JOIN ".CONFIG_DBN.".cfunit u ON u.measuretype = CFR.measuretype
					 AND u.is_systemdefined=1 AND u.companyid=:companyid
					 WHERE CFR.id = :id AND CFR.companyid=:companyid ");
                    $dao->addParameter(':id',$lastid);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $rawMaterialRec = $dao->executeRow();

                    $strloc = "INSERT INTO ".CONFIG_DBN.".cfrawmaterial_loc(lnkrawmaterialid,
                                                                    min_limit,
                                                                    min_unitid,
                                                                    inv_unitid,
                                                                    companyid,
                                                                    storeid)
                                                            VALUE(  :locid, 
                                                                    :minlimit,  
                                                                    :munit, 
                                                                    :inv_unitid,
                                                                    :companyid,
                                                                    :storeid)";
                    $dao->initCommand($strloc);
                    $dao->addParameter(':locid',$lastid);
                    $dao->addParameter(':minlimit',$minlimit);
                    $dao->addParameter(':munit',$data['munit']);
                    $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addparameter(':storeid',CONFIG_SID);
                    $dao->executeNonQuery();
                    //$ilastid = $dao->getLastInsertedId();

                   //Add unit
                    if(count($data['bind_unit'])>0) {
                        $bind_unit = $data['bind_unit'];
                        foreach ($bind_unit AS $ukey => $sub_unit) {
                            $strSql = "INSERT INTO " . CONFIG_DBN . ".cfrawmaterial_unit_rel(lnkrawid,lnkunitid,conversation_rate,createddatetime,created_user,companyid,storeid)";
                            $strSql .= " VALUE (:lnkrawid, :lnkunitid,:conversation_rate,:createddatetime,:created_user,:companyid,:storeid)";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':lnkrawid', $lastid);
                            $dao->addParameter(':lnkunitid', $ukey);
                            $dao->addParameter(':conversation_rate', $sub_unit['conversation_rate']);
                            $dao->addParameter(':createddatetime',$datetime);
                            $dao->addParameter(':created_user',CONFIG_UID);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':storeid', CONFIG_SID);
                            $dao->executeNonQuery();
                        }
                    }
                    $dao->releaseTransaction(TRUE);
                    $ObjAuditDao->addactivitylog($data['module'], $title, $hashkey, $json_data);
                    return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_ADD_SUC)));
                }
            }
           else
           {
               $id =$ObjCommonDao->getprimaryBycompany('cfrawmaterial',$data['id'],'id');
               $chk_name = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"name",$data['name'],$id);
               if($chk_name==1)
               {
                   return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG25)));
               }
               $chk_code = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"code",$data['code'],$id);
               if($chk_code==1)
               {
                   return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG26)));
               }

                $title = "Edit Record";
                $strSql = "UPDATE ".CONFIG_DBN.".cfrawmaterial SET name=:name,
                                                                    code=:code,
                                                                    lnkcategoryid=:category,
                                                                    measuretype=:measuretype,
                                                                    modifieddatetime=:modifieddatetime,
                                                                    modified_user=:modified_user,
                                                                    is_recipe=:is_recipe
                                                                    WHERE hashkey=:id AND companyid=:companyid";
                $dao->initCommand($strSql);
                $dao->addParameter(':id',$data['id']);
                $dao->addParameter(':name',$data['name']);
                $dao->addParameter(':code',$data['code']);
                $dao->addParameter(':category', $data['category']);
                $dao->addParameter(':measuretype', $data['measuretype']);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addParameter(':is_recipe', $data['is_recipe']);
                $dao->executeNonQuery();

                $rawmaterialid =$ObjCommonDao->getprimaryBycompany('cfrawmaterial',$data['id'],'id');
                $minlimit= ((float)$data['minlimit']*(float)$data['unitval']);

               if($data['locid']=='' || $data['locid']==0) {
                   $strloc = "INSERT INTO ".CONFIG_DBN.".cfrawmaterial_loc(lnkrawmaterialid,
                                                                    min_limit,
                                                                    min_unitid,
                                                                    companyid,
                                                                    storeid)
                                                            VALUE(  :locid,
                                                                    :minlimit,
                                                                    :munit,
                                                                    :companyid,
                                                                    :storeid)";
              }
              else {
                  $strloc = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET
                                                                    min_limit=:minlimit,
                                                                    min_unitid=:munit
                                                                    WHERE lnkrawmaterialid=:locid AND companyid=:companyid 
                                                                    AND storeid=:storeid";

              }
              $dao->initCommand($strloc);
              $dao->addParameter(':locid', $rawmaterialid);
              $dao->addParameter(':minlimit', $minlimit);
              $dao->addParameter(':munit',$data['munit']);
              $dao->addParameter(':companyid', CONFIG_CID);
              $dao->addparameter(':storeid',CONFIG_SID);
              $dao->executeNonQuery();

               //Add unit
               if(count($data['bind_unit'])>0) {

                   $strSql = "SELECT rawunitunkid FROM ".CONFIG_DBN.".cfrawmaterial_unit_rel WHERE lnkrawid=:lnkrawid AND companyid=:companyid";
                   $dao->initCommand($strSql);
                   $dao->addParameter(':lnkrawid', $id);
                   $dao->addParameter(':companyid',CONFIG_CID);
                   //$dao->addparameter(':storeid', CONFIG_SID);(Change unit company wise)
                   $particuler_unit = $dao->executeQuery();

                   $unit_arr = \util\util::getoneDarray($particuler_unit,'rawunitunkid');
                   $bind_unit = $data['bind_unit'];

                   if(count($bind_unit)>0) {

                       foreach ($bind_unit AS $ukey => $sub_unit) {

                           $strSql = "SELECT rawunitunkid,conversation_rate FROM ".CONFIG_DBN.".cfrawmaterial_unit_rel WHERE lnkrawid=:lnkrawid AND lnkunitid=:lnkunitid AND companyid=:companyid";
                           $dao->initCommand($strSql);
                           $dao->addParameter(':lnkrawid', $id);
                           $dao->addParameter(':lnkunitid', $ukey);
                           $dao->addParameter(':companyid',CONFIG_CID);
                           //$dao->addparameter(':storeid', CONFIG_SID);(Change unit company wise)
                           $particuler_unit = $dao->executeRow();
                           if (in_array($particuler_unit['rawunitunkid'], $unit_arr))
                           {
                               if($particuler_unit['conversation_rate']!=$sub_unit['conversation_rate']){
                                   return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG31)));
                               }
                               $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_unit_rel SET conversation_rate=:conversation_rate,modifieddatetime=:modifieddatetime,modified_user=:modified_user
                                          WHERE lnkrawid=:lnkrawid AND lnkunitid=:lnkunitid AND companyid=:companyid";
                               $dao->initCommand($strSql);
                               $dao->addParameter(':lnkrawid', $id);
                               $dao->addParameter(':lnkunitid', $ukey);
                               $dao->addParameter(':conversation_rate', $sub_unit['conversation_rate']);
                               $dao->addParameter(':modifieddatetime',$datetime);
                               $dao->addParameter(':modified_user',CONFIG_UID);
                               $dao->addParameter(':companyid', CONFIG_CID);
                               //$dao->addparameter(':storeid', CONFIG_SID);
                               //$dao->executeNonQuery();
                               if(($ukey = array_search($particuler_unit['rawunitunkid'], $unit_arr)) !== false) {
                                   unset($unit_arr[$ukey]);
                               }
                           }
                           else {
                               $strSql = "INSERT INTO " . CONFIG_DBN . ".cfrawmaterial_unit_rel(lnkrawid,lnkunitid,conversation_rate,createddatetime,created_user,companyid,storeid)";
                               $strSql .= " VALUE (:lnkrawid, :lnkunitid,:conversation_rate,:createddatetime,:created_user,:companyid,:storeid)";
                               $dao->initCommand($strSql);
                               $dao->addParameter(':lnkrawid', $id);
                               $dao->addParameter(':lnkunitid', $ukey);
                               $dao->addParameter(':conversation_rate', $sub_unit['conversation_rate']);
                               $dao->addParameter(':createddatetime',$datetime);
                               $dao->addParameter(':created_user',CONFIG_UID);
                               $dao->addParameter(':companyid', CONFIG_CID);
                               $dao->addparameter(':storeid', CONFIG_SID);
                               $dao->executeNonQuery();
                           }
                       }
                   }
                   if(count($unit_arr)>0){
                       return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG30)));
                      /* $strid = implode(",",$unit_arr);
                       $strSql = "DELETE FROM ".CONFIG_DBN.".cfrawmaterial_unit_rel WHERE lnkrawid=:lnkrawid AND rawunitunkid IN (".$strid.") AND companyid=:companyid AND storeid=:storeid";
                       $dao->initCommand($strSql);
                       $dao->addParameter(':lnkrawid', $id);
                       $dao->addParameter(':companyid', CONFIG_CID);
                       $dao->addparameter(':storeid', CONFIG_SID);
                       $dao->executeNonQuery();*/
                   }
               }
               else{
                   $strSql = "SELECT rawunitunkid FROM ".CONFIG_DBN.".cfrawmaterial_unit_rel WHERE lnkrawid=:lnkrawid AND companyid=:companyid";
                   $dao->initCommand($strSql);
                   $dao->addParameter(':lnkrawid', $id);
                   $dao->addParameter(':companyid',CONFIG_CID);
                   $particuler_unit = $dao->executeQuery();
                   if($particuler_unit){
                       return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG30)));
                   }

                   /*$strSql = "DELETE FROM ".CONFIG_DBN.".cfrawmaterial_unit_rel WHERE lnkrawid=:lnkrawid AND companyid=:companyid AND storeid=:storeid";
                   $dao->initCommand($strSql);
                   $dao->addParameter(':lnkrawid', $id);
                   $dao->addParameter(':companyid', CONFIG_CID);
                   $dao->addparameter(':storeid', CONFIG_SID);
                   $dao->executeNonQuery();*/
               }
                $dao->releaseTransaction(TRUE);
                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_UP_SUC)));
            }
        }
        catch(Exception $e)
        {
            $dao->releaseTransaction(FALSE);
            $this->log->logIt($this->module.' - addRawmaterial - '.$e);
        }
    }

    public function addInventory($data,$languageArr)
    {
        try {
            $this->log->logIt($this->module . ' - addInventory');
            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
           /* $this->log->logIt("Iunit val ::".$data['iunitval']);
            $this->log->logIt("unit ::".$data['unit']);*/

            $title = "Add Record";
            $rawid =$ObjCommonDao->getprimaryBycompany('cfrawmaterial',$data['rawhash'],'id');

            $strSql = "SELECT id,inventory FROM ".CONFIG_DBN.".cfrawmaterial_loc WHERE lnkrawmaterialid=:rawhash and storeid=:storeid and companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':rawhash',$rawid);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $loclist = $dao->executeRow();

                $strSql = "SELECT unitunkid FROM ".CONFIG_DBN.".cfunit WHERE measuretype=:measureval and is_systemdefined=1 and companyid=:companyid";
                $dao->initCommand($strSql);
                $dao->addParameter(':measureval',$data['measureval']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $unitlist = $dao->executeRow();
                $unitID=$unitlist['unitunkid'];

                $locID=$loclist['id'];
                $locInventory=$loclist['inventory'];
                $inventory = $data['quantity'] * $data['iunitval'];

                if(empty($locID)){
                    $strinLoc = "INSERT INTO " . CONFIG_DBN . ".cfrawmaterial_loc(
                                                                    lnkrawmaterialid,
                                                                    inventory, 
                                                                    inv_unitid,
                                                                    companyid,
                                                                    storeid)
                                                            VALUE(  :id,
                                                                    :inventory, 
                                                                    :unit,
                                                                    :companyid,
                                                                    :storeid)";
                    $dao->initCommand($strinLoc);
                    $dao->addParameter(':id', $rawid);
                    $dao->addParameter(':inventory', $inventory);
                    $dao->addParameter(':unit',$unitID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid', CONFIG_SID);
                    $dao->executeNonQuery();
                    $locID = $dao->getLastInsertedId();
                }
                else{
                    $totalInventory = ($locInventory + $inventory);

                    $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET inventory=:inventory,
                                                                        inv_unitid=:unit   
                                                                        WHERE lnkrawmaterialid=:id
                                                                        AND companyid=:companyid AND storeid=:storeid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':inventory', $totalInventory);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':unit',$unitID);
                    $dao->addParameter(':id', $rawid);
                    $dao->executeNonQuery();
                }
                $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(
                                                                    lnkrawmateriallocid,
                                                                    lnkrawmaterialid,
                                                                    lnkvendorid,
                                                                    inventory, 
                                                                    rate,
                                                                    total_rate,
                                                                    inv_unitid,
                                                                    inv_status,
                                                                    crdb,
                                                                    created_user,
                                                                    createddatetime,
                                                                    companyid,
                                                                    storeid)
                                                            VALUE(  :locid, 
                                                                    :id,
                                                                    :vendor,
                                                                    :inventory, 
                                                                    :rate,
                                                                    :rate,
                                                                    :unit,
                                                                    :invstatus,
                                                                    :crdb,
                                                                    :created_user,
                                                                    :createddatetime,                                                               
                                                                    :companyid,
                                                                    :storeid)";
                $dao->initCommand($strinventory);
                $dao->addParameter(':id', $rawid);
                $dao->addParameter(':locid', $locID);
                $dao->addParameter(':vendor', $data['vendor']);
                $dao->addParameter(':inventory', $inventory);
                $dao->addParameter(':rate', $data['price']);
                $dao->addParameter(':unit',$data['unit']);
                $dao->addParameter(':invstatus',INV_STATUS_ADD);
                $dao->addParameter(':crdb','cr');
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':created_user',CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':storeid', CONFIG_SID);
                $dao->executeNonQuery();

             //Update rate
           // $oldUnit =$ObjCommonDao->getprimarykey('cfrawmaterial',$data['rawhash'],'rateperunit');
            $strSql = "SELECT rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc WHERE lnkrawmaterialid=:id and companyid=:companyid and storeid=:storeid";
            $dao->initCommand($strSql);
            $dao->addParameter(':id',$rawid);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $rawloclist = $dao->executeRow();
            $oldUnit= $rawloclist['rateperunit'];

            $oldPrice=($oldUnit*$locInventory);

            $totalPrice=$oldPrice+$data['price'];
            $totalInventory = ($locInventory + $inventory);
            $rateperunit = $totalPrice / $totalInventory;

            /* $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial SET rateperunit=:rateperunit,
                                                               modifieddatetime=:modifieddatetime,
                                                               modified_user=:modified_user
                                                               WHERE id=:id AND companyid=:companyid";
           $dao->initCommand($strSql);
           $dao->addParameter(':rateperunit', $rateperunit);
           $dao->addParameter(':id', $rawid);
           $dao->addParameter(':modifieddatetime',$datetime);
           $dao->addParameter(':modified_user',CONFIG_UID);
           $dao->addParameter(':companyid', CONFIG_CID);
           $dao->executeNonQuery();*/

            $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit                         
                        WHERE lnkrawmaterialid=:id AND companyid=:companyid
                        and storeid=:storeid";
            $dao->initCommand($strSql);
            $dao->addParameter(':rateperunit', $rateperunit);
            $dao->addParameter(':id', $rawid);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $dao->executeNonQuery();

            //$ObjAuditDao->addactivitylog($data['module'], $title, $hashkey, $json_data);
            return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $languageArr->LANG29)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addInventory - '.$e);
        }
    }

    public function getInventoryRec($data,$languageArr,$defaultlanguageArr)
    {
        try
        {
            $this->log->logIt($this->module." - getInventoryRec");
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $rawid =$ObjCommonDao->getprimaryBycompany('cfrawmaterial',$data['rawhash'],'id');
            $strSql = "SELECT ROUND((CI.inventory/CU.unit),$round_off) as inventory,
                        ROUND((CI.inventory/CUR.conversation_rate),$round_off) as raw_inventory,CI.id,                       
                         CASE WHEN CI.inv_status=1 THEN '".$languageArr->LANG17."'
                              WHEN CI.inv_status=2 THEN '".$languageArr->LANG18." (".$defaultlanguageArr->ISSUEVOUCHER.")"."'
                              WHEN CI.inv_status=3 THEN '".$languageArr->LANG18." (".$defaultlanguageArr->GOODRECEIPTNOTE.")"."'
                              ELSE '".$languageArr->LANG18."'                           
                       END as remark,
                        CI.inv_status,CI.is_deleted,
                        ROUND((CL.inventory/CU1.unit),$round_off) as total_inventory,
                        CU1.shortcode as base_code,IFNULL(TRC.name,'') as vendor_name,
                        ROUND((CI.rate),$round_off) as rate,CU.shortcode,
                        IFNULL(ROUND(CL.rateperunit,$round_off),0) as rateperunit,
                        ROUND((CL.inventory*CL.rateperunit),$round_off) as total_rate,
                        IFNULL(CFU1.username,'') AS createduser,
                        IFNULL(DATE_FORMAT(CI.createddatetime,'".$mysqlformat."'),'') as created_date
                        FROM ".CONFIG_DBN.".cfinventorydetail as CI
                        INNER JOIN ".CONFIG_DBN.".cfunit AS CU ON CU.unitunkid=CI.inv_unitid
                        LEFT JOIN ".CONFIG_DBN.".cfvandor AS CV ON CV.vandorunkid=CI.lnkvendorid
                        LEFT JOIN ".CONFIG_DBN.".trcontact AS TRC ON CV.lnkcontactid=TRC.contactunkid 
                        INNER JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS CL ON CI.lnkrawmateriallocid=CL.id AND CL.companyid=:companyid AND CL.storeid=:storeid
                        INNER JOIN ".CONFIG_DBN.".cfrawmaterial AS CR ON CI.lnkrawmaterialid=CR.id
                        INNER JOIN ".CONFIG_DBN.".cfunit AS CU1 ON CL.inv_unitid=CU1.unitunkid
                        LEFT JOIN ".CONFIG_DBN.".cfrawmaterial_unit_rel AS CUR ON CUR.lnkrawid=CR.id AND CUR.lnkunitid=CU.unitunkid AND CUR.companyid=:companyid  
                        LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CI.created_user=CFU1.userunkid AND CI.companyid=CFU1.companyid
                        WHERE CI.companyid=:companyid AND CI.storeid=:storeid 
                        AND CI.lnkrawmaterialid=:id ORDER BY CI.createddatetime DESC";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':storeid',CONFIG_SID);
            $dao->addParameter(':id',$rawid);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getInventoryRec - ".$e);
            return false;
        }
    }

    public function rawmateriallist($limit,$offset,$name,$isactive=0,$category='')
    {
        try
        {
            $this->log->logIt($this->module.' - rawmateriallist - '.$name);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT CR.companyid,CR.storeid,CR.name,IF(CRL.inventory<=CRL.min_limit AND CRL.min_limit!=0,'yes','no')as islimit,
                       CR.code,CR.hashkey,CRL.inventory,CRL.min_limit,CR.measuretype,CRL.rateperunit,CR.is_active,IFNULL(CFU1.username,'') AS createduser,
                       IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CR.createddatetime,'".$mysqlformat."'),'') as created_date,
                       IFNULL(DATE_FORMAT(CR.modifieddatetime,'".$mysqlformat."'),'') as modified_date
                       FROM ".CONFIG_DBN.".cfrawmaterial AS CR
                       LEFT JOIN ".CONFIG_DBN.".cfrawmaterial_loc as CRL ON CR.id=CRL.lnkrawmaterialid and CRL.storeid=:storeid
                       LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CR.created_user=CFU1.userunkid AND CR.companyid=CFU1.companyid
                       LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON CR.modified_user=CFU2.userunkid AND CR.modified_user=CFU2.userunkid AND CR.companyid=CFU2.companyid
                       WHERE CR.companyid=:companyid AND CR.is_deleted=0";

            if($name!="")
                $strSql .= " AND CR.name LIKE '%".$name."%'";
            if($category!="")
                $strSql .= " AND CR.lnkcategoryid = '".$category."'";
            if($isactive==1)
                $strSql .= " AND CR.is_active=1";
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $strSql .= " ORDER BY CR.id DESC";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':storeid',CONFIG_SID);
            $data = $dao->executeQuery();

            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);

            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':storeid',CONFIG_SID);
            $rec = $dao->executeQuery();

            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue));
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - rawmateriallist - '.$e);
        }
    }

    public function getRawmaterialRec($data)
    {
       try
       {
           $this->log->logIt($this->module." - getRawmaterialRec");
           $dao = new \dao();
           $hashkey=$data['id'];
           $round_off = \database\parameter::getParameter('digitafterdecimal');
           $strSql = "SELECT CR.name,CR.code,CR.id,CR.measuretype,CR.is_active,
                      ROUND((RL.min_limit/CU.unit),$round_off)as min_limit,RL.id as locid,
                      RC.id as category,CU.unit as unitval,
                      CU.unitunkid as unitid,CU.name as unit_name,CR.is_recipe
                      FROM ".CONFIG_DBN.".cfrawmaterial as CR
                      LEFT JOIN ".CONFIG_DBN.".cfrawmaterial_loc AS RL
                        ON RL.lnkrawmaterialid=CR.id and RL.storeid=:storeid
                      INNER JOIN ".CONFIG_DBN.".cfrawmaterial_category AS RC
                        ON RC.id=CR.lnkcategoryid 
                      LEFT JOIN ".CONFIG_DBN.".cfunit AS CU
                        ON CU.unitunkid=RL.min_unitid      
                      WHERE CR.hashkey=:id AND CR.companyid=:companyid";

           $dao->initCommand($strSql);
           $dao->addParameter(':id',$hashkey);
           $dao->addParameter(':companyid',CONFIG_CID);
           $dao->addparameter(':storeid',CONFIG_SID);
           $res = $dao->executeRow();
           $ObjCommonDao = new \database\commondao;
           $rawid = $ObjCommonDao->getprimaryBycompany('cfrawmaterial',$hashkey,'id');

           $strUnit = "SELECT lnkunitid,conversation_rate FROM ".CONFIG_DBN.".cfrawmaterial_unit_rel      
                  WHERE lnkrawid=:lnkrawid AND companyid=:companyid";
           $dao->initCommand($strUnit);
           $dao->addParameter(':lnkrawid',$rawid);
           $dao->addParameter(':companyid',CONFIG_CID);
           $res['bind_unit'] = $dao->executeQuery();

           return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
       }
       catch(Exception $e)
       {
           $this->log->logIt($this->module." - getRawmaterialRec - ".$e);
           return false;
       }
   }

    public function getUnitsBYmeasureType($data)
    {
        try
        {
            $this->log->logIt($this->module." - getUnitsBYmeasureType");
            $dao = new \dao();
            $measuretype=$data['measureval'];
            $strSql = "SELECT name,unitunkid,unit FROM ".CONFIG_DBN.".cfunit 
                       WHERE companyid=:companyid AND is_active=1 AND is_deleted=0 AND measuretype=:measuretype";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':measuretype',$measuretype);
            $res['unit'] = $dao->executeQuery();

            $strCode = "SELECT shortcode FROM ".CONFIG_DBN.".cfunit 
                       WHERE companyid=:companyid AND is_active=1 AND is_deleted=0 AND measuretype=:measuretype and is_systemdefined=1";
            $dao->initCommand($strCode);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':measuretype',$measuretype);
            $resCode = $dao->executeRow();
            $res['code']=$resCode;

	    $strSql = "SELECT name,unitunkid,unit FROM ".CONFIG_DBN.".cfunit WHERE companyid=:companyid
             and is_deleted=0 and is_active=1 and unitunkid not in (SELECT cu2.unitunkid FROM ".CONFIG_DBN.".cfunit as cu2 WHERE cu2.companyid=:companyid and cu2.is_deleted=0 and cu2.is_active=1 and cu2.measuretype=$measuretype and cu2.is_systemdefined=1)";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $res['allunits'] = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUnitsBYmeasureType - ".$e);
            return false;
        }
    }

    public function getUnitsBYrawmaterial($data)
    {
        try
        {
            $this->log->logIt($this->module." - getUnitsBYrawmaterial");
            $dao = new \dao();
            $measuretype=$data['measureval'];
            $ObjCommonDao = new \database\commondao();
            $rawid = $ObjCommonDao->getprimaryBycompany('cfrawmaterial',$data['rawhash'],'id');

            $strUnit = "SELECT CRUREL.lnkunitid as unitid,CRUREL.conversation_rate as unitrate,CU.name FROM ".CONFIG_DBN.".cfrawmaterial_unit_rel as CRUREL
                  INNER JOIN ".CONFIG_DBN.".cfunit as CU on CU.unitunkid=CRUREL.lnkunitid and CU.companyid=:companyid
                  WHERE CRUREL.lnkrawid=:lnkrawid AND CRUREL.companyid=:companyid";
            $dao->initCommand($strUnit);
            $dao->addParameter(':lnkrawid',$rawid);
            $dao->addParameter(':companyid',CONFIG_CID);
            $resUnit = $dao->executeQuery();

            if(!$resUnit) {
                $strUnit = "SELECT name,unitunkid as unitid,unit as unitrate FROM " . CONFIG_DBN . ".cfunit 
                       WHERE companyid=:companyid AND is_active=1 AND is_deleted=0 AND measuretype=:measuretype";
                $dao->initCommand($strUnit);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':measuretype', $measuretype);
                $resUnit = $dao->executeQuery();
            }

            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$resUnit)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUnitsBYrawmaterial - ".$e);
            return false;
        }
    }

    public function getUnitByStore($data)
    {
        try
        {
            $this->log->logIt($this->module." - getUnitByStore");
            $dao = new \dao();

            $strUnit = "SELECT CRUREL.lnkunitid as unitunkid,CRUREL.conversation_rate as unit,CU.name,CLOC.rateperunit,CLOC.inventory
                FROM ".CONFIG_DBN.".cfrawmaterial_unit_rel as CRUREL
                INNER JOIN ".CONFIG_DBN.".cfunit as CU on CU.unitunkid=CRUREL.lnkunitid and CU.companyid=:companyid
                LEFT JOIN ".CONFIG_DBN.".cfrawmaterial_loc as CLOC on CLOC.lnkrawmaterialid=CRUREL.lnkrawid and CLOC.companyid=:companyid and CLOC.storeid=:storeid
                WHERE CRUREL.lnkrawid=:lnkrawid AND CRUREL.companyid=:companyid";
            $dao->initCommand($strUnit);
            $dao->addParameter(':lnkrawid',$data['rawid']);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':storeid',$data['storeid']);
            $resUnit = $dao->executeQuery();

            if(!$resUnit) {
                $strSql = " SELECT U.name,U.unitunkid,U.unit,CLOC.rateperunit,CLOC.inventory
                       FROM " . CONFIG_DBN . ".cfrawmaterial CRM 
                       LEFT JOIN " . CONFIG_DBN . ". cfunit U ON CRM.measuretype=U.measuretype AND U.companyid=:companyid AND U.is_active=1 and U.is_deleted=0
                       LEFT JOIN ".CONFIG_DBN.".cfrawmaterial_loc as CLOC on CLOC.lnkrawmaterialid=CRM.id and CLOC.companyid=:companyid and CLOC.storeid=:storeid
                       WHERE CRM.id =:id AND CRM.companyid=:companyid ";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', $data['storeid']);
                $dao->addParameter(':id', $data['rawid']);
                $resUnit = $dao->executeQuery();
            }

            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$resUnit)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUnitByStore - ".$e);
            return false;
        }
    }

    public function getAllRawdetail()
    {
        try
        {
            $this->log->logIt($this->module." - getAllRawdetail");
            $dao = new \dao();
            $response_arr = array();

            $strSql = "SELECT CR.name,CR.hashkey,CR.id,CR.measuretype,CR.is_active
                       FROM ".CONFIG_DBN.".cfrawmaterial AS CR
                       WHERE CR.companyid=:companyid AND CR.is_deleted=0 AND CR.is_active=1";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $res = $dao->executeQuery();
            $raw_arr = $res;

            foreach($res AS $key=>$val) {

              /*  $strUnit = "SELECT CRUREL.lnkunitid as unitid,CRUREL.conversation_rate as unitrate,CU.name FROM ".CONFIG_DBN.".cfrawmaterial_unit_rel as CRUREL
                  INNER JOIN ".CONFIG_DBN.".cfunit as CU on CU.unitunkid=CRUREL.lnkunitid and CU.companyid=:companyid
                  WHERE CRUREL.lnkrawid=:lnkrawid AND CRUREL.companyid=:companyid AND CRUREL.storeid=:storeid";
                $dao->initCommand($strUnit);
                $dao->addParameter(':lnkrawid',$val['id']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':storeid',CONFIG_SID);
                $resUnit = $dao->executeQuery();
                $raw_arr[$key]['unit'] = $resUnit;

                if(!$resUnit) {*/
                    $strSql = "SELECT CU.name as unit_name,CU.unit,CU.unitunkid
                       FROM " . CONFIG_DBN . ".cfunit AS CU
                       WHERE CU.is_active=1 AND CU.is_deleted=0 AND CU.companyid=:companyid AND CU.measuretype=:measuretype";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addParameter(':measuretype', $val['measuretype']);
                    $res = $dao->executeQuery();
                    $raw_arr[$key]['unit'] = $res;
                //}

               /* $strSql = "SELECT rateperunit,storeid,inventory
                       FROM ".CONFIG_DBN.".cfrawmaterial_loc
                       WHERE is_deleted=0 AND companyid=:companyid AND lnkrawmaterialid=:rawmaterialid";

                $dao->initCommand($strSql);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addParameter(':rawmaterialid',$val['id']);
                $res = $dao->executeQuery();
                $raw_arr[$key]['storerate'] = $res;*/
            }
            $response_arr['rawmaterial']=$raw_arr;

           $strSql2 = "SELECT storename,storeunkid
                      FROM ".CONFIG_DBN.".cfstoremaster
                      WHERE companyid=:companyid AND is_deleted=0 AND is_active=1";

           $dao->initCommand($strSql2);
           $dao->addParameter(':companyid',CONFIG_CID);
           $res_store = $dao->executeQuery();
           $response_arr['store'] = $res_store;

           return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$response_arr)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getAllRawdetail - ".$e);
            return false;
        }
    }

    public function getRawdetailByCat($data)
    {
        try
        {
            $this->log->logIt($this->module." - getRawdetailByCat");
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();

            $catid = $ObjCommonDao->getprimaryBycompany('cfrawmaterial_category',$data['rawcat'],'id');
            $strSql = "SELECT CR.name,CR.hashkey
                       FROM ".CONFIG_DBN.".cfrawmaterial AS CR
                       WHERE CR.companyid=:companyid AND CR.is_deleted=0 AND CR.is_active=1
                       AND lnkcategoryid=:rawcat";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':rawcat',$catid);
            $res = $dao->executeQuery();

            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getRawdetailByCat - ".$e);
            return false;
        }
    }

    public function removematerial($data,$defaultlanguageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - remove');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $ObjDependencyDao = new \database\dependencydao();
            $datetime=\util\util::getLocalDateTime();

            if(count($data)>0 && isset($data['module']) && isset($data['id']))
            {
                $id =$ObjCommonDao->getprimaryBycompany('cfrawmaterial',$data['id'],'id');
                $check = $ObjDependencyDao->checkdependencyBYcompany($data['module'],$id);
                if($check>0){
                    return html_entity_decode(json_encode(array("Success"=>"False","Message"=>$defaultlanguageArr->PLZ_REM_REL_RECORD)));
                }
                //Delete rawmaterial
                $strSql = "UPDATE ".CONFIG_DBN.".cfrawmaterial SET is_deleted=1,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE hashkey=:id AND companyid=:companyid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id",$data['id']);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->executeNonQuery();

                //Delete Inventory
                $strSql = "DELETE FROM ".CONFIG_DBN.".cfinventorydetail WHERE lnkrawmaterialid=:id AND companyid=:companyid AND storeid=:storeid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id",$id);
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addparameter(':storeid',CONFIG_SID);
                $dao->executeNonQuery();

                //Delete total inventory
                $strSql = "DELETE FROM ".CONFIG_DBN.".cfrawmaterial_loc WHERE lnkrawmaterialid=:id AND companyid=:companyid AND storeid=:storeid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id",$id);
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addparameter(':storeid',CONFIG_SID);
                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'],$defaultlanguageArr->DEL_RECORD,$data['id'],'');
                return html_entity_decode(json_encode(array("Success"=>"True","Message"=>$defaultlanguageArr->REM_REC_SUC)));
            }
            else
                return html_entity_decode(json_encode(array("Success"=>"False","Message"=>$defaultlanguageArr->INTERNAL_ERROR)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - remove - '.$e);
        }
    }

    public function removeinventory($data,$languageArr,$defaultlanguageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - remove inventory');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $datetime=\util\util::getLocalDateTime();

            $strSql = "SELECT lnkrawmateriallocid,inventory,rate,lnkrawmaterialid FROM ".CONFIG_DBN.".cfinventorydetail WHERE id=:id AND companyid=:companyid AND storeid=:storeid";
            $dao->initCommand($strSql);
            $dao->addParameter(":id",$data['id']);
            $dao->addParameter(":companyid",CONFIG_CID);
            $dao->addParameter(":storeid",CONFIG_SID);
            $rawloclist = $dao->executeRow();
            $rawlocID = $rawloclist['lnkrawmateriallocid'];
            $inventory = $rawloclist['inventory'];
            $inv_price = $rawloclist['rate'];
            $rawID = $rawloclist['lnkrawmaterialid'];

            $strSql = "SELECT id,inventory FROM ".CONFIG_DBN.".cfrawmaterial_loc WHERE id=:rawlocid  AND companyid=:companyid AND storeid=:storeid";
            $dao->initCommand($strSql);
            $dao->addParameter(':rawlocid',$rawlocID);
            $dao->addParameter(":companyid",CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $loclist = $dao->executeRow();
            $locInventory=$loclist['inventory'];

            if($locInventory >= $inventory) {

                $strSql = "SELECT rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc WHERE lnkrawmaterialid=:id and companyid=:companyid and storeid=:storeid";
                $dao->initCommand($strSql);
                $dao->addParameter(':id',$rawID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':storeid', CONFIG_SID);
                $rawloclist = $dao->executeRow();
                $oldUnit= $rawloclist['rateperunit'];

                $oldPrice=($oldUnit*$locInventory);
                if($oldPrice >= $inv_price) {
                    $newPrice = $oldPrice - $inv_price;

                    $strSql = "UPDATE " . CONFIG_DBN . ".cfinventorydetail SET is_deleted=1 WHERE id=:id AND companyid=:companyid AND storeid=:storeid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":id", $data['id']);
                    $dao->addParameter(":companyid", CONFIG_CID);
                    $dao->addParameter(":storeid", CONFIG_SID);
                    $dao->executeNonQuery();

                    //Update total inventory and rate
                    $totalInventory = ($locInventory - $inventory);
                    if($totalInventory>0){
                        $rateperunit = $newPrice / $totalInventory;
                    }else{
                        $rateperunit = 0;
                    }

                    $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET inventory=:inventory,rateperunit=:rateperunit 
                                                                        WHERE id=:rawlocid 
                                                                        AND lnkrawmaterialid=:id
                                                                        AND storeid=:storeid
                                                                        AND companyid=:companyid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':inventory', $totalInventory);
                    $dao->addParameter(':rateperunit', $rateperunit);
                    $dao->addParameter(':rawlocid', $rawlocID);
                    $dao->addParameter(':id', $rawID);
                    $dao->addparameter(':storeid', CONFIG_SID);
                    $dao->addParameter(":companyid", CONFIG_CID);
                    $dao->executeNonQuery();

                    return html_entity_decode(json_encode(array("Success"=>"True","Message"=>$defaultlanguageArr->REM_REC_SUC)));
                }else{
                    return html_entity_decode(json_encode(array("Success"=>"False","Message"=>$languageArr->LANG28)));
                }
            }
            else{
                return html_entity_decode(json_encode(array("Success"=>"False","Message"=>$languageArr->LANG27)));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - remove inventory- '.$e);
        }
    }

    public function getAllrawmaterials($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getAllrawmaterials - ');
            $dao = new \dao;

            $strSql = "SELECT CR.name,CR.hashkey,CR.is_recipe
                       FROM ".CONFIG_DBN.".cfrawmaterial AS CR
                       WHERE CR.companyid=:companyid AND CR.is_deleted=0 AND CR.is_active=1 ";
            if($data['id'] != 0)
            {
                $strSql .= " AND CR.hashkey != :hashkey ";
            }
            $strSql .= " ORDER BY CR.id DESC ";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            if($data['id'] != 0)
            {
                $dao->addParameter(':hashkey',$data['id']);
            }
            $res = $dao->executeQuery();

            if(isset($res) && count($res) > 0)
            {
                return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
            }else
            {
                return html_entity_decode(json_encode(array("Success"=>"False")));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getAllrawmaterials - '.$e);
        }
    }

    public function getRawMaterialUnits($data)
    {
        try
        {
            $this->log->logIt($this->module." - getRawMaterialUnits");
            $dao = new \dao();

            $ObjCommonDao = new \database\commondao;

            $array_list = array();

            if(isset($data['raw_mat_id']) && count($data['raw_mat_id'])>0)
            {
                for ($i=0;$i<count($data['raw_mat_id']);$i++)
                {
                        $rawid = $ObjCommonDao->getprimaryBycompany('cfrawmaterial',$data['raw_mat_id'][$i],'id');

                        $strUnit = "SELECT RL.lnkunitid,CU.name as unit_name,RL.lnkrawid FROM ".CONFIG_DBN.".cfrawmaterial_unit_rel AS RL
                              LEFT JOIN ".CONFIG_DBN.".cfunit AS CU
                                    ON CU.unitunkid=RL.lnkunitid AND RL.companyid=:companyid
                              WHERE RL.lnkrawid=:lnkrawid AND RL.companyid=:companyid AND RL.is_deleted=0";
                        $dao->initCommand($strUnit);
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $dao->addParameter(':lnkrawid',$rawid);
                        $res = $dao->executeQuery();

                       $array_list[$data['raw_mat_id'][$i]] = $res;
                }
            }

            if(isset($array_list) && count($array_list) > 0)
            {
                return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$array_list)));
            }else
            {
                return html_entity_decode(json_encode(array("Success"=>"False")));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getRawMaterialUnits - ".$e);
            return false;
        }
    }
}
?>

