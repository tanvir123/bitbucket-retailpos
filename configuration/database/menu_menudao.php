<?php

namespace database;

use util\util;

class menu_menudao
{
    public $module = 'DB_menu_menudao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addMenu($data, $languageArr, $defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - addMenu');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();

            $datetime = \util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();
            $tablename = "cfmenu";
            $ObjDependencyDao = new \database\dependencydao();

            $arr_log = array(
                'Menu Name' => $data['name'],
                'Long Description' => $data['longdescription'],
                'Status' => ($data['rdo_status'] == 1) ? 'Active' : 'Inactive',
            );
            $json_data = json_encode($arr_log);
            
            if ($data['id'] == 0) {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "name", $data['name']);
                if ($chk_name == 1) {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->RECORD_ALREADY_EXISTS));
                }
                $title = "Add Record";
                $hashkey = \util\util::gethash();
                $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu (companyid,locationid, name, long_desc, image, createddatetime, created_user, is_active, hashkey, timings, zomato_itemrate)
                            VALUE(:companyid,:locationid, :name, :long_desc, :image, :createddatetime, :created_user, :is_active, :hashkey,:timings,:zomato_itemrate)";
                $dao->initCommand($strSql);
                $dao->addParameter(':name', $data['name']);
                $dao->addParameter(':long_desc', $data['longdescription']);
                $dao->addParameter(':image', ($data['image'] != '' ? $data['image'] : NULL));
                $dao->addParameter(':is_active', $data['rdo_status']);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->addParameter(':timings', $data['timings']);
                $dao->addParameter(':zomato_itemrate', $data['zomato_itemrate']);
                $dao->addParameter(':created_user', CONFIG_UID);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                $itemunkid = $dao->getLastInsertedId();

                $ObjAuditDao->addactivitylog($data['module'], $title, $hashkey, $json_data);

                return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_ADD_SUC));
            } else {
                $id = $ObjCommonDao->getprimarykey('cfmenu', $data['id'], 'menuunkid');
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "name", $data['name'], $id);
                if ($chk_name == 1) {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->RECORD_ALREADY_EXISTS));
                }
                $title = "Edit Record";
                $strSql = "UPDATE " . CONFIG_DBN . ".cfmenu SET name=:name, long_desc=:long_desc,image=:image, modifieddatetime=:modifieddatetime, modified_user=:modified_user, timings=:timings, zomato_itemrate=:zomato_itemrate WHERE hashkey=:menuunkid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':menuunkid', $data['id']);
                $dao->addParameter(':name', $data['name']);
                $dao->addParameter(':long_desc', $data['longdescription']);
                $dao->addParameter(':image', ($data['image'] != '' ? $data['image'] : NULL));
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':timings', $data['timings']);
                $dao->addParameter(':zomato_itemrate', $data['zomato_itemrate']);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'], $title, $data['id'], $json_data);

                return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_UP_SUC));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addMenu - ' . $e);
        }
    }

    public function menulist($limit, $offset, $name, $isactive = 0)
    {
        try {
            $this->log->logIt($this->module . ' - menulist - ' . $name);

            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = "SELECT CFM.*,IFNULL(CFU1.username,'') AS createduser,
                                    IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CFM.createddatetime,'" . $mysqlformat . "'),'') as created_date,
                                    IFNULL(DATE_FORMAT(CFM.modifieddatetime,'" . $mysqlformat . "'),'') as modified_date
                                    FROM " . CONFIG_DBN . ".cfmenu AS CFM
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU1 ON CFM.created_user=CFU1.userunkid AND CFM.companyid=CFU1.companyid
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU2 ON CFM.modified_user=CFU2.userunkid AND CFM.modified_user=CFU2.userunkid AND CFM.companyid=CFU2.companyid
                                    WHERE CFM.companyid=:companyid AND CFM.locationid=:locationid AND CFM.is_deleted=0";

            if ($name != "")
                $strSql .= " AND name LIKE '%" . $name . "%'";
            if ($isactive == 1) {
                $strSql .= " AND is_active=1 ";
            }
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $data = $dao->executeQuery();

            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return json_encode($retvalue);
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - menulist - ' . $e);
        }
    }

    public function getMenuRec($data)
    {
        try {
            $this->log->logIt($this->module . " - getMenuRec");
            $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $id = (isset($data['id'])) ? $data['id'] : "";

            $ObjCommonDao = new \database\commondao();
            $menuid = $ObjCommonDao->getprimarykey('cfmenu', $id, 'menuunkid');

            $strSql = "SELECT name,long_desc,IFNULL(image,'" . CONFIG_ASSETS_URL . "noimage.png') as image, is_active,timings,zomato_itemrate FROM " . CONFIG_DBN . ".cfmenu WHERE hashkey=:menuunkid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':menuunkid', $id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $response_array['menu'] = $dao->executeRow();

            /*$restmp = [];
             array_walk($res,function($val,$key) use (&$restmp){
                 $restmp[$key] = html_entity_decode($val);
            });*/
            return json_encode(array("Success" => "True", "Data" => $response_array));

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getMenuRec - " . $e);
            return false;
        }
    }

    public function getCategory($data)
    {
        try {
            $this->log->logIt($this->module . " - getCategory");
            $dao = new \dao();

            $ObjCommonDao = new \database\commondao();
            $id = $ObjCommonDao->getprimarykey('cfmenu', $data['id'], 'menuunkid');

            $strSql = "SELECT categoryunkid,categoryname FROM  " . CONFIG_DBN . ".cfmenu_categories 
            WHERE is_active=1 AND is_deleted=0 AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeQuery();

            $str="SELECT breakfast_category_id,lunch_category_id,dinner_category_id   FROM  " . CONFIG_DBN . ".cfmenu
            WHERE menuunkid =:menuunkid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($str);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->addparameter(':menuunkid', $id);
            $selectedcategory = $dao->executeRow();


            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res,"selectedcategory"=>$selectedcategory)));

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getCategory - " . $e);
            return false;
        }
    }

    public function addCategory($data, $languageArr, $defaultlanguageArr)
{
    try {
        $this->log->logIt($this->module . ' - addCategory');
        $dao = new \dao();
        $ObjCommonDao = new \database\commondao();

        $datetime = \util\util::getLocalDateTime();
        $ObjAuditDao = new \database\auditlogdao();

        $arr_log = array(
            'Breakfast' => $data['breakfast_name'],
            'Lunch' => $data['lunch_name'],
            'Dinner' => $data['dinner_name'],
        );
        $json_data = json_encode($arr_log);

            $id = $ObjCommonDao->getprimarykey('cfmenu', $data['id'], 'menuunkid');

            $title = "Edit Record";
            $strSql = "UPDATE " . CONFIG_DBN . ".cfmenu SET breakfast_category_id=:breakfast_category_id, 
            lunch_category_id=:lunch_category_id,dinner_category_id=:dinner_category_id, 
            modifieddatetime=:modifieddatetime, modified_user=:modified_user  
            WHERE menuunkid=:menuunkid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':menuunkid', $id);
            $dao->addParameter(':breakfast_category_id', $data['breakfast_category_id']);
            $dao->addParameter(':lunch_category_id', $data['lunch_category_id']);
            $dao->addParameter(':dinner_category_id', $data['dinner_category_id']);
            $dao->addParameter(':modifieddatetime', $datetime);
            $dao->addParameter(':modified_user', CONFIG_UID);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $dao->executeNonQuery();
            $ObjAuditDao->addactivitylog($data['module'], $title, $data['id'], $json_data);
            return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_UP_SUC));

    } catch (Exception $e) {
        $this->log->logIt($this->module . ' - addCategory - ' . $e);
    }
}


}

?>