<?php
namespace database;

class vendordao
{
    public $module = 'DB_vendor';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function addVendor($data,$languageArr,$defaultlanguageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - addVendor');
            $dao = new \dao();
            $datetime=\util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $ObjDependencyDao = new \database\dependencydao();
            $tablename =  "cfvandor";

            $arr_log = array(
                'Name'=>$data['name'],
                'Business Name'=>$data['company'],
                'Mobile'=>$data['mobile'],
                'Email'=>$data['email'],
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive'
            );
            $json_data = html_entity_decode(json_encode($arr_log));

            if($data['vendorid']==0)
            {
                $chk_name = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"reg_no",$data['reg_no']);
                if($chk_name==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG16)));
                }
                $title = "Add Record";
                $hashkey = \util\util::gethash();

                $strSql = "INSERT INTO ".CONFIG_DBN.".trcontact(salutation,`name`, 
                                                                    business_name, 
                                                                    email, 
                                                                    address,
                                                                    country,
                                                                    state,
                                                                    city,
                                                                    zip,
                                                                    mobile,
                                                                    phone,
                                                                    fax,
                                                                    contacttype,
                                                                    companyid,
                                                                    storeid,
                                                                    hashkey)
                                                            VALUE(  :salutation, 
                                                                    :name, 
                                                                    :company, 
                                                                    :email,
                                                                    :address,
                                                                    :country,
                                                                    :state,
                                                                    :city,
                                                                    :zip,
                                                                    :mobile,
                                                                    :phone,
                                                                    :fax,
                                                                    :contacttype,
                                                                    :companyid,
                                                                    :storeid,
                                                                    :hashkey)";
                
                $dao->initCommand($strSql);
                $dao->addParameter(':salutation',$data['salutation']);
                $dao->addParameter(':name',$data['name']);
                $dao->addParameter(':company', $data['company']);
                $dao->addParameter(':email', $data['email']);
                $dao->addParameter(':address', $data['address']);
                $dao->addParameter(':country', $data['country']);
                $dao->addParameter(':state', $data['state']);
                $dao->addParameter(':city', $data['city']);
                $dao->addParameter(':zip', $data['zip']);
                $dao->addParameter(':mobile', $data['mobile']);
                $dao->addParameter(':phone', $data['phone']);
                $dao->addParameter(':fax', $data['fax']);
                $dao->addParameter(':contacttype', 3);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':storeid',CONFIG_SID);
                $dao->executeNonQuery();
                $lastid = $dao->getLastInsertedId();
                if($lastid){
                    $hashkey = \util\util::gethash();
                    $strVendor = "INSERT INTO ".CONFIG_DBN.".cfvandor(lnkcontactid,
                                                                    reg_no, 
                                                                    is_active,
                                                                    created_user,
                                                                    createddatetime,
                                                                    companyid,
                                                                    storeid,
                                                                    hashkey)
                                                            VALUE(  :lnkcontactid, 
                                                                    :reg_no, 
                                                                    :rdo_status,
                                                                    :created_user,
                                                                    :createddatetime,
                                                                    :companyid,
                                                                    :storeid,
                                                                    :hashkey)";
                    $dao->initCommand($strVendor);

                    $dao->addParameter(':lnkcontactid',$lastid);
                    $dao->addParameter(':rdo_status',$data['rdo_status']);
                    $dao->addParameter(':created_user',CONFIG_UID);
                    $dao->addParameter(':createddatetime',$datetime);
                    $dao->addParameter(':reg_no',$data['reg_no']);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addparameter(':storeid',CONFIG_SID);
                    $dao->addParameter(':hashkey', $hashkey);
                    $dao->executeNonQuery();
                    $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                    return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_ADD_SUC)));
                }
           }
           else
           {
               $vendorid =$ObjCommonDao->getprimaryBycompany('cfvandor',$data['vendorid'],'vandorunkid');
               $chk_name = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"reg_no",$data['reg_no'],$vendorid);
               if($chk_name==1)
               {
                   return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG16)));
               }
               $title = "Edit Record";
               $strSql = "UPDATE ".CONFIG_DBN.".trcontact SET salutation=:salutation,
                                                                    name=:name,
                                                                    business_name=:company,
                                                                    email=:email,
                                                                    address=:address,
                                                                    country=:country,
                                                                    state=:state,
                                                                    city=:city,
                                                                    zip=:zip,
                                                                    mobile=:mobile,
                                                                    phone=:phone,
                                                                    fax=:fax,
                                                                    modifieddatetime=:modifieddatetime,
                                                                    modified_user=:modified_user
                                                                    WHERE hashkey=:contactid AND companyid=:companyid";
               $dao->initCommand($strSql);
               $dao->addParameter(':contactid',$data['contactid']);
               $dao->addParameter(':salutation',$data['salutation']);
               $dao->addParameter(':name',$data['name']);
               $dao->addParameter(':company', $data['company']);
               $dao->addParameter(':email', $data['email']);
               $dao->addParameter(':address', $data['address']);
               $dao->addParameter(':country', $data['country']);
               $dao->addParameter(':state', $data['state']);
               $dao->addParameter(':city', $data['city']);
               $dao->addParameter(':zip', $data['zip']);
               $dao->addParameter(':mobile', $data['mobile']);
               $dao->addParameter(':phone', $data['phone']);
               $dao->addParameter(':fax', $data['fax']);
               $dao->addParameter(':modifieddatetime',$datetime);
               $dao->addParameter(':modified_user',CONFIG_UID);
               $dao->addParameter(':companyid',CONFIG_CID);
               $dao->executeNonQuery();

               $strSql = "UPDATE ".CONFIG_DBN.".cfvandor SET reg_no=:reg_no,
                                                                    modifieddatetime=:modifieddatetime,
                                                                    modified_user=:modified_user
                                                                    WHERE hashkey=:vandorunkid AND companyid=:companyid";
               $dao->initCommand($strSql);

               $dao->addParameter(':vandorunkid',$data['vendorid']);
               $dao->addParameter(':reg_no',$data['reg_no']);
               $dao->addParameter(':modifieddatetime',$datetime);
               $dao->addParameter(':modified_user',CONFIG_UID);
               $dao->addParameter(':companyid',CONFIG_CID);
               $dao->executeNonQuery();
               $ObjAuditDao->addactivitylog($data['module'],$title,$data['vendorid'],$json_data);
               return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_UP_SUC)));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addVendor - '.$e);
        }
    }
    public function vendorlist($limit,$offset,$name,$isactive=0)
    {
        try
        {
            $this->log->logIt($this->module.' - vendorlist - '.$name);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT CV.vandorunkid,TRC.hashkey as contactkey,CV.hashkey,CV.reg_no,TRC.name,TRC.business_name,IFNULL(TRC.mobile,'') as mobile,IFNULL(USR1.username,'') AS createduser,IFNULL(USR2.username,'') AS modifieduser".
                " ,IFNULL(DATE_FORMAT(CV.createddatetime,'".$mysqlformat."'),'') as created_date".
                " ,IFNULL(DATE_FORMAT(CV.modifieddatetime,'".$mysqlformat."'),'') as modified_date".
                " ,CV.is_active
                        FROM ".CONFIG_DBN.".cfvandor AS CV
                        LEFT JOIN ".CONFIG_DBN.".trcontact AS TRC
                            ON CV.lnkcontactid=TRC.contactunkid AND TRC.companyid=:companyid
                        LEFT JOIN ".CONFIG_DBN.".cfuser as USR1
                            ON USR1.userunkid = CV.created_user AND USR1.companyid=:companyid
                        LEFT JOIN ".CONFIG_DBN.".cfuser as USR2
                            ON USR2.userunkid=CV.modified_user AND USR2.companyid=:companyid
                        WHERE CV.companyid=:companyid AND CV.is_deleted=0 AND TRC.contacttype=3";

            if($name!="")
                $strSql .= " AND name LIKE '%".$name."%'";
            if($isactive==1)
                $strSql .= " AND is_active=1";
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $rec = $dao->executeQuery();

            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue));
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - vendorlist - '.$e);
        }
    }

    public function getrelatedRecords($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getRelatedRecords');
            $dao = new \dao();
            $result = array();
            $ObjCommonDao = new \database\commondao();
            if($data['id']!="")
            {
                $id = $ObjCommonDao->getprimaryBycompany('cfvandor',$data['id'],'vandorunkid');

                //Check Rawmaterial
                $strSql = "SELECT GROUP_CONCAT(CR.name) AS subject FROM ".CONFIG_DBN.".cfrawmaterial CR
                            INNER JOIN ".CONFIG_DBN.".cfvandor CV ON CV.vandorunkid=CR.lnkvandorid
                            WHERE CV.vandorunkid =:id AND CV.is_deleted=0 AND CR.is_deleted=0 
                            AND CV.companyid=:companyid  
                            and CR.companyid=:companyid 
                            ORDER BY CR.id";

                $dao->initCommand($strSql);
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addParameter(":id",$id);
                $resobj = $dao->executeRow();
                $arr_item = array();
                if($resobj['subject']!=""){
                    $arr_item['subject'] = $resobj['subject'];
                    $arr_item['module'] = \common\staticarray::$getmodulename['cfrawmaterial']['Display_Name'];
                    $result[] = $arr_item;
                }

                return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$result)));
            }
            else{
                return html_entity_decode(json_encode(array("Success"=>"False","Data"=>"")));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getRelatedRecords - '.$e);
        }
    }

    public function getVendorRec($data)
    {
        try
        {
            $this->log->logIt($this->module." - getVendorRec");
            $dao = new \dao();
            $vendorid=$data['vendorid'];
            $strSql = "SELECT CV.vandorunkid,TRC.hashkey as contactkey,CV.hashkey,CV.reg_no,TRC.salutation,TRC.name,TRC.shortcode,TRC.business_name,TRC.address,".
                "TRC.city,TRC.state,TRC.country,TRC.zip,TRC.mobile,TRC.phone,TRC.fax,TRC.email".
                        ",CV.is_active
                        FROM ".CONFIG_DBN.".cfvandor AS CV
                        LEFT JOIN ".CONFIG_DBN.".trcontact AS TRC
                            ON CV.lnkcontactid=TRC.contactunkid 
                        WHERE CV.hashkey=:vandorunkid AND CV.companyid=:companyid";

            $dao->initCommand($strSql);
            $dao->addParameter(':vandorunkid',$vendorid);
            $dao->addParameter(':companyid',CONFIG_CID);
            $res = $dao->executeRow();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getVendorRec - ".$e);
            return false;
        }
    }

    public function getAllVendor()
    {
        try
        {
            $this->log->logIt($this->module." - getAllVendor");
            $dao = new \dao();
            $strSql = "SELECT TRC.hashkey,CV.vandorunkid,TRC.name
                        FROM ".CONFIG_DBN.".cfvandor AS CV
                        LEFT JOIN ".CONFIG_DBN.".trcontact AS TRC
                            ON CV.lnkcontactid=TRC.contactunkid 
                        WHERE CV.companyid=:companyid and CV.is_active=1 and CV.is_deleted=0";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getAllVendor - ".$e);
            return false;
        }
    }
}
?>