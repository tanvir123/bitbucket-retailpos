<?php

namespace database;
class commondao
{
    public $module = 'DB_commondao';
    public $log;
    public $dbconnect;
    private $language, $default_lang_arr,$defaultlanguageArr;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language();
        $this->default_lang_arr = $this->language->loaddefaultlanguage();
        $this->defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
        $this->defaultlanguageArr = json_decode($this->defaultlanguageArr);

    }

    public function getCountryList()
    {
        try {
            $this->log->logIt($this->module . ' - getCountrylist');
            $dao = new \dao();
            $strSql = "SELECT id, countryName FROM " . CONFIG_DBN . ".vwcountry";
            $dao->initCommand($strSql);
            $res = $dao->executeQuery();
            $retarray = array();
            foreach ($res AS $key => $value) {
                $retarray[$value['id']] = $value['countryName'];
            }

            return $retarray;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getCountryList - ' . $e);
        }
    }

    public function toggleststus($data)
    {
        try {
            $this->log->logIt($this->module . ' - toggleststus');
            $dao = new \dao();

            $ObjAuditDao = new \database\auditlogdao();
            if (count($data) > 0 && isset($data['module']) && isset($data['id'])) {
                $datetime = \util\util::getLocalDateTime();
                $Objstaticarray = new \common\staticarray();
                $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $pd = $Objstaticarray->auditlogmodules[$data['module']]['pd'];
                $cn = $Objstaticarray->auditlogmodules[$data['module']]['status_cn'];
                $ObjDependencyDao = new \database\dependencydao();
                $id = $this->getprimarykey($table, $data['id'], $pd);

                if ($data['value'] == 1) {
                    $check = $ObjDependencyDao->checkstatusdependency($data['module'], $id, 1, 1);
                    if ($check > 0) {
                        return html_entity_decode(json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->PLZ_CHANGE_STATUS_REL_REC)));
                    }
                }
                $arr_log = array(
                    'Old Value' => ($data['value'] == 1) ? 'Active' : 'Inactive',
                    'New Value' => ($data['value'] == 1) ? 'InActive' : 'Active',
                );
                $json_data = json_encode($arr_log);
                if ($data['module'] == 'user') {
                    $loginid=CONFIG_LID;
                    $strSql = "UPDATE " . CONFIG_DBN . "." . $table . " SET modified_user=:modified_user, modifieddatetime=:modifieddatetime, " . $cn . " = IF(" . $cn . "=1,0,1) WHERE hashkey=:id AND companyid=:companyid AND FIND_IN_SET(:locationid,locationid)";
                } else {
                    if(CONFIG_LOGINTYPE==2){
                        $loginid=CONFIG_SID;
                        $strSql = "UPDATE " . CONFIG_DBN . "." . $table . " SET modified_user=:modified_user, modifieddatetime=:modifieddatetime, " . $cn . " = IF(" . $cn . "=1,0,1) WHERE hashkey=:id AND companyid=:companyid AND storeid=:locationid";
                    }
                    else {
                        $loginid=CONFIG_LID;
                        $strSql = "UPDATE " . CONFIG_DBN . "." . $table . " SET modified_user=:modified_user, modifieddatetime=:modifieddatetime, " . $cn . " = IF(" . $cn . "=1,0,1) WHERE hashkey=:id AND companyid=:companyid AND locationid=:locationid";
                    }
                }
                $dao->initCommand($strSql);
                $dao->addParameter(":id", $data['id']);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->addparameter(':locationid', $loginid);
                $dao->executeNonQuery();
		$ObjAuditDao->addactivitylog($data['module'], 'Change Status', $data['id'], $json_data);
                return html_entity_decode(json_encode(array("Success" => "True", "Message" => $this->defaultlanguageArr->STATUS_CHANGE_SUCCESS)));
            } else
                return html_entity_decode(json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->INTERNAL_ERROR)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - toggleststus - ' . $e);
        }
    }

    public function toggleststusBycompany($data)
    {
        try {
            $this->log->logIt($this->module . ' - toggleststusBycompany');
            $dao = new \dao();
            $ObjAuditDao = new \database\auditlogdao();
            if (count($data) > 0 && isset($data['module']) && isset($data['id'])) {
                $datetime = \util\util::getLocalDateTime();
                $Objstaticarray = new \common\staticarray();
                $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $pd = $Objstaticarray->auditlogmodules[$data['module']]['pd'];
                $cn = $Objstaticarray->auditlogmodules[$data['module']]['status_cn'];
                $ObjDependencyDao = new \database\dependencydao();
                $id = $this->getprimaryBycompany($table,$data['id'],$pd);

                if ($data['value'] == 1) {
                    $check = $ObjDependencyDao->checkstatusdependency($data['module'], $id, 1, 1);
                    if ($check > 0) {
                        return html_entity_decode(json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->PLZ_CHANGE_STATUS_REL_REC)));
                    }
                }
                $arr_log = array(
                    'Old Value' => ($data['value'] == 1) ? 'Active' : 'Inactive',
                    'New Value' => ($data['value'] == 1) ? 'InActive' : 'Active',
                );
                $json_data = json_encode($arr_log);

                $strSql = "UPDATE " . CONFIG_DBN . "." . $table . " SET modified_user=:modified_user, modifieddatetime=:modifieddatetime, " . $cn . " = IF(" . $cn . "=1,0,1) WHERE hashkey=:id AND companyid=:companyid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id", $data['id']);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'], 'Change Status', $data['id'], $json_data);
                return html_entity_decode(json_encode(array("Success" => "True", "Message" => $this->defaultlanguageArr->STATUS_CHANGE_SUCCESS)));
            } else
                return html_entity_decode(json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->INTERNAL_ERROR)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - toggleststusBycompany - ' . $e);
        }
    }

    public function remove($data)
    {
        try {
            $this->log->logIt($this->module . ' - remove');
            $dao = new \dao();
            $ObjDependencyDao = new \database\dependencydao();
            $ObjAuditDao = new \database\auditlogdao();
            if (count($data) > 0 && isset($data['module']) && isset($data['id'])) {
                $datetime = \util\util::getLocalDateTime();
                $Objstaticarray = new \common\staticarray();
                $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $pd = $Objstaticarray->auditlogmodules[$data['module']]['pd'];
                $cn = $Objstaticarray->auditlogmodules[$data['module']]['delete_cn'];
                $id = $this->getprimarykey($table, $data['id'], $pd);
                $check = $ObjDependencyDao->checkdependency($data['module'], $id,1);
                if ($check > 0) {
                    return html_entity_decode(json_encode(array("Success" => "False", "Message" =>$this->defaultlanguageArr->PLZ_REM_REL_RECORD)));
                }
                if(CONFIG_LOGINTYPE==2) {
                    $loginid = CONFIG_SID;
                    $strSql = "UPDATE " . CONFIG_DBN . "." . $table . " SET " . $cn . "=1,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE hashkey=:id AND companyid=:companyid AND storeid=:locationid";
                }
                else{
                    $loginid = CONFIG_LID;
                    $strSql = "UPDATE " . CONFIG_DBN . "." . $table . " SET " . $cn . "=1,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE hashkey=:id AND companyid=:companyid AND locationid=:locationid";
                }
                $dao->initCommand($strSql);
                $dao->addParameter(":id", $data['id']);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->addparameter(':locationid', $loginid);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'], 'Delete Record', $data['id'], '');
                return html_entity_decode(json_encode(array("Success" => "True", "Message" => $this->defaultlanguageArr->REM_REC_SUC)));
            } else
                return html_entity_decode(json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->INTERNAL_ERROR)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - remove - ' . $e);
        }
    }

    public function removeByCompany($data)
    {
        try {
            $this->log->logIt($this->module . ' - remove');
            $dao = new \dao();
            $ObjDependencyDao = new \database\dependencydao();
            $ObjAuditDao = new \database\auditlogdao();
            if (count($data) > 0 && isset($data['module']) && isset($data['id'])) {
                $datetime = \util\util::getLocalDateTime();
                $Objstaticarray = new \common\staticarray();
                $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $pd = $Objstaticarray->auditlogmodules[$data['module']]['pd'];
                $cn = $Objstaticarray->auditlogmodules[$data['module']]['delete_cn'];
                $id = $this->getprimaryBycompany($table, $data['id'], $pd);
                $check = $ObjDependencyDao->checkdependencyBYcompany($data['module'], $id);
                if ($check > 0) {
                    return html_entity_decode(json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->PLZ_REM_REL_RECORD)));
                }
                $strSql = "UPDATE " . CONFIG_DBN . "." . $table . " SET " . $cn . "=1,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE hashkey=:id AND companyid=:companyid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id", $data['id']);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'], 'Delete Record', $data['id'], '');
                return html_entity_decode(json_encode(array("Success" => "True", "Message" => $this->defaultlanguageArr->REM_REC_SUC)));
            } else
                return html_entity_decode(json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->INTERNAL_ERROR)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - remove - ' . $e);
        }
    }

    public function removetable($data)
    {
        try {
            $this->log->logIt($this->module . ' - removetable');
            $dao = new \dao();
            $ObjDependencyDao = new \database\dependencydao();
            $ObjAuditDao = new \database\auditlogdao();
            if (count($data) > 0 && isset($data['module']) && isset($data['id'])) {
                $datetime = \util\util::getLocalDateTime();
                $Objstaticarray = new \common\staticarray();
                $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $pd = $Objstaticarray->auditlogmodules[$data['module']]['pd'];
                $cn = $Objstaticarray->auditlogmodules[$data['module']]['delete_cn'];
                $id = $this->getprimarykey($table, $data['id'], $pd);
                $check = $ObjDependencyDao->checkdependencytable($data['module'], $id);
                if ($check > 0) {
                    return html_entity_decode(json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->PLZ_REM_REL_RECORD)));
                }
                $strSql = "UPDATE " . CONFIG_DBN . "." . $table . " SET " . $cn . "=1,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE hashkey=:id AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(":id", $data['id']);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'], 'Delete Record', $data['id'], '');
                return html_entity_decode(json_encode(array("Success" => "True", "Message" => $this->defaultlanguageArr->REM_REC_SUC)));
            } else
                return html_entity_decode(json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->INTERNAL_ERROR)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - removetable - ' . $e);
        }
    }

    public function getprimarykey($tablename, $hashkey, $field)
    {
        try {
            $this->log->logIt($this->module . ' - getprimarykey');
            $dao = new \dao;
            if ($tablename == 'cfuser') {
                $locationid=CONFIG_LID;
                $strSql = "SELECT " . $field . " AS field FROM " . CONFIG_DBN . "." . $tablename . " WHERE hashkey=:hashkey AND companyid=:companyid AND FIND_IN_SET(:locationid,locationid)";
            } else {
                if(CONFIG_LOGINTYPE==2){
                    $locationid=CONFIG_SID;
                    $strSql = "SELECT " . $field . " AS field FROM " . CONFIG_DBN . "." . $tablename . " WHERE hashkey=:hashkey AND companyid=:companyid AND storeid=:locationid";
                }
                else{
                    $locationid=CONFIG_LID;
                    $strSql = "SELECT " . $field . " AS field FROM " . CONFIG_DBN . "." . $tablename . " WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid";
                }
            }
            $dao->initCommand($strSql);
            $dao->addParameter(":hashkey", $hashkey);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', $locationid);
            $data = $dao->executeRow();
            return $data['field'];
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getprimarykey - ' . $e);
        }

    }

    public function getprimaryBycompany($tablename, $hashkey, $field)
    {
        try {
            $this->log->logIt($this->module . ' - getprimarykeyBYcompany');
            $dao = new \dao;
            $strSql = "SELECT " . $field . " AS field FROM " . CONFIG_DBN . "." . $tablename . " WHERE hashkey=:hashkey AND companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(":hashkey", $hashkey);
            $dao->addParameter(':companyid', CONFIG_CID);
            $data = $dao->executeRow();
            return $data['field'];
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getprimarykey - ' . $e);
        }
    }

    public function getmastertype($type)
    {
        try {
            $this->log->logIt($this->module . ' - getmastertype');
            $dao = new \dao;
            $strSql = "SELECT masterunkid,crdr FROM " . CONFIG_DBN . ".fasmaster WHERE mastertypeunkid=:type AND companyid=:companyid AND locationid=:locationid and isactive=1 and is_deleted=0";
            $dao->initCommand($strSql);
            $dao->addParameter(":type", $type);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $data = $dao->executeRow();
            return array(
                'MasterId' => $data['masterunkid'],
                'Cr_Dr' => $data['crdr']
            );
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getmastertype - ' . $e);
        }
    }

    public function getFieldValue($table, $column, $field, $value)
    {
        try {
            $this->log->logIt($this->module . ' - getFieldValue');
            $dao = new \dao;
            if($table =='vwcountry')
            {
                $strSql = "SELECT " . $column . " as field FROM " . CONFIG_DBN . "." . $table . " WHERE " . $field . "=:value";
            }
            else{
                $strSql = "SELECT " . $column . " as field FROM " . CONFIG_DBN . "." . $table . " WHERE " . $field . "=:value AND companyid=:companyid AND locationid=:locationid";
            }


            $dao->initCommand($strSql);
            $dao->addParameter(":value", $value);
            if($table !='vwcountry') {
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
            }
            $data = $dao->executeRow();
            return $data['field'];
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getFieldValue - ' . $e);
        }
    }

    public function getusermandatoryfields($value)
    {
        try {
            //$this->log->logIt($this->module.' - getusermandatoryfields');
            $res = "";
            if ($value != "" || $value !=0) {
                $ObjStaticArray = new \common\staticarray();
                $arr_fields = explode(',', $value);
                $user_fields = array();
                if($arr_fields)
                {
                    foreach ($arr_fields as $value) {
                        if(isset($value) && $value !=0)
                        $user_fields[] = $ObjStaticArray->usermandatoryinfo[$value];
                    }

                    if (!empty($user_fields) && count($user_fields) > 0) {
                        $res = implode(', ', $user_fields);
                    }
                }
            }
            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getusermandatoryfields - ' . $e);
        }
    }

    public function getCurrencySign()
    {
        try {
            $this->log->logIt($this->module . ' - getCurrencySign');
            $dao = new \dao();
            $strSql = "SELECT currency_sign FROM " . CONFIG_DBN . ".cflocation WHERE companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $res = $dao->executeRow();
            return $res['currency_sign'];
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getCurrencySign - ' . $e);
        }
    }
    public function getCurrencySignCompany()
    {
        try {
            $this->log->logIt($this->module . ' - getCurrencySign');
            $dao = new \dao();
            $strSql = "SELECT currency_sign FROM " . CONFIG_DBN . ".cfcompany WHERE companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);

            $res = $dao->executeRow();
            return $res['currency_sign'];
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getCurrencySign - ' . $e);
        }
    }

    public function getuserprivongroup($groupidforprivleges)
    {
        try {
            $this->log->logIt($this->module . ' - getuserprivongroup');
            $dao = new \dao();

            if(CONFIG_LOGINTYPE ==1)
            {
                if (CONFIG_USR_TYPE != 1) {
                    $strSql = "SELECT GROUP_CONCAT(DISTINCT(CRP.lnkprivilegeid),'') as lnkprivilegegroupid 
                        FROM " . CONFIG_DBN . ".cfroleprivileges as CRP 
                       INNER JOIN sysprivilegegroup as SPG ON CRP.lnkprivilegegroupid=SPG.privilegegroupunkid 
                      WHERE  CRP.companyid=:companyid
                      AND lnkprivilegegroupid=:lnkprivilegegroupid AND lnkroleid=:lnkroleid AND SPG.type=3";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':lnkroleid', CONFIG_RID);
                    $dao->addParameter(':lnkprivilegegroupid', $groupidforprivleges);
                    $res = $dao->executeRow();
                }
                else{
                    $strSql = "SELECT GROUP_CONCAT(DISTINCT(SP.id),'') as lnkprivilegegroupid 
                        FROM sysprivileges as SP 
                       INNER JOIN sysprivilegegroup as SPG ON SP.privilegegroupid=SPG.privilegegroupunkid 
                      WHERE privilegegroupid=:lnkprivilegegroupid AND SPG.type=3";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':lnkprivilegegroupid', $groupidforprivleges);
                    $res = $dao->executeRow();
                }


            }
            else{
                if (CONFIG_USR_TYPE != 1) {
                    $strSql = "SELECT GROUP_CONCAT(DISTINCT(CRP.lnkprivilegeid),'') as lnkprivilegegroupid 
                        FROM " . CONFIG_DBN . ".cfroleprivileges as CRP 
                       INNER JOIN sysprivilegegroup as SPG ON CRP.lnkprivilegegroupid=SPG.privilegegroupunkid 
                      WHERE  CRP.companyid=:companyid
                      AND lnkprivilegegroupid=:lnkprivilegegroupid AND lnkroleid=:lnkroleid AND SPG.type=2";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':lnkroleid', CONFIG_RID);
                    $dao->addParameter(':lnkprivilegegroupid', $groupidforprivleges);
                    $res = $dao->executeRow();
                }
                else{
                    $strSql = "SELECT GROUP_CONCAT(DISTINCT(SP.id),'') as lnkprivilegegroupid 
                        FROM sysprivileges as SP 
                       INNER JOIN sysprivilegegroup as SPG ON SP.privilegegroupid=SPG.privilegegroupunkid 
                      WHERE privilegegroupid=:lnkprivilegegroupid AND SPG.type=2";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':lnkprivilegegroupid', $groupidforprivleges);
                    $res = $dao->executeRow();
                }

            }
            /*$strSql = "SELECT GROUP_CONCAT(DISTINCT(CRP.lnkprivilegeid),'') as lnkprivilegegroupid
                        FROM " . CONFIG_DBN . ".cfroleprivileges as CRP 
                       INNER JOIN sysprivilegegroup as SPG ON CRP.lnkprivilegegroupid=SPG.privilegegroupunkid 
                      WHERE  CRP.companyid=:companyid
                      AND lnkprivilegegroupid=:lnkprivilegegroupid AND CRP.lnkroleid=:lnkroleid";
            $dao->initCommand($strSql);
            $dao->addParameter(':lnkroleid', CONFIG_RID);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':lnkprivilegegroupid', $groupidforprivleges);
            $res = $dao->executeRow();*/
            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getuserprivongroup - ' . $e);
        }

    }

    public function getListForSwitch()
    {
        try
        {
            $this->log->logIt($this->module.' - getListForSwitch');
            $dao = new \dao();
            $strSql1 = "SELECT userunkid,locationid,storeid FROM sysuser WHERE userunkid =:user_id AND companyid=:companyid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':user_id',CONFIG_UID);
            $resObj1 = $dao->executeRow();
            if(!empty($resObj1)){
                if($resObj1['locationid']!='' && $resObj1['locationid']!=0){
                    /* Get Location List */
                        $strSql2 = "SELECT syslocationid,locationname as name,1 as type FROM syslocation WHERE syslocationid IN (".$resObj1['locationid'].") AND is_active=1 AND is_deleted=0 AND companyid=:companyid";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $rec1 = $dao->executeQuery();
                    /* Get Location List */
                }
                /* Get Store List */
                $rec2=array();
                if($resObj1['storeid']!='' && $resObj1['storeid']!=0 && CONFIG_IS_STORE==1){
                        $strSql2 = "SELECT storeunkid,storename as name,2 as type FROM   " . CONFIG_DBN . ".cfstoremaster WHERE storeunkid IN (".$resObj1['storeid'].") AND is_active=1 AND is_deleted=0 AND companyid=:companyid";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $rec2 = $dao->executeQuery();
                }
                /* Get Store List */
                $result_arr = array_merge($rec1,$rec2);
                if(!empty($result_arr)){
                    $arr_list = json_encode($result_arr);
                }else{
                    $arr_list = '';
                }
            }
            if(CONFIG_LOGINTYPE==1){
                $strSql3 = "SELECT syslocationid,locationname as name FROM syslocation WHERE is_active=1 AND syslocationid = :locationid AND companyid=:companyid";
                $dao->initCommand($strSql3);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addParameter(':locationid',CONFIG_LID);
                $rec3 = $dao->executeRow();
            }
            else{
                $strSql3 = "SELECT storeunkid,storename as name FROM   " . CONFIG_DBN . ".cfstoremaster WHERE is_active=1 AND storeunkid = :storeid AND companyid=:companyid";
                $dao->initCommand($strSql3);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addParameter(':storeid',CONFIG_SID);
                $rec3 = $dao->executeRow();
            }
            return array("chain_list"=>$arr_list,"lname"=>$rec3['name']);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getListForSwitch - '.$e);
        }
    }
    public function validateUserForSwitch($oid,$type)
    {
        try
        {
            $this->log->logIt($this->module.' - validateUserForSwitch');
            $dao = new \dao();
            if($type==1){
                $strSql1 = "SELECT count(syslocationid) as cnt FROM syslocation WHERE companyid=:companyid AND is_active=1 AND syslocationid=:oid";
                $dao->initCommand($strSql1);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addParameter(':oid',$oid);
                $resObj1 = $dao->executeRow();
                $field = "locationid";
            }
            if($type==2){
                $strSql1 = "SELECT count(storeunkid) as cnt FROM   " . CONFIG_DBN . ".cfstoremaster WHERE companyid=:companyid AND is_active=1 AND storeunkid=:oid";
                $dao->initCommand($strSql1);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addParameter(':oid',$oid);
                $resObj1 = $dao->executeRow();
                $field = "storeid";
            }
            if($resObj1['cnt']>0){
                $strSql = "SELECT count(userunkid) AS cnt, userunkid FROM sysuser WHERE userunkid=:userunkid AND companyid=:companyid  AND isactive=1 AND FIND_IN_SET(:target_oid,$field)";
                $dao->initCommand($strSql);
                $dao->addParameter(':userunkid',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addParameter(':target_oid',$oid);
                $rec = $dao->executeRow();
                if($rec['cnt']==1){
                    return array("Success"=>"True", "data"=>$rec);
                }
                else{
                    return array("Success"=>"False", "Message"=>"User don't have access to this company");
                }
            }
            else{
                return array("Success"=>"False", "Message"=>"Selected outlet is not active");
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - validateUserForSwitch - '.$e);
        }
    }
    //
    public function getIndentDocumentNumbering($keyname, $action = '')
    {
        try {
            $this->log->logIt($this->module.' - getIndentDocumentNumbering');
            $dao = new \dao();

            $strSql = "SELECT prefix, startno,reset_type FROM ".CONFIG_DBN.".cfindentnumber WHERE companyid=:companyid AND storeid=:storeid AND keyname = :keyname  ";
            $dao->initCommand($strSql);
            $dao->addParameter(':keyname', $keyname);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $doc_numbering = $dao->executeRow();
            $todaysdate = \util\util::getLocalDate();
            $reset_type = $doc_numbering['reset_type'];
            $flag = 0;
            $dao = new \dao;

            if ($keyname == "forward_indent") {
                $strSql = "SELECT count(indent_doc_num) AS cnt FROM ".CONFIG_DBN.".cfrequestindent WHERE companyid=:companyid AND storeid=:storeid AND CAST(createddatetime AS DATE) = :todaysdate";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':todaysdate', $todaysdate);
                $data = $dao->executeRow();
            }
            if ($keyname == "issue_voucher") {
                $strSql = "SELECT count(issue_doc_num) AS cnt FROM ".CONFIG_DBN.".cfissuevoucher WHERE companyid=:companyid AND storeid=:storeid AND CAST(createddatetime AS DATE) = :todaysdate";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':todaysdate', $todaysdate);
                $data = $dao->executeRow();
            }
            if ($keyname == "purchase_order") {
                $strSql = "SELECT count(order_doc_num) AS cnt FROM ".CONFIG_DBN.".cfpurchaseorder WHERE companyid=:companyid AND storeid=:storeid AND CAST(createddatetime AS DATE) = :todaysdate";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':todaysdate', $todaysdate);
                $data = $dao->executeRow();
            }
            if ($keyname == "good_receipt_note" || $keyname == "grn_voucher_no") {
                $strSql = "SELECT count(grn_doc_num) AS cnt FROM ".CONFIG_DBN.".cfgrn WHERE companyid=:companyid AND storeid=:storeid AND CAST(createddatetime AS DATE) = :todaysdate";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':todaysdate', $todaysdate);
                $data = $dao->executeRow();
            }
            if ($keyname == "goods_return" || $keyname == "gr_voucher_no") {
                $strSql = "SELECT count(gr_doc_num) AS cnt FROM ".CONFIG_DBN.".cfgoodsreturn WHERE companyid=:companyid AND storeid=:storeid AND CAST(createddatetime AS DATE) = :todaysdate";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':todaysdate', $todaysdate);
                $data = $dao->executeRow();
            }

            if ($reset_type == '1') {
                if (isset($data['cnt']) &&  $data['cnt'] == '0') {
                    $flag = 1;
                }
            } elseif ($reset_type == '2') {
                $date = \DateTime::createFromFormat("Y-m-d", $todaysdate);
                $day = $date->format("d");
                $month = $date->format("m");
                if ($day == '01') {
                    if ($data['cnt'] == '0') {
                        $flag = 1;
                    }
                }
            } elseif ($reset_type == '3') {
                $date = \DateTime::createFromFormat("Y-m-d", $todaysdate);
                $day = $date->format("d");
                $month = $date->format("m");

                $financial_month = \database\parameter::getParameter('fncialyearstartmonth');
                $financial_day = \database\parameter::getParameter('fncialyearstartdate');
                //$financial_date = '3-31';
                //$arr_date = explode('-', $financial_date);
                if ($day == $financial_day && $month == $financial_month) {
                    if ($data['cnt'] == '0') {
                        $flag = 1;
                    }
                }
            }
            if ($flag == 0) {
                $no = $doc_numbering['prefix'].$doc_numbering['startno'];
                $last_no = $doc_numbering['startno'];
            }
            else {
                $no = $doc_numbering['prefix'].'1';
                $last_no = 1;
            }

            if ($action == 'inc') {
                $updatedno = $last_no + 1;
                $strSql = "UPDATE ".CONFIG_DBN.".cfindentnumber SET startno=:startno WHERE companyid=:companyid AND storeid=:storeid AND keyname=:keyname";
                $dao->initCommand($strSql);
                $dao->addParameter(':keyname', $keyname);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':startno', $updatedno);
                $dao->executeNonQuery();
            }
            return $no;
        } catch (Exception $e) {
            $this->log->logIt($this->module.' - getIndentDocumentNumbering - '.$e);
        }
    }

    public function getLocationInfo()
    {
        try {
            $this->log->logIt($this->module . ' - getLocationInfo');
            $dao = new \dao();
            $strSql = " SELECT pms_username,pms_password,locationname, CONCAT(address1,',',address2,',',city,',',state) AS address,phone,locationemail FROM ".CONFIG_DBN.".cflocation
                        WHERE companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $rec = $dao->executeRow();
            return $rec;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getLocationInfo - ' . $e);
        }
    }
    public function getallstorelist()
    {
        try {
            $this->log->logIt($this->module . '-getallstorelist');
            $dao = new \dao();
            $strSql = "SELECT storeunkid,storename FROM " . CONFIG_DBN . ".cfstoremaster 
            WHERE companyid=:companyid AND  is_active=1 AND  is_deleted=0";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $rec = $dao->executeQuery();
            return json_encode(array("Success" => "True", "Data" => $rec));
        } catch (\Exception $e) {
            $this->log->logIt($this->module . '-getallstorelist -' . $e);
        }
    }

    public function getDenominationlist()
    {
        try {
            $this->log->logIt($this->module . ' - getDenominationlist');
            $dao = new \dao();
            $strSql = "SELECT CFD.denominationunkid as dnmid,CFD.cash_amount as paisa FROM " . CONFIG_DBN . ".cfdenomination As CFD
                          WHERE companyid=:companyid AND locationid=:locationid AND is_active=1 And is_deleted=0 ORDER BY CFD.cash_amount DESC ";

            $dao->initCommand($strSql);
            $dao->addParameter(":locationid", CONFIG_LID);
            $dao->addParameter(':companyid', CONFIG_CID);
            $list = $dao->executeQuery();
            return $list;

        } catch (\Exception $e) {
            $this->log->logIt($this->module . ' - getDenominationlist - ' . $e);
        }

    }

    public function getStartEndINVOICENumber()
    {
        try {
            $this->log->logIt($this->module . ' - getStartEndINVOICENumber');
            $dao = new \dao();
            //$today_date = \util\util::getTodaysDate(); //\util\util::getLocalDate();
            $strSql = "SELECT IF(endno!=0 && startno>endno,0,1) as is_valid FROM " . CONFIG_DBN . ".cfdocnumber WHERE keyname='INVOICE' AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $rec = $dao->executeRow();
            return $rec;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getStartEndINVOICENumber- ' . $e);
        }
    }


    public function getInvoiceNumber($action = '',$flagdepend=0,$gettype='')
    {
        try {
            $this->log->logIt($this->module . ' - getInvoiceNumber');
            $dao = new \dao();
            if($gettype==''){
                $gettype='INVOICE';
            }elseif($gettype=='KOT'){
                $gettype='KOTNO';
            }
            $strSql = "SELECT prefix,startno,reset_type,main_start_no FROM " . CONFIG_DBN . ".cfdocnumber WHERE keyname='".$gettype."' AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $rec = $dao->executeRow();
            $today_date = \util\util::getTodaysDate(); //\util\util::getLocalDate();
            $reset_type = $rec['reset_type'];
            $flag = 0;
            $dao = new \dao;
            if ($reset_type == '1') {
                $strSql = "SELECT count(foliounkid) AS cnt FROM " . CONFIG_DBN . ".fasfoliomaster WHERE companyid=:companyid AND locationid=:locationid AND CAST(servedate AS DATE) = :todaysdate";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->addParameter(':todaysdate', $today_date);
                $data = $dao->executeRow();
                if ($data['cnt'] == '0')
                    $flag = 1;
            } elseif ($reset_type == '2') {
                $date = \DateTime::createFromFormat("Y-m-d", $today_date);
                $day = $date->format("d");
                if ($day == '01') {
                    $strSql = "SELECT count(foliounkid) AS cnt FROM " . CONFIG_DBN . ".fasfoliomaster WHERE companyid=:companyid AND locationid=:locationid AND CAST(servedate AS DATE) = :todaysdate";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $dao->addParameter(':todaysdate', $today_date);
                    $data = $dao->executeRow();
                    if ($data['cnt'] == '0')
                        $flag = 1;
                }
            } elseif ($reset_type == '3') {
                $date = \DateTime::createFromFormat("Y-m-d", $today_date);
                $day = $date->format("d");
                $month = $date->format("m");
                if ($day == '01' && $month == '04') {
                    $strSql = "SELECT count(foliounkid) AS cnt FROM " . CONFIG_DBN . ".fasfoliomaster WHERE companyid=:companyid AND locationid=:locationid AND CAST(servedate AS DATE) = :todaysdate";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $dao->addParameter(':todaysdate', $today_date);
                    $data = $dao->executeRow();
                    if ($data['cnt'] == '0')
                        $flag = 1;
                }
            }
            if($flag == 0) {
                $no = $rec['prefix'] . $rec['startno'];
                $rec['last_no'] = $rec['startno'];
            }else {
                $no = $rec['prefix'].''.$rec['main_start_no'];
                $rec['last_no'] = $rec['main_start_no'];
            }
            if($action == 'inc') {
                if($flag==1 && $action == 'inc' && $flagdepend==1){
                    $updated_no = $rec['last_no'];
                }else{
                    $updated_no = $rec['last_no'] + 1;
                }
                $strSql = "UPDATE " . CONFIG_DBN . ".cfdocnumber SET startno=:startno WHERE keyname='".$gettype."' AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->addParameter(':startno', $updated_no);
                $dao->executeNonQuery();
            }
            $rec['invoice_no'] = $no;
            return $rec;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getInvoiceNumber - ' . $e);
        }
    }

   /* public function waiterlist()
    {
        try
        {
            $this->log->logIt($this->module." - getWaiterList");
            $dao = new \dao();

            $strSql = "SELECT hashkey,name FROM " . CONFIG_DBN . ".waiter_mst WHERE companyid=:companyid AND locationid=:locationid AND is_deleted=0  ORDER BY z_waiterid_pk ASC";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeQuery();

            if(isset($res) && count($res)>0)
            {
                return json_encode(array('Data'=>$res));
            }else
            {
                return json_encode(array('Data'=>''));
            }

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getWaiterList - ".$e);
            return json_encode(array('Success'=>'False'));
        }
    }*/

    public function getZomatosetting()
    {
        try
        {
            $this->log->logIt($this->module." - getZomatosetting");
            $dao = new \dao();


            /*$strSql1 = "SELECT is_zomato FROM syslocation WHERE syslocationid=:syslocationid AND companyid=:companyid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':syslocationid',CONFIG_LID);
            $res = $dao->executeRow();

            if(isset($res['is_zomato']))
            {
                $poc_zomato = $res['is_zomato'];
                return $poc_zomato;
            }else
            {
                return 0;
            }*/

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getZomatosetting - ".$e);
        }
    }

    public function getZomatoAppkey()
    {
        try
        {
            $this->log->logIt($this->module." - getZomatoAppkey");
            $dao = new \dao();


            $strSql1 = "SELECT zomatoapikeys FROM syslocation WHERE syslocationid=:syslocationid AND companyid=:companyid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':syslocationid',CONFIG_LID);
            $res = $dao->executeRow();

            if(isset($res['zomatoapikeys']))
            {
                $poc_zomato = $res['zomatoapikeys'];
                return $poc_zomato;
            }else
            {
                return 0;
            }

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getZomatoAppkey - ".$e);
        }
    }

    public function getQuickBooksetting()
    {
        try
        {
            $this->log->logIt($this->module." - getQuickBooksetting");
            $dao = new \dao();


            $strSql1 = "SELECT isactive FROM " . CONFIG_DBN . ".cfquickbook WHERE locationid=:locationid AND companyid=:companyid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',CONFIG_LID);
            $res = $dao->executeRow();

            if(isset($res['isactive']))
            {
                $poc_quickbook = $res['isactive'];
                return $poc_quickbook;
            }else
            {
                return 0;
            }

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getQuickBooksetting - ".$e);
        }
    }

    public function getFiscalsetting()
    {
        try
        {
            $this->log->logIt($this->module." - getFiscalsetting");
            $dao = new \dao();

            /*$strSql1 = "SELECT is_fiscal FROM syslocation WHERE syslocationid=:syslocationid AND companyid=:companyid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':syslocationid',CONFIG_LID);
            $res = $dao->executeRow();

            if(isset($res['is_fiscal']))
            {
                $pos_fiscal = $res['is_fiscal'];
                return $pos_fiscal;
            }else
            {
                return 0;
            }*/

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getFiscalsetting - ".$e);
        }
    }


}

?>