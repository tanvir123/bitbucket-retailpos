<?php
namespace database;

class menu_item_unitdao
{
    public $module = 'DB_menu_item_unit';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function addItemunit($data,$languageArr,$defaultlanguageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - additemunit');
            $dao = new \dao();
            $datetime=\util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $ObjDependencyDao = new \database\dependencydao();
            $tablename =  "cfmenu_itemunit";
            $arr_log = array(
                'Unit Name'=>$data['name'],
                'Sortkey'=>$data['sortkey'],
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive'
            );
            $json_data = json_encode($arr_log);

            if($data['id']==0)
            {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"name",$data['name']);
                if($chk_name==1)
                {
                    return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->RECORD_ALREADY_EXISTS));
                }

                $title = "Add Record";
                $hashkey = \util\util::gethash();

                $strSql = "INSERT INTO ".CONFIG_DBN.".cfmenu_itemunit(`name`, 
                                                                    sortkey, 
                                                                    companyid,
                                                                    locationid,
                                                                    createddatetime, 
                                                                    created_user, 
                                                                    is_active, 
                                                                    hashkey)
                                                            VALUE(  :name, 
                                                                    :sortkey, 
                                                                    :companyid,
                                                                    :locationid, 
                                                                    :createddatetime, 
                                                                    :created_user, 
                                                                    :rdo_status, 
                                                                    :hashkey)";
                
                $dao->initCommand($strSql);
                $dao->addParameter(':name',$data['name']);
                $dao->addParameter(':sortkey',$data['sortkey']);
                $dao->addParameter(':rdo_status',$data['rdo_status']);
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':created_user',CONFIG_UID);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                return json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_ADD_SUC));
           }
           else
           {
               $id =$ObjCommonDao->getprimarykey('cfmenu_itemunit',$data['id'],'unitunkid');

               $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"name",$data['name'],$id);
               if($chk_name==1)
               {
                   return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->RECORD_ALREADY_EXISTS));
               }

                $title = "Edit Record";
                $strSql = "UPDATE ".CONFIG_DBN.".cfmenu_itemunit SET name=:name,
                                                                    sortkey=:sortkey,
                                                                    modifieddatetime=:modifieddatetime,
                                                                    modified_user=:modified_user
                                                                    WHERE hashkey=:id AND companyid=:companyid
                                                                    AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':id',$data['id']);
                $dao->addParameter(':name',$data['name']);
                $dao->addParameter(':sortkey',$data['sortkey']);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                return json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_UP_SUC));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addItemUnit - '.$e);
        }
    }

    public function itemunitlist($limit,$offset,$name,$isactive=0)
    {
        try
        {
            $this->log->logIt($this->module.' - itemunitlist - '.$name);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT CFIU.name,CFIU.hashkey,CFIU.sortkey,CFIU.is_active,IFNULL(CFU1.username,'') AS createduser,
                       IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CFIU.createddatetime,'".$mysqlformat."'),'') as created_date,
                       IFNULL(DATE_FORMAT(CFIU.modifieddatetime,'".$mysqlformat."'),'') as modified_date
                       FROM ".CONFIG_DBN.".cfmenu_itemunit AS CFIU
                       LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CFIU.created_user=CFU1.userunkid AND CFIU.companyid=CFU1.companyid
                       LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON CFIU.modified_user=CFU2.userunkid AND CFIU.modified_user=CFU2.userunkid AND CFIU.companyid=CFU2.companyid
                       WHERE CFIU.companyid=:companyid AND CFIU.locationid=:locationid AND CFIU.is_deleted=0";

            if($name!="")
                $strSql .= " AND name LIKE '%".$name."%'";
            if($isactive==1)
                $strSql .= " AND is_active=1";
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $rec = $dao->executeQuery();

            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue));
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - itemunitlist - '.$e);
        }
    }

    public function getItemunitRec($data)
    {
        try
        {
            $this->log->logIt($this->module." - getItemunitRec");
            $dao = new \dao();
            $id=$data['id'];
            $strSql = "SELECT `name`,hashkey,sortkey FROM ".CONFIG_DBN.".cfmenu_itemunit WHERE hashkey=:id AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':id',$id);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeRow();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getItemunitRec - ".$e);
            return false;
        }
    }
    public function getAllItemunit($data)
    {
        try
        {
            $this->log->logIt($this->module." - getAllItemunit");
            $dao = new \dao();
            $strSql = "SELECT `name`,hashkey FROM ".CONFIG_DBN.".cfmenu_itemunit WHERE companyid=:companyid AND locationid=:locationid AND is_active=1";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getAllItemunit - ".$e);
            return false;
        }
    }
}
?>