<?php
namespace database;

class menu_menuhoursdao
{
    public $module = 'DB_menu_menuhoursdao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function addmenuhour($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addmenuhour');
            $dao = new \dao();
            $datetime=\util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $tablename =  "cfmenu_menuhours";
            $open_time = date('H:i:s',strtotime($data['opentime']));
            $close_time = date('H:i:s',strtotime($data['closetime']));
            $ObjDependencyDao = new \database\dependencydao();

            $arr_log = array(
                'Name'=>$data['name'],
                'Open Time'=>$open_time,
                'Close Time'=>$close_time,
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive',
            );
            $json_data = json_encode($arr_log);

            if($data['id']=='0' || $data['id']==0)
            {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"name",$data['name']);
                if($chk_name==1)
                {
                    return json_encode(array('Success'=>'False','Message'=>'Menu Hours already Exists'));
                }
                $title = "Add Record";
                $hashkey = \util\util::gethash();
                
                $weeklist_arr = '';
                $weeklist_arr = implode(',',$data['dayofweek']);

                $strSql = "INSERT INTO ".CONFIG_DBN.".cfmenu_menuhours(companyid,locationid,name,day_of_week,open_time,close_time,createddatetime,created_user,is_active,hashkey)".
                                                             " VALUE(:companyid,:locationid,:txtname,:weekdays,:txtopentime,:txtclosetime,:createddatetime,:created_user,:is_active,:hashkey)";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->addParameter(':txtname',$data['name']);
                $dao->addParameter(':weekdays',$weeklist_arr);
                $dao->addParameter(':txtopentime',$open_time);
                $dao->addParameter(':txtclosetime',$close_time);
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':created_user',CONFIG_UID);
                $dao->addParameter(':is_active',$data['rdo_status']);
                $dao->addParameter(':hashkey',$hashkey);
                $dao->executeNonQuery();
                $id = $dao->getLastInsertedId();
                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                return json_encode(array('Success'=>'True','Message'=>'Menu Hours saved successfully'));
            }
            else
            {
                $id = $ObjCommonDao->getprimarykey('cfmenu_menuhours',$data['id'],'menuhoursunkid');
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"name",$data['name'],$id);
                if($chk_name==1)
                {
                    return json_encode(array('Success'=>'False','Message'=>'Menu Hours already Exists'));
                }
                $weeklist_arr = '';
                $weeklist_arr = implode(',',$data['dayofweek']);

                $title = "Edit Record";
                
                $strSql = "UPDATE ".CONFIG_DBN.".cfmenu_menuhours SET
                                                        name=:txtname,
                                                        day_of_week=:weekdays,
                                                        open_time=:txtopentime,
                                                        close_time=:txtclosetime, 
                                                        modifieddatetime=:modifieddatetime,
                                                        modified_user=:modified_user
                                                        WHERE
                                                        hashkey=:hashkey
                                                        AND companyid=:companyid AND locationid=:locationid";
                
                $dao->initCommand($strSql);
                $dao->addParameter(':hashkey',$data['id']);
                $dao->addParameter(':txtname',$data['name']);
                $dao->addParameter(':weekdays',$weeklist_arr);
                $dao->addParameter(':txtopentime',$open_time);
                $dao->addParameter(':txtclosetime',$close_time);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                return json_encode(array('Success'=>'True','Message'=>'Menu Hours update successfully'));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addmenuhour - '.$e);
        }
    }

    public function menuhourslist($limit,$offset,$name,$isactive=0)
    {
        try
        {
            $this->log->logIt($this->module.' - menuhourslist - '.$name);

            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $timeformat = \database\parameter::getParameter('timeformat');
            $mysqltime_format =  \common\staticarray::$mysqltimeformat[$timeformat];
            $strSql = "SELECT CFMH.*,TIME_FORMAT(open_time,'".$mysqltime_format."') as start_time,TIME_FORMAT(close_time,'".$mysqltime_format."') as end_time,IFNULL(CFU1.username,'') AS createduser,IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CFMH.createddatetime,'".$mysqlformat."'),'') as created_date,IFNULL(DATE_FORMAT(CFMH.modifieddatetime,'".$mysqlformat."'),'') as modified_date 
                            FROM ".CONFIG_DBN.".cfmenu_menuhours AS CFMH 
                            LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 
                            ON CFMH.created_user=CFU1.userunkid AND CFMH.companyid=CFU1.companyid 
                            LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 
                            ON CFMH.modified_user=CFU2.userunkid AND CFMH.modified_user=CFU2.userunkid AND CFMH.companyid=CFU2.companyid 
                            WHERE CFMH.companyid=:companyid AND CFMH.locationid=:locationid AND CFMH.is_deleted=0 ";
            if($name!="")
                $strSql .= " AND name LIKE '%".$name."%'";
            if($isactive == 1)
            {
                $strSql .= " and is_active=1 ";
            }   
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
           
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $data = $dao->executeQuery();
           
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);          
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $rec = $dao->executeQuery();
            
            if(count($data) != 0){               
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue));
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - menuhourslist - '.$e);
        }
    }
    
    public function getRecmenuhour($data)
    {
        try
        {
            $this->log->logIt($this->module." - getRecmenuhour");
            $dao = new \dao();
            $mysql_format =  \common\staticarray::$mysqltimeformat['h:i A'];
            $id=(isset($data['id']))?$data['id']:"";
            $strSql = "SELECT hashkey,name,day_of_week,is_active,TIME_FORMAT(open_time,'".$mysql_format."') as start_time,TIME_FORMAT(close_time,'".$mysql_format."') as end_time FROM ".CONFIG_DBN.".cfmenu_menuhours WHERE hashkey=:menuhoursunkid AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':menuhoursunkid',$id);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeRow();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getRecmenuhour - ".$e);
            return false; 
        }
    }



}
?>