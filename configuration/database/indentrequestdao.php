<?php

namespace database;

class indentrequestdao
{
    public $module = 'DB_indentrequest';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function indentlist($limit, $offset, $fromdate, $todate, $indent, $storeid, $statusid,$languageArr='')
    {
        try {

            $this->log->logIt($this->module . ' - indentlist');

            $objutil = new \util\util;
            $fromdate = $objutil->convertDateToMySql($fromdate);
            $todate = $objutil->convertDateToMySql($todate);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            // $round_off = \database\parameter::getParameter('digitafterdecimal');
            // IFNULL(ROUND(CRI.totalamount, $round_off),0) AS totalamount1


            $strSql = "SELECT CRI.*,
             CASE 
                        WHEN CRI.status=0 THEN '".$languageArr->LANG30."'
                        WHEN CRI.status=1 THEN '".$languageArr->LANG31."'                       
                        ELSE  '".$languageArr->LANG32."'
                        END as indent_stat,
            CL.storename, IFNULL(DATE_FORMAT(CRI.indent_date,'" . $mysqlformat . "'),'') AS indentdate,IFNULL(CFU1.username,'') AS createduser,
                                    IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CRI.createddatetime,'" . $mysqlformat . "'),'') AS created_date,
                                    IFNULL(DATE_FORMAT(CRI.modifieddatetime,'" . $mysqlformat . "'),'') AS modified_date                                   
                                    FROM " . CONFIG_DBN . ".cfrequestindent AS CRI
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU1 ON CRI.createduser=CFU1.userunkid AND CRI.companyid=CFU1.companyid
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU2 ON CRI.modifieduser=CFU2.userunkid AND CRI.modifieduser=CFU2.userunkid AND CRI.companyid=CFU2.companyid
                                    LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS CL ON CRI.recstoreid=CL.storeunkid AND CRI.companyid=CL.companyid
                                    WHERE CRI.companyid=:companyid AND CRI.storeid=:storeid AND CRI.is_deleted=0";

            if ($indent != "") {
                $strSql .= " AND CRI.indent_doc_num LIKE '%" . $indent . "%'";
            }
            if ($storeid != 0) {
                $strSql .= " AND CRI.recstoreid LIKE '%" . $storeid . "%'";
            }
            if ($statusid != 0) {
                $strSql .= " AND CRI.status LIKE '%" . $statusid . "%'";
            }
            if ($fromdate != "" && $todate != "") {
                $strSql .= " AND CRI.indent_date  BETWEEN  '" . $fromdate . "' AND '" . $todate . "' ";
            }
            $strSql .= " ORDER BY CRI.indentid DESC";
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $data = $dao->executeQuery();
            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
                $dao->initCommand($strSql);

            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return html_entity_decode(json_encode($retvalue));
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - indentlist - ' . $e);
        }
    }

    public function getrawmaterialoncategory($data)
    {
        try {
            $this->log->logIt($this->module . " - getrawmaterialoncategory");
            $dao = new \dao();


            $strSql = "SELECT id,name FROM " . CONFIG_DBN . ".cfrawmaterial
             WHERE is_active=1 and is_deleted=0 AND lnkcategoryid=:lnkcategoryid AND companyid=:companyid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':lnkcategoryid', $data['rawcatid']);

            $res = $dao->executeQuery();

            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getrawmaterialoncategory - " . $e);
            return false;
        }
    }

    public function getlastindentno()
    {
        try {
            $this->log->logIt($this->module . " - getlastindentno");
            $dao = new \dao();
            $strSql = "SELECT prefix,startno  FROM " . CONFIG_DBN . ".cfindentnumber WHERE 
            storeid=:storeid AND keyname='forward_indent'";

            $dao->initCommand($strSql);
            $dao->addParameter(':storeid', CONFIG_SID);
            $res = $dao->executeRow();


            $indent_doc_number = $res['prefix'] . $res['startno'];

            return $indent_doc_number;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getlastindentno - " . $e);
            return false;
        }
    }

    public function getUnitRate($data)
    {
        try {
            $this->log->logIt($this->module . " - getUnitRate");
            $dao = new \dao();

            $strUnit = "SELECT CRUREL.lnkunitid as unitunkid,CRUREL.conversation_rate as unit,CU.name,IFNULL(CRL.rateperunit,0) AS rateperunit
                  FROM ".CONFIG_DBN.".cfrawmaterial_unit_rel as CRUREL
                  INNER JOIN ".CONFIG_DBN.".cfunit as CU on CU.unitunkid=CRUREL.lnkunitid and CU.companyid=:companyid AND CU.is_active=1 and CU.is_deleted=0
                  LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial_loc CRL ON CRL.lnkrawmaterialid=CRUREL.lnkrawid AND CRL.storeid=:storeid AND CRL.companyid=:companyid
                  WHERE CRUREL.lnkrawid=:lnkrawid AND CRUREL.companyid=:companyid";
            $dao->initCommand($strUnit);
            $dao->addParameter(':lnkrawid',$data['rawid']);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':storeid',$data['locationid']);
            $res = $dao->executeQuery();

            if(!$res){
             $strSql = " SELECT U.name,U.unitunkid,U.unit,IFNULL(CRL.rateperunit,0) AS rateperunit
                         FROM " . CONFIG_DBN . ".cfrawmaterial CRM 
                       LEFT JOIN " . CONFIG_DBN . ". cfunit U ON CRM.measuretype=U.measuretype AND U.companyid=:companyid AND U.is_active=1 and U.is_deleted=0
                       LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial_loc CRL ON CRL.lnkrawmaterialid=CRM.id AND CRL.storeid=:storeid 
                       WHERE CRM.id =:id AND CRM.companyid=:companyid ";

               $dao->initCommand($strSql);
               $dao->addParameter(':companyid', CONFIG_CID);
               $dao->addParameter(':storeid', $data['locationid']);
               $dao->addParameter(':id', $data['rawid']);
               $res = $dao->executeQuery();
            }

            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getUnitRate - " . $e);
            return false;
        }
    }

    public function getIndentRecordsonID($data)
    {
        try {
            $this->log->logIt($this->module . " - getIndentRecordsonID");
            $dao = new \dao();
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];

            $strSqltogetindentrecords = "SELECT CRI.*,CL.storename,
            IFNULL(DATE_FORMAT(CRI.indent_date,'" . $mysqlformat . "'),'') as indentformated_date
             FROM " . CONFIG_DBN . ".cfrequestindent AS CRI 
            LEFT JOIN  " . CONFIG_DBN . ".cfstoremaster AS CL ON CRI.recstoreid=CL.storeunkid
            WHERE CRI.indentid=:indentid and CRI.companyid=:companyid and CRI.storeid=:storeid ";
            $dao->initCommand($strSqltogetindentrecords);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addParameter(':indentid', $data['id']);
            $indent = $dao->executeRow();

            $strSqltogetindentdetail = "SELECT CR.name AS selectedrm,U.name AS selectedunit,CRI.* 
            FROM " . CONFIG_DBN . ".cfindentrequestdetail AS CRI 
            LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CRI.lnkrawid=CR.id 
            LEFT JOIN " . CONFIG_DBN . ".cfunit U ON CRI.lnkunitid=U.unitunkid AND
            U.companyid=:companyid AND U.is_active=1 and U.is_deleted=0 
            WHERE CRI.lnkindentid=:lnkindentid AND lnkissueitemid=0";

            $dao->initCommand($strSqltogetindentdetail);
            $dao->addParameter(':lnkindentid', $indent['indentid']);
            $dao->addParameter(':companyid', CONFIG_CID);

            $indentdetail = $dao->executeQuery();


            return html_entity_decode(json_encode(array("Success" => "True", "indent" => $indent, "indentdetail" => $indentdetail)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getIndentRecordsonID - " . $e);
            return false;
        }
    }

    public function addindentrequest($data,$default_langlist)
    {
        try {
            $this->log->logIt($this->module . ' - addindentrequest - ');
            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $indentarray = $data['indentrecords'];
            $objutil = new \util\util;
            $indent_date = $objutil->convertDateToMySql($data['indent_date']);
            $round_off = \database\parameter::getParameter('digitafterdecimal');


            $ObjCommonDao = new \database\commondao();
            if ($data['id'] == '0' || $data['id'] == '') {
                $indent_doc_num = $ObjCommonDao->getIndentDocumentNumbering('indent_no', 'inc');

                $sql = "INSERT into " . CONFIG_DBN . ".cfrequestindent SET 
                indent_date=:indent_date,
                indent_doc_num=:indent_doc_num,
                storeid=:storeid,
                remarks=:remarks,
                companyid=:companyid,
                recstoreid=:recstoreid,
                createddatetime=:createddatetime,
                createduser=:createduser,
                totalamount=:totalamount";

                $dao->initCommand($sql);
                $dao->addParameter(':indent_date', $indent_date);
                $dao->addParameter(':indent_doc_num', $indent_doc_num);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':remarks', $data['premarks']);
                $dao->addParameter(':recstoreid', $data['plocationid']);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->addParameter(':createduser', CONFIG_UID);
                $dao->addParameter(':totalamount', round($data['totalamount'], $round_off));
                $dao->executeNonQuery();
                $lnkindentid = $dao->getLastInsertedId();
                foreach ($indentarray AS $key => $value) {
                    $sql = "INSERT into ".CONFIG_DBN.".cfindentrequestdetail SET 
                            lnkindentid=:lnkindentid,lnkrawcatid=:lnkrawcatid,
                            lnkrawid=:lnkrawid,lnkunitid=:lnkunitid,
                            qty=:qty,auth_qty=:qty,rate=:rate,total=:total,companyid=:companyid";
                    $dao->initCommand($sql);
                    $dao->addParameter(':lnkindentid', $lnkindentid);
                    $dao->addParameter(':lnkrawcatid', $value['pcatid']);
                    $dao->addParameter(':lnkrawid', $value['prawmaterialid']);
                    $dao->addParameter(':lnkunitid', $value['punitid']);
                    $dao->addParameter(':qty', $value['pqty']);
                    $dao->addParameter(':rate', round($value['prate'], $round_off));
                    $dao->addParameter(':total', round($value['ptotal'], $round_off));
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->executeNonQuery();
                }
                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $default_langlist->REC_ADD_SUC)));
            } else {

                $strforcheckkey = "SELECT indentrequnkid  FROM ".CONFIG_DBN.".cfindentrequestdetail 
                 WHERE lnkindentid=:lnkindentid AND companyid=:companyid";
                $dao->initCommand($strforcheckkey);
                $dao->addParameter(':lnkindentid', $data['id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $resultforidcheck = $dao->executeQuery();

                $indentrequnkid = \util\util::getoneDarray($resultforidcheck, 'indentrequnkid');

                $indentarr = [];
                foreach ($indentarray as $key => $value) {
                    if (in_array($value['indentrequnkid'], $indentrequnkid)) {
                        $strsql = "UPDATE ".CONFIG_DBN.".cfindentrequestdetail SET 
                            lnkindentid=:lnkindentid,lnkrawcatid=:lnkrawcatid,lnkrawid=:lnkrawid,
                            lnkunitid=:lnkunitid,qty=:qty,auth_qty=:qty,rate=:rate,total=:total 
                            WHERE indentrequnkid=:indentrequnkid AND lnkindentid=:lnkindentid AND companyid=:companyid";
                        $dao->initCommand($strsql);
                        $dao->addParameter(':indentrequnkid', $value['indentrequnkid']);
                        $dao->addParameter(':lnkindentid', $data['id']);
                        $dao->addParameter(':lnkrawcatid', $value['pcatid']);
                        $dao->addParameter(':lnkrawid', $value['prawmaterialid']);
                        $dao->addParameter(':lnkunitid', $value['1']);
                        $dao->addParameter(':qty', $value['pqty']);
                        $dao->addParameter(':rate', round($value['prate'], $round_off));
                        $dao->addParameter(':total', round($value['ptotal'], $round_off));
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->executeNonQuery();
                        $indentarr[] = $value['indentrequnkid'];
                    } else {
                        $sql = "INSERT into ".CONFIG_DBN.".cfindentrequestdetail SET
                                lnkindentid=:lnkindentid,lnkrawcatid=:lnkrawcatid,
                                lnkrawid=:lnkrawid,lnkunitid=:lnkunitid,
                                qty=:qty,auth_qty=:qty,rate=:rate,total=:total,companyid=:companyid";
                        $dao->initCommand($sql);
                        $dao->addParameter(':lnkindentid', $data['id']);
                        $dao->addParameter(':lnkrawcatid', $value['pcatid']);
                        $dao->addParameter(':lnkrawid', $value['prawmaterialid']);
                        $dao->addParameter(':lnkunitid', $value['punitid']);
                        $dao->addParameter(':qty', $value['pqty']);
                        $dao->addParameter(':rate', round($value['prate'], $round_off));
                        $dao->addParameter(':total', round($value['ptotal'], $round_off));
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->executeNonQuery();
                        $indentarr[] = $dao->getLastInsertedId();
                    }
                }
                $strsql = "DELETE FROM ".CONFIG_DBN.".cfindentrequestdetail              
                            WHERE indentrequnkid NOT IN ('".implode("','",$indentarr)."') 
                            AND lnkindentid=:lnkindentid AND companyid=:companyid";
                $dao->initCommand($strsql);
                $dao->addParameter(':lnkindentid', $data['id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->executeNonQuery();

                $sql = "UPDATE ".CONFIG_DBN.".cfrequestindent SET
                        remarks=:remarks,
                        totalamount=:totalamount,modifieddatetime=:modifieddatetime,
                        modifieduser=:modifieduser  WHERE indentid=:indentid";
                $dao->initCommand($sql);
                $dao->addParameter(':indentid', $data['id']);
                $dao->addParameter(':remarks', $data['premarks']);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':modifieduser', CONFIG_UID);
                $dao->addParameter(':totalamount', round($data['totalamount'], $round_off));
                $dao->executeNonQuery();
                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $default_langlist->REC_UP_SUC)));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module.' - addindentrequest - '.$e);
        }
    }

    public function removeindent($data,$default_langlist)
    {
        try {
            $this->log->logIt($this->module . ' - removeindent');

            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();

            $strSql = "UPDATE " . CONFIG_DBN . ".cfrequestindent SET 
            is_deleted=1,modifieduser=:modifieduser,modifieddatetime=:modifieddatetime 
            WHERE indentid=:indentid AND companyid=:companyid";

            $dao->initCommand($strSql);
            $dao->addParameter(":indentid", $data['id']);
            $dao->addParameter(':modifieduser', CONFIG_UID);
            $dao->addParameter(':modifieddatetime', $datetime);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->executeNonQuery();

            $strsql = "DELETE FROM  " . CONFIG_DBN . ".cfindentrequestdetail              
                WHERE lnkindentid=:lnkindentid";
            $dao->initCommand($strsql);
            $dao->addParameter(':lnkindentid', $data['id']);
            $dao->executeNonQuery();


            return html_entity_decode(json_encode(array("Success" => "True", "Message" => $default_langlist->REM_REC_SUC)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - remove - ' . $e);
        }
    }

    public function getallstorelist()
    {
        try {
            $this->log->logIt($this->module . '-getallstorelist');
            $dao = new \dao();
            $strSql = "SELECT storeunkid,storename FROM " . CONFIG_DBN . ".cfstoremaster 
            WHERE companyid=:companyid AND storeunkid!=:storeunkid AND  is_active=1 AND  is_deleted=0";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeunkid', CONFIG_SID);
            $rec = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $rec)));
        } catch (\Exception $e) {
            $this->log->logIt($this->module . '-getallstorelist -' . $e);
        }
    }

    public function getIndentItemsToIssue($indent_id)
    {
        try {
            $this->log->logIt($this->module." - getIndentItemsToIssue");
            $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $strSql = "SELECT CFRM.name AS raw_material,CFU.name AS item_unit,CFID.qty as actual_qty,CFID.auth_qty,CFID.rate,".
                "CFID.indentrequnkid,CFID.lnkindentid,ROUND(IFNULL(CFRI.inventory,0),$round_off) as stock_available FROM ".CONFIG_DBN.".cfindentrequestdetail AS CFID ".
                "LEFT JOIN ".CONFIG_DBN.".cfrawmaterial AS CFRM ON CFID.lnkrawid=CFRM.id AND CFRM.companyid=:companyid ".
                "LEFT JOIN ".CONFIG_DBN.".cfunit CFU ON CFID.lnkunitid=CFU.unitunkid AND CFU.companyid=:companyid ".
                "LEFT JOIN ".CONFIG_DBN.".cfrawmaterial_loc CFRI ON CFRI.lnkrawmaterialid=CFID.lnkrawid AND CFRI.companyid=:companyid AND CFRI.storeid=:storeid ".
                "AND CFU.is_active=1 and CFU.is_deleted=0 WHERE CFID.lnkindentid=:lnkindentid AND CFID.lnkissueitemid=0 AND CFID.companyid =:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':lnkindentid',$indent_id);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':storeid',CONFIG_SID);
            $resObj = $dao->executeQuery();
            return $resObj;
        } catch (Exception $e) {
            $this->log->logIt($this->module." - getIndentItemsToIssue - ".$e);
            return false;
        }
    }

    public function authorizeIndentItemsQty($data,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module.' - authorizeIndentItemsQty');
            $dao = new \dao();
            foreach ($data['indent_items'] as $key=>$value){
                $strSql = "UPDATE ".CONFIG_DBN.".cfindentrequestdetail SET auth_qty=:auth_qty 
                WHERE indentrequnkid=:id  AND lnkindentid=:indentid AND companyid=:companyid";
                $dao->initCommand($strSql);
                $dao->addParameter(':id', $key);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':indentid', $data['indentid']);
                $dao->addParameter(':auth_qty', $value['auth_qty']);
                $dao->executeNonQuery();
            }
            return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->ITEM_AUT_SUC)));
        } catch (\Exception $e) {
            $this->log->logIt($this->module.' - authorizeIndentItemsQty - '.$e);
        }
    }

    public function getforwardindentdetail($id)
    {
        try {
            $this->log->logIt($this->module . ' - getforwardindentdetail');
            $dao = new \dao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $resarr = array();
            $strSql = "SELECT C.companyname,C.address1 AS company_address,C.phone,C.currency_sign,C.companyemail,
                        CFV.indent_doc_num AS voucher_num,IFNULL(C.logo,'') as company_logo,
                        CFV.remarks,DATE_FORMAT(CFV.indent_date,'" . $mysql_format . "') as Voucher_Date,IFNULL(CFU1.username,'') AS createduser,
                        ROUND(IFNULL(CFV.totalamount,0),$round_off) as totalamount,
                        S.storename as issuing_store,ST.storename AS rec_store 
                        FROM " . CONFIG_DBN . ".cfrequestindent AS CFV 
                        LEFT JOIN " . CONFIG_DBN . ".cfcompany as C ON CFV.companyid= C.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster as S ON CFV.storeid=S.storeunkid AND S.companyid=:companyid AND S.is_active=1 AND S.is_deleted=0
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS ST on CFV.recstoreid=ST.storeunkid AND ST.companyid=:companyid AND ST.is_active=1 AND ST.is_deleted=0
                        LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CFV.createduser=CFU1.userunkid AND CFV.companyid=CFU1.companyid
                        WHERE CFV.storeid=:storeid AND CFV.is_deleted=0  AND
                         CFV.companyid=:companyid AND CFV.indentid=:indentid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $dao->addparameter(':indentid', $id);
            $indentrecords = $dao->executeRow();

            $resarr['company_name'] = $indentrecords['companyname'];
            $resarr['address'] = $indentrecords['company_address'];
            $resarr['phone'] = $indentrecords['phone'];
            $resarr['email'] = $indentrecords['companyemail'];
            $resarr['company_logo'] =!empty($indentrecords['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$indentrecords['company_logo']:'';
            $resarr['currency_sign'] = $indentrecords['currency_sign'];
            $resarr['voucher_num'] = $indentrecords['voucher_num'];
            $resarr['remarks'] = $indentrecords['remarks'];
            $resarr['Voucher_Date'] = $indentrecords['Voucher_Date'];
            $resarr['totalamount'] = $indentrecords['totalamount'];
            $resarr['issuing_store'] =$indentrecords['rec_store'];
            $resarr['rec_store'] =  $indentrecords['issuing_store'];
            $resarr['createduser'] = $indentrecords['createduser'];

            $str = "SELECT  CRM.name AS Item,IVD.qty,U.shortcode as Unitname,IVD.rate,CRL.inventory,CRL.inventory,
                    IFNULL(ROUND((IVD.qty*IVD.rate),$round_off),'') AS Amount 
                    FROM " . CONFIG_DBN . ".cfindentrequestdetail AS IVD 
                    LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CRM ON IVD.lnkrawid=CRM.id AND CRM.companyid=:companyid AND CRM.is_active=1 AND CRM.is_deleted=0
                    LEFT JOIN " . CONFIG_DBN . ".cfunit AS U ON IVD.lnkunitid=U.unitunkid AND U.companyid=:companyid AND U.is_active=1 AND U.is_deleted=0
                    LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial_loc AS CRL ON IVD.lnkrawid=CRL.lnkrawmaterialid AND CRL.companyid=:companyid AND  CRL.storeid=:storeid 
                 
                    WHERE IVD.companyid=:companyid AND IVD.lnkindentid=:indentid";

            $dao->initCommand($str);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addparameter(':indentid', $id);
            $indentdetail = $dao->executeQuery();

            $resarr['indentdetail'] = $indentdetail;
            $total= $to = 0;
            foreach ($indentdetail AS $val) {
                $to=$val['qty'] * $val['rate'];
                $total += $to;
            }

            $resarr['totalamount']=number_format($total, $round_off);
            $resarr['current_date'] = \util\util::convertMySqlDate($current_date);

            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getforwardindentdetail - ' . $e);
        }
    }

    public function updateInventoryForPMS($data1)
    {
        try {
            $this->log->logIt($this->module . ' - updateInventoryForPMS');
            $dao = new \dao();
            //Update Inventory
            if(isset($data1)){
                foreach ($data1 as $key=>$data){
                    if($data['rawid']==''){
                        die('Item Not Found');
                    }

                    if(empty($data['crdb'])){
                        return json_encode(array('Success'=>'False','Message'=>"Please enter valid Credit/Debit value !"));
                    }

                    $strSql = "SELECT id,measuretype FROM ".CONFIG_DBN.".cfrawmaterial WHERE id=:rawid  and companyid=:companyid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':rawid',$data['rawid']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $rawID = $dao->executeRow();

                    if(!$rawID){
                        return json_encode(array('Success'=>'False','Message'=>"Invalid Item !"));
                    }


                    $strSql = "SELECT unitunkid FROM ".CONFIG_DBN.".cfunit WHERE measuretype=:measureval and is_systemdefined=1 and companyid=:companyid ";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':measureval',$rawID['measuretype']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $unitlist = $dao->executeRow();
                    $unitID=$unitlist['unitunkid'];


                    $strSql = "SELECT unitunkid,unit FROM ".CONFIG_DBN.".cfunit WHERE unitunkid=:unitunkid and companyid=:companyid ";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':unitunkid',$data['unit_id']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $unitget = $dao->executeRow();

                    if(!$unitget){
                        return json_encode(array('Success'=>'False','Message'=>"Invalid Unit !"));
                    }


                    $amount =isset($data['item_price'])?$data['item_price']:0 ;
                    $inventory = $data['quantity'] * (float)$data['unit_rate'];

                    $strrawLoc = "SELECT id,inventory,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc 
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                    $dao->initCommand($strrawLoc);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':item_id', $data['rawid']);
                    $resrawLoc = $dao->executeRow();


                    $locInventory=$resrawLoc['inventory'];

                    $dao->initCommand("SELECT u.unitunkid FROM ".CONFIG_DBN.".cfrawmaterial CFR 
					    INNER JOIN ".CONFIG_DBN.".cfunit u ON u.measuretype = CFR.measuretype AND u.is_systemdefined=1 AND u.companyid=:companyid
					    WHERE CFR.id = :id AND CFR.companyid=:companyid ");
                    $dao->addParameter(':id',$data['rawid']);
                    $dao->addParameter(':companyid', CONFIG_CID);

                    $rawMaterialRec = $dao->executeRow();

                    if(!$resrawLoc)
                    {
                        $insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid";
                        if($data['crdb']=='cr'){
                            $insertInventorySql  .=",inventory";
                        }
                        $insertInventorySql  .= ") VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid";
                        if($data['crdb']=='cr'){
                            $insertInventorySql  .= ",:inventory ";
                        }
                        $insertInventorySql  .= ") ";
                        $dao->initCommand($insertInventorySql);
                        $dao->addParameter(':lnkrawmaterialid',$data['rawid']);
                        $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid',CONFIG_CID);
                        if($data['crdb']=='cr'){
                            $dao->addParameter(':inventory',$inventory);
                        }
                        $dao->executeNonQuery();
                        $resrawLoc = [];
                        $resrawLoc['id'] = $dao->getLastInsertedId();
                        $resrawLoc['rateperunit'] = 0;
                        $resrawLoc['inventory'] = 0;
                    }else{
                        $totalInventory = ($locInventory + $inventory);

                        $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET inventory=:inventory,
                                                                        inv_unitid=:unit   
                                                                        WHERE lnkrawmaterialid=:id
                                                                        AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':inventory', $totalInventory);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':unit',$unitID);
                        $dao->addParameter(':id', $data['rawid']);
                        $dao->executeNonQuery();
                    }

                    $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate,  :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid)";
                    $dao->initCommand($strinventory);
                    $dao->addParameter(':itemid', $data['rawid']);
                    $dao->addParameter(':rawlocid', $resrawLoc['id']);
                    $dao->addParameter(':inventory', $inventory);
                    $dao->addParameter(':rate', $amount);
                    $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                    $dao->addParameter(':ref_id',0);
                    $dao->addParameter(':invstatus',7);
                    if($data['crdb']=='cr'){
                        $dao->addParameter(':crdb','cr');
                    }else{
                        $dao->addParameter(':crdb','db');
                    }
                    $dao->addParameter(':createddatetime',date('Y-m-d H:i:s'));
                    $dao->addParameter(':created_user',0);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid',  CONFIG_SID);
                    $dao->executeNonQuery();

                    if($data['crdb']=="db"){
                        $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                        $total_inventory = $resrawLoc['inventory'] - $inventory ;
                        $totalPrice = $old_price - $amount;
                        $rateperunit = $totalPrice / $total_inventory;
                        //$rateperunit = ($rateperunit > 0) ? $rateperunit : 0;
                    }else{
                        $oldPrice=($resrawLoc['rateperunit']*$locInventory);

                        $totalPrice=$oldPrice+$amount;
                        $total_inventory = ($locInventory + $inventory);
                        //$rateperunit = $totalPrice / $total_inventory;
                    }
                    /*rateperunit=:rateperunit,*/

                    $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET 
                    inventory=:inventory
                                                                WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                                                and storeid=:storeid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':inventory', $total_inventory);
                    //$dao->addParameter(':rateperunit', $rateperunit);
                    $dao->addParameter(':itemid', $data['rawid']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid',  CONFIG_SID);
                    $dao->executeNonQuery();
                    //Update Inventory
                }
                return json_encode(array('Success'=>'Success','Message'=>"Inventory updated successfully !"));

            }

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - updateInventoryForPMS - ' . $e);
        }
    }


}

?>

