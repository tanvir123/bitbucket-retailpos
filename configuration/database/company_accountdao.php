<?php

namespace database;

class company_accountdao
{
    public $module = 'DB_company_accountdao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function addCompanyAcoount($data,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - addCompanyAcoount');
            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();

            $arr_log = array(
                'Company' => $data['company'],
                'Name' => $data['name'],
                'address' => $data['address'],
                'City' => $data['city'],
                'Phone' => $data['phone'],
                'Email' => $data['email'],
                'Mobile' => $data['mobile'],
                'Limit' => $data['amt_limit'],
                'Status' => ($data['rdo_status'] == 1) ? 'Active' : 'Inactive',
            );
            $json_data = json_encode($arr_log);

            if ($data['id'] == 0) {

                $title = "Add Record";
                $hashkey = \util\util::gethash();

                $strSql = "INSERT INTO " . CONFIG_DBN . ".cfcompany_account SET
                company=:company,
                name=:name,
                salutation=:salutation,
                city=:city,
                state=:state,
                country=:country,
                zipcode=:zipcode,
                phone=:phone,
                mobile=:mobile,
                email=:email,
                address=:address,
                amt_limit=:amt_limit,
                fax=:fax,
                sortkey=:sortkey,
                tinno=:tinno,
                is_active=:isactive,
                hashkey=:hashkey,
                createddatetime=:createddatetime,
                created_user=:created_user,
                companyid=:companyid,
                locationid=:locationid";

                $dao->initCommand($strSql);
                $dao->addParameter(':company', $data['company']);
                $dao->addParameter(':name', $data['name']);
                $dao->addParameter(':salutation', $data['salutation']);
                $dao->addParameter(':city', $data['city']);
                $dao->addParameter(':state', $data['state']);
                $dao->addParameter(':country', $data['country']);
                $dao->addParameter(':zipcode', $data['zipcode']);
                $dao->addParameter(':phone', $data['phone']);
                $dao->addParameter(':mobile', $data['mobile']);
                $dao->addParameter(':email', $data['email']);
                $dao->addParameter(':address', $data['address']);
                $dao->addParameter(':amt_limit', $data['amt_limit']);
                $dao->addParameter(':fax', $data['fax']);
                $dao->addParameter(':sortkey', $data['sortkey']);
                $dao->addParameter(':tinno', $data['tinno']);
                $dao->addParameter(':isactive', $data['rdo_status']);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->addParameter(':created_user', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                $dao->getLastInsertedId();

                $ObjAuditDao->addactivitylog($data['module'], $title, $hashkey, $json_data);
                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_ADD_SUC)));
            } else {

                $title = "Edit Record";

                $strSql = "UPDATE " . CONFIG_DBN . ".cfcompany_account
                            SET
                            company=:company,
                            name=:name,
                            salutation=:salutation,
                            city=:city,
                            state=:state,
                            country=:country,
                            zipcode=:zipcode,
                            phone=:phone,
                            mobile=:mobile,
                            email=:email,
                            address=:address,
                            amt_limit=:amt_limit,
                            fax=:fax,
                            sortkey=:sortkey,
                            tinno=:tinno,
                            modifieddatetime=:modifieddatetime,
                            modified_user=:modified_user
                            WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid";

                $dao->initCommand($strSql);

                $dao->addParameter(':company', $data['company']);
                $dao->addParameter(':name', $data['name']);
                $dao->addParameter(':salutation', $data['salutation']);
                $dao->addParameter(':city', $data['city']);
                $dao->addParameter(':state', $data['state']);
                $dao->addParameter(':country', $data['country']);
                $dao->addParameter(':zipcode', $data['zipcode']);
                $dao->addParameter(':phone', $data['phone']);
                $dao->addParameter(':email', $data['email']);
                $dao->addParameter(':mobile', $data['mobile']);
                $dao->addParameter(':address', $data['address']);
                $dao->addParameter(':amt_limit', $data['amt_limit']);
                $dao->addParameter(':fax', $data['fax']);
                $dao->addParameter(':sortkey', $data['sortkey']);
                $dao->addParameter(':tinno', $data['tinno']);
                $dao->addParameter(':hashkey', $data['id']);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'], $title, $data['id'], $json_data);
                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_UP_SUC)));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addCompanyAcoount - ' . $e);
        }
    }

    public function companyAccountlist($limit, $offset, $company)
    {
        try {
            $this->log->logIt($this->module . ' - companyAccountlist - ' . $company);

            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = "SELECT CNC.*,
            IFNULL(CFU1.username,'') AS createduser,IFNULL(CFU2.username,'') AS modifieduser,
            IFNULL(DATE_FORMAT(CNC.createddatetime,'" . $mysqlformat . "'),'') as created_date,
            IFNULL(DATE_FORMAT(CNC.modifieddatetime,'" . $mysqlformat . "'),'') as modified_date 
            FROM " . CONFIG_DBN . ".cfcompany_account AS CNC
            LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU1 ON CNC.created_user=CFU1.userunkid AND CNC.companyid=CFU1.companyid 
            LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU2 ON CNC.modified_user=CFU2.userunkid AND CNC.modified_user=CFU2.userunkid AND CNC.companyid=CFU2.companyid          
            WHERE CNC.companyid=:companyid AND CNC.locationid=:locationid AND CNC.is_deleted=0";

            if ($company != "")
                $strSql .= " AND CNC.company LIKE '%" . $company . "%'";

            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $data = $dao->executeQuery();

            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return html_entity_decode(json_encode($retvalue));
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - companyAccountlist - ' . $e);
        }
    }

    public function getCompanyAccountRec($data)
    {
        try {
            $this->log->logIt($this->module . " - getCompanyAccountRec");
            $dao = new \dao();
            $id = (isset($data['id'])) ? $data['id'] : "";
            $strSql = "SELECT A.*  FROM " . CONFIG_DBN . ".cfcompany_account AS A 
                          WHERE A.hashkey=:hashkey AND A.companyid=:companyid AND A.locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':hashkey', $id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeRow();
            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res)));

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getCompanyAccountRec - " . $e);
            return false;
        }
    }
}

?>