<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 25/8/17
 * Time: 6:26 PM
 */
namespace database;
class apisettingsdao
{
    public $module = 'DB_apisettingsdao';
    public $log;
    public $dbconnect;

    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function loadCompanyInfo()
    {
        try
        {
            $this->log->logIt($this->module.' - loadCompanyInfo');
            $dao = new \dao();
            $result = array();
            $ObjLocationDao = new \database\locationdao();
            $strSql1 = "SELECT companyname,tabinsta_integration,pms_integration as c_pms_integration,loyalty_integration as c_loyalty_integration,tabinsta_app_id,pms_username,pms_password,tabinsta_app_secret FROM ".CONFIG_DBN.".cfcompany WHERE companyid=:companyid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyid',CONFIG_CID);
            $rec1 = $dao->executeRow();

            $strSql1 = "SELECT pms_username,pms_integration,loyalty_integration,pms_password FROM ".CONFIG_DBN.".cflocation WHERE locationid=:locationid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':locationid',CONFIG_LID);
            $rec5 = $dao->executeRow();
            if($rec5){
                $rec1['pms_integration']=$rec5['pms_integration'];
                $rec1['pms_username']=isset($rec5['pms_username'])?$rec5['pms_username']:'';
                $rec1['pms_password']=isset($rec5['pms_password'])?$rec5['pms_password']:'';
                $rec1['loyalty_integration']=$rec5['loyalty_integration'];
                $rec1['loyalty_appid']=\database\parameter::getparameter('loyalty_appid',CONFIG_CID,CONFIG_LID);
                $rec1['loyalty_appsecret']=\database\parameter::getparameter('loyalty_appsecret',CONFIG_CID,CONFIG_LID);
            }

            $location_list = $ObjLocationDao->getLocationList();
            $locations= \util\util::getkeyfieldarray($location_list,'syslocationid');
            $cm_locations[CONFIG_LID]=$locations[CONFIG_LID];
            /*foreach($cm_locations as $key=>$value)
            {
                $cm_locations[$key]['location_id'] = \database\parameter::getparameter('tabinsta_locationid',CONFIG_CID,$key);
            }*/
            foreach($cm_locations as $key=>$value)
            {
                $cm_locations[$key]['pms_htl_id'] = \database\parameter::getparameter('pms_hotelid',CONFIG_CID,$key);
            }

            $result['company_info'] = $rec1;
            $result['location_list'] = $cm_locations;
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$result)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loadCompanyInfo - '.$e);
        }
    }
    public function UpdateApiSettings($data)
    {
        try
        {
            $this->log->logIt($this->module.' - UpdateApiSettings');
            $dao = new \dao();
			$enable_tabinsta_integration =  isset($data['enable_tabinsta']) && $data['enable_tabinsta'] == 1 ? 1 : 0;
			$enable_pms_integration =  isset($data['enable_pms']) && $data['enable_pms'] == 1 ? 1 : 0;
			$enable_loyalty_integration =  isset($data['enable_loyalty']) && $data['enable_loyalty'] == 1 ? 1 : 0;

            $strSql1 = "UPDATE ".CONFIG_DBN.".cflocation SET pms_username=:pms_username,pms_password=:pms_password,pms_integration=:pms_integration,loyalty_integration=:loyalty_integration WHERE locationid=:locationid";
            $dao->initCommand($strSql1);

            $dao->addParameter(':pms_username',$data['pms_username']);
            $dao->addParameter(':pms_password',$data['pms_password']);
            $dao->addParameter(':pms_integration',$enable_pms_integration);
            $dao->addParameter(':loyalty_integration',$enable_loyalty_integration);

            $dao->addParameter(':locationid',CONFIG_LID);
            $dao->executeNonQuery();

            \database\parameter::setparameter('loyalty_appsecret',empty($enable_loyalty_integration)?'':$data['loyalty_appsecret'], CONFIG_CID, CONFIG_LID);
            \database\parameter::setparameter('loyalty_appid',empty($enable_loyalty_integration)?'':$data['loyalty_appid'], CONFIG_CID, CONFIG_LID);

           /* if(isset($data['enable_tabinsta']) && $data['enable_tabinsta']==1 && isset($data['tabinstalocation']) && count($data['tabinstalocation'])>0) {
                $arr_loc = $data['tabinstalocation'];
                $duplicates = array_unique($arr_loc);
                $cnt = count($duplicates);
                $keyln = count($arr_loc);

                if($cnt == $keyln) {
                    foreach ($arr_loc as $key => $value) {
                        \database\parameter::setparameter('tabinsta_locationid', $value, CONFIG_CID, $key);
                    }
                }
                else{
                   return html_entity_decode(json_encode(array("Success"=>"False","Data"=>$duplicates,"Message"=>"Please_Select_Different_Location")));
                }

            }*/
            if(isset($data['enable_pms']) && $data['enable_pms']==1 && isset($data['pms_hotel_id']) && count($data['pms_hotel_id'])>0)
            {
                $arr = $data['pms_hotel_id'];
                foreach ($arr as $key=>$value)
                {
                    \database\parameter::setparameter('pms_hotelid',$value,CONFIG_CID,$key);
                }
            }
            return html_entity_decode(json_encode(array("Success"=>"True","Message"=>"Settings_Updated_Successfully")));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - UpdateApiSettings - '.$e);
            return 0;
        }
    }

}
?>