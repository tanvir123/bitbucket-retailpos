<?php

namespace database;

class good_receipt_notedao
{
    public $module = 'DB_good_receipt_note';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function grnlist($limit, $offset, $fromdate, $todate, $grnNo, $vendorid,$voucherno)
    {
        try {
            $this->log->logIt($this->module . ' - grnlist');
            $objutil = new \util\util;
            $fromdate = $objutil->convertDateToMySql($fromdate);
            $todate = $objutil->convertDateToMySql($todate);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $strSql = "SELECT CGRN.grnid,CGRN.lnkorderid,CGRN.voucher_no,CGRN.grn_doc_num,IFNULL(CGRN.remarks,'') as remarks,ROUND(CGRN.totalamount,$round_off)as totalamount,TRC.business_name, 
                        IFNULL(DATE_FORMAT(CGRN.grn_date,'" . $mysqlformat . "'),'') AS grn_date,IFNULL(CFU1.username,'') AS createduser,CGRN.is_returned,
                        IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CGRN.createddatetime,'" . $mysqlformat . "'),'') AS created_date,
                        IFNULL(DATE_FORMAT(CGRN.modifieddatetime,'" . $mysqlformat . "'),'') AS modified_date                                   
                        FROM " . CONFIG_DBN . ".cfgrn AS CGRN
                        LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU1 ON CGRN.createduser=CFU1.userunkid AND CGRN.companyid=CFU1.companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU2 ON CGRN.modifieduser=CFU2.userunkid AND CGRN.modifieduser=CFU2.userunkid AND CGRN.companyid=CFU2.companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfvandor AS CV ON CGRN.lnkvendorid=CV.vandorunkid AND CV.companyid=:companyid
                        LEFT JOIN ".CONFIG_DBN.".trcontact AS TRC ON CV.lnkcontactid=TRC.contactunkid AND TRC.companyid=:companyid
                        WHERE CGRN.companyid=:companyid AND CGRN.storeid=:storeid AND CGRN.is_deleted=0 ";

            if ($grnNo != "") {
                $strSql .= " AND CGRN.grn_doc_num LIKE '%" . $grnNo . "%'";
            }
            if ($vendorid != 0) {
                $strSql .= " AND CGRN.lnkvendorid =" . $vendorid . "";
            }
            if ($voucherno != "") {
                $strSql .= " AND CGRN.voucher_no LIKE '%" . $voucherno . "%'";
            }
            if ($fromdate != "" && $todate != "") {
                $strSql .= " AND CGRN.grn_date BETWEEN  '" . $fromdate . "' AND '" . $todate . "' ";
            }
            $strSql .= " ORDER BY CGRN.grnid DESC";
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $data = $dao->executeQuery();

            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
                $dao->initCommand($strSql);

            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return html_entity_decode(json_encode($retvalue),ENT_QUOTES);
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue),ENT_QUOTES);
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - grnlist - ' . $e);
        }
    }
    public function addGrn($data,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - addGrn - ');
            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $grnarray = $data['grn_items'];
            $objutil = new \util\util;
            $grn_date = $objutil->convertDateToMySql($data['grn_date']);
            $ObjCommonDao = new \database\commondao();
            $dao->bindTransaction();
            if ($data['id'] == '0' || $data['id'] == '') {
                $grn_doc_num = $ObjCommonDao->getIndentDocumentNumbering('good_receipt_note', 'inc');
                $voucherNo = $ObjCommonDao->getIndentDocumentNumbering('grn_voucher_no', 'inc');
                $strSql = "SELECT startno FROM ".CONFIG_DBN.".cfindentnumber WHERE companyid=:companyid AND storeid=:storeid AND keyname=:keyname";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':keyname', "good_receipt_note");
                $record = $dao->executeRow();

                $sql = "INSERT into ".CONFIG_DBN.".cfgrn SET 
                grn_date=:grn_date,
                grn_doc_num=:grn_doc_num,
                voucher_no=:voucher_no,
                pi_number=:pi_number,
                lnkvendorid=:lnkvendorid,
                companyid=:companyid,
                storeid=:storeid,
                createddatetime=:createddatetime,
                createduser=:createduser,
                totalamount=:total_amount,
                remarks=:remarks";

                $dao->initCommand($sql);
                $dao->addParameter(':grn_date', $grn_date);
                $dao->addParameter(':grn_doc_num', $grn_doc_num);
                $dao->addParameter(':voucher_no', $voucherNo);
                $dao->addParameter(':pi_number', $record['startno']);
                $dao->addParameter(':lnkvendorid', $data['grn_sel_vendor']);
                $dao->addParameter(':total_amount', $data['total_amount']);
                $dao->addParameter(':remarks', $data['grn_remarks']);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->addParameter(':createduser', CONFIG_UID);
                $dao->executeNonQuery();
                $grnId = $dao->getLastInsertedId();
                if($grnarray){
                    foreach ($grnarray AS $key => $value) {
                        if(is_array($value['item_tax_det']) && count($value['item_tax_det'])>0){
                            $tax_description = html_entity_decode(json_encode($value['item_tax_det']),ENT_QUOTES);
                        }else{
                            $tax_description = '';
                        }
                        $sql = "INSERT into ".CONFIG_DBN.".cfgrndetail SET 
                            lnkgrnid=:lnkgrnid,lnkrawid=:lnkrawid,
                            lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,
                            qty=:qty,rate=:rate,discount_per=:discount_per,discount_amount=:discount_amount,tax_amount=:tax_amount,
                            tax_description=:tax_description,total=:total,storeid=:storeid,companyid=:companyid";
                        $dao->initCommand($sql);
                        $dao->addParameter(':lnkgrnid', $grnId);
                        $dao->addParameter(':lnkrawid', $value['item_id']);
                        $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                        $dao->addParameter(':lnkunitid', $value['item_unit']);
                        $dao->addParameter(':qty', $value['item_qty']);
                        $dao->addParameter(':rate', $value['item_rpu']);
                        $dao->addParameter(':discount_per', $value['item_disc_per']);
                        $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                        $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                        $dao->addParameter(':tax_description', $tax_description);
                        $dao->addParameter(':total',$value['item_total_amt']);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->executeNonQuery();
                        $grnDetailId = $dao->getLastInsertedId();

                        //Update Inventory
                        $amount = $value['item_total_amt'] + $value['item_disc_amt'] - $value['item_tax_amt'];
                        $inventory = $value['item_qty'] * $value['item_unit_val'];

                        $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc 
                                  WHERE lnkrawmaterialid=:item_id
                                  AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strrawLoc);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':item_id', $value['item_id']);
                        $resrawLoc = $dao->executeRow();

                        $dao->initCommand("SELECT u.unitunkid FROM ".CONFIG_DBN.".cfrawmaterial CFR 
					INNER JOIN ".CONFIG_DBN.".cfunit u ON u.measuretype = CFR.measuretype AND u.is_systemdefined=1 AND u.companyid=:companyid
					 WHERE CFR.id = :id AND CFR.companyid=:companyid ");
                        $dao->addParameter(':id',$value['item_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);

                        $rawMaterialRec = $dao->executeRow();
                        if(!$resrawLoc)
                        {
                            $insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
                            $dao->initCommand($insertInventorySql);
                            $dao->addParameter(':lnkrawmaterialid',$value['item_id']);
                            $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                            $dao->addParameter(':storeid',CONFIG_SID);
                            $dao->addParameter(':companyid',CONFIG_CID);
                            $dao->executeNonQuery();
                            $resrawLoc = [];
                            $resrawLoc['id'] = $dao->getLastInsertedId();
                            $resrawLoc['rateperunit'] = 0;
                            $resrawLoc['inventory'] = 0;
                        }
                        $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,lnkvendorid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:lnkvendorid)";
                        $dao->initCommand($strinventory);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':rawlocid', $resrawLoc['id']);
                        $dao->addParameter(':inventory', $inventory);
                        $dao->addParameter(':lnkvendorid', $data['grn_sel_vendor']);
                        $dao->addParameter(':rate', $amount);
                        $dao->addParameter(':total_rate', $value['item_total_amt']);
                        $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                        $dao->addParameter(':ref_id',$grnDetailId);
                        $dao->addParameter(':invstatus',INV_STATUS_GRN);
                        $dao->addParameter(':crdb','cr');
                        $dao->addParameter(':createddatetime',$datetime);
                        $dao->addParameter(':created_user',CONFIG_UID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->executeNonQuery();

                        $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                        $total_inventory = $resrawLoc['inventory'] + $inventory ;
                        $totalPrice = $old_price + $value['item_total_amt'];
                        $rateperunit = $totalPrice / $total_inventory;
                        $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                        $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                                                WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                                                and storeid=:storeid";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':inventory', $total_inventory);
                        $dao->addParameter(':rateperunit', $rateperunit);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->executeNonQuery();

                        //Update Inventory//
                    }
                }

                $dao->releaseTransaction(TRUE);
                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_ADD_SUC)),ENT_QUOTES);
            } else {

                $strforcheckkey = "SELECT grndetailunkid  FROM ".CONFIG_DBN.".cfgrndetail 
                WHERE lnkgrnid=:lnkgrnid AND companyid=:companyid AND storeid=:storeid";
                $dao->initCommand($strforcheckkey);
                $dao->addParameter(':lnkgrnid', $data['id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $resultforidcheck = $dao->executeQuery();

                $grndetailunkid = \util\util::getoneDarray($resultforidcheck, 'grndetailunkid');

                $grnarr = [];
                foreach ($grnarray as $key => $value) {
                    if(is_array($value['item_tax_det']) && count($value['item_tax_det'])>0){
                        $tax_description = html_entity_decode(json_encode($value['item_tax_det']),ENT_QUOTES);
                    }else{
                        $tax_description = '';
                    }
                    if (in_array($value['itm_detId'], $grndetailunkid)) {

                        $strsql = "UPDATE ".CONFIG_DBN.".cfgrndetail SET 
                            lnkrawid=:lnkrawid,lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,
                            qty=:qty,rate=:rate,discount_per=:discount_per,discount_amount=:discount_amount,tax_amount=:tax_amount,
                            tax_description=:tax_description,total=:total
                            WHERE grndetailunkid=:grndetailunkid AND lnkgrnid=:lnkgrnid AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strsql);
                        $dao->addParameter(':grndetailunkid', $value['itm_detId']);
                        $dao->addParameter(':lnkgrnid', $data['id']);
                        $dao->addParameter(':lnkrawid', $value['item_id']);
                        $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                        $dao->addParameter(':lnkunitid', $value['item_unit']);
                        $dao->addParameter(':qty', $value['item_qty']);
                        $dao->addParameter(':rate', $value['item_rpu']);
                        $dao->addParameter(':discount_per', $value['item_disc_per']);
                        $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                        $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                        $dao->addParameter(':tax_description', $tax_description);
                        $dao->addParameter(':total',$value['item_total_amt']);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->executeNonQuery();
                        $grnarr[] = $value['itm_detId'];
                        
                        //Update Inventory
                        $amount = $value['item_total_amt'] + $value['item_disc_amt'] - $value['item_tax_amt'];
                        $inventory = $value['item_qty'] * $value['item_unit_val'];

                        $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc 
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strrawLoc);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':item_id', $value['item_id']);
                        $resrawLoc = $dao->executeRow();

                        $dao->initCommand("SELECT u.unitunkid FROM ".CONFIG_DBN.".cfrawmaterial CFR 
					INNER JOIN ".CONFIG_DBN.".cfunit u ON u.measuretype = CFR.measuretype AND u.is_systemdefined=1 AND u.companyid=:companyid
					 WHERE CFR.id = :id AND CFR.companyid=:companyid ");
                        $dao->addParameter(':id',$value['item_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $rawMaterialRec = $dao->executeRow();
                        if(!$resrawLoc)
                        {
                            $insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
                            $dao->initCommand($insertInventorySql);
                            $dao->addParameter(':lnkrawmaterialid',$value['item_id']);
                            $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                            $dao->addParameter(':storeid',CONFIG_SID);
                            $dao->addParameter(':companyid',CONFIG_CID);
                            $dao->executeNonQuery();
                            $resrawLoc = [];
                            $resrawLoc['id'] = $dao->getLastInsertedId();
                            $resrawLoc['rateperunit'] = 0;
                            $resrawLoc['inventory'] = 0;
                        }

                        $strInvDetail = "SELECT inventory,total_rate FROM ".CONFIG_DBN.".cfinventorydetail 
                                      WHERE lnkrawmaterialid=:itemid AND ref_id=:ref_id AND inv_status=:inv_status
                                      AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strInvDetail);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':ref_id', $value['itm_detId']);
                        $dao->addParameter(':inv_status', INV_STATUS_GRN);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $resInvDetail = $dao->executeRow();

                        $strInv = "UPDATE " . CONFIG_DBN . ".cfinventorydetail 
                                   SET inventory=:inventory,rate=:rate,total_rate=:total_rate,inv_unitid=:inv_unitid                      
                                   WHERE lnkrawmaterialid=:itemid AND ref_id=:ref_id AND inv_status=:inv_status
                                   AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strInv);
                        $dao->addParameter(':inventory', $inventory);
                        $dao->addParameter(':rate', $amount);
                        $dao->addParameter(':total_rate', $value['item_total_amt']);
                        $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':ref_id', $value['itm_detId']);
                        $dao->addParameter(':inv_status', INV_STATUS_GRN);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->executeNonQuery();

                        if($resInvDetail['inventory'] > $inventory){
                            $newDetail_Inv= $resInvDetail['inventory'] - $inventory;
                            $total_inventory = $resrawLoc['inventory'] - $newDetail_Inv ;
                        }
                        else{
                            $newDetail_Inv= $inventory - $resInvDetail['inventory'];
                            $total_inventory = $resrawLoc['inventory'] + $newDetail_Inv ;
                        }

                        $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                        if($resInvDetail['total_rate'] > $value['item_total_amt']){
                            $newDetail_Rate= $resInvDetail['total_rate'] - $value['item_total_amt'];
                            $totalPrice = $old_price - $newDetail_Rate;
                        }
                        else{
                            $newDetail_Rate= $value['item_total_amt'] - $resInvDetail['total_rate'];
                            $totalPrice = $old_price + $newDetail_Rate;
                        }
                        $rateperunit = $totalPrice / $total_inventory;
                        $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;
                        $strrawLoc = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                                                WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                                                and storeid=:storeid";
                        $dao->initCommand($strrawLoc);
                        $dao->addParameter(':inventory', $total_inventory);
                        $dao->addParameter(':rateperunit', $rateperunit);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->executeNonQuery();
                        //Update Inventory//

                        $dao->releaseTransaction(TRUE);
                    }
                    else {

                        $sql = "INSERT into ".CONFIG_DBN.".cfgrndetail SET 
                            lnkgrnid=:lnkgrnid,lnkrawid=:lnkrawid,
                            lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,
                            qty=:qty,rate=:rate,discount_per=:discount_per,discount_amount=:discount_amount,tax_amount=:tax_amount,
                            tax_description=:tax_description,total=:total,storeid=:storeid,companyid=:companyid";
                        $dao->initCommand($sql);
                        $dao->addParameter(':lnkgrnid', $data['id']);
                        $dao->addParameter(':lnkrawid', $value['item_id']);
                        $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                        $dao->addParameter(':lnkunitid', $value['item_unit']);
                        $dao->addParameter(':qty', $value['item_qty']);
                        $dao->addParameter(':rate', $value['item_rpu']);
                        $dao->addParameter(':discount_per', $value['item_disc_per']);
                        $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                        $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                        $dao->addParameter(':tax_description', $tax_description);
                        $dao->addParameter(':total',$value['item_total_amt']);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->executeNonQuery();
                        $grnarr[] = $dao->getLastInsertedId();
                        $grnDetailId = $dao->getLastInsertedId();

                        //Update Inventory
                        $amount = $value['item_total_amt'] + $value['item_disc_amt'] - $value['item_tax_amt'];
                        $inventory = $value['item_qty'] * $value['item_unit_val'];

                        $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc 
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strrawLoc);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':item_id', $value['item_id']);
                        $resrawLoc = $dao->executeRow();

                        $dao->initCommand("SELECT u.unitunkid FROM ".CONFIG_DBN.".cfrawmaterial CFR 
					INNER JOIN ".CONFIG_DBN.".cfunit u ON u.measuretype = CFR.measuretype AND u.is_systemdefined=1 AND u.companyid=:companyid
					 WHERE CFR.id = :id AND CFR.companyid=:companyid ");
                        $dao->addParameter(':id',$value['item_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $rawMaterialRec = $dao->executeRow();
                        if(!$resrawLoc)
                        {
                            $insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
                            $dao->initCommand($insertInventorySql);
                            $dao->addParameter(':lnkrawmaterialid',$value['item_id']);
                            $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                            $dao->addParameter(':storeid',CONFIG_SID);
                            $dao->addParameter(':companyid',CONFIG_CID);
                            $dao->executeNonQuery();
                            $resrawLoc = [];
                            $resrawLoc['id'] = $dao->getLastInsertedId();
                            $resrawLoc['rateperunit'] = 0;
                            $resrawLoc['inventory'] = 0;
                        }

                        $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,lnkvendorid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:lnkvendorid)";
                        $dao->initCommand($strinventory);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':rawlocid', $resrawLoc['id']);
                        $dao->addParameter(':inventory', $inventory);
                        $dao->addParameter(':rate', $amount);
                        $dao->addParameter(':lnkvendorid', $data['grn_sel_vendor']);
                        $dao->addParameter(':total_rate', $value['item_total_amt']);
                        $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                        $dao->addParameter(':ref_id',$grnDetailId);
                        $dao->addParameter(':invstatus',INV_STATUS_GRN);
                        $dao->addParameter(':crdb','cr');
                        $dao->addParameter(':createddatetime',$datetime);
                        $dao->addParameter(':created_user',CONFIG_UID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->executeNonQuery();

                        $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                        $total_inventory = $resrawLoc['inventory'] + $inventory ;
                        $totalPrice = $old_price + $value['item_total_amt'];
                        $rateperunit = $totalPrice / $total_inventory;
                        $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                        $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                                                WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                                                and storeid=:storeid";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':inventory', $total_inventory);
                        $dao->addParameter(':rateperunit', $rateperunit);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->executeNonQuery();
                        //Update Inventory//
                        $dao->releaseTransaction(TRUE);
                    }
                }

                //Update Inventory while delete
                $diffarr= array_diff($grndetailunkid,$grnarr);
                foreach($diffarr as $dkey=>$dval){
                    $strInv = "SELECT lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status
                                      FROM ".CONFIG_DBN.".cfinventorydetail 
                                      WHERE ref_id=:ref_id AND inv_status=:inv_status
                                      AND companyid=:companyid AND storeid=:storeid limit 1";
                    $dao->initCommand($strInv);
                    $dao->addParameter(':ref_id', $dval);
                    $dao->addParameter(':inv_status', INV_STATUS_GRN);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid', CONFIG_SID);
                    $resINV = $dao->executeRow();

                    $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc 
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                    $dao->initCommand($strrawLoc);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':item_id', $resINV['lnkrawmaterialid']);
                    $resrawLoc = $dao->executeRow();

                    $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,lnkvendorid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:lnkvendorid)";
                    $dao->initCommand($strinventory);
                    $dao->addParameter(':itemid', $resINV['lnkrawmaterialid']);
                    $dao->addParameter(':rawlocid', $resINV['lnkrawmateriallocid']);
                    $dao->addParameter(':inventory', $resINV['inventory']);
                    $dao->addParameter(':rate', $resINV['rate']);
                    $dao->addParameter(':lnkvendorid', $data['grn_sel_vendor']);
                    $dao->addParameter(':total_rate', $resINV['total_rate']);
                    $dao->addParameter(':inv_unitid',$resINV['inv_unitid']);
                    $dao->addParameter(':ref_id',$resINV['ref_id']);
                    $dao->addParameter(':invstatus',INV_STATUS_GRN);
                    $dao->addParameter(':crdb','db');
                    $dao->addParameter(':createddatetime',$datetime);
                    $dao->addParameter(':created_user',CONFIG_UID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid', CONFIG_SID);
                    $dao->executeNonQuery();

                    $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                    $total_inventory = $resrawLoc['inventory'] - $resINV['inventory'];
                    $totalPrice = $old_price - $resINV['total_rate'];
                    $rateperunit = $totalPrice / $total_inventory;
                    $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                    $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                                                WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                                                and storeid=:storeid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':inventory', $total_inventory);
                    $dao->addParameter(':rateperunit', $rateperunit);
                    $dao->addParameter(':itemid', $resINV['lnkrawmaterialid']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid', CONFIG_SID);
                    $dao->executeNonQuery();
                    //Update Inventory//

                }
                $strsql = "DELETE FROM ".CONFIG_DBN.".cfgrndetail              
                           WHERE grndetailunkid NOT IN ('".implode("','",$grnarr)."') 
                           AND lnkgrnid=:lnkgrnid AND companyid=:companyid AND storeid=:storeid";
                $dao->initCommand($strsql);
                $dao->addParameter(':lnkgrnid', $data['id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->executeNonQuery();

                $sql = "UPDATE ".CONFIG_DBN.".cfgrn SET
                        totalamount=:totalamount,modifieddatetime=:modifieddatetime,
                        remarks=:remarks,
                        modifieduser=:modifieduser WHERE grnid=:grnid AND companyid=:companyid AND storeid=:storeid";
                $dao->initCommand($sql);
                $dao->addParameter(':grnid', $data['id']);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':modifieduser', CONFIG_UID);
                $dao->addParameter(':totalamount', $data['total_amount']);
                $dao->addParameter(':remarks', $data['grn_remarks']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->executeNonQuery();
                $dao->releaseTransaction(TRUE);
                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_UP_SUC)),ENT_QUOTES);
            }
        } catch (Exception $e) {
            $dao->releaseTransaction(FALSE);
            $this->log->logIt($this->module.' - addGrn - '.$e);
        }
    }

    public function addGrnFromPO($data,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - addGrnFromPO - ');
            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $grnarray = $data['grn_items'];
            $objutil = new \util\util;
            $grn_date = $objutil->convertDateToMySql($data['grn_date']);

            $ObjCommonDao = new \database\commondao();
            if ($data['orderid'] != '0' && $data['orderid'] != '') {
                $grn_doc_num = $ObjCommonDao->getIndentDocumentNumbering('good_receipt_note', 'inc');
                $voucherNo = $ObjCommonDao->getIndentDocumentNumbering('grn_voucher_no', '');

                $strSql = "SELECT startno FROM ".CONFIG_DBN.".cfindentnumber WHERE companyid=:companyid AND storeid=:storeid AND keyname=:keyname";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':keyname', "good_receipt_note");
                $record = $dao->executeRow();
                $sql = "INSERT into ".CONFIG_DBN.".cfgrn SET 
                grn_date=:grn_date,
                grn_doc_num=:grn_doc_num,
                voucher_no=:voucher_no,
                pi_number=:pi_number,
                lnkorderid=:lnkorderid,
                lnkvendorid=:lnkvendorid,
                companyid=:companyid,
                storeid=:storeid,
                createddatetime=:createddatetime,
                createduser=:createduser,
                totalamount=:total_amount,
                remarks=:remarks";

                $dao->initCommand($sql);
                $dao->addParameter(':grn_date', $grn_date);
                $dao->addParameter(':grn_doc_num', $grn_doc_num);
                $dao->addParameter(':voucher_no', $voucherNo);
                $dao->addParameter(':pi_number', $record['startno']);
                $dao->addParameter(':lnkorderid', $data['orderid']);
                $dao->addParameter(':lnkvendorid', $data['grn_sel_vendor']);
                $dao->addParameter(':total_amount', $data['total_amount']);
                $dao->addParameter(':remarks', $data['grn_remarks']);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->addParameter(':createduser', CONFIG_UID);
                $dao->executeNonQuery();

                $grnId = $dao->getLastInsertedId();
                foreach ($grnarray AS $key => $value) {
                    if(is_array($value['item_tax_det']) && count($value['item_tax_det'])>0){
                        $tax_description = html_entity_decode(json_encode($value['item_tax_det']),ENT_QUOTES);
                    }else{
                        $tax_description = '';
                    }
                    $sql = "INSERT into ".CONFIG_DBN.".cfgrndetail SET 
                            lnkgrnid=:lnkgrnid,lnkorderdetailid=:lnkorderdetailid,lnkrawid=:lnkrawid,
                            lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,
                            qty=:qty,rate=:rate,discount_per=:discount_per,discount_amount=:discount_amount,tax_amount=:tax_amount,
                            tax_description=:tax_description,total=:total,storeid=:storeid,companyid=:companyid";
                    $dao->initCommand($sql);
                    $dao->addParameter(':lnkgrnid', $grnId);
                    $dao->addParameter(':lnkorderdetailid', $value['itm_detId']);
                    $dao->addParameter(':lnkrawid', $value['item_id']);
                    $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                    $dao->addParameter(':lnkunitid', $value['item_unit']);
                    $dao->addParameter(':qty', $value['item_qty']);
                    $dao->addParameter(':rate', $value['item_rpu']);
                    $dao->addParameter(':discount_per', $value['item_disc_per']);
                    $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                    $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                    $dao->addParameter(':tax_description', $tax_description);
                    $dao->addParameter(':total',$value['item_total_amt']);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->executeNonQuery();
                    $grnDetailId = $dao->getLastInsertedId();


                    $sqlPO = "UPDATE ".CONFIG_DBN.".cfpurchaseorderdetail SET 
                            lnkgrndetailid=:lnkgrndetailid
                            WHERE lnkorderid=:lnkorderid AND orderdetailunkid=:orderdetailunkid AND storeid=:storeid AND companyid=:companyid";
                    $dao->initCommand($sqlPO);
                    $dao->addParameter(':lnkorderid', $data['orderid']);
                    $dao->addParameter(':orderdetailunkid', $value['itm_detId']);
                    $dao->addParameter(':lnkgrndetailid', $grnDetailId);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->executeNonQuery();

                    //Update Inventory
                    $amount = $value['item_total_amt'] + $value['item_disc_amt'] - $value['item_tax_amt'];
                    $inventory = $value['item_qty'] * $value['item_unit_val'];

                    $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc 
                                  WHERE lnkrawmaterialid=:item_id
                                  AND companyid=:companyid AND storeid=:storeid";
                    $dao->initCommand($strrawLoc);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':item_id', $value['item_id']);
                    $resrawLoc = $dao->executeRow();

                    $dao->initCommand("SELECT u.unitunkid FROM ".CONFIG_DBN.".cfrawmaterial CFR 
					INNER JOIN ".CONFIG_DBN.".cfunit u ON u.measuretype = CFR.measuretype AND u.is_systemdefined=1 AND u.companyid=:companyid
					 WHERE CFR.id = :id AND CFR.companyid=:companyid ");
                    $dao->addParameter(':id',$value['item_id']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $rawMaterialRec = $dao->executeRow();
                    if(!$resrawLoc)
                    {
                        $insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
                        $dao->initCommand($insertInventorySql);
                        $dao->addParameter(':lnkrawmaterialid',$value['item_id']);
                        $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                        $dao->addParameter(':storeid',CONFIG_SID);
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $dao->executeNonQuery();
                        $resrawLoc = [];
                        $resrawLoc['id'] = $dao->getLastInsertedId();
                        $resrawLoc['rateperunit'] = 0;
                        $resrawLoc['inventory'] = 0;
                    }

                    $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,lnkvendorid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:lnkvendorid)";
                    $dao->initCommand($strinventory);
                    $dao->addParameter(':itemid', $value['item_id']);
                    $dao->addParameter(':rawlocid', $resrawLoc['id']);
                    $dao->addParameter(':inventory', $inventory);
                    $dao->addParameter(':rate', $amount);
                    $dao->addParameter(':lnkvendorid', $data['grn_sel_vendor']);
                    $dao->addParameter(':total_rate', $value['item_total_amt']);
                    $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                    $dao->addParameter(':ref_id',$grnDetailId);
                    $dao->addParameter(':invstatus',INV_STATUS_GRN);
                    $dao->addParameter(':crdb','cr');
                    $dao->addParameter(':createddatetime',$datetime);
                    $dao->addParameter(':created_user',CONFIG_UID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid', CONFIG_SID);
                    $dao->executeNonQuery();

                    $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                    $total_inventory = $resrawLoc['inventory'] + $inventory ;
                    $totalPrice = $old_price + $value['item_total_amt'];
                    $rateperunit = $totalPrice / $total_inventory;
                    $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                    $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                                                WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                                                and storeid=:storeid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':inventory', $total_inventory);
                    $dao->addParameter(':rateperunit', $rateperunit);
                    $dao->addParameter(':itemid', $value['item_id']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid', CONFIG_SID);
                    $dao->executeNonQuery();

                    //Update Inventory//
                }

                /*Check if all grn is done*/
                $strCNT = "SELECT COUNT(orderdetailunkid) as cnt FROM ".CONFIG_DBN.".cfpurchaseorderdetail WHERE lnkorderid=:orderid ".
                    "AND companyid=:companyid AND storeid=:storeid AND lnkgrndetailid=0";
                $dao->initCommand($strCNT);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':orderid', $data['orderid']);
                $resCNT = $dao->executeRow();
                if($resCNT['cnt']==0){
                    $strSql5 = "UPDATE ".CONFIG_DBN.".cfpurchaseorder SET status=3 WHERE orderid=:orderid AND companyid=:companyid AND storeid=:storeid";
                    $dao->initCommand($strSql5);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':orderid', $data['orderid']);
                    $dao->executeNonQuery();
                }
                else{
                    $strSql5 = "UPDATE ".CONFIG_DBN.".cfpurchaseorder SET status=2 WHERE orderid=:orderid AND companyid=:companyid AND storeid=:storeid";
                    $dao->initCommand($strSql5);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':orderid', $data['orderid']);
                    $dao->executeNonQuery();
                }
                /*Check if all grn is done*/
                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_ADD_SUC)),ENT_QUOTES);
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module.' - addGrnFromPO - '.$e);
        }
    }

    public function getgrnRecordsbyID($data)
    {
        try {
            $this->log->logIt($this->module . " - getgrnRecordsbyID");
            $dao = new \dao();
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $strgrn = "SELECT CGRN.grnid,CGRN.grn_doc_num,CGRN.voucher_no,IFNULL(CGRN.remarks,'') as remarks,ROUND(CGRN.totalamount,$round_off)as totalamount,
            CGRN.lnkvendorid,IFNULL(DATE_FORMAT(CGRN.grn_date,'" . $mysqlformat . "'),'') as grn_date
            FROM " . CONFIG_DBN . ".cfgrn AS CGRN 
            WHERE CGRN.grnid=:grnid and CGRN.companyid=:companyid and CGRN.storeid=:storeid ";
            $dao->initCommand($strgrn);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addParameter(':grnid', $data['id']);
            $resgrn = $dao->executeRow();

            $strgrnDetail = "SELECT CR.name AS storeitem,CU.name as unit,CU.unit as unit_val,
            CGRND.lnkgrnid,CGRND.grndetailunkid,CGRND.lnkrawid,CGRND.lnkrawcatid,CGRND.lnkunitid,ROUND(CGRND.qty,$round_off) as qty,ROUND(CGRND.rate,$round_off)as rate,
            CGRND.discount_per,ROUND(CGRND.discount_amount,$round_off) as discount_amount,ROUND(CGRND.tax_amount,$round_off) as tax_amount,
            CGRND.tax_description,ROUND(CGRND.total,$round_off)as total
            FROM " . CONFIG_DBN . ".cfgrndetail AS CGRND
            LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CGRND.lnkrawid=CR.id 
            LEFT JOIN " . CONFIG_DBN . ".cfunit CU ON CGRND.lnkunitid=CU.unitunkid AND
            CU.companyid=:companyid AND CU.is_active=1 and CU.is_deleted=0 
            WHERE CGRND.lnkgrnid=:lnkgrnid and CGRND.companyid=:companyid and CGRND.storeid=:storeid AND CGRND.lnkgrdetailid=0";

            $dao->initCommand($strgrnDetail);
            $dao->addParameter(':lnkgrnid',  $data['id']);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $resgrnDetail = $dao->executeQuery();

            return html_entity_decode(json_encode(array("Success" => "True", "good_receipt" => $resgrn, "grnDetail" => $resgrnDetail)),ENT_QUOTES);
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getgrnRecordsbyID - " . $e);
            return false;
        }
    }

    public function getlastgrnno()
    {
        try {
            $this->log->logIt($this->module . " - getlastgrnno");
            $dao = new \dao();
            $strSql = "SELECT prefix,startno  FROM " . CONFIG_DBN . ".cfindentnumber WHERE 
            storeid=:storeid AND keyname='good_receipt_note'";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_LID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $res = $dao->executeRow();
            $indent_doc_number = $res['prefix'] . $res['startno'];

            return $indent_doc_number;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getlastgrnno - " . $e);
            return false;
        }
    }

    public function getAllVendorList()
    {
        try {
            $this->log->logIt($this->module . '-getAllVendorList');
            $dao = new \dao();
            $strSql = "SELECT CFV.vandorunkid, TC.business_name FROM ".CONFIG_DBN.".cfvandor CFV 
            INNER JOIN ".CONFIG_DBN.".trcontact TC ON TC.contactunkid = CFV.lnkcontactid WHERE CFV.companyid=:companyid AND  CFV.is_active=1 AND  CFV.is_deleted=0";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $rec = $dao->executeQuery();
            return $rec;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . '-getAllVendorList -' . $e);
        }
    }
    public function getVendorCategory($data)
    {
        try {
            $this->log->logIt($this->module . ' - getVendorCategory');
            $categories = [];
            if (isset($data['vendorid']) && $data['vendorid'] > 0) {
                $dao = new \dao();
                $dao->initCommand("SELECT DISTINCT lnkcategoryid,CTRC.name AS category_name FROM " . CONFIG_DBN . ".cfrawmaterial CFR
              LEFT JOIN ".CONFIG_DBN.". cfinventorydetail ID ON CFR.id= ID.lnkrawmaterialid AND ID.lnkvendorid=:lnkvandorid
                 INNER JOIN " . CONFIG_DBN . ".cfrawmaterial_category CTRC 
				    ON CTRC.id = CFR.lnkcategoryid AND CTRC.is_deleted=0 AND CTRC.companyid = :companyid
				    WHERE  CFR.companyid = :companyid AND CFR.is_deleted=0 AND CFR.is_active=1 ORDER BY CTRC.name ASC ");
                $dao->addParameter(':lnkvandorid', $data['vendorid']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $categories = $dao->executeQuery();
            }
            return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => 'Vendor categories', 'categories' => $categories)),ENT_QUOTES);
        } catch (\Exception $e) {
            $this->log->logIt($this->module . ' - getVendorCategory - ' . $e);
        }
    }

    public function getRawMaterialFromCategory($data)
    {
        try {
            $this->log->logIt($this->module . " - getRawMaterialFromCategory");
            $dao = new \dao();

            $strSql = " SELECT id,name,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial
            WHERE is_active=1 and is_deleted=0 AND lnkcategoryid=:lnkcategoryid AND companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':lnkcategoryid', $data['rawcatid']);
            $res = $dao->executeQuery();

            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res)),ENT_QUOTES);
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getRawMaterialFromCategory - " . $e);
            return false;
        }
    }

	public function getAppliedTax()
	{
		try
		{
			$this->log->logIt($this->module.' - getAppliedTax');
			$todaysdate = \util\util::getLocalDate();
			$dao = new \dao();

			$strSql = " SELECT CASE WHEN CFTD.postingrule=1 THEN '%' WHEN CFTD.postingrule=2
                       THEN 'FLAT' END as pytype,CFTD.amount,CFT.taxunkid,CFT.lnkmasterunkid,CFTD.taxdetailunkid,CFT.tax,CFTD.postingrule,CFTD.taxapplyafter
                        FROM ".CONFIG_DBN.".cftax AS CFT
                        INNER JOIN ".CONFIG_DBN.".cftaxdetail CFTD
                          ON CFT.taxunkid = CFTD.taxunkid AND
                             CFTD.taxdetailunkid = (SELECT taxdetailunkid FROM ".CONFIG_DBN.".cftaxdetail WHERE taxunkid=CFT.taxunkid ORDER BY entrydatetime DESC LIMIT 1)
                        WHERE CFT.companyid =:companyid  AND CFT.isactive=1 AND CFT.is_deleted=0 AND CAST(CFTD.taxdate AS DATE)<=:todaysdate ";
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$strSql .= " AND CFT.locationid = :locationid ";
			}

			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$strSql .= " AND CFT.storeid = :storeid ";
			}

			$strSql .=" ORDER BY CFT.taxunkid";
			$dao->initCommand($strSql);
			$dao->addParameter(':todaysdate',$todaysdate);
			$dao->addParameter(':companyid',CONFIG_CID);
			if(defined('CONFIG_LID') && CONFIG_LID > 0)
			{
				$dao->addparameter(':locationid',CONFIG_LID);
			}

			if(defined('CONFIG_SID') && CONFIG_SID > 0)
			{
				$dao->addparameter(':storeid',CONFIG_SID);
			}
			$res = $dao->executeQuery();
			return $res;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module.' - getAppliedTax - '.$e);
		}
	}
    public function printGrn($id,$defaultlanguageArr='')
    {
        try {
            $this->log->logIt($this->module . ' - printGrn');
            $dao = new \dao();

            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $resarr = array();

            $strSql = "SELECT C.companyname,C.address1 AS company_address,C.phone AS company_phone,
                        C.currency_sign,C.companyemail,C.fax,IFNULL(C.logo,'') as company_logo,
                        TC.name AS vendor_name,TC.address AS vendor_address,TC.mobile AS vendor_mobile,TC.business_name AS Vendor_business,
                        TC.fax AS Vendor_fax,TC.email AS Vendor_email,
                        GR.grn_doc_num AS grn_voucher_num, GR.remarks,GR.voucher_no AS voucher_num,
                         IFNULL(DATE_FORMAT(GR.grn_date,'" . $mysql_format . "'),'')  AS Voucher_Date, 
                        ROUND(IFNULL(GR.totalamount,0),$round_off) as totalamount,
                        VC.countryName AS Company_Country                        
                        FROM " . CONFIG_DBN . ".cfgrn AS GR                         
                        LEFT JOIN " . CONFIG_DBN . ".cfcompany AS C ON GR.companyid= C.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfvandor AS V ON GR.lnkvendorid=V.vandorunkid AND V.companyid=GR.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".trcontact AS TC ON V.lnkcontactid=TC.contactunkid AND TC.companyid=V.companyid
                        LEFT JOIN " . CONFIG_DBN . ".vwcountry AS VC ON C.country=VC.id                       
                        WHERE GR.companyid=:companyid AND GR.grnid=:grnid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':grnid', $id);
            $grrecords = $dao->executeRow();


            $resarr['company_name'] = $grrecords['companyname'];
            $resarr['address'] = $grrecords['company_address'];
            $resarr['phone'] = $grrecords['company_phone'];
            $resarr['email'] = $grrecords['companyemail'];
            $resarr['company_logo'] = !empty($grrecords['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$grrecords['company_logo']:'';
            $resarr['fax'] = $grrecords['fax'];
            $resarr['Company_Country'] = $grrecords['Company_Country'];
            $resarr['currency_sign'] = $grrecords['currency_sign'];
            $resarr['grn_voucher_num'] = $grrecords['grn_voucher_num'];
            $resarr['voucher_num'] = $grrecords['voucher_num'];
            $resarr['Vendor_business'] = $grrecords['Vendor_business'];
            $resarr['remarks'] = $grrecords['remarks'];
            $resarr['Voucher_Date'] = $grrecords['Voucher_Date'];
            $resarr['totalamount'] = $grrecords['totalamount'];
            $resarr['vendor_name'] = $grrecords['vendor_name'];
            $resarr['print_by'] = CONFIG_UNM;
            $resarr['vendor_mobile'] = $grrecords['vendor_mobile'];
            $resarr['vendor_email'] = $grrecords['Vendor_email'];
            $resarr['current_date'] = \util\util::convertMySqlDate($current_date);

            $strPODetail = "SELECT CR.name AS storeitem,CU.shortcode AS unit,CPD.lnkgrnid,CPD.lnkrawid,CPD.lnkrawcatid," .
                "CPD.lnkunitid,ROUND(CPD.qty,$round_off) AS qty,ROUND(CPD.rate,$round_off) AS rate," .
                "CPD.discount_per,ROUND(CPD.discount_amount,$round_off) AS discount_amount,ROUND(CPD.tax_amount,$round_off) AS tax_amount," .
                "CPD.tax_description,ROUND(CPD.total,$round_off) AS total FROM " . CONFIG_DBN . ".cfgrndetail AS CPD " .
                "LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CPD.lnkrawid=CR.id " .
                "LEFT JOIN " . CONFIG_DBN . ".cfunit CU ON CPD.lnkunitid=CU.unitunkid AND " .
                "CU.companyid=:companyid AND CU.is_active=1 AND CU.is_deleted=0 " .
                "WHERE CPD.lnkgrnid=:lnkgrnid AND CPD.companyid=:companyid AND CPD.storeid=:storeid";
            $dao->initCommand($strPODetail);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addparameter(':lnkgrnid', $id);
            $grdetail = $dao->executeQuery();

            $taxTOtal = 0;

            if($grdetail){

                foreach ($grdetail as $key => $value) {
                    $taxname = array();
                    $grtaxdetail = json_decode($value['tax_description'], 1);
                    $taxeslist_dis='';
                    if($grtaxdetail){
                        foreach ($grtaxdetail as $key2 => $value2) {
                            $Tax_NAME='';
                            $posting_type=isset($grrecords['currency_sign'])?$grrecords['currency_sign']:'Flat';
                            if(isset($value2['posting_rule']) && $value2['posting_rule']==1){
                                $posting_type='%';
                            }

                            if(empty($value2['tax_name']))
                            {
                                $str = "SELECT tax FROM " . CONFIG_DBN . ".cftax
                 WHERE  companyid=:companyid AND storeid=:storeid  AND taxunkid=:taxunkid";
                                $dao->initCommand($str);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addParameter(':storeid', CONFIG_SID);
                                $dao->addParameter(':taxunkid', $value2['tax_id']);
                                $qtaxname = $dao->executeRow();
                                $Tax_NAME=$qtaxname['tax'];
                            }
                            else{
                                $Tax_NAME=$value2['tax_name'];
                            }

                            $taxeslist_dis .= "<b>".$Tax_NAME."</b> : (".number_format($value2['tax_value'],$round_off,'.','')." ".$posting_type.") : ".number_format($value2['tax_amt'], $round_off,'.','')." <br>";
                            $taxname[$value2['tax_id']] = $value2['tax_amt'];
                        }
                    }
                    $taxTOtal = ($taxTOtal+$value['tax_amount']);
                    $grdetail[$key]['tax_description'] = isset($taxeslist_dis)?$taxeslist_dis:'';
                }
            }
            $resarr['grdetail'] = $grdetail;
            $resarr['Tax_Total'] = number_format($taxTOtal,$round_off,'.','');
            $Discounttotal = $Dto = 0;
            foreach ($grdetail AS $val) {

                $to = $val['discount_amount'];
                $Discounttotal += $to;
            }

            $resarr['Discounttotal'] = number_format($Discounttotal, $round_off,'.','');
            $numberinwords = \util\util::convertNumber($grrecords['totalamount'],'','','',$defaultlanguageArr);
            $resarr['numberinwords'] = $numberinwords;

            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - printGrn - ' . $e);
        }
    }
}


?>

