<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 13/4/18
 * Time: 3:45 PM
 */

namespace database;
class indentdao
{
    public $module = 'DB_indentdao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function receivedIndentList($limit, $offset, $fromDate, $toDate, $indentNo, $store, $status)
    {
        try {
            $this->log->logIt($this->module . ' - receivedIndentList');
            $ObjUtil = new \util\util;
            $from_date = $ObjUtil->convertDateToMySql($fromDate);
            $to_date = $ObjUtil->convertDateToMySql($toDate);
            $dao = new \dao;
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];

            $strSql = "SELECT CRI.*,CL.storename, IFNULL(DATE_FORMAT(CRI.indent_date,'" . $mysql_format . "'),'') AS indentdate,IFNULL(CFU1.username,'') AS createduser,
                       IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CRI.createddatetime,'" . $mysql_format . "'),'') AS created_date,
                       IFNULL(DATE_FORMAT(CRI.modifieddatetime,'" . $mysql_format . "'),'') AS modified_date
                       FROM " . CONFIG_DBN . ".cfrequestindent AS CRI
                       LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU1 ON CRI.createduser=CFU1.userunkid AND CRI.companyid=CFU1.companyid
                       LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU2 ON CRI.modifieduser=CFU2.userunkid AND CRI.companyid=CFU2.companyid
                       LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS CL ON CRI.storeid=CL.storeunkid AND CRI.companyid=CL.companyid
                       WHERE CRI.companyid=:companyid AND CRI.recstoreid=:storeid AND CRI.is_deleted=0";
            if ($indentNo != "") {
                $strSql .= " AND CRI.indent_doc_num LIKE '%" . $indentNo . "%'";
            }
            if ($store != 0) {
                $strSql .= " AND CRI.storeid = '" . $store . "'";
            }
            if ($status != 0) {
                $strSql .= " AND CRI.status LIKE '%" . $status . "%'";
            }
            if ($from_date != "" && $to_date != "") {
                $strSql .= " AND CRI.indent_date  BETWEEN  '" . $from_date . "' AND '" . $to_date . "' ";
            }
            $strSql .= " ORDER BY CRI.indentid DESC";
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqlLmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $resObj = $dao->executeQuery();

            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $dao->initCommand($strSql . $strSqlLmt);
            } else {
                $dao->initCommand($strSql);
            }
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $rec = $dao->executeQuery();
            if (count($resObj) != 0) {
                $ret_value = array(array("cnt" => count($resObj), "data" => $rec));
                return json_encode($ret_value);
            } else {
                $ret_value = array(array("cnt" => 0, "data" => []));
                return json_encode($ret_value);
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - receivedIndentList - ' . $e);
        }
    }

    public function getIssueIndentRecordsonID($indent_id)
    {
        try {
            $this->log->logIt($this->module . " - getIssueIndentRecordsonID");

            $dao = new \dao();
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $strSqltogetindentrecords = "SELECT CRI.*,CL.storename,
            IFNULL(DATE_FORMAT(CRI.indent_date,'" . $mysqlformat . "'),'') as indentformated_date
             FROM " . CONFIG_DBN . ".cfrequestindent AS CRI 
            LEFT JOIN  " . CONFIG_DBN . ".cfstoremaster AS CL ON CRI.storeid=CL.storeunkid
            WHERE CRI.indentid=:indentid and CRI.companyid=:companyid and CRI.recstoreid=:storeid ";
            $dao->initCommand($strSqltogetindentrecords);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addParameter(':indentid', $indent_id);
            $indent = $dao->executeRow();

            $strSqltogetindentdetail = "SELECT CR.name AS selectedrm,U.name AS selectedunit,U.unit as unit_val,CRI.* 
            FROM " . CONFIG_DBN . ".cfindentrequestdetail AS CRI 
            LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CRI.lnkrawid=CR.id 
            LEFT JOIN " . CONFIG_DBN . ".cfunit U ON CRI.lnkunitid=U.unitunkid AND
            U.companyid=:companyid AND U.is_active=1 and U.is_deleted=0 
            WHERE CRI.lnkindentid=:lnkindentid AND lnkissueitemid=0";

            $dao->initCommand($strSqltogetindentdetail);
            $dao->addParameter(':lnkindentid', $indent['indentid']);
            $dao->addParameter(':companyid', CONFIG_CID);
            $indentdetail = $dao->executeQuery();

            return json_encode(array("Success" => "True", "order" => $indent, "orderDetail" => $indentdetail));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getIssueIndentRecordsonID - " . $e);
            return false;
        }
    }

    public function authorizeReceivedIndent($data,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - authorizeReceivedIndent');

            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $strSql = "UPDATE " . CONFIG_DBN . ".cfrequestindent SET authstatus=1,authorized_user=:userid,authorize_datetime=:datetime WHERE indentid=:id AND recstoreid=:recstoreid AND companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':userid', CONFIG_UID);
            $dao->addParameter(':recstoreid', CONFIG_SID);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':id', $data['indent_id']);
            $dao->addParameter(':datetime', $datetime);
            $dao->executeNonQuery();
            return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->ITEM_AUT_SUC));
        } catch (\Exception $e) {
            $this->log->logIt($this->module . ' - authorizeReceivedIndent - ' . $e);
        }
    }

    public function addissueindent($data,$languageArr,$defaultlanguageArr)
    {
        try {
        	$this->log->logIt($this->module . ' - addissueindent - ');
            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $issuearray = $data['po_items'];
            $objutil = new \util\util;
            $issue_date = $objutil->convertDateToMySql($data['issue_date']);
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $ObjCommonDao = new \database\commondao();
            $issue_doc_num = $ObjCommonDao->getIndentDocumentNumbering('issue_voucher', 'inc');
			$dao->bindTransaction();
            if ($data['Indent_id'] == 0 || $data['Indent_id'] == '') {
                if ($data['ISSUEID'] == 0 || $data['ISSUEID'] == '') {
                    $sql = "INSERT into " . CONFIG_DBN . ".cfissuevoucher SET 
                            issue_doc_num=:issue_doc_num,
                            issue_date=:issue_date,
                            lnkindentid=:lnkindentid,
                            remarks=:remarks,
                            relstoreid=:relstoreid,
                            totalamount=:totalamount,
                            storeid=:storeid,             
                            companyid=:companyid,
                            createddatetime=:createddatetime,
                            createduser=:createduser";
                    $dao->initCommand($sql);
                    $dao->addParameter(':issue_date', $issue_date);
                    $dao->addParameter(':issue_doc_num', $issue_doc_num);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':lnkindentid', 0);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':remarks', $data['premarks']);
                    $dao->addParameter(':relstoreid', $data['pstoreid']);
                    $dao->addParameter(':createddatetime', $datetime);
                    $dao->addParameter(':createduser', CONFIG_UID);
                    $dao->addParameter(':totalamount', round($data['total_amount'], $round_off));
                    $dao->executeNonQuery();
                    $poId = $dao->getLastInsertedId();
                    foreach($issuearray AS $key => $value) {
                        if (is_array($value['item_tax_det']) && count($value['item_tax_det']) > 0) {
                            $tax_description = json_encode($value['item_tax_det']);
                        } else {
                            $tax_description = '';
                        }
                        $sql = "INSERT into " . CONFIG_DBN . ".cfissuevoucherdetail SET 
                            lnkissueid=:lnkissueid,
                            lnkindentid=:lnkindentid,
                            lnkindentdetailid=:lnkindentdetailid, 
                            lnkrawcatid=:lnkrawcatid,
                            lnkrawid=:lnkrawid,                            
                            lnkunitid=:lnkunitid,
                            qty=:qty,
                            rate=:rate,
                            discount_per=:discount_per,
                            discount_amount=:discount_amount,
                            tax_amount=:tax_amount,
                            tax_description=:tax_description,
                            total=:total,
                            companyid=:companyid";
                        $dao->initCommand($sql);
                        $dao->addParameter(':lnkissueid', $poId);
                        $dao->addParameter(':lnkindentid', 0);
                        $dao->addParameter(':lnkindentdetailid', 0);
                        $dao->addParameter(':lnkrawid', $value['item_id']);
                        $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                        $dao->addParameter(':lnkunitid', $value['item_unit']);
                        $dao->addParameter(':qty', $value['item_qty']);
                        $dao->addParameter(':rate', $value['item_rpu']);
                        $dao->addParameter(':discount_per', $value['item_disc_per']);
                        $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                        $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                        $dao->addParameter(':tax_description', $tax_description);
                        $dao->addParameter(':total', $value['item_total_amt']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->executeNonQuery();
                        $issueID = $dao->getLastInsertedId();

                        //Inventory
                        $amount = $value['item_total_amt'] + $value['item_disc_amt'] - $value['item_tax_amt'];
                        $inventory = $value['item_qty'] * $value['item_unit_val'];

                        //Decrease Inventory
                        $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit,inv_unitid FROM ".CONFIG_DBN.".cfrawmaterial_loc
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strrawLoc);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':item_id', $value['item_id']);
                        $descrestockitemRow =  $resrawLoc = $dao->executeRow();

                        $dao->initCommand("SELECT u.unitunkid FROM ".CONFIG_DBN.".cfrawmaterial CFR 
					INNER JOIN ".CONFIG_DBN.".cfunit u ON u.measuretype = CFR.measuretype AND u.is_systemdefined=1 AND u.companyid=:companyid
					 WHERE CFR.id = :id AND CFR.companyid=:companyid ");
                        $dao->addParameter(':id',$value['item_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $rawMaterialRec = $dao->executeRow();
                        if(!$resrawLoc)
                        {
                            $insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) 
                            VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
                            $dao->initCommand($insertInventorySql);
                            $dao->addParameter(':lnkrawmaterialid',$value['item_id']);
                            $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                            $dao->addParameter(':storeid',CONFIG_SID);
                            $dao->addParameter(':companyid',CONFIG_CID);
                            $dao->executeNonQuery();
                            $resrawLoc = [];
                            $resrawLoc['id'] = $dao->getLastInsertedId();
                            $resrawLoc['rateperunit'] = 0;
                            $resrawLoc['inventory'] = 0;
                        }


                        
                        $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,rel_storeid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:rel_storeid)";
                        $dao->initCommand($strinventory);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':rawlocid', $resrawLoc['id']);
                        $dao->addParameter(':inventory', $inventory);
                        $dao->addParameter(':rate', $amount);
                        $dao->addParameter(':total_rate', $value['item_total_amt']);
                        $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                        $dao->addParameter(':ref_id',$issueID);
                        $dao->addParameter(':invstatus',INV_STATUS_IV);
                        $dao->addParameter(':crdb','db');
                        $dao->addParameter(':createddatetime',$datetime);
                        $dao->addParameter(':created_user',CONFIG_UID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->addparameter(':rel_storeid', $data['pstoreid']);
                        $dao->executeNonQuery();

                        $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                        $total_inventory = $resrawLoc['inventory'] - $inventory ;
                        $totalPrice = $old_price - $value['item_total_amt'];
                        $rateperunit = $totalPrice / $total_inventory;
                        $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                        $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                                                WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                                                and storeid=:storeid";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':inventory', $total_inventory);
                        $dao->addParameter(':rateperunit', $rateperunit);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->executeNonQuery();

                        //Increase Inventory
                        $strrawLoc2 = "SELECT lnkrawmaterialid,id,inventory,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc 
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strrawLoc2);
                        $dao->addParameter(':storeid', $data['pstoreid']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':item_id', $value['item_id']);
                        $resrawLoc2 = $dao->executeRow();
	
						/* Date : 24-04-2018
					 * If inventory item not exists in reciving store then first need to add that item into reciving store Line no 550 to 63 !$resrawLoc condition
					 * */
						if(!$resrawLoc2) {
							$insertInventorySql = " INSERT INTO  " . CONFIG_DBN . ".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
							$dao->initCommand($insertInventorySql);
							$dao->addParameter(':lnkrawmaterialid', $value['item_id']);
							$dao->addParameter(':inv_unitid', $rawMaterialRec['unitunkid']);
							$dao->addParameter(':storeid', $data['pstoreid']);
							$dao->addParameter(':companyid', CONFIG_CID);
							$dao->executeNonQuery();
							$resrawLoc2 = [];
							$resrawLoc2['id'] = $dao->getLastInsertedId();
							$resrawLoc2['rateperunit'] = 0;
							$resrawLoc2['inventory'] = 0;
						}

                        $strinventory2 = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,rel_storeid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:rel_storeid)";
                        $dao->initCommand($strinventory2);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':rawlocid', $resrawLoc2['id']);
                        $dao->addParameter(':inventory', $inventory);
                        $dao->addParameter(':rate', $amount);
                        $dao->addParameter(':total_rate', $value['item_total_amt']);
                        $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                        $dao->addParameter(':ref_id',$issueID);
                        $dao->addParameter(':invstatus',INV_STATUS_IV);
                        $dao->addParameter(':crdb','cr');
                        $dao->addParameter(':createddatetime',$datetime);
                        $dao->addParameter(':created_user',CONFIG_UID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', $data['pstoreid']);
                        $dao->addparameter(':rel_storeid',CONFIG_SID);
                        $dao->executeNonQuery();

                        $old_price = $resrawLoc2['rateperunit'] * $resrawLoc2['inventory'];
                        $total_inventory = $resrawLoc2['inventory'] + $inventory ;
                        $totalPrice = $old_price + $value['item_total_amt'];
                        $rateperunit = $totalPrice / $total_inventory;
                        $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                        $strSql2 = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                    WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                    and storeid=:storeid";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(':inventory', $total_inventory);
                        $dao->addParameter(':rateperunit', $rateperunit);
                        $dao->addParameter(':itemid', $value['item_id']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', $data['pstoreid']);
                        $dao->executeNonQuery();
                        //Inventory//
                    }
                    $dao->releaseTransaction(TRUE);
                    return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_ADD_SUC));
                }
                else {
                    $strforcheckkey = "SELECT issuerequnkid  FROM " . CONFIG_DBN . ".cfissuevoucherdetail 
                                        WHERE lnkissueid=:lnkissueid AND companyid=:companyid  AND lnkindentid=0 AND lnkindentdetailid=0";
                    $dao->initCommand($strforcheckkey);
                    $dao->addParameter(':lnkissueid', $data['ISSUEID']);
                    $dao->addParameter(':companyid', CONFIG_CID);

                    $resultforidcheck = $dao->executeQuery();

                    $orderdetailunkid = \util\util::getoneDarray($resultforidcheck, 'issuerequnkid');

                    $poarr = [];
                    foreach ($issuearray AS $key => $value) {
                        if (is_array($value['item_tax_det']) && count($value['item_tax_det']) > 0) {
                            $tax_description = json_encode($value['item_tax_det']);
                        } else {
                            $tax_description = '';
                        }
                        if (in_array($value['itm_detId'], $orderdetailunkid)) {
                            $strsql = "UPDATE " . CONFIG_DBN . ".cfissuevoucherdetail SET 
                            lnkrawid=:lnkrawid,lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,
                            qty=:qty,rate=:rate,discount_per=:discount_per,discount_amount=:discount_amount,tax_amount=:tax_amount,
                            tax_description=:tax_description,total=:total
                            WHERE issuerequnkid=:issuerequnkid AND lnkissueid=:lnkissueid AND companyid=:companyid ";
                            $dao->initCommand($strsql);
                            $dao->addParameter(':issuerequnkid', $value['itm_detId']);
                            $dao->addParameter(':lnkissueid', $data['ISSUEID']);
                            $dao->addParameter(':lnkrawid', $value['item_id']);
                            $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                            $dao->addParameter(':lnkunitid', $value['item_unit']);
                            $dao->addParameter(':qty', $value['item_qty']);
                            $dao->addParameter(':rate', $value['item_rpu']);
                            $dao->addParameter(':discount_per', $value['item_disc_per']);
                            $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                            $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                            $dao->addParameter(':tax_description', $tax_description);
                            $dao->addParameter(':total', $value['item_total_amt']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->executeNonQuery();
                            $poarr[] = $value['itm_detId'];
                            $lnkissuedetailid = $dao->getLastInsertedId();

                            //Inventory
                            $amount = $value['item_total_amt'] + $value['item_disc_amt'] - $value['item_tax_amt'];

                            $dao->initCommand("SELECT unit FROM ".CONFIG_DBN.".cfunit WHERE unitunkid = :unitunkid ");
                            $dao->addParameter(':unitunkid',$value['item_unit']);
                            $unitRecord = $dao->executeRow();
                            $inventory = $value['item_qty'] * $unitRecord['unit'];

                            //Decrease Inventory
                            $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit,inv_unitid FROM ".CONFIG_DBN.".cfrawmaterial_loc
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                            $dao->initCommand($strrawLoc);
                            $dao->addParameter(':storeid', CONFIG_SID);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addParameter(':item_id', $value['item_id']);
                            $decreseitemStockrow =  $resrawLoc = $dao->executeRow();

                            $dao->initCommand("SELECT u.unitunkid FROM ".CONFIG_DBN.".cfrawmaterial CFR 
					INNER JOIN ".CONFIG_DBN.".cfunit u ON u.measuretype = CFR.measuretype AND u.is_systemdefined=1 AND u.companyid=:companyid
					 WHERE CFR.id = :id AND CFR.companyid=:companyid ");
                            $dao->addParameter(':id',$value['item_id']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $rawMaterialRec = $dao->executeRow();
                            if(!$resrawLoc)
                            {
                                $insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
                                $dao->initCommand($insertInventorySql);
                                $dao->addParameter(':lnkrawmaterialid',$value['item_id']);
                                $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                                $dao->addParameter(':storeid',CONFIG_SID);
                                $dao->addParameter(':companyid',CONFIG_CID);
                                $dao->executeNonQuery();
                                $resrawLoc = [];
                                $resrawLoc['id'] = $dao->getLastInsertedId();
                                $resrawLoc['rateperunit'] = 0;
                                $resrawLoc['inventory'] = 0;
                            }

                            $strInvDetail = "SELECT inventory,total_rate FROM ".CONFIG_DBN.".cfinventorydetail 
                                      WHERE lnkrawmaterialid=:itemid AND ref_id=:ref_id AND inv_status=:inv_status
                                      AND companyid=:companyid AND storeid=:storeid";
                            $dao->initCommand($strInvDetail);
                            $dao->addParameter(':itemid', $value['item_id']);
                            $dao->addParameter(':ref_id', $value['itm_detId']);
                            $dao->addParameter(':inv_status', INV_STATUS_IV);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':storeid', CONFIG_SID);
                            $resInvDetail = $dao->executeRow();

                            $strInv = "UPDATE " . CONFIG_DBN . ".cfinventorydetail 
                                   SET inventory=:inventory,rate=:rate,total_rate=:total_rate,inv_unitid=:inv_unitid                      
                                   WHERE lnkrawmaterialid=:itemid AND ref_id=:ref_id AND inv_status=:inv_status
                                   AND companyid=:companyid AND storeid=:storeid";
                            $dao->initCommand($strInv);
                            $dao->addParameter(':inventory', $inventory);
                            $dao->addParameter(':rate', $amount);
                            $dao->addParameter(':total_rate', $value['item_total_amt']);
                            $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                            $dao->addParameter(':itemid', $value['item_id']);
                            $dao->addParameter(':ref_id', $value['itm_detId']);
                            $dao->addParameter(':inv_status', INV_STATUS_IV);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':storeid', CONFIG_SID);
                            $dao->executeNonQuery();

                            if($resInvDetail['inventory'] > $inventory){
                                $newDetail_Inv= $resInvDetail['inventory'] - $inventory;
                                $total_inventory = $resrawLoc['inventory'] + $newDetail_Inv ;
                            }
                            else{
                                $newDetail_Inv= $inventory - $resInvDetail['inventory'];
                                $total_inventory = $resrawLoc['inventory'] - $newDetail_Inv ;
                            }
                            $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                            if($resInvDetail['total_rate'] > $value['item_total_amt']){
                                $newDetail_Rate= $resInvDetail['total_rate'] - $value['item_total_amt'];
                                $totalPrice = $old_price - $newDetail_Rate;
                            }
                            else{
                                $newDetail_Rate= $value['item_total_amt'] - $resInvDetail['total_rate'];
                                $totalPrice = $old_price + $newDetail_Rate;
                            }
                            $rateperunit = $totalPrice / $total_inventory;
                            $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                            $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                                                WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                                                and storeid=:storeid";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':inventory', $total_inventory);
                            $dao->addParameter(':rateperunit', $rateperunit);
                            $dao->addParameter(':itemid', $value['item_id']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':storeid', CONFIG_SID);
                            $dao->executeNonQuery();

                            //Increase Inventory
                            $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc 
                                          WHERE lnkrawmaterialid=:item_id
                                          AND companyid=:companyid AND storeid=:storeid";
                            $dao->initCommand($strrawLoc);
                            $dao->addParameter(':storeid', $data['pstoreid']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addParameter(':item_id', $value['item_id']);
                            $resrawLoc = $dao->executeRow();
                            if(!$resrawLoc)
                            {
                                $insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
                                $dao->initCommand($insertInventorySql);
                                $dao->addParameter(':lnkrawmaterialid',$value['item_id']);
                                $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                                $dao->addParameter(':storeid',$data['pstoreid']);
                                $dao->addParameter(':companyid',CONFIG_CID);
                                $dao->executeNonQuery();
                                $resrawLoc = [];
                                $resrawLoc['id'] = $dao->getLastInsertedId();
                                $resrawLoc['rateperunit'] = 0;
                                $resrawLoc['inventory'] = 0;
                            }

                            $strInvDetail = "SELECT inventory,total_rate FROM ".CONFIG_DBN.".cfinventorydetail 
                                      WHERE lnkrawmaterialid=:itemid AND ref_id=:ref_id AND inv_status=:inv_status
                                      AND companyid=:companyid AND storeid=:storeid";
                            $dao->initCommand($strInvDetail);
                            $dao->addParameter(':itemid', $value['item_id']);
                            $dao->addParameter(':ref_id', $value['itm_detId']);
                            $dao->addParameter(':inv_status', INV_STATUS_IV);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':storeid', $data['pstoreid']);
                            $resInvDetail = $dao->executeRow();

                            $strInv = "UPDATE " . CONFIG_DBN . ".cfinventorydetail 
                                   SET inventory=:inventory,rate=:rate,total_rate=:total_rate,inv_unitid=:inv_unitid                      
                                   WHERE lnkrawmaterialid=:itemid AND ref_id=:ref_id AND inv_status=:inv_status
                                   AND companyid=:companyid AND storeid=:storeid";
                            $dao->initCommand($strInv);
                            $dao->addParameter(':inventory', $inventory);
                            $dao->addParameter(':rate', $amount);
                            $dao->addParameter(':total_rate', $value['item_total_amt']);
                            $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                            $dao->addParameter(':itemid', $value['item_id']);
                            $dao->addParameter(':ref_id', $value['itm_detId']);
                            $dao->addParameter(':inv_status', INV_STATUS_IV);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':storeid', $data['pstoreid']);
                            $dao->executeNonQuery();

                            if($resInvDetail['inventory'] > $inventory){
                                $newDetail_Inv= $resInvDetail['inventory'] - $inventory;
                                $total_inventory = $resrawLoc['inventory'] - $newDetail_Inv ;
                            }
                            else{
                                $newDetail_Inv= $inventory - $resInvDetail['inventory'];
                                $total_inventory = $resrawLoc['inventory'] + $newDetail_Inv ;
                            }
                            $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                            if($resInvDetail['total_rate'] > $value['item_total_amt']){
                                $newDetail_Rate= $resInvDetail['total_rate'] - $value['item_total_amt'];
                                $totalPrice = $old_price - $newDetail_Rate;
                            }
                            else{
                                $newDetail_Rate= $value['item_total_amt'] - $resInvDetail['total_rate'];
                                $totalPrice = $old_price + $newDetail_Rate;
                            }
                            $rateperunit = $totalPrice / $total_inventory;
                            $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                            $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                    WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                    and storeid=:storeid";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':inventory', $total_inventory);
                            $dao->addParameter(':rateperunit', $rateperunit);
                            $dao->addParameter(':itemid', $value['item_id']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':storeid', $data['pstoreid']);
                            $dao->executeNonQuery();
                            //Inventory//

                        } else {
                            $sql = "INSERT into " . CONFIG_DBN . ".cfissuevoucherdetail SET 
                            lnkissueid=:lnkissueid,lnkrawid=:lnkrawid,
                            lnkrawcatid=:lnkrawcatid,lnkunitid=:lnkunitid,
                            qty=:qty,rate=:rate,discount_per=:discount_per,discount_amount=:discount_amount,tax_amount=:tax_amount,
                            tax_description=:tax_description,total=:total,companyid=:companyid";
                            $dao->initCommand($sql);
                            $dao->addParameter(':lnkissueid', $data['ISSUEID']);
                            $dao->addParameter(':lnkrawid', $value['item_id']);
                            $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                            $dao->addParameter(':lnkunitid', $value['item_unit']);
                            $dao->addParameter(':qty', $value['item_qty']);
                            $dao->addParameter(':rate', $value['item_rpu']);
                            $dao->addParameter(':discount_per', $value['item_disc_per']);
                            $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                            $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                            $dao->addParameter(':tax_description', $tax_description);
                            $dao->addParameter(':total', $value['item_total_amt']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->executeNonQuery();
                            $poarr[] = $dao->getLastInsertedId();
                            $lnkissuedetailid = $dao->getLastInsertedId();

                            //Inventory
                            $amount = $value['item_total_amt'] + $value['item_disc_amt'] - $value['item_tax_amt'];

                            $dao->initCommand("SELECT unit FROM ".CONFIG_DBN.".cfunit WHERE unitunkid = :unitunkid ");
                            $dao->addParameter(':unitunkid',$value['item_unit']);
                            $unitRecord = $dao->executeRow();
                            $inventory = $value['item_qty'] * $unitRecord['unit'];

                            //Decrease Inventory
                            $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit,inv_unitid FROM ".CONFIG_DBN.".cfrawmaterial_loc
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                            $dao->initCommand($strrawLoc);
                            $dao->addParameter(':storeid', CONFIG_SID);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addParameter(':item_id', $value['item_id']);
                            $decreseitemStockrow =  $resrawLoc = $dao->executeRow();

                            $dao->initCommand("SELECT u.unitunkid FROM ".CONFIG_DBN.".cfrawmaterial CFR 
					INNER JOIN ".CONFIG_DBN.".cfunit u ON u.measuretype = CFR.measuretype AND u.is_systemdefined=1 AND u.companyid=:companyid
					 WHERE CFR.id = :id AND CFR.companyid=:companyid ");
                            $dao->addParameter(':id',$value['item_id']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $rawMaterialRec = $dao->executeRow();
                            if(!$resrawLoc)
                            {
                                $insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
                                $dao->initCommand($insertInventorySql);
                                $dao->addParameter(':lnkrawmaterialid',$value['item_id']);
                                $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                                $dao->addParameter(':storeid',CONFIG_SID);
                                $dao->addParameter(':companyid',CONFIG_CID);
                                $dao->executeNonQuery();
                                $resrawLoc = [];
                                $resrawLoc['id'] = $dao->getLastInsertedId();
                                $resrawLoc['rateperunit'] = 0;
                                $resrawLoc['inventory'] = 0;
                            }

                            $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,rel_storeid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:rel_storeid)";
                            $dao->initCommand($strinventory);
                            $dao->addParameter(':itemid', $value['item_id']);
                            $dao->addParameter(':rawlocid', $resrawLoc['id']);
                            $dao->addParameter(':inventory', $inventory);
                            $dao->addParameter(':rate', $amount);
                            $dao->addParameter(':total_rate', $value['item_total_amt']);
                            $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                            $dao->addParameter(':ref_id',$lnkissuedetailid);
                            $dao->addParameter(':invstatus',INV_STATUS_IV);
                            $dao->addParameter(':crdb','db');
                            $dao->addParameter(':createddatetime',$datetime);
                            $dao->addParameter(':created_user',CONFIG_UID);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':storeid', CONFIG_SID);
                            $dao->addparameter(':rel_storeid', $data['pstoreid']);
                            $dao->executeNonQuery();

                            $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                            $total_inventory = $resrawLoc['inventory'] - $inventory ;
                            $totalPrice = $old_price - $value['item_total_amt'];
                            $rateperunit = $totalPrice / $total_inventory;
                            $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;
                            $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                                                WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                                                and storeid=:storeid";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':inventory', $total_inventory);
                            $dao->addParameter(':rateperunit', $rateperunit);
                            $dao->addParameter(':itemid', $value['item_id']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':storeid', CONFIG_SID);
                            $dao->executeNonQuery();

                            //Increase Inventory
                            $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc 
                                          WHERE lnkrawmaterialid=:item_id
                                          AND companyid=:companyid AND storeid=:storeid";
                            $dao->initCommand($strrawLoc);
                            $dao->addParameter(':storeid', $data['pstoreid']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addParameter(':item_id', $value['item_id']);
                            $resrawLoc = $dao->executeRow();
                            if(!$resrawLoc)
                            {
                                $insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
                                $dao->initCommand($insertInventorySql);
                                $dao->addParameter(':lnkrawmaterialid',$value['item_id']);
                                $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                                $dao->addParameter(':storeid',$data['pstoreid']);
                                $dao->addParameter(':companyid',CONFIG_CID);
                                $dao->executeNonQuery();
                                $resrawLoc = [];
                                $resrawLoc['id'] = $dao->getLastInsertedId();
                                $resrawLoc['rateperunit'] = 0;
                                $resrawLoc['inventory'] = 0;
                            }

                            $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,rel_storeid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:rel_storeid)";
                            $dao->initCommand($strinventory);
                            $dao->addParameter(':itemid', $value['item_id']);
                            $dao->addParameter(':rawlocid', $resrawLoc['id']);
                            $dao->addParameter(':inventory', $inventory);
                            $dao->addParameter(':rate', $amount);
                            $dao->addParameter(':total_rate', $value['item_total_amt']);
                            $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                            $dao->addParameter(':ref_id',$lnkissuedetailid);
                            $dao->addParameter(':invstatus',INV_STATUS_IV);
                            $dao->addParameter(':crdb','cr');
                            $dao->addParameter(':createddatetime',$datetime);
                            $dao->addParameter(':created_user',CONFIG_UID);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':storeid', $data['pstoreid']);
                            $dao->addparameter(':rel_storeid',CONFIG_SID);
                            $dao->executeNonQuery();

                            $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                            $total_inventory = $resrawLoc['inventory'] + $inventory ;
                            $totalPrice = $old_price + $value['item_total_amt'];
                            $rateperunit = $totalPrice / $total_inventory;
                            $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                            $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                    WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                    and storeid=:storeid";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':inventory', $total_inventory);
                            $dao->addParameter(':rateperunit', $rateperunit);
                            $dao->addParameter(':itemid', $value['item_id']);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':storeid', $data['pstoreid']);
                            $dao->executeNonQuery();
                            //Inventory//
                        }
                    }
                    //Update inventory while delete
                    $diffarr= array_diff($orderdetailunkid,$poarr);
                    foreach($diffarr as $dkey=>$dval) {
                        $strInv = "SELECT lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,rel_storeid
                                      FROM " . CONFIG_DBN . ".cfinventorydetail 
                                      WHERE ref_id=:ref_id AND inv_status=:inv_status
                                      AND companyid=:companyid AND storeid=:storeid limit 1";
                        $dao->initCommand($strInv);
                        $dao->addParameter(':ref_id', $dval);
                        $dao->addParameter(':inv_status', INV_STATUS_IV);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $resINV = $dao->executeRow();

                        //Increase Inventory
                        $strrawLoc2 = "SELECT lnkrawmaterialid,id,inventory,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc 
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strrawLoc2);
                        $dao->addParameter(':storeid', CONFIG_SID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':item_id', $resINV['lnkrawmaterialid']);
                        $resrawLoc2 = $dao->executeRow();

                        $strinventory2 = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,rel_storeid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:rel_storeid)";
                        $dao->initCommand($strinventory2);
                        $dao->addParameter(':itemid', $resINV['lnkrawmaterialid']);
                        $dao->addParameter(':rawlocid',  $resrawLoc2['id']);
                        $dao->addParameter(':inventory', $resINV['inventory']);
                        $dao->addParameter(':rate', $resINV['rate']);
                        $dao->addParameter(':total_rate', $resINV['total_rate']);
                        $dao->addParameter(':inv_unitid',$resINV['inv_unitid']);
                        $dao->addParameter(':ref_id',$resINV['ref_id']);
                        $dao->addParameter(':invstatus',INV_STATUS_IV);
                        $dao->addParameter(':crdb','cr');
                        $dao->addParameter(':createddatetime',$datetime);
                        $dao->addParameter(':created_user',CONFIG_UID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->addparameter(':rel_storeid',$resINV['rel_storeid']);
                        $dao->executeNonQuery();

                        $old_price = $resrawLoc2['rateperunit'] * $resrawLoc2['inventory'];
                        $total_inventory = $resrawLoc2['inventory'] + $resINV['inventory'] ;
                        $totalPrice = $old_price + $resINV['total_rate'];
                        $rateperunit = $totalPrice / $total_inventory;
                        $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                        $strSql2 = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                    WHERE lnkrawmaterialid=:item_id AND companyid=:companyid
                                    and storeid=:storeid";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(':inventory', $total_inventory);
                        $dao->addParameter(':rateperunit', $rateperunit);
                        $dao->addParameter(':item_id', $resINV['lnkrawmaterialid']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid', CONFIG_SID);
                        $dao->executeNonQuery();
                        //

                        //Decrease Inventory

                        $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit,inv_unitid FROM ".CONFIG_DBN.".cfrawmaterial_loc
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                        $dao->initCommand($strrawLoc);
                        $dao->addParameter(':storeid', $resINV['rel_storeid']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addParameter(':item_id', $resINV['lnkrawmaterialid']);
                        $resrawLoc = $dao->executeRow();

                        $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,rel_storeid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:rel_storeid)";
                        $dao->initCommand($strinventory);
                        $dao->addParameter(':itemid', $resINV['lnkrawmaterialid']);
                        $dao->addParameter(':rawlocid', $resrawLoc['id']);
                        $dao->addParameter(':inventory', $resINV['inventory']);
                        $dao->addParameter(':rate', $resINV['rate']);
                        $dao->addParameter(':total_rate', $resINV['total_rate']);
                        $dao->addParameter(':inv_unitid',$resINV['inv_unitid']);
                        $dao->addParameter(':ref_id',$resINV['ref_id']);
                        $dao->addParameter(':invstatus',INV_STATUS_IV);
                        $dao->addParameter(':crdb','db');
                        $dao->addParameter(':createddatetime',$datetime);
                        $dao->addParameter(':created_user',CONFIG_UID);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid',  $resINV['rel_storeid']);
                        $dao->addparameter(':rel_storeid', CONFIG_SID);
                        $dao->executeNonQuery();

                        $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                        $total_inventory = $resrawLoc['inventory'] - $resINV['inventory'] ;
                        $totalPrice = $old_price - $resINV['total_rate'];
                        $rateperunit = $totalPrice / $total_inventory;
                        $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                        $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                                                WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                                                and storeid=:storeid";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':inventory', $total_inventory);
                        $dao->addParameter(':rateperunit', $rateperunit);
                        $dao->addParameter(':itemid', $resINV['lnkrawmaterialid']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':storeid',  $resINV['rel_storeid']);
                        $dao->executeNonQuery();

                    }
                    //
                    $strsql = "DELETE FROM " . CONFIG_DBN . ".cfissuevoucherdetail              
                       WHERE issuerequnkid NOT IN ('" . implode("','", $poarr) . "') 
                       AND lnkissueid=:lnkissueid AND companyid=:companyid ";
                    $dao->initCommand($strsql);
                    $dao->addParameter(':lnkissueid', $data['ISSUEID']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->executeNonQuery();

                    $strforcheckkey = "Update  ".CONFIG_DBN.".cfissuevoucher SET 
                    remarks=:remarks,
                    totalamount=:totalamount,
                    modifieduser=:modifieduser,
                    modifieddatetime=:modifieddatetime
                    WHERE lnkindentid=:lnkindentid AND companyid=:companyid AND issueid=:issueid 
                    AND storeid=:storeid AND relstoreid=:relstoreid ";
                    $dao->initCommand($strforcheckkey);
                    $dao->addParameter(':lnkindentid', 0);
                    $dao->addParameter(':remarks', $data['premarks']);
                    $dao->addParameter(':issueid', $data['ISSUEID']);
                    $dao->addParameter(':totalamount', $data['total_amount']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':modifieddatetime', $datetime);
                    $dao->addParameter(':modifieduser', CONFIG_UID);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':relstoreid', $data['pstoreid']);
                    $dao->executeNonQuery();

                    $dao->releaseTransaction(TRUE);
                    return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_UP_SUC));
                }
            }
            else {

                $sql = "INSERT into " . CONFIG_DBN . ".cfissuevoucher SET 
                             issue_doc_num=:issue_doc_num,
                issue_date=:issue_date,
                lnkindentid=:lnkindentid,
                remarks=:remarks,
                relstoreid=:relstoreid,
                totalamount=:totalamount,
                storeid=:storeid,             
                companyid=:companyid,
                createddatetime=:createddatetime,
                createduser=:createduser";
                $dao->initCommand($sql);
                $dao->addParameter(':issue_date', $issue_date);
                $dao->addParameter(':issue_doc_num', $issue_doc_num);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addParameter(':lnkindentid', $data['Indent_id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':remarks', $data['premarks']);
                $dao->addParameter(':relstoreid', $data['pstoreid']);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->addParameter(':createduser', CONFIG_UID);
                $dao->addParameter(':totalamount', round($data['total_amount'], $round_off));
                $dao->executeNonQuery();
                $poId = $dao->getLastInsertedId();
                foreach ($issuearray AS $key => $value) {
                    if (is_array($value['item_tax_det']) && count($value['item_tax_det']) > 0) {
                        $tax_description = json_encode($value['item_tax_det']);
                    } else {
                        $tax_description = '';
                    }
                    $sql = "INSERT into " . CONFIG_DBN . ".cfissuevoucherdetail SET 
                            lnkissueid=:lnkissueid,
                            lnkindentid=:lnkindentid,
                            lnkindentdetailid=:lnkindentdetailid, 
                            lnkrawcatid=:lnkrawcatid,
                            lnkrawid=:lnkrawid,                            
                            lnkunitid=:lnkunitid,
                            qty=:qty,
                            rate=:rate,
                            discount_per=:discount_per,
                            discount_amount=:discount_amount,
                            tax_amount=:tax_amount,
                            tax_description=:tax_description,
                            total=:total,                            
                            companyid=:companyid";
                    $dao->initCommand($sql);
                    $dao->addParameter(':lnkissueid', $poId);
                    $dao->addParameter(':lnkindentid', $data['Indent_id']);
                    $dao->addParameter(':lnkindentdetailid', $value['itm_detId']);
                    $dao->addParameter(':lnkrawid', $value['item_id']);
                    $dao->addParameter(':lnkrawcatid', $value['itm_category']);
                    $dao->addParameter(':lnkunitid', $value['item_unit']);
                    $dao->addParameter(':qty', $value['item_qty']);
                    $dao->addParameter(':rate', $value['item_rpu']);
                    $dao->addParameter(':discount_per', $value['item_disc_per']);
                    $dao->addParameter(':discount_amount', $value['item_disc_amt']);
                    $dao->addParameter(':tax_amount', $value['item_tax_amt']);
                    $dao->addParameter(':tax_description', $tax_description);
                    $dao->addParameter(':total', $value['item_total_amt']);

                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->executeNonQuery();
                    $lnkissuedetailid = $dao->getLastInsertedId();

                    //Inventory
                    $amount = $value['item_total_amt'] + $value['item_disc_amt'] - $value['item_tax_amt'];
                    
                    $dao->initCommand("SELECT * FROM ".CONFIG_DBN.".cfunit WHERE unitunkid = :unitunkid ");
                    $dao->addParameter(':unitunkid',$value['item_unit']);
                    $unitRecord = $dao->executeRow();
                    
                    $inventory = $value['item_qty'] * $unitRecord['unit'];

                    //Decrease Inventory
                    $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit,inv_unitid FROM ".CONFIG_DBN.".cfrawmaterial_loc
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                    $dao->initCommand($strrawLoc);
                    $dao->addParameter(':storeid', CONFIG_SID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':item_id', $value['item_id']);
                    $decreseitemStockrow =  $resrawLoc = $dao->executeRow();


					$dao->initCommand("SELECT u.unitunkid FROM ".CONFIG_DBN.".cfrawmaterial CFR 
					INNER JOIN ".CONFIG_DBN.".cfunit u ON u.measuretype = CFR.measuretype AND u.is_systemdefined=1 AND u.companyid=:companyid
					 WHERE CFR.id = :id AND CFR.companyid=:companyid ");
					$dao->addParameter(':id',$value['item_id']);
                    $dao->addParameter(':companyid', CONFIG_CID);
					$rawMaterialRec = $dao->executeRow();

					if(!$resrawLoc)
					{
						$insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
						$dao->initCommand($insertInventorySql);
						$dao->addParameter(':lnkrawmaterialid',$value['item_id']);
						$dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
						$dao->addParameter(':storeid',CONFIG_SID);
						$dao->addParameter(':companyid',CONFIG_CID);
						$dao->executeNonQuery();
						$resrawLoc = [];
						$resrawLoc['id'] = $dao->getLastInsertedId();
						$resrawLoc['rateperunit'] = 0;
						$resrawLoc['inventory'] = 0;
					}
					
				
					
                    $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,rel_storeid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:rel_storeid)";
                    $dao->initCommand($strinventory);
                    $dao->addParameter(':itemid', $value['item_id']);
                    $dao->addParameter(':rawlocid', $resrawLoc['id']);
                    $dao->addParameter(':inventory', $inventory);
                    $dao->addParameter(':rate', $amount);
                    $dao->addParameter(':total_rate', $value['item_total_amt']);
                    /*
                    	set default base unit as we convert inventory in base unit
                    	$dao->addParameter(':inv_unitid',$value['item_unit']);
                     *
                     * */
                    $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                    $dao->addParameter(':ref_id',$lnkissuedetailid);
                    $dao->addParameter(':invstatus',INV_STATUS_IV);
                    $dao->addParameter(':crdb','db');
                    $dao->addParameter(':createddatetime',$datetime);
                    $dao->addParameter(':created_user',CONFIG_UID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid', CONFIG_SID);
                    $dao->addparameter(':rel_storeid', $data['pstoreid']);
                    $dao->executeNonQuery();

                    $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                    $total_inventory = $resrawLoc['inventory'] - $inventory ;
                    $totalPrice = $old_price - $value['item_total_amt'];
                    $rateperunit = $totalPrice / $total_inventory;
					$rateperunit = ($rateperunit > 0) ? $rateperunit : 0;
                    $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                                                WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                                                and storeid=:storeid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':inventory', $total_inventory);
                    $dao->addParameter(':rateperunit', $rateperunit);
                    $dao->addParameter(':itemid', $value['item_id']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid', CONFIG_SID);
                    $dao->executeNonQuery();

                    //Increase Inventory
                    $strrawLoc = "SELECT lnkrawmaterialid,id,inventory,rateperunit FROM ".CONFIG_DBN.".cfrawmaterial_loc 
                                      WHERE lnkrawmaterialid=:item_id
                                      AND companyid=:companyid AND storeid=:storeid";
                    $dao->initCommand($strrawLoc);
                    $dao->addParameter(':storeid', $data['pstoreid']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':item_id', $value['item_id']);
                    $resrawLoc = $dao->executeRow();
	
                    /* Date : 24-04-2018
                     * If inventory item not exists in reciving store then first need to add that item into reciving store Line no 550 to 63 !$resrawLoc condition
                     * */
                    if(!$resrawLoc)
					{
						$insertInventorySql  = " INSERT INTO  ".CONFIG_DBN.".cfrawmaterial_loc (lnkrawmaterialid,inv_unitid,storeid,companyid) VALUES (:lnkrawmaterialid,:inv_unitid,:storeid,:companyid) ";
						$dao->initCommand($insertInventorySql);
						$dao->addParameter(':lnkrawmaterialid',$value['item_id']);
						$dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
						$dao->addParameter(':storeid',$data['pstoreid']);
						$dao->addParameter(':companyid',CONFIG_CID);
						$dao->executeNonQuery();
						$resrawLoc = [];
						$resrawLoc['id'] = $dao->getLastInsertedId();
						$resrawLoc['rateperunit'] = 0;
						$resrawLoc['inventory'] = 0;
					}
     

                    $strinventory = "INSERT INTO " . CONFIG_DBN . ".cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,rate,total_rate,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,companyid,storeid,rel_storeid)
                                                            VALUE(:rawlocid,:itemid,:inventory,:rate, :total_rate, :inv_unitid,:ref_id,:invstatus,:crdb,:created_user,:createddatetime,:companyid,:storeid,:rel_storeid)";
                    $dao->initCommand($strinventory);
                    $dao->addParameter(':itemid', $value['item_id']);
                    $dao->addParameter(':rawlocid', $resrawLoc['id']);
                    $dao->addParameter(':inventory', $inventory);
                    $dao->addParameter(':rate', $amount);
                    $dao->addParameter(':total_rate', $value['item_total_amt']);
                    $dao->addParameter(':inv_unitid',$rawMaterialRec['unitunkid']);
                    $dao->addParameter(':ref_id',$lnkissuedetailid);
                    $dao->addParameter(':invstatus',INV_STATUS_IV);
                    $dao->addParameter(':crdb','cr');
                    $dao->addParameter(':createddatetime',$datetime);
                    $dao->addParameter(':created_user',CONFIG_UID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid', $data['pstoreid']);
                    $dao->addparameter(':rel_storeid',CONFIG_SID);
                    $dao->executeNonQuery();

                    $old_price = $resrawLoc['rateperunit'] * $resrawLoc['inventory'];
                    $total_inventory = $resrawLoc['inventory'] + $inventory ;
                    $totalPrice = $old_price + $value['item_total_amt'];
                    $rateperunit = $totalPrice / $total_inventory;
                    $rateperunit = ($rateperunit > 0) ? $rateperunit : 0;

                    $strSql = "UPDATE " . CONFIG_DBN . ".cfrawmaterial_loc SET rateperunit=:rateperunit,inventory=:inventory                         
                                    WHERE lnkrawmaterialid=:itemid AND companyid=:companyid
                                    and storeid=:storeid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':inventory', $total_inventory);
                    $dao->addParameter(':rateperunit', $rateperunit);
                    $dao->addParameter(':itemid', $value['item_id']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':storeid', $data['pstoreid']);
                    $dao->executeNonQuery();
                    //Inventory//

                    if ($data['Indent_id'] != '') {
                        $Str = "UPDATE " . CONFIG_DBN . ".cfindentrequestdetail SET
                     lnkissueitemid=:lnkissueitemid WHERE lnkindentid=:lnkindentid AND companyid=:companyid 
                     AND indentrequnkid=:indentrequnkid";
                        $dao->initCommand($Str);
                        $dao->addParameter(':lnkissueitemid', $lnkissuedetailid);
                        $dao->addParameter(':lnkindentid', $data['Indent_id']);
                        $dao->addParameter(':indentrequnkid', $value['itm_detId']);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->executeNonQuery();

                    }
                }

                $Status = $this->getstatus($data['Indent_id']);
                $strSql = "UPDATE  " . CONFIG_DBN . ".cfrequestindent SET 
                status=:status WHERE companyid=:companyid and indentid=:indentid AND storeid=:storeid AND recstoreid=:recstoreid";
                $dao->initCommand($strSql);
                $dao->addParameter(':indentid', $data['Indent_id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':status', $Status);
                $dao->addParameter(':storeid', $data['pstoreid']);
                $dao->addParameter(':recstoreid', CONFIG_SID);
                $dao->executeNonQuery();
                $dao->releaseTransaction(TRUE);
                return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->ITEM_ISU_SUC));
            }

        } catch (Exception $e) {
        	$dao->releaseTransaction(FALSE);
            $this->log->logIt($this->module . ' - addissueindent - ' . $e);
        }
    }

    public function getIndentDetail($indent_id)
    {
        try {
            $this->log->logIt($this->module . ' - getIndentDetail');
            $dao = new \dao();
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $strSql = "SELECT CRI.indentid,CRI.indent_doc_num ,CRI.totalamount,CL.storename,IFNULL(DATE_FORMAT(CRI.indent_date,'" . $mysql_format . "'),'') as indent_date " .
                "FROM " . CONFIG_DBN . ".cfrequestindent AS CRI LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS CL ON CRI.storeid=CL.storeunkid " .
                "WHERE CRI.indentid=:id and CRI.companyid=:companyid and CRI.recstoreid=:recstoreid";
            $dao->initCommand($strSql);
            $dao->addParameter(':recstoreid', CONFIG_SID);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':id', $indent_id);
            $resObj = $dao->executeRow();
            return $resObj;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . ' - getIndentDetail - ' . $e);
        }
    }

    public function getstatus($indentid)
    {
        try {
            $this->log->logIt($this->module . ' - getstatus');
            $dao = new \dao();
            $this->log->logIt($indentid);
            $strSql = "SELECT COUNT(CASE WHEN lnkissueitemid = 0 then 1 ELSE NULL END) as 'Pending', 
            COUNT(CASE WHEN lnkissueitemid != 0 then 1 ELSE NULL END) as 'Completed' from 
            " . CONFIG_DBN . ".cfindentrequestdetail WHERE lnkindentid=:lnkindentid and companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':lnkindentid', $indentid);
            $res = $dao->executeRow();

            $status = 0;
            if ($res['Pending'] > 0 && $res['Completed'] == 0) {
                $status = 0;
            } elseif ($res['Completed'] > 0 && $res['Pending'] > 0) {
                $status = 1;
            } else {
                $status = 2;
            }

            return $status;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getstatus - ' . $e);
        }
    }

    public function getreceviedindentdetail($id)
    {
        try {
            $this->log->logIt($this->module . ' - getreceviedindentdetail');
            $dao = new \dao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $resarr = array();
            $strSql = "SELECT CASE WHEN CFV.authstatus=0 THEN 'Unauthorised' WHEN CFV.authstatus=1
                       THEN 'Authorised' END as auth_stat,C.companyname,C.address1 AS company_address,C.phone,C.currency_sign,C.companyemail,
                        CFV.indent_doc_num AS voucher_num,CFU2.username AS auth_user,IFNULL(C.logo,'') as company_logo,
                        CFV.remarks,DATE_FORMAT(CFV.indent_date,'" . $mysql_format . "') as Voucher_Date,IFNULL(CFU1.username,'') AS createduser,
                        ROUND(IFNULL(CFV.totalamount,0),$round_off) as totalamount,
                        S.storename as issuing_store,ST.storename AS rec_store 
                        FROM " . CONFIG_DBN . ".cfrequestindent AS CFV 
                        LEFT JOIN " . CONFIG_DBN . ".cfcompany as C ON CFV.companyid= C.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster as S ON CFV.storeid=S.storeunkid AND S.companyid=:companyid AND S.is_active=1 AND S.is_deleted=0
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS ST on CFV.recstoreid=ST.storeunkid AND ST.companyid=:companyid AND ST.is_active=1 AND ST.is_deleted=0
                        LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU1 ON CFV.createduser=CFU1.userunkid AND CFU1.companyid=CFU1.companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU2 ON CFV.authorized_user=CFU2.userunkid AND CFU2.companyid=CFU1.companyid
                        WHERE CFV.recstoreid=:storeid AND CFV.is_deleted=0  AND
                         CFV.companyid=:companyid AND CFV.indentid=:indentid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $dao->addparameter(':indentid', $id);
            $indentrecords = $dao->executeRow();


            $resarr['company_name'] = $indentrecords['companyname'];
            $resarr['address'] = $indentrecords['company_address'];
            $resarr['phone'] = $indentrecords['phone'];
            $resarr['email'] = $indentrecords['companyemail'];
            $resarr['company_logo'] = !empty($indentrecords['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$indentrecords['company_logo']:'';
            $resarr['currency_sign'] = $indentrecords['currency_sign'];
            $resarr['voucher_num'] = $indentrecords['voucher_num'];
            $resarr['remarks'] = $indentrecords['remarks'];
            $resarr['Voucher_Date'] = $indentrecords['Voucher_Date'];
            $resarr['totalamount'] = $indentrecords['totalamount'];
            $resarr['rec_store'] = $indentrecords['issuing_store'];
            $resarr['issuing_store'] = $indentrecords['rec_store'];
            $resarr['createduser'] = $indentrecords['createduser'];
            $resarr['auth_user'] = $indentrecords['auth_user'];
            $resarr['auth_stat'] = $indentrecords['auth_stat'];

            $str = "SELECT  CRM.name AS Item,IVD.qty,U.shortcode as Unitname,IVD.rate,CRL.inventory,CRL.inventory,
                    IFNULL(ROUND((IVD.qty*IVD.rate),$round_off),'') AS Amount 
                    FROM " . CONFIG_DBN . ".cfindentrequestdetail AS IVD 
                    LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CRM ON IVD.lnkrawid=CRM.id AND CRM.companyid=:companyid AND CRM.is_active=1 AND CRM.is_deleted=0
                    LEFT JOIN " . CONFIG_DBN . ".cfunit AS U ON IVD.lnkunitid=U.unitunkid AND U.companyid=:companyid AND U.is_active=1 AND U.is_deleted=0
                    LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial_loc AS CRL ON IVD.lnkrawid=CRL.lnkrawmaterialid AND CRL.companyid=:companyid AND  CRL.storeid=:storeid 
                 
                    WHERE IVD.companyid=:companyid AND IVD.lnkindentid=:indentid";

                $dao->initCommand($str);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeid', CONFIG_SID);
                $dao->addparameter(':indentid', $id);

                $indentdetail = $dao->executeQuery();

            $resarr['indentdetail'] = $indentdetail;
            $total = $to = 0;
            foreach ($indentdetail AS $val) {
                $to = $val['qty'] * $val['rate'];
                $total += $to;
            }

            $resarr['totalamount'] = number_format($total, $round_off);
            $resarr['current_date'] = \util\util::convertMySqlDate($current_date);

            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getreceviedindentdetail - ' . $e);
        }
    }

    public function getIssueRecordsonID($issueid)
    {
        try {
            $this->log->logIt($this->module . " - getIssueRecordsonID");
            $dao = new \dao();
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $strSqltogetindentrecords = "SELECT CRI.*,CL.storename,CRI.issue_doc_num as indent_doc_num,CRI.relstoreid AS storeid,
             IFNULL(DATE_FORMAT(CRI.issue_date,'" . $mysqlformat . "'),'') as indentformated_date
             FROM " . CONFIG_DBN . ".cfissuevoucher AS CRI 
            LEFT JOIN  " . CONFIG_DBN . ".cfstoremaster AS CL ON CRI.storeid=CL.storeunkid
            WHERE CRI.issueid=:issueid and CRI.companyid=:companyid and CRI.storeid=:storeid";
            $dao->initCommand($strSqltogetindentrecords);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addParameter(':issueid', $issueid);
            $indent = $dao->executeRow();

            $strSqltogetindentdetail = "SELECT CR.name AS selectedrm,U.name AS selectedunit,U.unit as unit_val,CRI.*,CRI.issuerequnkid AS indentrequnkid,CRI.qty AS auth_qty
            FROM " . CONFIG_DBN . ".cfissuevoucherdetail AS CRI 
            LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CRI.lnkrawid=CR.id 
            LEFT JOIN " . CONFIG_DBN . ".cfunit U ON CRI.lnkunitid=U.unitunkid AND
            U.companyid=:companyid AND U.is_active=1 and U.is_deleted=0 
            WHERE CRI.lnkissueid=:lnkissueid";

            $dao->initCommand($strSqltogetindentdetail);
            $dao->addParameter(':lnkissueid', $indent['issueid']);
            $dao->addParameter(':companyid', CONFIG_CID);
            $indentdetail = $dao->executeQuery();

            return json_encode(array("Success" => "True", "order" => $indent, "orderDetail" => $indentdetail));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getIssueRecordsonIDz - " . $e);
            return false;
        }
    }


}

?>