<?php
namespace database;

class unitdao
{
    public $module = 'DB_unit';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function addUnit($data,$languageArr,$defaultlanguageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - addUnit');
            $dao = new \dao();
            $datetime=\util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $ObjDependencyDao = new \database\dependencydao();
            $tablename =  "cfunit";
            $measuretype= '';
            if($data['measuretype']==1){$measuretype=''.$languageArr->LANG8;}
            elseif($data['measuretype']==2){$measuretype=''.$languageArr->LANG9;}
            else{$measuretype=''.$languageArr->LANG10;}

            $arr_log = array(
                'Unit Name'=>$data['name'],
                'Shortcode'=>$data['shortcode'],
                'Unit'=>$data['unit'],
                'Measure Type'=>$measuretype,
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive'
            );
            $json_data = html_entity_decode(json_encode($arr_log));

            if($data['id']==0)
            {
                $chk_name = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"name",$data['name']);
                if($chk_name==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG11)));
                }
                $chk_scode = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"shortcode",$data['shortcode']);
                if($chk_scode==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG12)));
                }
                $title = "Add Record";
                $hashkey = \util\util::gethash();

                $strSql = "INSERT INTO ".CONFIG_DBN.".cfunit(`name`, 
                                                                    shortcode, 
                                                                    unit, 
                                                                    measuretype,
                                                                    is_systemdefined,
                                                                    companyid,
                                                                    storeid,
                                                                    createddatetime, 
                                                                    created_user, 
                                                                    is_active, 
                                                                    hashkey)
                                                            VALUE(  :name, 
                                                                    :shortcode, 
                                                                    :unit, 
                                                                    :measuretype,
                                                                    :is_systemdefined,
                                                                    :companyid,
                                                                    :storeid, 
                                                                    :createddatetime, 
                                                                    :created_user, 
                                                                    :rdo_status, 
                                                                    :hashkey)";
                
                $dao->initCommand($strSql);
                $dao->addParameter(':name',$data['name']);
                $dao->addParameter(':shortcode',$data['shortcode']);
                $dao->addParameter(':unit', $data['unit']);
                $dao->addParameter(':measuretype', $data['measuretype']);
                $dao->addParameter(':is_systemdefined', 0);
                $dao->addParameter(':rdo_status',$data['rdo_status']);
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':created_user',CONFIG_UID);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':storeid',CONFIG_SID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_ADD_SUC)));
           }
           else
           {
               $id =$ObjCommonDao->getprimaryBycompany('cfunit',$data['id'],'unitunkid');
               $chk_name = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"name",$data['name'],$id);
               if($chk_name==1)
               {
                   return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG11)));
               }
               $chk_scode = $ObjDependencyDao->checkduplicaterecordBycompany($tablename,"shortcode",$data['shortcode'],$id);
               if($chk_scode==1)
               {
                   return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG12)));
               }

                $title = "Edit Record";
                $strSql = "UPDATE ".CONFIG_DBN.".cfunit SET name=:name,
                                                                       shortcode=:shortcode,
                                                                    unit=:unit,
                                                                    measuretype=:measuretype,
                                                                    modifieddatetime=:modifieddatetime,
                                                                    modified_user=:modified_user
                                                                    WHERE hashkey=:unitunkid AND companyid=:companyid";
                $dao->initCommand($strSql);
                $dao->addParameter(':unitunkid',$data['id']);
                $dao->addParameter(':name',$data['name']);
                $dao->addParameter(':shortcode',$data['shortcode']);
                $dao->addParameter(':unit', $data['unit']);
                $dao->addParameter(':measuretype', $data['measuretype']);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_UP_SUC)));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addUnit - '.$e);
        }
    }

    public function getrelatedRecords($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getRelatedRecords');
            $dao = new \dao();
            $result = array();
            $ObjCommonDao = new \database\commondao();
            if($data['id']!="")
            {
                $id = $ObjCommonDao->getprimaryBycompany('cfunit',$data['id'],'unitunkid');

                //Check Rawmaterial
                $strSql = "SELECT GROUP_CONCAT(CR.name) AS subject FROM ".CONFIG_DBN.".cfrawmaterial CR
                            INNER JOIN ".CONFIG_DBN.". cfrawmaterial_loc CRL ON CRL.lnkrawmaterialid=CR.id 
                            INNER JOIN ".CONFIG_DBN.".cfunit CU ON CRL.min_unitid=CU.unitunkid OR CRL.inv_unitid=CU.unitunkid
                            WHERE CU.unitunkid =:id AND CU.is_deleted=0 AND CR.is_deleted=0 
                            AND CU.companyid=:companyid and CR.companyid=:companyid 
                            AND CRL.storeid=:storeid ORDER BY CR.id";

                $dao->initCommand($strSql);
                $dao->addParameter(":companyid",CONFIG_CID);
                $dao->addparameter(':storeid',CONFIG_SID);
                $dao->addParameter(":id",$id);
                $resobj = $dao->executeRow();
                $arr_item = array();
                if($resobj['subject']!=""){
                    $arr_item['subject'] = $resobj['subject'];
                    $arr_item['module'] = \common\staticarray::$getmodulename['cfrawmaterial']['Display_Name'];
                    $result[] = $arr_item;
                }
                return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$result)));
            }
            else{
                return html_entity_decode(json_encode(array("Success"=>"False","Data"=>"")));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getRelatedRecords - '.$e);
        }
    }

    public function unitlist($limit,$offset,$name,$isactive=0)
    {
        try
        {
            $this->log->logIt($this->module.' - unitlist - '.$name);
            $dao = new \dao;
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT CU.name,CU.hashkey,ROUND(CU.unit,$round_off) as unit,CU.is_systemdefined,CASE CU.measuretype WHEN 1 THEN 'Weight' WHEN 2 THEN 'Liquid Capacity' WHEN 3 THEN 'Pieces' END as measuretype,CU.is_active,IFNULL(CFU1.username,'') AS createduser,
                       IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CU.createddatetime,'".$mysqlformat."'),'') as created_date,
                       IFNULL(DATE_FORMAT(CU.modifieddatetime,'".$mysqlformat."'),'') as modified_date
                       FROM ".CONFIG_DBN.".cfunit AS CU
                       LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CU.created_user=CFU1.userunkid AND CU.companyid=CFU1.companyid
                       LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON CU.modified_user=CFU2.userunkid AND CU.modified_user=CFU2.userunkid AND CU.companyid=CFU2.companyid
                       WHERE CU.companyid=:companyid AND CU.is_deleted=0";

            if($name!="")
                $strSql .= " AND name LIKE '%".$name."%'";
            if($isactive==1)
                $strSql .= " AND is_active=1";
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $rec = $dao->executeQuery();

            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue));
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - unitlist - '.$e);
        }
    }

    public function getUnitRec($data)
    {
        try
        {
            $this->log->logIt($this->module." - getUnitRec");
            $dao = new \dao();
            $id=$data['id'];
            $strSql = "SELECT `name`,unitunkid,shortcode,unit,measuretype,is_active FROM ".CONFIG_DBN.".cfunit WHERE hashkey=:unitunkid AND companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':unitunkid',$id);
            $dao->addParameter(':companyid',CONFIG_CID);
            $res = $dao->executeRow();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUnitRec - ".$e);
            return false;
        }
    }
}
?>