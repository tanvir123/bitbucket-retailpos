<?php
namespace database;

class displaysettingsdao
{
    public $module = 'DB_displaysettingsdao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function loaddisplaysettingslist()
    {
        try
        {
            $this->log->logIt($this->module.' - loaddisplaysettingslist');

            $gratuity_tax_desciption= \database\parameter::getParameter('gratuity_tax_detail');
			$res['timezone'] = html_entity_decode(\database\parameter::getParameter('timezone_main'),ENT_QUOTES);
            $res['dateformat'] = \database\parameter::getParameter('dateformat');
            $res['timeformat'] = \database\parameter::getParameter('timeformat');
			$res['salutation'] = \database\parameter::getParameter('salutation');
			$res['country'] = \database\parameter::getParameter('country');
			$res['nationality'] = \database\parameter::getParameter('nationality');
			$res['roundoff'] = \database\parameter::getParameter('roundoff');
			$opening_time=\database\parameter::getParameter('restaurant_opening_time');
			$res['restaurant_opening_time'] = isset($opening_time)?$opening_time:'';
			$closing_time=\database\parameter::getParameter('restaurant_closing_time');
			$res['restaurant_closing_time'] = isset($closing_time)?$closing_time:'';
			$nightauditchnage_time=\database\parameter::getParameter('nightauditchnage_time');
			$res['nightauditchnage_time'] = isset($nightauditchnage_time)?$nightauditchnage_time:'';
			//$res['is_preorder'] = \database\parameter::getParameter('is_preorder');
			//$res['location_info'] = \database\parameter::getParameter('location_info');
			//$res['terms_condition'] = \database\parameter::getParameter('terms_condition');
			$res['usermandatoryinfo'] = \database\parameter::getParameter('usermandatoryinfo');
			$res['visibility_field'] = \database\parameter::getParameter('visibility_field');
            $res['digitafterdecimal'] = \database\parameter::getParameter('digitafterdecimal');
            $res['payment_type'] = \database\parameter::getParameter('payment_type');
			$res['invoice'] = \database\parameter::getParameter('invoice');
			$res['fncialyearstartmonth'] = \database\parameter::getParameter('fncialyearstartmonth');
			$res['fncialyearstartdate'] = \database\parameter::getParameter('fncialyearstartdate');
			//$res['closefolioonsettlement'] = \database\parameter::getParameter('closefolioonsettlement');
			$res['invoice_display_lbl'] = \database\parameter::getParameter('invoice_display_lbl');
           // $res['takeaway_nocharge_user_available'] = \database\parameter::getParameter('takeaway_nocharge_user_available');
            //$res['genrate_invoice_for_nochargeuser'] = \database\parameter::getParameter('genrate_invoice_for_nochargeuser');
            $res['cashdrawer_popup_at_login_logouttime'] = \database\parameter::getParameter('cashdrawer_popup_at_login_logouttime');
            $res['table_netlock'] = \database\parameter::getParameter('table_netlock');
			//$res['consolidatemenuitemoption'] = \database\parameter::getParameter('consolidatemenuitemoption');
            //$res['printonfinishorder'] = \database\parameter::getParameter('printonfinishorder');
            //$res['kot_display_with_price'] = \database\parameter::getParameter('kot_display_with_price');
            $res['use_multicurrency_on_settlement'] = \database\parameter::getParameter('use_multicurrency_on_settlement');
           // $res['waiting_list'] = \database\parameter::getParameter('waiting_list');
            $res['kot_display_with_other_info'] = \database\parameter::getParameter('kot_display_with_other_info');
            $res['choose_kot_format'] = \database\parameter::getParameter('choose_kot_format');
            $res['fiscal_invoice_types'] = \database\parameter::getParameter('fiscal_invoice_types');
            $res['print_kotsettings_after_onetimeprint'] = \database\parameter::getParameter('print_kotsettings_after_onetimeprint');
            //$res['choose_rate'] = \database\parameter::getParameter('choose_rate');
            $res['avg_food_time'] = \database\parameter::getParameter('avg_food_time');
            $res['display_unit_selection'] = \database\parameter::getParameter('display_unit_selection');
            $res['display_unit_selection_for_combo'] = \database\parameter::getParameter('display_unit_selection_for_combo');
            $res['kot_print_preview_enable'] = \database\parameter::getParameter('kot_print_preview_enable');
            $res['item_display_type_in_menu'] = \database\parameter::getParameter('item_display_type_in_menu');
            //$res['finish_and_print_print_kot_or_invoice'] = \database\parameter::getParameter('finish_and_print_print_kot_or_invoice');
            $res['kot_cancel_item_display'] = \database\parameter::getParameter('kot_cancel_item_display');
	    //$res['display_table_class_in_kot'] = \database\parameter::getParameter('display_table_class_in_kot');
            $res['refund_payment_and_show_refundorders'] = \database\parameter::getParameter('refund_payment_and_show_refundorders');
            $res['bill_to_company_account'] = \database\parameter::getParameter('bill_to_company_account');
            $res['item_replacement_option_for_combo_item'] = \database\parameter::getParameter('item_replacement_option_for_combo_item');
            $res['cancel_order_kot_generate'] = \database\parameter::getParameter('cancel_order_kot_generate');
            $res['monthly_statistics'] = \database\parameter::getParameter('monthly_statistics');
            $res['daily_revenue'] = \database\parameter::getParameter('daily_revenue');
            $res['item_sale_statistics'] = \database\parameter::getParameter('item_sale_statistics');
            $res['daily_satistics'] = \database\parameter::getParameter('daily_satistics');
            $res['night_audit_email'] = \database\parameter::getParameter('night_audit_email');
            $res['payment_type_report'] = \database\parameter::getParameter('payment_type_report');
            $res['nochargeuser_report'] = \database\parameter::getParameter('nochargeuser_report');
            $res['complementary_report'] = \database\parameter::getParameter('complementary_report');
            $res['gratuity_report'] = \database\parameter::getParameter('gratuity_report');
            $res['combo_report'] = \database\parameter::getParameter('combo_report');
	     $res['waiter_wise_sale_report'] = \database\parameter::getParameter('waiter_wise_sale_report');
            $res['cashdrawer_report'] = \database\parameter::getParameter('cashdrawer_report');
            $res['gratuity_tax_value'] = \database\parameter::getParameter('gratuity_tax_value');
            $res['gratuity_tax_type'] = \database\parameter::getParameter('gratuity_tax_type');
            $res['gratuity_tax_detail'] = html_entity_decode($gratuity_tax_desciption,ENT_QUOTES);
            $res['night_audit_cc_email'] = \database\parameter::getParameter('night_audit_cc_email');
            $res['kot_port_type'] = \database\parameter::getParameter('kot_port_type');
            $CommonDao = new commondao();
            $country = $CommonDao->getCountryList();
            $displaySetting = new \displaysettings();
            $res['location_meta'] = json_decode($displaySetting->getmetarecords(),true);
            $arr['countrylist'] = $country;
			$arr['displaysettings'] = $res;
			
            return json_encode(array("Success"=>"True","Data"=>$arr));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loaddisplaysettingslist - '.$e);
        }
    }
    public function editDisplaysettings($data)
    {
        try
        {
            $this->log->logIt($this->module.' - editDisplaysettings');
            $user_info = '0';
            $visibility_field = '0';
            $dao = new \dao();

			if($data['user_info']!="" && count($data['user_info'])>0){
                $user_info = implode(',', $data['user_info']);
            }if($data['visibility_field']!="" && count($data['visibility_field'])>0){
                $visibility_field = implode(',', $data['visibility_field']);
            }

            $timezone4 = substr(urldecode($data['select_timezone']), 4, 6);
            \database\parameter::setParameter('timezone',$timezone4);
            \database\parameter::setParameter('timezone_main',$data['select_timezone']);
			\database\parameter::setParameter('dateformat',$data['select_date_format']);
            \database\parameter::setParameter('timeformat',$data['select_time_format']);
			\database\parameter::setParameter('salutation',$data['select_salutation']);
			\database\parameter::setParameter('fncialyearstartdate',$data['fncialyearstartdate']);
			\database\parameter::setParameter('fncialyearstartmonth',$data['fncialyearstartmonth']);
			\database\parameter::setParameter('country',$data['select_country']);
			\database\parameter::setParameter('nationality',$data['select_nationality']);
			\database\parameter::setParameter('digitafterdecimal',$data['select_decimal_digit']);
            \database\parameter::setParameter('payment_type',$data['payment_type']);
			\database\parameter::setParameter('invoice',$data['invoice']);
			// \database\parameter::setParameter('closefolioonsettlement',$data['closefolioonsettlement']);
			\database\parameter::setParameter('invoice_display_lbl',$data['invoice_display_lbl']);
			//\database\parameter::setParameter('takeaway_nocharge_user_available',$data['takeaway_nocharge_user_available']);
			//\database\parameter::setParameter('genrate_invoice_for_nochargeuser',$data['genrate_invoice_for_nochargeuser']);
			\database\parameter::setParameter('cashdrawer_popup_at_login_logouttime',$data['cashdrawer_popup_at_login_logouttime']);
			\database\parameter::setParameter('table_netlock',$data['table_netlock']);
			//\database\parameter::setParameter('consolidatemenuitemoption',$data['consolidatemenuitemoption']);
			//\database\parameter::setParameter('printonfinishorder',$data['printonfinishorder']);
			//\database\parameter::setParameter('kot_display_with_price',$data['kot_display_with_price']);
			\database\parameter::setParameter('use_multicurrency_on_settlement',$data['use_multicurrency_on_settlement']);
			//\database\parameter::setParameter('waiting_list',$data['waiting_list']);
			\database\parameter::setParameter('kot_display_with_other_info',$data['kot_display_with_other_info']);
			\database\parameter::setParameter('choose_kot_format',$data['choose_kot_format']);
			\database\parameter::setParameter('fiscal_invoice_types',$data['fiscal_invoice_types']);
			\database\parameter::setParameter('print_kotsettings_after_onetimeprint',$data['print_kotsettings_after_onetimeprint']);
			//\database\parameter::setParameter('choose_rate',$data['choose_rate']);
			\database\parameter::setParameter('avg_food_time',$data['avg_food_time']);
            \database\parameter::setParameter('display_unit_selection',$data['display_unit_selection']);
            \database\parameter::setParameter('display_unit_selection_for_combo',$data['display_unit_selection_for_combo']);
            \database\parameter::setParameter('kot_print_preview_enable',$data['kot_print_preview_enable']);
            \database\parameter::setParameter('item_display_type_in_menu',$data['item_display_type_in_menu']);
            //\database\parameter::setParameter('finish_and_print_print_kot_or_invoice',$data['finish_and_print_print_kot_or_invoice']);
            \database\parameter::setParameter('kot_cancel_item_display',$data['kot_cancel_item_display']);
	    //\database\parameter::setParameter('display_table_class_in_kot',$data['display_table_class_in_kot']);
            \database\parameter::setParameter('refund_payment_and_show_refundorders',$data['refund_payment_and_show_refundorders']);
            \database\parameter::setParameter('bill_to_company_account',$data['bill_to_company_account']);
            \database\parameter::setParameter('item_replacement_option_for_combo_item',$data['item_replacement_option_for_combo_item']);
            \database\parameter::setParameter('cancel_order_kot_generate',$data['cancel_order_kot_generate']);
            \database\parameter::setParameter('monthly_statistics',$data['monthly_statistics']);
            \database\parameter::setParameter('daily_revenue',$data['daily_revenue']);
            \database\parameter::setParameter('item_sale_statistics',$data['item_sale_statistics']);
            \database\parameter::setParameter('daily_satistics',$data['daily_satistics']);
            \database\parameter::setParameter('night_audit_email',$data['night_audit_email']);
            \database\parameter::setParameter('payment_type_report',$data['payment_type_report']);
            \database\parameter::setParameter('nochargeuser_report',$data['nochargeuser_report']);
            \database\parameter::setParameter('complementary_report',$data['complementary_report']);
            \database\parameter::setParameter('gratuity_report',$data['gratuity_report']);
            \database\parameter::setParameter('combo_report',$data['combo_report']);
	    \database\parameter::setParameter('waiter_wise_sale_report',$data['waiter_wise_sale_report']);
            \database\parameter::setParameter('cashdrawer_report',$data['cashdrawer_report']);
            \database\parameter::setParameter('usermandatoryinfo',$user_info);
            \database\parameter::setParameter('visibility_field',$visibility_field);
            \database\parameter::setParameter('gratuity_tax_detail',json_encode($data['gratuity_tax_detail']));
            \database\parameter::setParameter('gratuity_tax_type',$data['gratuity_tax_type']);
            \database\parameter::setParameter('gratuity_tax_value',$data['gratuity_tax_value']);
            \database\parameter::setParameter('td_no',$data['td_no']);
            \database\parameter::setParameter('p_no',$data['p_no']);
            \database\parameter::setParameter('h2_no',$data['h2_no']);
            \database\parameter::setParameter('h3_no',$data['h3_no']);
            \database\parameter::setParameter('h4_no',$data['h4_no']);
            \database\parameter::setParameter('h5_no',$data['h5_no']);
            \database\parameter::setParameter('h6_no',$data['h6_no']);
			$round = ($data['select_round_type']=='0')?$data['txtlimit']:$data['select_round_type'];
			\database\parameter::setParameter('roundoff',$round);
			\database\parameter::setParameter('restaurant_opening_time',$data['opening_time']);
			\database\parameter::setParameter('restaurant_closing_time',$data['closing_time']);
			\database\parameter::setParameter('nightauditchnage_time',$data['nightauditchnage_time']);
			//\database\parameter::setParameter('is_preorder',$data['is_preorder']);
			//\database\parameter::setParameter('location_info',$data['location_info']);
			//\database\parameter::setParameter('terms_condition',$data['terms_condition']);
			\database\parameter::setParameter('night_audit_cc_email',$data['night_audit_cc_email']);
			\database\parameter::setParameter('kot_port_type',$data['kot_port_type']);

            $strSql = "UPDATE ".CONFIG_DBN.".cflocation SET is_ordering='".$data['is_ordering']."' WHERE locationid = ".CONFIG_LID."";
            $dao->initCommand($strSql);
            $dao->executeNonQuery();


            if ($data['invoice'] == 3) {

                $OBJlocationdao = new \database\document_numberingdao();
                $location_data = $OBJlocationdao->getInvoiceNumbering();

                $locationdetal_array = array();
                $locationdetal_array['dian_num'] = $data['dian_num'];
                $locationdetal_array['nit_number'] = $data['registration_no'];
                $locationdetal_array['appliesfrom'] = $data['appliesfrom'];
                $locationdetal_array['main_start_no'] = $location_data['main_start_no'];
                $locationdetal_array['endno'] = $location_data['endno'];
                $locationdetal_array['welcomemsg'] = $data['welcomemsg'];
                $locationdetal_array['note'] = $data['note'];

                //this will work only if template 3 is selected ! Just used currently for Columbian client requirement.
                $strSql2 = "INSERT INTO " . CONFIG_DBN . ". cflocationtemplate_meta SET  meta_value=:meta_value,meta_key=:meta_key,
                            companyid=:companyid,locationid=:locationid,description=:description";
                $dao->initCommand($strSql2);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $dao->addParameter(':meta_key', 'template3');
                $dao->addParameter(':description', 'it contains DIAN number for registering their folio number, NIT num : Fiscal restuarant number, welcome message, Invoice note which will display at the end of template3');
                $dao->addParameter(':meta_value', json_encode($locationdetal_array));
                $dao->executeNonQuery();

            }

            if(isset($data['keyboard_status'])){
                $keyboard_status = ($data['keyboard_status']==1)?1:0;
                $keybordsql = " INSERT INTO  " . CONFIG_DBN. ".cfcompanylocation_meta (`meta_value`,`meta_key`,`companyid`,`locationid`) VALUES ('" . $keyboard_status. "','keyboard_status','" . CONFIG_CID . "','".CONFIG_LID."') ON DUPLICATE KEY  UPDATE `meta_value` = '" . $keyboard_status . "';";
                $dao->initCommand($keybordsql);
                $dao->executeNonQuery();
            }

            $bulksql='';
            if(isset($data['print_portdata']) && $data['print_portdata']!=''){

                foreach ($data['print_portdata'] as $key=>$val){
                    $bulksql.= " INSERT INTO  " . CONFIG_DBN. ".cfcompanylocation_meta (`meta_value`,`meta_key`,`companyid`,`locationid`) VALUES ('" . $val. "','" . $key  . "','" . CONFIG_CID . "','".CONFIG_LID."') ON DUPLICATE KEY  UPDATE `meta_value` = '" . $val . "';";
                }
            }
            if($bulksql){
                $dao->initCommand($bulksql);
                $dao->executeNonQuery();
            }

            $tag_array = array();
            $tag_array['td_no'] = $data['td_no'];
            $tag_array['p_no'] = $data['p_no'];
            $tag_array['h2_no'] = $data['h2_no'];
            $tag_array['h3_no'] = $data['h3_no'];
            $tag_array['h4_no'] = $data['h4_no'];
            $tag_array['h5_no'] = $data['h5_no'];
            $tag_array['h6_no'] = $data['h6_no'];



            $strSql2 = "SELECT locationmetaunkid FROM " . CONFIG_DBN . ". cflocationtemplate_meta  WHERE
                            companyid=:companyid AND locationid=:locationid AND meta_key=:meta_key ";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->addParameter(':meta_key', "tags_value");
            $tag_detail= $dao->executeRow();


            if($tag_detail) {
                $strSql2 = "UPDATE " . CONFIG_DBN . ". cflocationtemplate_meta SET  meta_value=:meta_value WHERE locationmetaunkid=:locationmetaunkid ";
                $dao->initCommand($strSql2);
                $dao->addParameter(':locationmetaunkid', $tag_detail['locationmetaunkid']);
                $dao->addParameter(':meta_value', json_encode($tag_array));
                $dao->executeNonQuery();
            }else{
                $strSql2 = "INSERT INTO " . CONFIG_DBN . ". cflocationtemplate_meta SET  meta_value=:meta_value,meta_key=:meta_key,
                            companyid=:companyid,locationid=:locationid,description=:description";
                $dao->initCommand($strSql2);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $dao->addParameter(':meta_key', 'tags_value');
                $dao->addParameter(':description', 'Template Tag values in pixel');
                $dao->addParameter(':meta_value', json_encode($tag_array));
                $dao->executeNonQuery();
            }


            $table_booking_setting = array();
            $table_booking_setting['pax_limit'] = $data['pax_limit'];
            $table_booking_setting['avg_time'] = $data['avg_time'];
            $table_booking_setting['days_before_booking'] = $data['days_before_booking'];
            $table_booking_setting['hours_before_booking'] = $data['hours_before_booking'];
            $table_booking_setting['advanced_payment_available'] = $data['advanced_payment_available'];
            //$table_booking_setting['advanced_payment_amount'] = $data['advanced_payment_amount'];



            $strSql2 = "SELECT locationmetaunkid FROM " . CONFIG_DBN . ". cflocationtemplate_meta  WHERE
                            companyid=:companyid AND locationid=:locationid AND meta_key=:meta_key ";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->addParameter(':meta_key', "table_booking_settings");
            $table_booking_id= $dao->executeRow();


            if($table_booking_id) {
                $strSql2 = "UPDATE " . CONFIG_DBN . ". cflocationtemplate_meta SET  meta_value=:meta_value WHERE locationmetaunkid=:locationmetaunkid ";
                $dao->initCommand($strSql2);
                $dao->addParameter(':locationmetaunkid', $table_booking_id['locationmetaunkid']);
                $dao->addParameter(':meta_value', json_encode($table_booking_setting));
                $dao->executeNonQuery();
            }else{
                $strSql2 = "INSERT INTO " . CONFIG_DBN . ". cflocationtemplate_meta SET  meta_value=:meta_value,meta_key=:meta_key,
                            companyid=:companyid,locationid=:locationid,description=:description";
                $dao->initCommand($strSql2);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':locationid', CONFIG_LID);
                $dao->addParameter(':meta_key', 'table_booking_settings');
                $dao->addParameter(':description', 'Online Table booking settigs');
                $dao->addParameter(':meta_value', json_encode($table_booking_setting));
                $dao->executeNonQuery();
            }

            return json_encode(array('Success'=>'True','Message'=>'REC_UP_SUC'));
		}
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - editDisplaysettings - '.$e);
        }
    }


    public function getTagrecords()
    {
        try {
            $this->log->logIt($this->module . " - getTagrecords");
            $dao = new \dao();
            $strSql2 = "SELECT meta_value FROM " . CONFIG_DBN . ". cflocationtemplate_meta  WHERE
                            companyid=:companyid AND locationid=:locationid AND meta_key=:meta_key ";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->addParameter(':meta_key', "tags_value");
            $res= $dao->executeRow();

            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getTagrecords - " . $e);
            return false;
        }
    }
    public function getTableSettings()
    {
        try {
            $this->log->logIt($this->module . " - getTableSettings");
            $dao = new \dao();
            $strSql2 = "SELECT meta_value FROM " . CONFIG_DBN . ". cflocationtemplate_meta  WHERE
                            companyid=:companyid AND locationid=:locationid AND meta_key=:meta_key ";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->addParameter(':meta_key', "table_booking_settings");
            $res= $dao->executeRow();

            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getTableSettings - " . $e);
            return false;
        }
    }
    public function getLocationInfo()
    {
        try {
            $this->log->logIt($this->module . " - getLocationInfo");
            $dao = new \dao();
            $strSql2 = "SELECT is_night_audit_enable FROM syslocation  WHERE
                            companyid=:companyid AND syslocationid=:locationid  ";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $res= $dao->executeRow();

            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getLocationInfo - " . $e);
            return false;
        }
    }
}

?>