<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 13/4/18
 * Time: 4:36 PM
 */

namespace database;
class storesettingsdao
{
    public $module = 'DB_displaysettingsdao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function loadStoreDisplaySettings()
    {
        try
        {
            $this->log->logIt($this->module.' - loadStoreDisplaySettings');
            $res['timezone'] = \database\parameter::getParameter('timezone_main');
            $res['dateformat'] = \database\parameter::getParameter('dateformat');
            $res['timeformat'] = \database\parameter::getParameter('timeformat');
            $res['salutation'] = \database\parameter::getParameter('salutation');
            $res['country'] = \database\parameter::getParameter('country');
            $res['nationality'] = \database\parameter::getParameter('nationality');
            $res['roundoff'] = \database\parameter::getParameter('roundoff');
            $res['digitafterdecimal'] = \database\parameter::getParameter('digitafterdecimal');
            $CommonDao = new commondao();
            $country = $CommonDao->getCountryList();
            $arr['countrylist'] = $country;
            $arr['displaysettings'] = $res;
            return json_encode(array("Success"=>"True","Data"=>$arr));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'- loadStoreDisplaySettings - '.$e);
        }
    }
    public function editStoreDisplaySettings($data,$languageArr='')
    {
        try
        {
            $this->log->logIt($this->module.' - editStoreDisplaySettings');
            $timezone4 = substr(urldecode($data['select_timezone']), 4, 6);
            \database\parameter::setParameter('timezone',$timezone4);
            \database\parameter::setParameter('timezone_main',$data['select_timezone']);
            \database\parameter::setParameter('dateformat',$data['select_date_format']);
            \database\parameter::setParameter('timeformat',$data['select_time_format']);
            \database\parameter::setParameter('salutation',$data['select_salutation']);
            \database\parameter::setParameter('country',$data['select_country']);
            \database\parameter::setParameter('nationality',$data['select_nationality']);
            \database\parameter::setParameter('digitafterdecimal',$data['select_decimal_digit']);
            $round = ($data['select_round_type']=='0')?$data['txtlimit']:$data['select_round_type'];
            
            \database\parameter::setParameter('roundoff',$round);
            return json_encode(array('Success'=>'True','Message'=>$languageArr->LANG11));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - editStoreDisplaySettings - '.$e);
        }
    }
}
?>