<?php
namespace database;

class combo_productdao
{
    public $module = 'DB_combo_product';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addComboProduct($res, $languageArr='', $defaultlanguageArr='')
    {
        try {
            $this->log->logIt($this->module . ' - addComboProduct');

            $dao = new \dao();
            $datetime = \util\util::getLocalDateTime();
            $tablename = "cfmenu_combo";
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $ObjDependencyDao = new \database\dependencydao();
            $arr_log = array(
                'Combo Name' => $res['name'],
                'Status' => ($res['rdo_status'] == 1) ? 'Active' : 'Inactive',
                'SKU' => $res['sku'],
                'Long Description' => $res['longdesc'],
            );
            $json_data = json_encode($arr_log);

            if ($res['id'] == 0) {
                $title = "Add Record";
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "comboname", $res['name']);
                if ($chk_name == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $languageArr->LANG26)));
                }
                $chk_scode = $ObjDependencyDao->checkduplicaterecord($tablename, "sku", $res['sku']);
                if ($chk_scode == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $languageArr->LANG47)));
                }

                $hashkey = \util\util::gethash();

                $serveunit = $ObjCommonDao->getprimarykey('cfmenu_itemunit', $res['serve_unit'], 'unitunkid');

                $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu_combo( comboname, 
                                                                    long_desc,
                                                                    image,
                                                                    sku,                                                                  
                                                                    tax_apply_rate,
                                                                    serve_unit, 
                                                                    companyid,
                                                                    locationid, 
                                                                    createddatetime, 
                                                                    created_user, 
                                                                    is_active, 
                                                                    hashkey)
                                                            VALUE(  :comboname,
                                                                    :txtlongscription,
                                                                    :upload_img,
                                                                    :txtsku,                                                                  
                                                                    :rates,
                                                                    :serve_unit,                                                                  
                                                                    :companyid,
                                                                    :locationid, 
                                                                    :createddatetime, 
                                                                    :created_user, 
                                                                    :isactive, 
                                                                    :hashkey)";

                $dao->initCommand($strSql);
                $dao->addParameter(':comboname', $res['name']);
                $dao->addParameter(':txtsku', $res['sku']);
                $dao->addParameter(':txtlongscription', $res['longdesc']);
                $dao->addParameter(':upload_img', ($res['image'] != '' ? $res['image'] : NULL));
                $dao->addParameter(':serve_unit', $serveunit);
                $dao->addParameter(':rates', $res['rates']);
                $dao->addParameter(':isactive', $res['rdo_status']);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->addParameter(':created_user', CONFIG_UID);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();
                $comboid = $dao->getLastInsertedId();

                //Bind unit
                if (count($res['bind_unit']) > 0) {

                    $bind_unit = $res['bind_unit'];
                    foreach ($bind_unit AS $ukey => $sub_unit) {

                        $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu_itemunit_rel(lnkitemid,lnkitemunitid,rate1,rate2,rate3,rate4,default_rate,created_user, createddatetime,recipe_type,companyid,locationid)";
                        $strSql .= " VALUE (:lnkitemid, :lnkitemunitid, :rate1,:rate2,:rate3,:rate4,:defaultrate,:created_user, :createddatetime,:recipe_type,:companyid, :locationid)";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':lnkitemid', $comboid);
                        $dao->addParameter(':lnkitemunitid', $ukey);
                        $dao->addParameter(':rate1', $sub_unit['rate1']);
                        $dao->addParameter(':rate2', $sub_unit['rate2']);
                        $dao->addParameter(':rate3', $sub_unit['rate3']);
                        $dao->addParameter(':rate4', $sub_unit['rate4']);
                        $dao->addParameter(':defaultrate', $sub_unit['defaultrate']);
                        $dao->addParameter(':created_user', CONFIG_UID);
                        $dao->addParameter(':createddatetime', $datetime);
                        $dao->addParameter(':recipe_type', '4');
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->executeNonQuery();
                        $relcombounitid = $dao->getLastInsertedId();

                        // item Groups

                        if (isset($sub_unit['itemGroups']) && count($sub_unit['itemGroups']) > 0) {
                            $bind_groups = $sub_unit['itemGroups'];
                            foreach ($bind_groups AS $val4) {

                                $strSql = " INSERT INTO " . CONFIG_DBN . ".combounit_itemgroups_relation (lnkcombounitid,defaultitemid,itemgroupid,comboid,created_user,createddatetime,companyid,locationid)";
                                $strSql .= " VALUE (:lnkcombounitid,:defaultitemid,:itemgroupid,:comboid, :created_user, :createddatetime, :companyid,:locationid)";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':lnkcombounitid', $relcombounitid);
                                $dao->addParameter(':comboid', $comboid);
                                $dao->addParameter(':defaultitemid', $val4['defaultItem']);
                                $dao->addParameter(':itemgroupid', $val4['itemgroupid']);
                                $dao->addParameter(':created_user', CONFIG_UID);
                                $dao->addParameter(':createddatetime', $datetime);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $dao->executeNonQuery();
                                $relcomboitemgroupid = $dao->getLastInsertedId();

                      // Items

                               if (isset($val4['items']) && count($val4['items']) > 0) {
                                    $bind_item = $val4['items'];

                                    foreach ($bind_item AS $val) {
                                        $strSql = " INSERT INTO " . CONFIG_DBN . ".comboitemgroups_items_relation (lnkcombounitid,itemid,comboid,extracharge,lnkcomboitemgrouprelid,itemgroupid,created_user,createddatetime,companyid,locationid)";
                                        $strSql .= " VALUE (:lnkcombounitid, :itemid, :comboid, :extracharge, :lnkcomboitemgrouprelid,:itemgroupid, :created_user, :createddatetime, :companyid, :locationid)";
                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':lnkcombounitid', $relcombounitid);
                                        $dao->addParameter(':comboid', $comboid);
                                        $dao->addParameter(':itemid', $val['itemid']);
                                        $dao->addParameter(':extracharge', $val['extraCharge']);
                                        $dao->addParameter(':lnkcomboitemgrouprelid', $relcomboitemgroupid);
                                        $dao->addParameter(':itemgroupid', $val4['itemgroupid']);
                                        $dao->addParameter(':created_user', CONFIG_UID);
                                        $dao->addParameter(':createddatetime', $datetime);
                                        $dao->addParameter(':companyid', CONFIG_CID);
                                        $dao->addparameter(':locationid', CONFIG_LID);
                                        $dao->executeNonQuery();
                                        $relcomboitemid = $dao->getLastInsertedId();


                                        if (isset($val['itemunitsdetail']) && count($val['itemunitsdetail']) > 0) {
                                            $bind_itemunit = $val['itemunitsdetail'];

                                            foreach ($bind_itemunit AS $val1) {
                                                $strSql = " INSERT INTO " . CONFIG_DBN . ".comboitem_unit_relation (lnkcombogroup_itemrel,itemunitid,itemid,quantity,is_include,created_user,createddatetime,companyid,locationid)";
                                                $strSql .= " VALUE (:lnkcombogroup_itemrel,:itemunitid,:itemid,:quantity, :is_include, :created_user, :createddatetime, :companyid,:locationid)";
                                                $dao->initCommand($strSql);
                                                $dao->addParameter(':lnkcombogroup_itemrel', $relcomboitemid);
                                                $dao->addParameter(':itemunitid', $val1['itemunit']);
                                                $dao->addParameter(':itemid', $val['itemid']);
                                                $dao->addParameter(':quantity', $val1['itemqty']);
                                                $dao->addParameter(':is_include', isset($val1['modifier']) ? '1' : '0');
                                                $dao->addParameter(':created_user', CONFIG_UID);
                                                $dao->addParameter(':createddatetime', $datetime);
                                                $dao->addParameter(':companyid', CONFIG_CID);
                                                $dao->addparameter(':locationid', CONFIG_LID);
                                                $dao->executeNonQuery();
                                                $relcomboitemunitid = $dao->getLastInsertedId();

                                                // modifier

                                                if (isset($val1['modifier']) && count($val1['modifier']) > 0) {

                                                    $bind_modifier = $val1['modifier'];

                                                    foreach ($bind_modifier AS $val2) {
                                                        $strSql = "INSERT INTO " . CONFIG_DBN . ".comboitem_mod_relation (lnkcomboitem_unitrel,modifierid,modifierunitid, quantity,itemid,itemunitid,is_include , created_user, createddatetime, companyid,locationid,comboid)";
                                                        $strSql .= " VALUE (:lnkcomboitem_unitrel, :modifierid,:modifierunitid, :quantity,:itemid, :itemunitid, :is_include, :created_user, :createddatetime, :companyid,:locationid,:comboid)";
                                                        $dao->initCommand($strSql);
                                                        $dao->addParameter(':lnkcomboitem_unitrel', $relcomboitemunitid);
                                                        $dao->addParameter(':modifierid', $val2['modid']);
                                                        $dao->addParameter(':modifierunitid', isset($val2['mounit']) ? $val2['mounit'] : '0');
                                                        $dao->addParameter(':quantity', isset($val2['moqty']) ? $val2['moqty'] : '0');
                                                        $dao->addParameter(':itemid', $val['itemid']);
                                                        $dao->addParameter(':itemunitid', $val1['itemunit']);
                                                        $dao->addParameter(':is_include', isset($val2['modifieritem']) ? '1' : '0');
                                                        $dao->addParameter(':created_user', CONFIG_UID);
                                                        $dao->addParameter(':createddatetime', $datetime);
                                                        $dao->addParameter(':companyid', CONFIG_CID);
                                                        $dao->addparameter(':locationid', CONFIG_LID);
                                                        $dao->addParameter(':comboid', $comboid);
                                                        $dao->executeNonQuery();
                                                        $relunitmodifier = $dao->getLastInsertedId();

                                                        if (isset($val2['modifieritem']) && count($val2['modifieritem']) > 0) {

                                                            $bind_modifieritem = $val2['modifieritem'];

                                                            foreach ($bind_modifieritem AS $val3) {
                                                                $strSql = "INSERT INTO " . CONFIG_DBN . ".combomod_item_relation (lnkcombomodifierrel,modifieritemid,modifieritemunitid,quantity,modifierid,created_user,createddatetime,companyid,locationid,comboid)";
                                                                $strSql .= " VALUE (:lnkcombomodifierrel, :modifieritemid,:modifieritemunitid, :quantity, :modifierid, :created_user, :createddatetime, :companyid,:locationid,:comboid)";
                                                                $dao->initCommand($strSql);
                                                                $dao->addParameter(':lnkcombomodifierrel', $relunitmodifier);
                                                                $dao->addParameter(':modifieritemid', $val3['moditemid']);
                                                                $dao->addParameter(':modifieritemunitid', $val3['moitemunit']);
                                                                $dao->addParameter(':quantity', $val3['moitemqty']);
                                                                $dao->addParameter(':modifierid', $val2['modid']);
                                                                $dao->addParameter(':created_user', CONFIG_UID);
                                                                $dao->addParameter(':createddatetime', $datetime);
                                                                $dao->addParameter(':companyid', CONFIG_CID);
                                                                $dao->addparameter(':locationid', CONFIG_LID);
                                                                $dao->addParameter(':comboid', $comboid);
                                                                $dao->executeNonQuery();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //Bind Tax
                if (isset($res['bind_tax']) && count($res['bind_tax']) > 0 && $res['bind_tax'] != '') {
                    $bind_tax = $res['bind_tax'];

                    foreach ($bind_tax AS $tkey => $tvalue) {
                        if (isset($tvalue['taxid']) && $tvalue['taxid'] != '') {
                            $strSqlTax = "SELECT postingrule FROM " . CONFIG_DBN . ".cftaxdetail WHERE taxunkid=:taxunkid ORDER BY entrydatetime DESC LIMIT 1";
                            $dao->initCommand($strSqlTax);
                            $dao->addParameter(':taxunkid', $tvalue['taxid']);
                            $tax_pr = $dao->executeRow();

                            $strSql = "INSERT INTO " . CONFIG_DBN . ".cfitemtax_rel(lnkitemid,lnktaxunkid,amount,taxapplyafter,postingrule,item_type,created_user, createddatetime,companyid,locationid)";
                            $strSql .= " VALUE (:lnkitemid, :lnktaxunkid, :amount,:taxapplyafter,:postingrule,:item_type,:created_user, :createddatetime,:companyid, :locationid)";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':lnkitemid', $comboid);
                            $dao->addParameter(':lnktaxunkid', $tkey);
                            $dao->addParameter(':amount', $tvalue['amount']);
                            $dao->addParameter(':taxapplyafter', $tvalue['applytax']);
                            $dao->addParameter(':postingrule', $tax_pr['postingrule']);
                            $dao->addParameter(':item_type', '4');
                            $dao->addParameter(':created_user', CONFIG_UID);
                            $dao->addParameter(':createddatetime', $datetime);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $dao->executeNonQuery();
                        }
                    }
                }


                //Add Item
                //              $itemlist = $res['itemdata'];
//                foreach($itemlist as $value)
//                {
//                    $qtyarray = explode('-&gt;',$value);
//                    $item = $qtyarray[0];
//                    $quantity = $qtyarray[1];
//                    $menuitemid = $ObjCommonDao->getprimarykey('cfmenu_items',$item,'itemunkid');
//
//                    $strSql = "INSERT INTO " . CONFIG_DBN . ".cfcombo_item_rel (lnkcomboid,lnkitemid,quantity, created_user, createddatetime, companyid,locationid)";
//                    $strSql .= " VALUE (:lnkcomboid, :lnkitemid, :quantity, :created_user, :createddatetime, :companyid, :locationid)";
//                    $dao->initCommand($strSql);
//                    $dao->addParameter(':lnkcomboid', $comboid);
//                    $dao->addParameter(':lnkitemid', $menuitemid);
//                    $dao->addParameter(':quantity', $quantity);
//                    $dao->addParameter(':created_user', CONFIG_UID);
//                    $dao->addParameter(':createddatetime', $datetime);
//                    $dao->addParameter(':companyid', CONFIG_CID);
//                    $dao->addparameter(':locationid', CONFIG_LID);
//                    $dao->executeNonQuery();
//                }


                $ObjAuditDao->addactivitylog($res['module'], $title, $hashkey, $json_data);

                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_ADD_SUC)));
            } else {
                $title = "Edit Record";

                $serveunit = $ObjCommonDao->getprimarykey('cfmenu_itemunit', $res['serve_unit'], 'unitunkid');

                $id = $ObjCommonDao->getprimarykey($tablename, $res['id'], 'combounkid');

                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "comboname", $res['name'], $id);

                if ($chk_name == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $languageArr->LANG26)));
                }
                $chk_scode = $ObjDependencyDao->checkduplicaterecord($tablename, "sku", $res['sku'], $id);
                if ($chk_scode == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $languageArr->LANG47)));
                }

                $strSql = "UPDATE " . CONFIG_DBN . ".cfmenu_combo SET comboname=:comboname, 
                           sku=:txtsku,long_desc=:txtlongdescription,
                           image=:upload_img,serve_unit=:serve_unit,modifieddatetime=:modifieddatetime,
                           modified_user=:modified_user,tax_apply_rate=:rates
                           WHERE hashkey=:combounkid AND companyid=:companyid AND locationid=:locationid";

                $dao->initCommand($strSql);
                $dao->addParameter(':combounkid',$res['id']);
                $dao->addParameter(':comboname',$res['name']);
                $dao->addParameter(':txtsku',$res['sku']);
                $dao->addParameter(':txtlongdescription',$res['longdesc']);
                $dao->addParameter(':upload_img', ($res['image'] != '' ? $res['image'] : NULL));
                $dao->addParameter(':serve_unit',$serveunit);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':rates',$res['rates']);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();

                $comboid = $ObjCommonDao->getprimarykey('cfmenu_combo',$res['id'],'combounkid');
                //Bind unit
                if (count($res['bind_unit']) > 0) {

                    $strSql = "SELECT itmunitunkid FROM " . CONFIG_DBN . ".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':lnkitemid', $comboid);
                    $dao->addParameter(':recipe_type', '4');
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $particuler_unit = $dao->executeQuery();

                    $unit_arr = \util\util::getoneDarray($particuler_unit, 'itmunitunkid');

                    $bind_unit = $res['bind_unit'];

                        foreach ($bind_unit AS $ukey => $sub_unit) {

                            $strSql = "SELECT itmunitunkid FROM " . CONFIG_DBN . ".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND recipe_type=:recipe_type AND lnkitemunitid=:lnkitemunitid AND companyid=:companyid AND locationid=:locationid";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':lnkitemid', $comboid);
                            $dao->addParameter(':recipe_type', '4');
                            $dao->addParameter(':lnkitemunitid', $ukey);
                            $dao->addParameter(':companyid', CONFIG_CID);
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $particuler_unit = $dao->executeRow();

                            if (in_array($particuler_unit['itmunitunkid'], $unit_arr)) {
                                $strSql = "UPDATE " . CONFIG_DBN . ".cfmenu_itemunit_rel SET lnkitemid=:lnkitemid, lnkitemunitid=:lnkitemunitid, rate1=:rate1, rate2=:rate2,rate3=:rate3,rate4=:rate4,
                                           default_rate=:defaultrate,modified_user=:modified_user, modifieddatetime=:modifieddatetime WHERE recipe_type=:recipe_type AND companyid=:companyid AND itmunitunkid=:itmunitunkid
                                           AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':itmunitunkid', $particuler_unit['itmunitunkid']);
                                $dao->addParameter(':lnkitemid', $comboid);
                                $dao->addParameter(':lnkitemunitid', $ukey);
                                $dao->addParameter(':rate1', $sub_unit['rate1']);
                                $dao->addParameter(':rate2', $sub_unit['rate2']);
                                $dao->addParameter(':rate3', $sub_unit['rate3']);
                                $dao->addParameter(':rate4', $sub_unit['rate4']);
                                $dao->addParameter(':defaultrate', $sub_unit['defaultrate']);
                                $dao->addParameter(':modified_user', CONFIG_UID);
                                $dao->addParameter(':modifieddatetime', $datetime);
                                $dao->addParameter(':recipe_type', '4');
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $dao->executeNonQuery();
                                $relcombounitid = $particuler_unit['itmunitunkid'];
                                if (($ukey = array_search($particuler_unit['itmunitunkid'], $unit_arr)) !== false) {
                                    unset($unit_arr[$ukey]);
                                }

                            } else {
                                $strSql = "INSERT INTO " . CONFIG_DBN . ".cfmenu_itemunit_rel(lnkitemid,lnkitemunitid,rate1,rate2,rate3,rate4,default_rate,created_user, createddatetime,recipe_type,companyid,locationid)";
                                $strSql .= " VALUE (:lnkitemid, :lnkitemunitid, :rate1,:rate2,:rate3,:rate4,:defaultrate,:created_user, :createddatetime,:recipe_type,:companyid, :locationid)";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':lnkitemid', $comboid);
                                $dao->addParameter(':lnkitemunitid', $ukey);
                                $dao->addParameter(':rate1', $sub_unit['rate1']);
                                $dao->addParameter(':rate2', $sub_unit['rate2']);
                                $dao->addParameter(':rate3', $sub_unit['rate3']);
                                $dao->addParameter(':rate4', $sub_unit['rate4']);
                                $dao->addParameter(':defaultrate', $sub_unit['defaultrate']);
                                $dao->addParameter(':created_user', CONFIG_UID);
                                $dao->addParameter(':createddatetime', $datetime);
                                $dao->addParameter(':recipe_type', '4');
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $dao->executeNonQuery();
                                $relcombounitid = $dao->getLastInsertedId();
                            }


                            if (isset($sub_unit['itemGroups']) && count($sub_unit['itemGroups']) > 0) {

                                $strSql = " SELECT 	unititemgrouprelationunkid FROM " . CONFIG_DBN . ".combounit_itemgroups_relation WHERE comboid=:comboid AND lnkcombounitid=:lnkcombounitid AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':comboid', $comboid);
                                $dao->addParameter(':lnkcombounitid', $relcombounitid);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $itemgroups = $dao->executeQuery();
                                $itemgroups_arr = \util\util::getoneDarray($itemgroups, 'unititemgrouprelationunkid');

                                $bind_groups = $sub_unit['itemGroups'];

                                foreach ($bind_groups AS $val4) {

                                    $itemgroupid = $val4['itemgroupid'];

                                    $strSql = "SELECT unititemgrouprelationunkid FROM " . CONFIG_DBN . ".combounit_itemgroups_relation  WHERE itemgroupid=:itemgroupid AND comboid=:comboid AND lnkcombounitid=:lnkcombounitid AND companyid=:companyid AND locationid=:locationid";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':itemgroupid', $itemgroupid);
                                    $dao->addParameter(':comboid', $comboid);
                                    $dao->addParameter(':lnkcombounitid', $relcombounitid);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $perticuler_itemgroup = $dao->executeRow();

                                    if (in_array($perticuler_itemgroup['unititemgrouprelationunkid'], $itemgroups_arr)) {
                                        $strSql = " UPDATE " . CONFIG_DBN . ".combounit_itemgroups_relation SET defaultitemid=:defaultitemid,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND unititemgrouprelationunkid=:unititemgrouprelationunkid AND locationid=:locationid";

                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':unititemgrouprelationunkid', $perticuler_itemgroup['unititemgrouprelationunkid']);
                                        $dao->addParameter(':modified_user', CONFIG_UID);
                                        $dao->addParameter(':defaultitemid', $val4['defaultItem']);
                                        $dao->addParameter(':modifieddatetime', $datetime);
                                        $dao->addParameter(':companyid', CONFIG_CID);
                                        $dao->addparameter(':locationid', CONFIG_LID);
                                        $dao->executeNonQuery();

                                        $relcomboitemgroupid = $perticuler_itemgroup['unititemgrouprelationunkid'];
                                        if (($key = array_search($relcomboitemgroupid, $itemgroups_arr)) !== false) {
                                            unset($itemgroups_arr[$key]);
                                        }
                                    } else {
                                        $strSql = " INSERT INTO " . CONFIG_DBN . ".combounit_itemgroups_relation (lnkcombounitid,defaultitemid,itemgroupid,comboid,created_user,createddatetime,companyid,locationid)";
                                        $strSql .= " VALUE (:lnkcombounitid,:defaultitemid,:itemgroupid,:comboid, :created_user, :createddatetime, :companyid,:locationid)";
                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':lnkcombounitid', $relcombounitid);
                                        $dao->addParameter(':comboid', $comboid);
                                        $dao->addParameter(':defaultitemid', $val4['defaultItem']);
                                        $dao->addParameter(':itemgroupid', $val4['itemgroupid']);
                                        $dao->addParameter(':created_user', CONFIG_UID);
                                        $dao->addParameter(':createddatetime', $datetime);
                                        $dao->addParameter(':companyid', CONFIG_CID);
                                        $dao->addparameter(':locationid', CONFIG_LID);
                                        $dao->executeNonQuery();
                                        $relcomboitemgroupid = $dao->getLastInsertedId();
                                    }

                                    // item

                                    if (isset($val4['items']) && count($val4['items']) > 0) {

                                        $strSql = " SELECT groupitemrelationunkid FROM " . CONFIG_DBN . ".comboitemgroups_items_relation WHERE comboid=:comboid AND lnkcombounitid=:lnkcombounitid AND itemgroupid=:itemgroupid AND lnkcomboitemgrouprelid=:lnkcomboitemgrouprelid AND companyid=:companyid AND locationid=:locationid";
                                        $dao->initCommand($strSql);
                                        $dao->addParameter(':comboid', $comboid);
                                        $dao->addParameter(':lnkcombounitid', $relcombounitid);
                                        $dao->addParameter(':lnkcomboitemgrouprelid', $relcomboitemgroupid);
                                        $dao->addParameter(':itemgroupid', $val4['itemgroupid']);
                                        $dao->addParameter(':companyid', CONFIG_CID);
                                        $dao->addparameter(':locationid', CONFIG_LID);
                                        $items = $dao->executeQuery();
                                        $items_arr = \util\util::getoneDarray($items, 'groupitemrelationunkid');

                                        $bind_item = $val4['items'];

                                        foreach ($bind_item AS $val) {

                                            $itemid = $val['itemid'];
                                            $strSql = "SELECT groupitemrelationunkid FROM " . CONFIG_DBN . ".comboitemgroups_items_relation  WHERE itemid=:itemid AND comboid=:comboid AND lnkcombounitid=:lnkcombounitid AND itemgroupid=:itemgroupid AND lnkcomboitemgrouprelid=:lnkcomboitemgrouprelid AND companyid=:companyid AND locationid=:locationid";
                                            $dao->initCommand($strSql);
                                            $dao->addParameter(':itemid', $itemid);
                                            $dao->addParameter(':comboid', $comboid);
                                            $dao->addParameter(':lnkcombounitid', $relcombounitid);
                                            $dao->addParameter(':lnkcomboitemgrouprelid', $relcomboitemgroupid);
                                            $dao->addParameter(':itemgroupid', $val4['itemgroupid']);
                                            $dao->addParameter(':companyid', CONFIG_CID);
                                            $dao->addparameter(':locationid', CONFIG_LID);
                                            $perticuler_item = $dao->executeRow();

                                            if (in_array($perticuler_item['groupitemrelationunkid'], $items_arr)) {
                                                $strSql = " UPDATE " . CONFIG_DBN . ".comboitemgroups_items_relation SET extracharge=:extracharge,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND groupitemrelationunkid=:groupitemrelationunkid AND locationid=:locationid";

                                                $dao->initCommand($strSql);
                                                $dao->addParameter(':groupitemrelationunkid', $perticuler_item['groupitemrelationunkid']);
                                                $dao->addParameter(':extracharge', $val['extraCharge']);
                                                $dao->addParameter(':modified_user', CONFIG_UID);
                                                $dao->addParameter(':modifieddatetime', $datetime);
                                                $dao->addParameter(':companyid', CONFIG_CID);
                                                $dao->addparameter(':locationid', CONFIG_LID);
                                                $dao->executeNonQuery();

                                                $relcomboitemid = $perticuler_item['groupitemrelationunkid'];
                                                if (($key = array_search($relcomboitemid, $items_arr)) !== false) {
                                                    unset($items_arr[$key]);
                                                }
                                            } else {
                                                $strSql = " INSERT INTO " . CONFIG_DBN . ".comboitemgroups_items_relation (lnkcombounitid,itemid,comboid,extracharge,lnkcomboitemgrouprelid,itemgroupid,created_user,createddatetime,companyid,locationid)";
                                                $strSql .= " VALUE (:lnkcombounitid, :itemid, :comboid, :extracharge, :lnkcomboitemgrouprelid,:itemgroupid, :created_user, :createddatetime, :companyid, :locationid)";
                                                $dao->initCommand($strSql);
                                                $dao->addParameter(':lnkcombounitid', $relcombounitid);
                                                $dao->addParameter(':comboid', $comboid);
                                                $dao->addParameter(':itemid', $val['itemid']);
                                                $dao->addParameter(':extracharge', $val['extraCharge']);
                                                $dao->addParameter(':lnkcomboitemgrouprelid', $relcomboitemgroupid);
                                                $dao->addParameter(':itemgroupid', $val4['itemgroupid']);
                                                $dao->addParameter(':created_user', CONFIG_UID);
                                                $dao->addParameter(':createddatetime', $datetime);
                                                $dao->addParameter(':companyid', CONFIG_CID);
                                                $dao->addparameter(':locationid', CONFIG_LID);
                                                $dao->executeNonQuery();
                                                $relcomboitemid = $dao->getLastInsertedId();
                                            }

                                            if (isset($val['itemunitsdetail']) && count($val['itemunitsdetail']) > 0) {

                                                $strSql = "SELECT itemunitrelationunkid FROM " . CONFIG_DBN . ".comboitem_unit_relation WHERE itemid=:itemid AND lnkcombogroup_itemrel=:lnkcombogroup_itemrel AND companyid=:companyid AND locationid=:locationid";
                                                $dao->initCommand($strSql);
                                                $dao->addParameter(':itemid', $itemid);
                                                $dao->addParameter(':lnkcombogroup_itemrel', $relcomboitemid);
                                                $dao->addParameter(':companyid', CONFIG_CID);
                                                $dao->addparameter(':locationid', CONFIG_LID);
                                                $perticuler_itemunit = $dao->executeQuery();

                                                $itemsunit_arr = \util\util::getoneDarray($perticuler_itemunit, 'itemunitrelationunkid');
                                                $bind_itemunit = $val['itemunitsdetail'];

                                                foreach ($bind_itemunit AS $val1) {

                                                    $itemunitid = $val1['itemunit'];
                                                    $strSql = "SELECT itemunitrelationunkid FROM " . CONFIG_DBN . ".comboitem_unit_relation WHERE itemid=:itemid AND lnkcombogroup_itemrel=:lnkcombogroup_itemrel  AND itemunitid=:itemunitid AND companyid=:companyid AND locationid=:locationid";
                                                    $dao->initCommand($strSql);
                                                    $dao->addParameter(':itemid', $itemid);
                                                    $dao->addParameter(':lnkcombogroup_itemrel', $relcomboitemid);
                                                    $dao->addParameter(':itemunitid', $itemunitid);
                                                    $dao->addParameter(':companyid', CONFIG_CID);
                                                    $dao->addparameter(':locationid', CONFIG_LID);
                                                    $perticuler_itemunit = $dao->executeRow();


                                                    if (in_array($perticuler_itemunit['itemunitrelationunkid'], $itemsunit_arr)) {
                                                        $strSql = " UPDATE " . CONFIG_DBN . ".comboitem_unit_relation SET quantity=:quantity,is_include=:is_include,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND itemunitrelationunkid=:itemunitrelationunkid AND locationid=:locationid";

                                                        $dao->initCommand($strSql);
                                                        $dao->addParameter(':itemunitrelationunkid', $perticuler_itemunit['itemunitrelationunkid']);
                                                        $dao->addParameter(':quantity', $val1['itemqty']);
                                                        $dao->addParameter(':is_include', isset($val1['modifier']) ? '1' : '0');
                                                        $dao->addParameter(':modified_user', CONFIG_UID);
                                                        $dao->addParameter(':modifieddatetime', $datetime);
                                                        $dao->addParameter(':companyid', CONFIG_CID);
                                                        $dao->addparameter(':locationid', CONFIG_LID);
                                                        $dao->executeNonQuery();

                                                        $relcomboitemunitid = $perticuler_itemunit['itemunitrelationunkid'];
                                                        if (($key = array_search($relcomboitemunitid, $itemsunit_arr)) !== false) {
                                                            unset($itemsunit_arr[$key]);
                                                        }

                                                    } else {
                                                        $strSql = " INSERT INTO " . CONFIG_DBN . ".comboitem_unit_relation (lnkcombogroup_itemrel,itemunitid,itemid,quantity,is_include,created_user,createddatetime,companyid,locationid)";
                                                        $strSql .= " VALUE (:lnkcombogroup_itemrel,:itemunitid,:itemid,:quantity, :is_include, :created_user, :createddatetime, :companyid,:locationid)";
                                                        $dao->initCommand($strSql);
                                                        $dao->addParameter(':lnkcombogroup_itemrel', $relcomboitemid);
                                                        $dao->addParameter(':itemunitid', $itemunitid);
                                                        $dao->addParameter(':itemid', $val['itemid']);
                                                        $dao->addParameter(':quantity', $val1['itemqty']);
                                                        $dao->addParameter(':is_include', isset($val1['modifier']) ? '1' : '0');
                                                        $dao->addParameter(':created_user', CONFIG_UID);
                                                        $dao->addParameter(':createddatetime', $datetime);
                                                        $dao->addParameter(':companyid', CONFIG_CID);
                                                        $dao->addparameter(':locationid', CONFIG_LID);
                                                        $dao->executeNonQuery();
                                                        $relcomboitemunitid = $dao->getLastInsertedId();
                                                    }
                                                    // modifier

                                                    if (isset($val1['modifier']) && count($val1['modifier']) > 0) {

                                                        $strSql = "SELECT itemmodrelationunkid FROM " . CONFIG_DBN . ".comboitem_mod_relation WHERE itemid=:itemid AND lnkcomboitem_unitrel=:lnkcomboitem_unitrel  AND itemunitid=:itemunitid AND companyid=:companyid AND locationid=:locationid";
                                                        $dao->initCommand($strSql);
                                                        $dao->addParameter(':itemid', $itemid);
                                                        $dao->addParameter(':itemunitid', $itemunitid);
                                                        $dao->addParameter(':lnkcomboitem_unitrel', $relcomboitemunitid);
                                                        $dao->addParameter(':companyid', CONFIG_CID);
                                                        $dao->addparameter(':locationid', CONFIG_LID);
                                                        $perticuler_mod = $dao->executeQuery();

                                                        $mod_arr = \util\util::getoneDarray($perticuler_mod, 'itemmodrelationunkid');

                                                        $bind_modifier = $val1['modifier'];

                                                        if (count($bind_modifier) > 0) {

                                                            foreach ($bind_modifier AS $val2) {

                                                                $modifierid = $val2['modid'];

                                                                $strSql = "SELECT itemmodrelationunkid FROM " . CONFIG_DBN . ".comboitem_mod_relation WHERE itemid=:itemid AND lnkcomboitem_unitrel=:lnkcomboitem_unitrel  AND itemunitid=:itemunitid AND modifierid=:modifierid AND companyid=:companyid AND locationid=:locationid";
                                                                $dao->initCommand($strSql);
                                                                $dao->addParameter(':itemid', $itemid);
                                                                $dao->addParameter(':itemunitid', $itemunitid);
                                                                $dao->addParameter(':lnkcomboitem_unitrel', $relcomboitemunitid);
                                                                $dao->addParameter(':modifierid', $modifierid);
                                                                $dao->addParameter(':companyid', CONFIG_CID);
                                                                $dao->addparameter(':locationid', CONFIG_LID);
                                                                $perticuler_mod = $dao->executeRow();

                                                                if (in_array($perticuler_mod['itemmodrelationunkid'], $mod_arr)) {
                                                                    $strSql = " UPDATE " . CONFIG_DBN . ".comboitem_mod_relation SET modifierunitid=:modifierunitid,quantity=:quantity,is_include=:is_include,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND itemmodrelationunkid=:itemmodrelationunkid AND locationid=:locationid";

                                                                    $dao->initCommand($strSql);
                                                                    $dao->addParameter(':itemmodrelationunkid', $perticuler_mod['itemmodrelationunkid']);
                                                                    $dao->addParameter(':quantity', isset($val2['moqty']) ? $val2['moqty'] : '0');
                                                                    $dao->addParameter(':modifierunitid', isset($val2['mounit']) ? $val2['mounit'] : '0');
                                                                    $dao->addParameter(':is_include', isset($val2['modifieritem']) ? '1' : '0');
                                                                    $dao->addParameter(':modified_user', CONFIG_UID);
                                                                    $dao->addParameter(':modifieddatetime', $datetime);
                                                                    $dao->addParameter(':companyid', CONFIG_CID);
                                                                    $dao->addparameter(':locationid', CONFIG_LID);
                                                                    $dao->executeNonQuery();

                                                                    $relunitmodifier = $perticuler_mod['itemmodrelationunkid'];
                                                                    if (($key = array_search($relunitmodifier, $mod_arr)) !== false) {
                                                                        unset($mod_arr[$key]);
                                                                    }
                                                                } else {
                                                                    $strSql = "INSERT INTO " . CONFIG_DBN . ".comboitem_mod_relation (lnkcomboitem_unitrel,modifierid,modifierunitid, quantity,itemid,itemunitid,is_include , created_user, createddatetime, companyid,locationid)";
                                                                    $strSql .= " VALUE (:lnkcomboitem_unitrel, :modifierid,:modifierunitid, :quantity,:itemid, :itemunitid, :is_include, :created_user, :createddatetime, :companyid,:locationid)";
                                                                    $dao->initCommand($strSql);
                                                                    $dao->addParameter(':lnkcomboitem_unitrel', $relcomboitemunitid);
                                                                    $dao->addParameter(':modifierid', $val2['modid']);
                                                                    $dao->addParameter(':modifierunitid', isset($val2['mounit']) ? $val2['mounit'] : '0');
                                                                    $dao->addParameter(':quantity', isset($val2['moqty']) ? $val2['moqty'] : '0');
                                                                    $dao->addParameter(':itemid', $val['itemid']);
                                                                    $dao->addParameter(':itemunitid', $val1['itemunit']);
                                                                    $dao->addParameter(':is_include', isset($val2['modifieritem']) ? '1' : '0');
                                                                    $dao->addParameter(':created_user', CONFIG_UID);
                                                                    $dao->addParameter(':createddatetime', $datetime);
                                                                    $dao->addParameter(':companyid', CONFIG_CID);
                                                                    $dao->addparameter(':locationid', CONFIG_LID);
                                                                    $dao->executeNonQuery();
                                                                    $relunitmodifier = $dao->getLastInsertedId();
                                                                }

                                                                if (isset($val2['modifieritem']) && count($val2['modifieritem']) > 0) {

                                                                    $strSql = "SELECT moditemrelationunkid  FROM " . CONFIG_DBN . ".combomod_item_relation WHERE modifierid=:modifierid AND lnkcombomodifierrel=:lnkcombomodifierrel AND companyid=:companyid AND locationid=:locationid";
                                                                    $dao->initCommand($strSql);
                                                                    $dao->addParameter(':modifierid', $modifierid);
                                                                    $dao->addParameter(':lnkcombomodifierrel', $relunitmodifier);
                                                                    $dao->addParameter(':companyid', CONFIG_CID);
                                                                    $dao->addparameter(':locationid', CONFIG_LID);
                                                                    $perticuler_moditem = $dao->executeQuery();

                                                                    $moditem_arr = \util\util::getoneDarray($perticuler_moditem, 'moditemrelationunkid');

                                                                    $bind_modifieritem = $val2['modifieritem'];

                                                                    foreach ($bind_modifieritem AS $val3) {

                                                                        $modifieritemid = $val3['moditemid'];

                                                                        $strSql = "SELECT moditemrelationunkid FROM " . CONFIG_DBN . ".combomod_item_relation WHERE modifierid=:modifierid AND lnkcombomodifierrel=:lnkcombomodifierrel AND modifieritemid=:modifieritemid AND companyid=:companyid AND locationid=:locationid";
                                                                        $dao->initCommand($strSql);
                                                                        $dao->addParameter(':modifierid', $modifierid);
                                                                        $dao->addParameter(':lnkcombomodifierrel', $relunitmodifier);
                                                                        $dao->addParameter(':modifieritemid', $modifieritemid);
                                                                        $dao->addParameter(':companyid', CONFIG_CID);
                                                                        $dao->addparameter(':locationid', CONFIG_LID);
                                                                        $perticuler_moditem = $dao->executeRow();

                                                                        if (in_array($perticuler_moditem['moditemrelationunkid'], $moditem_arr)) {
                                                                            $strSql = " UPDATE " . CONFIG_DBN . ".combomod_item_relation SET modifieritemunitid=:modifieritemunitid,quantity=:quantity,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND moditemrelationunkid=:moditemrelationunkid AND locationid=:locationid";

                                                                            $dao->initCommand($strSql);
                                                                            $dao->addParameter(':moditemrelationunkid', $perticuler_moditem['moditemrelationunkid']);
                                                                            $dao->addParameter(':modifieritemunitid', $val3['moitemunit']);
                                                                            $dao->addParameter(':quantity', $val3['moitemqty']);
                                                                            $dao->addParameter(':modified_user', CONFIG_UID);
                                                                            $dao->addParameter(':modifieddatetime', $datetime);
                                                                            $dao->addParameter(':companyid', CONFIG_CID);
                                                                            $dao->addparameter(':locationid', CONFIG_LID);
                                                                            $dao->executeNonQuery();

                                                                            $relunitmodifieritem = $perticuler_moditem['moditemrelationunkid'];
                                                                            if (($key = array_search($relunitmodifieritem, $moditem_arr)) !== false) {
                                                                                unset($moditem_arr[$key]);
                                                                            }
                                                                        } else {
                                                                            $strSql = "INSERT INTO " . CONFIG_DBN . ".combomod_item_relation (lnkcombomodifierrel,modifieritemid,modifieritemunitid,quantity,modifierid,created_user,createddatetime,companyid,locationid)";
                                                                            $strSql .= " VALUE (:lnkcombomodifierrel, :modifieritemid,:modifieritemunitid, :quantity, :modifierid, :created_user, :createddatetime, :companyid,:locationid)";
                                                                            $dao->initCommand($strSql);
                                                                            $dao->addParameter(':lnkcombomodifierrel', $relunitmodifier);
                                                                            $dao->addParameter(':modifieritemid', $val3['moditemid']);
                                                                            $dao->addParameter(':modifieritemunitid', $val3['moitemunit']);
                                                                            $dao->addParameter(':quantity', $val3['moitemqty']);
                                                                            $dao->addParameter(':modifierid', $val2['modid']);
                                                                            $dao->addParameter(':created_user', CONFIG_UID);
                                                                            $dao->addParameter(':createddatetime', $datetime);
                                                                            $dao->addParameter(':companyid', CONFIG_CID);
                                                                            $dao->addparameter(':locationid', CONFIG_LID);
                                                                            $dao->executeNonQuery();
                                                                        }

                                                                    }

                                                                    if (count($moditem_arr) > 0) {
                                                                        $strid = implode(",", $moditem_arr);
                                                                        $strSql = "DELETE FROM " . CONFIG_DBN . ".combomod_item_relation WHERE modifierid=:modifierid AND moditemrelationunkid IN (" . $strid . ") AND companyid=:companyid AND locationid=:locationid";
                                                                        $dao->initCommand($strSql);
                                                                        $dao->addParameter(':modifierid', $modifierid);
                                                                        $dao->addParameter(':companyid', CONFIG_CID);
                                                                        $dao->addparameter(':locationid', CONFIG_LID);
                                                                        $dao->executeNonQuery();
                                                                    }
                                                                }
                                                            }
                                                            if (count($mod_arr) > 0) {
                                                                $strid = implode(",", $mod_arr);

                                                                $strSql = "DELETE FROM " . CONFIG_DBN . ".comboitem_mod_relation WHERE itemid=:itemid AND itemunitid=:itemunitid AND itemmodrelationunkid IN (" . $strid . ") AND companyid=:companyid AND locationid=:locationid";
                                                                $dao->initCommand($strSql);
                                                                $dao->addParameter(':itemid', $itemid);
                                                                $dao->addParameter(':itemunitid', $itemunitid);
                                                                $dao->addParameter(':companyid', CONFIG_CID);
                                                                $dao->addparameter(':locationid', CONFIG_LID);
                                                                $dao->executeNonQuery();

                                                                $strSql = "DELETE FROM " . CONFIG_DBN . ".combomod_item_relation WHERE lnkcombomodifierrel IN (" . $strid . ") AND companyid=:companyid AND locationid=:locationid";
                                                                $dao->initCommand($strSql);
                                                                $dao->addParameter(':companyid', CONFIG_CID);
                                                                $dao->addparameter(':locationid', CONFIG_LID);
                                                                $dao->executeNonQuery();
                                                            }
                                                        } else {

                                                            $strSql = "SELECT itemmodrelationunkid FROM " . CONFIG_DBN . ".comboitem_mod_relation WHERE itemid=:itemid AND lnkcomboitem_unitrel=:lnkcomboitem_unitrel  AND itemunitid=:itemunitid AND companyid=:companyid AND locationid=:locationid";
                                                            $dao->initCommand($strSql);
                                                            $dao->addParameter(':itemid', $itemid);
                                                            $dao->addParameter(':itemunitid', $itemunitid);
                                                            $dao->addParameter(':lnkcomboitem_unitrel', $relcomboitemunitid);
                                                            $dao->addParameter(':companyid', CONFIG_CID);
                                                            $dao->addparameter(':locationid', CONFIG_LID);
                                                            $perticuler_mod = $dao->executeQuery();

                                                            $mod_arr = \util\util::getoneDarray($perticuler_mod, 'itemmodrelationunkid');

                                                            $strid = implode(",", $mod_arr);

                                                            $strSql = "DELETE FROM " . CONFIG_DBN . ".comboitem_mod_relation WHERE itemid=:itemid AND itemunitid=:itemunitid AND itemmodrelationunkid IN (" . $strid . ") AND companyid=:companyid AND locationid=:locationid";
                                                            $dao->initCommand($strSql);
                                                            $dao->addParameter(':itemid', $itemid);
                                                            $dao->addParameter(':itemunitid', $itemunitid);
                                                            $dao->addParameter(':companyid', CONFIG_CID);
                                                            $dao->addparameter(':locationid', CONFIG_LID);
                                                            $dao->executeNonQuery();

                                                            $strSql = "DELETE FROM " . CONFIG_DBN . ".combomod_item_relation WHERE companyid=:companyid  AND lnkcombomodifierrel IN (" . $strid . ") AND locationid=:locationid";
                                                            $dao->initCommand($strSql);
                                                            $dao->addParameter(':companyid', CONFIG_CID);
                                                            $dao->addparameter(':locationid', CONFIG_LID);
                                                            $dao->executeNonQuery();

                                                        }
                                                    }
                                                }
                                                if (count($itemsunit_arr) > 0) {
                                                    $strid = implode(",", $itemsunit_arr);
                                                    $strSql = "DELETE FROM " . CONFIG_DBN . ".comboitem_unit_relation WHERE itemid=:itemid AND itemunitrelationunkid IN (" . $strid . ") AND companyid=:companyid AND locationid=:locationid";
                                                    $dao->initCommand($strSql);
                                                    $dao->addParameter(':itemid', $itemid);
                                                    $dao->addParameter(':companyid', CONFIG_CID);
                                                    $dao->addparameter(':locationid', CONFIG_LID);
                                                    $dao->executeNonQuery();
                                                }
                                            }
                                        }
                                        if (count($items_arr) > 0) {
                                            $strid = implode(",", $items_arr);
                                            $strSql = "DELETE FROM " . CONFIG_DBN . ".comboitemgroups_items_relation WHERE comboid=:comboid AND lnkcombounitid=:lnkcombounitid AND unititemgrouprelationunkid IN (" . $strid . ") AND companyid=:companyid AND locationid=:locationid AND itemgroupid=:itemgroupid AND lnkcomboitemgrouprelid=:lnkcomboitemgrouprelid";
                                            $dao->initCommand($strSql);
                                            $dao->addParameter(':comboid', $comboid);
                                            $dao->addParameter(':lnkcombounitid', $relcombounitid);
                                            $dao->addParameter(':lnkcomboitemgrouprelid', $relcomboitemgroupid);
                                            $dao->addParameter(':itemgroupid', $val4['itemgroupid']);
                                            $dao->addParameter(':companyid', CONFIG_CID);
                                            $dao->addparameter(':locationid', CONFIG_LID);
                                            $dao->executeNonQuery();
                                        }
                                    }


                                }

                                if (count($itemgroups_arr) > 0) {
                                    $strid = implode(",", $itemgroups_arr);

                                    $strSql = "DELETE FROM " . CONFIG_DBN . ".combounit_itemgroups_relation WHERE comboid=:comboid AND lnkcombounitid=:lnkcombounitid AND unititemgrouprelationunkid IN (" . $strid . ") AND companyid=:companyid AND locationid=:locationid ";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':comboid', $comboid);
                                    $dao->addParameter(':lnkcombounitid', $relcombounitid);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $dao->executeNonQuery();

                                    $strSql = "DELETE FROM " . CONFIG_DBN . ".comboitemgroups_items_relation WHERE comboid=:comboid AND lnkcombounitid=:lnkcombounitid AND lnkcomboitemgrouprelid IN (" . $strid . ") AND companyid=:companyid AND locationid=:locationid ";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':comboid', $comboid);
                                    $dao->addParameter(':lnkcombounitid', $relcombounitid);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $dao->executeNonQuery();
                                }
                            }
                        }

                    if(count($unit_arr)>0){
                        $strid = implode(",",$unit_arr);
                        $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND itmunitunkid IN (".$strid.") AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':lnkitemid',$comboid);
                        $dao->addParameter(':recipe_type', '4');
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $dao->addparameter(':locationid',CONFIG_LID);
                        $dao->executeNonQuery();
                     }
                }
                else
                {
                    //Delete From Item- Unit
                    $strSql = "DELETE FROM ".CONFIG_DBN.".cfmenu_itemunit_rel WHERE lnkitemid=:lnkitemid AND recipe_type=:recipe_type AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":lnkitemid",$comboid);
                    $dao->addParameter(':recipe_type', '4');
                    $dao->addParameter(":companyid",CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $dao->executeNonQuery();
                    //Delete From Item- Unit

                }

                //Bind Tax
                if(isset($res['bind_tax']) && count($res['bind_tax'])>0 && $res['bind_tax']!='') {
                    $strSql = "SELECT itmtaxrelid FROM ".CONFIG_DBN.".cfitemtax_rel WHERE lnkitemid=:lnkitemid AND item_type=:item_type AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':lnkitemid',$comboid);
                    $dao->addParameter(':item_type', '4');
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $particuler_tax = $dao->executeQuery();

                    $tax_arr = \util\util::getoneDarray($particuler_tax,'itmtaxrelid');
                    $bind_tax = $res['bind_tax'];

                    if(count($bind_tax)>0) {
                        foreach ($bind_tax AS $tkey => $tvalue) {
                            if (isset($tvalue['taxid']) && $tvalue['taxid'] != '') {

                                $strSqlTax = "SELECT postingrule FROM ".CONFIG_DBN.".cftaxdetail WHERE taxunkid=:taxunkid  ORDER BY entrydatetime DESC LIMIT 1";
                                $dao->initCommand($strSqlTax);
                                $dao->addParameter(':taxunkid', $tvalue['taxid']);
                                $tax_pr=$dao->executeRow();

                                $strSql = "SELECT itmtaxrelid FROM " . CONFIG_DBN . ".cfitemtax_rel WHERE lnkitemid=:lnkitemid AND lnktaxunkid=:lnktaxunkid AND item_type=:item_type AND companyid=:companyid AND locationid=:locationid";
                                $dao->initCommand($strSql);
                                $dao->addParameter(':lnkitemid', $comboid);
                                $dao->addParameter(':lnktaxunkid', $tvalue['taxid']);
                                $dao->addParameter(':item_type', '4');
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $particuler_tax = $dao->executeRow();

                                if (in_array($particuler_tax['itmtaxrelid'], $tax_arr)) {

                                    $strSql = "UPDATE " . CONFIG_DBN . ".cfitemtax_rel SET amount=:amount, taxapplyafter=:taxapplyafter,item_type=:item_type,modified_user=:modified_user,postingrule=:postingrule,modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND item_type=:item_type AND lnktaxunkid=:lnktaxunkid
                                           AND locationid=:locationid AND lnkitemid=:lnkitemid";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':lnkitemid', $comboid);
                                    $dao->addParameter(':lnktaxunkid', $tvalue['taxid']);
                                    $dao->addParameter(':amount', $tvalue['amount']);
                                    $dao->addParameter(':taxapplyafter', $tvalue['applytax']);
                                    $dao->addParameter(':postingrule', $tax_pr['postingrule']);
                                    $dao->addParameter(':item_type', '4');
                                    $dao->addParameter(':modified_user', CONFIG_UID);
                                    $dao->addParameter(':modifieddatetime', $datetime);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $dao->executeNonQuery();
                                    if (($tkey = array_search($particuler_tax['itmtaxrelid'], $tax_arr)) !== false) {
                                        unset($tax_arr[$tkey]);
                                    }
                                } else {
                                    $strSql = "INSERT INTO " . CONFIG_DBN . ".cfitemtax_rel(lnkitemid,lnktaxunkid,amount,taxapplyafter,postingrule,item_type,created_user, createddatetime,companyid,locationid)";
                                    $strSql .= " VALUE (:lnkitemid, :lnktaxunkid, :amount,:taxapplyafter,:postingrule,:item_type,:created_user, :createddatetime,:companyid, :locationid)";
                                    $dao->initCommand($strSql);
                                    $dao->addParameter(':lnkitemid', $comboid);
                                    $dao->addParameter(':lnktaxunkid', $tvalue['taxid']);
                                    $dao->addParameter(':amount', $tvalue['amount']);
                                    $dao->addParameter(':taxapplyafter', $tvalue['applytax']);
                                    $dao->addParameter(':postingrule', $tax_pr['postingrule']);
                                    $dao->addParameter(':item_type', '4');
                                    $dao->addParameter(':created_user', CONFIG_UID);
                                    $dao->addParameter(':createddatetime', $datetime);
                                    $dao->addParameter(':companyid', CONFIG_CID);
                                    $dao->addparameter(':locationid', CONFIG_LID);
                                    $dao->executeNonQuery();
                                }
                            }
                        }
                    }
                    if(count($tax_arr)>0){
                        $strid = implode(",",$tax_arr);
                        $strSql = "DELETE FROM ".CONFIG_DBN.".cfitemtax_rel WHERE lnkitemid=:lnkitemid AND itmtaxrelid IN (".$strid.") AND item_type=:item_type AND companyid=:companyid AND locationid=:locationid";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':lnkitemid',$comboid);
                        $dao->addParameter(':item_type', '4');
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $dao->addparameter(':locationid',CONFIG_LID);
                        $dao->executeNonQuery();
                    }
                }
                else{
                    $strSql = "DELETE FROM ".CONFIG_DBN.".cfitemtax_rel WHERE lnkitemid=:id AND item_type=:item_type AND companyid=:companyid AND locationid=:locationid";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":id",$comboid);
                    $dao->addParameter(':item_type', '4');
                    $dao->addParameter(":companyid",CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $dao->executeNonQuery();
                }
                //

                $ObjAuditDao->addactivitylog($res['module'], $title, $res['id'], $json_data);
                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_UP_SUC)));
            }


            //           else
//           {
//               $id = $ObjCommonDao->getprimarykey('cfmenu_combo',$data['id'],'combounkid');
//               $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"comboname",$data['comboname'],$id);
//               if($chk_name==1){
//                   return json_encode(array('Success'=>'False','Message'=>'Combo Name already exists'));
//               }
//
//                $title = "Edit Record";
//                $strSql = "UPDATE ".CONFIG_DBN.".cfmenu_combo SET comboname=:comboname,
//                                                                    amount=:amount,
//                                                                    modifieddatetime=:modifieddatetime,
//                                                                    modified_user=:modified_user
//                                                                    WHERE hashkey=:combounkid AND companyid=:companyid AND locationid=:locationid";
//                $dao->initCommand($strSql);
//                $dao->addParameter(':combounkid',$data['id']);
//                $dao->addParameter(':comboname',$data['comboname']);
//                $dao->addParameter(':amount',$data['amount']);
//                $dao->addParameter(':modifieddatetime',$datetime);
//                $dao->addParameter(':modified_user',CONFIG_UID);
//                $dao->addParameter(':companyid',CONFIG_CID);
//                $dao->addparameter(':locationid',CONFIG_LID);
//                $dao->executeNonQuery();
//                $comboid =$ObjCommonDao->getprimarykey('cfmenu_combo',$data['id'],'combounkid');
//
//                //Add items
//                   $itemlist = $data['itemdata'];
//                   $strSql = "SELECT relationunkid FROM ".CONFIG_DBN.".cfcombo_item_rel WHERE lnkcomboid=:lnkcomboid AND companyid=:companyid AND locationid=:locationid";
//                   $dao->initCommand($strSql);
//                   $dao->addParameter(':lnkcomboid',$comboid);
//                   $dao->addParameter(':companyid',CONFIG_CID);
//                   $dao->addparameter(':locationid',CONFIG_LID);
//                   $particuler_item = $dao->executeQuery();
//                   $item_arr = \util\util::getoneDarray($particuler_item,'relationunkid');
//
//               if(count($itemlist)>0) {
//                   foreach ($itemlist as $value) {
//                       $qtyarray = explode('-&gt;', $value);
//                       $item = $qtyarray[0];
//                       $quantity = $qtyarray[1];
//                       $menuitemid = $ObjCommonDao->getprimarykey('cfmenu_items', $item, 'itemunkid');
//
//                       $strSql = "SELECT relationunkid FROM " . CONFIG_DBN . ".cfcombo_item_rel WHERE lnkcomboid=:lnkcomboid AND lnkitemid=:lnkitemid AND companyid=:companyid AND locationid=:locationid";
//                       $dao->initCommand($strSql);
//                       $dao->addParameter(':lnkcomboid', $comboid);
//                       $dao->addParameter(':lnkitemid', $menuitemid);
//                       $dao->addParameter(':companyid', CONFIG_CID);
//                       $dao->addparameter(':locationid', CONFIG_LID);
//                       $particuler_item = $dao->executeRow();
//
//                       if (in_array($particuler_item['relationunkid'], $item_arr)) {
//                           $strSql = "UPDATE " . CONFIG_DBN . ".cfcombo_item_rel SET lnkcomboid=:lnkcomboid, lnkitemid=:lnkitemid, quantity=:quantity,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE relationunkid=:relationunkid AND companyid=:companyid AND locationid=:locationid";
//                           $dao->initCommand($strSql);
//                           $dao->addParameter(':relationunkid', $particuler_item['relationunkid']);
//                           $dao->addParameter(':lnkcomboid', $comboid);
//                           $dao->addParameter(':lnkitemid', $menuitemid);
//                           $dao->addParameter(':quantity', $quantity);
//                           $dao->addParameter(':modified_user', CONFIG_UID);
//                           $dao->addParameter(':modifieddatetime', $datetime);
//                           $dao->addParameter(':companyid', CONFIG_CID);
//                           $dao->addparameter(':locationid', CONFIG_LID);
//                           $dao->executeNonQuery();
//                           if (($ukey = array_search($particuler_item['relationunkid'], $item_arr)) !== false) {
//                               unset($item_arr[$ukey]);
//                           }
//                       }
//                       else {
//                           $strSql = "INSERT INTO " . CONFIG_DBN . ".cfcombo_item_rel (lnkcomboid,lnkitemid,quantity,created_user,createddatetime,companyid,locationid)";
//                           $strSql .= " VALUE (:lnkcomboid, :lnkitemid, :quantity, :created_user, :createddatetime, :companyid, :locationid)";
//                           $dao->initCommand($strSql);
//                           $dao->addParameter(':lnkcomboid', $comboid);
//                           $dao->addParameter(':lnkitemid', $menuitemid);
//                           $dao->addParameter(':quantity', $quantity);
//                           $dao->addParameter(':created_user', CONFIG_UID);
//                           $dao->addParameter(':createddatetime', $datetime);
//                           $dao->addParameter(':companyid', CONFIG_CID);
//                           $dao->addparameter(':locationid', CONFIG_LID);
//                           $dao->executeNonQuery();
//                       }
//                   }
//               }
//               if(count($itemlist)>0){
//                   $strid = implode(",",$item_arr);
//                   if($strid!='') {
//                       $strSql = "DELETE FROM " . CONFIG_DBN . ".cfcombo_item_rel WHERE lnkcomboid=:lnkcomboid AND relationunkid IN (" . $strid . ") AND companyid=:companyid AND locationid=:locationid";
//                       $dao->initCommand($strSql);
//                       $dao->addParameter(':lnkcomboid', $comboid);
//                       $dao->addParameter(':companyid', CONFIG_CID);
//                       $dao->addparameter(':locationid', CONFIG_LID);
//                       $dao->executeNonQuery();
//                   }
//               }
//               //
//
//               $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
//                return json_encode(array('Success'=>'True','Message'=>'Combo Product updated successfully'));
//            }


        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addComboProduct - ' . $e);
        }
    }

    public function comboproductlist($limit, $offset, $name, $isactive = 0)
    {
        try {
            $this->log->logIt($this->module . ' - comboproductlist - ' . $name);
            $dao = new \dao;
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT CFC.hashkey,CFC.comboname,CFC.is_active,IFNULL(CFU1.username,'') AS createduser,
                       IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(CFC.createddatetime,'" . $mysqlformat . "'),'') as created_date,
                       IFNULL(DATE_FORMAT(CFC.modifieddatetime,'" . $mysqlformat . "'),'') as modified_date,
                        IFNULL(CASE 
                        WHEN CIUR.default_rate=1 THEN ROUND(CIUR.rate1,$round_off)
                        WHEN CIUR.default_rate=2 THEN ROUND(CIUR.rate2,$round_off)
                        WHEN CIUR.default_rate=3 THEN ROUND(CIUR.rate3,$round_off)
                        ELSE ROUND(CIUR.rate4,$round_off)
                        END ,'')as amount 
                       FROM " . CONFIG_DBN . ".cfmenu_combo AS CFC
                       LEFT JOIN ".CONFIG_DBN.".cfmenu_itemunit_rel as CIUR ON CFC.combounkid=CIUR.lnkitemid AND CFC.serve_unit=CIUR.lnkitemunitid AND CIUR.recipe_type=4
                       LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU1 ON CFC.created_user=CFU1.userunkid AND CFC.companyid=CFU1.companyid
                       LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU2 ON CFC.modified_user=CFU2.userunkid AND CFC.modified_user=CFU2.userunkid AND CFC.companyid=CFU2.companyid
                       WHERE CFC.companyid=:companyid AND CFC.locationid=:locationid AND CFC.is_deleted=0";

            if ($name != "")
                $strSql .= " AND comboname LIKE '%" . $name . "%'";
            if ($isactive == 1)
                $strSql .= " AND is_active=1";
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $data = $dao->executeQuery();
            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return html_entity_decode(json_encode($retvalue));
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - comboproductlist - ' . $e);
        }
    }

    public function getComboRec($data)
    {
        try {
            $this->log->logIt($this->module . " - getComboRec");
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();

            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $hashkey = (isset($data['id'])) ? $data['id'] : "";

            $combounkid = $ObjCommonDao->getprimarykey('cfmenu_combo', $hashkey , 'combounkid');


            $strSql = "SELECT CFI.comboname,CFI.long_desc,IFNULL(CFI.image,'') as image,CFI.tax_apply_rate,
                       CIU.hashkey as serve_unit,CFI.sku,CFI.is_active";
            $strSql .= " FROM ";
            $strSql .= CONFIG_DBN . ".cfmenu_combo AS CFI                        
                        LEFT JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS CIU ON CIU.unitunkid = CFI.serve_unit AND CIU.companyid=:companyid and CIU.locationid=:locationid AND CIU.is_deleted = 0 AND CIU.is_active = 1 
                        WHERE CFI.combounkid=:combounkid AND CFI.companyid=:companyid AND CFI.locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':combounkid', $combounkid);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeRow();

            $response_array = array();
            $response_array['combodata'] = $res;
            $response_array['bind_unit'] = array();
            $response_array['bind_tax'] = array();

            $strSql = "SELECT CIU.hashkey as unithash,CIUR.itmunitunkid,
                  IFNULL(ROUND(CIUR.rate1,$round_off),'') as rate1,IFNULL(ROUND(CIUR.rate2,$round_off),'') as rate2,
                  IFNULL(ROUND(CIUR.rate3,$round_off),'') as rate3,IFNULL(ROUND(CIUR.rate4,$round_off),'') as rate4,
                  CIUR.default_rate,CIU.name,CIUR.lnkitemunitid
                  FROM " . CONFIG_DBN . ".cfmenu_itemunit_rel as CIUR
                  INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS CIU
                  ON CIU.unitunkid=CIUR.lnkitemunitid AND CIU.companyid=:companyid AND CIU.is_deleted = 0 AND CIU.is_active = 1  
                  WHERE CIUR.lnkitemid=:itemid AND CIUR.recipe_type=:recipe_type AND CIUR.companyid=:companyid AND CIUR.locationid=:locationid";

            $dao->initCommand($strSql);
            $dao->addParameter(':itemid', $combounkid);
            $dao->addParameter(':recipe_type', '4');
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $unitdata = $dao->executeQuery();

            foreach ($unitdata as $key4 => $val4) {

                $strSql = "SELECT CFI.hashkey as defaultItem,CIR.itemgroupid,CIR.unititemgrouprelationunkid,CIR.defaultitemid";
                $strSql .= "  FROM " . CONFIG_DBN . ".combounit_itemgroups_relation AS CIR ";
                $strSql .= "  INNER JOIN " . CONFIG_DBN . ".cfmenu_items AS CFI ON CFI.itemunkid=CIR.defaultitemid AND CFI.companyid =:companyid AND CFI.locationid=:locationid AND CFI.is_deleted = 0 AND CFI.is_active = 1 ";
                $strSql .= "  WHERE CIR.lnkcombounitid = :lnkcombounitid AND CIR.companyid =:companyid AND CIR.locationid=:locationid";

                $dao->initCommand($strSql);
                $dao->addParameter(':lnkcombounitid', $val4['itmunitunkid']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $itemgroupdata = $dao->executeQuery();

                foreach ($itemgroupdata as $key => $val) {

                    $strSql = "SELECT CIR.itemid,CUR.itemunitid,CUR.quantity,CUR.itemunitrelationunkid,CUR.is_include,CIU.hashkey as itemunithash,CFI.hashkey as itemhash,CIR.extracharge,CIR.groupitemrelationunkid";
                    $strSql .= "  FROM " . CONFIG_DBN . ".comboitemgroups_items_relation AS CIR ";
//                    $strSql .= "  INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit_rel AS CFIR ON CIR.lnkcombounitid = CFIR.itmunitunkid AND CIR.comboid = CFIR.lnkitemid AND CFIR.companyid = :companyid AND CFIR.locationid=:locationid AND CFIR.recipe_type=4 ";
                    $strSql .= "  LEFT JOIN " . CONFIG_DBN . ".comboitem_unit_relation AS CUR ON CUR.lnkcombogroup_itemrel = CIR.groupitemrelationunkid AND CUR.companyid=:companyid AND CUR.locationid=:locationid";
                    $strSql .= "  LEFT JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS CIU ON CIU.unitunkid=CUR.itemunitid AND CIU.companyid =:companyid AND CIU.is_deleted = 0 AND CIU.is_active = 1 ";
                    $strSql .= "  INNER JOIN " . CONFIG_DBN . ".cfmenu_items AS CFI ON CFI.itemunkid=CIR.itemid AND CFI.companyid =:companyid AND CFI.locationid=:locationid AND CFI.is_deleted = 0 AND CFI.is_active = 1 ";
                    $strSql .= "  WHERE CIR.lnkcombounitid = :lnkcombounitid AND CIR.companyid =:companyid AND CIR.locationid=:locationid AND CIR.itemgroupid = :itemgroupid AND CIR.lnkcomboitemgrouprelid = :lnkcomboitemgrouprelid ";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':lnkcombounitid', $val4['itmunitunkid']);
                    $dao->addParameter(':itemgroupid', $val['itemgroupid']);
                    $dao->addParameter(':lnkcomboitemgrouprelid', $val['unititemgrouprelationunkid']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $itemdata = $dao->executeQuery();

                    $item_arr = array();

                    $cnt1 = 0;

                    $marr_maintain = '';

                    for ($p = 0; $p < count($itemdata); $p++) {                     // if item id is same and if item has multipleunits

                        if ($itemdata[$p]['itemid'] != $marr_maintain) {
                                                                                     // than 2nd time it will go in elseif loop
                            $cnt1 = 0;
                            $item_arr[$itemdata[$p]['itemid']]= array();
                            $item_arr[$itemdata[$p]['itemid']]['itemhash'] = $itemdata[$p]['itemhash'];
                            $item_arr[$itemdata[$p]['itemid']]['itemid'] = $itemdata[$p]['itemid'];
                            $item_arr[$itemdata[$p]['itemid']]['extracharge'] = $itemdata[$p]['extracharge'];
                            $item_arr[$itemdata[$p]['itemid']]['itemunitsdetails'] = array();
                            $item_arr[$itemdata[$p]['itemid']]['itemunitsdetails'][$cnt1]['itemunitid'] = $itemdata[$p]['itemunitid'];
                            $item_arr[$itemdata[$p]['itemid']]['itemunitsdetails'][$cnt1]['itemunithash'] = $itemdata[$p]['itemunithash'];
                            $item_arr[$itemdata[$p]['itemid']]['itemunitsdetails'][$cnt1]['quantity'] = $itemdata[$p]['quantity'];
                            $item_arr[$itemdata[$p]['itemid']]['itemunitsdetails'][$cnt1]['itemunitrelationunkid'] = $itemdata[$p]['itemunitrelationunkid'];
                            $item_arr[$itemdata[$p]['itemid']]['itemunitsdetails'][$cnt1]['is_include'] = $itemdata[$p]['is_include'];
                            $marr_maintain = $itemdata[$p]['itemid'];
                        } elseif ($itemdata[$p]['itemid'] == $marr_maintain) {
                            $cnt1++;
                            $item_arr[$itemdata[$p]['itemid']]['itemunitsdetails'][$cnt1]['itemunitid'] = $itemdata[$p]['itemunitid'];
                            $item_arr[$itemdata[$p]['itemid']]['itemunitsdetails'][$cnt1]['itemunithash'] = $itemdata[$p]['itemunithash'];
                            $item_arr[$itemdata[$p]['itemid']]['itemunitsdetails'][$cnt1]['quantity'] = $itemdata[$p]['quantity'];
                            $item_arr[$itemdata[$p]['itemid']]['itemunitsdetails'][$cnt1]['itemunitrelationunkid'] = $itemdata[$p]['itemunitrelationunkid'];
                            $item_arr[$itemdata[$p]['itemid']]['itemunitsdetails'][$cnt1]['is_include'] = $itemdata[$p]['is_include'];
                            $marr_maintain = $itemdata[$p]['itemid'];
                        }
                    }

                    foreach ($item_arr as $key3 => $val3) {

                        foreach ($val3['itemunitsdetails'] as $key2 => $val2) {

                            if ($val2['is_include'] == 1) {

                                $strSql = "SELECT FDIM2.min,FDIM2.max,FDIM.quantity as modqty,FDIM.modifierid,FDIM.modifierunitid,FDIM.is_include,CFM.modifiername,FDMI.modifieritemid,FDMI.modifieritemunitid,FDMI.quantity as moditemqty,CFMI.itemname as moditemname,CIU.name as modifierunitname,CIU2.name as moditemunitname,".$val2['itemunitid']." AS itemunitid";
                                $strSql .= "  FROM " . CONFIG_DBN . ".comboitem_mod_relation AS FDIM ";
                                $strSql .= "  INNER JOIN " . CONFIG_DBN . ".cfmenu_modifiers AS CFM ON FDIM.modifierid = CFM.modifierunkid AND CFM.companyid = :companyid AND CFM.locationid=:locationid AND CFM.is_deleted = 0 AND CFM.is_active = 1";
                                $strSql .= "  LEFT JOIN " . CONFIG_DBN . ".combomod_item_relation AS FDMI ON FDMI.lnkcombomodifierrel = FDIM.itemmodrelationunkid AND FDIM.companyid = :companyid AND FDIM.locationid=:locationid";
                                $strSql .= "  LEFT JOIN " . CONFIG_DBN . ".cfmenu_modifier_items AS CFMI ON CFMI.modifieritemunkid = FDMI.modifieritemid AND CFMI.companyid = :companyid AND CFMI.locationid=:locationid AND CFMI.is_deleted = 0 AND CFMI.is_active = 1";
                                $strSql .= "  LEFT JOIN " . CONFIG_DBN . ".fditem_modifier_relation AS FDIM2 ON FDIM2.lnkmodifierid = FDIM.modifierid AND FDIM2.lnkitemid = :lnkitemid AND FDIM2.companyid =:companyid AND FDIM2.locationid =:locationid";
                                $strSql .= "  INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit_rel AS CFIR ON CFIR.itmunitunkid = FDIM2.lnkitemunitid AND CFIR.lnkitemunitid = :lnkitemunitid AND CFIR.lnkitemid = :lnkitemid AND CFIR.companyid = :companyid AND CFIR.locationid=:locationid AND CFIR.recipe_type=1";
                                $strSql .= "  LEFT JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS CIU ON CIU.unitunkid=FDIM.modifierunitid AND CIU.companyid=:companyid AND CIU.is_deleted = 0 AND CIU.is_active = 1 ";
                                $strSql .= "  LEFT JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS CIU2 ON CIU2.unitunkid=FDMI.modifieritemunitid AND CIU2.companyid=:companyid AND CIU2.is_deleted = 0 AND CIU2.is_active = 1";
                                $strSql .= "  WHERE FDIM.lnkcomboitem_unitrel = :lnkcomboitem_unitrel AND FDIM.companyid = :companyid AND FDIM.locationid=:locationid";

                                $dao->initCommand($strSql);
                                $dao->addParameter(':lnkcomboitem_unitrel', $val2['itemunitrelationunkid']);
                                $dao->addParameter(':lnkitemid', $key3);
                                $dao->addParameter(':lnkitemunitid', $val2['itemunitid']);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addparameter(':locationid', CONFIG_LID);
                                $moddata = $dao->executeQuery();

                                $modifier_arr = array();
                                $arr_maintain = array();
                                $cnt = 0;

                                for ($i = 0; $i < count($moddata); $i++) {
                                    if ($i == 0) {
                                        $modifier_arr[$cnt]['itemunitid'] = $moddata[$i]['itemunitid'];
                                        $modifier_arr[$cnt]['mod_name'] = $moddata[$i]['modifiername'];
                                        $modifier_arr[$cnt]['mod_qty'] = $moddata[$i]['modqty'];
                                        $modifier_arr[$cnt]['mod_min'] = $moddata[$i]['min'];
                                        $modifier_arr[$cnt]['mod_max'] = $moddata[$i]['max'];
                                        $modifier_arr[$cnt]['mod_unit'] = $moddata[$i]['modifierunitid'];
                                        $modifier_arr[$cnt]['mod_is_included'] = $moddata[$i]['is_include'];
                                        $modifier_arr[$cnt]['mod_hash'] = $moddata[$i]['modifierid'];
                                        $arr_maintain[$moddata[$i]['modifierid']] = $cnt;
                                        $modifier_arr[$cnt]['mi_hashkeys'] = "";
                                        $modifier_arr[$cnt]['mod_unitname'] = $moddata[$i]['modifierunitname'];
                                        $modifier_arr[$cnt]['mi_unitnames'] = "";
                                    } else if ($i != 0 && isset($arr_maintain[$moddata[$i]['modifierid']]) != 1) {
                                        $cnt++;
                                        $modifier_arr[$cnt]['itemunitid'] = $moddata[$i]['itemunitid'];
                                        $modifier_arr[$cnt]['mod_name'] = $moddata[$i]['modifiername'];
                                        $modifier_arr[$cnt]['mod_qty'] = $moddata[$i]['modqty'];
                                        $modifier_arr[$cnt]['mod_min'] = $moddata[$i]['min'];
                                        $modifier_arr[$cnt]['mod_max'] = $moddata[$i]['max'];
                                        $modifier_arr[$cnt]['mod_unit'] = $moddata[$i]['modifierunitid'];
                                        $modifier_arr[$cnt]['mod_is_included'] = $moddata[$i]['is_include'];
                                        $modifier_arr[$cnt]['mod_hash'] = $moddata[$i]['modifierid'];
                                        $arr_maintain[$moddata[$i]['modifierid']] = $cnt;
                                        $modifier_arr[$cnt]['mi_hashkeys'] = "";
                                        $modifier_arr[$cnt]['mod_unitname'] = $moddata[$i]['modifierunitname'];
                                        $modifier_arr[$cnt]['mi_unitnames'] = "";
                                    }


                                    $index = $arr_maintain[$moddata[$i]['modifierid']];

                                    if ($modifier_arr[$index]['mi_hashkeys'] == "")
                                        $modifier_arr[$index]['mi_hashkeys'] .= $moddata[$i]['modifieritemid'];
                                    else
                                        $modifier_arr[$index]['mi_hashkeys'] .= "," . $moddata[$i]['modifieritemid'];

                                    if ($modifier_arr[$index]['mi_unitnames'] == "")
                                        $modifier_arr[$index]['mi_unitnames'] .= $moddata[$i]['moditemunitname'];
                                    else
                                        $modifier_arr[$index]['mi_unitnames'] .= "," . $moddata[$i]['moditemunitname'];


                                    if ($moddata[$i]['modifieritemid'] != "" && $moddata[$i]['modifieritemunitid'] != "") {

                                        $modifier_arr[$index]['modifieritem'][] = array(
                                            "mi_hashkey" => $moddata[$i]['modifieritemid'],
                                            "mi_unit" => $moddata[$i]['modifieritemunitid'],
                                            "mi_unitname" => $moddata[$i]['moditemunitname'],
                                            "mi_name" => $moddata[$i]['moditemname'],
                                            "mi_qty" => $moddata[$i]['moditemqty'],
                                        );
                                    } else {
                                        $modifier_arr[$index]['modifieritem'] = array();
                                    }
                                }

                                $val3['itemunitsdetails'][$key2]['bind_modifier'] = $modifier_arr;

                            }

                        }

                        $item_arr[$key3] = $val3;

                    }
                    $itemgroupdata[$key]['bind_items'] = $item_arr;
                }

                $unitdata[$key4]['bind_itemgroups'] = $itemgroupdata;
            }

            $response_array['bind_unit'] = $unitdata;


                $strSql = "SELECT CITR.lnktaxunkid,
                  ROUND(CITR.amount,$round_off) as amount,CITR.taxapplyafter
                  FROM " . CONFIG_DBN . ".cfitemtax_rel as CITR         
                  WHERE CITR.lnkitemid=:itemid AND CITR.item_type=:item_type AND CITR.companyid=:companyid AND CITR.locationid=:locationid";

                $dao->initCommand($strSql);
                $dao->addParameter(':itemid', $combounkid);
                $dao->addParameter(':item_type', '4');
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $taxdata = $dao->executeQuery();
                $response_array['bind_tax'] = $taxdata;

                return html_entity_decode(json_encode(array("Success" => "True", "Data" => $response_array),1));

//            $strSql = "SELECT comboname,amount,is_active FROM ".CONFIG_DBN.".cfmenu_combo WHERE hashkey=:combounkid AND companyid=:companyid AND locationid=:locationid";
//            $dao->initCommand($strSql);
//            $dao->addParameter(':combounkid',$id);
//            $dao->addParameter(':companyid',CONFIG_CID);
//            $dao->addparameter(':locationid',CONFIG_LID);
//            $res = $dao->executeRow();
//
//            $strRel = "SELECT lnkitemid,quantity FROM ".CONFIG_DBN.".cfcombo_item_rel WHERE lnkcomboid=:combounkid AND companyid=:companyid AND locationid=:locationid";
//            $dao->initCommand($strRel);
//            $dao->addParameter(':combounkid',$combounkid);
//            $dao->addParameter(':companyid',CONFIG_CID);
//            $dao->addparameter(':locationid',CONFIG_LID);
//            $resRel = $dao->executeQuery();
//
//            $item=[];
//            $itemData=[];
//
//            foreach($resRel as $value) {
//                $item[]=$value['lnkitemid'];
//                $itemData[]=$value['lnkitemid']."->".$value['quantity'];
//            }
//            $items =implode(',',$item);
//            $itemData =implode(',',$itemData);
//            $res['itemdata']=$itemData;
//
//            $strSql1= "SELECT GROUP_CONCAT(CFITEM.hashkey) AS items,CFCTGRY.hashkey as category FROM ".CONFIG_DBN.".cfmenu_items AS CFITEM INNER JOIN ".CONFIG_DBN.".cfmenu_categories AS CFCTGRY ON CFITEM.categoryunkid=CFCTGRY.categoryunkid WHERE CFITEM.is_deleted=0 AND FIND_IN_SET(itemunkid,'".$items."') GROUP BY CFITEM.categoryunkid";
//            $dao->initCommand($strSql1);
//            $res1 =  $dao->executeQuery();
//            $res['items']=$res1;

//                        $strSql = "SELECT CMR.modifierid,CMR.modifierunitid,CMR.quantity,CMR.is_include,CMR.itemmodrelationunkid";
//                        $strSql .= "  FROM " . CONFIG_DBN . ".comboitem_mod_relation AS CMR ";
//                        $strSql .= "  INNER JOIN " . CONFIG_DBN . ".comboitem_unit_relation AS CFIR ON CMR.lnkcombounit_itemrel = CFIR.itemunitrelationunkid AND CFIR.companyid = :companyid AND CFIR.locationid=:locationid";
//                        $strSql .= "  WHERE CMR.lnkcomboitem_unitrel = :lnkcomboitem_unitrel AND CMR.lnkitemunitid=:lnkitemunitid AND CMR.companyid = :companyid AND CMR.locationid=:locationid";
//
//                        $dao->initCommand($strSql);
//                        $dao->addParameter(':lnkcomboitem_unitrel', $val2['itemunitrelationunkid']);
//                        $dao->addParameter(':companyid', CONFIG_CID);
//                        $dao->addparameter(':locationid', CONFIG_LID);
//                        $moddata = $dao->executeQuery();

//
//                        foreach ($itemunitdata as $key2 => $val2) {
//
//                            $strSql = "SELECT CMR.modifierid,CMR.modifierunitid,CMR.quantity,CMR.is_include,CMR.itemmodrelationunkid";
//                            $strSql .= "  FROM " . CONFIG_DBN . ".comboitem_mod_relation AS CMR ";
//                            $strSql .= "  INNER JOIN " . CONFIG_DBN . ".comboitem_unit_relation AS CFIR ON CMR.lnkcombounit_itemrel = CFIR.itemunitrelationunkid AND CFIR.companyid = :companyid AND CFIR.locationid=:locationid";
//                            $strSql .= "  WHERE CMR.lnkcomboitem_unitrel = :lnkcomboitem_unitrel AND CMR.lnkitemunitid=:lnkitemunitid AND CMR.companyid = :companyid AND CMR.locationid=:locationid";
//
//                            $dao->initCommand($strSql);
//                            $dao->addParameter(':lnkcomboitem_unitrel', $val2['itemunitrelationunkid']);
//                            $dao->addParameter(':companyid', CONFIG_CID);
//                            $dao->addparameter(':locationid', CONFIG_LID);
//                            $moddata = $dao->executeQuery();
//                        }


        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getComboRec - ".$e);
			return false;
		}
	}

    public function getItemlist()
    {
        try
        {
			$this->log->logIt($this->module." - getItemlist");
			$dao = new \dao();

            $strSql = "SELECT hashkey,itemunkid,itemname FROM ".CONFIG_DBN.".cfmenu_items 
                        WHERE companyid=:companyid AND locationid=:locationid 
                        AND is_deleted=0 AND is_active=1";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res['item'] = $dao->executeQuery();

            $strSql = "SELECT CFI.hashkey as itemunkid,
                        GROUP_CONCAT(CIU.unitunkid) as unitunkid,GROUP_CONCAT(CIU.hashkey) as hashkey,GROUP_CONCAT(CIU.name) as itemunit
                        FROM ".CONFIG_DBN.".cfmenu_items as CFI
                        INNER JOIN ".CONFIG_DBN.".cfmenu_itemunit_rel as CUREL on CUREL.lnkitemid=CFI.itemunkid and CUREL.recipe_type=:recipe_type  
                        INNER JOIN ".CONFIG_DBN.".cfmenu_itemunit as CIU on CUREL.lnkitemunitid=CIU.unitunkid AND CIU.is_deleted = 0 AND CIU.is_active = 1  
                        WHERE CFI.companyid=:companyid AND CFI.locationid=:locationid
                        AND CFI.is_deleted=0 AND CFI.is_active=1 GROUP BY CFI.itemunkid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $dao->addParameter(':recipe_type',1);

            $res['itemunit'] = $dao->executeQuery();

            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getItemlist - ".$e);
			return false;
		}
    }

    public function getItemModifier($data)
    {
        try {
            $this->log->logIt($this->module . " - getItemModifier");
            $dao = new \dao();


           $hashkey=(isset($data['itemid']))?$data['itemid']:"";
           $hashkeyunit=(isset($data['itemunitkey']))?$data['itemunitkey']:"";

           $ObjCommonDao = new \database\commondao;

                $itemunkid = $ObjCommonDao->getprimarykey('cfmenu_items',$hashkey, 'itemunkid');
                $unitunkid = $ObjCommonDao->getprimarykey('cfmenu_itemunit',$hashkeyunit, 'unitunkid');


                $strSql = "SELECT CIU.hashkey as unithash,CIUR.itmunitunkid,                
                              CIU.name,CIUR.lnkitemunitid,CIUR.lnkitemid
                  FROM " . CONFIG_DBN . ".cfmenu_itemunit_rel as CIUR
                  INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS CIU
                  ON CIU.unitunkid=CIUR.lnkitemunitid AND CIU.is_deleted = 0 AND CIU.is_active = 1                
                  WHERE CIUR.lnkitemid=:lnkitemid AND CIUR.recipe_type=:recipe_type AND CIUR.companyid=:companyid AND CIUR.locationid=:locationid AND CIU.unitunkid=:unitunkid";

                $dao->initCommand($strSql);
                $dao->addParameter(':recipe_type', '1');
                $dao->addParameter(':lnkitemid',$itemunkid);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->addparameter(':unitunkid', $unitunkid);
                $unitdata = $dao->executeQuery();

                foreach ($unitdata as $key => $val) {

                    $strSql = "SELECT FDIM.min, FDIM.max,FDIM.lnkmodiunitid,CFM.is_included,CFM.modifiername,CFM.modifierunkid AS modifier_hash,FDMI.lnkmodiunitid AS mi_modiunitid,CFMI.modifieritemunkid AS mi_hashkey, ".$val['lnkitemunitid']." AS itemunitid,CIU.name as modifierunitname,CIU2.name as moditemunitname,CFMI.itemname as moditemname";
                    $strSql .= "  FROM " . CONFIG_DBN . ".fditem_modifier_relation AS FDIM ";
                    $strSql .= " LEFT JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS CIU ON CIU.unitunkid=FDIM.lnkmodiunitid AND CIU.is_deleted = 0 AND CIU.is_active = 1 ";
                    $strSql .= "  INNER JOIN " . CONFIG_DBN . ".cfmenu_modifiers AS CFM ON FDIM.lnkmodifierid = CFM.modifierunkid AND CFM.companyid = :companyid AND CFM.locationid=:locationid AND CFM.is_deleted = 0 AND CFM.is_active = 1 ";
                    $strSql .= "  LEFT JOIN " . CONFIG_DBN . ".fdmodifier_item_relation AS FDMI ON FDMI.lnkmodifierrelid = FDIM.relationunkid AND FDMI.lnkitemid = FDIM.lnkitemid AND FDIM.companyid = :companyid AND FDIM.locationid=:locationid";
                    $strSql .= "  LEFT JOIN " . CONFIG_DBN . ".cfmenu_modifier_items AS CFMI ON CFMI.modifieritemunkid = FDMI.lnkmodifieritemid AND CFMI.companyid = :companyid AND CFMI.locationid=:locationid AND CFMI.is_deleted = 0 AND CFMI.is_active = 1 ";
                    $strSql .= "  LEFT JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS CIU2 ON CIU2.unitunkid=FDMI.lnkmodiunitid AND CIU2.is_deleted = 0 AND CIU2.is_active = 1 ";
                    $strSql .= "  WHERE FDIM.lnkitemid = :itemid AND FDIM.lnkitemunitid=:lnkitemunitid AND FDIM.companyid = :companyid AND FDIM.locationid=:locationid";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':itemid', $itemunkid);
                    $dao->addParameter(':lnkitemunitid', $val['itmunitunkid']);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $data = $dao->executeQuery();

                    $modifier_arr = array();
                    $arr_maintain = array();
                    $cnt = 0;

                    for ($i = 0; $i < count($data); $i++) {
                        if ($i == 0) {
                            $modifier_arr[$cnt]['itemunitid'] = $data[$i]['itemunitid'];
                            $modifier_arr[$cnt]['mod_name'] = $data[$i]['modifiername'];
                            $modifier_arr[$cnt]['mod_min'] = $data[$i]['min'];
                            $modifier_arr[$cnt]['mod_max'] = $data[$i]['max'];
                            $modifier_arr[$cnt]['mod_unit'] = $data[$i]['lnkmodiunitid'];
                            $modifier_arr[$cnt]['mod_is_included'] = $data[$i]['is_included'];
                            $modifier_arr[$cnt]['mod_hash'] = $data[$i]['modifier_hash'];
                            $arr_maintain[$data[$i]['modifier_hash']] = $cnt;
                            $modifier_arr[$cnt]['mi_hashkeys'] = "";
                            $modifier_arr[$cnt]['mod_unitname'] = $data[$i]['modifierunitname'];
                            $modifier_arr[$cnt]['mi_unitnames'] = "";
                        } else if ($i != 0 && isset($arr_maintain[$data[$i]['modifier_hash']]) != 1) {
                            $cnt++;
                            $modifier_arr[$cnt]['itemunitid'] = $data[$i]['itemunitid'];
                            $modifier_arr[$cnt]['mod_name'] = $data[$i]['modifiername'];
                            $modifier_arr[$cnt]['mod_min'] = $data[$i]['min'];
                            $modifier_arr[$cnt]['mod_max'] = $data[$i]['max'];
                            $modifier_arr[$cnt]['mod_unit'] = $data[$i]['lnkmodiunitid'];
                            $modifier_arr[$cnt]['mod_is_included'] = $data[$i]['is_included'];
                            $modifier_arr[$cnt]['mod_hash'] = $data[$i]['modifier_hash'];
                            $arr_maintain[$data[$i]['modifier_hash']] = $cnt;
                            $modifier_arr[$cnt]['mi_hashkeys'] = "";
                            $modifier_arr[$cnt]['mod_unitname'] = $data[$i]['modifierunitname'];
                            $modifier_arr[$cnt]['mi_unitnames'] = "";
                        }
                        $index = $arr_maintain[$data[$i]['modifier_hash']];

                        if ($modifier_arr[$index]['mi_hashkeys'] == "")
                            $modifier_arr[$index]['mi_hashkeys'] .= $data[$i]['mi_hashkey'];
                        else
                            $modifier_arr[$index]['mi_hashkeys'] .= "," . $data[$i]['mi_hashkey'];

                        if ($modifier_arr[$index]['mi_unitnames'] == "")
                            $modifier_arr[$index]['mi_unitnames'] .= $data[$i]['moditemunitname'];
                        else
                            $modifier_arr[$index]['mi_unitnames'] .= "," . $data[$i]['moditemunitname'];

                        if ($data[$i]['mi_hashkey'] != "" && $data[$i]['mi_modiunitid'] != "") {
                            $modifier_arr[$index]['modifieritem'][] = array(
                                "mi_hashkey" => $data[$i]['mi_hashkey'],
                                "mi_unit" => $data[$i]['mi_modiunitid'],
                                "mi_unitname" => $data[$i]['moditemunitname'],
                                "mi_name" => $data[$i]['moditemname'],
                            );
                        } else {
                            $modifier_arr[$index]['modifieritem'] = array();
                        }
                    }

                    $unitdata[$key]['bind_modifier'] =array();
                    $unitdata[$key]['bind_modifier'] = $modifier_arr;
                }

               return html_entity_decode(json_encode(array("Success" => "True", "Data" => $unitdata)));

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getItemModifier - ".$e);
            return false;
        }
    }

    public function objToArray($obj, &$arr){

        if(!is_object($obj) && !is_array($obj)){
            $arr = $obj;
            return $arr;
        }

        foreach ($obj as $key => $value)
        {
            if (!empty($value))
            {
                $arr[$key] = array();
                self::objToArray($value, $arr[$key]);
            }
            else
            {
                $arr[$key] = $value;
            }
        }
        return $arr;
    }

    public function addZomatoCombosetting($data, $languageArr, $defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - addZomatoCombosetting');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $datetime = \util\util::getLocalDateTime();

            if ($data['zid'] == 0) {
                $strSql = "INSERT INTO " . CONFIG_DBN . ".cfcombozomatosetting (companyid,locationid, createddatetime, created_user, merge_combo_with_menu, timings, zomato_itemrate)
                            VALUE(:companyid,:locationid,:createddatetime, :created_user, :merge_combo_with_menu,:timings,:zomato_itemrate)";
                $dao->initCommand($strSql);
                $dao->addParameter(':merge_combo_with_menu', $data['merge_combo_with_menu']);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->addParameter(':timings', $data['timings']);
                $dao->addParameter(':zomato_itemrate', $data['zomato_itemrate']);
                $dao->addParameter(':created_user', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();

                return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_ADD_SUC));
            } else {

                $strSql = "UPDATE " . CONFIG_DBN . ".cfcombozomatosetting SET modifieddatetime=:modifieddatetime, modified_user=:modified_user, timings=:timings, zomato_itemrate=:zomato_itemrate, merge_combo_with_menu=:merge_combo_with_menu WHERE combozomatosettingunkid=:combozomatosettingunkid AND companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':merge_combo_with_menu', $data['merge_combo_with_menu']);
                $dao->addParameter(':combozomatosettingunkid', $data['zid']);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':timings', $data['timings']);
                $dao->addParameter(':zomato_itemrate', $data['zomato_itemrate']);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();

                return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_UP_SUC));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addZomatoCombosetting - ' . $e);
        }
    }

    public function getComboZomatoSetting()
    {
        try {
            $this->log->logIt($this->module . " - getComboZomatoSetting");
            $dao = new \dao();

            $strSql = "SELECT merge_combo_with_menu,timings,zomato_itemrate,combozomatosettingunkid FROM " . CONFIG_DBN . ".cfcombozomatosetting WHERE companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeRow();

            if(isset($res) && !empty($res))
            {
                $response_array['menu'] = $dao->executeRow();
                return json_encode(array("Success" => "True", "Data" => $response_array));
            }else{
                return json_encode(array("Success" => "False"));
            }

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getComboZomatoSetting - " . $e);
            return false;
        }
    }



}
?>