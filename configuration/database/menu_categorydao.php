<?php
namespace database;

class menu_categorydao
{
    public $module = 'DB_menu_categorydao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function addCategory($data,$languageArr,$defaultlanguageArr)
    {
        try
        {
            $this->log->logIt($this->module.' - addCategory');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $datetime=\util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();
            $tablename =  "cfmenu_categories";
            $ObjDependencyDao = new \database\dependencydao();

            $arr_log = array(
                'Category Name'=>$data['categoryname'],
                'SKU'=>$data['sku'],
                'Long Description'=>$data['longdescription'],
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive'
            );
            if($data['category_color']!='' && $data['category_color']!=null){
                $arr_log['Category Color'] = $data['category_color'];
            }
            if($data['category_font_color']!='' && $data['category_font_color']!=null){
                $arr_log['Category Font Color'] = $data['category_font_color'];
            }
            $json_data = json_encode($arr_log);
            if($data['id']==0)
            {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"categoryname",$data['categoryname']);
                if($chk_name==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->RECORD_ALREADY_EXISTS)));
                }
                $chk_sku = $ObjDependencyDao->checkduplicaterecord($tablename,"sku",$data['sku']);
                if($chk_sku==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG6)));

                }
                $title = "Add Record";
                $hashkey = \util\util::gethash();
                $modifier = $ObjCommonDao->getprimarykey('cfmenu',$data['menuunkid'],'menuunkid');
                $pcategory = 0;
                if(isset($data['pcatid']) && $data['pcatid']!=''){
                    $pcategory = $ObjCommonDao->getprimarykey('cfmenu_categories',$data['pcatid'],'categoryunkid');
                }
               // $menuhours = $ObjCommonDao->getprimarykey('cfmenu_menuhours',$data['menuhoursunkid'],'menuhoursunkid');

                $strSql = "INSERT INTO ".CONFIG_DBN.".cfmenu_categories (menuunkid,parent_id,categoryname, long_desc, image, sku, companyid,locationid, createddatetime, created_user, is_active, hashkey, category_color, category_font_color)
                            VALUE(:menuunkid,:parent_id,:categoryname, :long_desc, :image, :sku, :companyid,:locationid, :createddatetime, :created_user, :isactive, :hashkey, :category_color, :category_font_color)";
                
                $dao->initCommand($strSql);
                $dao->addParameter(':menuunkid',$modifier);
                $dao->addParameter(':parent_id',$pcategory);
                $dao->addParameter(':categoryname',$data['categoryname']);
                $dao->addParameter(':long_desc',$data['longdescription']);
                $dao->addParameter(':image', $data['image']);
                $dao->addParameter(':sku',$data['sku']);
                $dao->addParameter(':isactive',$data['rdo_status']);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':created_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->addparameter(':category_color',$data['category_color']);
                $dao->addparameter(':category_font_color',$data['category_font_color']);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_ADD_SUC)));
           }
           else
           {
                $id = $ObjCommonDao->getprimarykey('cfmenu_categories',$data['id'],'categoryunkid');
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"categoryname",$data['categoryname'],$id);
                
                if($chk_name==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->RECORD_ALREAD_EXISTS)));
                }
                $chk_sku = $ObjDependencyDao->checkduplicaterecord($tablename,"sku",$data['sku'],$id);
                if($chk_sku==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG6)));
                }
                
                $title = "Edit Record";

                $modifier = $ObjCommonDao->getprimarykey('cfmenu',$data['menuunkid'],'menuunkid');
               $pcategory = 0;
               if(isset($data['pcatid']) && $data['pcatid']!=''){
                   $pcategory = $ObjCommonDao->getprimarykey('cfmenu_categories',$data['pcatid'],'categoryunkid');
               }
               // $menuhours = $ObjCommonDao->getprimarykey('cfmenu_menuhours',$data['menuhoursunkid'],'menuhoursunkid');

                $strSql = "UPDATE ".CONFIG_DBN.".cfmenu_categories 
                            SET 
                            menuunkid=:menuunkid,parent_id=:parent_id, 
                            categoryname=:categoryname, 
                            long_desc=:long_desc, 
                            image=:image, sku=:sku, 
                            modifieddatetime=:modifieddatetime, 
                            modified_user=:modified_user,
                            category_color=:category_color,
                            category_font_color=:category_font_color
                            WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid";
                
                $dao->initCommand($strSql);
                $dao->addParameter(':menuunkid',$modifier);
               $dao->addParameter(':parent_id',$pcategory);
                $dao->addParameter(':hashkey',$data['id']);
                $dao->addParameter(':categoryname',$data['categoryname']);
                $dao->addParameter(':long_desc',$data['longdescription']);
                $dao->addParameter(':image', $data['image']);
                $dao->addParameter(':sku',$data['sku']);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->addparameter(':category_color',$data['category_color']);
                $dao->addparameter(':category_font_color',$data['category_font_color']);
                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_UP_SUC)));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addCategory - '.$e);
        }
    }

    public function categorylist($limit,$offset,$name,$isactive=0)
    {
        try
        {
            $this->log->logIt($this->module.' - categorylist - '.$name);
            
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = "SELECT CFC.*,IFNULL(CFU1.username,'') AS createduser,IFNULL(CFU2.username,'') AS modifieduser,
            IFNULL(DATE_FORMAT(CFC.createddatetime,'".$mysqlformat."'),'') as created_date,
            IFNULL(DATE_FORMAT(CFC.modifieddatetime,'".$mysqlformat."'),'') as modified_date 
            FROM ".CONFIG_DBN.".cfmenu_categories AS CFC 
            LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CFC.created_user=CFU1.userunkid AND CFC.companyid=CFU1.companyid 
            LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON CFC.modified_user=CFU2.userunkid AND CFC.modified_user=CFU2.userunkid AND CFC.companyid=CFU2.companyid         
            WHERE CFC.companyid=:companyid AND CFC.locationid=:locationid AND CFC.is_deleted=0";
            
            if($name!="")
                $strSql .= " AND categoryname LIKE '%".$name."%'";
            if($isactive == 1)
            {
                $strSql .= " and is_active=1 ";
            }
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
           
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $data = $dao->executeQuery();
           
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);          
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $rec = $dao->executeQuery();

            if(count($data) != 0){               
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return html_entity_decode(json_encode($retvalue));
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return html_entity_decode(json_encode($retvalue));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - categorylist - '.$e);
        }
    }
    
    public function getCatRec($data)
    {
		try
        {
			$this->log->logIt($this->module." - getCatRec");
            $ObjCommonDao = new \database\commondao();
			$dao = new \dao();
            $id=(isset($data['id']))?$data['id']:"";
            $strSql = "SELECT A.categoryname, A.long_desc,IFNULL(A.image,'".CONFIG_ASSETS_URL."noimage.png') as image,parent_id,category_color,category_font_color,
                        A.sku, A.is_active, B.hashkey AS menuhashkey
                        FROM ".CONFIG_DBN.".cfmenu_categories AS A 
                        INNER JOIN ".CONFIG_DBN.".cfmenu AS B
                        ON A.menuunkid=B.menuunkid AND B.companyid=:companyid AND B.locationid=:locationid
                        WHERE A.hashkey=:categoryunkid AND A.companyid=:companyid AND A.locationid=:locationid";
             
            $dao->initCommand($strSql);
            $dao->addParameter(':categoryunkid',$id);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res = $dao->executeRow();
            if($res){
                $parenthash =$ObjCommonDao->getFieldValue('cfmenu_categories','hashkey','categoryunkid',$res['parent_id']);
                $res['parenthashkey']=isset($parenthash)?$parenthash:0;
            }
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
           
        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getCatRec - ".$e);
			return false; 
		}
	}

    public function getCategoryList($data)
    {
        try
        {
            $this->log->logIt($this->module." - getItemList");
            $dao = new \dao();

            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = "SELECT CFC.*,IFNULL(CFU1.username,'') AS createduser,IFNULL(CFU2.username,'') AS modifieduser,
            IFNULL(DATE_FORMAT(CFC.createddatetime,'".$mysqlformat."'),'') as created_date,
            IFNULL(DATE_FORMAT(CFC.modifieddatetime,'".$mysqlformat."'),'') as modified_date FROM ".CONFIG_DBN.".cfmenu_categories AS CFC 
            INNER JOIN ".CONFIG_DBN.".cfmenu as CFM ON CFM.menuunkid = CFC.menuunkid AND CFM.companyid = :companyid
            LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CFC.created_user=CFU1.userunkid AND CFC.companyid=CFU1.companyid 
            LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON CFC.modified_user=CFU2.userunkid AND CFC.modified_user=CFU2.userunkid AND CFC.companyid=CFU2.companyid
          
            WHERE CFM.hashkey = :hashkey AND  CFC.companyid=:companyid AND CFC.locationid=:locationid AND CFC.is_deleted=0";

            $dao->initCommand($strSql);
            $dao->addParameter(':hashkey',$data['id']);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);

            $res = $dao->executeQuery();
            return json_encode(array('Success'=>'True', 'Data'=>$res));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getItemList - ".$e);
            return json_encode(array('Success'=>'False'));
        }
    }

    
}
?>