<?php
namespace database;

use libraries\Logger;
use util\util;

class zomato_api_integrationdao
{
    public $module = 'DB_zomato_api_integrationdao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function syncMenu($langlist)
    {
        try
        {
            $this->log->logIt($this->module." - syncMenu") ;
            $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $datetime = \util\util::getLocalDateTime();

            $final_menu_array = $timings = $final_categories_array = array();
            $item_tags = array("veg");
            $itemid = '';


            $strSql = " SELECT CFM.menuunkid AS vendorEntityId,CFM.name,IFNULL(CFM.timings,'') AS timings,@n := @n + 1 as order1
                FROM ".CONFIG_DBN.".cfmenu AS CFM
                ,(SELECT @n := 0) m WHERE CFM.companyid=:companyid AND CFM.locationid=:locationid AND CFM.is_deleted=0 AND CFM.is_active=1 ";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);         // get menu
            $dao->addparameter(':locationid',CONFIG_LID);
            $res_menu = $dao->executeQuery();


                               /* Menu Array */

            if(isset($res_menu) && count($res_menu) >0)
            {
                $final_categories_array =  $res_menu;

                foreach ($res_menu as $rkey=>$rval)
                {
                
                    unset($final_categories_array[$rkey]['order1']);

                    if ($rval['timings'] != '')
                        $final_categories_array[$rkey]['timings'] = array(json_decode(htmlspecialchars_decode($rval['timings']),1));
                    else
                        unset($final_categories_array[$rkey]['timings']);

                    $final_categories_array[$rkey]['name'] = htmlspecialchars_decode($rval['name']);
                    $final_categories_array[$rkey]['vendorEntityId'] = 'Category_Item-'.$rval['vendorEntityId'];

                    $strSql = "SELECT CFC.categoryunkid AS vendorEntityId,CFC.categoryname AS name ,@n := @n + 1 order1                
                   FROM ".CONFIG_DBN.".cfmenu_categories AS CFC
                   ,(SELECT @n := 0) m WHERE CFC.companyid=:companyid AND CFC.locationid=:locationid AND CFC.is_deleted=0 AND CFC.is_active=1  AND CFC.menuunkid=:menuunkid";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid',CONFIG_CID);                  // get sub category
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $dao->addparameter(':menuunkid',$rval['vendorEntityId']);
                    $res_category = $dao->executeQuery();

                    $category = array();

                    if(isset($res_category) && count($res_category) >0)
                    {
                        $category = $res_category;

                        foreach ($res_category as $rCkey=>$rCval) {

                            $category[$rCkey]['name'] = htmlspecialchars_decode($rCval['name']);
                            $category[$rCkey]['vendorEntityId'] = 'SubCategory_Item-'.$rCval['vendorEntityId'];

                          //  $category[$rCkey]['order'] = $rCval['order1'];
                            unset($category[$rCkey]['order1']);

                            $strSql = "SELECT CFI.itemunkid AS vendorEntityId ,@n := @n + 1 order1 
                          FROM " . CONFIG_DBN . ".cfmenu_items AS CFI ,(SELECT @n := 0) m 
                          WHERE CFI.companyid=:companyid AND CFI.locationid=:locationid AND CFI.is_deleted=0 AND CFI.is_active=1 AND CFI.categoryunkid=:categoryunkid";
                            $dao->initCommand($strSql);
                            $dao->addParameter(':companyid', CONFIG_CID);                   // get item
                            $dao->addparameter(':locationid', CONFIG_LID);
                            $dao->addparameter(':categoryunkid', $rCval['vendorEntityId']);
                            $res_item = $dao->executeQuery();

                            $item = array();

                            if (isset($res_item) && count($res_item) > 0) {

                                $item = $res_item;

                                foreach ($res_item as $rIkey => $rIval) {

                                    $item[$rIkey]['entityType'] = 'catalogue';
                                    $item[$rIkey]['vendorEntityId'] = 'ItemId-' . $rIval['vendorEntityId'];

                                  //  $item[$rIkey]['order'] = $rIval['order1'];
                                    unset($item[$rIkey]['order1']);

                                    if($itemid == ''){
                                        $itemid = $rIval['vendorEntityId'];
                                    }else{
                                        $itemid.= ','.$rIval['vendorEntityId'];
                                    }
                                }

                                $category[$rCkey]["entities"] = $item;

                            }
                            else {
                                unset($category[$rCkey]);
                            }
                        }
                        if(count($category)>0)
                        {
                            $final_categories_array[$rkey]["subCategories"] = (array)$category;
                        }else
                        {
                            unset($final_categories_array[$rkey]);
                        }
                    }
                    else
                    {
                        unset($final_categories_array[$rkey]);
                    }
                }
                if(count($final_categories_array)>0)
                {
                    $final_menu_array["categories"] = (array)$final_categories_array;
                }else
                {
                    return json_encode(array('Success'=>'False','Message'=>$langlist->LANG41));
                }
            }else{

                return json_encode(array('Success'=>'False','Message'=>$langlist->LANG42));
            }


                                               /* Catalogues Array */


            $onlymodid =  $modItemid = $modId = '';

            $items = array();

            $strSql = "SELECT CFI.itemunkid AS vendorEntityId,CFI.itemname as name,CFI.long_desc as description,'true' as inStock, IFNULL(CFM.zomato_itemrate,0) as zomato_itemrate FROM " . CONFIG_DBN . ".cfmenu_items AS CFI
                        INNER JOIN ".CONFIG_DBN.".cfmenu_categories as CFC ON CFC.categoryunkid = CFI.categoryunkid AND CFC.companyid=:companyid AND CFC.locationid=:locationid 
                        INNER JOIN ".CONFIG_DBN.".cfmenu as CFM ON CFM.menuunkid = CFC.menuunkid AND CFM.companyid=:companyid AND CFM.locationid=:locationid
                          WHERE CFI.companyid=:companyid AND CFI.locationid=:locationid AND CFI.itemunkid IN (".$itemid.")";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res_items = $dao->executeQuery();

            if (isset($res_items) && count($res_items) > 0) {

                $items = $res_items;

                foreach ($res_items as $rsIkey => $rsIval) {

                    $items[$rsIkey]['vendorEntityId'] = 'ItemId-' . $rsIval['vendorEntityId'];
                    $items[$rsIkey]['tags'] = $item_tags;
                    $items[$rsIkey]['description'] = strip_tags(htmlspecialchars_decode($rsIval['description']));
                    unset($items[$rsIkey]['zomato_itemrate']);

                    $strSql = "SELECT CIUR.default_rate
                                      FROM " . CONFIG_DBN . ".cfmenu_itemunit_rel as CIUR WHERE CIUR.lnkitemid=:itemid AND CIUR.recipe_type=:recipe_type AND CIUR.companyid=:companyid AND CIUR.locationid=:locationid AND CIUR.is_deleted=0";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':itemid', $rsIval['vendorEntityId']);
                    $dao->addParameter(':recipe_type', '1');                  // get item unit
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $res_itemunitdefaultrate = $dao->executeRow();

                    $zomatorate = $rsIval['zomato_itemrate'];
                    if($rsIval['zomato_itemrate'] == 0)
                    {
                        $zomatorate = $res_itemunitdefaultrate['default_rate'];
                    }

                    $strSql = "SELECT CIUR.itmunitunkid AS vendorEntityId,CIU.name as value,CIU.unitunkid,
                               CASE WHEN CIUR.rate" . $zomatorate . " = 0 THEN ROUND(CIUR.rate" . $res_itemunitdefaultrate['default_rate'] . ",$round_off) ELSE ROUND(CIUR.rate" . $zomatorate . ",$round_off) END AS price, 
                               CASE WHEN CIUR.rate" . $zomatorate . " = 0 THEN ".$res_itemunitdefaultrate['default_rate']." ELSE " . $zomatorate . " END AS item_rate
                              FROM " . CONFIG_DBN . ".cfmenu_itemunit_rel as CIUR
                              INNER JOIN " . CONFIG_DBN . ".cfmenu_itemunit AS CIU
                              ON CIU.unitunkid=CIUR.lnkitemunitid AND CIU.companyid=:companyid AND CIU.locationid=:locationid
                              WHERE CIUR.lnkitemid=:itemid AND CIUR.recipe_type=:recipe_type AND CIUR.companyid=:companyid 
                              AND CIUR.locationid=:locationid AND CIUR.is_deleted=0";

                    $dao->initCommand($strSql);

                    $dao->addParameter(':itemid', $rsIval['vendorEntityId']);
                    $dao->addParameter(':recipe_type', '1');                  // get item unit
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $res_itemunit = $dao->executeQuery();

                    $properties = array("name"=>"Select Unit","order"=>1,"vendorEntityId"=>'P_'.$rsIval['vendorEntityId']);

                    $propertyValues = $propertyValues_varient = $price = $variants = array();

                    if (isset($res_itemunit) && count($res_itemunit) > 0) {

                        $modifiers = array();

                        foreach ($res_itemunit as $rUkey => $rUval) {

                                  /* properties */

                            $propertyValues[$rUkey]['value'] = htmlspecialchars_decode($rUval['value']);
                            $propertyValues[$rUkey]['vendorEntityId'] = 'ItemUnitid-' . $rUval['vendorEntityId'] . "-" . $rUval['item_rate'] . "-" . $rUval['unitunkid'];


                                      /* variants */

                            $propertyValues_varient[0]['vendorEntityId'] = 'ItemUnitid-' . $rUval['vendorEntityId'] . "-" . $rUval['item_rate'] . "-" . $rUval['unitunkid'];

                            $price[0]['service'] = 'delivery';
                            $price[0]['price'] = $rUval['price'];

                            $variants[$rUkey]["vendorEntityId"] = 'V_'.$rsIval['vendorEntityId'].'_'.$rUval['vendorEntityId'];
                            $variants[$rUkey]["propertyValues"] = $propertyValues_varient;
                            $variants[$rUkey]["prices"] = $price;


                                      /* Modifiers Array */

                            $strSql="SELECT
                                            CASE WHEN CFM.is_included=0 THEN CONCAT('MODIFIER-',FDIM.relationunkid) WHEN CFM.is_included=1 THEN CONCAT('MODIFIERITEM-',FDMI.relationunkid) END AS item_id , 
                                            CONCAT('MODIFIER-',FDIM.relationunkid) AS vendorEntityId , 
                                            CASE WHEN CFM.is_included=0 THEN FDIM.relationunkid WHEN CFM.is_included=1 THEN FDMI.relationunkid END AS modifierId ,                                            
                                            @n := @n + 1 order1,                                            
                                            FDIM.relationunkid as onlymodid ";
                            $strSql.="  FROM ".CONFIG_DBN.".fditem_modifier_relation AS FDIM  ";
                            $strSql.="  INNER JOIN ".CONFIG_DBN.".cfmenu_modifiers AS CFM ON FDIM.lnkmodifierid = CFM.modifierunkid AND CFM.companyid = :companyid AND CFM.locationid=:locationid";
                            $strSql.="  LEFT JOIN ".CONFIG_DBN.".fdmodifier_item_relation AS FDMI ON FDMI.lnkmodifierrelid = FDIM.relationunkid AND FDMI.companyid = :companyid AND FDMI.locationid=:locationid AND FDMI.is_deleted=0 ";
                            $strSql.=" ,(SELECT @n := 0) m ";
                            $strSql.="  WHERE FDIM.lnkitemid = :itemid AND FDIM.lnkitemunitid=:lnkitemunitid AND FDIM.companyid = :companyid AND FDIM.locationid=:locationid AND FDIM.is_deleted=0 ";

                            $dao->initCommand($strSql);
                            $dao->addParameter(':itemid',$rsIval['vendorEntityId']);
                            $dao->addParameter(':lnkitemunitid',$rUval['vendorEntityId']);
                            $dao->addParameter(':companyid',CONFIG_CID);
                            $dao->addparameter(':locationid',CONFIG_LID);
                            $res_mod = $dao->executeQuery();

                            if(isset($res_mod) && count($res_mod) >0) {

                                $modifiers = $res_mod;

                                foreach ($res_mod as $rMkey => $rMval) {

                                    unset($modifiers[$rMkey]['modifierId']);
                                    unset($modifiers[$rMkey]['onlymodid']);
                                 //   $modifiers[$rMkey]['order'] = $rMval['order1'];
                                    unset($modifiers[$rMkey]['order1']);
                                    unset($modifiers[$rMkey]['item_id']);

                                    if($onlymodid == '')
                                        $onlymodid = $rMval['onlymodid'];
                                    else
                                        $onlymodid.= ','.$rMval['onlymodid'];


                                    if (strpos($rMval['item_id'], 'MODIFIERITEM') !== false) {
                                        if($modItemid == '')
                                           $modItemid = $rMval['modifierId'];
                                        else
                                            $modItemid.= ','.$rMval['modifierId'];
                                    }else{
                                        if($modId == '')
                                            $modId = $rMval['modifierId'];
                                        else
                                            $modId.= ','.$rMval['modifierId'];
                                    }

                                }

                                $variants[$rUkey]["modifierGroups"] = $modifiers;

                            }

                        }
                        $properties["propertyValues"] = $propertyValues;
                    }


                    $strSql = "SELECT CFD.zomato_tax AS tax_id                                                          
                                          FROM ".CONFIG_DBN.".cfitemtax_rel AS CITR
                                          INNER JOIN ".CONFIG_DBN.".cfmenu_items AS CFM ON CFM.itemunkid = CITR.lnkitemid AND FIND_IN_SET(".$rUval['item_rate'].",CFM.tax_apply_rate) > 0 
                                          INNER JOIN ".CONFIG_DBN.".cftaxdetail CFD ON CITR.lnktaxunkid = CFD.taxunkid
 AND CFD.taxdetailunkid = (SELECT taxdetailunkid FROM ".CONFIG_DBN.".cftaxdetail 
                                          WHERE taxunkid = CITR.lnktaxunkid ORDER BY entrydatetime DESC LIMIT 1) 
                                          WHERE CITR.lnkitemid=:itemid AND CITR.item_type=:item_type AND CITR.companyid=:companyid AND CITR.locationid=:locationid AND CITR.is_deleted=0";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':itemid',$rsIval['vendorEntityId']);
                    $dao->addParameter(':item_type', '1');
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $res_itemtaxdata = $dao->executeQuery();


                    if(isset($res_itemtaxdata) && count($res_itemtaxdata) >0 && count($res_itemunit) >0)
                    {
                        $alltaxids_item =  util::getoneDarray($res_itemtaxdata,'tax_id');

                        $items[$rsIkey]["taxes"] = $alltaxids_item;
                    }

                    $strSql = " SELECT CFC.chargesunkid AS vendorEntityId
                            FROM ".CONFIG_DBN.".cfcharges AS CFC
                            WHERE CFC.charge_type IN(1) AND CFC.apply_zomato=1
                             AND CFC.companyid=:companyid AND CFC.locationid =:locationid ";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addparameter(':locationid',CONFIG_LID);
                    $res_item_charges = $dao->executeQuery();

                    if(isset($res_item_charges) && count($res_item_charges) >0)
                    {
                        $items[$rsIkey]["charges"] = $res_item_charges;
                    }

                    $items[$rsIkey]["properties"] = array($properties);
                    $items[$rsIkey]["variants"] = $variants;
                }

                $final_menu_array["catalogues"] = (array)$items;
            }


                     /* modifiers merge in catelogue like items (whole modifier discription) */


            $strSql="SELECT
                                            CASE WHEN CFM.is_included=0 THEN CONCAT('MODIFIER-',FDIM.relationunkid,'-',FDIM.min) WHEN CFM.is_included=1 THEN CONCAT('MODIFIERITEM-',FDMI.relationunkid,'-',FDIM.min) END AS vendorEntityId , 
                                            CASE WHEN CFM.is_included=0 THEN CONCAT('MODIFIER-',FDIM.relationunkid,'-', CFM.modifierunkid) WHEN CFM.is_included=1 THEN CONCAT('MODIFIERITEM-',FDMI.relationunkid,'-',CFMI.modifieritemunkid) END AS modifierunkid ,
                                            CASE WHEN CFM.is_included=0 THEN CONCAT(CFM.modifiername,'  X  ',FDIM.min) WHEN CFM.is_included=1 THEN CONCAT(CFMI.itemname,'  X  ',FDIM.min) END AS name ,
                                            CASE WHEN CFM.is_included=0 THEN IFNULL(ROUND(FDIM.min*FDIM.sale_amount,$round_off),0) WHEN CFM.is_included=1 THEN IFNULL(ROUND(FDIM.min*FDMI.sale_amount,$round_off),0) END AS price 
                                              
                                             ";

            $strSql.="  FROM ".CONFIG_DBN.".fditem_modifier_relation AS FDIM ";
            $strSql.="  INNER JOIN ".CONFIG_DBN.".cfmenu_modifiers AS CFM ON FDIM.lnkmodifierid = CFM.modifierunkid AND CFM.companyid = :companyid AND CFM.locationid=:locationid";
            $strSql.="  LEFT JOIN ".CONFIG_DBN.".fdmodifier_item_relation AS FDMI ON FDMI.lnkmodifierrelid = FDIM.relationunkid  AND FDMI.companyid = :companyid AND FDMI.locationid=:locationid AND FDMI.is_deleted=0 AND FDMI.relationunkid IN (".$modItemid.") ";
            $strSql.="  LEFT JOIN ".CONFIG_DBN.".cfmenu_modifier_items AS CFMI ON CFMI.modifieritemunkid = FDMI.lnkmodifieritemid AND CFMI.companyid = :companyid AND CFMI.locationid=:locationid";
//            $strSql.="  LEFT JOIN ".CONFIG_DBN.".cfmenu_itemunit_rel AS CFU ON CFU.lnkitemid = FDIM.lnkmodifierid AND CFU.companyid = :companyid AND CFU.locationid=:locationid AND CFU.recipe_type=2";
//            $strSql.="  LEFT JOIN ".CONFIG_DBN.".cfmenu_itemunit_rel AS CFU2 ON CFU2.lnkitemid = FDMI.lnkmodifieritemid AND CFU2.companyid = :companyid AND CFU2.locationid=:locationid AND CFU2.recipe_type=3";
            $strSql.="  WHERE FDIM.relationunkid IN (".$onlymodid.") AND FDIM.companyid = :companyid AND FDIM.locationid=:locationid AND FDIM.is_deleted=0 ";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $catlogue_mod = $dao->executeQuery();

            $catlogue_modifiers = array();

            if(isset($catlogue_mod) && count($catlogue_mod) >0) {

                $M_price = $M_variants = array();

                foreach ($catlogue_mod as $rCMkey => $rCMval) {

                    $catlogue_modifiers[$rCMkey]['name'] = htmlspecialchars_decode($rCMval['name']);
                    $catlogue_modifiers[$rCMkey]['vendorEntityId'] = $rCMval['modifierunkid'];
                    $catlogue_modifiers[$rCMkey]['inStock'] = 1;

                    $modstring = explode('-', $rCMval['modifierunkid']);

                    /* variants */

                    $M_price[0]['service'] = 'delivery';
                    $M_price[0]['price'] = $rCMval['price'];

                    $M_variants[0]["vendorEntityId"] = $rCMval['vendorEntityId'];
                    $M_variants[0]["prices"] = $M_price;

                    $catlogue_modifiers[$rCMkey]['variants'] = $M_variants;

                


 

                }

                $merge_get_catalogues = $final_menu_array["catalogues"];

                $merge_catalogues = array_merge($merge_get_catalogues,$catlogue_modifiers);

                $final_menu_array["catalogues"] = $merge_catalogues;

            }


                                                /* modifierGroups Array */

            $mod_ary = $mVariants = array();


            $strSql="SELECT CFM.modifiername AS name , CFM.modifiername AS displayName ,CONCAT('MODIFIER-',FDIM.relationunkid) AS vendorEntityId , CFM.is_included,FDIM.relationunkid ,FDIM.min ";

            $strSql.="  FROM ".CONFIG_DBN.".fditem_modifier_relation AS FDIM ";
            $strSql.="  INNER JOIN ".CONFIG_DBN.".cfmenu_modifiers AS CFM ON FDIM.lnkmodifierid = CFM.modifierunkid AND CFM.companyid = :companyid AND CFM.locationid=:locationid";
            $strSql.="  WHERE FDIM.relationunkid IN (".$onlymodid.") AND FDIM.companyid = :companyid AND FDIM.locationid=:locationid AND FDIM.is_deleted=0 ";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res_mod1 = $dao->executeQuery();

            if (isset($res_mod1) && count($res_mod1) > 0) {

                $mod_ary = $res_mod1;

                foreach ($res_mod1 as $rMkey1 => $rMval1) {

                    if ($rMval1['is_included'] == 1) {

                        $strSql = " SELECT CONCAT('MODIFIERITEM-',FDMI.relationunkid,'-'," . $rMval1['min'] . ") AS vendorEntityId FROM " . CONFIG_DBN . ".fdmodifier_item_relation AS FDMI ";
                        $strSql .= " LEFT JOIN " . CONFIG_DBN . ".cfmenu_modifier_items AS CFMI ON CFMI.modifieritemunkid = FDMI.lnkmodifieritemid AND CFMI.companyid = :companyid AND CFMI.locationid=:locationid ";
                        $strSql .= " WHERE FDMI.companyid = :companyid AND FDMI.locationid=:locationid AND FDMI.is_deleted=0 AND FDMI.lnkmodifierrelid = :lnkmodifierrelid ";
                        $dao->initCommand($strSql);
                        $dao->addParameter(':companyid', CONFIG_CID);
                        $dao->addparameter(':locationid', CONFIG_LID);
                        $dao->addparameter(':lnkmodifierrelid', $rMval1['relationunkid']);
                        $res_modItem1 = $dao->executeQuery();


                        $mod_ary[$rMkey1]['variants'] = $res_modItem1;
                        $mod_ary[$rMkey1]['max'] = count($res_modItem1);
                        $mod_ary[$rMkey1]['min'] = 0;

                    } else {

                        $mVariants[0]['vendorEntityId'] = $rMval1['vendorEntityId'].'-'.$rMval1['min'];
                        $mod_ary[$rMkey1]['variants'] = $mVariants;
                        $mod_ary[$rMkey1]['max'] = 1;
                        $mod_ary[$rMkey1]['min'] = 0;
                    }
                    unset($mod_ary[$rMkey1]['relationunkid']);
                    unset($mod_ary[$rMkey1]['is_included']);

                }
                $final_menu_array["modifierGroups"] = (array)$mod_ary;
            }


                                      /*  Charges Array  for Only Packaging Charge  */

            $strSql = " SELECT CFC.chargesunkid AS vendorEntityId,
                               CASE WHEN CFC.type = 1 THEN 'PC_D_P' WHEN CFC.type = 2 THEN 'PC_D_F' END AS slug,
                               IFNULL(ROUND(CFC.amount,$round_off),0) AS chargeValue,'false' AS multiItem,'false' AS applicableOnItem
                            FROM ".CONFIG_DBN.".cfcharges AS CFC
                            WHERE CFC.charge_type IN(1) AND CFC.apply_zomato=1 AND CFC.companyid=:companyid AND CFC.locationid =:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res_charges = $dao->executeQuery();

                                   /* Charges Array for Only Delivery charge */


            $strSql = " SELECT '00f14ad109fe534ed19279b33b383deea0' AS outletId,                              
                               IFNULL(ROUND(CFC.amount,$round_off),0) AS value,
                               CASE WHEN CFC.charge_always_applicable = 1 THEN 'true' ELSE 'false' END AS alwaysApplicable,IFNULL(CFC.charge_applicable_below_order_amount,'') AS applicableBelowOrderAmount 
                            FROM ".CONFIG_DBN.".cfcharges AS CFC
                            WHERE CFC.charge_type IN(2) AND CFC.apply_zomato=1 AND CFC.companyid=:companyid AND CFC.locationid =:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',CONFIG_LID);
            $res_charges_delivery = $dao->executeRow();



                                                 /* Combo Integration */

            $strSql = " SELECT IFNULL(CFM.timings,'') AS timings,CFM.zomato_itemrate,CFM.merge_combo_with_menu FROM ".CONFIG_DBN.".cfcombozomatosetting AS CFM WHERE CFM.companyid=:companyid AND CFM.locationid=:locationid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);                       // create combo menu
            $dao->addparameter(':locationid',CONFIG_LID);
            $res_combosetting = $dao->executeRow();

            if(isset($res_combosetting) && !empty($res_combosetting)) {
                if ($res_combosetting['merge_combo_with_menu'] == 1) {

                    $combo_category = array("vendorEntityId" => "Category_Combo-123", "name" => "Combo");
                    $combo_subCategories = array("vendorEntityId" => "SubCategory_Combo-123", "name" => "Combo");

                    $strSql = " SELECT CFI.combounkid AS vendorEntityId , 'combo' AS entityType FROM " . CONFIG_DBN . ".cfmenu_combo AS CFI                                           
                        WHERE CFI.companyid=:companyid AND CFI.locationid=:locationid AND CFI.is_deleted=0 AND CFI.is_active=1 ";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid', CONFIG_CID);             // get combo
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $res_combo = $dao->executeQuery();

                    $combo = array();

                    if (isset($res_combo) && count($res_combo) > 0) {

                        $combo = $res_combo;

                        foreach ($res_combo as $rCmkey => $rCmval) {

                            $combo[$rCmkey]['vendorEntityId'] = "ComboId-".$rCmval['vendorEntityId'];
                        }

                        $combo_subCategories['entities'] = $combo;
                        $combo_category['subCategories'] = $combo_subCategories;

                        $this->log->logIt($combo_category);

                        /* Pending from Combo integration */

                    }

                }
            }

                                               /* End Combo Integration and pending from Here  */



            // 00f14ad109fe534ed19279b33b383deea0 locationid take as outletId (static id)

            $final_request_body = array("outletId"=>"00f14ad109fe534ed19279b33b383deea0","menu"=>$final_menu_array,"charges"=>isset($res_charges) && !empty($res_charges)?$res_charges:[]);
            $final_request_body_json = json_encode($final_request_body);

            $this->log->logIt('================================================ Zomato Request');
            $this->log->logIt($final_request_body);
            $this->log->logIt($final_request_body_json);


                    // outletid dynamic we have to pass

            $ObjCommonDao = new \database\commondao();
            $Zappkey = $ObjCommonDao->getZomatoAppkey();
            $Zsetting = $ObjCommonDao->getZomatosetting();

            if($Zsetting == 1 && $Zappkey != '')
            {

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, CONFIG_ZOMATO_MENUINTEGRATION_URL);
                curl_setopt($ch, CURLOPT_TIMEOUT, 180);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $final_request_body_json);
                curl_setopt($ch, CURLOPT_HTTPHEADER , array(
                        "Content-Type:application/json",
                        "x-zomato-api-key:".$Zappkey
                    )
                );
                $res=curl_exec($ch);
                curl_close($ch);
                $data_json = trim($res);

                $data_array = json_decode($data_json,1);
                $this->log->logIt('================================================ Zoamto Response');
                $this->log->logIt($data_array);


                if(isset($data_array) && !empty($data_array))
                {
                    $strSql = "INSERT INTO " . CONFIG_DBN . ".cfzomatoMenuIntegrateResponse (request_id,menujson,valid,processed,message,companyid,locationid, createddatetime,created_user)
                        VALUE(:request_id,:menujson,:valid,:processed,:message,:companyid,:locationid,:createddatetime, :created_user)";

                    $dao->initCommand($strSql);
                    if($data_array['menu_response']['valid'] == 1 && $data_array['menu_response']['processed'] == 1) {
                        $dao->addParameter(':request_id', $data_array['request_id']);

                        if(isset($res_charges_delivery) && !empty($res_charges_delivery)){

                            $res_charges_delivery_json = json_encode($res_charges_delivery);
                            $this->log->logIt('================================================ Zoamto Delivery Request');
                            $this->log->logIt($res_charges_delivery_json);

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, CONFIG_ZOMATO_DELIVERYCHARGEUPDATE_URL);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 180);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $res_charges_delivery_json);
                            curl_setopt($ch, CURLOPT_HTTPHEADER , array(
                                    "Content-Type:application/json",
                                    "x-zomato-api-key:".$Zappkey
                                )
                            );
                            $res1=curl_exec($ch);
                            curl_close($ch);
                            $data_json1 = trim($res1);
                            $data_array1 = json_decode($data_json1,1);
                            $this->log->logIt('================================================ Zoamto Delivery Response');
                            $this->log->logIt($data_array1);
                        }
                    }else {
                        $dao->addParameter(':request_id', '');
                    }
                    $dao->addParameter(':menujson', htmlspecialchars($final_request_body_json));
                    $dao->addParameter(':valid', $data_array['menu_response']['valid']);
                    $dao->addParameter(':processed', $data_array['menu_response']['processed']);
                    $dao->addParameter(':message', $data_array['menu_response']['message']);
                    $dao->addParameter(':createddatetime', $datetime);
                    $dao->addParameter(':created_user', CONFIG_UID);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $dao->executeNonQuery();

                    if($data_array['menu_response']['valid'] == 1 && $data_array['menu_response']['processed'] == 1) {
                        return json_encode(array('Success'=>'True','Message'=>$langlist->LANG38));
                    }else
                    {
                        return json_encode(array('Success'=>'False','Message'=>$data_array['menu_response']['message']));
                    }
                }

            }else
            {
                return json_encode(array('Success'=>'False','Message'=>$langlist->LANG37));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - syncMenu - ".$e);
            return false;
        }
    }

    
}





?>
