<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 16/4/18
 * Time: 3:37 PM
 */

namespace database;
class issuevoucherdao
{
    public $module = 'DB_issuevoucherdao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function createdVoucherList($limit, $offset, $fromDate, $toDate, $voucherNo, $store)
    {
        try {
            $this->log->logIt($this->module . ' - createdVoucherList');
            $ObjUtil = new \util\util;
            $from_date = $ObjUtil->convertDateToMySql($fromDate);
            $to_date = $ObjUtil->convertDateToMySql($toDate);
            $dao = new \dao;
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $strSql = "SELECT CFIV.lnkindentid,CFIV.issueid,CFIV.issue_doc_num,DATE_FORMAT(CFIV.issue_date,'" . $mysql_format . "') as issueDate,IFNULL(CFRI.indent_doc_num,'') as indentNo," .
                "CFSM.storename,IFNULL(CFU1.username,'') AS created_user FROM " . CONFIG_DBN . ".cfissuevoucher CFIV " .
                "LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS CFSM ON CFSM.storeunkid=CFIV.relstoreid AND CFSM.companyid =:companyid " .
                "LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU1 ON CFIV.createduser=CFU1.userunkid AND CFU1.companyid =:companyid " .
                "LEFT JOIN " . CONFIG_DBN . ".cfrequestindent AS CFRI ON CFRI.indentid=CFIV.lnkindentid AND CFRI.companyid=:companyid AND CFRI.storeid = CFIV.relstoreid " .
                "WHERE CFIV.companyid=:companyid AND CFIV.storeid=:storeid AND CFIV.is_deleted=0";
            if ($voucherNo != "") {
                $strSql .= " AND CFIV.issue_doc_num LIKE '%" . $voucherNo . "%'";
            }
            if ($store != 0) {
                $strSql .= " AND CFIV.relstoreid = '" . $store . "'";
            }
            if ($from_date != "" && $to_date != "") {
                $strSql .= " AND CFIV.issue_date BETWEEN '" . $from_date . "' AND '" . $to_date . "' ";
            }
            $strSql .= " ORDER BY CFIV.issueid DESC";
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqlLmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $resObj = $dao->executeQuery();

            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $dao->initCommand($strSql . $strSqlLmt);
            } else {
                $dao->initCommand($strSql);
            }
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $rec = $dao->executeQuery();
            if (count($resObj) != 0) {
                $ret_value = array(array("cnt" => count($resObj), "data" => $rec));
                return json_encode($ret_value);
            } else {
                $ret_value = array(array("cnt" => 0, "data" => []));
                return json_encode($ret_value);
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - createdVoucherList - ' . $e);
        }
    }

    public function receivedVoucherList($limit, $offset, $fromDate, $toDate, $voucherNo, $store)
    {
        try {
            $this->log->logIt($this->module . ' - receivedVoucherList');
            $ObjUtil = new \util\util;
            $from_date = $ObjUtil->convertDateToMySql($fromDate);
            $to_date = $ObjUtil->convertDateToMySql($toDate);
            $dao = new \dao;
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $strSql = "SELECT CFIV.issueid,CFIV.issue_doc_num,
            DATE_FORMAT(CFIV.issue_date,'" . $mysql_format . "') as issueDate,IFNULL(CFRI.indent_doc_num,'') as indentNo," .
                "CFSM.storename,IFNULL(CFU1.username,'') AS created_user FROM " . CONFIG_DBN . ".cfissuevoucher CFIV " .
                "LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS CFSM ON CFSM.storeunkid=CFIV.storeid AND CFSM.companyid =:companyid " .
                "LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU1 ON CFIV.createduser=CFU1.userunkid AND CFU1.companyid =:companyid " .
                "LEFT JOIN " . CONFIG_DBN . ".cfrequestindent AS CFRI ON CFRI.indentid=CFIV.lnkindentid AND CFRI.companyid=:companyid AND CFRI.storeid = CFIV.relstoreid " .
                "WHERE CFIV.companyid=:companyid AND CFIV.relstoreid=:storeid AND CFIV.is_deleted=0";
            if ($voucherNo != "") {
                $strSql .= " AND CFIV.issue_doc_num LIKE '%" . $voucherNo . "%'";
            }
            if ($store != 0) {
                $strSql .= " AND CFIV.storeid = '" . $store . "'";
            }
            if ($from_date != "" && $to_date != "") {
                $strSql .= " AND CFIV.issue_date BETWEEN '" . $from_date . "' AND '" . $to_date . "' ";
            }
            $strSql .= " ORDER BY CFIV.issueid DESC";
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqlLmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $resObj = $dao->executeQuery();

            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $dao->initCommand($strSql . $strSqlLmt);
            } else {
                $dao->initCommand($strSql);
            }
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $rec = $dao->executeQuery();
            if (count($resObj) != 0) {
                $ret_value = array(array("cnt" => count($resObj), "data" => $rec));
                return json_encode($ret_value);
            } else {
                $ret_value = array(array("cnt" => 0, "data" => []));
                return json_encode($ret_value);
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - receivedVoucherList - ' . $e);
        }
    }

    public function getissuedvoucherdetail($id)
    {
        try {
            $this->log->logIt($this->module . ' - getissuedvoucherdetail');
            $dao = new \dao();

            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $resarr = array();
            $strSql = "SELECT C.companyname,C.address1 AS company_address,C.phone,C.currency_sign,C.companyemail,
                        CFV.issue_doc_num AS voucher_num,CFV.lnkindentid AS indentid,IFNULL(logo,'') as company_logo,
                        CFV.remarks,DATE_FORMAT(CFV.issue_date,'" . $mysql_format . "') as Voucher_Date,IFNULL(CFU1.username,'') AS createduser,
                        ROUND(IFNULL(CFV.totalamount,0),$round_off) as totalamount,
                        S.storename as issuing_store,ST.storename AS rec_store 
                        FROM " . CONFIG_DBN . ".cfissuevoucher AS CFV 
                        LEFT JOIN " . CONFIG_DBN . ".cfcompany as C ON CFV.companyid= C.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster as S ON CFV.storeid=S.storeunkid AND S.companyid=:companyid AND S.is_active=1 AND S.is_deleted=0
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS ST on CFV.relstoreid=ST.storeunkid AND ST.companyid=:companyid AND ST.is_active=1 AND ST.is_deleted=0
                        LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU1 ON CFV.createduser=CFU1.userunkid AND CFV.companyid=CFU1.companyid
                        WHERE CFV.relstoreid=:storeid AND CFV.is_deleted=0  AND
                         CFV.companyid=:companyid AND CFV.issueid=:issueid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $dao->addparameter(':issueid', $id);
            $issuerecords = $dao->executeRow();


            $resarr['company_name'] = $issuerecords['companyname'];
            $resarr['address'] = $issuerecords['company_address'];
            $resarr['phone'] = $issuerecords['phone'];
            $resarr['email'] = $issuerecords['companyemail'];
            $resarr['company_logo'] = !empty($issuerecords['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$issuerecords['company_logo']:'';
            $resarr['currency_sign'] = $issuerecords['currency_sign'];
            $resarr['voucher_num'] = $issuerecords['voucher_num'];
            $resarr['remarks'] = $issuerecords['remarks'];
            $resarr['Voucher_Date'] = $issuerecords['Voucher_Date'];
            $resarr['totalamount'] = $issuerecords['totalamount'];
            $resarr['issuing_store'] = $issuerecords['issuing_store'];
            $resarr['rec_store'] = $issuerecords['rec_store'];
            $resarr['createduser'] = $issuerecords['createduser'];
            $str = "SELECT  CRM.name AS Item,IVD.qty,U.shortcode as Unitname,IVD.rate,
                    IFNULL(ROUND((IVD.qty*IVD.rate),$round_off),'') AS Amount 
                    FROM " . CONFIG_DBN . ".cfissuevoucherdetail AS IVD 
                    LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CRM ON IVD.lnkrawid=CRM.id AND CRM.companyid=:companyid AND CRM.is_active=1 AND CRM.is_deleted=0
                    LEFT JOIN " . CONFIG_DBN . ".cfunit AS U ON IVD.lnkunitid=U.unitunkid AND U.companyid=:companyid AND U.is_active=1 AND U.is_deleted=0
                    WHERE IVD.companyid=:companyid AND IVD.lnkissueid=:issueid AND IVD.lnkindentid=:lnkindentid";
            $dao->initCommand($str);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':issueid', $id);
            $dao->addparameter(':lnkindentid', $issuerecords['indentid']);
            $issuedetail = $dao->executeQuery();
            $resarr['issuedetail'] = $issuedetail;
            $total = $to = 0;
            foreach ($issuedetail AS $val) {
                $to = $val['qty'] * $val['rate'];
                $total += $to;
            }
            $resarr['totalamount'] = number_format($total, $round_off);
            $resarr['current_date'] = \util\util::convertMySqlDate($current_date);

            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getissuedvoucherdetail - ' . $e);
        }
    }

    public function getissuedetail($id, $defaultlanguageArr = '')
    {
        try {
            $this->log->logIt($this->module . ' - getissuedetail');
            $dao = new \dao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $resarr = array();
            $strSql = "SELECT C.companyname,C.address1 AS company_address,C.phone,C.currency_sign,C.companyemail,
                        CFV.issue_doc_num AS voucher_num,CFV.lnkindentid AS indentid,IFNULL(logo,'') as company_logo,
                        CFV.remarks,DATE_FORMAT(CFV.issue_date,'" . $mysql_format . "') as Voucher_Date,IFNULL(CFU1.username,'') AS createduser,
                        ROUND(IFNULL(CFV.totalamount,0),$round_off) as totalamount,
                        S.storename as issuing_store,ST.storename AS rec_store 
                        FROM " . CONFIG_DBN . ".cfissuevoucher AS CFV 
                        LEFT JOIN " . CONFIG_DBN . ".cfcompany as C ON CFV.companyid= C.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster as S ON CFV.storeid=S.storeunkid AND S.companyid=:companyid AND S.is_active=1 AND S.is_deleted=0
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS ST on CFV.relstoreid=ST.storeunkid AND ST.companyid=:companyid AND ST.is_active=1 AND ST.is_deleted=0
                        LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU1 ON CFV.createduser=CFU1.userunkid AND CFV.companyid=CFU1.companyid
                        WHERE CFV.storeid=:storeid AND CFV.is_deleted=0  AND
                        CFV.companyid=:companyid AND CFV.issueid=:issueid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $dao->addparameter(':issueid', $id);
            $issuerecords = $dao->executeRow();

            $resarr['company_name'] = $issuerecords['companyname'];
            $resarr['address'] = $issuerecords['company_address'];
            $resarr['phone'] = $issuerecords['phone'];
            $resarr['email'] = $issuerecords['companyemail'];
            $resarr['company_logo'] = !empty($issuerecords['company_logo'])?CONFIG_BASE_URL."companyadmin/assets/company_logo/".$issuerecords['company_logo']:'';
            $resarr['currency_sign'] = $issuerecords['currency_sign'];
            $resarr['voucher_num'] = $issuerecords['voucher_num'];
            $resarr['remarks'] = $issuerecords['remarks'];
            $resarr['Voucher_Date'] = $issuerecords['Voucher_Date'];
            $resarr['totalamount'] = $issuerecords['totalamount'];
            $resarr['issuing_store'] = $issuerecords['issuing_store'];
            $resarr['rec_store'] = $issuerecords['rec_store'];
            $resarr['createduser'] = $issuerecords['createduser'];

            $strissueDetail = "SELECT CR.name AS storeitem,CU.shortcode AS unit,CPD.lnkissueid,CPD.issuerequnkid,CPD.lnkrawid,CPD.lnkrawcatid," .
                "CPD.lnkunitid,ROUND(CPD.qty,$round_off) AS qty,ROUND(CPD.rate,$round_off) AS rate," .
                "CPD.discount_per,ROUND(CPD.discount_amount,$round_off) AS discount_amount,ROUND(CPD.tax_amount,$round_off) AS tax_amount," .
                "CPD.tax_description,ROUND(CPD.total,$round_off) AS total FROM " . CONFIG_DBN . ".cfissuevoucherdetail AS CPD " .
                "LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CPD.lnkrawid=CR.id " .
                "LEFT JOIN " . CONFIG_DBN . ".cfunit CU ON CPD.lnkunitid=CU.unitunkid AND " .
                "CU.companyid=:companyid AND CU.is_active=1 AND CU.is_deleted=0 " .
                "WHERE CPD.lnkissueid=:lnkissueid AND CPD.companyid=:companyid ";
            $dao->initCommand($strissueDetail);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':lnkissueid', $id);
            $issuedetail = $dao->executeQuery();

            $taxTOtal = 0;

            if ($issuedetail) {
                foreach ($issuedetail as $key => $value) {
                    $taxname = array();
                    $issuetaxdetail = json_decode($value['tax_description'], 1);
                    $taxeslist_dis ='';
                    if($issuetaxdetail)
                    {
                        foreach ($issuetaxdetail as $key2 => $value2) {
                            $Tax_NAME ='';
                            $posting_type=isset($issuerecords['currency_sign'])?$issuerecords['currency_sign']:'Flat';
                            if(empty($value2['tax_name']))
                            {
                                $str = "SELECT tax FROM " . CONFIG_DBN . ".cftax
                            WHERE  companyid=:companyid AND storeid=:storeid  AND taxunkid=:taxunkid";
                                $dao->initCommand($str);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addParameter(':storeid', CONFIG_SID);
                                $dao->addParameter(':taxunkid', $value2['tax_id']);
                                $qtaxname = $dao->executeRow();
                                $Tax_NAME=$qtaxname['tax'];
                            }
                            else{
                                $Tax_NAME=$value2['tax_name'];
                            }if(isset($value2['posting_rule']) && $value2['posting_rule'] == 1)
                            {
                                $posting_type='%';
                            }
                            $taxeslist_dis .= "<b>".$Tax_NAME."</b> : (".number_format($value2['tax_value'],$round_off,'.','')." ".$posting_type.") : ".number_format($value2['tax_amt'], $round_off,'.','')." <br>";
                            $taxname[$value2['tax_id']] = $value2['tax_amt'];
                        }
                    }
                    $taxTOtal = ($taxTOtal+$value['tax_amount']);
                    $issuedetail[$key]['tax_description'] = isset($taxeslist_dis)?$taxeslist_dis:'';
                }
            }
            $resarr['issuedetail'] = $issuedetail;
            $resarr['Tax_Total'] =  number_format($taxTOtal,$round_off,'.','');

            $Discounttotal = $Dto = 0;
            foreach ($issuedetail AS $val) {
                $to = $val['discount_amount'];
                $Discounttotal += $to;
            }

            $resarr['Discounttotal'] =number_format($Discounttotal,$round_off,'.','');
            $numberinwords = \util\util::convertNumber($issuerecords['totalamount'], '', '', '', $defaultlanguageArr);

            $resarr['numberinwords'] = $numberinwords;
            $resarr['current_date'] = \util\util::convertMySqlDate($current_date);

            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getissuedetail - ' . $e);
        }
    }

}

?>
