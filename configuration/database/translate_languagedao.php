<?php

namespace database;

use util\util;

class translate_languagedao
{
    public $module = 'DB_translate_languagedao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function gettranslateLanguageRec($id)
    {
        try {
            $this->log->logIt($this->module . " - gettranslateLanguageRec");
            $dao = new \dao();

            $table_name='';
            $select_column_name='';
            if($id==0){
                $table_name='cfmenu_items';
                $select_column_name='itemunkid as id,itemname as name,companyid,locationid';
            }elseif ($id==1){
                $table_name='cfmenu_modifiers';
                $select_column_name='modifierunkid as id,modifiername as name,companyid,locationid';
            }elseif ($id==2){
                $table_name='cfmenu_modifier_items';
                $select_column_name='modifieritemunkid as id,itemname as name,companyid,locationid';
            }elseif ($id==3){
                $table_name='cfmenu_combo';
                $select_column_name='combounkid as id,comboname as name,companyid,locationid';
            }else{
                return html_entity_decode(json_encode(array("Success" => "False")));
            }

            $strSql = "SELECT ".$select_column_name." FROM  " . CONFIG_DBN . ".".$table_name." 
            WHERE is_deleted=0 AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', CONFIG_LID);
            $res = $dao->executeQuery();



            $ObjStaticArray = new \common\staticarray();
            $arr_lang = $ObjStaticArray->language_codes;

            $lang_names=[];
            if(CONFIG_TRANSLATE_LANGUAGES!=''){
                $translate_lang=explode(',',CONFIG_TRANSLATE_LANGUAGES);
                foreach ($translate_lang as $k=>$v){
                    $lang_names[$v]=$arr_lang[$v];
                }
            }

            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res,"TRANSLATE_LANGUAGES"=>$lang_names)));

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - gettranslateLanguageRec - " . $e);
            return false;
        }
    }

    public function addeditfrm($data,$default_langlist='')
    {
        try {
            $this->log->logIt($this->module . " - addeditfrm");
            $dao = new \dao();
            $i='';
            $type_order='';
            if(isset($data) && $data!=''){
                $b_query='';
                foreach ($data as $k=>$v){
                        $ids=explode('_',$k);
                        if($i==''){
                            $type_order=$ids['1'];
                        }
                        $i='notnull';
                        $b_query.= "INSERT INTO ".CONFIG_DBN.".cflocation_itemname_translate_language (item_id,item_name,item_type,lang_type,companyid,locationid) values(".$ids[0].",'".$v."',".$ids['1'].",'".$ids['2']."',".$ids['3'].",".$ids['4'].") ON DUPLICATE KEY UPDATE item_name='".$v."';";
                }

                $dao->initCommand($b_query);
                $dao->executeNonQuery();
            }

            return html_entity_decode(json_encode(array("Success" => "True", "Data" => isset($res)?$res:'',"Message"=>$default_langlist->REC_UP_SUC)));

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - addeditfrm - " . $e);
            return false;
        }
    }

    function getTLanguagedata($type_order){
        try {
            $this->log->logIt($this->module . " - getTLanguagedata - ");
            $dao = new \dao();

            $squery = "select CONCAT(item_id,'_',item_type,'_',lang_type,'_',companyid,'_',locationid) as key_name,item_name as key_value from " . CONFIG_DBN . ".cflocation_itemname_translate_language where companyid=:companyid AND locationid=:locationid AND item_type=:item_type";
            $dao->initCommand($squery);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->addParameter(':item_type', $type_order);
            $res = $dao->executeQuery();

            return html_entity_decode(json_encode(array("Success" => "True", "Data" => isset($res)?$res:'')),ENT_NOQUOTES);
        }catch (Exception $e) {
            $this->log->logIt($this->module . " - getTLanguagedata - " . $e);
            return false;
        }
    }


}

?>