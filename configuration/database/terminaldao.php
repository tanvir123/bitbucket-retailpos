<?php

namespace database;

class terminaldao
{
    public $module = 'DB_terminaldao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addterminal($data)
    {
        try {
            $this->log->logIt($this->module . ' - addterminal');
            $dao = new \dao();


            $ObjAuditDao = new \database\auditlogdao();
            $tablename = "cfterminal";

            $datetime = \util\util::getLocalDateTime();

            $ObjCommonDao = new \database\commondao();
            $ObjDependencyDao = new \database\dependencydao();

            $arr_log = array(
                'Terminal Name' => $data['terminalname'],
                'Type' => $data['lnkdevicetype'],
                'Status' => ($data['rdo_status'] == 1) ? 'Active' : 'Inactive',
            );
            $json_data = json_encode($arr_log);

            if ($data['id'] == '0' || $data['id'] == 0) {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "terminalname", $data['terminalname']);

                if ($chk_name == 1) {
                    return json_encode(array('Success' => 'False', 'Message' => 'Select Different Terminal for Both Device'));
                }
                $title = "Add Record";
                $hashkey = \util\util::gethash();

                $strSql1 = "INSERT INTO " . CONFIG_DBN . ".cfterminal SET                             
                            terminalname=:terminalname,                           
                            is_active=:is_active,
                            createddatetime=:createddatetime,
                            created_user=:created_user,
                            companyid=:companyid,
                            locationid=:locationid,
                            hashkey=:hashkey";
                $dao->initCommand($strSql1);
                $dao->addparameter(':terminalname', $data['terminalname']);
                $dao->addparameter(':is_active', $data['rdo_status']);
                $dao->addparameter(':createddatetime', $datetime);
                $dao->addparameter(':created_user', CONFIG_UID);
                $dao->addparameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->addparameter(':hashkey', $hashkey);
                $dao->executeNonQuery();
                $terminalid = $id = $dao->getLastInsertedId();
                //$this->log->logIt($terminalid);

                $qab = array_combine($data['lnkdevicetype'], $data['ip']);
                foreach($qab as  $key=> $valll)
                {
                    $strSql1 = "INSERT INTO " . CONFIG_DBN . ".cfterminalmapping SET                             
                            ip=:ip,
                            lnkdevicetypeid=:lnkdevicetypeid,                         
                            lnkterminalid=:lnkterminalid,
                            createddatetime=:createddatetime,
                            created_user=:created_user,
                            companyid=:companyid,
                            locationid=:locationid";

                    $dao->initCommand($strSql1);
                    $dao->addparameter(':ip', $valll);
                    $dao->addparameter(':lnkdevicetypeid', $key);
                    $dao->addparameter(':lnkterminalid', $terminalid);
                    $dao->addparameter(':createddatetime', $datetime);
                    $dao->addparameter(':created_user', CONFIG_UID);
                    $dao->addparameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $dao->executeNonQuery();
                }

                 $ObjAuditDao->addactivitylog($data['module'], $title, $hashkey, $json_data);
                return json_encode(array('Success' => 'True', 'Message' => 'Terminal saved successfully'));
            } else {

                $id = $ObjCommonDao->getprimarykey('cfterminal', $data['id'], 'terminalunkid');
                $this->log->logIt($id);
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "terminalname", $data['terminalname'], $id);
                $this->log->logIt($chk_name);
                if ($chk_name == 1) {
                    return json_encode(array('Success' => 'False', 'Message' => 'Terminal already Exists'));
                }

                $title = "Edit Record";
                $strSql = "UPDATE " . CONFIG_DBN . ".cfterminal SET
                
                 terminalname=:terminalname,                
                 is_active=:is_active,
                 modifieddatetime=:modifieddatetime,
                 modified_user=:modified_user
                 WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid";

                $dao->initCommand($strSql);

                $dao->addparameter(':terminalname', $data['terminalname']);
                $dao->addparameter(':is_active', $data['rdo_status']);;
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':hashkey', $data['id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', CONFIG_LID);
                $dao->executeNonQuery();




                $qab = array_combine($data['lnkdevicetype'], $data['ip']);
                foreach($qab as  $key=> $valll)
                {
                    $strSql1 = "UPDATE " . CONFIG_DBN . ".cfterminalmapping SET                             
                            ip=:ip,
                            lnkdevicetypeid=:lnkdevicetypeid,                         
                           
                            createddatetime=:createddatetime,
                            created_user=:created_user,
                            companyid=:companyid,
                            locationid=:locationid WHERE lnkterminalid=:lnkterminalid";

                    $dao->initCommand($strSql1);
                    $dao->addparameter(':ip', $valll);
                    $dao->addparameter(':lnkdevicetypeid', $key);
                    $dao->addparameter(':lnkterminalid', $id);
                    $dao->addparameter(':createddatetime', $datetime);
                    $dao->addparameter(':created_user', CONFIG_UID);
                    $dao->addparameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', CONFIG_LID);
                    $dao->executeNonQuery();
                }

                $ObjAuditDao->addactivitylog($data['module'], $title, $data['id'], $json_data);
                return json_encode(array('Success' => 'True', 'Message' => 'Terminal updated successfully'));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addterminal - ' . $e);
            return 0;
        }
}

public
function terminallist($limit, $offset, $name)
{
    try {
        $this->log->logIt($this->module . ' - terminallist - ' . $limit . ' - ' . $offset . ' - ' . $name);
        $dao = new \dao();
        $dateformat = \database\parameter::getParameter('dateformat');
        $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];

        $strSql = "SELECT T.*,IFNULL(CFU1.username,'') AS created_user,IFNULL(CFU2.username,'') AS modified_user,                        IFNULL(DATE_FORMAT(T.createddatetime,'" . $mysqlformat . "'),'') AS created_date,
                        IFNULL(DATE_FORMAT(T.modifieddatetime,'" . $mysqlformat . "'),'') AS modified_date
                        FROM " . CONFIG_DBN . ".cfterminal AS T
                        
                        LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU1 ON T.created_user=CFU1.userunkid AND T.companyid=CFU1.companyid
                        LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU2 ON T.modified_user=CFU2.userunkid AND T.modified_user=CFU2.userunkid AND T.companyid=CFU2.companyid
                        WHERE T.companyid=:companyid AND T.locationid=:locationid AND T.is_deleted=0";
        if ($name != "") {
            $strSql .= " and T.terminalname LIKE '%" . $name . "%' ";
        }
        if ($limit != "" && ($offset != "" || $offset != 0)) {
            $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
        }
        $dao->initCommand($strSql);
        $dao->addParameter(':companyid', CONFIG_CID);
        $dao->addParameter(':locationid', CONFIG_LID);
        $data = $dao->executeQuery();
        if ($limit != "" && ($offset != "" || $offset != 0))
            $dao->initCommand($strSql . $strSqllmt);
        else
            $dao->initCommand($strSql);
        $dao->addParameter(':companyid', CONFIG_CID);
        $dao->addParameter(':locationid', CONFIG_LID);
        $rec = $dao->executeQuery();
        if (count($data) != 0) {
            $retvalue = array(array("cnt" => count($data), "data" => $rec));
            return html_entity_decode(json_encode($retvalue));
        } else {
            $retvalue = array(array("cnt" => 0, "data" => []));
            return html_entity_decode(json_encode($retvalue));
        }
    } catch (Exception $e) {
        $this->log->logIt($this->module . ' - terminallist - ' . $e);
    }
}

public function editterminallist($data)
{
    try {
        $this->log->logIt($this->module . " - editterminallist");

        $dao = new \dao();
        $id = (isset($data['id'])) ? $data['id'] : "";
        $strSql = "SELECT CT.terminalname,CT.is_active,GROUP_CONCAT(CTM.lnkdevicetypeid,'') AS A ,
                    GROUP_CONCAT(CTM.ip,'') AS B
                    FROM " . CONFIG_DBN . ".cfterminal AS CT
                    LEFT JOIN " . CONFIG_DBN . ". cfterminalmapping AS CTM
                    ON CT.terminalunkid=CTM.lnkterminalid AND CTM.is_deleted=0 
                    AND CTM.companyid=:companyid AND CTM.locationid=:locationid
                    WHERE CT.hashkey=:hashkey AND CT.companyid=:companyid 
                    AND CT.locationid=:locationid GROUP BY CT.terminalunkid";
        $dao->initCommand($strSql);

        $dao->addParameter(':companyid', CONFIG_CID);
        $dao->addparameter(':locationid', CONFIG_LID);
        $dao->addParameter(':hashkey', $id);
        $res = $dao->executeRow();


        return json_encode(array("Success" => "True", "Data" => $res));
    } catch (Exception $e) {
        $this->log->logIt($this->module . " - editterminallist - " . $e);
        return false;
    }
}


public
function countrylist()
{
    try {
        $this->log->logIt($this->module . ' - countrylist');

        $dao = new \dao();
        $strSql = "SELECT * FROM " . CONFIG_DBN . ".vwcountry ORDER BY countryCode ASC";
        $dao->initCommand($strSql);
        $cntdata = $dao->executeQuery();

        $data = $dao->executeQuery();
        if (count($data) == 0) {
            $retvalue = array(array("cnt" => 0, "data" => []));
            return html_entity_decode(json_encode($retvalue));
        } else {
            return json_encode(array(array("cnt" => count($cntdata), "data" => $data)));
        }
    } catch (Exception $e) {
        $this->log->logIt($this->module . ' - countrylist - ' . $e);
    }
}


}

?>