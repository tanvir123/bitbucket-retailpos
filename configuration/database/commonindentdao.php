<?php

namespace database;
class commonindentdao
{
    public $module = 'DB_commonindentdao';
    public $log;
    public $dbconnect;
    private $language,$default_lang_arr,$defaultlanguageArr;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language();
        $this->default_lang_arr = $this->language->loaddefaultlanguage();
        $this->defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
        $this->defaultlanguageArr = json_decode($this->defaultlanguageArr);
    }
    //forwardindent.html
    function GetIndentRequestDetailonId($id)
    {
        try {
            $this->log->logIt($this->module . " - GetIndentRequestDetailonId");

           $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];

            $strSqltogetindentrecords = "SELECT CRI.indent_doc_num,CL.storename,ST.storename as CurrentStore,
            CRI.totalamount,CRI.remarks,
            IFNULL(DATE_FORMAT(CRI.indent_date,'" . $mysqlformat . "'),'') as indentformated_date
             FROM " . CONFIG_DBN . ".cfrequestindent AS CRI 
            LEFT JOIN  " . CONFIG_DBN . ".cfstoremaster AS CL ON CRI.recstoreid=CL.storeunkid
            LEFT JOIN  " . CONFIG_DBN . ".cfstoremaster AS ST ON CRI.storeid=ST.storeunkid
            WHERE CRI.indentid=:indentid and CRI.companyid=:companyid and CRI.storeid=:storeid ";
            $dao->initCommand($strSqltogetindentrecords);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addParameter(':indentid', $id);
            $indent = $dao->executeRow();
            $resarr = array();

            $resarr['doc_num'] = $indent['indent_doc_num'];
            $resarr['RStore'] = $indent['storename'];
            $resarr['Date'] = $indent['indentformated_date'];
            $resarr['Cstore'] = $indent['CurrentStore'];
            $resarr['TotalAmount'] = $indent['totalamount'];
            $resarr['remarks'] = isset($indent['remarks'])?$indent['remarks']:'';
            $resarr['Title'] = $this->defaultlanguageArr->INDENTREQUEST;

            $strSqltogetindentdetail = "SELECT CR.name AS Item,U.name AS Unit,CRI.* ,IFNULL(ROUND((CRI.qty*CRI.rate),$round_off),'') AS Amount 
            FROM " . CONFIG_DBN . ".cfindentrequestdetail AS CRI 
            LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CRI.lnkrawid=CR.id 
            LEFT JOIN " . CONFIG_DBN . ".cfunit U ON CRI.lnkunitid=U.unitunkid AND
            U.companyid=:companyid AND U.is_active=1 and U.is_deleted=0 
            WHERE CRI.lnkindentid=:lnkindentid ";

            $dao->initCommand($strSqltogetindentdetail);
            $dao->addParameter(':lnkindentid', $id);
            $dao->addParameter(':companyid', CONFIG_CID);

            $indentdetail = $dao->executeQuery();

            $total= $to = 0;
            foreach ($indentdetail AS $val) {
                $to=$val['qty'] * $val['rate'];
                $total += $to;
            }

            $resarr['grandTotal']=number_format($total, $round_off,'.','');
            $resarr['detail'] = $indentdetail;

            return $resarr;


        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - GetIndentRequestDetailonId - ' . $e);
        }
    }
    //received_indent.html
    function GetRecievedIndentDetailId($id)
    {
        try {
            $this->log->logIt($this->module . " - GetRecievedIndentDetailId");

           $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];

            $strSqltogetindentrecords = "SELECT CRI.indent_doc_num,CL.storename,ST.storename as CurrentStore,
            CRI.totalamount,CRI.remarks,
            IFNULL(DATE_FORMAT(CRI.indent_date,'" . $mysqlformat . "'),'') as indentformated_date
             FROM " . CONFIG_DBN . ".cfrequestindent AS CRI 
            LEFT JOIN  " . CONFIG_DBN . ".cfstoremaster AS CL ON CRI.recstoreid=CL.storeunkid
            LEFT JOIN  " . CONFIG_DBN . ".cfstoremaster AS ST ON CRI.storeid=ST.storeunkid
            WHERE CRI.indentid=:indentid and CRI.companyid=:companyid and CRI.recstoreid=:storeid ";
            $dao->initCommand($strSqltogetindentrecords);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addParameter(':indentid', $id);
            $indent = $dao->executeRow();
            $resarr = array();

            $resarr['doc_num'] = $indent['indent_doc_num'];
            $resarr['RStore'] = $indent['storename'];
            $resarr['Date'] = $indent['indentformated_date'];
            $resarr['Cstore'] = $indent['CurrentStore'];
            $resarr['TotalAmount'] = $indent['totalamount'];
            $resarr['remarks'] = isset($indent['remarks'])?$indent['remarks']:'';
            $resarr['Title'] = $this->defaultlanguageArr->RECEIVEDINDENT;

            $strSqltogetindentdetail = "SELECT CR.name AS Item,U.name AS Unit,CRI.* ,IFNULL(ROUND((CRI.qty*CRI.rate),$round_off),'') AS Amount 
            FROM " . CONFIG_DBN . ".cfindentrequestdetail AS CRI 
            LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CRI.lnkrawid=CR.id 
            LEFT JOIN " . CONFIG_DBN . ".cfunit U ON CRI.lnkunitid=U.unitunkid AND
            U.companyid=:companyid AND U.is_active=1 and U.is_deleted=0 
            WHERE CRI.lnkindentid=:lnkindentid ";

            $dao->initCommand($strSqltogetindentdetail);
            $dao->addParameter(':lnkindentid', $id);
            $dao->addParameter(':companyid', CONFIG_CID);

            $indentdetail = $dao->executeQuery();

            $resarr['grandTotal']=number_format(isset($indent['totalamount'])?$indent['totalamount']:0, $round_off,'.','');
            $resarr['detail'] = $indentdetail;

            return $resarr;



        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - GetRecievedIndentDetailId - ' . $e);
        }
    }
    //issue_voucher_list.html
    public function getissuedetail($id,$defaultlanguageArr='')
    {
        try {
            $this->log->logIt($this->module . ' - getissuedvoucherdetail');
            $dao = new \dao();
            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $resarr = array();
            $strSql = "SELECT 
                        CFV.issue_doc_num AS voucher_num,CFV.lnkindentid AS indentid,
                        CFV.remarks,DATE_FORMAT(CFV.issue_date,'" . $mysql_format . "') as Voucher_Date,IFNULL(CFU1.username,'') AS createduser,
                        ROUND(IFNULL(CFV.totalamount,0),$round_off) as totalamount,
                        S.storename as issuing_store,ST.storename AS rec_store 
                        FROM " . CONFIG_DBN . ".cfissuevoucher AS CFV 
                        
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster as S ON CFV.storeid=S.storeunkid AND S.companyid=:companyid AND S.is_active=1 AND S.is_deleted=0
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS ST on CFV.relstoreid=ST.storeunkid AND ST.companyid=:companyid AND ST.is_active=1 AND ST.is_deleted=0
                        LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CFV.createduser=CFU1.userunkid AND CFV.companyid=CFU1.companyid
                        WHERE CFV.storeid=:storeid AND CFV.is_deleted=0  AND
                         CFV.companyid=:companyid AND CFV.issueid=:issueid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $dao->addparameter(':issueid', $id);
            $issuerecords = $dao->executeRow();

            $resarr['Title'] = $this->defaultlanguageArr->ISSUEVOUCHER;
            $resarr['doc_num'] = $issuerecords['voucher_num'];
            $resarr['remarks'] = isset($issuerecords['remarks'])?$issuerecords['remarks']:'';
            $resarr['Date'] = $issuerecords['Voucher_Date'];
            $resarr['TotalAmount'] = $issuerecords['totalamount'];
            $resarr['Cstore'] = $issuerecords['issuing_store'];
            $resarr['RStore'] = $issuerecords['rec_store'];

            $str = "SELECT  CRM.name AS Item,IVD.qty,U.name as Unit,IVD.rate,
                    IVD.total AS Amount,IVD.tax_description,IVD.discount_amount,IVD.tax_amount 
                    FROM " . CONFIG_DBN . ".cfissuevoucherdetail AS IVD 
                    LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CRM ON IVD.lnkrawid=CRM.id AND CRM.companyid=:companyid AND CRM.is_active=1 AND CRM.is_deleted=0
                    LEFT JOIN " . CONFIG_DBN . ".cfunit AS U ON IVD.lnkunitid=U.unitunkid AND U.companyid=:companyid AND U.is_active=1 AND U.is_deleted=0
                    WHERE IVD.companyid=:companyid AND IVD.lnkissueid=:issueid ";
            $dao->initCommand($str);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':issueid', $id);

            $issuedetail = $dao->executeQuery();

            $taxTOtal = 0;
            if(isset($issuedetail)){
                foreach ($issuedetail as $key => $value) {
                    $taxname = array();
                    $potaxdetail = json_decode($value['tax_description'], 1);
                    $taxeslist_dis ='';
                    if(isset($potaxdetail)) {
                        foreach ($potaxdetail as $key2 => $value2) {
                            $Tax_NAME ='';
                            $posting_type=isset($porecords['currency_sign'])?$porecords['currency_sign']:'Flat';
                            if(empty($value2['tax_name']))
                            {
                                $str = "SELECT tax FROM " . CONFIG_DBN . ".cftax
                            WHERE  companyid=:companyid AND storeid=:storeid  AND taxunkid=:taxunkid";
                                $dao->initCommand($str);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addParameter(':storeid', CONFIG_SID);
                                $dao->addParameter(':taxunkid', $value2['tax_id']);
                                $qtaxname = $dao->executeRow();
                                $Tax_NAME=$qtaxname['tax'];
                            }
                            else{
                                $Tax_NAME=$value2['tax_name'];
                            }
                            if(isset($value2['posting_rule']) && $value2['posting_rule'] == 1)
                            {
                                $posting_type='%';
                            }
                            $taxeslist_dis .= "<b>".$Tax_NAME."</b> : (".number_format($value2['tax_value'],$round_off,'.','')." ".$posting_type.") : ".number_format($value2['tax_amt'], $round_off,'.','')." <br>";
                            $taxname[$value2['tax_id']] = $value2['tax_amt'];
                        }
                    }
                    $taxTOtal = ($taxTOtal+$value['tax_amount']);
                    $issuedetail[$key]['tax_description'] = isset($taxeslist_dis)?$taxeslist_dis:'';
                }
            }

            $resarr['grandTotal']=number_format($issuerecords['totalamount'], $round_off,'.','');
            $resarr['detail'] = $issuedetail;
            $resarr['Tax_Total'] =  number_format($taxTOtal,$round_off,'.','');


            $Discounttotal = $Dto = 0;
            foreach ($issuedetail AS $val) {

                $to = $val['discount_amount'];
                $Discounttotal += $to;
            }

            $resarr['Discounttotal'] = number_format($Discounttotal,$round_off,'.','');
            $numberinwords = \util\util::convertNumber($issuerecords['totalamount'],'','','',$defaultlanguageArr);

            $resarr['numberinwords'] = $numberinwords;


            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getissuedvoucherdetail - ' . $e);
        }
    }
    //received_voucher.html
    function GetRecievedVoucherDetail($id)
    {
        try {
            $this->log->logIt($this->module . " - GetRecievedVoucherDetail");

            $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT C.companyname,C.address1 AS company_address,C.phone,C.currency_sign,C.companyemail,
                        CFV.issue_doc_num AS voucher_num,CFV.lnkindentid AS indentid,
                        CFV.remarks,DATE_FORMAT(CFV.issue_date,'" . $mysqlformat . "') as Voucher_Date,IFNULL(CFU1.username,'') AS createduser,
                        ROUND(IFNULL(CFV.totalamount,0),$round_off) as totalamount,
                        S.storename as storename,ST.storename AS CurrentStore 
                        FROM " . CONFIG_DBN . ".cfissuevoucher AS CFV 
                        LEFT JOIN " . CONFIG_DBN . ".cfcompany as C ON CFV.companyid= C.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster as S ON CFV.storeid=S.storeunkid AND S.companyid=:companyid AND S.is_active=1 AND S.is_deleted=0
                        LEFT JOIN " . CONFIG_DBN . ".cfstoremaster AS ST on CFV.relstoreid=ST.storeunkid AND ST.companyid=:companyid AND ST.is_active=1 AND ST.is_deleted=0
                        LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CFV.createduser=CFU1.userunkid AND CFV.companyid=CFU1.companyid
                        WHERE CFV.relstoreid=:storeid AND CFV.is_deleted=0  AND
                         CFV.companyid=:companyid AND CFV.issueid=:issueid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':storeid', CONFIG_SID);
            $dao->addparameter(':issueid', $id);
            $issue = $dao->executeRow();
            $resarr = array();

            $resarr['doc_num'] = $issue['voucher_num'];
            $resarr['RStore'] = $issue['CurrentStore'];
            $resarr['Date'] = $issue['Voucher_Date'];
            $resarr['Cstore'] = $issue['storename'];
            $resarr['TotalAmount'] = $issue['totalamount'];
            $resarr['remarks'] = isset($issue['remarks'])?$issue['remarks']:'';
            $resarr['Title'] = $this->defaultlanguageArr->RECEIVEDVOUCHER;

            $str = "SELECT  CRM.name AS Item,IVD.qty,U.shortcode as Unit,IVD.rate,
                    IFNULL(ROUND((IVD.qty*IVD.rate),$round_off),'') AS Amount 
                    FROM " . CONFIG_DBN . ".cfissuevoucherdetail AS IVD 
                    LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CRM ON IVD.lnkrawid=CRM.id AND CRM.companyid=:companyid AND CRM.is_active=1 AND CRM.is_deleted=0
                    LEFT JOIN " . CONFIG_DBN . ".cfunit AS U ON IVD.lnkunitid=U.unitunkid AND U.companyid=:companyid AND U.is_active=1 AND U.is_deleted=0
                    WHERE IVD.companyid=:companyid AND IVD.lnkissueid=:issueid AND IVD.lnkindentid=:lnkindentid";
            $dao->initCommand($str);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':issueid', $id);
            $dao->addparameter(':lnkindentid', $issue['indentid']);
            $issuedetail = $dao->executeQuery();

            $total= $to = 0;
            foreach ($issuedetail AS $val) {
                $to=$val['qty'] * $val['rate'];
                $total += $to;
            }

            $resarr['grandTotal']=number_format($total, $round_off,'.','');
            $resarr['detail'] = $issuedetail;

            return $resarr;



        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - GetRecievedVoucherDetail - ' . $e);
        }
    }
    public function GetPurchaseOrderDetail($id,$defaultlanguageArr='')
    {
        try {
            $this->log->logIt($this->module . ' - GetPurchaseOrderDetail');
            $dao = new \dao();

            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $resarr = array();
            $strSql = "SELECT C.companyname,C.address1 AS company_address,C.phone AS company_phone,
                        C.currency_sign,C.companyemail,C.fax,
                        TC.name AS vendor_name,TC.address AS vendor_address,TC.mobile AS vendor_mobile,TC.fax AS Vendor_fax,TC.email AS Vendor_email,TC.business_name AS vendor_business,
                        PO.order_doc_num AS voucher_num, PO.remarks AS po_remarks,
                         IFNULL(DATE_FORMAT(PO.order_date,'" . $mysql_format . "'),'')  AS Voucher_Date, 
                        IFNULL(CFU1.username,'') AS createduser,ROUND(IFNULL(PO.totalamount,0),$round_off) as totalamount,
                        VC.countryName AS Company_Country                        
                        FROM " . CONFIG_DBN . ".cfpurchaseorder AS PO                         
                        LEFT JOIN " . CONFIG_DBN . ".cfcompany AS C ON PO.companyid= C.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU1 ON PO.createduser=CFU1.userunkid AND PO.companyid=CFU1.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfuser AS CFU2 ON PO.authorized_user=CFU2.userunkid AND PO.companyid=CFU2.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfvandor AS V ON PO.lnkvendorid=V.vandorunkid AND V.companyid=PO.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".trcontact AS TC ON V.lnkcontactid=TC.contactunkid AND TC.companyid=V.companyid
                        LEFT JOIN " . CONFIG_DBN . ".vwcountry AS VC ON C.country=VC.id                       
                        WHERE PO.companyid=:companyid AND PO.orderid=:orderid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':orderid', $id);
            $porecords = $dao->executeRow();

            $resarr['doc_num'] = $porecords['voucher_num'];
            $resarr['Vendor_Business'] = $porecords['vendor_business'];
            $resarr['Date'] = $porecords['Voucher_Date'];
            $resarr['Vendor_Name'] = $porecords['vendor_name'];
            $resarr['grandTotal'] = $porecords['totalamount'];
            $resarr['remarks'] = isset($porecords['po_remarks'])?$porecords['po_remarks']:'';
            $resarr['Title'] = $this->defaultlanguageArr->PURCHASEORDER;

            $resarr['current_date'] = \util\util::convertMySqlDate($current_date);

            $strPODetail = "SELECT CR.name AS Item,CU.shortcode AS Unit,CPD.lnkorderid,CPD.orderdetailunkid,CPD.lnkrawid,CPD.lnkrawcatid," .
                "CPD.lnkunitid,ROUND(CPD.qty,$round_off) AS qty,ROUND(CPD.rate,$round_off) AS rate," .
                "CPD.discount_per,ROUND(CPD.discount_amount,$round_off) AS discount_amount,ROUND(CPD.tax_amount,$round_off) AS tax_amount," .
                "CPD.tax_description,ROUND(CPD.total,$round_off) AS Amount FROM " . CONFIG_DBN . ".cfpurchaseorderdetail AS CPD " .
                "LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CPD.lnkrawid=CR.id " .
                "LEFT JOIN " . CONFIG_DBN . ".cfunit CU ON CPD.lnkunitid=CU.unitunkid AND " .
                "CU.companyid=:companyid AND CU.is_active=1 AND CU.is_deleted=0 " .
                "WHERE CPD.lnkorderid=:lnkorderid AND CPD.companyid=:companyid AND CPD.storeid=:storeid";
            $dao->initCommand($strPODetail);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addparameter(':lnkorderid', $id);
            $podetail = $dao->executeQuery();

            $taxTOtal = 0;
            if(isset($podetail)){
                foreach ($podetail as $key => $value) {
                    $taxname = array();
                    $potaxdetail = json_decode($value['tax_description'], 1);
                    $taxeslist_dis ='';
                    if(isset($potaxdetail)) {
                        foreach ($potaxdetail as $key2 => $value2) {
                            $Tax_NAME ='';
                            $posting_type=isset($porecords['currency_sign'])?$porecords['currency_sign']:'Flat';
                            if(empty($value2['tax_name']))
                            {
                                $str = "SELECT tax FROM " . CONFIG_DBN . ".cftax
                            WHERE  companyid=:companyid AND storeid=:storeid  AND taxunkid=:taxunkid";
                                $dao->initCommand($str);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addParameter(':storeid', CONFIG_SID);
                                $dao->addParameter(':taxunkid', $value2['tax_id']);
                                $qtaxname = $dao->executeRow();
                                $Tax_NAME=$qtaxname['tax'];
                            }
                            else{
                                $Tax_NAME=$value2['tax_name'];
                            }
                            if(isset($value2['posting_rule']) && $value2['posting_rule'] == 1)
                            {
                                $posting_type='%';
                            }
                            $taxeslist_dis .= "<b>".$Tax_NAME."</b> : (".number_format($value2['tax_value'],$round_off,'.','')." ".$posting_type.") : ".number_format($value2['tax_amt'], $round_off,'.','')." <br>";
                            $taxname[$value2['tax_id']] = $value2['tax_amt'];
                        }
                    }
                    $taxTOtal = ($taxTOtal+$value['tax_amount']);
                    $podetail[$key]['tax_description'] = isset($taxeslist_dis)?$taxeslist_dis:'';
                }
            }

            $resarr['detail'] = $podetail;
            $resarr['Tax_Total'] =  number_format($taxTOtal,$round_off,'.','');


            $Discounttotal = $Dto = 0;
            foreach ($podetail AS $val) {

                $to = $val['discount_amount'];
                $Discounttotal += $to;
            }

            $resarr['Discounttotal'] = number_format($Discounttotal,$round_off,'.','');
            $numberinwords = \util\util::convertNumber($porecords['totalamount'],'','','',$defaultlanguageArr);

            $resarr['numberinwords'] = $numberinwords;

            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - GetPurchaseOrderDetail - ' . $e);
        }
    }
    public function GetGoodsReturnNoteDetail($id,$defaultlanguageArr='')
    {
        try {
            $this->log->logIt($this->module . ' - GetGoodsReturnNoteDetail');
            $dao = new \dao();

            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $resarr = array();

            $strSql = "SELECT C.companyname,C.address1 AS company_address,C.phone AS company_phone,
                        C.currency_sign,C.companyemail,C.fax,
                        TC.name AS vendor_name,TC.address AS vendor_address,TC.mobile AS vendor_mobile,
                        TC.business_name AS Vendor_business,
                        TC.fax AS Vendor_fax,TC.email AS Vendor_email,
                        GR.grn_doc_num AS grn_voucher_num, GR.remarks,GR.voucher_no AS voucher_num,
                         IFNULL(DATE_FORMAT(GR.grn_date,'" . $mysql_format . "'),'')  AS Voucher_Date, 
                        ROUND(IFNULL(GR.totalamount,0),$round_off) as totalamount,
                        VC.countryName AS Company_Country                        
                        FROM " . CONFIG_DBN . ".cfgrn AS GR                         
                        LEFT JOIN " . CONFIG_DBN . ".cfcompany AS C ON GR.companyid= C.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfvandor AS V ON GR.lnkvendorid=V.vandorunkid AND V.companyid=GR.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".trcontact AS TC ON V.lnkcontactid=TC.contactunkid AND TC.companyid=V.companyid
                        LEFT JOIN " . CONFIG_DBN . ".vwcountry AS VC ON C.country=VC.id                       
                        WHERE GR.companyid=:companyid AND GR.grnid=:grnid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':grnid', $id);
            $grrecords = $dao->executeRow();

            $resarr['doc_num'] = $grrecords['grn_voucher_num'];
            $resarr['VoucherNum'] = $grrecords['voucher_num'];
            $resarr['Vendor_Business'] = $grrecords['Vendor_business'];
            $resarr['Date'] = $grrecords['Voucher_Date'];
            $resarr['Vendor_Name'] = $grrecords['vendor_name'];
            $resarr['grandTotal'] = $grrecords['totalamount'];
            $resarr['remarks'] = isset($grrecords['remarks'])?$grrecords['remarks']:'';
            $resarr['Title'] = $this->defaultlanguageArr->GOODRECEIPTNOTE;



            $strPODetail = "SELECT CR.name AS Item,CU.shortcode AS Unit,CPD.lnkgrnid,CPD.lnkrawid,CPD.lnkrawcatid," .
                "CPD.lnkunitid,ROUND(CPD.qty,$round_off) AS qty,ROUND(CPD.rate,$round_off) AS rate," .
                "CPD.discount_per,ROUND(CPD.discount_amount,$round_off) AS discount_amount,
                ROUND(CPD.tax_amount,$round_off) AS tax_amount," .
                "CPD.tax_description,ROUND(CPD.total,$round_off) AS Amount FROM " . CONFIG_DBN . ".cfgrndetail AS CPD " .
                "LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CPD.lnkrawid=CR.id " .
                "LEFT JOIN " . CONFIG_DBN . ".cfunit CU ON CPD.lnkunitid=CU.unitunkid AND " .
                "CU.companyid=:companyid AND CU.is_active=1 AND CU.is_deleted=0 " .
                "WHERE CPD.lnkgrnid=:lnkgrnid AND CPD.companyid=:companyid AND CPD.storeid=:storeid";
            $dao->initCommand($strPODetail);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addparameter(':lnkgrnid', $id);
            $grdetail = $dao->executeQuery();

            $taxTOtal = 0;
            if($grdetail){

                foreach ($grdetail as $key => $value) {
                    $taxname = array();
                    $grtaxdetail = json_decode($value['tax_description'], 1);
                    $taxeslist_dis='';
                    if($grtaxdetail){
                        foreach ($grtaxdetail as $key2 => $value2) {
                            $Tax_NAME='';
                            $posting_type=isset($grrecords['currency_sign'])?$grrecords['currency_sign']:'Flat';
                            if(isset($value2['posting_rule']) && $value2['posting_rule']==1){
                                $posting_type='%';
                            }

                            if(empty($value2['tax_name']))
                            {
                                $str = "SELECT tax FROM " . CONFIG_DBN . ".cftax
                 WHERE  companyid=:companyid AND storeid=:storeid  AND taxunkid=:taxunkid";
                                $dao->initCommand($str);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addParameter(':storeid', CONFIG_SID);
                                $dao->addParameter(':taxunkid', $value2['tax_id']);
                                $qtaxname = $dao->executeRow();
                                $Tax_NAME=$qtaxname['tax'];
                            }
                            else{
                                $Tax_NAME=$value2['tax_name'];
                            }

                            $taxeslist_dis .= "<b>".$Tax_NAME."</b> : (".number_format($value2['tax_value'],$round_off,'.','')." ".$posting_type.") : ".number_format($value2['tax_amt'], $round_off,'.','')." <br>";
                            $taxname[$value2['tax_id']] = $value2['tax_amt'];
                        }
                    }
                    $taxTOtal = ($taxTOtal+$value['tax_amount']);
                    $grdetail[$key]['tax_description'] = isset($taxeslist_dis)?$taxeslist_dis:'';
                }
            }
            $resarr['detail'] = $grdetail;
            $resarr['Tax_Total'] = number_format($taxTOtal,$round_off,'.','');
            $Discounttotal = $Dto = 0;
            foreach ($grdetail AS $val) {

                $to = $val['discount_amount'];
                $Discounttotal += $to;
            }

            $resarr['Discounttotal'] = number_format($Discounttotal, $round_off,'.','');
            $numberinwords = \util\util::convertNumber($grrecords['totalamount'],'','','',$defaultlanguageArr);
            $resarr['numberinwords'] = $numberinwords;

            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - GetGoodsReturnNoteDetail - ' . $e);
        }
    }
    public function GetGoodsReturnDetail($id,$defaultlanguageArr='')
    {
        try {
            $this->log->logIt($this->module . ' - GetGoodsReturnDetail');
            $dao = new \dao();


            $current_date = \util\util::getLocalDate();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $date_format = \database\parameter::getParameter('dateformat');
            $mysql_format = \common\staticarray::$mysqldateformat[$date_format];
            $resarr = array();

            $strSql = "SELECT C.companyname,C.address1 AS company_address,C.phone AS company_phone,
                        C.currency_sign,C.companyemail,C.fax,
                        TC.name AS vendor_name,TC.address AS vendor_address,TC.mobile AS vendor_mobile,TC.business_name AS Vendor_business,
                        TC.fax AS Vendor_fax,TC.email AS Vendor_email,
                        GR.gr_doc_num AS gr_voucher_num, GR.remarks,GR.voucher_no AS voucher_num,
                         IFNULL(DATE_FORMAT(GR.gr_date,'" . $mysql_format . "'),'')  AS Voucher_Date, 
                        ROUND(IFNULL(GR.totalamount,0),$round_off) as totalamount,
                        VC.countryName AS Company_Country                        
                        FROM " . CONFIG_DBN . ".cfgoodsreturn AS GR                         
                        LEFT JOIN " . CONFIG_DBN . ".cfcompany AS C ON GR.companyid= C.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".cfvandor AS V ON GR.lnkvendorid=V.vandorunkid AND V.companyid=GR.companyid 
                        LEFT JOIN " . CONFIG_DBN . ".trcontact AS TC ON V.lnkcontactid=TC.contactunkid AND TC.companyid=V.companyid
                        LEFT JOIN " . CONFIG_DBN . ".vwcountry AS VC ON C.country=VC.id                       
                        WHERE GR.companyid=:companyid AND GR.grid=:grid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':grid', $id);
            $grrecords = $dao->executeRow();

            $resarr['doc_num'] = $grrecords['gr_voucher_num'];
            $resarr['VoucherNum'] = $grrecords['voucher_num'];
            $resarr['Vendor_Business'] = $grrecords['Vendor_business'];
            $resarr['Date'] = $grrecords['Voucher_Date'];
            $resarr['Vendor_Name'] = $grrecords['vendor_name'];
            $resarr['grandTotal'] = $grrecords['totalamount'];
            $resarr['remarks'] = isset($grrecords['remarks'])?$grrecords['remarks']:'';
            $resarr['Title'] = $this->defaultlanguageArr->GOODRECEIPTNOTE;




            $strPODetail = "SELECT CR.name AS Item,CU.shortcode AS Unit,CPD.lnkgrid,CPD.lnkrawid,CPD.lnkrawcatid," .
                "CPD.lnkunitid,ROUND(CPD.qty,$round_off) AS qty,ROUND(CPD.rate,$round_off) AS rate," .
                "CPD.discount,ROUND(CPD.discount_amount,$round_off) AS discount_amount,ROUND(CPD.tax_amount,$round_off) AS tax_amount," .
                "CPD.tax_description,ROUND(CPD.total,$round_off) AS Amount FROM " . CONFIG_DBN . ".cfgoodsreturndetail AS CPD " .
                "LEFT JOIN " . CONFIG_DBN . ".cfrawmaterial AS CR ON CPD.lnkrawid=CR.id " .
                "LEFT JOIN " . CONFIG_DBN . ".cfunit CU ON CPD.lnkunitid=CU.unitunkid AND " .
                "CU.companyid=:companyid AND CU.is_active=1 AND CU.is_deleted=0 " .
                "WHERE CPD.lnkgrid=:lnkgrid AND CPD.companyid=:companyid AND CPD.storeid=:storeid";
            $dao->initCommand($strPODetail);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':storeid', CONFIG_SID);
            $dao->addparameter(':lnkgrid', $id);
            $grdetail = $dao->executeQuery();


            $taxTOtal = 0;
            if(isset($grdetail)){
                foreach ($grdetail as $key => $value) {
                    $taxname = array();
                    $grtaxdetail = json_decode($value['tax_description'], 1);
                    $taxeslist_dis='';
                    if($grtaxdetail){
                        foreach ($grtaxdetail as $key2 => $value2) {
                            $Tax_NAME='';
                            $posting_type=isset($grrecords['currency_sign'])?$grrecords['currency_sign']:'Flat';
                            if(isset($value2['posting_rule']) && $value2['posting_rule']==1){
                                $posting_type='%';
                            }
                            if(empty($value2['tax_name']))
                            {
                                $str = "SELECT tax FROM " . CONFIG_DBN . ".cftax
                 WHERE  companyid=:companyid AND storeid=:storeid  AND taxunkid=:taxunkid";
                                $dao->initCommand($str);
                                $dao->addParameter(':companyid', CONFIG_CID);
                                $dao->addParameter(':storeid', CONFIG_SID);
                                $dao->addParameter(':taxunkid', $value2['tax_id']);
                                $qtaxname = $dao->executeRow();
                                $Tax_NAME=$qtaxname['tax'];
                            }
                            else{
                                $Tax_NAME=$value2['tax_name'];
                            }
                            $taxeslist_dis .= "<b>".$Tax_NAME."</b> : (".number_format($value2['tax_value'],$round_off,'.','')." ".$posting_type.") : ".number_format($value2['tax_amt'], $round_off,'.','')." <br>";
                            $taxname[$value2['tax_id']] = $value2['tax_amt'];

                        }
                    }
                    $taxTOtal = ($taxTOtal+$value['tax_amount']);
                    $grdetail[$key]['tax_description'] = isset($taxeslist_dis)?$taxeslist_dis:'';
                }
            }

            $resarr['detail'] = $grdetail;
            $resarr['Tax_Total'] =  number_format($taxTOtal,$round_off,'.','');

            $Discounttotal = $Dto = 0;
            foreach ($grdetail AS $val) {

                $to = $val['discount_amount'];
                $Discounttotal += $to;
            }

            $resarr['Discounttotal'] = number_format($Discounttotal, $round_off,'.','');
            $numberinwords = \util\util::convertNumber($grrecords['totalamount'],'','','',$defaultlanguageArr);

            $resarr['numberinwords'] = $numberinwords;

            return $resarr;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - GetGoodsReturnDetail - ' . $e);
        }
    }


}
?>