<?php
/**
 * Created by PhpStorm.
 * User: romal
 * Date: 28/2/17
 * Time: 10:47 AM
 */

namespace database;


use util\util;

class authdao
{
    private $module = 'DB_authdao';
    private $log,$util,$dao;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->util = new \util\util();
        $this->dao = new \dao();
    }

    public function generateToken()
    {
        try
        {
            $this->log->logIt($this->module.' - doLogin');

            $input = util::input();
            $input_arr = json_decode($input);

            $username = $input_arr->request->username;
            $password = $input_arr->request->password;

            $strSql = "SELECT * FROM auth_token where username = '".$username."'";
            $this->dao->initCommand($strSql);
            $data = $this->dao->executeQuery();

            if(count($data) > 0)
            {
                $this->log->logIt($this->module.' - doLogin >> '.$data[0]['password']);
                if(isset($data[0]['password']) && $data[0]['password'] == $password)
                {   
                    //$access_token = md5(base64_encode($data[0]['id']."_".$username."_".time()));
                    $access_token =  bin2hex(openssl_random_pseudo_bytes(128));
                    $this->log->logIt($this->module.' - doLogin - access tk >> '.$access_token);
                    $sql = "update auth_token set access_token = '".$access_token."' where id = ".$data[0]['id'];
                    $this->dao->initCommand($sql);
                    $this->dao->executeNonQuery();
                    
                    $status = 200;
                    $message = "Success";
                    $response = array("auth_token"=>$access_token);
                        
                    echo util::response($status,$message,$response);
                }
                else{
                    echo 'romal';
                    $status = 500;
                    $message = "Error";
                    $response = "Invalid username/password..";
                    echo util::response($status,$message,$response);
                }
            }
            else{
                $status = 500;
                $message = "Error";
                $response = "Invalid username/password..";
                echo util::response($status,$message,$response);
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - doLogin - '.$e);
        }
    }
    public function checkLogin()
    {
        try
        {
            $this->log->logIt($this->module."-"."checkLogin");

            $input = util::input();
            $input_arr = json_decode($input);

            if(isset($input_arr->headers))
            {   
                if(isset($input_arr->headers->{"X-User-Agent"}) && $input_arr->headers->{"X-User-Agent"} == "parghiinfotech")
                {
                    if(isset($input_arr->headers->Authorization))
                    {
                        $strSql = "SELECT * FROM auth_token where access_token = '".$input_arr->headers->Authorization."'";
                        $this->dao->initCommand($strSql);
                        $data = $this->dao->executeQuery();
                        
                        if(count($data) > 0) {
                            $this->log->logIt($this->module . ' - checkLogin >> Record found');
                            return true;
                        }
                        else{
                            return false;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - checkLogin - '.$e);
        }

    }

}