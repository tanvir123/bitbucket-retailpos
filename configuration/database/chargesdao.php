<?php
namespace database;

use Aws\CloudTrail\LogFileIterator;

class chargesdao
{
    public $module = 'DB_chargesdao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function addCharges($data,$languageArr,$defaultlanguageArr)
    {

        try{
            $this->log->logIt($this->module.' - addCharges');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $datetime=\util\util::getLocalDateTime();
            $objutil = new \util\util;
            $getdate = $objutil->convertDateToMySql($data['appliesfrom']);
            $tablename =  "cfcharges";
            $ObjDependencyDao = new \database\dependencydao();

            $arr_log = array(
                'Short Name'=>$data['shortname'],
                'Charge Name'=>$data['chargename'],
                'Applies From'=>$getdate,
                'Posting Type'=>($data['chargerule']==1)?'Percentage':'Flat',
                'Amount'=>$data['amount'],
                'Status'=>($data['rdo_status']==1)?'Active':'Inactive',
            );
            $json_data =json_encode($arr_log);

            if($data['id']=='0' || $data['id']==0)
            {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"charge",$data['chargename']);

                if($chk_name==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG3)));
                }

                $title = "Add Record";
                $hashkey = \util\util::gethash();
                $strSql = "INSERT INTO ".CONFIG_DBN.".cfcharges(companyid,locationid,shortcode,charge,amount,type,appliesfrom,fasmastertype,is_default,createddatetime,created_user,isactive,hashkey,is_tier,tiers_description,applicable_on,charge_always_applicable,charge_applicable_below_order_amount)
                            VALUE(:companyid,:locationid, :shortcode, :charge,:amount,:type,:appliesfrom, :fasmastertype, :is_default, :createddatetime, :created_user, :isactive, :hashkey, :is_tier, :tiers_description, :applicable_on, :charge_always_applicable, :charge_applicable_below_order_amount)";
                $dao->initCommand($strSql);
                $dao->addParameter(':shortcode',$data['shortname']);
                $dao->addParameter(':charge',$data['chargename']);
                $dao->addParameter(':amount',$data['amount']);
                $dao->addParameter(':type',$data['chargerule']);
                $dao->addParameter(':appliesfrom',$getdate);
                $dao->addParameter(':fasmastertype','charge');
                $dao->addParameter(':isactive',$data['rdo_status']);
                $dao->addParameter(':charge_always_applicable',$data['charge_always_applicable']);
                $dao->addParameter(':charge_applicable_below_order_amount',$data['charge_applicable_below_order_amount']);
                $dao->addParameter(':is_default',0);
                $dao->addParameter(':is_tier',isset($data['is_tier'])?$data['is_tier']:0);
                $dao->addParameter(':tiers_description',isset($data['tiers_description'])?$data['tiers_description']:NULL);
                $dao->addParameter(':applicable_on',$data['applicable_on']);
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':created_user',CONFIG_UID);
                $dao->addParameter(':hashkey',$hashkey);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_ADD_SUC)));
            }
            else
            {
                $id = $ObjCommonDao->getprimarykey('cfcharges',$data['id'],'chargesunkid');
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename,"charge",$data['chargename'],$id);
                if($chk_name==1)
                {
                    return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$languageArr->LANG23)));
                }
                $title = "Edit Record";
                $strSql = "UPDATE ".CONFIG_DBN.".cfcharges SET shortcode=:shortcode, amount=:amount, type=:type, appliesfrom=:appliesfrom,is_default=:is_default,modifieddatetime=:modifieddatetime, modified_user=:modified_user, is_tier=:is_tier, tiers_description=:tiers_description, applicable_on=:applicable_on , charge_always_applicable=:charge_always_applicable , charge_applicable_below_order_amount=:charge_applicable_below_order_amount ";
                if($id!=1 && $id!=2)  // && $id!=3   remove service charge
                 {
                    $strSql.= " ,charge=:charge ";
                }
                $strSql.= " WHERE hashkey=:hashkey AND companyid=:companyid AND locationid=:locationid ";

                $dao->initCommand($strSql);
                $dao->addParameter(':hashkey',$data['id']);
                $dao->addParameter(':shortcode',$data['shortname']);
                if($id!=1 && $id!=2) // && $id!=3
                {
                    $dao->addParameter(':charge',$data['chargename']);
                }
                $dao->addParameter(':amount',$data['amount']);
                $dao->addParameter(':type',$data['chargerule']);
                $dao->addParameter(':appliesfrom',$getdate);
                $dao->addParameter(':charge_always_applicable',$data['charge_always_applicable']);
                $dao->addParameter(':charge_applicable_below_order_amount',$data['charge_applicable_below_order_amount']);
                $dao->addParameter(':is_default',0);
                $dao->addParameter(':is_tier',isset($data['is_tier'])?$data['is_tier']:0);
                $dao->addParameter(':tiers_description',isset($data['tiers_description'])?$data['tiers_description']:NULL);
                $dao->addParameter(':applicable_on',$data['applicable_on']);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->addparameter(':locationid',CONFIG_LID);
                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$defaultlanguageArr->REC_UP_SUC)));
            }
        }catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addCharges - '.$e);
        }

    }

    public function chargeslist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - chargeslist - '.$name);
            $dao = new \dao;
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = "SELECT CFT.*,IFNULL(CFU1.username,'') AS createduser,IFNULL(CFU2.username,'') AS modifieduser,
            IFNULL(DATE_FORMAT(CFT.createddatetime,'".$mysqlformat."'),'') as created_date,
            IFNULL(DATE_FORMAT(CFT.modifieddatetime,'".$mysqlformat."'),'') as modified_date FROM ".CONFIG_DBN.".cfcharges AS CFT 
            LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON CFT.created_user=CFU1.userunkid AND CFT.companyid=CFU1.companyid 
            LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON CFT.modified_user=CFU2.userunkid AND CFT.modified_user=CFU2.userunkid AND CFT.companyid=CFU2.companyid
            WHERE CFT.companyid=:companyid  AND CFT.is_deleted=0 ";
            if($name!="")
                $strSql .= " AND charge LIKE '%".$name."%'";

			$strSql .= " AND CFT.locationid = :locationid ";

            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
           
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
			$dao->addparameter(':locationid',CONFIG_LID);
			
            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);          
            $dao->addParameter(':companyid',CONFIG_CID);
			$dao->addparameter(':locationid',CONFIG_LID);
            $rec = $dao->executeQuery();
            
            if(count($data) != 0){               
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return json_encode($retvalue);
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return json_encode($retvalue);
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - chargeslist - '.$e);
        }
    }

    public function getChargeRec($data)
    {
		try
        {
			$this->log->logIt($this->module." - getChargeRec");
			$dao = new \dao();
            $dateformat = \database\parameter::getParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = " SELECT CFC.chargesunkid,CFC.charge,CFC.shortcode,CFC.isactive,CFC.is_default,CFC.amount,CFC.type,CFC.appliesfrom,CFC.is_tier,CFC.tiers_description,CFC.applicable_on,CFC.charge_type,CFC.charge_always_applicable,CFC.charge_applicable_below_order_amount
                            FROM ".CONFIG_DBN.".cfcharges AS CFC
                            WHERE CFC.hashkey=:hashkey AND CFC.companyid=:companyid AND CFC.locationid = :locationid ";
			$dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
			$dao->addparameter(':locationid',CONFIG_LID);
            $dao->addParameter(':hashkey',$data['id']);
            $res = $dao->executeRow();

            return json_encode(array("Success"=>"True","Data"=>$res));
        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getChargeRec - ".$e);
			return false; 
		}
	}

    public function togglezomato($data,$defaultlanguageArr)
    {
        try {
            $this->log->logIt($this->module . ' - togglezomato');
            $dao = new \dao();

            $ObjAuditDao = new \database\auditlogdao();
            if (count($data) > 0 && isset($data['module']) && isset($data['id'])) {
                $datetime = \util\util::getLocalDateTime();
                $table = 'cfcharges';
                $pd = 'chargesunkid';
                $cn = 'apply_zomato';

                $ObjCommonDao = new \database\commondao();
                $id = $ObjCommonDao->getprimarykey($table, $data['id'],$pd);

                $arr_log = array(
                    'Old Value' => ($data['value'] == 1) ? 'Active' : 'Inactive',
                    'New Value' => ($data['value'] == 1) ? 'InActive' : 'Active',
                );
                $json_data = json_encode($arr_log);

                        $loginid=CONFIG_LID;
                        $strSql = "UPDATE " . CONFIG_DBN . "." . $table . " SET modified_user=:modified_user, modifieddatetime=:modifieddatetime, " . $cn . " = IF(" . $cn . "=1,0,1) WHERE chargesunkid=:id AND companyid=:companyid AND locationid=:locationid";

                $dao->initCommand($strSql);
                $dao->addParameter(":id", $id);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->addparameter(':locationid', $loginid);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'], 'Change Apply Charges to Zomato', $data['id'], $json_data);
                return html_entity_decode(json_encode(array("Success" => "True", "Message" => $defaultlanguageArr->STATUS_CHANGE_SUCCESS)));
            } else
                return html_entity_decode(json_encode(array("Success" => "False", "Message" => $defaultlanguageArr->INTERNAL_ERROR)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - togglezomato - ' . $e);
        }
    }
}

?>