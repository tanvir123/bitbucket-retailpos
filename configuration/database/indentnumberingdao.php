<?php
namespace database;

class indentnumberingdao
{
    public $module = 'DB_indentnumberingdao';
    public $log;
    public $dbconnect;

    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function doclist()
    {
        try
        {
            $this->log->logIt($this->module.' - doclist - ');
            $dao = new \dao();

            $strSql = " SELECT keyname,displayname,prefix,startno,reset_type,hashkey from ".CONFIG_DBN.".cfindentnumber WHERE companyid=:companyid AND storeid=:storeid ";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':storeid',CONFIG_SID);
            $cntdata = $dao->executeQuery();
            $data = $dao->executeQuery();
            if(count($data)==0)
            {
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return html_entity_decode(json_encode($retvalue));
            }
            else
            {
                return html_entity_decode(json_encode(array(array("cnt"=>count($cntdata),"data"=>$data))));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - doclist - '.$e);
        }
    }


    public function docupdate($data)
    {
        try
        {
            $this->log->logIt($this->module.' - docupdate');
            $dao = new \dao();

            $ObjAuditDao = new \database\auditlogdao();
            $strSql = "";
            $title = "Edit Record";

            foreach($data['reset'] as $key=>$value)
            {

                if($key!="")
                {
                    $strSql .= " UPDATE ".CONFIG_DBN.".cfindentnumber SET prefix='".$data['prefix'][$key]."',
                    startno='".$data['startno'][$key]."',reset_type='".$data['reset'][$key]."' 
                    WHERE  hashkey='".$key."' AND companyid='".CONFIG_CID."' AND storeid='".CONFIG_SID."' ; ";
                    if($data['reset'][$key]==1){
                        $reset_type="Daily";
                    }
                    if($data['reset'][$key]==2){
                        $reset_type="Monthly";
                    }
                    if($data['reset'][$key]==3){
                        $reset_type="Yearly";
                    }

                    $arr_log = array(
                        "Prefix"=>$data['prefix'][$key],
                        "Start No"=>$data['startno'][$key],
                        "Reset Type"=>$reset_type,
                    );

                    $json_data = json_encode($arr_log);
                    $ObjAuditDao->addactivitylog($data['module'],$title,$key,$json_data);

                }
            }

            $dao->initCommand($strSql);
            $dao->executeNonQuery();
            return 1;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - docupdate - '.$e);
            return 0;
        }
    }

    public function getInvoiceNumbering()
    {
        try
        {
            $this->log->logIt($this->module.' - getInvoiceNumbering');

            $dao = new \dao();
            $strSql = "SELECT prefix,startno,reset_type FROM ".CONFIG_DBN.".cfindentnumber WHERE keyname='INVOICE' AND companyid=:companyid";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $rec = $dao->executeRow();
            return $rec;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getInvoiceNumbering - '.$e);
            return 0;
        }
    }
}
?>