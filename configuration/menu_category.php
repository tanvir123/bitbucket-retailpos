<?php
require_once(dirname(__FILE__)."/common/s3fileUpload.php");

class menu_category   
{
    public $module='menu_category';
    public $log;
    private $language,$lang_arr,$default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_menu_category');
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(7,$this->module);


            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(7);

            $jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            
            $ObjUserDao = new \database\menu_categorydao();
            $data = $ObjUserDao->categorylist(50,"0",'');
            $datacat = $ObjUserDao->categorylist('','','',1);

            $categorylist=json_decode($datacat,true);

            $Objtabinstadao = new \database\synctabinstadao();
            $tbdata = $Objtabinstadao->tabint();
			
            $ObjmenuDao = new \database\menu_menudao();
            $menu_data = $ObjmenuDao->menulist('','','',1);
            
            $menulist = json_decode($menu_data,true);
            
            $template = $twig->loadTemplate('menu_category.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
			$senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;
            $senderarr['categorylist'] = $categorylist[0]['data'];
            $senderarr['tablist'] = $tbdata;
			$senderarr['menulist'] = $menulist[0]['data'];
			$senderarr['jsdateformat'] = $jsdate;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
			
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    
    public function addeditfrm($data)
    {
        try {
            $this->log->logIt($this->module . ' - addeditfrm');
            $flag1 = \util\validate::check_combo($data, array('menuunkid'));
            $flag2 = \util\validate::check_notnull($data, array('categoryname', 'sku', 'rdo_status'));
            $this->loadLang();
            $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            if ($flag1 == 'true' && $flag2 == 'true') {
//				$bucket=CONFIG_BUCKET_NAME;
//	            $directoryname='pure-ipos-menu-category.images';
                $flag_file = isset($_FILES['upload_img']) ? $_FILES['upload_img'] : "";
                $img_flag = "";
                if ($flag_file['tmp_name'] != "") {
                    $directoryname = dirname(__FILE__) . '/imageconfiguration/';
                    if (!is_dir($directoryname)) {
                        //Directory does not exist, so lets create it.
                        mkdir($directoryname, 0777, true);
                    }
                    $temporary = explode(".", $flag_file["name"]);
                    $file_extension = end($temporary);
                    $flag_name = time() . '-' . $flag_file["name"];
                    $flag_dirname = dirname(__FILE__) . '/imageconfiguration/' . $flag_name;
                    $flag_db = "";
//                    $flag_name = time().'-'.$flag_file["name"];
                    if ((($flag_file["type"] == "image/png") || ($flag_file["type"] == "image/jpg") || ($flag_file["type"] == "image/jpeg"))
                        && ($flag_file["size"] < 24800000)) {
                        move_uploaded_file($flag_file["tmp_name"], $flag_dirname);
                        $flag_db = $flag_name;
//                        $arr_file = array();
//                        $arr_file[0]['name'] = $directoryname.'/'.$flag_name;
//                        $arr_file[0]['tmp_name'] = $flag_file['tmp_name'];
//                        $arr_file[0]['type'] = $flag_file["type"];
//
//                        $ObjFile = new s3fileUpload();
//						$ObjFile->createBucket($bucket);
//                        $resFile = $ObjFile->uploadFiles($arr_file,$bucket);
//                        if($resFile!='' && $resFile!=0){
//                            $img_flag = urldecode($resFile[0]);
                    }
                    $img_flag = (file_exists("imageconfiguration/$flag_db")) ? $flag_db : "";
//                    else{
//                            $img_flag = '';
//                        }
                }
                if ($img_flag != "") {
                    $imagename = CONFIG_COMMON_URL . 'imageconfiguration/' . $data['imagephoto'];

                    if ($imagename != "") {
                        $path = $imagename;
                        if (file_exists($path)) {
                            unlink($path);
                        }
                    }
                    $res = CONFIG_COMMON_URL . 'imageconfiguration/' . $img_flag; //add
                } else {
                    $res = $data['imagephoto'];
                }

//                if($img_flag!=""){
//                    $imagename = $data['imagephoto'];
//                    if($imagename!="")
//                    {
//                        $rmv_file = array();
//                        $rmv_file[] = $imagename;
//                        $ObjFile = new s3fileUpload();
//                        $ObjFile->deleteFiles($rmv_file,$bucket);
//                    }
//                    $res = $img_flag;
//                }else{
//                    $res = $data['imagephoto'];
//                }

                $reqarr = array(
                    "menuunkid" => $data['menuunkid'],
                    "pcatid" => $data['pcatid'],
                    "categoryname" => $data['categoryname'],
                    "longdescription" => $data['longdescription'],
                    "image" => $res,
                    "sku" => $data['sku'],
                    "rdo_status" => $data['rdo_status'],
                    "id" => $data['id'],
                    "module" => $this->module,
                    "category_color" => ($data['category_color'] != '') ? $data['category_color'] : null,
                    "category_font_color" => ($data['category_font_color'] != '') ? $data['category_font_color'] : null
                );
                $ObjMenuCategoryDao = new \database\menu_categorydao();

                $data = $ObjMenuCategoryDao->addCategory($reqarr, $languageArr, $defaultlanguageArr);
                return $data;

            } else{
                return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->SOME_FIELD_MISSING));
            }
        }

        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }
	
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            global $twig;
            $limit=50;
           $offset="0";
            $name="";
            
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
			$ObjUserDao = new \database\menu_categorydao();
			$data = $ObjUserDao->categorylist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

	public function getTaxRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getTaxRec");
			$ObjMenuCategoryDao = new \database\menu_categorydao();
			$data1 = $ObjMenuCategoryDao->getCatRec($data);
			return $data1;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getTaxRec - ".$e);
			return false; 
		}
	}

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_menu_category;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }

    public function categorysync()
    {
        try
        {
            $this->log->logIt($this->module.'categorysync');
            $modulename = $this->module;
            $location = \database\parameter::getparameter('tabinsta_locationid',CONFIG_CID,CONFIG_LID);

            $ObjDao = new \database\apisettingsdao();
            $data = $ObjDao->loadCompanyInfo();
            $data_arr = json_decode($data,true);
            $tabinsta_app_id = $data_arr['Data']['company_info']['tabinsta_app_id'];
            $tabinsta_app_secret = $data_arr['Data']['company_info']['tabinsta_app_secret'];

            $arr = array(
                "location_id"=>$location
            );

            $arr_str = json_encode($arr);

            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,CONFIG_CATEGORYSYNC);
            curl_setopt($ch,CURLOPT_HTTPHEADER,array(
                "Content-Type:application/json",
                "app-id:$tabinsta_app_id",
                "app-secret:$tabinsta_app_secret",
            ));
            curl_setopt($ch,CURLOPT_POST,1);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$arr_str);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            $res = curl_exec($ch);
            curl_close($ch);
            $data = trim($res);
            $dataa = json_decode($data,true);

            $listobj = new \database\synctabinstadao();
            $finaldata = $listobj->updatecategoryfromtabinsta($dataa,$modulename);
            return $finaldata;

        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module.'-categorysync -'.$e);
        }
    }

    public function getCategoryList($data)
    {
        try {
            $this->log->logIt($this->module . " - getCategoryList");
            $ObjmenuitemDao = new \database\menu_categorydao();
            $res = $ObjmenuitemDao->getCategoryList($data);

            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getItemList - " . $e);
            return false;
        }

    }
}
?>
    
