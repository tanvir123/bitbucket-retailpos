<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 31/5/17
 * Time: 11:53 AM
 */
class change_password
{
    private $module='change_password';
    private $log;
    private $encdec;

    function __construct()
    {
        try
        {
            $this->log = new \util\logger();
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module." - load");
            global $twig;
            $flag = 0;
            if(!isset($_POST) || !isset($_POST['id']) || !isset($_POST['module']))
            {
                $flag==1;
                $Obj="";
            }
            else{
                $id = (isset($_POST['id']))?$_POST['id']:"";
                $Objstaticarray =new \common\staticarray();
                $module = $Objstaticarray->auditlogmodules[$_POST['module']]['table'];
                $masterfield = $Objstaticarray->auditlogmodules[$_POST['module']]['masterfield'];
                $pd = $Objstaticarray->auditlogmodules[$_POST['module']]['pd'];

                $ObjactivityDao = new \database\auditlogdao();
                $data = $ObjactivityDao->loadindividualauditlogs($id,$module,$masterfield,$pd);


                $bj = json_decode($data,1);
                $Obj = $bj['Data'];
            }
            $template = $twig->loadTemplate('audit_individual_activity.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['flag'] = $flag;
            $senderarr['rec'] = $Obj;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - load - ".$e);
        }
    }

}
?>