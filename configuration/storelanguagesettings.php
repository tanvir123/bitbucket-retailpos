<?php

class storelanguagesettings
{
    public $module = 'storelanguagesettings';
    public $log;
    private $language,$lang_arr,$default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->redis = new \Redis();
        $this->redis->connect(CONFIG_REDIS_HOST, CONFIG_REDIS_PORT);
        $this->redis->select('15');
        $this->language = new \util\language($this->module);
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            /*$arr_store_modules = \common\staticlang::$store_modules;*/
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(66,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(66);
            $this->loadLang();
            $template = $twig->loadTemplate('storelanguage.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];

            /*$senderarr['store_modules'] = $arr_store_modules;*/

            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function loadLangParameters($data)
    {
        try {
            $this->log->logIt($this->module . ' - loadLangParameters');
            $arr_lang_label = array();
            $i = 0;
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            if($data['module_name']!='0'){
                $default_lang_arr = \common\staticlang::${$data['module_name']};
            }else{
                return json_encode(array("Success" => 'False', "Message" => $languageArr->LANG4));
            }
            $custom_lang_arr = json_decode($this->redis->get('3_'.CONFIG_CID.'_'.CONFIG_UID.'_'.CONFIG_DBN.'_'.$data['module_name'] . "_" . CONFIG_LANG), true);
            foreach ($default_lang_arr as $key => $value) {
                $arr_lang_label[$i]['key'] = $key;
                $arr_lang_label[$i]['default_label'] = $value;
               if (isset($custom_lang_arr[$key]) && $custom_lang_arr[$key] != '') {
                    $arr_lang_label[$i]['custom_label'] = $custom_lang_arr[$key];
                } else {
                    $arr_lang_label[$i]['custom_label'] = $default_lang_arr[$key];
                }
                $i++;
            }
            return json_encode(array("Success" => 'True', "Data" => $arr_lang_label));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - loadLangParameters - ' . $e);
        }
    }

    public function updateCustomLangLabel($data)
    {
        try {
            $this->log->logIt($this->module . ' - updateCustomLangLabel');
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);
            $this->redis->set('3_'.CONFIG_CID.'_'.CONFIG_UID.'_'.CONFIG_DBN.'_'.$data['module_name'] . "_" . CONFIG_LANG, json_encode($data['customlanglabel']));
            return json_encode(array('Success' => 'True', 'Message' => $defaultlanguageArr->REC_UP_SUC));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - updateCustomLangLabel - ' . $e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$storelanguagesettings;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }
}

?>