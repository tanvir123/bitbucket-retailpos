<?php
require(dirname(__FILE__).'/config/config.php');
require(dirname(__FILE__).'/common/encdec.php');

global $commonurl;
if(!count($_REQUEST)) exit(0);
            
if(!isset($_REQUEST['service']) && $_REQUEST['service']!='validuser') exit(0);

if(!isset($_REQUEST['opcode']) && $_REQUEST['opcode']!='checkcredentials') exit(0);

$opcode=$_REQUEST['opcode'];
$service = $_REQUEST['service'];

$obj = new $service();
$result = $obj->$opcode();

echo $result;

class validuser
{
    private $module='validuser';
    private $encdec;
    
    function __construct()
    {
        $this->encdec = new \common\encdec();
    }
    
    public function checkcredentials()
    {
        $username= $_POST['uname'];
        $pwd= $_POST['pwd'];
        $cid= $_POST['cid'];
        $arr = array(
            "service"=>"login",
            "opcode"=>"checkcredentials",
            "uname"=>$username,
            "pwd"=>$pwd,
            "cid"=>$cid,
        );
        $arr_str = json_encode($arr);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, CONFIG_SERVICEURL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,urlencode($arr_str));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $res=curl_exec($ch);
        curl_close($ch);
        $data = trim($res);
        return $data;
    }

    public function login_location()
    {
        $username= $_POST['uname'];
        $pwd= $_POST['pwd'];
        $cid= $_POST['cid'];
        $lid= $_POST['lid'];
        $type= $_POST['type'];
        $arr = array(
            "service"=>"login",
            "opcode"=>"login_location",
            "uname"=>$username,
            "pwd"=>$pwd,
            "cid"=>$cid,
            "lid"=>$lid,
            "type"=>$type,
        );
        $arr_str = json_encode($arr);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, CONFIG_SERVICEURL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,urlencode($arr_str));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $res=curl_exec($ch);
        curl_close($ch);
        $data = trim($res);
        $arr = json_decode($data,true);
        $uname = $this->encdec->openssl('encrypt',$arr['Data']['username']);
        $domain = ($_SERVER['HTTP_HOST'] != '192.168.0.42') ? $_SERVER['HTTP_HOST'] : false;
        setcookie('POS_ATKN_ID', $arr['Data']['access_token'], time() + (86400 * 30), '/',$domain,false);
        setcookie('POS_CNF_UN', $uname, time() + (86400 * 30), '/',$domain,false);
        return $data;
    }
}
?>