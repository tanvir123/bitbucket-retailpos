 <?php
 require_once(dirname(__FILE__)."/common/s3fileUpload.php");

class menu_modifier
{
    public $module = 'menu_modifier';
    public $log;
    private $language,$lang_arr,$default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_menu_modifier');
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(11,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(11);
            $ObjUserDao = new \database\menu_modifierdao();
            $data = $ObjUserDao->modifierlist(50,'0' ,'', '');

            $ObjitmunitDao = new \database\menu_item_unitdao();
            $itmunitData = $ObjitmunitDao->itemunitlist('','','',1);
            $itemUnitData = json_decode($itmunitData,1);

            $Objtabinstadao = new \database\synctabinstadao();
            $tbdata = $Objtabinstadao->tabint();

            $ObjTaxDao = new \database\taxdao();
            $taxDetail=$ObjTaxDao->getAppliedTax();

            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $template = $twig->loadTemplate('menu_modifier.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
            $senderarr['tablist'] = $tbdata;
            $senderarr['itmunitlist'] = $itemUnitData[0]['data'];
            $senderarr['taxlist'] = $taxDetail;
            $senderarr['datalist'] = $data;
            $senderarr['round_off'] = $round_off;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function addeditfrm($data)
    {
        try {
            $this->log->logIt($this->module . ' - addeditfrm');
            $ObjCommonDao = new \database\commondao;
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $flag1 = \util\validate::check_numeric($data, array('txtmin', 'txtmax'));
            $flag = \util\validate::check_notnull($data, array('txtname', 'txtmin', 'txtmax','txtsku'));
            $flag2 = 'true';
            $unit_arr = array();
            $rate_arr = array();
            if(isset($data['rates'])) {
                foreach ($data['rates'] AS $rval) {
                    $rate_arr[] = $rval;
                }
                $rates = implode(",", $rate_arr);
            }


            $is_tax_available=isset($data['tax'])?1:0;
            $is_rate_available=isset($data['rates'])?1:0;


            if($is_tax_available > 0 && $is_rate_available==0){
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
            }else if($is_tax_available == 0 && $is_rate_available>0){
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
            }


            //Bind Unit
            $bind_unit = isset($data['unit_detail']) ? $data['unit_detail'] : '';

            if ($bind_unit != '') {
                $bind_unit_arr = explode(',', $bind_unit);
                foreach ($bind_unit_arr AS $sub_unit) {
                    $upkey = $ObjCommonDao->getprimaryBycompany('cfmenu_itemunit', $sub_unit, 'unitunkid');
                    $unit_arr[$upkey]['rate1'] = $data['rate1_' . $sub_unit];
                    $unit_arr[$upkey]['rate2'] = $data['rate2_' . $sub_unit];
                    $unit_arr[$upkey]['rate3'] = $data['rate3_' . $sub_unit];
                    $unit_arr[$upkey]['rate4'] = $data['rate4_' . $sub_unit];

                    $unit_arr[$upkey]['defaultrate'] = $data['defaultrate_' . $sub_unit];

                    /* $defaultRate = $data['defaultrate_' . $sub_unit];
                     if (($data['defaultrate_' . $sub_unit] != "") && ($data['rate'.$defaultRate.'_' . $sub_unit] == ''))
                         $flag2 = 'false';
                     else
                         $unit_arr[$upkey]['rate'.$defaultRate.''] = $data['rate'.$defaultRate.'_' . $sub_unit];*/

                    //Bind recipe
                    $bind_recipe = isset($data['raw_detail_'.$sub_unit])?$data['raw_detail_'.$sub_unit]:'';

                    if ($bind_recipe != ''){
                        $bind_recipe_arr = explode(',', $bind_recipe);
                        $recipe_arr = array();

                        foreach ($bind_recipe_arr AS $sub_recipe) {

                            if (isset($data['store_' .$sub_unit.'_'. $sub_recipe]) != 1 || $data['store_' .$sub_unit.'_'. $sub_recipe] == "")
                                $flag2 = 'false';
                            else
                                $recipe_arr[$sub_recipe]['store'] = $data['store_' .$sub_unit.'_'.$sub_recipe];

                            if (isset($data['unit_' .$sub_unit.'_'. $sub_recipe]) != 1 || $data['unit_' .$sub_unit.'_'. $sub_recipe] == "")
                                $flag2 = 'false';
                            else
                                $recipe_arr[$sub_recipe]['unit'] = $data['unit_' .$sub_unit.'_'.$sub_recipe];

                            if ($data['unitval_' .$sub_unit.'_' .$sub_recipe] == "")
                                $flag2 = 'false';
                            else
                                $recipe_arr[$sub_recipe]['unitval'] = $data['unitval_'.$sub_unit.'_'.$sub_recipe];

                            if (isset($data['quantity_' .$sub_unit.'_'.$sub_recipe]) != 1)
                                $flag2 = 'false';
                            else
                                $recipe_arr[$sub_recipe]['quantity'] = $data['quantity_' .$sub_unit.'_'.$sub_recipe];
                        }
                        $unit_arr[$upkey]['recipe'] = $recipe_arr;
                    }
                }
            }
            //


            if ($flag == 'true' && $flag1 == 'true' && $flag2 == 'true') {
//				$bucket=CONFIG_BUCKET_NAME;
//				$directoryname='pure-ipos-menu-modifiers.images';
                $flag_file = isset($_FILES['upload_img'])?$_FILES['upload_img']:"";
                $img_flag="";
                if($flag_file['tmp_name']!="")
                {
                    $directoryname = dirname(__FILE__).'/imageconfiguration/';
                    if(!is_dir($directoryname)){
                        //Directory does not exist, so lets create it.
                        mkdir($directoryname, 0777, true);
                    }
                    $temporary = explode(".", $flag_file["name"]);
                    $file_extension = end($temporary);
                    $flag_name = time() . '-' . $flag_file["name"];
                    $flag_dirname = dirname(__FILE__) . '/imageconfiguration/' . $flag_name;

                    $flag_db = "";
//                    $flag_name = time().'-'.$flag_file["name"];
                    if ((($flag_file["type"] == "image/png") || ($flag_file["type"] == "image/jpg") || ($flag_file["type"] == "image/jpeg"))
                        && ($flag_file["size"] < 24800000))
                    {
                        move_uploaded_file($flag_file["tmp_name"], $flag_dirname);
                        $flag_db = $flag_name;
//                        $arr_file = array();
//                        $arr_file[0]['name'] = $directoryname.'/'.$flag_name;
//                        $arr_file[0]['tmp_name'] = $flag_file['tmp_name'];
//                        $arr_file[0]['type'] = $flag_file["type"];
//
//                        $ObjFile = new s3fileUpload();
//						$ObjFile->createBucket($bucket);
//                        $resFile = $ObjFile->uploadFiles($arr_file,$bucket);
//                        if($resFile!='' && $resFile!=0){
//                            $img_flag = urldecode($resFile[0]);
//                        }else{
//                            $img_flag = '';
//                        }
                    }
                    $img_flag = (file_exists("imageconfiguration/$flag_db")) ? $flag_db : "";
                }

                if($img_flag!=""){
                    $imagename = CONFIG_COMMON_URL . 'imageconfiguration/' . $data['imagephoto'];

                    if($imagename!="")
                    {
//                        $rmv_file = array();
//                        $rmv_file[] = $imagename;
//                        $ObjFile = new s3fileUpload();
//                        $ObjFile->deleteFiles($rmv_file,$bucket);
                        $path = CONFIG_COMMON_URL.'imageconfiguration/'.$imagename;

                        if (file_exists($path)) {

                          unlink($path);
                        }
                    }
                    $res = CONFIG_COMMON_URL . 'imageconfiguration/' . $img_flag; //add

                }else{
                    $res = $data['imagephoto'];
                }


                $reqarr = array(
                    "name" => $data['txtname'],
                    "min" => $data['txtmin'],
                    "max" => $data['txtmax'],
                    "txtlongdesc" => $data['txtlongdesc'],
                    "image" => $res,
                    "is_included" => $data['is_included'],
                    "txtsaleamount" => isset($data['txtsaleamount']) ? $data['txtsaleamount'] :0,
                    "sku" => $data['txtsku'],
                    "id" => $data['id'],
                    "rdo_status" => $data['rdo_status'],
                    "bind_unit"=>$unit_arr,
                    "bind_tax"=> isset($data['tax']) ? $data['tax'] : [],
                    "rates"=> isset($rates) ? $rates:'',
                    "module" => $this->module
                );
                $ObjmodifierDao = new \database\menu_modifierdao();
                $data = $ObjmodifierDao->addmodifier($reqarr,$defaultlanguageArr,$languageArr);
                return $data;
            } else
                return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->SOME_FIELD_MISSING));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addeditfrm - ' . $e);
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');
            $limit = 50;
            $offset = 0;
            $name = "";

            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['nm']) && $data['nm'] != "")
                $name = $data['nm'];
            $ObjmodifierDao = new \database\menu_modifierdao();
            $data = $ObjmodifierDao->modifierlist($limit, $offset, $name);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function removemodifier($data)
    {
        try
        {
            $this->log->logIt($this->module.' - removemodifier');
            $ObjCommonDao = new \database\menu_modifierdao();
            $data['module'] = $this->module;
            $res = $ObjCommonDao->removemodifier($data);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - removemodifier - '.$e);
        }
    }
    public function getRecmodifier($data)
    {
        try {
            $this->log->logIt($this->module . " - getRecmodifier");
            $ObjmodifierDao = new \database\menu_modifierdao();
            $data = $ObjmodifierDao->getRecmodifier($data);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getRecmodifier - " . $e);
            return false;
        }
    }

    public function getRawmaterial()
    {
        try
        {
            $this->log->logIt($this->module." - getRawmaterial");
            $ObjrawDao = new \database\rawmaterialdao();
            $data = $ObjrawDao->getAllRawdetail();
            return $data;
        }catch(Exception $e){
            $this->log->logIt($this->module." - getRawmaterial - ".$e);
            return false;
        }
    }
    public function getitemincluded($data)
    {
        try
        {
            $this->log->logIt($this->module." - getitemincluded");
            $ObjmodifierDao = new \database\menu_modifierdao();
            $data = $ObjmodifierDao->getitemincluded($data);
            return $data;
        }catch(Exception $e){
            $this->log->logIt($this->module." - getitemincluded - ".$e);
            return false;
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_menu_modifier;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }

    public function syncmenufromtabinsta()
    {
        try
        {
            $this->log->logIt($this->module.'-syncmenufromtabinsta');
            $modulename = $this->module;
            $location = \database\parameter::getparameter('tabinsta_locationid',CONFIG_CID,CONFIG_LID);

            $ObjDao = new \database\apisettingsdao();
            $data = $ObjDao->loadCompanyInfo();
            $data_arr = json_decode($data,true);
            $tabinsta_app_id = $data_arr['Data']['company_info']['tabinsta_app_id'];
            $tabinsta_app_secret = $data_arr['Data']['company_info']['tabinsta_app_secret'];

            $arr = array(
                "location_id"=>$location
            );

            $arr_str = json_encode($arr);

            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,CONFIG_MODIFIERSYNC);
            curl_setopt($ch,CURLOPT_HTTPHEADER,array(
                "Content-Type:application/json",
                "app-id:$tabinsta_app_id",
                "app-secret:$tabinsta_app_secret",
            ));
            curl_setopt($ch,CURLOPT_POST,1);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$arr_str);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            $res = curl_exec($ch);
            curl_close($ch);
            $data = trim($res);
            $dataa = json_decode($data,true);

            $listobj = new \database\synctabinstadao();
            $finaldata = $listobj->updatemodifierfromtabinsta($dataa,$modulename);
            return $finaldata;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-syncmenufromtabinsta -'.$e);
        }
    }
}

?>