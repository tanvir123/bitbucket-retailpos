<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 13/4/18
 * Time: 3:06 PM
 */
class received_voucher
{
    public $module = 'received_voucher';
    public $log;
    private $language, $lang_arr, $default_lang_arr;
    public $ObjIndentDao;
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->ObjIndentDao = new \database\indentdao();
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(55,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(55);
            $ObjVoucherDao = new \database\issuevoucherdao();
            $ObjStoreDao = new \database\indentrequestdao();

            $arr_indent = $ObjVoucherDao->receivedVoucherList(50,'0', '', '', '', '');
            $storeData = $ObjStoreDao->getallstorelist();
            $store_list = json_decode($storeData, 1);
            $template = $twig->loadTemplate('received_voucher.html');

            $ObjCategoryDao = new \database\raw_categorydao();
            $arr_category = $ObjCategoryDao->getAllCategory();
            $category_list = json_decode($arr_category, 1);
            
            $js_date_format = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $this->loadLang();
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['login_type'] = CONFIG_LOGINTYPE;
            $sender_arr['module'] = $this->module;
            $sender_arr['datalist'] = $arr_indent;
            $sender_arr['storelist'] = $store_list['Data'];
            $sender_arr['category'] = $category_list['Data'];
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $sender_arr['langlist'] = json_decode($languageArr);
            $sender_arr['jsdateformat'] = $js_date_format;
            $sender_arr['roundoff'] = $round_off;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $sender_arr['default_langlist'] = json_decode($defaultlanguageArr);
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            $sender_arr['lang_type'] = CONFIG_CUSTOM_LANG;
            $sender_arr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];

            echo $template->render($sender_arr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }
    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');
            $limit = 50;
            $offset = 0;
            $from_date = "";
            $to_date = "";
            $voucher = "";
            $store_id = "";

            if (isset($data['limit']) && $data['limit']!= "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset']!= "")
                $offset = $data['offset'];
            if (isset($data['fromdate']) && $data['fromdate']!= "")
                $from_date = $data['fromdate'];
            if (isset($data['todate']) && $data['todate']!= "")
                $to_date = $data['todate'];
            if (isset($data['voucher']) && $data['voucher']!= "")
                $voucher = $data['voucher'];
            if (isset($data['storeid']) && $data['storeid']!= "")
                $store_id = $data['storeid'];

            $ObjVoucherDao = new \database\issuevoucherdao();
            $result = $ObjVoucherDao->receivedVoucherList($limit, $offset,$from_date,$to_date,$voucher,$store_id);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }
    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$received_voucher;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
    public function printInvoice($data)
    {
        try
        {
            $this->log->logIt($this->module.' - printInvoice');
            $id = $data['id'];

            $ObjIndentDao = new \database\indentdao();
            $headerData = $ObjIndentDao->getreceviedindentdetail($id);
            $PrintReport = new \common\printreport();

            $PrintReport->addTemplate('receivedindentvoucher');
            $width = '793px';
            $height = '1122px';

            $PrintReport->addRecord($headerData);
            $data = $PrintReport->Request();
            return json_encode(array("Success"=>"True","Data"=>$data, "Height"=>$height, "Width"=>$width));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - printInvoice - '.$e);
        }
    }public function received_voucherprintInvoice($data)
    {
        try
        {
            $this->log->logIt($this->module.' - received_voucherprintInvoice');
            $id = $data['id'];

            $ObjIndentDao = new \database\issuevoucherdao();
            $headerData = $ObjIndentDao->getissuedvoucherdetail($id);
            if($headerData){
                $this->loadLang();
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
                $headerData['default_langlist'] = json_decode($defaultlanguageArr);         $senderarr['user_type'] = CONFIG_USR_TYPE;
                $indentlanguage = new \util\language('AuthorizeReceivedindent');
                $llang_arr = \common\staticlang::$AuthorizeReceivedindent;
                $languageArr=html_entity_decode(json_encode($indentlanguage->loadlanguage($llang_arr)),ENT_QUOTES);
                $headerData['langlist'] = json_decode($languageArr);
            }
            $PrintReport = new \common\printreport();

            $PrintReport->addTemplate('receivedvoucher');
            $width = '793px';
            $height = '1122px';

            $PrintReport->addRecord($headerData);
            $data = $PrintReport->Request();
            return json_encode(array("Success"=>"True","Data"=>$data, "Height"=>$height, "Width"=>$width));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - received_voucherprintInvoice - '.$e);
        }
    }
}
?>