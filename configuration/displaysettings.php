<?php
class displaysettings   
{
    public $module='displaysettings';
    public $log;
    public $jsdateformat;
    private $language,$lang_arr,$default_lang_arr,$store_language,$invoice_template,$month,$configUser_Mandatory,$config_number;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_displaysettings');
        $this->store_language = new \util\language('config_salutation');
        $this->invoice_template = new \util\language('config_invoice_template');
        $this->month = new \util\language('config_Month');
        $this->configUser_Mandatory = new \util\language('config_User_Mandatory');
        $this->config_number = new \util\language('config_number');
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(16,$this->module);
            $ObjStaticArray =new \common\staticarray();
			$ObjDao = new \database\displaysettingsdao();
			$loc_info = $ObjDao->getLocationInfo();

            $timezoneslist = $ObjStaticArray->timezones;
            $default_dateformat = $ObjStaticArray->default_dateformat;
            $default_timeformat = $ObjStaticArray->default_timeformat;

            $salutation_lang_arr = \common\staticlang::$config_salutation;
            $salutation = $this->store_language->loadlanguage($salutation_lang_arr);
            $salutationArr=html_entity_decode(json_encode($salutation),ENT_QUOTES);
            $salutationArr = (array)json_decode($salutationArr);

            $Invoice_lang_arr = \common\staticlang::$config_invoice_template;
            $invoice = $this->invoice_template->loadlanguage($Invoice_lang_arr);
            $invoiceArr=html_entity_decode(json_encode($invoice),ENT_QUOTES);
            $invoice_arr = (array)json_decode($invoiceArr);

            $Month_lang_arr = \common\staticlang::$config_Month;
            $month = $this->month->loadlanguage($Month_lang_arr);
            $MonthArr=html_entity_decode(json_encode($month),ENT_QUOTES);
            $month_arr = (array)json_decode($MonthArr);

            $Config_mandatory_lang_arr = \common\staticlang::$config_User_Mandatory;
            $mandatory= $this->configUser_Mandatory->loadlanguage($Config_mandatory_lang_arr);
            $mandatoryArr=html_entity_decode(json_encode($mandatory),ENT_QUOTES);
            $mandatory_arr = (array)json_decode($mandatoryArr);

            $Config_number_lang_arr = \common\staticlang::$config_number;
            $number= $this->config_number->loadlanguage($Config_number_lang_arr);
            $numberArr=html_entity_decode(json_encode($number),ENT_QUOTES);
            $number_arr = (array)json_decode($numberArr);

            $roundoff = $ObjStaticArray->roundoff;

            $ObjPayTypeDao = new \database\paymenttypedao();
            $paymenttype = $ObjPayTypeDao->getAllPaymentTypes();
            $paymenttype = json_decode($paymenttype,1);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(16);

            $is_ordering=$OBJCOMMONDAO->getFieldValue('cflocation','is_ordering','locationid',CONFIG_LID);

            $getdenominationlist=$OBJCOMMONDAO->getDenominationlist();

            if(count($getdenominationlist) == 0)
            {
                \database\parameter::setParameter('cashdrawer_popup_at_login_logouttime',0);
                \database\parameter::setParameter('cashdrawer_report',0);

            }

            $data = $ObjDao->loaddisplaysettingslist();
            $data_arr = json_decode($data,true);
            $data_arr['Data']['displaysettings']['is_ordering']=isset($is_ordering)?$is_ordering:0;

            $template = $twig->loadTemplate('displaysettings.html');
            $this->loadLang();

            $tagDetail=$ObjDao->getTagrecords();
            if($tagDetail){
                $tagDetail=json_decode($tagDetail['meta_value'],1);
            }

            $tablesettings=$ObjDao->getTableSettings();
            if($tablesettings){
                $tablesettings=json_decode($tablesettings['meta_value'],1);
            }

            $ObjTaxDao = new \database\taxdao();
            $taxDetail=$ObjTaxDao->getAppliedTax();

            $ObjUserDao = new \database\menu_categorydao();
            $categorylist = $ObjUserDao->categorylist('','','',1);
            $categorylist = json_decode($categorylist,1);

            $metarec=$this->getmetarecords();

            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['categorylist'] = $categorylist[0]['data'];
            $senderarr['module'] = $this->module;
            $senderarr['timezoneslist'] = $timezoneslist;
            $senderarr['mandatoryinfo'] = $mandatory_arr;
            $senderarr['default_dateformat'] = $default_dateformat;
            $senderarr['default_timeformat'] = $default_timeformat;
            $senderarr['salutations'] = $salutationArr;
            $senderarr['roundoff'] = $roundoff;
            $senderarr['monthlist'] = $month_arr;
            $senderarr['numberlist'] = $number_arr;
			$senderarr['invoice_arr'] = $invoice_arr;
            $jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $jstime =  \common\staticarray::$jstimeformat[\database\parameter::getParameter('timeformat')];
            $senderarr['countrylist'] = $data_arr['Data']['countrylist'];
            $senderarr['jsdateformat'] = $jsdate;
            $senderarr['jstimeformat'] = $jstime;
            $senderarr['datalist'] = $data_arr['Data']['displaysettings'];
            $senderarr['fncialyearstartmonth'] = $data_arr['Data']['displaysettings']['fncialyearstartmonth'];
            $senderarr['fncialyearstartdate'] = $data_arr['Data']['displaysettings']['fncialyearstartdate'];
            $senderarr['consolidatemenuitemoption'] = $data_arr['Data']['displaysettings']['consolidatemenuitemoption'];


            $senderarr['td_no'] =$tagDetail['td_no'];
            $senderarr['p_no'] = $tagDetail['p_no'];
            $senderarr['h2_no'] = $tagDetail['h2_no'];
            $senderarr['h3_no'] = $tagDetail['h3_no'];
            $senderarr['h4_no'] = $tagDetail['h4_no'];
            $senderarr['h5_no'] = $tagDetail['h5_no'];
            $senderarr['h6_no'] = $tagDetail['h6_no'];


            $senderarr['pax_limit'] =$tablesettings['pax_limit'];
            $senderarr['avg_time'] = $tablesettings['avg_time'];
            $senderarr['days_before_booking'] = $tablesettings['days_before_booking'];
            $senderarr['hours_before_booking'] = $tablesettings['hours_before_booking'];
            $senderarr['advanced_payment_available'] = $tablesettings['advanced_payment_available'];


            $senderarr['is_night_audit_enable'] = $loc_info['is_night_audit_enable'];
//            $senderarr['is_table_booking_enable'] = $loc_info['is_table_booking_enable'];
            $senderarr['is_ordering_app'] = CONFIG_IS_ORDERING_APPLICATION;


            $tax_detail= $data_arr['Data']['displaysettings']['gratuity_tax_detail'];
            $senderarr['gratuity_tax_detail'] = html_entity_decode($tax_detail,ENT_QUOTES);
            $senderarr['payment_type'] = $paymenttype['Data'];
            $senderarr['taxlist'] = $taxDetail;
            $senderarr['metarec'] = json_decode($metarec,1);

            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;

            $senderarr['denomination_list'] = count($getdenominationlist);

            $fiscal_setting = $OBJCOMMONDAO->getFiscalsetting();
            $senderarr['fiscal_setting'] = $fiscal_setting;

            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function editdisplaysettings($data)
    {
        try
        {
            $this->log->logIt($this->module.' - editdisplaysettings');
            $flag = \util\validate::check_notnull($data,array('select_time_format','select_date_format','timezone','select_country','select_round_type','select_salutation','select_nationality','invoice'));
            $flag2 = \util\validate::check_combo($data,array('payment_type'));

            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $langlist = json_decode($languageArr,true);

            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            if(isset($data['night_audit_cc_email']) && $data['night_audit_cc_email']!='')
            {
                $arr = explode(',',$data['night_audit_cc_email']);
                $arr1 = array_unique($arr);
                if(count($arr) != count($arr1)) {
                    return json_encode(array("Success"=> "False","Message"=>$langlist['LANG148'].' '.$langlist['LANG151'].' '.$langlist['LANG147']));
                }
                if(count($arr1)<11) {
                    foreach ($arr1 AS $value) {
                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            return json_encode(array("Success"=> "False","Message"=>$langlist['LANG149'].' '.$langlist['LANG151'].' '.$langlist['LANG147']));
                        }
                    }
                }
                else {
                    return json_encode(array("Success"=> "False","Message"=>$langlist['LANG150'].' '.$langlist['LANG151'].' '.$langlist['LANG147']));
                }
            }

            if($data['taxrule']==1)
            {
                $amount = 0;
            }
            else{
                $amount = $data['amount'];
            }

            $tax_array =array();
            if(isset($data['tax']))
            {
                foreach($data['tax'] as $key=>$value)
                {
                    if(isset($value['taxid']))
                        $tax_array[$value['taxid']]=$value;
                }
            }

            if($flag=='true' && $flag2=='true'){
                $reqarr = array(
                            "select_time_format" => $data['select_time_format'],
                            "select_date_format"=>$data['select_date_format'],
                            "select_timezone"=>$data['timezone'],
                            "fncialyearstartdate"=>$data['fncialyearstartdate'],
                            "fncialyearstartmonth"=>$data['fncialyearstartmonth'],
                            "select_country"=>$data['select_country'],
                            "select_nationality"=>$data['select_nationality'],
                            "select_salutation"=>$data['select_salutation'],
                            "select_round_type"=>$data['select_round_type'],
                            "select_decimal_digit"=>$data['select_decimal_digit'],
                            "opening_time"=>$data['opening_time'],
                            "closing_time"=>$data['closing_time'],
                            "nightauditchnage_time"=>$data['nightauditchnage_time'],
                           // "is_preorder"=>isset($data['is_preorder'])?$data['is_preorder']:'',
                           // "location_info"=>isset($data['location_info'])?$data['location_info']:'',
                           // "terms_condition"=>isset($data['terms_condition'])?$data['terms_condition']:'',
                            "txtlimit"=>isset($data['txtlimit'])?$data['txtlimit']:"",
                            "payment_type"=>$data['payment_type'],
                            "datetime"=> \util\util::getLocalDateTime(),
							"user_info"=>isset($data['user_info'])?$data['user_info']:"",
							"visibility_field"=>isset($data['personal_visibilityinfo'])?$data['personal_visibilityinfo']:"",
							"invoice"=>isset($data['invoice'])?$data['invoice']:"",
							//"closefolioonsettlement"=>isset($data['closefolioonsettlement'])?$data['closefolioonsettlement']:0,
							"invoice_display_lbl"=>isset($data['invoice_display_lbl'])?$data['invoice_display_lbl']:'',
                   // "consolidatemenuitemoption"=>isset($data['consolidatemenuitemoption'])?$data['consolidatemenuitemoption']:0,
        // "takeaway_nocharge_user_available"=>isset($data['takeaway_nocharge_user_available']) ?$data['takeaway_nocharge_user_available']:0,
	// "genrate_invoice_for_nochargeuser"=>isset($data['genrate_invoice_for_nochargeuser'])?$data['genrate_invoice_for_nochargeuser']:0,
                    "cashdrawer_popup_at_login_logouttime"=>isset($data['cashdrawer_popup_at_login_logouttime'])?$data['cashdrawer_popup_at_login_logouttime']:0,
	                        "table_netlock"=>isset($data['table_netlock'])?$data['table_netlock']:0,
                            "display_unit_selection"=>isset($data['display_unit_selection'])?$data['display_unit_selection']:0,
                            "display_unit_selection_for_combo"=>isset($data['display_unit_selection_for_combo'])?$data['display_unit_selection_for_combo']:0,
                            "kot_print_preview_enable"=>isset($data['kot_print_preview_enable'])?$data['kot_print_preview_enable']:0,
                            "item_display_type_in_menu"=>isset($data['item_display_type_in_menu'])?$data['item_display_type_in_menu']:0,
                           // "finish_and_print_print_kot_or_invoice"=>isset($data['finish_and_print_print_kot_or_invoice'])?$data['finish_and_print_print_kot_or_invoice']:0,
                            "kot_cancel_item_display"=>isset($data['kot_cancel_item_display'])?$data['kot_cancel_item_display']:0,
			 //   "display_table_class_in_kot"=>isset($data['display_table_class_in_kot'])?$data['display_table_class_in_kot']:0,
                            "refund_payment_and_show_refundorders"=>isset($data['refund_payment_and_show_refundorders'])?$data['refund_payment_and_show_refundorders']:0,
                            "bill_to_company_account"=>isset($data['bill_to_company_account'])?$data['bill_to_company_account']:0,
                            "item_replacement_option_for_combo_item"=>isset($data['item_replacement_option_for_combo_item'])?$data['item_replacement_option_for_combo_item']:0,
                            "cancel_order_kot_generate"=>isset($data['cancel_order_kot_generate'])?$data['cancel_order_kot_generate']:0,
                            //"kot_display_with_price"=>isset($data['kot_display_with_price'])?$data['kot_display_with_price']:0,
                            "use_multicurrency_on_settlement"=>isset($data['use_multicurrency_on_settlement'])?$data['use_multicurrency_on_settlement']:0,
                            //"waiting_list"=>isset($data['waiting_list'])?$data['waiting_list']:0,
                            "kot_display_with_other_info"=>isset($data['kot_display_with_other_info'])?$data['kot_display_with_other_info']:0,
                            "choose_kot_format"=>isset($data['choose_kot_format'])?$data['choose_kot_format']:1,
                            "fiscal_invoice_types"=>isset($data['fiscal_invoice_types'])?$data['fiscal_invoice_types']:1,
                            "print_kotsettings_after_onetimeprint"=>isset($data['print_kotsettings_after_onetimeprint'])?$data['print_kotsettings_after_onetimeprint']:1,
                           // "choose_rate"=>isset($data['choose_rate'])?$data['choose_rate']:1,
                            "avg_food_time"=>isset($data['avg_food_time'])?$data['avg_food_time']:0,
                           // "printonfinishorder"=>isset($data['printonfinishorder'])?$data['printonfinishorder']:0,
                            "monthly_statistics"=>isset($data['monthly_statistics'])?$data['monthly_statistics']:0,
                            "daily_revenue"=>isset($data['daily_revenue'])?$data['daily_revenue']:0,
                            "item_sale_statistics"=>isset($data['item_sale_statistics'])?$data['item_sale_statistics']:0,
                            "daily_satistics"=>isset($data['daily_satistics'])?$data['daily_satistics']:0,
                            "night_audit_email"=>isset($data['night_audit_email'])?$data['night_audit_email']:"",
                            "payment_type_report"=>isset($data['payment_type_report'])?$data['payment_type_report']:0,
                    "nochargeuser_report"=>isset($data['nochargeuser_report'])?$data['nochargeuser_report']:0,
                    "complementary_report"=>isset($data['complementary_report'])?$data['complementary_report']:0,
                    "gratuity_report"=>isset($data['gratuity_report'])?$data['gratuity_report']:0,
		    "waiter_wise_sale_report"=>isset($data['waiter_wise_sale_report'])?$data['waiter_wise_sale_report']:0,
                    "combo_report"=>isset($data['combo_report'])?$data['combo_report']:0,
                    "cashdrawer_report"=>isset($data['cashdrawer_report'])?$data['cashdrawer_report']:0,
                            "gratuity_tax_detail"=>isset($tax_array)?$tax_array:'',
                            "gratuity_tax_type"=>$data['taxrule'],
                            "gratuity_tax_value"=>$amount,
                            "userid"=> CONFIG_UID,
                            "dian_num"=>isset($data['dian_num'])?$data['dian_num']:'',
                            "registration_no"=>isset($data['registration_no'])?$data['registration_no']:'',
                            "appliesfrom"=>isset($data['appliesfrom'])?$data['appliesfrom']:'',
                    "welcomemsg" => isset($data['welcomemsg']) ? $data['welcomemsg'] : '',
                    "note" => isset($data['note']) ? $data['note'] : '',
                    "td_no" => isset($data['td_no']) ? $data['td_no'] : '',
                    "p_no" => isset($data['p_no']) ? $data['p_no'] : '',
                    "h2_no" => isset($data['h2_no']) ? $data['h2_no'] : '',
                    "h3_no" => isset($data['h3_no']) ? $data['h3_no'] : '',
                    "h4_no" => isset($data['h4_no']) ? $data['h4_no'] : '',
                    "h5_no" => isset($data['h5_no']) ? $data['h5_no'] : '',
                    "h6_no" => isset($data['h6_no']) ? $data['h6_no'] : '',
                    "pax_limit" => isset($data['pax_limit']) ? $data['pax_limit'] : '',
                    "avg_time" => isset($data['avg_time']) ? $data['avg_time'] : '',
                    "days_before_booking" => isset($data['days_before_booking']) ? $data['days_before_booking'] : '',
                    "hours_before_booking" => isset($data['hours_before_booking']) ? $data['hours_before_booking'] : '',
                    "advanced_payment_available" => isset($data['advanced_payment_available']) ? $data['advanced_payment_available'] : 0,
                    "is_ordering" => isset($data['is_ordering']) ? $data['is_ordering'] : 0,
                    "kot_port_type" => isset($data['kot_port_type']) ? $data['kot_port_type'] : 0,
                    "keyboard_status" => isset($data['keyboard_status']) ? $data['keyboard_status'] : 0,


                     "companyid"=> CONFIG_CID,
                     "module" => $this->module
                    );


                $reqarr['print_portdata']['lpt1']=isset($data['lpt1']) ? $data['lpt1'] : 0;
                $reqarr['print_portdata']['lpt2']=isset($data['lpt2']) ? $data['lpt2'] : 0;
                $reqarr['print_portdata']['lpt3']=isset($data['lpt3']) ? $data['lpt3'] : 0;
                if(isset($data['night_audit_cc_email']) && $data['night_audit_cc_email']!='undefined'){
                    $reqarr['night_audit_cc_email'] = isset($data['night_audit_cc_email']) ? $data['night_audit_cc_email'] : '';
                }

                $reqarr['print_portdata']['lpt1_detail']=isset($data['lpt1_detail']) ? $data['lpt1_detail'] : '';
                $reqarr['print_portdata']['lpt2_detail']=isset($data['lpt2_detail']) ? $data['lpt2_detail'] : '';
                $reqarr['print_portdata']['lpt3_detail']=isset($data['lpt3_detail']) ? $data['lpt3_detail'] : '';

                $reqarr['print_portdata']['port1_name']=isset($data['port1_name']) ? $data['port1_name'] : '';
                $reqarr['print_portdata']['port2_name']=isset($data['port2_name']) ? $data['port2_name'] : '';
                $reqarr['print_portdata']['port3_name']=isset($data['port3_name']) ? $data['port3_name'] : '';


                $ObjDao = new \database\displaysettingsdao();
                $result = $ObjDao->editDisplaysettings($reqarr);
				return $result;
            }else
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
             
        }catch(Exception $e){
            $this->log->logIt($this->module.' - editdisplaysettings - '.$e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_displaysettings;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }

    public function gettemplaterecords($data)
    {
        try {
            $this->log->logIt($this->module . " - gettemplaterecords");
            $dao = new \dao();
            $meta_key="template".$data['template_id'];
            $strSql2 = "SELECT meta_value FROM " . CONFIG_DBN . ". cflocationtemplate_meta  WHERE
                            companyid=:companyid AND locationid=:locationid AND meta_key=:meta_key 
                            ORDER BY locationmetaunkid DESC LIMIT 1";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
            $dao->addParameter(':meta_key', $meta_key);
           $res= $dao->executeRow();
            return html_entity_decode(json_encode(array("Success" => "True", "Data" =>$res)));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - gettemplaterecords - " . $e);
            return false;
        }
    }


    public function getmetarecords()
    {
        try {
            $this->log->logIt($this->module . " - getmetarecords");
            $dao = new \dao();
            $strSql2 = "SELECT meta_key,meta_value FROM " . CONFIG_DBN . ".cfcompanylocation_meta  WHERE
                            companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addParameter(':locationid', CONFIG_LID);
           $res= $dao->executeQuery();

            $res = \util\util::arrangeArrayPair($res,'meta_key','meta_value');
            if(!$res){
                $res['lpt1']=0;
                $res['lpt2']=0;
                $res['lpt3']=0;
                $res['lpt1_detail']='';
                $res['lpt2_detail']='';
                $res['lpt3_detail']='';
                $res['keyboard_status']=0;
            }else{
                $res['keyboard_status'] = isset($res['keyboard_status'])?$res['keyboard_status']:0;
            }
            return json_encode($res);
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getmetarecords - " . $e);
            return false;
        }
    }

}
?>