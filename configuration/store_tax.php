<?php
class store_tax
{
    public $module='tax';
    public $log;
    private $language,$lang_arr,$default_lang_arr;
    public $jsdateformat;
    
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('tax');
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(52,$this->module);
            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(52);

            $jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $ObjUserDao = new \database\taxdao();
			$data = $ObjUserDao->taxlist(50,'0','');
			$Tax_Count= (array)json_decode($data);
			$total_tax= $Tax_Count[0]->cnt;
            $Objtabinstadao = new \database\synctabinstadao();
            $tbdata = $Objtabinstadao->tabint();

            $Objlocationdao = new \database\locationdao();
            $locationdata = $Objlocationdao->loadLocationInfo();

            $template = $twig->loadTemplate('store_tax.html');
            
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['datalist'] = $data;
            $senderarr['location'] = $locationdata;
            $senderarr['tablist'] = $tbdata;
            $senderarr['module'] = $this->module;
            $senderarr['jsdateformat'] = $jsdate;
            $senderarr['Total_tax'] = $total_tax;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
            $flag1 = \util\validate::check_numeric($data,array('amount'));
            $flag2 = \util\validate::check_notnull($data,array('shortname','taxname','appliesfrom','amount'));
            if($flag1=='true' && $flag2=="true"){
				$reqarr = array(
                            "shortname" => $data['shortname'],
                            "tax"=> $data['taxname'],
                            "appliesfrom"=> $data['appliesfrom'],
                            "amount"=> $data['amount'],
                            "is_gst" => isset($data['is_gst']) ? $data['is_gst'] : 0,
                            "taxrule"=> $data['taxrule'],
                            "applytax"=> $data['applytax'],
                            "id"=> $data['id'],
                            "rdo_status"=> $data['rdo_status'],
                            "module" => $this->module
                        );
                $this->loadLang();
                $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);
				$ObjPaymentTypeDao = new \database\taxdao();
                $data = $ObjPaymentTypeDao->addTax($reqarr,$languageArr,$defaultlanguageArr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>$this->default_lang_arr['Missing_Field']));
        }catch(Exception $e){
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }

    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
			$ObjUserDao = new \database\taxdao();
			$data = $ObjUserDao->taxlist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	public function getTaxRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getTaxRec");
			$ObjPaymentTypeDao = new \database\taxdao();
			$data = $ObjPaymentTypeDao->getTaxRec($data);
			return $data;
		}catch(Exception $e)
        {
			$this->log->logIt($this->module." - getTaxRec - ".$e);
			return false; 
		}
	}

    public function getDetailsRec($data)
    {
        try
        {
            $this->log->logIt($this->module." - getDetailsRec");
            $ObjPaymentTypeDao = new \database\taxdao();
            $data = $ObjPaymentTypeDao->getDetailsRec($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getDetailsRec - ".$e);
            return false; 
        }
    }

     public function loadLang()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$tax;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
    public function gettaxfrompms()
    {
        try
        {
            $this->log->logIt($this->module." -gettaxfrompms");

            $userhtl = \database\parameter::getParameter('pms_hotelid');

            $objUser =   new \database\orderdao();
            $data1 = $objUser->takedataforreservation();

            $arr = array(
                "service" =>"taxlistforpos",
                "opcode" => "taxlist",
                "uname"=>$data1['pms_username'],
                "pwd"=>$data1['pms_password'],
                "hid"=>$userhtl,
            );

            $arr_str = json_encode($arr);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, CONFIG_PMSSERVICE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,urlencode($arr_str));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $res=curl_exec($ch);
            curl_close($ch);
            $data = trim($res);
            // $arr = json_decode($data,true);
            return $data;

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." -gettaxfrompms -".$e);
        }
    }
    public function GetTaxCount()
    {
        try
        {
            $this->log->logIt($this->module." -GetTaxCount");
            $ObjPaymentTypeDao = new \database\taxdao();
            $data = $ObjPaymentTypeDao->GetTaxCount();
            return $data;

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." -GetTaxCount -".$e);
        }
    }

    public function synctaxfromtabinsta()
    {
        try
        {

            $this->log->logIt($this->module.'-synctaxfromtabinsta');
            $modulename = $this->module;

            $ObjDao = new \database\apisettingsdao();
            $data = $ObjDao->loadCompanyInfo();
            $data_arr = json_decode($data,true);
            $tabinsta_app_id = $data_arr['Data']['company_info']['tabinsta_app_id'];
            $tabinsta_app_secret = $data_arr['Data']['company_info']['tabinsta_app_secret'];


            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,CONFIG_TAXSYNC);
            curl_setopt($ch,CURLOPT_HTTPHEADER,array(
                "Content-Type:application/json",
                "app-id:$tabinsta_app_id",
                "app-secret:$tabinsta_app_secret",
            ));
            curl_setopt($ch,CURLOPT_HTTPGET,1);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            $res = curl_exec($ch);
            curl_close($ch);
            $data = trim($res);
            $dataa = json_decode($data,true);

            $listobj = new \database\synctabinstadao();
            $finaldata = $listobj->updatetaxfromtabinsta($dataa,$modulename);
            return $finaldata;

        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module.'-synctaxfromtabinsta -'.$e);
        }
    }




}
?>