<?php

require_once(dirname(__FILE__)."/common/s3fileUpload.php");

class combo_product
{
    public $module = 'combo_product';
    public $log;
    private $language, $lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_combo_product');
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(13, $this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(13);
            $ObjUserDao = new \database\combo_productdao();
            $data = $ObjUserDao->comboproductlist(50, '0', '');
            $ObjcategoryDao = new \database\menu_categorydao();
            $categorydata = $ObjcategoryDao->categorylist(50, '0', '', 1);
            $categorylist = json_decode($categorydata, true);

            $ObjitmunitDao = new \database\menu_item_unitdao();
            $itmunitData = $ObjitmunitDao->itemunitlist('', '', '', 1);
            $itemUnitData = json_decode($itmunitData, 1);

            $ObjTaxDao = new \database\taxdao();
            $taxDetail = $ObjTaxDao->getAppliedTax();

            $Zomatosetting = $OBJCOMMONDAO->getZomatosetting();

            $jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $round_off = \database\parameter::getParameter('digitafterdecimal');

            $template = $twig->loadTemplate('combo_product.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;
            $senderarr['categorylist'] = $categorylist[0]['data'];
            $senderarr['itmunitlist'] = $itemUnitData[0]['data'];
            $senderarr['taxlist'] = $taxDetail;
            $senderarr['langlist'] = $this->lang_arr;
            $senderarr['default_langlist'] = $this->default_lang_arr;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['round_off'] = $round_off;
            $senderarr['is_zomato'] = $Zomatosetting;
            $senderarr['jsdateformat'] = $jsdate;

            /*if($template->render($senderarr)){
                header("Location:".CONFIG_COMMON_URL."cloud/dashboard");
            }
            */

            echo $template->render($senderarr);

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$config_combo_product;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }

//    public function getChkboxRec($data)
//    {
//        try {
//            $this->log->logIt($this->module . ' - getChkboxRec');
//            $ObjComboDao = new \database\combo_productdao();
//            $data = $ObjComboDao->getChkboxRec($data);
//
//            return $data;
//        } catch (Exception $e) {
//            $this->log->logIt($this->module . ' - getChkboxRec - ' . $e);
//        }
//    }

    public function addeditfrm($data)
    {
        try {
            $this->log->logIt($this->module . ' - addeditfrm');

            $ObjCommonDao = new \database\commondao;
            $this->loadLang();
            $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $flag1 = \util\validate::check_notnull($data, array('comboname', 'txtsku', 'rdo_status'));
            $flag2 = \util\validate::check_combo($data, array('serve_unit'));
            $flag4 = 'true';

            $ObjmenuitemDao = new \database\menu_itemdao();
            $is_tax_available = $ObjmenuitemDao->checktaxavailableornot(isset($data['tax']) ? $data['tax'] : '');
            $is_rate_available = isset($data['rates']) ? 1 : 0;


            if ($is_tax_available > 0 && $is_rate_available == 0) {
                return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->SOME_FIELD_MISSING));
            } else if ($is_tax_available == 0 && $is_rate_available > 0) {
                return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->SOME_FIELD_MISSING));
            }

            $unit_arr = array();
            $rate_arr = array();
            if (isset($data['rates']) && $data['rates'] != '') {
                foreach ($data['rates'] AS $rval) {
                    $rate_arr[] = $rval;
                }
            }

            $rates = implode(",", $rate_arr);

            $bind_unit = isset($data['unit_detail']) ? $data['unit_detail'] : '';

            if ($bind_unit != '') {
                $bind_unit_arr = explode(',', $bind_unit);

                foreach ($bind_unit_arr AS $sub_unit) {

                    $upkey = $ObjCommonDao->getprimaryBycompany('cfmenu_itemunit', $sub_unit, 'unitunkid');
                    $unit_arr[$upkey]['rate1'] = $data['rate1_' . $sub_unit];
                    $unit_arr[$upkey]['rate2'] = $data['rate2_' . $sub_unit];
                    $unit_arr[$upkey]['rate3'] = $data['rate3_' . $sub_unit];
                    $unit_arr[$upkey]['rate4'] = $data['rate4_' . $sub_unit];

                    $unit_arr[$upkey]['defaultrate'] = $data['defaultrate_' . $sub_unit];

                    // Bind Group

                    $item_group = isset($data['itemgroups_' . $sub_unit]) ? $data['itemgroups_' . $sub_unit] : '';
                    if ($item_group != '') {
                        $item_group_arr = explode(',', $item_group);

                        $itemgroup_bind_arr = array();

                        $cnt5 =0;

                        foreach ($item_group_arr AS $val5) {

                        $itemgroup_bind_arr[$cnt5]['itemgroupid'] = $val5;

                        if(isset($data['defaultItem_' . $sub_unit . '_' . $val5 ]) && $data['defaultItem_' . $sub_unit . '_' . $val5 ] == ""  && $data['defaultItem_' . $sub_unit . '_' . $val5 ] == 0)
                            $flag4 = 'false';
                        else
                            $itemgroup_bind_arr[$cnt5]['defaultItem'] = isset($data['defaultItem_' . $sub_unit . '_' . $val5])? $ObjCommonDao->getprimarykey('cfmenu_items', $data['defaultItem_' . $sub_unit . '_' . $val5], 'itemunkid'):'';

                            //Bind Itemunit

                            $item_detail = isset($data['item_detail_' . $sub_unit. '_' .$val5]) ? $data['item_detail_' . $sub_unit. '_' .$val5] : '';
                            if ($item_detail != '') {
                                $item_detail_arr = explode(',', $item_detail);

                                $item_bind_arr = array();

                                $cnt = 0;
                                foreach ($item_detail_arr AS $val) {

                                    $upkey2 = $ObjCommonDao->getprimarykey('cfmenu_items', $val, 'itemunkid');
                                    $item_bind_arr[$cnt]['itemid'] = $upkey2;

                                    if($itemgroup_bind_arr[$cnt5]['defaultItem'] == $item_bind_arr[$cnt]['itemid'])
                                    {
                                        $item_bind_arr[$cnt]['extraCharge'] =0;
                                    }
                                    else
                                    {
                                        $item_bind_arr[$cnt]['extraCharge'] = isset($data['extraCharge_' . $sub_unit . '_' . $val5 . '_' . $val])?$data['extraCharge_' . $sub_unit . '_' . $val5 . '_' . $val]:0;

                                    }

                                        $itemunit_detail = isset($data['oldunit_item_' . $sub_unit . '_' . $val5 . '_' . $val]) ? $data['oldunit_item_' . $sub_unit . '_' .$val5 . '_' . $val] : '';

                                    if ($itemunit_detail != '') {
                                        $itemunit_detail_arr = explode(',', $itemunit_detail);

                                        $cnt2 = 0;

                                        $itemunit_bind_arr = array();

                                        foreach ($itemunit_detail_arr AS $val1) {
                                            $upkey3 = $ObjCommonDao->getprimaryBycompany('cfmenu_itemunit', $val1, 'unitunkid');
                                            $itemunit_bind_arr[$cnt2]['itemunit'] = $upkey3;

                                            if (isset($data['itemqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1]) && $data['itemqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1] == ""  && $data['itemqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1] == 0)
                                                $flag4 = 'false';
                                            else
                                                $itemunit_bind_arr[$cnt2]['itemqty'] = isset($data['itemqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1])?$data['itemqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1]:'';



                                            $mod_str = isset($data['allmodifier_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1])?$data['allmodifier_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1]:'';


                                            if($mod_str != '')
                                            {
                                                $mod_detail = explode(',', $mod_str);

                                                $cnt3 = 0;

                                                $mod_arr = array();


                                                foreach ($mod_detail AS $val2) {

                                                    $mod_arr[$cnt3]['modid'] = $val2;

                                                    if (isset($data['sub_modifieritem_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2]) && $data['sub_modifieritem_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2] != "" && $data['sub_modifieritem_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2] != 0){

                                                        $moditem_detail = $data['sub_modifieritem_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2];

                                                        $bind_sub_modifier_arr = explode(',', $moditem_detail);

                                                        $cnt4 = 0;

                                                        $moditem_arr = array();

                                                        foreach ($bind_sub_modifier_arr AS $val3) {

                                                            $moditem_arr[$cnt4]['moditemid'] = $val3;

                                                            if (isset($data['moitemunit_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val3]) && $data['moitemunit_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val3] == 0 && $data['moitemunit_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val3] == '')
                                                                $flag4 = 'false';
                                                            else
                                                                $moditem_arr[$cnt4]['moitemunit'] = isset($data['moitemunit_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val3])?$data['moitemunit_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val3]:'';

                                                            if (isset($data['moitemqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val3]) && $data['moitemqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val3] == "" && $data['moitemqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val3] == 0)
                                                                $flag4 = 'false';
                                                            else
                                                                $moditem_arr[$cnt4]['moitemqty'] = isset($data['moitemqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val3])?$data['moitemqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val3]:'';

                                                            $cnt4++;
                                                        }

                                                        $mod_arr[$cnt3]['modifieritem'] = $moditem_arr;

                                                    } else {

                                                        if (isset($data['mounit_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2]) && $data['mounit_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2] == "" && $data['mounit_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2] == 0)
                                                            $flag4 = 'false';
                                                        else
                                                            $mod_arr[$cnt3]['mounit'] = isset($data['mounit_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2])?$data['mounit_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2]:'';


                                                        if (isset($data['moqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2])  && $data['moqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2] == "" && $data['moqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2] == 0)
                                                            $flag4 = 'false';
                                                        else
                                                            $mod_arr[$cnt3]['moqty'] = isset($data['moqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2])?$data['moqty_' . $sub_unit . '_' . $val5 . '_' . $val . '_' . $val1 . '_' . $val2]:'';

                                                    }

                                                    $cnt3++;
                                                }

                                                $itemunit_bind_arr[$cnt2]['modifier'] = $mod_arr;
                                            }
                                            $cnt2++;
                                        }

                                        $item_bind_arr[$cnt]['itemunitsdetail'] = $itemunit_bind_arr;

                                    } else {
                                        $flag4 = 'false';
                                    }

                                    $cnt++;
                                }

                                $itemgroup_bind_arr[$cnt5]['items'] = $item_bind_arr;


                            } else {
                                $flag4 = 'false';
                            }

                            $cnt5++;
                        }
                        $unit_arr[$upkey]['itemGroups'] = $itemgroup_bind_arr;
                    }

                }
            } else {
                $flag4 = 'false';
            }

            if ($flag1 == 'true' && $flag2 == 'true' && $flag4 == 'true') {
//                $bucket = CONFIG_BUCKET_NAME;
//                $directoryname = 'pure-ipos-combo-item.images';
                $flag_file = isset($_FILES['upload_img']) ? $_FILES['upload_img'] : "";
                $img_flag = "";
                if ($flag_file['tmp_name'] != "") {
                    $directoryname = dirname(__FILE__).'/imageconfiguration/';
                    if(!is_dir($directoryname)){
                        //Directory does not exist, so lets create it.
                        mkdir($directoryname, 0777, true);
                    }
                    $temporary = explode(".", $flag_file["name"]);
                    $file_extension = end($temporary);
                    $flag_name = time() . '-' . $flag_file["name"];
                    $flag_dirname = dirname(__FILE__) . '/imageconfiguration/' . $flag_name;

                    $flag_db = "";
//                    $flag_name = time() . '-' . $flag_file["name"];
                    if ((($flag_file["type"] == "image/png") || ($flag_file["type"] == "image/jpg") || ($flag_file["type"] == "image/jpeg"))
                        && ($flag_file["size"] < 24800000)
                    ) {
                        move_uploaded_file($flag_file["tmp_name"], $flag_dirname);
                        $flag_db = $flag_name;
//                        $arr_file = array();
//                        $arr_file[0]['name'] = $directoryname . '/' . $flag_name;
//                        $arr_file[0]['tmp_name'] = $flag_file['tmp_name'];
//                        $arr_file[0]['type'] = $flag_file["type"];
//
//                        $ObjFile = new s3fileUpload();
//                        $ObjFile->createBucket($bucket);
//                        $resFile = $ObjFile->uploadFiles($arr_file, $bucket);
//                        if ($resFile != '' && $resFile != 0) {
//                            $img_flag = urldecode($resFile[0]);
//                        } else {
//                            $img_flag = '';
//                        }
                    }
                    $img_flag = (file_exists("imageconfiguration/$flag_db")) ? $flag_db : "";
                }

                if ($img_flag != "") {
                    $imagename = CONFIG_COMMON_URL . 'imageconfiguration/' . $data['imagephoto'];
                    if ($imagename != "") {
//                        $rmv_file = array();
//                        $rmv_file[] = $imagename;
//                        $ObjFile = new s3fileUpload();
//                        $ObjFile->deleteFiles($rmv_file, $bucket);
                        $path = CONFIG_COMMON_URL.'imageconfiguration/'.$imagename;
                        if (file_exists($path)) {
                            unlink($path);
                        }
                    }
                    $res = CONFIG_COMMON_URL . 'imageconfiguration/' . $img_flag; //add
                } else {
                    $res = $data['imagephoto'];
                }

                $req_data = array(
                    "name" => $data['comboname'],
                    "sku" => $data['txtsku'],
                    "longdesc" => $data['txtlongdescription'],
                    "image" => $res,
                    "serve_unit" => $data['serve_unit'],
                    "id" => $data['id'],
                    "rdo_status" => $data['rdo_status'],
                    "bind_unit" => $unit_arr,
                    "bind_tax" => isset($data['tax']) ? $data['tax'] : [],
                    "rates" => isset($rates) ? $rates : '',
                    "module" => $this->module
                );




//            $flag = \util\validate::check_notnull($data, array('comboname', 'amount'));
//            $item = array();
//            if ($flag == 'true') {
//                if (isset($data['categoryunkid'])) {
//
//                    for ($i = 0; $i < count($data['categoryunkid']); $i++) {
//                        $categoryid = $data['categoryunkid'][$i];
//                        if (isset($data['items_' . $categoryid]) && count($data['items_' . $categoryid]) > 0) {
//                            $cat_arr = explode(',', $data['items_' . $categoryid]);
//                            for ($j = 0; $j < count($cat_arr); $j++) {
//                                $item[] = $cat_arr[$j];
//                            }
//                        }
//                    }
//                }
//                $reqarr = array(
//                    "comboname" => $data['comboname'],
//                    "amount" => $data['amount'],
//                    "categoryunkid" => $data['categoryunkid'],
//                    "itemdata" => $item,
//                    "rdo_status" => $data['rdo_status'],
//                    "id" => $data['id'],
//                    "module" => $this->module
//                );
//                $ObjComboProductDao = new \database\combo_productdao();
//                $data = $ObjComboProductDao->addComboProduct($reqarr);
//                return $data;
//            } else
//                return json_encode(array('Success' => 'False', 'Message' => 'Some field is missing'));

                $ObjComboProductDao = new \database\combo_productdao();
                $data = $ObjComboProductDao->addComboProduct($req_data, $languageArr, $defaultlanguageArr);
                return $data;
            }

			else{
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
               }
        }
        catch (Exception $e) {
            $this->log->logIt($this->module . ' - addeditfrm - ' . $e);
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');
            global $twig;
            $limit = 50;
            $offset = 0;
            $name = "";

            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['nm']) && $data['nm'] != "")
                $name = $data['nm'];

            $ObjUserDao = new \database\combo_productdao();
            $data = $ObjUserDao->comboproductlist($limit, $offset, $name);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function getComboRec($data)
    {
        try {
            $this->log->logIt($this->module . " - getComboRec");
            $ObjComboDao = new \database\combo_productdao();
            $data = $ObjComboDao->getComboRec($data);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getComboRec - " . $e);
            return false;
        }
    }

    public function getComboProducts()
    {
        $this->log->logIt($this->module . " - getComboProducts");
        $ObjComboDao = new \database\combo_productdao();
        $res = $ObjComboDao->comboproductlist('', '', '', 1);
        $result = json_decode($res, 1);
        $list = $result[0]['data'];
        return json_encode(array('Success' => 'True', 'Data' => $list));
    }

    public function getItemlist()
    {
        try {
            $this->log->logIt($this->module . ' - getItemlist');
            $ObjComboDao = new \database\combo_productdao();
            $data = $ObjComboDao->getItemlist();

            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getItemlist - ' . $e);
        }
    }

    public function getItemModifier($data)
    {
        try {
            $this->log->logIt($this->module . ' - getItemModifier');
            $ObjComboDao = new \database\combo_productdao();
            $data = $ObjComboDao->getItemModifier($data);

            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getItemModifier - ' . $e);
        }
    }

    public function addZomatoCombosetting($data)
    {
        try {
            $this->log->logIt($this->module . ' - addZomatoCombosetting');

            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $OBJCOMMONDAO = new \database\commondao();
            $daytimeArray = array();
            $dayArray = array();

            $Zomatosetting = $OBJCOMMONDAO->getZomatosetting();

            $flag = false;

            if($Zomatosetting == 1) {

                $flag = \util\validate::check_notnull($data, array('zomato_itemrate'));

                $timings = [];

                    $startDate  = isset($data['startDate']) && $data['startDate'] !=''?$data['startDate']:'';
                    $endDate  = isset($data['endDate']) && $data['endDate'] !=''?$data['endDate']:'';

                    if($startDate != '' && $endDate != ''){
                        if(strtotime($endDate) < strtotime($startDate)){
                            return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG40));
                        }else{
                            $timings['startDate'] = $startDate;
                            $timings['endDate'] = $endDate;
                        }
                    }else{
                        $timings['startDate']  = $timings['endDate'] = '';
                    }

                    $timelist = [1,2,3];
                    for ($i=0;$i<count($timelist);$i++) {
                        $day = (string)$timelist[$i][0];

                        $fromtime1 = isset($data['time' . $timelist[$i].'From'])?$data['time' . $timelist[$i].'From']:'';
                        $totime1 = isset($data['time' . $timelist[$i].'To'])?$data['time' . $timelist[$i].'To']:'';

                        if($fromtime1 != '' && $totime1 != ''){
                            $fromtime = strtotime($fromtime1);
                            $totime = strtotime($totime1);

                            if($fromtime>=$totime)                    {
                                return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG29));
                            }else{
                                $timings['time' . $timelist[$i].'From'] = $data['time' . $timelist[$i].'From'];
                                $timings['time' . $timelist[$i].'To'] = $data['time' . $timelist[$i].'To'];
                            }
                        }else{
                            $timings['time' . $timelist[$i].'From']  = $timings['time' . $timelist[$i].'To'] = '';
                        }

                    }

                    $daylist = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
                    $cnt=0;

                    for ($i=0;$i<count($daylist);$i++) {
                        $day = 'day'.$daylist[$i];
                        if(isset($data[$day])){
                            $true = 'true';
                            $cnt++;
                        }else{
                            $true = 'false';
                        }
                        $timings[$day] = $true;
                    }
                    $timings['service'] = 'delivery';
                    if($cnt == 0){
                        return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG41));
                    }


                if ($flag == 'true') {

                    $reqarr = array(
                        "zid"=>$data['zid'],
                        "merge_combo_with_menu"=>isset($data['merge_combo_with_menu'])?$data['merge_combo_with_menu']:0,
                        "timings"=>isset($timings)?htmlspecialchars(json_encode($timings)):"",
                        "zomato_itemrate"=>isset($data['zomato_itemrate'])?$data['zomato_itemrate']:0,
                        "module" => $this->module
                    );

                    $this->loadLang();

                    $ObjMenuDao = new \database\combo_productdao();
                    $data = $ObjMenuDao->addZomatoCombosetting($reqarr,$languageArr,$defaultlanguageArr);
                    return $data;
                }else{
                    return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
                }
            }else
                {
                    return json_encode(array('Success'=>'False','Message'=>$languageArr->LANG65));

                }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addZomatoCombosetting - ' . $e);
        }
    }

    public function getComboZomatoSetting()
    {
        try
        {
            $this->log->logIt($this->module." - getComboZomatoSetting");
            $ObjMenuDao = new \database\combo_productdao();
            $data = $ObjMenuDao->getComboZomatoSetting();
            return $data;
        }catch(Exception $e){
            $this->log->logIt($this->module." - getComboZomatoSetting - ".$e);
            return false;
        }
    }

}

?>