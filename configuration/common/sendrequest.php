<?php
namespace common;
require("./config/config.php");

class sendrequest
{
    private $module = "SendRequest";
    private $service;
    private $opcode;
    private $parameter=array();
    private $log;
    
    function __construct()
    {
        try
        {
            $this->log = new \util\logger();
            $this->parameter = array();
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
    
    public function addService($service)
    {
        try
        {
            $this->service = $service;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - addService - ".$e);
        }
    }
    
    public function addOpcode($opcode)
    {
        try
        {
            $this->opcode = $opcode;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - addService - ".$e);
        }
    }
    
    public function addParameter($param,$value)
    {
        try
        {
            $this->parameter[$param] =$value;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - addParameter - ".$e);
        }
    }
    public function Request()
    {
        try
        {
            $this->log->logIt($this->module." -Send Request - ".$this->service." - ".$this->opcode);
            global $commonurl;
            $url_server = $commonurl;
            $url = $url_server."/service/servicecontroller.php";
            
            if($_SESSION[$_SESSION['prefix']]['enc_uname']!="" && $_SESSION[$_SESSION['prefix']]['enc_pwd']!="" && $_SESSION[$_SESSION['prefix']]['userid']!="" && $_SESSION[$_SESSION['prefix']]['dbn']!="" && $_SESSION[$_SESSION['prefix']]['companyid']!="")
            {
                $arr = array(
                    "enc_uid"=>$_SESSION[$_SESSION['prefix']]['userid'],
                    "enc_uname"=>$_SESSION[$_SESSION['prefix']]['enc_uname'],
                    "dbn"=>$_SESSION[$_SESSION['prefix']]['dbn'],
                    "cid"=>$_SESSION[$_SESSION['prefix']]['companyid'],
                    "service"=>$this->service,
                    "opcode"=>$this->opcode,
                );
                
                if(count($this->parameter)>0)
                {
                    foreach($this->parameter AS $key=>$value){
                        $arr[$key]= $value;
                    }
                }
               
                $ch=curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);									
                curl_setopt($ch, CURLOPT_TIMEOUT, 180);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);	
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arr));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                    'Content-Type: application/json',                                                                                
                    'Content-Length: ' . strlen(json_encode($arr)))                                                                       
                ); 
                       
                $res=curl_exec($ch);
                curl_close($ch);
                $data = trim($res);
                
                $this->parameter = array();
                $this->service = "";
                $this->opcode = "";
                return $data;
            }
            else
                header("location: ".$url_server."cloud/dashboard");
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - Request - ".$e);
        }
    }
}
?>