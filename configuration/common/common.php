<?php
require_once(dirname(__FILE__).'/../common/TwigConfig.php');
require_once(dirname(__FILE__).'/../config/config.php');
require_once(dirname(__FILE__).'/../common/dao.php');
require_once(dirname(__FILE__).'/../config/dbconnect.php');
$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
spl_autoload_register(function ($className) {
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('', DIRECTORY_SEPARATOR, $className) . '.php';
    require_once dirname(__DIR__).'/'.$fileName;
});

if(isset($_COOKIE['POS_ATKN_ID']) && isset($_COOKIE['POS_CNF_UN']))
{
    $ObjConn = new dbconnect();
    $ObjConn->config_connect();
    $objFunctions = new \common\functions();
    $rec = $objFunctions->checkAuth($_COOKIE['POS_ATKN_ID'],$_COOKIE['POS_CNF_UN']);
	
    
    if(isset($rec) && $rec!='' && $rec!=0)
    {
        require_once(dirname(__FILE__).'/../config/constant.php');
		global $twig;
		$twig->addGlobal('CONFIG_OEM_NAME', CONFIG_OEM_NAME);
		$twig->addGlobal('CONFIG_OEM_TITLE', CONFIG_OEM_TITLE);
		$twig->addGlobal('CONFIG_OEM_ID', CONFIG_OEM_ID);
		$twig->addGlobal('CONFIG_OEM_LOGO', CONFIG_OEM_LOGO);
    }
    else if($action=='controller'){
        echo json_encode(array('Success'=>'Expired'));
        exit(0);
    }
    else{
        header('location: '.CONFIG_COMMON_URL.'index.php');
        exit(0);
    }
}
else{
    if($action=='controller'){
        echo json_encode(array('Success'=>'Expired'));
        exit(0);
    }else{
        header("Location:".CONFIG_COMMON_URL."index.php");
        exit(0);
    }
}
?>