<?php
namespace common;
class functions
{
    private $log;
    private $module='functions';
    public $encdec;
    
    function __construct()
    {
        try
        {
            $this->log = new \util\logger();
            $this->encdec = new \common\encdec();
            $this->redis = new \Redis();
            $this->redis->connect(CONFIG_REDIS_HOST, CONFIG_REDIS_PORT);
            $this->redis->select(15);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }

    function addAuthToken($data)
    {
        $this->redis->hmset("auth_token:".$data['access_token'],[
                "access_token" => $data['access_token'],
                "dbname" => $data['dbname'],
                "bucket_name" => $data['bucket_name'],
                "translate_languages" => $data['translate_languages'],
                "username" => $data['username'],
                "userid" => $data['userid'],
                "user_type" => $data['user_type'],
                "sysdefined" => $data['sysdefined'],
                "companyid" => $data['companyid'],
                "locationid" => $data['locationid'],
                "storeid" => $data['storeid'],
                "login_type"=>$data['login_type'],
                "lang_type" => $data['lang_type'],
                "lang_code" => $data['lang_code'],
                "locationname" => $data['locationname'],
                "domain" => 1,
                "tabinsta" => $data['tabinsta'],
				'oem_name' => $data['oem_name'],
				'oem_logo' => $data['oem_logo'],
				'oem_title' => $data['oem_title'],
				'oem_id' => $data['oem_id'],
                "pms" => $data['pms'],
                "store" => $data['store'],
                "banquet" => $data['banquet'],
                "groupid" => $data['groupid'],
                "roleid" => $data['roleid'],
            ]
        );

        $this->redis->expire("auth_token:".$data['access_token'],1800);
        $res_token = $this->redis->hgetall("auth_token:".$data['access_token']);
        return $res_token;
    }
    function checkAuth($AuthToken,$Enc_Un)
    {
        try
        {
            require_once(dirname(__FILE__).'/../config/dbconnect.php');
            require_once(dirname(__FILE__).'/dao.php');
            $this->log->logIt($this->module." checkAuth");
            $ObjEncDec = new \common\encdec();
            $un = $ObjEncDec->openssl('decrypt',$Enc_Un);
            $check_token = $this->redis->exists("auth_token:".$AuthToken);
            
            if($check_token==1){
                $res_token = $this->redis->hgetall("auth_token:".$AuthToken);
                if(isset($res_token) && isset($res_token['companyid']) && $res_token['companyid']!='' && $res_token!='' && $res_token['username']==$un && $res_token['access_token']==$AuthToken){
                    $this->redis->expire("auth_token:".$AuthToken,1800);
                    $dbconnect  = new \dbconnect();
                    $dbconnect->config_connect();
                    $dao = new \dao();
                    $dao->initCommand("SELECT * FROM syscompany WHERE syscompanyid = :syscompanyid AND isactive = :isactive");
                    $dao->addParameter(':syscompanyid',$res_token['companyid']);
                    $dao->addParameter(':isactive',1);
                    $getOEM = $dao->executeRow();
                    if($getOEM)
                    {
                        return $res_token;
                    }
                        return 0;

                }else{
                    return 0;
                }
            }else{
                return 0;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - setsession -".$e);
            return json_encode(array("Success"=>"False"));
        }
    }
    function checkToken($AuthToken,$un)
    {
        try
        {
            require_once(dirname(__FILE__).'/../config/dbconnect.php');
            require_once(dirname(__FILE__).'/dao.php');
            $this->log->logIt('checkToken');
            $check_token = $this->redis->exists("auth_token:".$AuthToken);
            if($check_token==1){
                $res_token = $this->redis->hgetall("auth_token:".$AuthToken);
                if($res_token!='' && $res_token['username']==$un && $res_token['access_token']==$AuthToken){
                    $dbconnect  = new \dbconnect();
                    $dbconnect->config_connect();
                    $dao = new \dao();
                    $dao->initCommand("SELECT * FROM ".CONFIG_MYSQL_DBNAME.".syscompany WHERE syscompanyid = :syscompanyid AND isactive = :isactive");
                    $dao->addParameter(':syscompanyid',$res_token['companyid']);
                    $dao->addParameter(':isactive',1);
                    $getOEM = $dao->executeRow();
                    if($getOEM)
                    {
                        return $res_token;
                    }
                        return 0;
                }else{
                    return 0;
                }
            }else{
                return 0;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - setsession -".$e);
            return json_encode(array("Success"=>"False"));
        }
    }
    function checkAuth_service($AuthToken,$Enc_Un)
    {
        try
        {
            $arr = array(
                "service"=>"login",
                "opcode"=>"checktoken",
                "un"=>$Enc_Un,
                "token"=>$AuthToken
            );
            $arr_str = json_encode($arr);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, CONFIG_SERVICEURL);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,urlencode($arr_str));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $res=curl_exec($ch);
            curl_close($ch);
            $data = trim($res);
            $arr = json_decode($data,true);
            if($arr['Success']=='True' && $arr['cnt']>0){
                $rec = $arr['Data'];
                return $arr;
            }else{
                header('location: '.CONFIG_COMMON_URL.'logout.php');
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - setsession -".$e);
            return json_encode(array("Success"=>"False"));
        }
    }
    function removeToken($AuthToken,$Enc_Un)
    {
        try
        {
            $this->log->logIt($this->module);
            $ObjEncDec = new \common\encdec();
            $un = $ObjEncDec->openssl('decrypt',$Enc_Un);
            $check_token = $this->redis->exists("auth_token:".$AuthToken);
            if($check_token==1){
                $this->redis->del("auth_token:".$AuthToken);
            }else{
                return 0;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - setsession -".$e);
            return json_encode(array("Success"=>"False"));
        }
    }
    public function updateAuthToken($oid,$type)
    {
        try
        {
            $this->log->logIt($this->module.' - updateAuthToken');
            $dao = new \dao();
            $cid = CONFIG_CID;
            if($type==1){
                $lid = $oid;
                $sid = 0;
                $strSql = "SELECT USR.*,IFNULL(SC.translate_languages,'') as translate_languages,SC.databasename,SC.bucket_name,SL.locationname as lcName,SC.is_oem FROM sysuser AS USR
                           INNER JOIN syscompany AS SC ON USR.companyid=SC.syscompanyid AND SC.isactive=1 
                           INNER JOIN syslocation AS SL ON SL.companyid=SC.syscompanyid AND SL.is_active=1 AND SL.is_deleted=0
                           WHERE USR.companyid=:companyid AND USR.userunkid=:userunkid 
                           AND USR.isactive=1 AND USR.is_deleted=0 AND FIND_IN_SET($lid,USR.locationid) AND SL.syslocationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':userunkid',CONFIG_UID);
                $dao->addParameter(':companyid',$cid);
                $dao->addParameter(':locationid',$lid);
                $rec = $dao->executeQuery();
            }else{
                $lid = 0;
                $sid = $oid;
                $strSql1 = "SELECT databasename FROM syscompany WHERE syscompanyid=:syscompanyid";
                $dao->initCommand($strSql1);
                $dao->addParameter(':syscompanyid',$cid);
                $dbname = $dao->executeRow();
                $databasename=$dbname['databasename'];

                $strSql = "SELECT USR.*,IFNULL(SC.translate_languages,'') as translate_languages,SC.databasename,SC.bucket_name,SS.storename as lcName,SC.is_oem FROM sysuser AS USR
                           INNER JOIN syscompany AS SC ON USR.companyid=SC.syscompanyid AND SC.isactive=1 
                           INNER JOIN ".$databasename.".cfstoremaster AS SS ON SS.companyid=SC.syscompanyid AND SS.is_active=1 AND SS.is_deleted=0
                           WHERE USR.companyid=:companyid AND USR.userunkid=:userunkid 
                           AND USR.isactive=1 AND USR.is_deleted=0 AND FIND_IN_SET($sid,USR.storeid) AND SS.storeunkid=:storeid";
                $dao->initCommand($strSql);
                $dao->addParameter(':userunkid',CONFIG_UID);
                $dao->addParameter(':companyid',$cid);
                $dao->addParameter(':storeid',$sid);
                $rec = $dao->executeQuery();
            }
            if(count($rec)>0 && isset($rec[0]['userunkid']))
            {
                $db_name = $rec[0]['databasename'];
                /*Integration settings */
                    $strSql2 = "SELECT store_available FROM ".$db_name.".cfcompany WHERE companyid=:companyid ";
                    $dao->initCommand($strSql2);
                    $dao->addParameter(':companyid',$cid);
                    $apiSettings = $dao->executeRow();

                /*if($apiSettings && $lid){

                    $str = " SELECT pms_integration FROM ".$db_name.".cflocation WHERE locationid=:locationid ";
                    $dao->initCommand($str);
                    $dao->addParameter(':locationid', $lid);
                    $apilocationsettings = $dao->executeRow();

                    if($apilocationsettings){
                        $apiSettings['pms_integration']=$apilocationsettings['pms_integration'];
                    }
                }*/

                $strSql3 = "SELECT meta_value FROM syscompany_meta WHERE syscompanyid=:companyid AND meta_key=:meta_key";
                $dao->initCommand($strSql3);
                $dao->addParameter(':companyid',$cid);
                $dao->addParameter(':meta_key','is_banquet_available');
                $banquet = $dao->executeRow();

                /*Integration settings*/
                /*OEM details*/
                    $oemDetails  = array('name' => '','logo' => '','title' =>'','id' => '');
                    if($rec[0]['is_oem'] > 0)
                    {
                        $dao->initCommand("SELECT * FROM oem_settings WHERE id = :id");
                        $dao->addParameter(":id",$rec[0]['is_oem']);
                        $oemDetails = $dao->executeRow();
                    }
                /*OEM details*/
                /*Privileges*/

                if($type==1){
                    $systemdefined = $rec[0]['sysdefined'];
                    if ($systemdefined != 1) {
                        $strSql = "SELECT GROUP_CONCAT(DISTINCT(CRP.lnkprivilegegroupid),'') as lnkprivilegegroupid 
                              FROM " . $db_name . ".cfroleprivileges as CRP 
                              INNER JOIN sysprivilegegroup as SPG ON CRP.lnkprivilegegroupid=SPG.privilegegroupunkid 
                              WHERE  CRP.companyid=:companyid AND CRP.lnkroleid=:lnkroleid AND type=3";

                        $dao->initCommand($strSql);
                        $dao->addParameter(':lnkroleid', $rec[0]['roleunkid']);
                        $dao->addParameter(':companyid', $cid);
                        $groupid = $dao->executeRow();
                    }
                    else{

                        $strSql = "SELECT  GROUP_CONCAT(DISTINCT(privilegegroupunkid),'') as lnkprivilegegroupid
                              FROM sysprivilegegroup WHERE type=3";
                        $dao->initCommand($strSql);
                        $groupid = $dao->executeRow();

                    }
                    $group_privilege = $groupid['lnkprivilegegroupid'];
                    
                }else{
                    $systemdefined = $rec[0]['sysdefined'];
                    if ($systemdefined != 1) {
                        $strSql = "SELECT GROUP_CONCAT(DISTINCT(CRP.lnkprivilegegroupid),'') as lnkprivilegegroupid 
                              FROM " . $db_name . ".cfroleprivileges as CRP 
                              INNER JOIN sysprivilegegroup as SPG ON CRP.lnkprivilegegroupid=SPG.privilegegroupunkid 
                              WHERE  CRP.companyid=:companyid AND CRP.lnkroleid=:lnkroleid AND type=2";

                        $dao->initCommand($strSql);
                        $dao->addParameter(':lnkroleid', $rec[0]['roleunkid']);
                        $dao->addParameter(':companyid', $cid);
                        $groupid = $dao->executeRow();
                    }
                    else{

                        $strSql = "SELECT  GROUP_CONCAT(DISTINCT(privilegegroupunkid),'') as lnkprivilegegroupid
                              FROM sysprivilegegroup WHERE type=2";
                        $dao->initCommand($strSql);
                        $groupid = $dao->executeRow();

                    }
                    $group_privilege = $groupid['lnkprivilegegroupid'];
                }
                /*Privileges*/
                $ObjEncDec = new \common\encdec();
                $un = $ObjEncDec->openssl('decrypt',$_COOKIE['POS_CNF_UN']);
                $token =  $_COOKIE['POS_ATKN_ID'];
                //Remove Token
                    $this->removeToken($token,$_COOKIE['POS_CNF_UN']);
                /* Add New Auth Token to redis */
                    $new_token = \util\util::getauthtoken(CONFIG_CID);
                    $arr_token = array(
                        "access_token" => $new_token,
                        "dbname" => $rec[0]['databasename'],
                        "bucket_name" => $rec[0]['bucket_name'],
                        "translate_languages" => $rec[0]['translate_languages'],
                        "username" => $rec[0]['username'],
                        "userid" => $rec[0]['userunkid'],
                        "user_type" => $rec[0]['user_type'],
                        "sysdefined" => $rec[0]['sysdefined'],
                        "companyid" => $cid,
                        "locationid" => $lid,
                        "storeid" => $sid,
                        "login_type"=>$type,
                        "locationname" => $rec[0]['lcName'],
                        "lang_type" => $rec[0]['lang_type'],
                        "lang_code" => $rec[0]['lang_code'],
                        //"tabinsta" => $apiSettings['tabinsta_integration'],
                        'oem_name' => $oemDetails['name'],
                        'oem_logo' => $oemDetails['logo'],
                        'oem_title' => $oemDetails['title'],
                        'oem_id' => $oemDetails['id'],
                       // "pms" => $apiSettings['pms_integration'],
                        "store" => $apiSettings['store_available'],
                        "banquet" => $banquet['meta_value'],
                        "groupid" => $group_privilege,
                        "roleid" => $rec[0]['roleunkid'],
                    );
                    $res_token = $this->addAuthToken($arr_token);
                /* Add Auth Token to redis */
                $uname = $this->encdec->openssl('encrypt',$rec[0]['username']);
                $domain = ($_SERVER['HTTP_HOST'] != '192.168.0.42') ? $_SERVER['HTTP_HOST'] : false;
                setcookie('POS_ATKN_ID', $new_token, time() + (86400 * 30), '/',$domain,false);
                setcookie('POS_CNF_UN', $uname, time() + (86400 * 30), '/',$domain,false);
                return 1;
            }
            else{
                return 0;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - updateAuthToken - '.$e);
        }
    }
    function fn_flatten_GP_array(array $var, $prefix = FALSE)
    {
        $return = array();
        foreach ($var as $idx => $value) {
            if (is_scalar($value)) {
                if ($prefix) {
                    $return[$prefix . '[' . $idx . ']'] = $value;
                } else {
                    $return[$idx] = $value;
                }
            } else {
                if(is_array($value))
                {
                    $return = array_merge($return, fn_flatten_GP_array($value, $prefix ? $prefix . '[' . $idx . ']' : $idx));
                }else{
                    $return = array_merge($return, array($value));
                }
            }
        }
        return $return;
    }
    function checkModuleAccess($groupid,$module='')
    {
        try
        {
            $this->log->logIt($this->module. " checkModuleAccess");
            $ObjStaticArray = new \common\staticarray();
            $arr_privilege = explode(',',CONFIG_GID);
            if(CONFIG_LOGINTYPE==1){
                $arr_type = 'config_modules';
            }else{
                $arr_type = 'store_modules';
            }
            if(!in_array($module,$ObjStaticArray->modules_list[$arr_type])){
                header('location: '.CONFIG_COMMON_URL.'cloud/dashboard');
            }else{
                if(CONFIG_LOGINTYPE==1){
                    if(!in_array($groupid,$arr_privilege)){
                        header('location: '.CONFIG_COMMON_URL.'cloud/dashboard');
                    }
                }else{
                   if(!in_array($groupid,$arr_privilege)){
                        header('location: '.CONFIG_COMMON_URL.'cloud/dashboard');
                    }
                }
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - checkModuleAccess -".$e);
        }
    }
}
?>