<?php

class forwardindent
{
    public $module = 'forwardindent';
    public $log;
    private $language, $lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(31,$this->module);

            $ObjCommonDao = new \database\commondao();
            $privilegeList = $ObjCommonDao->getuserprivongroup(31);

            $ObjIdent = new \database\indentrequestdao();
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $Indentdata = $ObjIdent->indentlist(50,'0', '', '', '', '', '',json_decode($languageArr));
            $template = $twig->loadTemplate('forwardindent.html');

            $storeData = $ObjIdent->getallstorelist();
            $storelist = json_decode($storeData, 1);

            $currencysign =$ObjCommonDao->getCurrencySignCompany();
            $indentNo = $ObjCommonDao->getIndentDocumentNumbering('indent_no', '');

            $Objcategorydao = new \database\raw_categorydao();
            $catData = $Objcategorydao->getAllCategory();
            $catlist = json_decode($catData, 1);

            $jsdate = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $roundoff = \database\parameter::getParameter('digitafterdecimal');

            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $Indentdata;
            $senderarr['indentid'] = $indentNo;
            $senderarr['category'] = $catlist['Data'];
            $senderarr['storelist'] = $storelist['Data'];
            $senderarr['jsdateformat'] = $jsdate;
            $senderarr['roundoff'] = $roundoff;
            $senderarr['currencysign'] = $currencysign;
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);$senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $this->log->logIt($senderarr);
            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function addindentrequest($data)
    {
        try {
            $this->log->logIt($this->module . ' - addindentrequest');
            $reqarr = array(
                "indentrecords" => $data['indentrecords'],
                "totalamount" => $data['totalamount'],
                "indent_date" => $data['indate'],
                "premarks" => $data['premarks'],
                "plocationid" => $data['plocationid'],
                "id" => $data['id'],
                "module" => $this->module
            );
            $ObjUnitDao = new \database\indentrequestdao();
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);$senderarr['user_type'] = CONFIG_USR_TYPE;
            $data = $ObjUnitDao->addindentrequest($reqarr,$default_langlist);
            return $data;

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addindentrequest - ' . $e);
        }
    }

    public function getrawmaterialoncategory($data)
    {
        try {
            $this->log->logIt($this->module . ' - getrawmaterialoncategory');
            $ObjUnitDao = new \database\indentrequestdao();
            $data = $ObjUnitDao->getrawmaterialoncategory($data);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getrawmaterialoncategory - ' . $e);
        }
    }

    public function callindent($data)
    {
        try {
            $this->log->logIt($this->module . " - callindent");
            global $twig;
            $ObjIdent = new \database\indentrequestdao();
            $storeData = $ObjIdent->getallstorelist();
            $storelist = json_decode($storeData, 1);
            $ObjCommonDao = new \database\commondao();
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(31,$this->module);

             $privilegeList = $ObjCommonDao->getuserprivongroup(31);

            $Objcategorydao = new \database\raw_categorydao();
            $catData = $Objcategorydao->getAllCategory();
            $catlist = json_decode($catData, 1);
            $currencysign =$ObjCommonDao->getCurrencySignCompany();

            $indentNo = $ObjCommonDao->getIndentDocumentNumbering('indent_no', '');

            $jsdate = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $template = $twig->loadTemplate('indentrequest.html');
            $this->loadLang();

            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;
            $senderarr['id'] = $data['id'];
            $senderarr['currencysign'] = $currencysign;
            $senderarr['indentid'] = $indentNo;
            $senderarr['category'] = $catlist['Data'];
            $senderarr['storelist'] = $storelist['Data'];
            $langarr=html_entity_decode(json_encode($this->lang_arr));
            $senderarr['langlist'] = json_decode($langarr);
            $senderarr['jsdateformat'] = $jsdate;
            $senderarr['default_langlist'] = $this->default_lang_arr;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];

            echo html_entity_decode(json_encode(array("Success" => "True", "Data" => $template->render($senderarr))));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - callindent - " . $e);
            return false;
        }
    }

    public function getrelatedRecords($data)
    {
        try {
            $this->log->logIt($this->module . " - getrelatedRecords");
            global $twig;
            global $commonurl;
            $flag = 0;
            if (!isset($data) || !isset($data['id']) || !isset($data['module'])) {
                $flag == 1;
                $Obj = "";
            } else {
                $ObjrawDao = new \database\rawmaterialdao();
                $res = $ObjrawDao->getrelatedRecords($data);
                $bj = json_decode($res, 1);
                $Obj = $bj['Data'];
            }
            $template = $twig->loadTemplate('dependency.html');
            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['flag'] = $flag;
            $senderarr['rec'] = $Obj;
            $ret = array('data' => $template->render($senderarr));
            return html_entity_decode(json_encode($ret));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getrelatedRecords - " . $e);
            return false;
        }
    }

    public function getallRelatedRecords($data)
    {
        try {
            $this->log->logIt($this->module . " - getallRelatedRecords");
            global $twig;
            global $commonurl;
            $flag = 0;
            if (!isset($data) || !isset($data['id']) || !isset($data['module'])) {
                $flag == 1;
                $Obj = "";
            } else {
                $ObjrawDao = new \database\rawmaterialdao();
                $res = $ObjrawDao->getallRelatedRecords($data);
                $bj = json_decode($res, 1);
                $Obj = $bj['Data'];
            }
            $template = $twig->loadTemplate('alldependency.html');
            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['flag'] = $flag;
            $senderarr['rec'] = $Obj;
            $ret = array('data' => $template->render($senderarr));
            return html_entity_decode(json_encode($ret));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getallRelatedRecords - " . $e);
            return false;
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');

            $limit = 50;
            $offset = 0;
            $fromdate = "";
            $todate = "";
            $indent = "";
            $storeid = "";
            $statusid = "";

            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['fromdate']) && $data['fromdate'] != "")
                $fromdate = $data['fromdate'];
            if (isset($data['todate']) && $data['todate'] != "")
                $todate = $data['todate'];
            if (isset($data['indent']) && $data['indent'] != "")
                $indent = $data['indent'];
            if (isset($data['locationid']) && $data['locationid'] != "")
                $storeid = $data['locationid'];
            if (isset($data['statusid']) && $data['statusid'] != "")
                $statusid = $data['statusid'];

            $ObjUserDao = new \database\indentrequestdao();
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $data = $ObjUserDao->indentlist($limit, $offset, $fromdate, $todate, $indent, $storeid, $statusid,json_decode($languageArr));
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function getUnitRate($data)
    {
        try {
            $this->log->logIt($this->module . " - getUnitRate");
            $ObjUnitDao = new \database\indentrequestdao();
            $data = $ObjUnitDao->getUnitRate($data);

            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getUnitRate - " . $e);
            return false;
        }
    }

    public function removeindent($data)
    {
        try {
            $this->log->logIt($this->module . " - getUnitRate");
            $ObjUnitDao = new \database\indentrequestdao();
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);$senderarr['user_type'] = CONFIG_USR_TYPE;
            $data = $ObjUnitDao->removeindent($data,$default_langlist);

            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getUnitRate - " . $e);
            return false;
        }
    }

    public function getIndentRecordsonID($data)
    {
        try {
            $this->log->logIt($this->module . " - getIndentRecordsonID");
            $ObjUnitDao = new \database\indentrequestdao();
            $data = $ObjUnitDao->getIndentRecordsonID($data);

            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getIndentRecordsonID - " . $e);
            return false;
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$forwardindent;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
    public function ForwardIndentprintInvoice($data)
    {
        try
        {
            $this->log->logIt($this->module.' - ForwardIndentprintInvoice');
            $id = $data['id'];

            $ObjIndentDao = new \database\indentrequestdao();
            $headerData = $ObjIndentDao->getforwardindentdetail($id);
            if($headerData){
                $this->loadLang();
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
                $headerData['default_langlist'] = json_decode($defaultlanguageArr);
                $indentlanguage = new \util\language('AuthorizeReceivedindent');
                $llang_arr = \common\staticlang::$AuthorizeReceivedindent;
                $languageArr=html_entity_decode(json_encode($indentlanguage->loadlanguage($llang_arr)),ENT_QUOTES);
                $headerData['langlist'] = json_decode($languageArr);
            }
            $PrintReport = new \common\printreport();

            $PrintReport->addTemplate('forwardindentvoucher');
            $width = '793px';
            $height = '1122px';

            $PrintReport->addRecord($headerData);
            $data = $PrintReport->Request();
            return json_encode(array("Success"=>"True","Data"=>$data, "Height"=>$height, "Width"=>$width));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - ForwardIndentprintInvoice - '.$e);
        }
    }
}

?>