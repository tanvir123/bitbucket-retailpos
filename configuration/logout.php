<?php
require_once "config/config.php";
require_once "util/logger.php";
require_once "common/functions.php";
require_once "common/encdec.php";
$ObjFunctions = new \common\functions();
/*$getAuthToken = $ObjFunctions->checkAuth($_COOKIE['POS_ATKN_ID'],$_COOKIE['POS_CNF_UN']);
$oem_url = '';
if($getAuthToken)
{
	$oem_url = $getAuthToken['oem_name']!='' ? $getAuthToken['oem_name'].'/' : '';
}*/

if(isset($_COOKIE['POS_ATKN_ID']) && isset($_COOKIE['POS_CNF_UN'])){
    $rec = $ObjFunctions->removeToken($_COOKIE['POS_ATKN_ID'],$_COOKIE['POS_CNF_UN']);
}

$domain = ($_SERVER['HTTP_HOST'] != '192.168.0.42') ? $_SERVER['HTTP_HOST'] : false;
setcookie("POS_ATKN_ID","", time()-3600, "/",$domain,false);
setcookie("POS_CNF_UN","", time()-3600, "/",$domain,false);
header("location:".CONFIG_BASE_URL."configuration/index.php");
?>