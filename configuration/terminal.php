<?php

class terminal
{
    public $module = 'terminal';
    public $log;
    private $language, $lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(20,$this->module);
            $Objterminaldao = new \database\terminaldao();
            $data = $Objterminaldao->terminallist(50, '0', '');


            $template = $twig->loadTemplate('terminal.html');

            $this->loadLang();


            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;
            $senderarr['langlist'] = $this->lang_arr;

            $senderarr['default_langlist'] = $this->default_lang_arr;

            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public
    function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $arr = array(
                "LANG1" => "Terminal",
                "LANG2" => "Name",
                "LANG3" => "Device Type",
                "LANG4" => "KOT",
                "LANG5" => "Reciept",
                "LANG6" => "IP Address",
            );
            $this->lang_arr = $this->language->loadlanguage($arr);

            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }

    public function addeditfrm($data)
    {
        try {
            $this->log->logIt($this->module . ' - addeditfrm');

            $abc = $data['select_device'];

            if($abc[0]==$abc[1]){
                return json_encode(array('Success' => 'False', 'Message' => 'Select different terminal'));
            }

            $arr_check = array('terminalname', 'select_device');
            if (isset($data['terminalname'])) {
                $arr_check[] = "terminalname";
            }
            if (isset($data['select_device'])) {
                $arr_check[] = "select_device";
            }

            $flag1 = \util\validate::check_notnull($data, $arr_check);
            if ($flag1 == 'true') {
                $reqarr = array(
                    "terminalname" => $data['terminalname'],
                    "lnkdevicetype" => $data['select_device'],
                    "ip" => $data['ipadd'],
                    "rdo_status" => $data['rdo_status'],
                    "id" => $data['id'],
                    "module" => $this->module
                );
                $ObjDao = new \database\terminaldao();
                $data = $ObjDao->addterminal($reqarr);
                return $data;
            } else
                return json_encode(array('Success' => 'False', 'Message' => 'Some field is missing'));

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addeditfrm - ' . $e);
        }
    }

    public function editterminallist($data)
    {
        try {
            $this->log->logIt($this->module . " - editterminallist");
            $ObjDao = new \database\terminaldao();
            $data = $ObjDao->editterminallist($data);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - editterminallist - " . $e);
            return false;
        }
    }

    public
    function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');
            $limit = 50;
            $offset = 0;
            $name = "";
            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['nm']) && $data['nm'] != "")
                $name = $data['nm'];
            $ObjUserDao = new \database\terminaldao();
            $res = $ObjUserDao->terminallist($limit, $offset, $name);
            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

}

?>