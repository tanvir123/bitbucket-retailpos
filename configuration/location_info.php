<?php
require_once(dirname(__FILE__)."/common/s3fileUpload.php");
class location_info
{
    private $module='location_info';
    private $log;
    private $language,$lang_arr,$default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_locationinfo');
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(18,$this->module);
            $ObjDao = new \database\locationdao();
            $ObjcurrencyDao = new \database\currencylistdao();
            $data_arr = $ObjDao->loadLocationInfo();
            $currency_list = $ObjcurrencyDao->currencylist('','','');
            $currency_list = json_decode($currency_list,1);
            $ObjUserDao = new \database\commondao();
            $countrydata = $ObjUserDao->getCountryList();

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(18);

            $OBJAMENITIESDAO = new \database\amenitiesdao();
            $amenitiesList = $OBJAMENITIESDAO->getamenitiesRecord();
            $this->loadLang();
            $template = $twig->loadTemplate('location_info.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['datalist'] = $data_arr;
            $senderarr['currencylist'] =$currency_list[0]['data'] ;
            $senderarr['countrylist'] = $countrydata;
            $senderarr['amenitieslist'] = $amenitiesList;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo \util\util::convert_html_specials($template->render($senderarr));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    public function editLocationInfo($data)
    {
        try
        {
            $this->log->logIt($this->module.' - editLocationInfo');
            $flag1 = \util\validate::check_notnull($data,array(
                    'txtlocationname','txtaddress1','txtstate',
                    'txtcity','txtphoneno','txtrescontactno',
                    'txtemail','txtwebsite','currencylnkid'));
            $flag2 = \util\validate::check_combo($data,array('select_country'));
            $this->loadLang();
            if($flag1=='true' && $flag2=='true')
            {
//                $bucket=CONFIG_BUCKET_NAME;
//                $directoryname='pure-ipos-location.images';
                $flag_file = isset($_FILES['upload_img'])?$_FILES['upload_img']:"";
                $img_flag="";
                if($flag_file['tmp_name']!="")
                {
                    $directoryname = dirname(__FILE__).'/imageconfiguration/';
                    if(!is_dir($directoryname)){
                        //Directory does not exist, so lets create it.
                        mkdir($directoryname, 0777, true);
                    }
                    $temporary = explode(".", $flag_file["name"]);
                    $file_extension = end($temporary);
                    $flag_name = time() . '-' . $flag_file["name"];
                    $flag_dirname = dirname(__FILE__) . '/imageconfiguration/' . $flag_name;

                    $flag_db = "";
//                    $flag_name = time().'-'.$flag_file["name"];
                    if ((($flag_file["type"] == "image/png") || ($flag_file["type"] == "image/jpg") || ($flag_file["type"] == "image/jpeg"))
                        && ($flag_file["size"] < 24800000))
                    {
                        move_uploaded_file($flag_file["tmp_name"], $flag_dirname);
                        $flag_db = $flag_name;
//                        $arr_file = array();
//                        $arr_file[0]['name'] = $directoryname.'/'.$flag_name;
//                        $arr_file[0]['tmp_name'] = $flag_file['tmp_name'];
//                        $arr_file[0]['type'] = $flag_file["type"];
//
//                        $ObjFile = new s3fileUpload();
//                        $ObjFile->createBucket($bucket);
//                        $resFile = $ObjFile->uploadFiles($arr_file,$bucket);
//                        if($resFile!='' && $resFile!=0){
//                            $img_flag = urldecode($resFile[0]);
//                        }else{
//                            $img_flag = '';
//                        }
                    }
                    $img_flag = (file_exists("imageconfiguration/$flag_db")) ? $flag_db : "";
                }
                if($img_flag!=""){
                    $imagename = CONFIG_COMMON_URL . 'imageconfiguration/' . $data['imagephoto'];
                    if($imagename!="")
                    {
//                        $rmv_file = array();
//                        $rmv_file[] = $imagename;
//                        $ObjFile = new s3fileUpload();
//                        $ObjFile->deleteFiles($rmv_file,$bucket);
                        $path = CONFIG_COMMON_URL.'imageconfiguration/'.$imagename;
                        if (file_exists($path)) {
                            unlink($path);
                        }
                    }
                    $res = CONFIG_COMMON_URL . 'imageconfiguration/' . $img_flag; //add
                }else{
                    $res = $data['imagephoto'];
                }
                $data['image']=$res;


                $ObjLocationDao = new \database\locationdao();
                $rec = $ObjLocationDao->editLocationInformation($data);
                if($rec==1){
                    return json_encode(array('Success'=>'True','Message'=>'REC_UP_SUC'));
                }else{
                    return json_encode(array('Success'=>'False','Message'=>'INTERNAL_ERROR'));
                }
            }else{
                return json_encode(array('Success'=>'False','Message'=>'SOME_FIELD_MISSING'));
            }
        }catch(Exception $e){
            $this->log->logIt($this->module.' - editLocationInfo - '.$e);
        }
    }
    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_locationinfo;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }
    public function getAllLocation()
    {
        try
        {
            $this->log->logIt($this->module." - getAllLocation");
            $ObjUnitDao = new \database\locationdao();
            $data = $ObjUnitDao->getAllLocation();
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getAllLocation - ".$e);
            return false;
        }
    }

}
?>