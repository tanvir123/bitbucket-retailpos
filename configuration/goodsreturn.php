<?php
class goodsreturn
{
    public $module = 'goodsreturn';
    public $log;
    private $language, $lang_arr, $default_lang_arr;
    public $ObjPurchaseDao;
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->ObjGoodReturnDao = new \database\goodsreturndao();
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(58, $this->module);
            $ObjCommonDao = new \database\commondao();

            $privilegeList = $ObjCommonDao->getuserprivongroup(58);

            $arr_purchase_orders = $this->ObjGoodReturnDao->goodsReturnList(50,'0', '', '', '', '','');
            $template = $twig->loadTemplate('goodsreturn.html');

            $vendor_list = $this->ObjGoodReturnDao->getAllVendorList();

            $currency_sign =$ObjCommonDao->getCurrencySignCompany();
            $indentNo = $ObjCommonDao->getIndentDocumentNumbering('goods_return', '');
            
            $ObjCategoryDao = new \database\raw_categorydao();
            $catData = $ObjCategoryDao->getAllCategory();
            $cat_list = json_decode($catData, 1);
            
            $js_date = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $this->loadLang();
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['login_type'] = CONFIG_LOGINTYPE;
            $sender_arr['module'] = $this->module;
            $sender_arr['datalist'] = $arr_purchase_orders;
            $sender_arr['indentid'] = $indentNo;
            $sender_arr['category'] = $cat_list['Data'];
            $sender_arr['vendor_list'] = $vendor_list;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $sender_arr['langlist'] = json_decode($languageArr);
            $sender_arr['jsdateformat'] = $js_date;
            $sender_arr['roundoff'] = $round_off;
            $sender_arr['currencysign'] = $currency_sign;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $sender_arr['default_langlist'] = json_decode($defaultlanguageArr);
            $sender_arr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            $sender_arr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($sender_arr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');

            $limit = 50;
            $offset = 0;
            $from_date = "";
            $to_date = "";
            $order_no = "";
            $vendor_id = "";
            $voucherno = "";

            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['fromdate']) && $data['fromdate'] != "")
                $from_date = $data['fromdate'];
            if (isset($data['todate']) && $data['todate'] != "")
                $to_date = $data['todate'];
            if (isset($data['orderno']) && $data['orderno'] != "")
                $order_no = $data['orderno'];
            if (isset($data['voucherno']) && $data['voucherno'] != "")
                $voucherno = $data['voucherno'];
            if (isset($data['vendorid']) && $data['vendorid'] != "")
                $vendor_id = $data['vendorid'];

            $result =  $this->ObjGoodReturnDao->goodsReturnList($limit, $offset, $from_date, $to_date, $order_no, $vendor_id,$voucherno);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function loadGoodsReturn($data)
    {
        try {
            $this->log->logIt($this->module." - loadGoodsReturn");
            global $twig;
            $vendor_list = $this->ObjGoodReturnDao->getAllVendorList();

            $ObjCommonDao = new \database\commondao();
            $currency_sign =$ObjCommonDao->getCurrencySignCompany();
            $ObjCommonDao = new \database\commondao();

            $privilegeList = $ObjCommonDao->getuserprivongroup(57);
            if($data['id']=='' || $data['id'] ==0) {
                $grNo = $ObjCommonDao->getIndentDocumentNumbering('goods_return', '');
                $gr_vcNo = $ObjCommonDao->getIndentDocumentNumbering('gr_voucher_no', '');
            }
            else{
                $grNo='';
                $gr_vcNo = '';
            }
            $applicable_taxes = $this->ObjGoodReturnDao->getAppliedTax();

            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $js_date = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $template = $twig->loadTemplate('goodsreturnrequest.html');
            $this->loadLang();
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['store'] = CONFIG_IS_STORE;
            $sender_arr['module'] = $this->module;
            $sender_arr['datalist'] = $data;
            $sender_arr['id'] = $data['id'];
            $sender_arr['grn_id'] = $data['grn_id'];
            $sender_arr['currencysign'] = $currency_sign;
            $sender_arr['grno'] = $grNo;
            $sender_arr['gr_vcno'] = $gr_vcNo;
            $sender_arr['vendor_list'] = $vendor_list;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $sender_arr['langlist'] = json_decode($languageArr);
            $sender_arr['applicable_taxes'] = $applicable_taxes;
            $sender_arr['jsdateformat'] = $js_date;
            $sender_arr['roundoff'] = $round_off;
            $sender_arr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $sender_arr['default_langlist'] = json_decode($defaultlanguageArr);
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            echo json_encode(array("Success" => "True", "Data" => $template->render($sender_arr)));
        } catch (Exception $e) {
            $this->log->logIt($this->module." - loadGoodsReturn - ".$e);
            return false;
        }
    }
    public function addGoodsReturn($data)
    {
        try {
            $this->log->logIt($this->module.' - addGoodsReturn');
            $req_arr = array(
                "gr_items" => $data['gr_items'],
                "total_amount" => $data['total_amount'],
                "gr_date" => $data['gr_date'],
                "gr_sel_vendor" => $data['vendor_id'],
                "gr_remarks" => $data['gr_remarks'],
                "id" => $data['id'],
                "module" => $this->module
            );
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr=json_decode($defaultlanguageArr);
            $result = $this->ObjGoodReturnDao->addGR($req_arr,$defaultlanguageArr);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module.' - addGoodsReturn - '.$e);
        }
    }
    public function addGoodsReturnFromReceipt($data)
    {
        try {
            $this->log->logIt($this->module.' - addGoodsReturnFromReceipt');
            $flag = \util\validate::check_notnull($data, array('total_amount','gr_date','vendor_id','grn_id'));
            if ($flag == 'true') {
                $req_arr = array(
                    "gr_items" => $data['gr_items'],
                    "total_amount" => $data['total_amount'],
                    "gr_date" => $data['gr_date'],
                    "gr_sel_vendor" => $data['vendor_id'],
                    "gr_remarks" => $data['gr_remarks'],
                    "id" => $data['id'],
                    "grn_id" => $data['grn_id'],
                    "module" => $this->module
                );
                $this->loadLang();
                $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
                $languageArr = json_decode($languageArr);

                $result = $this->ObjGoodReturnDao->addGRFromGRN($req_arr,$languageArr);
                return $result;
            }else{
                return json_encode(array('Success' => 'False', 'Message' => 'Some field is missing'));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module.' - addGoodsReturnFromReceipt - '.$e);
        }
    }
    public function getGrDetail($data)
    {
        try {
            $this->log->logIt($this->module." - getGrDetail");
            $result = $this->ObjGoodReturnDao->getGrRecordsByID($data);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module." - getGrDetail - ".$e);
            return false;
        }
    }
    public function getVendorCategory($data)
    {
        try {
            $this->log->logIt($this->module . " - getVendorCategory");
            $result = $this->ObjGoodReturnDao->getVendorCategory($data);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getVendorCategory - " . $e);
            return false;
        }
    }
    public function getRawMaterialByCategory($data)
    {
        try {
            $this->log->logIt($this->module . ' - getRawMaterialByCategory');
            $result = $this->ObjGoodReturnDao->getRawMaterialFromCategory($data);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getRawMaterialByCategory - ' . $e);
        }
    }

    public function getGrnItems($data)
    {
        try {
            $this->log->logIt($this->module . " - getGrnItems");
            $result = $this->ObjGoodReturnDao->getGrnDetailForReturn($data);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getGrnItems - " . $e);
            return false;
        }
    }
    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$goodsreturn;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
    public function printGoodsReturn($data)
    {
        try
        {
            $this->log->logIt($this->module.' - printGoodsReturn');

            $id = $data['id'];
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $headerData = $this->ObjGoodReturnDao->printGoodsReturn($id,$defaultlanguageArr);
            if($headerData){
                $headerData['default_langlist'] = json_decode($defaultlanguageArr);
                $indentlanguage = new \util\language('AuthorizeReceivedindent');
                $llang_arr = \common\staticlang::$AuthorizeReceivedindent;
                $languageArr=html_entity_decode(json_encode($indentlanguage->loadlanguage($llang_arr)),ENT_QUOTES);
                $headerData['langlist'] = json_decode($languageArr);
            }
            $PrintReport = new \common\printreport();

            $PrintReport->addTemplate('goodsreturn');
            $width = '793px';
            $height = '1122px';

            $PrintReport->addRecord($headerData);
            $data = $PrintReport->Request();
            return json_encode(array("Success"=>"True","Data"=>$data, "Height"=>$height, "Width"=>$width));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - printGoodsReturn - '.$e);
        }
    }
}

?>