<?php
require_once('TwigConfig.php');
require_once(dirname(__FILE__).'/../config/config.php');
require_once(dirname(__FILE__).'/../config/constant.php');
require_once(dirname(__FILE__).'/../util/logger.php');
require_once(dirname(__FILE__).'/../util/util.php');

$request = urldecode(file_get_contents("php://input"));

if($request=="")
{
    $request = json_encode($_POST);
    if($request=="" || count($request)==0)
    {
        exit(0);
    }
}
$json_request = json_decode($request,1);
$ObjUtil = new \util\util();
$log = new \util\logger();
$json_request = $ObjUtil->cleanVariables($json_request);

$basePath = dirname(__FILE__);

define("BASEPATH",$basePath);

if(!count($json_request)) exit(0);

if(!isset($json_request['template'])) exit(0);

if(!isset($json_request['record'])) exit(0);
if(!isset($json_request['Language'])) exit(0);
if(!isset($json_request['FrontLanguage'])) exit(0);

$opcode = $json_request['template'];
$data = $json_request['record'];
$FrontLanguage = $json_request['FrontLanguage'];

$Language = $json_request['Language'];


if(!file_exists($basePath."/tmpl/".$json_request['template'].".html"))
{
    die("Invalid Service");
}

global $twig;
global $commonurl;

$template = $twig->loadTemplate($json_request['template'].".html");

$senderarr = array();
$senderarr['langlist'] = $Language;
$senderarr['default_langlist'] = $FrontLanguage;
$senderarr['data'] = $data;
$senderarr['imgurl'] = CONFIG_ASSETS_URL."company_logo/";

$data =  $template->render($senderarr);
echo $data;

?>