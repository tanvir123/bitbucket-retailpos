<?php

    define('CONFIG_DBN',isset($rec['dbname'])?$rec['dbname']:'');
    define('CONFIG_UID',isset($rec['userid'])?$rec['userid']:'');
    define('CONFIG_UNM',isset($rec['username'])?$rec['username']:'');
    define('CONFIG_CID',isset($rec['companyid'])?$rec['companyid']:'');
    define('CONFIG_BUCKET_NAME',isset($rec['bucket_name'])?$rec['bucket_name']:'');
    define('CONFIG_LID',isset($rec['locationid'])?$rec['locationid']:'');
    define('CONFIG_USR_TYPE',isset($rec['user_type'])?$rec['user_type']:'');
    define('CONFIG_CUSTOM_LANG',isset($rec['lang_type'])?$rec['lang_type']:'');
    define('CONFIG_LANG',isset($rec['lang_code'])?$rec['lang_code']:'');
    define('CONFIG_IS_STORE',isset($rec['store'])?$rec['store']:'');
    define('CONFIG_GID',isset($rec['groupid'])?$rec['groupid']:'');
    define('CONFIG_OEM_NAME',isset($rec['oem_name'])?$rec['oem_name']:'');
    define('CONFIG_OEM_TITLE',isset($rec['oem_title'])?$rec['oem_title']:'');
    define('CONFIG_OEM_ID',isset($rec['oem_id'])?$rec['oem_id']:'');
    define('CONFIG_OEM_LOGO',isset($rec['oem_logo'])?$rec['oem_logo']:'');
    define('CONFIG_SID',isset($rec['storeid'])?$rec['storeid']:'');
    define('CONFIG_LOGINTYPE',isset($rec['login_type'])?$rec['login_type']:'');
    define('CONFIG_RID',isset($rec['roleid'])?$rec['roleid']:'');
    define('CONFIG_IS_ORDERING_APPLICATION',isset($rec['tabinsta'])?$rec['tabinsta']:0);
    define('CONFIG_TRANSLATE_LANGUAGES',isset($rec['translate_languages'])?$rec['translate_languages']:'');
    define('CONFIG_POWERED_BY',isset($rec['oem_title'])? $rec['oem_title'] : 'Pure ITES');

    define('INV_STATUS_ADD',1);
    define('INV_STATUS_IV',2);
    define('INV_STATUS_GRN',3);
    define('INV_STATUS_GR',4);
    define('INV_STATUS_ORDER',5);

    define('LIQUOR_IDS',['2_15'=>'Soup','2_6'=>'Hot Drinks']);

?>
