<?php
#Author: RJ Lumbhani  - 1 June 2015

class dbconnect
{
	private $module = 'POS';
	private $log;	
	
	public function __construct()
	{
	    $this->log=new \util\logger();
	}
	
	public function connect($db_name)
	{	
		try
		{
			$this->log->logIt($this->module."-"."connect - ".$db_name);
            global $connection;
			$dsnNew="mysql:host=".CONFIG_MYSQL_HOST.";dbname=".CONFIG_MYSQL_DBNAME.";charset=utf8;";
            $connection=new PDO($dsnNew,CONFIG_MYSQL_USER,CONFIG_MYSQL_PWD);
			@$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$connection->setAttribute(PDO::ATTR_TIMEOUT,30);
			$connection->exec("SET NAMES utf8");
			return 'Success';
		}
		catch(Exception $e)
		{
			echo $e;
			$this->log->logIt($this->module."-"."connect"."-".$e);
		}
	}
	
	public function config_connect()
	{	
		try
		{
			$this->log->logIt($this->module."-"."config_connect");
            global $connection;
            $dsnNew="mysql:host=".CONFIG_MYSQL_HOST.";dbname=".CONFIG_MYSQL_DBNAME.";charset=utf8;";
            $connection=new PDO($dsnNew,CONFIG_MYSQL_USER,CONFIG_MYSQL_PWD);
			@$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$connection->setAttribute(PDO::ATTR_TIMEOUT,30);
			$connection->exec("SET NAMES utf8");
			return 'Success';
		}
		catch(Exception $e)
		{
			echo $e;
			$this->log->logIt($this->module."-"."config_connect"."-".$e);
		}
	}
}
?>