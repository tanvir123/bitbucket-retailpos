<?php

class audit_individual_activity
{
    private $module='audit_individual_activity';
    private $log;
    private $encdec;
    private $language,$default_lang_arr,$lang_arr;
    
    function __construct()
    {
        try
        {
            $this->log = new \util\logger();
            $this->language = new \util\language();
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
    public function load($data)
    {
        try
        {
            $this->log->logIt($this->module." - load");
            global $twig;
            $flag = 0;
            $this->loadLang();
            //$module_name='$'.$data['module'];
            $this->language = new \util\language($data['module']);
            if(CONFIG_LOGINTYPE== 1)
            {
                $Old_lang_arr = \common\staticlang::${"config_".$data['module']};
            }
            else{
                $Old_lang_arr = \common\staticlang::${$data['module']};
            }

            $this->lang_arr = $this->language->loadlanguage($Old_lang_arr);
            $NewlanguageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $NewlanguageArr=json_decode($NewlanguageArr);

            $default_dis_languageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_dis_languageArr=(array)json_decode($default_dis_languageArr);
            if(!isset($data) || !isset($data['id']) || !isset($data['module']))
            {
                $flag==1;
                $Obj="";
            }
            else{
                $id = (isset($data['id']))?$data['id']:"";
                $ObjStaticArray =new \common\staticarray();
                $module = $ObjStaticArray->auditlogmodules[$data['module']]['table'];
                $masterField = $ObjStaticArray->auditlogmodules[$data['module']]['masterfield'];
                $pd = $ObjStaticArray->auditlogmodules[$data['module']]['pd'];
                
                $ObjAuditDao = new \database\auditlogdao();
                $data = $ObjAuditDao->loadindividualauditlogs($id,$module,$masterField,$pd,$Old_lang_arr,$NewlanguageArr,$default_dis_languageArr);
                $res = json_decode($data,1);
                $Obj = $res['Data'];
            }
            $template = $twig->loadTemplate('audit_individual_activity.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['flag'] = $flag;
            $senderarr['rec'] = $Obj;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);

            $rec = $template->render($senderarr);
            return json_encode(array("Data"=>$rec));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - load - ".$e);
        }
    }

    public function loadLang()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
   
}
?>