<?php
class orders   
{
    public $module='orders';
    public $log;
    public $jsdateformat;
    private $language,$lang_arr,$default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();

    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;

            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(2,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(2);

            $this->language = new \util\language('config_orders');
            $this->loadLang('config_orders');
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr=json_decode($defaultlanguageArr);

            $template = $twig->loadTemplate('orders.html');
			$ObjOrdersDao = new \database\orderdao();
			$data = $ObjOrdersDao->orderslist(50,"0",'','','','',$defaultlanguageArr);
			//$invno = $ObjOrdersDao->getInvoiceNumber();
            $adjustment = \database\parameter::getParameter('roundoff');
            $round_off = \database\parameter::getParameter('digitafterdecimal');
			$jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
			$Objstaticarray =new \common\staticarray();
			$salutation = $Objstaticarray->salutation;
            $pmsdata = $ObjOrdersDao->pmsint();

			$datalist = array();
			$datalist['salutation'] = \database\parameter::getParameter('salutation');
			$datalist['country'] = \database\parameter::getParameter('country');
            $datalist['data'] = $data;

			//$countrylist = $OBJCOMMONDAO->getCountryList();

			//$ObjUserDao = new \database\menu_categorydao();
			//$categorylist = $ObjUserDao->categorylist('','','',1);
            //$categorylist = json_decode($categorylist,1);
			$usermandatoryinfo = \database\parameter::getparameter('usermandatoryinfo');

            //$Objclasslist = new \database\table_classdao();
			//$classlist = $Objclasslist->classlist('','','',1);
			//$classlist = json_decode($classlist,true);


			//$ObjDiscountDao = new \database\discountdao();
			//$disc_list = $ObjDiscountDao->getActiveDiscList();

            $roomNumberlist = $this->roomNumberlist();
            $roomNumberlist = json_decode($roomNumberlist,1);

            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['currency_sign'] = $OBJCOMMONDAO->getCurrencySign();
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
			//$senderarr['countrylist'] = $countrylist;
            $senderarr['pmslist'] = $pmsdata;
			//$senderarr['categorylist'] = $categorylist[0]['data'];
			//$senderarr['classlist'] = $classlist[0]['data'];
			//$senderarr['disc_list'] = $disc_list;
			//$senderarr['invno'] = $invno;
            $senderarr['roundoff'] = $round_off;
            $senderarr['adjustment'] = $adjustment;
			$senderarr['jsdateformat'] = $jsdate;
            $senderarr['mandatory'] = $usermandatoryinfo;
			$senderarr['salutations'] = $salutation;
			$senderarr['datalist'] = $datalist;
			$senderarr['roomlist'] = isset($roomNumberlist['Data'])?$roomNumberlist['Data']:'';
			$languageArr=html_entity_decode(json_encode($this->lang_arr));
			$senderarr['langlist'] = json_decode($languageArr);
            $senderarr['default_langlist'] = $defaultlanguageArr;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            $from="";
            $to="";
            $inv_no="";

            $this->language = new \util\language('config_orders');
            $this->loadLang('config_orders');
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
            $defaultlanguageArr=json_decode($defaultlanguageArr);

            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            if(isset($data['from']) && $data['from']!="")
                $from = $data['from'];
            if(isset($data['to']) && $data['to']!="")
                $to = $data['to'];
            if(isset($data['inv']) && $data['inv']!="")
                $inv_no = $data['inv'];
            $ObjOrdersDao = new \database\orderdao();
            $data = $ObjOrdersDao->orderslist($limit,$offset,$name,$from,$to,$inv_no,$defaultlanguageArr);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - rec - '.$e);
        }
    }
    public function addfolio($data)
    {
        try {
            $this->log->logIt($this->module . ' - addfolio');

            $arr_validate_info = array();
            $UserMandatoryInfo = \database\parameter::getparameter('usermandatoryinfo');
            if ($UserMandatoryInfo != "" && $UserMandatoryInfo != "0") {
                $arr_info = explode(',', $UserMandatoryInfo);
                $ObjStaticArray = new \common\staticarray();
                foreach ($arr_info as $key => $value) {
                    $input_name = $ObjStaticArray->validcontactinfo[$value]['Input_Name'];
                    if($input_name!=''){
                        $arr_validate_info[] = $input_name;
                    }
                }
            }
            $flag1 = \util\validate::check_notnull($data, $arr_validate_info);
            if ($flag1 == 'true') {
                $objOrderDao = new \database\orderdao;
                $rec = $objOrderDao->addfolio($data);
                return $rec;
            } else{
                return json_encode(array('Success' => 'False','Message'=>'Some field is missing'));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addfolio - '.$e);
        }
    }
    public function loadorderitems($data)
    {
        try
        {
			$this->log->logIt($this->module.' - loadorderitems');
			$folio = $data['folio'];
			if($folio!="" && $folio!=0){
			    $ObjOrdersDao = new \database\orderdao();
                $res = $ObjOrdersDao->loadorderitems($data);
                return $res;
			}else{
			    return 0;
            }
		}
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loadorderitems - '.$e);
        }
    }
    public function addorder($data)
    {
        try
        {
			$this->log->logIt($this->module.' - addorder');
			$ObjOrderDao = new \database\orderdao;
            $chk_null = \util\validate::check_notnull($data,array('amount','folioid','contactid','quantity'));
            $chk_combo = \util\validate::check_combo($data,array('item'));
            $chk_numeric = \util\validate::check_numeric($data,array('amount','quantity'));
            $chk_discount = 'true';
            $this->language = new \util\language('config_orders');
            $this->loadLang('config_orders');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            if($data['opendiscount']==1){
                if(isset($data['discount']) && $data['discount']!=""){
                    $chk_numeric = \util\validate::check_numeric($data,array('discount'));
                }
            }
            if($chk_null=="true" && $chk_combo=="true" && $chk_numeric=="true" && $chk_discount=="true"){
                $rec = $ObjOrderDao->addorder($data,$languageArr,$defaultlanguageArr);
                return $rec;
            }else{
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addorder - '.$e);
        }
    }
	public function getcustomizerec($data)
    {
        try
        {
			$this->log->logIt($this->module.' - addorder');
			$objItemDao = new \database\menu_itemdao;
			$rec = $objItemDao->getrec($data);
			$rec_arr = json_decode($rec,1);
			$mod = $rec_arr['Data']['bind_modifier'];
			global $twig;

			$template = $twig->loadTemplate('customizeitem.html');
			$senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['bucketurl'] = CONFIG_BUCKET_URL;
			$senderarr['data'] = $mod;
            $html = $template->render($senderarr);
			
			return json_encode(array("Success"=>"True","Data"=>$html));
		}
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addorder - '.$e);
			return json_encode(array("Success"=>"False"));
        }
    }
    public function voiditem($data)
    {
        try
        {
            $this->log->logIt($this->module.' - voiditem');
            $ObjOrdersDao = new \database\orderdao();
            $this->language = new \util\language('config_orders');
            $this->loadLang('config_orders');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $res = $ObjOrdersDao->voiditem($data,$languageArr,$defaultlanguageArr);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - voiditem - '.$e);
        }
    }
    public function getuserdetails($data)
    {
        try
        {
            $this->log->logIt($this->module." - getuserdetails");

            $ObjOrdersDao = new \database\orderdao();
            $res = $ObjOrdersDao->getuserdetails($data);
            return $res;
        }catch(Exception $e){
            $this->log->logIt($this->module." - getuserdetails - ".$e);
            return false;
        }
    }
	public function printinvoice($data)
	{
		try
        {
            $this->log->logIt($this->module.' - printinvoice');
            global $twig;
            $id = $data['id'];
			$ObjOrdersDao = new \database\orderdao();
            $this->language = new \util\language('config_PrintInvoice');
            $this->loadLang('config_PrintInvoice');
            $languageArr = html_entity_decode(json_encode($this->lang_arr));
            $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr));
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
            $PrintReport = new \common\printreport();
            $template = \database\parameter::getParameter('invoice');
            $headerdata = $ObjOrdersDao->getinvoicerec($id,$template,$languageArr,$defaultlanguageArr);
            if(isset($headerdata['itemarr']) && count($headerdata['itemarr']) > 0)
            {

                $tempArr = [];
                $round_off = \database\parameter::getParameter('digitafterdecimal');
                foreach($headerdata['itemarr'] as $ke => $itemar)
                {
                    if(!empty($tempArr) && count($tempArr) > 0)
                    {
                        $totltmpItem = count($tempArr);
                        $inc = 0;
                        foreach($tempArr as $tmke => $tmpItem)
                        {
                            if(isset($tmpItem['item']) && count($tmpItem['item']) > 0)
                            {
                                //$this->log->logIt($itemar['item']['detailunkid'].'----===>  '.$tmpItem['item']['unitname'].' ==> '.$itemar['item']['unitname'].'  ----- '.$tmpItem['item']['ordertype'] .' ==>'. $itemar['item']['ordertype'].'  ----  '.$tmpItem['item']['lnkmappingunkid'] .' ==> '. $itemar['item']['lnkmappingunkid'].' ----  '.$tmpItem['item']['particular'] .' ==> '. $itemar['item']['particular'].' ---- '.$tmpItem['item']['isvoid'] .' ==> '. $itemar['item']['isvoid'].' ----  '.$tmpItem['item']['mastertypeunkid'] .' ==> '. $itemar['item']['mastertypeunkid'].' ---- '.$tmpItem['item']['Baseamt'] .' ==> '. $itemar['item']['Baseamt'].' ---- '.$tmpItem['item']['masterunkid'] .' ==> '. $itemar['item']['masterunkid']);
                                if($tmpItem['item']['unitname'] == $itemar['item']['unitname'] && $tmpItem['item']['ordertype'] == $itemar['item']['ordertype'] && $tmpItem['item']['lnkmappingunkid'] == $itemar['item']['lnkmappingunkid'] && $tmpItem['item']['particular'] == $itemar['item']['particular'] && $tmpItem['item']['isvoid'] == $itemar['item']['isvoid'] && $tmpItem['item']['mastertypeunkid'] == $itemar['item']['mastertypeunkid']  && $tmpItem['item']['masterunkid'] == $itemar['item']['masterunkid'])
                                {
                                    // $this->log->logIt("In side if");
                                    $tempArr[$tmke]['item']['quantity'] = $itemar['item']['quantity'] + $tempArr[$tmke]['item']['quantity'];

                                    $tmTotl = number_format($tempArr[$tmke]['item']['total']+ $itemar['item']['total'],'2','.','');

                                    $tempArr[$tmke]['item']['total'] = $tmTotl;
                                    $tempArr[$tmke]['item']['Baseamt'] = $tmTotl;
                                    break;
                                }else{
                                    // $this->log->logIt("In side else");

                                    if(($inc+1) == $totltmpItem)
                                    {
                                        $tempArr[$ke]['item'] = $itemar['item'];
                                    }

                                }
                            }
                            $inc++;
                        }
                    }else{
                        // $this->log->logIt("Outer space");
                        $tempArr[$ke]['item'] = $itemar['item'];

                    }
                }
                if(count($tempArr) > 0)
                {

                    $headerdata['itemarr'] = $tempArr;
                }
            }
            if(isset($headerdata['liquor_info']) && count($headerdata['liquor_info'])>0 && isset($headerdata['liquor_info']['itemarr']) && count($headerdata['liquor_info']['itemarr']) > 0)
            {

                $tempArr = [];
                $round_off = \database\parameter::getParameter('digitafterdecimal');
                foreach($headerdata['liquor_info']['itemarr'] as $ke => $itemar)
                {
                    if(!empty($tempArr) && count($tempArr) > 0)
                    {
                        $totltmpItem = count($tempArr);
                        $inc = 0;
                        foreach($tempArr as $tmke => $tmpItem)
                        {
                            if(isset($tmpItem['item']) && count($tmpItem['item']) > 0)
                            {
                                //$this->log->logIt($itemar['item']['detailunkid'].'----===>  '.$tmpItem['item']['unitname'].' ==> '.$itemar['item']['unitname'].'  ----- '.$tmpItem['item']['ordertype'] .' ==>'. $itemar['item']['ordertype'].'  ----  '.$tmpItem['item']['lnkmappingunkid'] .' ==> '. $itemar['item']['lnkmappingunkid'].' ----  '.$tmpItem['item']['particular'] .' ==> '. $itemar['item']['particular'].' ---- '.$tmpItem['item']['isvoid'] .' ==> '. $itemar['item']['isvoid'].' ----  '.$tmpItem['item']['mastertypeunkid'] .' ==> '. $itemar['item']['mastertypeunkid'].' ---- '.$tmpItem['item']['Baseamt'] .' ==> '. $itemar['item']['Baseamt'].' ---- '.$tmpItem['item']['masterunkid'] .' ==> '. $itemar['item']['masterunkid']);
                                if($tmpItem['item']['unitname'] == $itemar['item']['unitname'] && $tmpItem['item']['ordertype'] == $itemar['item']['ordertype'] && $tmpItem['item']['lnkmappingunkid'] == $itemar['item']['lnkmappingunkid'] && $tmpItem['item']['particular'] == $itemar['item']['particular'] && $tmpItem['item']['isvoid'] == $itemar['item']['isvoid'] && $tmpItem['item']['mastertypeunkid'] == $itemar['item']['mastertypeunkid']  && $tmpItem['item']['masterunkid'] == $itemar['item']['masterunkid'])
                                {
                                    // $this->log->logIt("In side if");
                                    $tempArr[$tmke]['item']['quantity'] = $itemar['item']['quantity'] + $tempArr[$tmke]['item']['quantity'];

                                    $tmTotl = number_format($tempArr[$tmke]['item']['total']+ $itemar['item']['total'],'2','.','');

                                    $tempArr[$tmke]['item']['total'] = $tmTotl;
                                    $tempArr[$tmke]['item']['Baseamt'] = $tmTotl;
                                    break;
                                }else{
                                    // $this->log->logIt("In side else");

                                    if(($inc+1) == $totltmpItem)
                                    {
                                        $tempArr[$ke]['item'] = $itemar['item'];
                                    }

                                }
                            }
                            $inc++;
                        }
                    }else{
                        // $this->log->logIt("Outer space");
                        $tempArr[$ke]['item'] = $itemar['item'];

                    }
                }
                if(count($tempArr) > 0)
                {

                    $headerdata['liquor_info']['itemarr'] = $tempArr;
                }
            }
            $tagDetail=$ObjOrdersDao->getTagrecords();
            if($tagDetail){
                $tagDetail=json_decode($tagDetail['meta_value'],1);
                $headerdata['tagdetail']=$tagDetail;
            }
            $exchange_Detail=$ObjOrdersDao->getExchangeRateDetails($id,$headerdata['locationinfo']);
            if($exchange_Detail){
                $headerdata['chang_currency_detail']=$exchange_Detail;
            }
            $inv_dis_settings= \database\parameter::getParameter('invoice_display_lbl');

            if($template==1){
				$PrintReport->addTemplate('invoice1');
				$width = '302px';
				$height = '793px';
                $default_Setting='company_logo,company_ttl,address,bil_no,amt_in,date,item_qty,item_rate,item_total,sub_ttl,tax_detail,loyalty_detail,discount,gra,adjustment,grnd_ttl,production_by,tbl_no,due_amt,tender,payment_detail';
			}elseif ($template == 3) {
                $PrintReport->addTemplate('invoice3');
                $width = '302px';
                $height = '793px';
                $default_Setting = 'company_logo,company_ttl,address,bil_no,amt_in,date,item_qty,item_rate,item_total,sub_ttl,tax_detail,loyalty_detail,discount,gra,adjustment,grnd_ttl,production_by,wlc_msg,auth_detail,waiter_name,bil_station,no_of_per,cash_detail,inv_note,tbl_no,due_amt,tender,payment_detail';
            }elseif($template==4){
                $PrintReport->addTemplate('invoice4');
                $width = '302px';
                $height = '793px';
                $default_Setting='company_logo,company_ttl,address,bil_no,amt_in,date,item_qty,item_rate,item_total,sub_ttl,tax_detail,loyalty_detail,discount,gra,adjustment,grnd_ttl,production_by,tbl_no,due_amt,tender,payment_detail';
            }else{
				$PrintReport->addTemplate('invoice2');
				$width = '793px';
				$height = '1122px';
                $default_Setting='company_logo,company_ttl,address,bil_no,amt_in,date,item_qty,item_rate,item_total,sub_ttl,tax_detail,loyalty_detail,discount,gra,adjustment,grnd_ttl,production_by,phone_no,fax,email,itm_desc,due_amt,tender,payment_detail';
			}

            $headerdata['inv_dis_settings']=!empty($inv_dis_settings)?$inv_dis_settings:$default_Setting;
            $PrintReport->Language($languageArr);
            $PrintReport->FrontLanguage($defaultlanguageArr);
			$PrintReport->addRecord($headerdata);
			$data = $PrintReport->Request();
			return json_encode(array("Success"=>"True","Data"=>$data, "Height"=>$height, "Width"=>$width));
		}
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - printinvoice - '.$e);
        }
	}
    public function closefolio($data)
    {
        try
        {
            $this->log->logIt($this->module.' - closefolio');
            $ObjOrdersDao = new \database\orderdao();
            $this->language = new \util\language('config_orders');
            $this->loadLang('config_orders');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $res = $ObjOrdersDao->closefolio($data,$languageArr,$defaultlanguageArr);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - closefolio - '.$e);
        }
    }
    public function searchGuest($data)
    {
        try
        {
            $this->log->logIt($this->module.' - searchGuest');
            $ObjOrdersDao = new \database\orderdao();
            $res = $ObjOrdersDao->searchGuest($data);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - searchGuest - '.$e);
        }
    }
    public function removefolio($data)
    {
        try
        {
            $this->log->logIt($this->module.' - removefolio');
            $ObjOrderDao = new \database\orderdao();
            $this->language = new \util\language('config_orders');
            $this->loadLang('config_orders');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);

            $res = $ObjOrderDao->removefolio($data,$languageArr);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - removefolio - '.$e);
        }
    }
    public function getitemprice($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getitemprice');
            if($data['type']==1){
                $ObjItemDao = new \database\menu_itemdao();
                $data1 = $ObjItemDao->getrec_itemonly($data);
            }
            if($data['type']==2){
                $ObjComboDao = new \database\combo_productdao();
                $data1 = $ObjComboDao->getComboRec($data);
            }
            return $data1;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getitemprice - '.$e);
        }
    }
    public function searchlist($data)
    {
        try
        {
            $this->log->logIt($this->module.' - searchlist');
            $hid = \database\parameter::getParameter('pms_hotelid');
            $objUser =   new \database\orderdao();
            $data1 = $objUser->takedataforreservation();
            $todaysdate = \database\parameter::getParameter('todaysdate');
            $arr = array(
                "service"=>"posintegration",
                "opcode"=>"getReservationList",
                "guest_name" =>$data['name'],
                "reservation_no" => $data['reservationno'],
                "uname"=>$data1['pms_username'],
                "pwd"=>$data1['pms_password'],
                "hid"=>$hid,
                "roomid"=>$data['roomid'],
                "todaydate"=>$todaysdate,
            );
            $arr_str = json_encode($arr);
            $this->log->logIt('================================== Request Array ');
            $this->log->logIt($arr_str);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, CONFIG_PMSSERVICE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,urlencode($arr_str));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $res=curl_exec($ch);
            curl_close($ch);
            $data = trim($res);
            $this->log->logIt('================================== Response Array ');
            $this->log->logIt($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - searchlist - '.$e);
        }
    }

    public function roomNumberlist()
    {
        try
        {
            $this->log->logIt($this->module.' - roomNumberlist');
            $hid = \database\parameter::getParameter('pms_hotelid');
            $objUser =   new \database\orderdao();
            $data1 = $objUser->takedataforreservation();
            $todaysdate = \database\parameter::getParameter('todaysdate');

            $arr = array(
                "service"=>"posintegration",
                "opcode"=>"getRoomList",
                "uname"=>$data1['pms_username'],
                "pwd"=>$data1['pms_password'],
                "hid"=>$hid,
                "todaydate"=>$todaysdate,
            );
            $arr_str = json_encode($arr);
            $this->log->logIt('================================== Request Array ');
            $this->log->logIt($arr_str);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, CONFIG_PMSSERVICE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,urlencode($arr_str));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $res=curl_exec($ch);
            curl_close($ch);

            $data = trim($res);
            $this->log->logIt('================================== Response Array ');
            $this->log->logIt($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - roomNumberlist - '.$e);
        }
    }

    public function postcharge($data)
    {
        try
        {
            $this->log->logIt($this->module . ' - postcharge');

            $hid = \database\parameter::getParameter('pms_hotelid');
            $objOrder =   new \database\orderdao();
            $data1 = $objOrder->takedataforreservation();
            $objOrder =   new \database\orderdao();
            $ObjCommonDao =   new \database\commondao();
            $hash =$ObjCommonDao->getFieldValue('fasfoliomaster','hashkey','foliounkid',$data['folioid']);
            $rec_amount = $objOrder->getamount($hash);
            $todaysdate = \database\parameter::getParameter('todaysdate');


            if(isset($rec_amount) && $rec_amount['foliono']==''){
                $genrate_invoice_nocharge = \database\parameter::getParameter('genrate_invoice_for_nochargeuser');
                if($rec_amount['contacttype'] == 4 && $genrate_invoice_nocharge == 0){
                    GOTO outinvoice;
                }else{
                    $objcommoninvno=$ObjCommonDao->getStartEndINVOICENumber();
                    if($objcommoninvno['is_valid']==0){
                        return json_encode(array('Success' => 'False', 'Message' => 'Invoice No. limit is over. Please change your invoice No !'));
                    }
                    $invoice = $ObjCommonDao->getInvoiceNumber('inc');
                }
            }
            outinvoice:

            $arr = array(
                "service" =>"posintegration",
                "opcode" => "postChargeToResFolio",
                "tranid"=>$data['tranid'],
                "amount" => $rec_amount['orderAmt'],
                "folio_no"=>isset($invoice['invoice_no'])?$invoice['invoice_no']:'',
                "uname"=>$data1['pms_username'],
                "pwd"=>$data1['pms_password'],
                "hid"=>$hid,
                "todaydate"=>$todaysdate,
            );
            $arr_str = json_encode($arr);
            $this->log->logIt('================================== Request Array ');
            $this->log->logIt($arr_str);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, CONFIG_PMSSERVICE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,urlencode($arr_str));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $res=curl_exec($ch);
            curl_close($ch);
            $resdata = trim($res);
            $res=json_decode($resdata,1);
            $this->log->logIt('================================== Response Array ');
            $this->log->logIt($resdata);

            $this->language = new \util\language('config_orders');
            $this->loadLang('config_orders');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);


            if($res["Success"]=="True"){
                $orderdata = $objOrder->closefolioOnPostCharge($data,$languageArr,isset($invoice)?$invoice:'');
                $ObjAuditDao = new \database\orderlogdao();
                $ObjAuditDao->addOrderLog($data['folioid'], 'POST_TO_PMS_FOLIO', $data);
            }
            if($res["Success"]=="True" && $res["Message"]=='Charges Posted Successfully')
            return json_encode(array("Success" => "True", "Message" => $languageArr->LANG60));

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - postcharge - '.$e);
        }
    }
    public function updateordercharge($data)
    {
        try
        {
            $this->log->logIt($this->module.'-updateordercharge');

                $Objorder =  new \database\orderdao();
            $this->language = new \util\language('config_orders');
            $this->loadLang('config_orders');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $data = $Objorder->updatesplitorder($data,$languageArr,$defaultlanguageArr);
                return $data;
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module.'- updateordercharge-'.$e);
        }
    }
    public function getlistdataofguest($data)
    {
        try
        {
            $this->log->logIt($this->module.'-getlistdataofguest');
            $obj = new \database\orderdao();
            $res = $obj->getlistofguest($data);
            return $res;

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-getlistdataofguest -'.$e);
        }
    }
    public function getlistdata($data)
    {
        try
        {
            $this->log->logIt($this->module.'-getlistdata');
            $obj = new \database\orderdao();
            $res = $obj->getlistofguestdata($data);
            return $res;

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-getlistdata -'.$e);
        }
    }
    public function invoicebysearch($data)
    {
        try
        {
            $this->log->logIt($this->module.'-invoicebysearch');
            $obj = new \database\orderdao();
            $this->language = new \util\language('config_orders');
            $this->loadLang('config_orders');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $result = $obj->invoicebysearch($data,$languageArr,$defaultlanguageArr);
            return $result;
		}
        catch (Exception $e)
        {
            $this->log->logIt($this->module.'-invoicebysearch-'.$e);
        }
    }
    public function orderdata($data)
    {
        try
        {
            $this->log->logIt($this->module.'-orderdata-');

            $obj = new \database\orderdao();
            $data = $obj->orderlistformerge($data);
            return $data;

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-orderdata-'.$e);
        }
    }
    public function mergeorderlist($data)
    {
        try
        {
            $this->log->logIt($this->module.'-mergeorderlist');
            $obj = new \database\orderdao();
            $this->language = new \util\language('config_orders');
            $this->loadLang('config_orders');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $merge = $obj->mergeorder($data,$languageArr,$defaultlanguageArr);
            return $merge;
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module.'-mergeorderlist -'.$e);
        }
    }
    public function tablelist($data)
    {
        try
        {
           $this->log->logIt($this->module.'-tablelist');
           $Obj = new \database\table_tabledao();
           $res = $Obj->gettableclasswise($data);
           return $res;
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module.'-tablelist -'.$e);
        }
    }
    public function removeorder($data)
    {
        try
        {
            $this->log->logIt($this->module.'-removeorder');
            $Obj = new \database\orderdao();
            $this->language = new \util\language('config_orders');
            $this->loadLang('config_orders');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $result = $Obj->removeorder($data,$languageArr,$defaultlanguageArr);
            return $result;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-removeorder -'.$e);
        }
    }
    public function cancelOrder($data)
    {
        try
        {
            $this->log->logIt($this->module.' - cancelOrder');
            $ObjOrderDao = new \database\orderdao();
            $this->language = new \util\language('config_orders');
            $this->loadLang('config_orders');
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $rec = $ObjOrderDao->cancelOrder($data,$languageArr,$defaultlanguageArr);
            return $rec;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - cancelOrder - '.$e);
        }
    }
	public function getItemList()
    {
        try
        {
            $this->log->logIt($this->module.'-removeorder');
            $Obj = new \database\orderdao();
            $data = $Obj->getItemList();
            return $data;
		}
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-removeorder -'.$e);
        }
    }
    public function loadLang($operation)
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            if($operation =='config_orders')
            {
                $default_lang_arr = \common\staticlang::$config_orders;
            }
            if($operation =='config_PrintInvoice')
            {
                $default_lang_arr = \common\staticlang::$config_PrintInvoice;
            }
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();


        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }
}
?>