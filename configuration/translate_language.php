<?php

class translate_language
{
    public $module='translate_language';
    public $log;
    private $language,$lang_arr,$default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_translate_language');
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(6,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(6);

            $this->loadLang();
            $template = $twig->loadTemplate('translate_language.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
			$senderarr['module'] = $this->module;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	
    public function gettranslateLanguageRec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - gettranslateLanguageRec');

            $id= $data['search_as'];
            $ObjUserDao = new \database\translate_languagedao();
			$data = $ObjUserDao->gettranslateLanguageRec($id);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - gettranslateLanguageRec - '.$e);
        }
    }

    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);
            $ObjUserDao = new \database\translate_languagedao();
            unset($data['service']);
            unset($data['opcode']);
			$data = $ObjUserDao->addeditfrm($data,$default_langlist);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }
    public function getTLanguagedata($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getTLanguagedata');

            $ObjUserDao = new \database\translate_languagedao();
			$data = $ObjUserDao->getTLanguagedata($data['id']);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getTLanguagedata - '.$e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_translate_language;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }

}


?>