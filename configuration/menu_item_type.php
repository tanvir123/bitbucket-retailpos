<?php
require_once(dirname(__FILE__)."/common/s3fileUpload.php");
class menu_item_type
{
    public $module='menu_item_type';
    public $log;
    private $language,$lang_arr,$default_lang_arr;
    
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_menu_item_type');
    }
	public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(8,$this->module);
            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(8);
            $ObjTypeDao = new \database\menu_item_typedao();
            $data = $ObjTypeDao->itemtypelist(50,"0",'');
			$template = $twig->loadTemplate('menu_item_type.html');
            $this->loadLang();	
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
			$senderarr['module'] = $this->module;
			$senderarr['datalist'] = $data;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);

        }

        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	
	public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
            $flag = \util\validate::check_notnull($data,array('name','sortkey'));
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            if($flag=='true')
            {

                //$bucket=CONFIG_BUCKET_NAME;
//                $directoryname='pure-ipos-menu.images';
                $flag_file = isset($_FILES['upload_img'])?$_FILES['upload_img']:"";

                $img_flag="";
                if($flag_file['tmp_name']!="")
                {
                    $directoryname = dirname(__FILE__).'/imageconfiguration/';
                    if(!is_dir($directoryname)){
                        //Directory does not exist, so lets create it.
                        mkdir($directoryname, 0777, true);
                    }
                    $temporary = explode(".", $flag_file["name"]);
                    $file_extension = end($temporary);
                    $flag_name = time() . '-' . $flag_file["name"];
                    $flag_dirname = dirname(__FILE__) . '/imageconfiguration/' . $flag_name;

                    $flag_db = "";
                    //$flag_name = time().'-'.$flag_file["name"];
                    if ((($flag_file["type"] == "image/png") || ($flag_file["type"] == "image/jpg") || ($flag_file["type"] == "image/jpeg"))
                        && ($flag_file["size"] < 24800000))
                    {
                        move_uploaded_file($flag_file["tmp_name"], $flag_dirname);
                        $flag_db = $flag_name;
//                        $arr_file = array();
//                        $arr_file[0]['name'] = $directoryname.'/'.$flag_name;
//                        $arr_file[0]['tmp_name'] = $flag_file['tmp_name'];
//                        $arr_file[0]['type'] = $flag_file["type"];
//
//                        $ObjFile = new s3fileUpload();
//                        $ObjFile->createBucket($bucket);
//                        $resFile = $ObjFile->uploadFiles($arr_file,$bucket);
//                        if($resFile!='' && $resFile!=0){
//                            $img_flag = urldecode($resFile[0]);
                    //     }
//                    else{
//                            $img_flag = '';
//                        }
                    }
                    $img_flag = (file_exists("imageconfiguration/$flag_db")) ? $flag_db : "";

                }

                if($img_flag!=""){
                    $imagename = CONFIG_COMMON_URL . 'imageconfiguration/' . $data['imagephoto'];
                    if($imagename!="")
                    {
//                        $rmv_file = array();
//                        $rmv_file[] = $imagename;
//                        $ObjFile = new s3fileUpload();
//                        $ObjFile->deleteFiles($rmv_file,$bucket);
                        $path = $imagename;
                        if (file_exists($path)) {
                            unlink($path);
                        }
                    }
                    $res = CONFIG_COMMON_URL . 'imageconfiguration/' . $img_flag; //add
                }else{
                    $res =$data['imagephoto'];
                }


                $reqarr = array(
                            'name'=>$data['name'],
                            'sortkey'=>$data['sortkey'],
                            'image'=>$res,
                            'rdo_status'=>$data['rdo_status'],
                            "id"=>$data['id'],
                            "module" => $this->module
                        );
				$ObjUnitDao = new \database\menu_item_typedao();
				$data = $ObjUnitDao->addItemtype($reqarr,$languageArr,$defaultlanguageArr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }
	
	public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
			
            $ObjUserDao = new \database\menu_item_typedao();
			$data = $ObjUserDao->itemtypelist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	
	public function getItemtypeRec($data)
	{
		try
		{
            $this->log->logIt($this->module." - getItemtypeRec");
            $ObjUnitDao = new \database\menu_item_typedao();
			$data = $ObjUnitDao->getItemtypeRec($data);
            return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - getItemtypeRec - ".$e);
			return false; 
		}
	}


    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_menu_item_type;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }
}
?>