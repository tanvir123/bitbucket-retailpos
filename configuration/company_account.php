<?php


class company_account
{
    public $module='company_account';
    public $log;
    private $language,$lang_arr,$default_lang_arr,$Reset_type;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_company_account');
        $this->Reset_type = new \util\language('config_Reset');
        $this->store_language = new \util\language('config_salutation');

    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(94,$this->module);

            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(94);

            $jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];

            $ObjUserDao = new \database\company_accountdao();
            $data = $ObjUserDao->companyAccountlist(50,"0",'','','',$languageArr,$defaultlanguageArr);

            $reset_lang_arr = \common\staticlang::$config_Reset;
            $reset = $this->Reset_type->loadlanguage($reset_lang_arr);
            $resetArr=html_entity_decode(json_encode($reset),ENT_QUOTES);
            $reset_arr = (array)json_decode($resetArr);

            $salutation_lang_arr = \common\staticlang::$config_salutation;
            $salutation = $this->store_language->loadlanguage($salutation_lang_arr);
            $salutationArr=html_entity_decode(json_encode($salutation),ENT_QUOTES);
            $salutationArr = (array)json_decode($salutationArr);

            $country = $OBJCOMMONDAO->getCountryList();

            $template = $twig->loadTemplate('company_account.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
			$senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;
            $senderarr['reset'] = $reset_arr;
            $senderarr['jsdateformat'] = $jsdate;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['salutations'] = $salutationArr;
            $senderarr['countrylist'] = $country;

            echo $template->render($senderarr);
			
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    
    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
            $flag1 = \util\validate::check_notnull($data,array('company','salutation','name',
                'address','city','state','country','mobile','email','tinno','zipcode'));
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            if($flag1=='true')
            {
                $reqarr = array(
                            "company"=>$data['company'],
                            "salutation" =>$data['salutation'],
                            "name" => $data['name'],
                            "address" => $data['address'],
							"city"=>$data['city'],
							"state"=>$data['state'],
                            "country"=>$data['country'],
                            "zipcode"=>$data['zipcode'],
                            "phone"=>isset($data['phone']) && $data['phone'] !='' ?$data['phone']:'',
                            "mobile"=>$data['mobile'],
                            "fax"=>isset($data['fax']) && $data['fax'] !='' ?$data['fax']:'',
                            "email"=>$data['email'],
                            "sortkey"=>isset($data['sortkey']) && $data['sortkey'] !='' ?$data['sortkey']:'',
                            "tinno"=>$data['tinno'],
							"amt_limit"=>$data['amt_limit'],
                            "rdo_status"=>$data['rdo_status'],
                            "id"=>$data['id'],
                            "module" => $this->module
                        );
				$ObjCompanyAcoountDao = new \database\company_accountdao();
				$data = $ObjCompanyAcoountDao->addCompanyAcoount($reqarr,$defaultlanguageArr);
                return $data;
                 
            }else
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }
	
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset="0";
            $company="";

            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['cm']) && $data['cm']!="")
                $company = $data['cm'];
			$ObjUserDao = new \database\company_accountdao();
			$data = $ObjUserDao->companyAccountlist($limit,$offset,$company);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    public function getCompanyAccountRec($data)
    {
        try
        {
            $this->log->logIt($this->module." - getCompanyAccountRec");
            $ObjVendorDao = new \database\company_accountdao();
            $data = $ObjVendorDao->getCompanyAccountRec($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getCompanyAccountRec - ".$e);
            return false;
        }
    }


    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_company_account;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }


}
?>
    
