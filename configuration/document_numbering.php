<?php
class document_numbering
{
    private $module='document_numbering';
    private $log;
    private $language,$lang_arr,$default_lang_arr,$Reset_type;
    function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_document_numbering');
        $this->Reset_type = new \util\language('config_Reset');
    }
    
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(19,$this->module);
            $Objdocdao = new \database\document_numberingdao();
            $docnumdata = $Objdocdao->doclist();
            
            $data = json_decode($docnumdata,true);

            $reset_lang_arr = \common\staticlang::$config_Reset;
            $reset = $this->Reset_type->loadlanguage($reset_lang_arr);
            $resetArr=html_entity_decode(json_encode($reset),ENT_QUOTES);
            $reset_arr = (array)json_decode($resetArr);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(19);


            $template = $twig->loadTemplate('document_numbering.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data[0]['data'];
            $senderarr['reset'] = $reset_arr;
            $languageArr=html_entity_decode(json_encode($this->lang_arr));
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    public function rec()
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            global $twig;
            $Objdocdao = new \database\document_numberingdao();
            $accountdata = $Objdocdao->doclist();
            return $accountdata;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    public function docnum($data)
    {
        try
        {
            $this->log->logIt($this->module." - docnum");
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $data['module']=$this->module;
            $ObjDocDao = new \database\document_numberingdao();
            $id = $ObjDocDao->docupdate($data,$languageArr);
            if($id=="" || $id==0)
                return json_encode(array("Success"=>"False",'Data'=>$id));
            else
                return json_encode(array("Success"=>"True",'Data'=>$id));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - docnum - ".$e);
        }
    }


    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_document_numbering;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }
}
?>