<?php

class orderslogs
{
    private $module='orderslogs';
    private $log;
    private $encdec;
    private $language,$default_lang_arr;

    function __construct()
    {
        try
        {
            $this->log = new \util\logger();
            $this->language = new \util\language('orderslogs');
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
    public function load($data)
    {
        try
        {
            $this->log->logIt($this->module." - load");

            global $twig;
            $flag = 0;
            if(!isset($data) || !isset($data['id']) || !isset($data['module']))
            {
                $flag==1;
                $Obj="";
            }
            else{
                $id = (isset($data['id']))?$data['id']:"";
                $ObjAuditDao = new \database\orderlogdao();
                $data = $ObjAuditDao->listOrderLogs($id);

                $res = json_decode($data,1);
                $Obj = $res['Data'];
            }
            $template = $twig->loadTemplate('orderslogs.html');

            $senderarr = array();
            $this->loadLang();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['flag'] = $flag;
            $senderarr['rec'] = $Obj;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);

            $rec = $template->render($senderarr);
            return json_encode(array("Data"=>$rec));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - load - ".$e);
        }
    }
    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }

}
?>
