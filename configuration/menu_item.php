<?php
require_once(dirname(__FILE__)."/common/s3fileUpload.php");

class menu_item
{
    public $module='menu_item';
    public $log;
    private $language,$lang_arr,$default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_menu_item');
    }

    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(10,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(10);
            $ObjmenuitemDao = new \database\menu_itemdao();
			$data = $ObjmenuitemDao->itemlist(50,'0','');

            $ObjitmunitDao = new \database\menu_item_unitdao();
            $itmunitData = $ObjitmunitDao->itemunitlist('','','',1);
            $itemUnitData = json_decode($itmunitData,1);

            $ObjUserDao = new \database\menu_categorydao();
			$categorylist = $ObjUserDao->categorylist('','','',1);
            $categorylist = json_decode($categorylist,1);

            $ObjTypeDao = new \database\menu_item_typedao();
            $itemtypelist = $ObjTypeDao->itemtypelist('','','',1);
            $itemtypeData = json_decode($itemtypelist,1);

            $obj = new \database\synctabinstadao();
            $tabmenulist = $obj->gettabmenulist();
            $menulist = json_decode($tabmenulist,1);

            $objdata = new \database\synctabinstadao();
            $tabcategorylist = $objdata->gettabcatlist();
            $catlist = json_decode($tabcategorylist,1);

            $Objtabinstadao = new \database\synctabinstadao();
            $tbdata = $Objtabinstadao->tabint();

            $ObjTaxDao = new \database\taxdao();
            $taxDetail=$ObjTaxDao->getAppliedTax();

            $ObjmodifierDao = new \database\menu_modifierdao();
			$modifierlist = $ObjmodifierDao->getmodifier_combo();
            $template = $twig->loadTemplate('menu_item.html');

			$modifierlist1 = array();
            foreach($modifierlist['modifier'] AS $val)
			{
				$modifierlist1[$val['modifierunkid']] = $val;
			}

            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $Zomatosetting = $OBJCOMMONDAO->getZomatosetting();

            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;
            $senderarr['categorylist'] = $categorylist[0]['data'];
            $senderarr['tabinstamenu'] = $menulist['Data'];
            $senderarr['tabinstacategory'] = $catlist['Data'];
            $senderarr['tablist'] = $tbdata;
            $senderarr['modifierlist'] = $modifierlist;
            $senderarr['itmunitlist'] = $itemUnitData[0]['data'];
            $senderarr['itemtypelist'] = $itemtypeData[0]['data'];
			$senderarr['modifierlist_str'] = json_encode($modifierlist1);
            $senderarr['modifierunit'] = json_encode($modifierlist['modifier_unit']);
            $senderarr['taxlist'] = $taxDetail;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['round_off'] = $round_off;
            $senderarr['is_zomato'] = $Zomatosetting;

            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function addeditfrm($data)
    {
        try
        {

			$this->log->logIt($this->module.' - addeditfrm');
			$ObjCommonDao = new \database\commondao;
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);
			$flag1 = \util\validate::check_notnull($data,array('txtname','txtsku','rdo_status'));
			$flag2 = \util\validate::check_combo($data,array('txtcategory','serve_unit','itemtype'));
			$flag4 = $flag5  ='true';

            $ObjmenuitemDao = new \database\menu_itemdao();
            $is_tax_available=$ObjmenuitemDao->checktaxavailableornot(isset($data['tax'])?$data['tax']:'');
            $is_rate_available=isset($data['rates'])?1:0;

            if($is_tax_available > 0 && $is_rate_available==0){
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
            }else if($is_tax_available == 0 && $is_rate_available>0){
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
            }

            $unit_arr = array();
            $rate_arr = array();
            if(isset($data['rates']) && $data['rates']!=''){
                foreach ($data['rates'] AS $rval) {
                    $rate_arr[] = $rval;
                }
            }
            $rates= implode(",",$rate_arr);

            $bind_unit = isset($data['unit_detail']) ? $data['unit_detail'] : '';

            if ($bind_unit != '') {
                $bind_unit_arr = explode(',', $bind_unit);
                foreach ($bind_unit_arr AS $sub_unit) {
                    $upkey = $ObjCommonDao->getprimaryBycompany('cfmenu_itemunit', $sub_unit, 'unitunkid');
                    $unit_arr[$upkey]['rate1'] = $data['rate1_' . $sub_unit];
                    $unit_arr[$upkey]['rate2'] = $data['rate2_' . $sub_unit];
                    $unit_arr[$upkey]['rate3'] = $data['rate3_' . $sub_unit];
                    $unit_arr[$upkey]['rate4'] = $data['rate4_' . $sub_unit];

                    $unit_arr[$upkey]['defaultrate'] = $data['defaultrate_' . $sub_unit];
                   /* $defaultRate = $data['defaultrate_' . $sub_unit];
                    if (($data['defaultrate_' . $sub_unit] != "") && ($data['rate'.$defaultRate.'_' . $sub_unit] == ''))
                        $flag5 = 'false';
                    else
                        $unit_arr[$upkey]['rate'.$defaultRate.''] = $data['rate'.$defaultRate.'_' . $sub_unit];*/

                   //Bind Modifier

                        $bind_modifier = isset($data['bind_modifiers_' . $sub_unit])?$data['bind_modifiers_' . $sub_unit]:'';
                        if($bind_modifier!='') {
                            $bind_modifier_arr = explode(',',$bind_modifier);
                            $modifire_bind_arr = array();
                            $cnt=0;
                            foreach($bind_modifier_arr AS $val)
                            {
                                $modifire_bind_arr[$cnt]['modid'] = $val;
                                if(isset($data['min_'.$sub_unit.'_'.$val])!=1 || $data['min_'.$sub_unit.'_'.$val]=="")
                                    $flag4 = 'false';
                                else
                                    $modifire_bind_arr[$cnt]['min'] = $data['min_'.$sub_unit.'_'.$val];

                                if(isset($data['max_'.$sub_unit.'_'.$val])!=1 || $data['max_'.$sub_unit.'_'.$val]=="")
                                    $flag4 = 'false';

                                else
                                    $modifire_bind_arr[$cnt]['max'] = $data['max_'.$sub_unit.'_'.$val];

                                if((isset($data['mounit_'.$sub_unit.'_'.$val])) && ($data['mounit_' .$sub_unit.'_'. $val] == 0))
                                    $flag4 = 'false';
                                else
                                    $modifire_bind_arr[$cnt]['mounit'] = isset($data['mounit_'.$sub_unit.'_' . $val])?$data['mounit_'.$sub_unit.'_' . $val]:'';

                                if((isset($data['sale_amount_'.$sub_unit.'_'.$val])) && ($data['sale_amount_' .$sub_unit.'_'. $val] == ""))
                                    $flag4 = 'false';
                                else
                                    $modifire_bind_arr[$cnt]['sale_amount'] = isset($data['sale_amount_'.$sub_unit.'_' . $val])?$data['sale_amount_'.$sub_unit.'_' . $val]:'';

                                $bind_sub_modifier = isset($data['sub_modifier_item_'.$sub_unit.'_'.$val])?$data['sub_modifier_item_'.$sub_unit.'_'.$val]:'';

                                if($bind_sub_modifier != '')
                                {
                                    $bind_sub_modifier_arr = explode(',',$bind_sub_modifier);
                                    $sub_arr = array();
                                    foreach($bind_sub_modifier_arr AS $sub_val)
                                    {
                                        $pkey = $sub_val;

                                        if((isset($data['mounit_'.$sub_unit.'_'.$sub_val])) &&  ($data['mounit_' .$sub_unit.'_'. $sub_val] == 0))
                                            $flag4 = 'false';
                                        else
                                            $sub_arr[$pkey]['mounit'] = $data['mounit_'.$sub_unit.'_' . $sub_val];

                                        if((isset($data['sale_amount_'.$sub_unit.'_'.$sub_val])) &&  ($data['sale_amount_' .$sub_unit.'_'. $sub_val] == ""))
                                            $flag4 = 'false';
                                        else
                                            $sub_arr[$pkey]['sale_amount'] = $data['sale_amount_'.$sub_unit.'_' . $sub_val];

                                        $sub_arr[$pkey]['description'] = $data['description_'.$sub_unit.'_' . $sub_val];
                                    }
                                    $modifire_bind_arr[$cnt]['modifier_item'] = $sub_arr;
                                }
                                $cnt++;
                            }
                            $unit_arr[$upkey]['modifier'] = $modifire_bind_arr;
                        }
                    //
                    //$this->log->logIt("Bind recipe ::".json_encode($data));
                    //Bind recipe
                    $bind_recipe = isset($data['raw_detail_'.$sub_unit])?$data['raw_detail_'.$sub_unit]:'';

                    if ($bind_recipe != ''){
                        $bind_recipe_arr = explode(',', $bind_recipe);
                        $recipe_arr = array();

                        foreach ($bind_recipe_arr AS $sub_recipe) {
                        	
                            if (isset($data['store_' .$sub_unit.'_'. $sub_recipe]) != 1 || $data['store_' .$sub_unit.'_'. $sub_recipe] == "")
                          	{
								$flag5 = 'false';
							}
                            else
                                $recipe_arr[$sub_recipe]['store'] = $data['store_' .$sub_unit.'_'.$sub_recipe];
                            if (isset($data['unit_' .$sub_unit.'_'. $sub_recipe]) != 1 || $data['unit_' .$sub_unit.'_'. $sub_recipe] == "")
							{
								$flag5 = 'false';
							}
                            else
                                $recipe_arr[$sub_recipe]['unit'] = $data['unit_' .$sub_unit.'_'.$sub_recipe];
                            if ($data['unitval_' .$sub_unit.'_' .$sub_recipe] == "")
							{
								$flag5 = 'false';
							}
                            else
                                $recipe_arr[$sub_recipe]['unitval'] = $data['unitval_'.$sub_unit.'_'.$sub_recipe];
                            if (isset($data['quantity_' .$sub_unit.'_'.$sub_recipe]) != 1){
								$flag5 = 'false';
							}
                            else
                                $recipe_arr[$sub_recipe]['quantity'] = $data['quantity_' .$sub_unit.'_'.$sub_recipe];
                        }
                        $unit_arr[$upkey]['recipe'] = $recipe_arr;
                    }
                    //
                }
            }

			if($flag1=='true' && $flag2=='true' && $flag4=='true' && $flag5=='true')
			{
//				$bucket=CONFIG_BUCKET_NAME;
//				$directoryname='pure-ipos-menu-item.images';
				$flag_file = isset($_FILES['upload_img'])?$_FILES['upload_img']:"";
				$img_flag="";
				 if($flag_file['tmp_name']!="")
				  {
                      $directoryname = dirname(__FILE__).'/imageconfiguration/';
                      if(!is_dir($directoryname)){
                          //Directory does not exist, so lets create it.
                          mkdir($directoryname, 0777, true);
                      }
                      $temporary = explode(".", $flag_file["name"]);
                      $file_extension = end($temporary);
                      $flag_name = time() . '-' . $flag_file["name"];
                      $flag_dirname = dirname(__FILE__) . '/imageconfiguration/' . $flag_name;

                      $flag_db = "";
//					  $flag_name = time().'-'.$flag_file["name"];
					  if ((($flag_file["type"] == "image/png") || ($flag_file["type"] == "image/jpg") || ($flag_file["type"] == "image/jpeg"))
					  && ($flag_file["size"] < 24800000))
					  {
					      move_uploaded_file($flag_file["tmp_name"], $flag_dirname);
                          $flag_db = $flag_name;
//                          $arr_file = array();
//                          $arr_file[0]['name'] = $directoryname.'/'.$flag_name;
//                          $arr_file[0]['tmp_name'] = $flag_file['tmp_name'];
//                          $arr_file[0]['type'] = $flag_file["type"];
//
//                          $ObjFile = new s3fileUpload();
//						  $ObjFile->createBucket($bucket);
//                          $resFile = $ObjFile->uploadFiles($arr_file,$bucket);
//                          if($resFile!='' && $resFile!=0){
//                              $img_flag = urldecode($resFile[0]);
//                          }else{
//                              $img_flag = '';
//                          }
					  }
                     $img_flag = (file_exists("imageconfiguration/$flag_db")) ? $flag_db : "";
				  }
	
				  if($img_flag!=""){
                      $imagename = CONFIG_COMMON_URL . 'imageconfiguration/' . $data['imagephoto'];
					  if($imagename!="")
					  {
//                          $rmv_file = array();
//                          $rmv_file[] = $imagename;
//                          $ObjFile = new s3fileUpload();
//                          $ObjFile->deleteFiles($rmv_file,$bucket);
                          $path = $imagename;
                          if (file_exists($path)) {
                              unlink($path);
                          }
					  }
                      $res = CONFIG_COMMON_URL . 'imageconfiguration/' . $img_flag; //add
				  }else{
                      $res = $data['imagephoto'];
				  }

                $req_data = array(
					  "name" => $data['txtname'],
					  "sku" => $data['txtsku'],
                      "spicy_type" => isset($data['spicy_type'])?$data['spicy_type']:0,
					  "saleamount" => isset($data['txtsaleamount']) && $data['txtsaleamount'] != '' ? $data['txtsaleamount'] :0,
					  "cat" => $data['txtcategory'],
                      "itemtype" => $data['itemtype'],
					  "longdesc" => $data['txtlongdescription'],
					  "image" => $res,
                      "serve_unit" => $data['serve_unit'],
					  "id"=>$data['id'],
					  "rdo_status"=>$data['rdo_status'],
					  "item_type" =>isset($data['item_type']) ? $data['item_type'] :0,
                      "bind_unit"=>$unit_arr,
                      "bind_tax"=> isset($data['tax']) ? $data['tax'] : [],
                      "rates"=> isset($rates) ? $rates:'',
                      "module" => $this->module
				  );

                $data = $ObjmenuitemDao->additem($req_data,$languageArr,$defaultlanguageArr);
				  return $data;
			}
			else{
				return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
			}
        }
		catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }

	public function getrec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getrec');
            $ObjmenuitemDao = new \database\menu_itemdao();
			$data1 = $ObjmenuitemDao->getrec($data);
			return $data1;
		}
		catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getrec - '.$e);
        }
    }

	public function getrec_itemonly($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getrec_itemonly');
            $ObjmenuitemDao = new \database\menu_itemdao();
			$data1 = $ObjmenuitemDao->getrec_itemonly($data);
			return $data1;
		}
		catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getrec - '.$e);
        }
    }

    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset="0";
            $name="";
            
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
          
            $ObjmenuitemDao = new \database\menu_itemdao();
			$data = $ObjmenuitemDao->itemlist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

	public function getmodifieritem($data)
	{
		try
		{
			$this->log->logIt($this->module." - getmodifieritem");
			//$ObjCommonDao = new \database\commondao;
			//$unkid = $ObjCommonDao->getprimarykey('cfmenu_modifiers',$data['modifierid'],'modifierunkid');
			$ObjmenuitemDao = new \database\menu_modifier_itemdao();
			$data1 = $ObjmenuitemDao->getmodifieritem_modifier($data['modifierid']);
			return $data1;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getmodifieritem - ".$e);
			return false; 
		}
	}
    public function getRawmaterial()
    {
        try
        {
            $this->log->logIt($this->module." - getRawmaterial");
            $ObjrawDao = new \database\rawmaterialdao();
            $data = $ObjrawDao->getAllRawdetail();
            return $data;
        }catch(Exception $e){
            $this->log->logIt($this->module." - getRawmaterial - ".$e);
            return false;
        }
    }

    public function getAllItemunit($data)
    {
        try
        {
            $this->log->logIt($this->module." - getAllItemunit");
            $ObjitmunitDao = new \database\menu_item_unitdao();
            $data = $ObjitmunitDao->getAllItemunit($data);
            return $data;
        }catch(Exception $e){
            $this->log->logIt($this->module." - getAllItemunit - ".$e);
            return false;
        }
    }

    public function getItemList($data)
	{
		try
		{
			$this->log->logIt($this->module." - getItemList");
			$ObjmenuitemDao = new \database\menu_itemdao();
			$res = $ObjmenuitemDao->getItemList($data);
			return $res;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - getItemList - ".$e);
			return false; 
		}
		
	}

    public function getRelatedItems($data)
    {
        try
        {
            $this->log->logIt($this->module." - getRelatedItems");
            global $twig;
            global $commonurl;
            $this->loadLang();
            $flag = 0;
            if(!isset($data) || !isset($data['id']) || !isset($data['module']))
            {
                $flag==1;
                $Obj="";
            }else{
                $ObjItemDao = new \database\menu_itemdao();
                $res = $ObjItemDao->getrelateditems($data);
                $bj = json_decode($res,1);
                $Obj = $bj['Data'];
            }
            $template = $twig->loadTemplate('dependency.html');
            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['flag'] = $flag;
            $senderarr['rec'] = $Obj;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $ret = array('data'=>$template->render($senderarr));
            return json_encode($ret);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getRelatedItems - ".$e);
            return false;
        }
    }

    public function geItemByType($data)
    {
        try
        {
            $this->log->logIt($this->module." - geItemByType");
            $ObjmenuitemDao = new \database\menu_itemdao();
            $res = $ObjmenuitemDao->geItemByType($data);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - geItemByType - ".$e);
            return false;
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_menu_item;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }

    function syncitem($dataa)
    {
        try
        {
            $this->log->logIt($this->module.'-syncitem');
            $modulename = $this->module;

            $location = \database\parameter::getparameter('tabinsta_locationid',CONFIG_CID,CONFIG_LID);

            $ObjDao = new \database\apisettingsdao();
            $data = $ObjDao->loadCompanyInfo();
            $data_arr = json_decode($data,true);
            $tabinsta_app_id = $data_arr['Data']['company_info']['tabinsta_app_id'];
            $tabinsta_app_secret = $data_arr['Data']['company_info']['tabinsta_app_secret'];

            $arr = array(
                "location_id"=>$location,
                "menu_id" => $dataa['menulist'],
                "category_id"=>$dataa['category']
            );
            $arr_str = json_encode($arr);

            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,CONFIG_ITEMSYNC);
            curl_setopt($ch,CURLOPT_HTTPHEADER,array(
                "Content-Type:application/json",
                "app-id:$tabinsta_app_id",
                "app-secret:$tabinsta_app_secret",
            ));
            curl_setopt($ch,CURLOPT_POST,1);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$arr_str);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            $res = curl_exec($ch);
            curl_close($ch);
            $data = trim($res);
            $result = json_decode($data,true);

            $listobj = new \database\synctabinstadao();
            $finaldata = $listobj->updateitemfromtabinsta($result,$arr,$modulename);
            return $finaldata;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-syncitem -'.$e);
        }
    }

}

?>