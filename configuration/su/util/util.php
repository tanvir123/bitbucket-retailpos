<?php
namespace util;
class util
{
	public $log;
	public $module = "Util";
	
	function __construct()
    {
        $this->log = new \util\logger();
    }
	
	function date_diffcal($dat1,$dat2)
	{
	  $tmp_dat1 = mktime(0,0,0,
		 substr($dat1,5,2),substr($dat1,8,2),substr($dat1,0,4));
	  $tmp_dat2 = mktime(0,0,0,
		 substr($dat2,5,2),substr($dat2,8,2),substr($dat2,0,4));
	
	  $yeardiff = date('Y',$tmp_dat1)-date('Y',$tmp_dat2);
	  $diff = date('z',$tmp_dat1)-date('z',$tmp_dat2) + 
			   floor($yeardiff /4)*1461;
	
	  for ($yeardiff = $yeardiff % 4; $yeardiff>0; $yeardiff--)
	   {
		 $diff += 365 + date('L',
			 mktime(0,0,0,1,1,
			   intval(
				 substr(
				   (($tmp_dat1>$tmp_dat2) ? $dat1 : $dat2),0,4))
			   -$yeardiff+1));
	   }
	
	  return $diff;
	} 
	
	public function getDateFormatWise($date='',$dateformat='')
	{
		switch ($dateformat) {
		case 'dd-mm-yy':
			list($checkin_day,$checkin_month,$checkin_year)=explode("-",$date);
			break;
		case 'mm-dd-yy':
			list($checkin_month,$checkin_day,$checkin_year)=explode("-",$date);
			break;
		case 'yy-mm-dd':
			list($checkin_year,$checkin_month,$checkin_day)=explode("-",$date);
			break;
		case 'yy-dd-mm':
			list($checkin_year,$checkin_day,$checkin_month)=explode("-",$date);
			break;
		case 'mm-yy-dd':
			list($checkin_month,$checkin_year,$checkin_day)=explode("-",$date);
			break;
		case 'dd-yy-mm':
			list($checkin_day,$checkin_year,$checkin_month)=explode("-",$date);
			break;			
		case 'd MM yy':
			list($checkin_day,$month,$checkin_year)=explode(" ",$date);
			for($i=1;$i<=12;$i++){
			if(date("F", mktime(0, 0, 0, $i, 1, 0)) == $month){
				$month = (strlen($i)==1)?"0".$i:$i;
				break;
				}
			}
			$checkin_month=$month; 
			break;
		case 'd MM,yy':
			$day=explode(" ",$date);
			$checkin_day=$day[0];
			$month=explode(",",$day[1]);			
			$year=explode(",",$date);			
			$checkin_year=$year[1];			
			for($i=1;$i<=12;$i++){
			if(date("F", mktime(0, 0, 0, $i, 1, 0)) == $month[0]){
				$checkin_month = (strlen($i)==1)?"0".$i:$i;
				break;
				}
			} 
			break;
		}		
		$in_date1=mktime(0,0,0,$checkin_month,$checkin_day,$checkin_year);
		return $in_date1;
	}
	
	public function Config_Format_Date($date_val,$dateformat)
	{
			$date_list=date($dateformat, strtotime($date_val));
			return $date_list;
	}

	public function Config_Format_Time($Time_val,$timeformat)
	{
			$date_list=date($timeformat, strtotime($Time_val));
			return $date_list;
	}
	
	public function getNights($checkindate='',$checkoutdate)
	{
		list($checkin_year,$checkin_month,$checkin_day)=explode("-",$checkindate);
		list($checkout_year,$checkout_month,$checkout_day)=explode("-",$checkoutdate);		
		$out_date1=mktime(0,0,0,$checkout_month,$checkout_day,$checkout_year);
		$in_date1=mktime(0,0,0,$checkin_month,$checkin_day,$checkin_year);
		$nights = abs(($out_date1 - $in_date1) / (3600 * 24));
		return $nights;
	}
	
	public function generateDateRange($strDateFrom='',$strDateTo='')
	{
		$aryRange=array();
		$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));
						
	    if ($iDateTo>=$iDateFrom) {
			array_push($aryRange,date('Y-m-d',$iDateFrom)); 
					
			while ($iDateFrom<$iDateTo) {
			  $iDateFrom+=86400; // add 24 hours
			  array_push($aryRange,date('Y-m-d',$iDateFrom));
			  }
		}//if
		return $aryRange;
	}
	
	public function getFormattedNumber($value)
	{
		return number_format($value, $_SESSION[$_SESSION['prefix']]['digits_after_decimal']);
	}
	
	public function getSemiFormattedNumber($value)
	{
		return number_format($value, $_SESSION[$_SESSION['prefix']]['digits_after_decimal']);
	}
	
	public function convertMySqlTime($date)
	{
		$timeformat = \database\parameter::getParameter('timeformat');
		$date=trim($date);
		if(strlen($date)==0) return '';
		if(strlen($date)>10)
			$dt = date_create_from_format('Y-m-d H:i:s',$date);
		else
			$dt = date_create_from_format('H:i:s',$date);
		return $dt->format($timeformat);
	}
	
	function convert_line_breaks($string, $line_break=PHP_EOL) {
		$patterns = array(   
				    "/(<br>|<br \/>|<br\/>)\s*/i",
				    "/(\r\n|\r|\n)/"
		);
		    $replacements = array(   
					PHP_EOL,
					$line_break
		);
		$string = preg_replace($patterns, $replacements, $string);
		return $string;
	}
	
	public function in_arrayr( $needle, $haystack ) {
	$d='';$t=0;
		$cntsp=0;
	 	foreach( $haystack as $v ){
				foreach($v as $k=>$val)
				{
					if($k=='applyon_rateplan' && $val==$needle)
					{
						echo $needle."-".$cntsp."\n";						
						return $needle."-".$cntsp;
						$t++;
						break;
					}	
				}
				if($t>0)
					break;	
			$cntsp++;
		}
	}
	
	public function in_array_booked( $needle, $haystack) {
		$log=new logger();
		#$log->LogIt("needle ".$needle);
		#$log->LogIt($haystack);
		if(!empty($haystack))
		{
			if(in_array($needle,$haystack))	
			{
				#$log->LogIt($needle."  match.... ");
				#$log->LogIt($haystack);
				return 1;				
			}
		}
		else
			return 0;
	}
	
	function hmac_sha1($key, $data)
	{
		// Adjust key to exactly 64 bytes
		if (strlen($key) > 64) {
			$key = str_pad(sha1($key, true), 64, chr(0));
		}
		if (strlen($key) < 64) {
			$key = str_pad($key, 64, chr(0));
		}
	
		// Outter and Inner pad
		$opad = str_repeat(chr(0x5C), 64);
		$ipad = str_repeat(chr(0x36), 64);
	
		// Xor key with opad & ipad
		for ($i = 0; $i < strlen($key); $i++) {
			$opad[$i] = $opad[$i] ^ $key[$i];
			$ipad[$i] = $ipad[$i] ^ $key[$i];
		}
	
		return sha1($opad.sha1($ipad.$data, true));
	}
	
	function fillArrayCombo($arrName, $selected='')
	{
		$strHTML = "";
		reset($arrName);
		while(list($key,$val) = each($arrName))
		{ 
			$strHTML .= "<option value=\"". $key. "\"";
			if($selected == $key)
				$strHTML .= " selected ";
			$strHTML .= ">".$val. "</option>";
		}
		return $strHTML;
	}
	
	public function convertDateToMySql($date)
	{
		$dateformat = \database\parameter::getParameter('dateformat');
		$datetimeformat = $dateformat.' '.\database\parameter::getParameter('timeformat');
		$date=trim($date);
		if(strlen($date)==0) return '';
		if(strlen($date)>10)
			$dt = date_create_from_format($datetimeformat,$date);
		else
			$dt = date_create_from_format($dateformat,$date);
		return $dt->format('Y-m-d');
	}
	
	public function getmysqlnplusminusdate($date,$number)
	{
		$date=trim($date);
		if(strlen($date)==0) return '';
		if(strlen($date)>10)
			$stop_date = date('Y-m-d H:i:s', strtotime($date . ' '.$number.' day'));
		else
			$stop_date = date('Y-m-d', strtotime($date . ' '.$number.' day'));
		return $stop_date;
	}
	
	public function DateDiff($d1,$d2)
	{
		$interval=date_diff(date_create(date('Y-m-d',strtotime($d1))),date_create(date('Y-m-d',strtotime($d2))));
		return $interval->format("%a");
	}

	public function getLocalDateTime($companyid='',$locationid='',$dbname='')
	{
		$timezone = \database\parameter::getParameter('timezone',$companyid,$locationid,$dbname);
		$timediff=explode(":",$timezone);
		$localdatetime = gmdate("Y-m-d H:i:s",mktime(date("H")+($timediff[0]),date("i")+($timediff[1]),date("s"),date("n"),date("j"),date("Y")));
		return $localdatetime;
	}
	public function getLocalDate()
	{
		$timezone = \database\parameter::getParameter('timezone');
		$timediff=explode(":",$timezone);
		$localdatetime = gmdate("Y-m-d",mktime(date("H")+($timediff[0]),date("i")+($timediff[1]),date("s"),date("n"),date("j"),date("Y")));
		return $localdatetime;
	}
	public function getYearCode()
	{
		$timezone = \database\parameter::getParameter('timezone');
		$timediff=explode(":",$timezone);
		$localdatetime = gmdate("Y-m-d",mktime(date("H")+($timediff[0]),date("i")+($timediff[1]),date("s"),date("n"),date("j"),date("Y")));
		$date = \DateTime::createFromFormat("Y-m-d", $localdatetime);
		$year = $date->format("Y");
		return $year;
	}
	public function getLocalTime()
	{
		$timezone = \database\parameter::getParameter('timezone');
		$timediff=explode(":",$timezone);
		$localdatetime = gmdate("H:i:s",mktime(date("H")+($timediff[0]),date("i")+($timediff[1]),date("s"),date("n"),date("j"),date("Y")));
		return $localdatetime;
	}
	
	#For ipay88 payment
	function iPay88_signature($source) 
	{ 
	  $hexSource=sha1($source);
	  $strlen = strlen($hexSource); 
	  $bin ='';
	  for($i=0;$i<strlen($hexSource);$i=$i+2) 
	  { 
		$bin.=chr(hexdec(substr($hexSource,$i,2))); 
	  }	  
	  return base64_encode($bin);  
	} 


	function strip_html_tags( $text )
	{
		// PHP's strip_tags() function will remove tags, but it
		// doesn't remove scripts, styles, and other unwanted
		// invisible text between tags.  Also, as a prelude to
		// tokenizing the text, we need to insure that when
		// block-level tags (such as <p> or <div>) are removed,
		// neighboring words aren't joined.
		$text = preg_replace(
			array(
				// Remove invisible content
				'@<head[^>]*?>.*?</head>@siu',
				'@<style[^>]*?>.*?</style>@siu',
				'@<script[^>]*?.*?</script>@siu',
				'@<object[^>]*?.*?</object>@siu',
				'@<embed[^>]*?.*?</embed>@siu',
				'@<applet[^>]*?.*?</applet>@siu',
				'@<noframes[^>]*?.*?</noframes>@siu',
				'@<noscript[^>]*?.*?</noscript>@siu',
				'@<noembed[^>]*?.*?</noembed>@siu',
	
				// Add line breaks before & after blocks
				'@<((br)|(hr))@iu',
				'@</?((address)|(blockquote)|(center)|(del))@iu',
				'@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
				'@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
				'@</?((table)|(th)|(td)|(caption))@iu',
				'@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
				'@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
				'@</?((frameset)|(frame)|(iframe))@iu',
			),
			array(
				' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
				"\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
				"\n\$0", "\n\$0",
			),$text );
	
		// Remove all remaining tags and comments and return.
		return strip_tags($text);
	}
	
	function imageResize($width, $height, $target, $flag=1) {
	//takes the larger size of the width and height and applies the
	//formula accordingly...this is so this script will work
	//dynamically with any size image
	if($width > $height) {
	$percentage = ($target / $width);
	} else {
	$percentage = ($target / $height);
	}
	//gets the new value and applies the percentage, then rounds the value
	$width = round($width * $percentage);
	$height = round($height * $percentage);
	//returns the new sizes in html image tag format...this is so you
	//can plug this function inside an image tag and just get the
	if($flag==1)
		return "width=\"$width\" height=\"$height\"";
	else
		return "width=\"$width\"";	
	}
	
	function getmonth ($month = null, $year = null)
	{		
		  #The current month is used if none is supplied.
		  if (is_null($month))
			  $month = date('n');
	
		  #The current year is used if none is supplied.
		  if (is_null($year))
			  $year = date('Y');
	
		  #Verifying if the month exist
		  if (!checkdate($month, 1, $year))
			  return null;
	
		  #Calculating the days of the month
		  $first_of_month = mktime(0, 0, 0, $month, 1, $year);
		  $days_in_month = date('t', $first_of_month);
		  $last_of_month = mktime(0, 0, 0, $month, $days_in_month, $year);
	
		  $m = array();
		  $m['first_mday'] = 1;
		  $m['first_wday'] = date('w', $first_of_month);
		  $m['first_weekday'] = strftime('%A', $first_of_month);
		  $m['first_yday'] = date('z', $first_of_month);
		  $m['first_week'] = date('W', $first_of_month);
		  $m['last_mday'] = $days_in_month;
		  $m['last_wday'] = date('w', $last_of_month);
		  $m['last_weekday'] = strftime('%A', $last_of_month);
		  $m['last_yday'] = date('z', $last_of_month);
		  $m['last_week'] = date('W', $last_of_month);
		  $m['mon'] = $month;
		  $m['month'] = strftime('%B', $first_of_month);
		  $m['year'] = $year;
	
		  return $m;		
	}
	
	
	function addDaysinDate($date,$days)
	{
		list($year,$month,$day)=explode("-",$date);
		$resdate=date('Y-m-d',mktime(0,0,0,$month,$day+($days),$year));
		return $resdate;	
	}
	function removeDaysinDate($date,$days)
	{
		list($year,$month,$day)=explode("-",$date);
		$resdate=date('Y-m-d',mktime(0,0,0,$month,$day-($days),$year));
		return $resdate;	
	}
	
	function resize($img, $w, $h, $newfilename) 
	{
		 #Check if GD extension is loaded
		 if (!extension_loaded('gd') && !extension_loaded('gd2')) {
			trigger_error("GD is not loaded", E_USER_WARNING);
			return false;
		 }
		 
		 #Get Image size info
		 $imgInfo = getimagesize($img);
		 switch ($imgInfo[2]) {
			case 1: $im = imagecreatefromgif($img); break;
			case 2: $im = imagecreatefromjpeg($img);  break;
			case 3: $im = imagecreatefrompng($img); break;
			default:  trigger_error('Unsupported filetype!', E_USER_WARNING);  break;
		 }
		 
		 #If image dimension is smaller, do not resize
		 if ($imgInfo[0] <= $w && $imgInfo[1] <= $h) {
			$nHeight = $imgInfo[1];
			$nWidth = $imgInfo[0];
		 }else{
		  if ($w/$imgInfo[0] > $h/$imgInfo[1]) {
			$nWidth = $w;
			$nHeight = $imgInfo[1]*($w/$imgInfo[0]);
		  }else{
			$nWidth = $imgInfo[0]*($h/$imgInfo[1]);
			$nHeight = $h;
		  }
		 }
		 
		 $nWidth = round($nWidth);
		 $nHeight = round($nHeight);
		 $newImg = imagecreatetruecolor($nWidth, $nHeight);
		 
		 /* Check if this image is PNG or GIF, then set if Transparent*/  
		 if(($imgInfo[2] == 1) OR ($imgInfo[2]==3)){
			  imagealphablending($newImg, true);
			  imagesavealpha($newImg,false);	
			  $transparent = imagecolorallocate($newImg, 255, 255, 255);		 
			  imagefill($newImg,0,0,$transparent);
		 }
		 
		 imagecopyresampled($newImg, $im, 0, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);
		 
		 #Generate the file, and rename it to $newfilename
		 switch ($imgInfo[2]) {
			case 1: imagegif($newImg,$newfilename); break;
			case 2: imagejpeg($newImg,$newfilename);  break;
			case 3: imagepng($newImg,$newfilename); break;
			default:  trigger_error('Failed resize image!', E_USER_WARNING);  break;
		 }
	  return $newfilename;
	}
	
	public function VisitorIP()
	{ 
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$TheIp=$_SERVER['HTTP_X_FORWARDED_FOR'];
		else 
			$TheIp=$_SERVER['REMOTE_ADDR'];

		$iparr = explode(',',$TheIp);
		
		if(count($iparr)>0)
		{
			$TheIp=trim($iparr[0]);
		}
		
		return trim($TheIp);
	}
	
	public function VisitorProxyIP()
	{ 
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$TheIp=$_SERVER['HTTP_X_FORWARDED_FOR'];
		else 
			$TheIp=$_SERVER['REMOTE_ADDR'];

		return trim($TheIp);
	}
	
	public function getLocationFromIP($ip)
	{
		try
		{
			$result = array();
			$ip2location_data = ip2location_finder($ip);
			if(is_array($ip2location_data) && !empty($ip2location_data))
			{
				if($ip2location_data['RESPONSE'] == 'TIMEOUT')
				{
					return array();
				}
				else
				{
					$result['country'] = $ip2location_data['countryname'];
					$result['city'] = $ip2location_data['city'];
				}
			}
			else
			{
				return array();
			}
		}
		catch(Exception $e)
		{
			return array();
		}
		return $result;
	}
	
	public function convertMySqlDate($date)
	{
		$date=trim($date);
		$dateformat = \database\parameter::getParameter('dateformat');
		if(strlen($date)==0) return '';
		if(strlen($date)>10)
			$dt = date_create_from_format('Y-m-d H:i:s',$date);
		else
			$dt = date_create_from_format('Y-m-d',$date);
		return $dt->format($dateformat);
	}
		
	
	function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
	}
	
	function AllowOnlyNumeric($string)
	{
		if($string!='')
		{
			if (preg_match('/^[0-9]*$/', $string)) {
				return true;
			} else {
				return false;
			}	
		}
		//return false;
	}
	
	function replace_between($str, $needle_start, $needle_end, $replacement) {
		$pos = strpos($str, $needle_start);
		$start = $pos === false ? 0 : $pos + strlen($needle_start);
	    
		$pos = strpos($str, $needle_end, $start);
		$end = $start === false ? strlen($str) : $pos;
	     
		return substr_replace($str,$replacement,  $start, $end - $start);
	}
	
	function removeptag($string)
	{
		$tags_to_strip = Array("p");
		$replace_with="";
		foreach ($tags_to_strip as $tag)
		{
		    $string = preg_replace("/<\\/?" . $tag . "(.|\\s)*?>/",$replace_with,$string);
		
		}
		return $string;
	}
	
	function removeBrtag($string)
	{
		$tags_to_strip = Array("br");
		$replace_with="";
		foreach ($tags_to_strip as $tag)
		{
		    $string = preg_replace("/<\\/?" . $tag . "(.|\\s)*?>/",$replace_with,$string);
		
		}
		return $string;
	}
	function convertTimeToMySql($date)
	{
		$timeformat = \database\parameter::getParameter('timeformat');
		$datetimeformat = \database\parameter::getParameter('dateformat').' '.$timeformat;
		$date=trim($date);
		if(strlen($date)==0) return '';
		if(strlen($date)>10)
			$dt = date_create_from_format($datetimeformat,$date);
		else
			$dt = date_create_from_format($timeformat,$date);
        return $dt->format('H:i:s');
	}
	
	public function smart_resize_image($file,$string=null,$width=0,$height = 0, $proportional = false, $output = 'file', $delete_original = true, $use_linux_commands = false, $quality  = 100,$grayscale = false)
	{
			if ( $height <= 0 && $width <= 0 ) return false;
			if ( $file === null && $string === null ) return false;
			# Setting defaults and meta
			$info                         = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
			$image                        = '';
			$final_width                  = 0;
			$final_height                 = 0;
			list($width_old, $height_old) = $info;
				$cropHeight = $cropWidth = 0;
			# Calculating proportionality
			if ($proportional) {
			  if      ($width  == 0)  $factor = $height/$height_old;
			  elseif  ($height == 0)  $factor = $width/$width_old;
			  else                    $factor = min( $width / $width_old, $height / $height_old );
			  $final_width  = round( $width_old * $factor );
			  $final_height = round( $height_old * $factor );
			}
			else {
			  $final_width = ( $width <= 0 ) ? $width_old : $width;
			  $final_height = ( $height <= 0 ) ? $height_old : $height;
				  $widthX = $width_old / $width;
				  $heightX = $height_old / $height;
				  
				  $x = min($widthX, $heightX);
				  $cropWidth = ($width_old - $width * $x) / 2;
				  $cropHeight = ($height_old - $height * $x) / 2;
			}
			# Loading image to memory according to type
			switch ( $info[2] ) {
			  case IMAGETYPE_JPEG:  $file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);  break;
			  case IMAGETYPE_GIF:   $file !== null ? $image = imagecreatefromgif($file)  : $image = imagecreatefromstring($string);  break;
			  case IMAGETYPE_PNG:   $file !== null ? $image = imagecreatefrompng($file)  : $image = imagecreatefromstring($string);  break;
			  default: return false;
			}
			
			# Making the image grayscale, if needed
			if ($grayscale) {
			  imagefilter($image, IMG_FILTER_GRAYSCALE);
			}    
			
			# This is the resizing/resampling/transparency-preserving magic
			$image_resized = imagecreatetruecolor( $final_width, $final_height );
			if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
			  $transparency = imagecolortransparent($image);
			  $palletsize = imagecolorstotal($image);
			  if ($transparency >= 0 && $transparency < $palletsize) {
				$transparent_color  = imagecolorsforindex($image, $transparency);
				$transparency       = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
				imagefill($image_resized, 0, 0, $transparency);
				imagecolortransparent($image_resized, $transparency);
			  }
			  elseif ($info[2] == IMAGETYPE_PNG) {
				imagealphablending($image_resized, false);
				$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
				imagefill($image_resized, 0, 0, $color);
				imagesavealpha($image_resized, true);
			  }
			}
			imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);
				
				
			# Taking care of original, if needed
			if ( $delete_original ) {
			  if ( $use_linux_commands ) exec('rm '.$file);
			  else @unlink($file);
			}
			# Preparing a method of providing result
			switch ( strtolower($output) ) {
			  case 'browser':
				$mime = image_type_to_mime_type($info[2]);
				header("Content-type: $mime");
				$output = NULL;
			  break;
			  case 'file':
				$output = $file;
			  break;
			  case 'return':
				return $image_resized;
			  break;
			  default:
			  break;
			}
			
			# Writing image according to type to the output destination and image quality
			switch ( $info[2] ) {
			  case IMAGETYPE_GIF:   imagegif($image_resized, $output);    break;
			  case IMAGETYPE_JPEG:  imagejpeg($image_resized, $output, $quality);   break;
			  case IMAGETYPE_PNG:
				$quality = 9 - (int)((0.9*$quality)/10.0);
				imagepng($image_resized, $output, $quality);
				break;
			  default: return false;
			}
			return true;
	}
	function cleanVariables($array) {
        $flat = array();
        foreach($array as $k=>$value) {
            if (is_array($value)) {
				   $flat[$k] = self::cleanVariables($value);
            }
            else {
                $flat[$k] = trim(htmlentities(urldecode($value), ENT_QUOTES, 'UTF-8'));
            }
        }
        return $flat;
    }
	public static function input()
	{
        $log = new logger();
		try
		{
            $log->logIt("in input >> ");
            $requestData = file_get_contents("php://input");
            $log->logIt("input parameter >> ".print_r($requestData,true));
            $header = getallheaders();
			$request = (array) json_decode($requestData);
			$final_arr = array("headers"=>$header,"request"=>$request);
			header('Content-Type:application/json');
            return json_encode($final_arr);
		}
		catch(Exception $e)
		{
            $log->logIt("Error in input >> ".$e);
		}
    }

    public static function response($status=200,$message,$response)
	{
        header('Content-Type: application/json');
        header('x', true, $status);
        $respon_arr = array("message"=>$message,"data"=>$response);
		return json_encode($respon_arr);
	}
	public static function gethash($companyid="")
	{
		return $companyid.'00'.md5(uniqid(date("Y-m-d H:i:s").'-'.rand(), true));
	}
	public static function getoneDarray($arr,$field)
	{
		$arr1 = array();
		foreach($arr AS $val)
		{
			array_push($arr1,$val["".$field.""]);
		}
		return $arr1;
	}
	function random_password( $length = 8 ) {
		$chars = "daisy_bhavsar_123456789_741258963";
		$password = substr( str_shuffle( $chars ), 0, $length );
		return $password;
	}
	public static function getkeyfieldarray($arr,$field)
	{
		$arr1 = array();
		foreach($arr AS $val)
		{
			if(isset($val[$field]))
			{
				$arr1[$val[$field]] = $val;
			}
		}
		return $arr1;
	}
    public static function generateauthkey()
    {
        $key = 'pureecho%123#';
        return '00'.md5(uniqid(date("Y-m-d H:i:s").'-'.rand().'-'.$key, true));
    }

	public static function str_slug($title,$separator = '-')
	{
		$title = self::ascii($title);
		
		// Convert all dashes/underscores into separator
		$flip = $separator == '-' ? '_' : '-';
		
		$title = preg_replace('![' . preg_quote($flip) . ']+!u',$separator,$title);
		
		// Remove all characters that are not the separator, letters, numbers, or whitespace.
		$title = preg_replace('![^' . preg_quote($separator) . '\pL\pN\s]+!u','',mb_strtolower($title));
		
		// Replace all separator characters and whitespace by a single separator
		$title = preg_replace('![' . preg_quote($separator) . '\s]+!u',$separator,$title);
		
		return trim($title,$separator);
	}
	
	public static function ascii($value)
	{
		$charArray = self::charsArray();
		if(count($charArray)>0)
		{
			foreach ($charArray as $key => $val) {
				$value = str_replace($val,$key,$value);
			}
		}
		
		return preg_replace('/[^\x20-\x7E]/u','',$value);
	}
	
	public static function charsArray()
	{
		static $charsArray;
		
		if (isset($charsArray)) {
			return $charsArray;
		}
		
		return $charsArray = [
			'0'    => ['°','₀','۰'],
			'1'    => ['¹','₁','۱'],
			'2'    => ['²','₂','۲'],
			'3'    => ['³','₃','۳'],
			'4'    => ['⁴','₄','۴','٤'],
			'5'    => ['⁵','₅','۵','٥'],
			'6'    => ['⁶','₆','۶','٦'],
			'7'    => ['⁷','₇','۷'],
			'8'    => ['⁸','₈','۸'],
			'9'    => ['⁹','₉','۹'],
			'a'    => ['à','á','ả','ã','ạ','ă','ắ','ằ','ẳ','ẵ','ặ','â','ấ','ầ','ẩ','ẫ','ậ','ā','ą','å','α','ά','ἀ','ἁ','ἂ','ἃ','ἄ','ἅ','ἆ','ἇ','ᾀ','ᾁ','ᾂ','ᾃ','ᾄ','ᾅ','ᾆ','ᾇ','ὰ','ά','ᾰ','ᾱ','ᾲ','ᾳ','ᾴ','ᾶ','ᾷ','а','أ','အ','ာ','ါ','ǻ','ǎ','ª','ა','अ','ا'],
			'b'    => ['б','β','Ъ','Ь','ب','ဗ','ბ'],
			'c'    => ['ç','ć','č','ĉ','ċ'],
			'd'    => ['ď','ð','đ','ƌ','ȡ','ɖ','ɗ','ᵭ','ᶁ','ᶑ','д','δ','د','ض','ဍ','ဒ','დ'],
			'e'    => ['é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ','ë','ē','ę','ě','ĕ','ė','ε','έ','ἐ','ἑ','ἒ','ἓ','ἔ','ἕ','ὲ','έ','е','ё','э','є','ə','ဧ','ေ','ဲ','ე','ए','إ','ئ'],
			'f'    => ['ф','φ','ف','ƒ','ფ'],
			'g'    => ['ĝ','ğ','ġ','ģ','г','ґ','γ','ဂ','გ','گ'],
			'h'    => ['ĥ','ħ','η','ή','ح','ه','ဟ','ှ','ჰ'],
			'i'    => ['í','ì','ỉ','ĩ','ị','î','ï','ī','ĭ','į','ı','ι','ί','ϊ','ΐ','ἰ','ἱ','ἲ','ἳ','ἴ','ἵ','ἶ','ἷ','ὶ','ί','ῐ','ῑ','ῒ','ΐ','ῖ','ῗ','і','ї','и','ဣ','ိ','ီ','ည်','ǐ','ი','इ'],
			'j'    => ['ĵ','ј','Ј','ჯ','ج'],
			'k'    => ['ķ','ĸ','к','κ','Ķ','ق','ك','က','კ','ქ','ک'],
			'l'    => ['ł','ľ','ĺ','ļ','ŀ','л','λ','ل','လ','ლ'],
			'm'    => ['м','μ','م','မ','მ'],
			'n'    => ['ñ','ń','ň','ņ','ŉ','ŋ','ν','н','ن','န','ნ'],
			'o'    => ['ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ','ø','ō','ő','ŏ','ο','ὀ','ὁ','ὂ','ὃ','ὄ','ὅ','ὸ','ό','о','و','θ','ို','ǒ','ǿ','º','ო','ओ'],
			'p'    => ['п','π','ပ','პ','پ'],
			'q'    => ['ყ'],
			'r'    => ['ŕ','ř','ŗ','р','ρ','ر','რ'],
			's'    => ['ś','š','ş','с','σ','ș','ς','س','ص','စ','ſ','ს'],
			't'    => ['ť','ţ','т','τ','ț','ت','ط','ဋ','တ','ŧ','თ','ტ'],
			'u'    => ['ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự','û','ū','ů','ű','ŭ','ų','µ','у','ဉ','ု','ူ','ǔ','ǖ','ǘ','ǚ','ǜ','უ','उ'],
			'v'    => ['в','ვ','ϐ'],
			'w'    => ['ŵ','ω','ώ','ဝ','ွ'],
			'x'    => ['χ','ξ'],
			'y'    => ['ý','ỳ','ỷ','ỹ','ỵ','ÿ','ŷ','й','ы','υ','ϋ','ύ','ΰ','ي','ယ'],
			'z'    => ['ź','ž','ż','з','ζ','ز','ဇ','ზ'],
			'aa'   => ['ع','आ','آ'],
			'ae'   => ['ä','æ','ǽ'],
			'ai'   => ['ऐ'],
			'at'   => ['@'],
			'ch'   => ['ч','ჩ','ჭ','چ'],
			'dj'   => ['ђ','đ'],
			'dz'   => ['џ','ძ'],
			'ei'   => ['ऍ'],
			'gh'   => ['غ','ღ'],
			'ii'   => ['ई'],
			'ij'   => ['ĳ'],
			'kh'   => ['х','خ','ხ'],
			'lj'   => ['љ'],
			'nj'   => ['њ'],
			'oe'   => ['ö','œ','ؤ'],
			'oi'   => ['ऑ'],
			'oii'  => ['ऒ'],
			'ps'   => ['ψ'],
			'sh'   => ['ш','შ','ش'],
			'shch' => ['щ'],
			'ss'   => ['ß'],
			'sx'   => ['ŝ'],
			'th'   => ['þ','ϑ','ث','ذ','ظ'],
			'ts'   => ['ц','ც','წ'],
			'ue'   => ['ü'],
			'uu'   => ['ऊ'],
			'ya'   => ['я'],
			'yu'   => ['ю'],
			'zh'   => ['ж','ჟ','ژ'],
			'(c)'  => ['©'],
			'A'    => ['Á','À','Ả','Ã','Ạ','Ă','Ắ','Ằ','Ẳ','Ẵ','Ặ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ','Å','Ā','Ą','Α','Ά','Ἀ','Ἁ','Ἂ','Ἃ','Ἄ','Ἅ','Ἆ','Ἇ','ᾈ','ᾉ','ᾊ','ᾋ','ᾌ','ᾍ','ᾎ','ᾏ','Ᾰ','Ᾱ','Ὰ','Ά','ᾼ','А','Ǻ','Ǎ'],
			'B'    => ['Б','Β','ब'],
			'C'    => ['Ç','Ć','Č','Ĉ','Ċ'],
			'D'    => ['Ď','Ð','Đ','Ɖ','Ɗ','Ƌ','ᴅ','ᴆ','Д','Δ'],
			'E'    => ['É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ','Ë','Ē','Ę','Ě','Ĕ','Ė','Ε','Έ','Ἐ','Ἑ','Ἒ','Ἓ','Ἔ','Ἕ','Έ','Ὲ','Е','Ё','Э','Є','Ə'],
			'F'    => ['Ф','Φ'],
			'G'    => ['Ğ','Ġ','Ģ','Г','Ґ','Γ'],
			'H'    => ['Η','Ή','Ħ'],
			'I'    => ['Í','Ì','Ỉ','Ĩ','Ị','Î','Ï','Ī','Ĭ','Į','İ','Ι','Ί','Ϊ','Ἰ','Ἱ','Ἳ','Ἴ','Ἵ','Ἶ','Ἷ','Ῐ','Ῑ','Ὶ','Ί','И','І','Ї','Ǐ','ϒ'],
			'K'    => ['К','Κ'],
			'L'    => ['Ĺ','Ł','Л','Λ','Ļ','Ľ','Ŀ','ल'],
			'M'    => ['М','Μ'],
			'N'    => ['Ń','Ñ','Ň','Ņ','Ŋ','Н','Ν'],
			'O'    => ['Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ','Ø','Ō','Ő','Ŏ','Ο','Ό','Ὀ','Ὁ','Ὂ','Ὃ','Ὄ','Ὅ','Ὸ','Ό','О','Θ','Ө','Ǒ','Ǿ'],
			'P'    => ['П','Π'],
			'R'    => ['Ř','Ŕ','Р','Ρ','Ŗ'],
			'S'    => ['Ş','Ŝ','Ș','Š','Ś','С','Σ'],
			'T'    => ['Ť','Ţ','Ŧ','Ț','Т','Τ'],
			'U'    => ['Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự','Û','Ū','Ů','Ű','Ŭ','Ų','У','Ǔ','Ǖ','Ǘ','Ǚ','Ǜ'],
			'V'    => ['В'],
			'W'    => ['Ω','Ώ','Ŵ'],
			'X'    => ['Χ','Ξ'],
			'Y'    => ['Ý','Ỳ','Ỷ','Ỹ','Ỵ','Ÿ','Ῠ','Ῡ','Ὺ','Ύ','Ы','Й','Υ','Ϋ','Ŷ'],
			'Z'    => ['Ź','Ž','Ż','З','Ζ'],
			'AE'   => ['Ä','Æ','Ǽ'],
			'CH'   => ['Ч'],
			'DJ'   => ['Ђ'],
			'DZ'   => ['Џ'],
			'GX'   => ['Ĝ'],
			'HX'   => ['Ĥ'],
			'IJ'   => ['Ĳ'],
			'JX'   => ['Ĵ'],
			'KH'   => ['Х'],
			'LJ'   => ['Љ'],
			'NJ'   => ['Њ'],
			'OE'   => ['Ö','Œ'],
			'PS'   => ['Ψ'],
			'SH'   => ['Ш'],
			'SHCH' => ['Щ'],
			'SS'   => ['ẞ'],
			'TH'   => ['Þ'],
			'TS'   => ['Ц'],
			'UE'   => ['Ü'],
			'YA'   => ['Я'],
			'YU'   => ['Ю'],
			'ZH'   => ['Ж'],
			' '    => ["\xC2\xA0","\xE2\x80\x80","\xE2\x80\x81","\xE2\x80\x82","\xE2\x80\x83","\xE2\x80\x84","\xE2\x80\x85","\xE2\x80\x86","\xE2\x80\x87","\xE2\x80\x88","\xE2\x80\x89","\xE2\x80\x8A","\xE2\x80\xAF","\xE2\x81\x9F","\xE3\x80\x80"],
		];
	}


    public static function getkeyvaluearray($arr,$keyfield,$valfield)
    {
        $arr1 = array();
        foreach($arr AS $val)
        {
            if(isset($val[$keyfield]))
            {
                $arr1[$val[$keyfield]] = $val[$valfield];
            }
        }
        return $arr1;
    }
    public static function unsetNullArrayValue($array)
    {
        foreach ($array as $key => $value) {
            $value = trim($value);
            if (empty($value))
                unset($array[$key]);
        }

        return $array;
    }
}
?>