<?php

class langmagic
{
	public $mainlangconnection;
	public $customlangconnection;	
	public $module;
	public $lang;
	public $msgkey;
	public $msgvalue;
	
	private $log;
	
	public $objLanguage1=array();
	public $objLanguage2=array();
	
	function __construct()
	{
		try
		{
			$this->log=new logger();
			$this->mainlangconnection=$this->doSQLiteConnection(0);
			$this->customlangconnection=$this->doSQLiteConnection($_SESSION[$_SESSION['prefix']]['hotel_code']);	
			
			if(count(staticarray::$languagearray)>0)
			{	
				$str='';	
				foreach(staticarray::$languagearray as $langkey => $langvalue)
				{	
					/*if($langkey!='en')
					{*/	
						if($str=='')
							$str=" `lang_".$langkey."` TEXT,";
						else
							$str.=" `lang_".$langkey."` TEXT,";
					//}
				}	
			}	
			
			#Create Language Table in Main Language Database			
			$this->createTable($this->mainlangconnection,$str);
			
			#Create Language Table in Custom Language Database			
			$this->createTable($this->customlangconnection,$str);
		}
		catch(Exception $e)
		{
			throw $e;
		}
    }
	
	public function loadLanguage($moduleName)
	{
		try
		{	
			$log=new logger();
			#$log->logIt("------------------------------------------------".$moduleName);
			$this->module=$moduleName;
			$result=$this->getModuleMessage($this->mainlangconnection);
			$count=count($result);
			
			if($count>0)
			{
				foreach($result as $lang1_var)
				{
					$this->objLanguage1[$lang1_var['key']] = $lang1_var['value'];	
				}
			}
			
			
			#$log->logIt("------------------------------------------------".$_SESSION[$_SESSION['prefix']]['WebSelectedLanguage']);
			if(array_key_exists('WebSelectedLanguage',$_SESSION[$_SESSION['prefix']]))
			{			
				if(isset($_SESSION[$_SESSION['prefix']]['WebSelectedLanguage']) && $_SESSION[$_SESSION['prefix']]['WebSelectedLanguage']==1)#Flora
				{					
						$result=$this->getModuleMessage($this->customlangconnection,$_SESSION[$_SESSION['prefix']]['WebLanguageCode']);	
				}//if
				else
				{
					$result=$this->getModuleMessage($this->customlangconnection);//if not any language then call data from custom... default is just for display purpose
				}	
			}
			else	
			{					
				$result=$this->getModuleMessage($this->customlangconnection);//if not any language then call data from custom... default is just for display purpose
			}
				
			$count=count($result);				
			
			if($count>0)
			{
				foreach($result as $lang2_var)
				{
					if($lang2_var['value']!='')
						$this->objLanguage2[$lang2_var['key']] = $lang2_var['value'];	
				}	
			}						
		}
		catch(Exception $e)
		{
			$this->log->logIt($e);			
		}
	}
	
	public function getLanguage($moduleName,$msgCode,$default)
	{
		try
		{			
			
			//return $default;			
			if($_SESSION[$_SESSION['prefix']]['language']==1)
			{	
				$this->module=$moduleName;
				$this->lang=$_SESSION[$_SESSION['prefix']]['language'];
				$this->msgkey=$msgCode;
				$this->msgvalue=$default;
					
				if(array_key_exists($msgCode,$this->objLanguage1))
				{
					if($this->objLanguage1[$msgCode]!=$this->msgvalue)
					{
						//$this->log->logIt("------------------------".$moduleName." |".$this->objLanguage1[$msgCode]."|".$this->msgvalue);
						$this->update($this->mainlangconnection);						
						$this->objLanguage1[$msgCode]=$this->msgvalue;
					}					
				}
				else
				{		
					$this->insert($this->mainlangconnection);
					$this->objLanguage1[$msgCode]=$this->msgvalue;													
				}	
				return $this->objLanguage1[$msgCode];
			}	
			else if($_SESSION[$_SESSION['prefix']]['language']==2)
			{
				#$this->log->logIt("------------------------".$moduleName." |".$this->objLanguage1[$msgCode]."|".$this->msgvalue);
				$this->module=$moduleName;
				$this->lang=$_SESSION[$_SESSION['prefix']]['language'];
				$this->msgkey=$msgCode;
				$this->msgvalue=$default;
				
				if(array_key_exists($msgCode,$this->objLanguage1))
				{
					if($this->objLanguage1[$msgCode]!=$this->msgvalue)
					{
						//$this->log->logIt("------------------------".$moduleName." |".$this->objLanguage1[$msgCode]."|".$this->msgvalue);
						$this->update($this->mainlangconnection);						
						$this->objLanguage1[$msgCode]=$this->msgvalue;
					}					
				}			
				
				if(array_key_exists($msgCode,$this->objLanguage2))
				{
					return $this->objLanguage2[$msgCode];
				}
				else 
				{	
					if(array_key_exists($msgCode,$this->objLanguage1))
					{
						if($this->objLanguage1[$msgCode]!=$this->msgvalue)
						{
							//$this->log->logIt("------------------------".$moduleName." |".$this->objLanguage1[$msgCode]."|".$this->msgvalue);
							$this->update($this->mainlangconnection);						
							$this->objLanguage1[$msgCode]=$this->msgvalue;
						}
					}
					else
					{
						$this->insert($this->mainlangconnection);
						$this->objLanguage1[$msgCode]=$this->msgvalue;	
					}
					return $this->objLanguage1[$msgCode];
				}				
			}
			else
			{
				return $default;
			}			
		}
		catch(Exception $e)
		{	
			$this->log->logIt( $e);
			return $default;
		}
	}
	
	public function insert($conn,$lang_field='')
	{
		if($lang_field=='')
			$lang_field='value';
		else
			$lang_field="lang_".$lang_field;
		
		$insert_Sql="INSERT INTO cflanguagevalues (key,`".$lang_field."`,module)";
		$insert_Sql.="VALUES(:key,:msgvalue,:module)";	
		$cmd=$conn->prepare($insert_Sql);
		
		$cmd->bindParam(":key",$this->msgkey,PDO::PARAM_STR);
		$cmd->bindParam(":msgvalue",$this->msgvalue,PDO::PARAM_STR);
		$cmd->bindParam(":module",$this->module,PDO::PARAM_STR);
		
		$result=$cmd->execute();
		return $result;
	}
	
	public function update($conn,$lang_field='')
	{
		if($lang_field=='')
			$lang_field='value';
		else
			$lang_field="lang_".$lang_field;
			
		$update_Sql="UPDATE cflanguagevalues SET `".$lang_field."`=:msgvalue WHERE module=:module AND key=:key";					
		$cmd=$conn->prepare($update_Sql);
		
		$cmd->bindParam(":key",$this->msgkey,PDO::PARAM_STR);
		$cmd->bindParam(":msgvalue",$this->msgvalue,PDO::PARAM_STR);
		$cmd->bindParam(":module",$this->module,PDO::PARAM_STR);
		
		$result=$cmd->execute();
		return $result;
	}
	
	public function getMessageValue($conn,$lang_field='')
	{
		$is_exists_sql="SELECT key,";
		if($lang_field!='')
			$is_exists_sql.="`lang_".$lang_field."` as value FROM cflanguagevalues ";
		else	
			$is_exists_sql.="value FROM cflanguagevalues ";
			
		$is_exists_sql.="WHERE module='".$this->module."' AND key='".$this->msgkey."'";	
		
		$cmd=$conn->prepare($is_exists_sql);
		$cmd->execute();
		$cmd->setFetchMode(PDO::FETCH_ASSOC);
		$result=$cmd->fetchAll();		
		return $result;
	}
	
	public function doSQLiteConnection($hotel_code)
	{
		try
		{		
			global $langconnection;
			global $langPath;
			$langconnection=new PDO("sqlite:".$langPath.'lang_'.$hotel_code.'.db','utf8');	
			$langconnection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		
			return $langconnection;
		}
		catch(PDOException $e)
		{
			$this->log->logIt($e);
		}
	}
	
	public function createTable($conn,$str)
	{				
		$createtable_sql="CREATE TABLE IF NOT EXISTS cflanguagevalues (";	
		$createtable_sql.="`key` VARCHAR( 1000 ),";
		$createtable_sql.="`value` TEXT ,";
		$createtable_sql.="	`module` VARCHAR( 1000 ),";
		$createtable_sql.=$str;
		$createtable_sql.=" PRIMARY KEY (module, key))";
		
			
		$cmd=$conn->prepare($createtable_sql);
		$cmd->execute();
	}
	
	public function getModuleMessage($conn,$lang_field='')
	{
		#$this->log->logIt($lang_field." |".$_SESSION[$_SESSION['prefix']]['WebLanguageCode']);
		$log=new logger();
		$is_exists_sql="SELECT key,";
		if($lang_field!='')
			$is_exists_sql.="`lang_".$lang_field."` as value FROM cflanguagevalues ";
		else	
			$is_exists_sql.="value FROM cflanguagevalues ";
			
		$is_exists_sql.="WHERE module='".$this->module."'";	
		
		$cmd=$conn->prepare($is_exists_sql);
		$cmd->execute();
		$cmd->setFetchMode(PDO::FETCH_ASSOC);
		$result=$cmd->fetchAll();
		
		return $result;
	}	
}#Class
?>
