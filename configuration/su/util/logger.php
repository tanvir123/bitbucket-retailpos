<?php

namespace util;
class logger
{
	private $enable=true;
	private $defaultLogFolder;
	private $defaultLogFile;
	private $defaultLogType;
	private $defaultLogMethod;
	private $defaultLogMailId;
	private $defaultEmailSubject;
	
	public function __construct($logfilename='')
	{
		$logsPath =   dirname(__DIR__).'/logs/';
		$this->defaultLogFolder 	= $logsPath;
		
		if($logfilename=='')
			$this->defaultLogFile 		= 'POS_'.date('Ymd').'.log';
		else
			$this->defaultLogFile 		= $logfilename."_".date('Ymd').'.log';
		
		if(file_exists($this->defaultLogFolder.$this->defaultLogFile))
		{
			if(filesize($this->defaultLogFolder.$this->defaultLogFile)>20000000)
			{
				rename($this->defaultLogFolder.$this->defaultLogFile,$this->defaultLogFolder.date('Ymd_His').$this->defaultLogFile);
			}
		}
		$this->defaultLogType 		= 'INFO';
		$this->defaultLogMethod 	= 'FILE';
		$this->defaultLogMailId 	= 'pureipos365@gmail.com';
		$this->defaultEmailSubject	= 'Notification';
	}
	
	public function logIt($logData, $logType = '', $logMethod = '', $logFile = '', $logMailId = '')
	{
		if(is_array($logData) || is_object($logData)) 
			$finalLogData = print_r($logData, TRUE);
		else
			$finalLogData = $logData;
	
		if(trim($logType) == '')
			$logType = $this->defaultLogType;
		else
			$logType = strtoupper(strtolower($logType));			
		
		if(trim($logMethod) == '')
			$logMethod = $this->defaultLogMethod;
		
		if(trim($logFile) == '')
			$logFile = $this->defaultLogFolder.$this->defaultLogFile;

		if(trim($logMailId) == '')
			$logMailId = $this->defaultLogMailId;		
		
		if(isset($_SESSION['prefix']) && isset($_SESSION[$_SESSION['prefix']]['companyid']))
			$finalLogData = "Company - ".$_SESSION[$_SESSION['prefix']]['companyid']." - ".$_SERVER['SERVER_NAME']." - ".$this->VisitorIP()." - ".date("Y-m-d H:i:s")." => [".$logType."] ".$finalLogData."\n";
		else
			$finalLogData = "Saas - ".$_SERVER['SERVER_NAME']." - ".$this->VisitorIP()." - ".date("Y-m-d H:i:s")." => [".$logType."] ".$finalLogData."\n";
			
		switch($logMethod)
		{
			case 'EMAIL':
				if($this->enable)
				{
					#Send the notification email
					//mail($logMailId, $this->defaultEmailSubject, $finalLogData);
				}			
			break;

			case 'FILE':
			default:
				if($this->enable)
				{
					
					$fp = fopen($logFile, 'a');
					
					global $server;
					if($server!="local")
					{
						$findword  = 'exception';		
						if(preg_match('/'.$findword.'/',$finalLogData))
						{
							$finalLogData="Error Found : ".$finalLogData;						
							if($server!='local')
							{
								if(isset($_SESSION['prefix']))
								{
									if(isset($_SESSION[$_SESSION['prefix']]))
									{
										if(isset($_SESSION[$_SESSION['prefix']]['companyid']))
										{			
											if($_SESSION[$_SESSION['prefix']]['companyid']!=1  && $_SERVER['SERVER_NAME']!='192.168.0.42')
											{
												$headers  = 'MIME-Version: 1.0' . "\r\n";
												$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
												$headers .= 'From: saas <pureipos365@gmail.com>'."\r\n";
												//mail('pureipos365@gmail.com', 'Error Found on Silver Square', $finalLogData, $headers);
											}	
										}
									}
								}
							}
						}
					}	
					fwrite($fp, $finalLogData);
					fclose($fp);
				}
			
			break;
		}
	}
	function VisitorIP()
	{ 
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$TheIp=$_SERVER['HTTP_X_FORWARDED_FOR'];
		else 
			$TheIp=$_SERVER['REMOTE_ADDR'];

		$iparr = explode(',',$TheIp);
		
		if(count($iparr)>0)
		{
			$TheIp=trim($iparr[0]);
		}
		
		return trim($TheIp);
	}
}
?>