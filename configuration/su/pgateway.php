<?php
class pgateway
{
    public $module='pgateway';
    public $log;

    public function __construct()
    {
        $this->log = new \util\logger();

    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            global $commonurl;
            $ObjPayTypeDao = new \database\pgatewaydao();
			$data = $ObjPayTypeDao->grouplist(50,'0','');
            $template = $twig->loadTemplate('pgateway.html');

            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;

            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function addgroup($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addgroup');
            $flag = \util\validate::check_notnull($data,array('paymentmethod','shortcode'));
            if($flag=='true'){
                $reqarr = array(
                    "paymentmethod"=>$data['paymentmethod'],
                    "shortcode"=>$data['shortcode'],
                    "type"=>$data['type'],
                    "active"=>$data['active'],
                    "groupid"=>$data['groupid'],
                    "module" => $this->module
                        );
                $ObjPrivilege_groupDao = new \database\pgatewaydao();
                $data = $ObjPrivilege_groupDao->addGroup($reqarr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>'Add Missing Field !'));
                
        }catch(Exception $e){
            $this->log->logIt($this->module.' - addgroup - '.$e);
        }
    }

    public function getgroupname($data)
	{
		try
		{
			$this->log->logIt($this->module." - getgroupname");


			$ObjPrivilege_groupDao = new \database\pgatewaydao();
			$data = $ObjPrivilege_groupDao->getgroupname($data);
            return $data;
		}catch(Exception $e){
			return false;
		}
	}
    
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            $ObjUserDao = new \database\pgatewaydao();
			$res = $ObjUserDao->grouplist($limit,$offset,$name);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }


}
?>