<?php
class dblist
{
    public $module='dblist';
    public $log;
    public $encdec;
    
    public function __construct()
    {
        $this->log = new \util\logger();
    }
    
	public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
			
		    global $twig;
            global $commonurl;
			$data = array();
			$template = $twig->loadTemplate('dblist.html');
			$ObjCommonDao = new \database\commondao();
			$db_list = $ObjCommonDao->loaddblist(0,50,'');
			$senderarr['commonurl'] = $commonurl;
			$senderarr['datalist'] = $db_list;
			
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	public function adddatabase($data)
    {
        try
        {
            $this->log->logIt($this->module.' - adddatabase');
			$chk_null = \util\validate::check_notnull($data,array('database_name'));
			
			if($chk_null=="true"){
				$ObjCommonDao = new \database\commondao();
				$res = $ObjCommonDao->addDatabase($data);
				return $res;
			}
			else{
				return json_encode(array("Success"=>"False","Message"=>"Record is missing"));
			}
		}
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - adddatabase - '.$e);
        }
    }

	public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            global $twig;
            global $commonurl;
            $limit=50;
            $offset=0;
            $name="";
			
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
			
            $ObjCommonDao = new \database\commondao();
			$data = $ObjCommonDao->loaddblist($limit,$offset,$name);
           
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	public function toggleststus($data)
    {
        try
        {
            $this->log->logIt($this->module.' - toggleststus');

            $ObjCommonDao = new \database\commondao();
			$data = $ObjCommonDao->toggleststus($data);
           
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - toggleststus - '.$e);
        }
    }
    public function toggleststusBycompany($data)
    {
        try
        {
            $this->log->logIt($this->module.' - toggleststusBycompany');

            $ObjCommonDao = new \database\commondao();
            $data = $ObjCommonDao->toggleststusBycompany($data);

            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - toggleststusBycompany - '.$e);
        }
    }
}
?>