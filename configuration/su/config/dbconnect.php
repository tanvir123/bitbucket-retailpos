<?php
#Author: RJ Lumbhani  - 1 June 2015

class dbconnect
{
	private $module = 'POS';
	private $log;	
	
	public function __construct()
	{
	    $this->log=new \util\logger();
	}
	
	public function connect($db_name)
	{	
		try
		{
			$this->log->logIt($this->module."-"."connect - ".$db_name);
			
			global $connection,$mysqlhost,$mysqluser,$mysqlpwd;
			
			$dsnNew="mysql:host=".$mysqlhost.";dbname=".$db_name.";charset=UTF8";
										
			$connection=new PDO($dsnNew,$mysqluser,$mysqlpwd);
			@$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$connection->setAttribute(PDO::ATTR_TIMEOUT,30);
			$connection->exec("SET NAMES utf8");
			return 'Success';
		}
		catch(Exception $e)
		{
			echo $e;
			$this->log->logIt($this->module."-"."connect"."-".$e);
		}
	}
	
	public function config_connect()
	{	
		try
		{
			$this->log->logIt($this->module."-"."config_connect");
			
			global $connection,$mysqlhost,$mysqluser,$mysqlpwd,$dbname;
			
			$dsnNew="mysql:host=".$mysqlhost.";dbname=".$dbname."";	
										
			$connection=new PDO($dsnNew,$mysqluser,$mysqlpwd);
			@$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$connection->setAttribute(PDO::ATTR_TIMEOUT,30);
			$connection->exec("SET NAMES utf8");
			return 'Success';
		}
		catch(Exception $e)
		{
			echo $e;
			$this->log->logIt($this->module."-"."config_connect"."-".$e);
		}
	}
}
?>