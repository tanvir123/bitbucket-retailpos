<?php
require_once "common/TwigConfig.php";
require_once "config/config.php";
require_once "util/logger.php";
require_once "../common/functions.php";
require_once "../common/encdec.php";

if(isset($_SESSION['su_prefix']) && $_SESSION['su_prefix'] == "saas_super_POS"){
    global $commonurl;
    header("Location:".$commonurl."cloud/dashboard");
    exit(0);
}


global $twig;
global $commonurl;

$template = $twig->loadTemplate('login.html');

$senderarr = array();
$senderarr['commonurl'] = $commonurl;
echo $template->render($senderarr);
?>