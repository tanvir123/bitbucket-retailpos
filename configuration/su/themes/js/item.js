function updateAttributes(obj)
{
    var modifier = jQuery('#'+obj).val();
    if (modifier=="0") {
        return false;
    }
    var modifier_txt = jQuery('#'+obj+" option:selected").text();
    var req={};
    req['service'] = 'menu_item';
    req['opcode'] = 'getmodifieritem';
    req['modifierid'] = modifier;
    var str = JSON.stringify(req);
   
    var min = modifierobj[modifier]['min'];
    var max = modifierobj[modifier]['max'];
    HttpSendRequest(str,function(data){
        if(data['Success']=='True')
        {
            var rec = data['Data'];
            var card ='<div id="modifier_'+modifier +'" class="card" style="margin-top:15px"><div class="card-header">';
            card +='<div class="card-title"> <span id="frmtitle_'+modifier+'" class="title">'+modifier_txt+'</span> <input type="hidden" id="sub_modifier_item_'+modifier+'" name="sub_modifier_item_'+modifier+'"> </div>';
            card +='<a mod="'+modifier +'" onclick="closemod(this)" style="float:right;cursor: pointer;font-size: 15px;" class="card-title"> <i class="fa fa-close"></i></a></div><div class="card-body"><div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft" for="name">Quantity</label>';
            card +='<div class="col-sm-9"><div class="controls"><input id="min_'+modifier +'" name="min_'+modifier +'" class="integer form-control"  value="'+min+'" type="text" placeholder="Min" required="" style="width:47%;margin-right:5%;display:inline-block">';
            card +='<input id="max_'+modifier +'" name="max_'+modifier +'" class="integer form-control" type="text" value="'+max+'" placeholder="Max" required="" style="width:47%;display:inline-block">';
            card +='</div></div></div><div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft ">Modifier Items</label>';
            card +='<div class="col-sm-9"><select id="modifier_sel_'+modifier +'" class="Modifier_Items_'+modifier +'" form-control" multiple="multiple" style="width:100%">';
            var arr = {};
            for(var i=0; i<rec.length; i++)
            {
                var hashkey = rec[i]['hashkey'];
                var itemname = rec[i]['itemname'];
                card +='<option class="option_moditem_'+hashkey+'" value="'+hashkey+'">'+itemname+'</option>';
                arr[hashkey] = {};
                arr[hashkey]['name'] = itemname;
                arr[hashkey]['amount'] = rec[i]['amount'];
                arr[hashkey]['sale_amount'] = rec[i]['sale_amount'];
                arr[hashkey]['desc'] = rec[i]['short_desc'];
            }
            modifieritemobj[modifier]=arr;
            card +='</select></div></div><div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft "></label>';
            card +='<div class="col-sm-9"><button mod="'+modifier+'" type="button" class="button btn" style="margin-right: 10px" onclick="selectAllAttributes(this);">Select All</button><button style="margin-right: 10px" mod="'+modifier+'" type="button" class="button btn" onclick="selectNoneAttributes(this);">Select None</button><button  style="float: right" mod="'+modifier+'" type="button" class="btn btn-primary" onclick="loadmodifieritems(this);">Load</button><input type="hidden" id="sub_modifier_'+modifier+'" name="sub_modifier_'+modifier+'" />';
            card +='</div></div><div id="modifier_item_div_'+modifier +'"></div></div></div>';
            jQuery('#modifier_list').append(card);
            jQuery('#modifier_sel_'+modifier).select2();
            var bid_modifier = jQuery("#bind_modifiers").val();
            if (bid_modifier=="")
                jQuery("#bind_modifiers").val(modifier);
            else
                jQuery("#bind_modifiers").val(bid_modifier+","+modifier);
            
            jQuery(".option_"+modifier).attr("disabled","disabled");
            jQuery('#'+obj).val('0');
        }
    });												
}
function selectAllAttributes(current)
{
    var modifier = jQuery(current).attr('mod');
    jQuery('#modifier_sel_'+modifier).find('option').each(function(){
       jQuery(this).prop('selected',true);
    });
    jQuery('#modifier_sel_'+modifier).change();
    return false;
}
function selectNoneAttributes(current)
{
    var modifier = jQuery(current).attr('mod');
    jQuery('#modifier_sel_'+modifier).find('option').prop( 'selected',false );
    jQuery('#modifier_sel_'+modifier).change();
    return false;
}
function loadmodifieritems(current)
{
    var modifier = jQuery(current).attr('mod');
    var sub_modifier = jQuery('#modifier_sel_'+modifier).val();
    var sub_modifier_obj = JSON.stringify(sub_modifier);
    jQuery("#sub_modifier_"+modifier).val(sub_modifier_obj);
    jQuery("#modifier_item_div_"+modifier).empty();
    var modifierobj_sub = modifieritemobj[modifier];
    var modifier_item_str = '';
   
    if (sub_modifier!=null) {
        for (var i=0;i<sub_modifier.length;i++) {
            var moditem_hash = sub_modifier[i];
            if (modifier_item_str=='')
                modifier_item_str += moditem_hash;
            else
                modifier_item_str +=","+moditem_hash;
            var card='<div id="modifier_item_'+sub_modifier[i]+'" class="col-xs-4" style="margin-bottom: 15px;"><div class="card">';
            card +='<div class="card-header"><div class="card-title"> <span id="frmtitle_'+sub_modifier[i]+'" class="title">'+modifierobj_sub[moditem_hash]['name']+'</span>';
            card +='</div></div><div class="card-body"><div class="form-group clearfix">';
            card +='<div><div class="controls"><input id="amount_'+sub_modifier[i]+'" name="amount_'+sub_modifier[i]+'" value="'+modifierobj_sub[moditem_hash]['amount']+'" class="integer form-control" type="text" placeholder="Amount" required="" style="width:47%;margin-right:5%;display:inline-block">';
            card +='<input id="sale_amount_'+sub_modifier[i]+'" name="sale_amount_'+sub_modifier[i]+'" class="integer form-control" type="text" placeholder="Sale Amount" required="" style="width:47%;display:inline-block" value="'+modifierobj_sub[moditem_hash]['sale_amount']+'"></div></div></div>';
            card +='<div class="form-group clearfix"><div><div class="controls"><textarea id="description_'+sub_modifier[i]+'" name="description_'+sub_modifier[i]+'" class="form-control" type="text" placeholder="Description" required="">'+modifierobj_sub[moditem_hash]['desc']+'</textarea></div>';
            card +='</div></div></div></div></div>';
           
            jQuery('#modifier_item_div_'+modifier).append(card);
        }
        jQuery('#sub_modifier_item_'+modifier).val(modifier_item_str);
    }
    else{
        jQuery('#sub_modifier_item_'+modifier).val('');
        jQuery("#sub_modifier_"+modifier).val('');
    }
    
}
function validate_variableproduct(modifire_str) {
    var flag = 1;
    var modifire_arr = modifire_str.split(',');
    for (var i=0; i<modifire_arr.length; i++) {
        var mdf = modifire_arr[i];
        var modifier_item_str = jQuery('#sub_modifier_item_'+mdf).val();
        var modifier_item_arr = modifier_item_str.split(',');
        if (jQuery("#min_"+mdf).val()=="") {
            jQuery("#min_"+mdf).addClass('invalid');
            flag=0;
        }
        if (jQuery("#max_"+mdf).val()=="") {
            jQuery("#max_"+mdf).addClass('invalid');
            flag=0;
        }
        for (var j=0; j<modifier_item_arr.length; j++)
        {
            var mdfi = modifier_item_arr[j];
            if (jQuery("#amount_"+mdfi).val()=="") {
                jQuery("#amount_"+mdfi).addClass('invalid');
                flag=0;
            }
            if (jQuery("#sale_amount_"+mdfi).val()=="") {
                jQuery("#sale_amount_"+mdfi).addClass('invalid');
                flag=0;
            }
        }
    }
    return flag;
}
function loaditem_modlist(bind_modifier) {
   
    jQuery("#modifier_div").show();
    for (var i=0;i<bind_modifier.length;i++)
    {
        (function(i){
            var modifier = bind_modifier[i]['mod_hash'];
            var modifier_txt =  bind_modifier[i]['mod_name'];
            var req={};
            req['service'] = 'menu_item';
            req['opcode'] = 'getmodifieritem';
            req['modifierid'] = modifier;
            var str = JSON.stringify(req);
           
            var min = bind_modifier[i]['mod_min'];
            var max = bind_modifier[i]['mod_max'];
            var sub_modifier = bind_modifier[i]['modifieritem'];
            var mi_hashkeys = bind_modifier[i]['mi_hashkeys'];
            HttpSendRequest(str,function(data){
                if(data['Success']=='True')
                {
                    var rec = data['Data'];
                    
                    var card ='<div id="modifier_'+modifier +'" class="card" style="margin-top:15px"><div class="card-header">';
                    card +='<div class="card-title"> <span id="frmtitle_'+modifier+'" class="title">'+modifier_txt+'</span> <input type="hidden" id="sub_modifier_item_'+modifier+'" name="sub_modifier_item_'+modifier+'"> </div>';
                    card +='<a mod="'+modifier +'" onclick="closemod(this)" style="float:right;cursor: pointer;font-size: 15px;" class="card-title"> <i class="fa fa-close"></i></a></div><div class="card-body"><div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft" for="name">Quantity</label>';
                    card +='<div class="col-sm-9"><div class="controls"><input id="min_'+modifier +'" name="min_'+modifier +'" class="integer form-control"  value="'+min+'" type="text" placeholder="Min" required="" style="width:47%;margin-right:5%;display:inline-block">';
                    card +='<input id="max_'+modifier +'" name="max_'+modifier +'" class="integer form-control" type="text" value="'+max+'" placeholder="Max" required="" style="width:47%;display:inline-block">';
                    card +='</div></div></div><div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft ">Modifier Items</label>';
                    card +='<div class="col-sm-9"><select id="modifier_sel_'+modifier +'" class="Modifier_Items_'+modifier +'" form-control" multiple="multiple" style="width:100%">';
                    var arr = {};
                    for(var i=0; i<rec.length; i++)
                    {
                        var hashkey = rec[i]['hashkey'];
                        var itemname = rec[i]['itemname'];
                        card +='<option class="option_moditem_'+hashkey+'" class="" value="'+hashkey+'">'+itemname+'</option>';
                        arr[hashkey] = {};
                        arr[hashkey]['name'] = itemname;
                        arr[hashkey]['amount'] = rec[i]['amount'];
                        arr[hashkey]['sale_amount'] = rec[i]['sale_amount'];
                        arr[hashkey]['desc'] = rec[i]['short_desc'];
                    }
                    modifieritemobj[modifier]=arr;
                    card +='</select></div></div><div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft "></label>';
                    card +='<div class="col-sm-9"><button mod="'+modifier+'" type="button" class="button btn" style="margin-right: 10px" onclick="selectAllAttributes(this);">Select All</button><button style="margin-right: 10px" mod="'+modifier+'" type="button" class="button btn" onclick="selectNoneAttributes(this);">Select None</button><button  style="float: right" mod="'+modifier+'" type="button" class="btn btn-primary" onclick="loadmodifieritems(this);">Load</button><input type="hidden" id="sub_modifier_'+modifier+'" name="sub_modifier_'+modifier+'" />';
                    card +='</div></div><div id="modifier_item_div_'+modifier +'"></div></div></div>';
                    jQuery('#modifier_list').append(card);
                    jQuery('#modifier_sel_'+modifier).select2();
                    var bid_modifier = jQuery("#bind_modifiers").val();
                    if (bid_modifier=="")
                        jQuery("#bind_modifiers").val(modifier);
                    else
                        jQuery("#bind_modifiers").val(bid_modifier+","+modifier);
                    
                    jQuery(".option_"+modifier).attr("disabled","disabled");
                    
                    jQuery("#sub_modifier_"+modifier).val(mi_hashkeys);
                    jQuery("#modifier_item_div_"+modifier).empty();
                    var modifierobj_sub = modifieritemobj[modifier];
                    var modifier_item_str = '';
                    for (var i=0;i<sub_modifier.length;i++) {
                        //  console.log(sub_modifier[i]['mi_hashkey']);
                        var moditem_hash = sub_modifier[i]['mi_hashkey'];
                        if (modifier_item_str=='')
                            modifier_item_str += moditem_hash;
                        else
                            modifier_item_str +=","+moditem_hash;
                        var card='<div id="modifier_item_'+sub_modifier[i]['mi_hashkey']+'" class="col-xs-4" style="margin-bottom: 15px;"><div class="card">';
                        card +='<div class="card-header"><div class="card-title"> <span id="frmtitle_'+sub_modifier[i]['mi_hashkey']+'" class="title">'+sub_modifier[i]['mi_itemname']+'</span>';
                        card +='</div></div><div class="card-body"><div class="form-group clearfix">';
                        card +='<div><div class="controls"><input id="amount_'+sub_modifier[i]['mi_hashkey']+'" name="amount_'+sub_modifier[i]['mi_hashkey']+'" value="'+sub_modifier[i]['mi_amount']+'" class="integer form-control" type="text" placeholder="Amount" required="" style="width:47%;margin-right:5%;display:inline-block">';
                        card +='<input id="sale_amount_'+sub_modifier[i]['mi_hashkey']+'" name="sale_amount_'+sub_modifier[i]['mi_hashkey']+'" class="integer form-control" type="text" placeholder="Sale Amount" required="" style="width:47%;display:inline-block" value="'+sub_modifier[i]['mi_sale_amount']+'"></div></div></div>';
                        card +='<div class="form-group clearfix"><div><div class="controls"><textarea id="description_'+sub_modifier[i]['mi_hashkey']+'" name="description_'+sub_modifier[i]['mi_hashkey']+'" class="form-control" type="text" placeholder="Description" required="">'+sub_modifier[i]['mi_description']+'</textarea></div>';
                        card +='</div></div></div></div></div>';
                       
                        jQuery(".option_moditem_"+moditem_hash).prop('selected',true);
                        jQuery('#modifier_item_div_'+modifier).append(card);
                    }
                    jQuery('#modifier_sel_'+modifier).change();
                    jQuery('#sub_modifier_item_'+modifier).val(modifier_item_str);
                }
            });
        })(i);
    }
}
function closemod(obj) {
    var  modifier = jQuery(obj).attr('mod');
    var bid_modifier = jQuery("#bind_modifiers").val();
    jQuery("#modifier_"+modifier).remove();
    var ObjMod = bid_modifier.split(',');
    var i = ObjMod.indexOf(modifier);
    if(i != -1) {
        ObjMod.splice(i, 1);
    }
    var StrMod = ObjMod.toString();
    jQuery("#bind_modifiers").val(StrMod);
    jQuery(".option_"+modifier).removeAttr("disabled");
}