
function loaditem_unitlist(countrylist) {

    jQuery("#country_div").removeClass('hide');
            jQuery('#country_list').select2();
            for (var i=0;i<countrylist.length;i++) {
                var country_list = countrylist[i];
                jQuery(".option_"+country_list).prop('selected',true);
            }
            jQuery('#country_list').change();
}

function selectAllCountry()
{
    jQuery('#country_list').find('option').each(function(){
        jQuery(this).prop('selected',true);
    });
    jQuery('#country_list').change();
    return false;
}

function selectNoneCountry()
{
    jQuery('#country_list').find('option').prop( 'selected',false );
    jQuery('#country_list').change();
    return false;
}

