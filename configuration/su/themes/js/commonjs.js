var pageno=1;
var pagereset=true;

$(document).ready(function(){
   jQuery("#logout").click(function(){
        jQuery.ajax({
            url: "controller",
            type: "post",
            data: "service=logout&opcode=load",
            success: function(data){
                if(data == 1)
                {
                    window.location = "../index.php";
                }
            }
        });
   });
});
function showjQloading() {
   jQuery("#content").addClass('loader');
}
function hidejQloading(){
   jQuery("#content").removeClass('loader');
}

function PrintElem(elem)
{
      var mywindow = window.open('', 'PRINT', 'height=400,width=600');


      mywindow.document.write('<html><head><title>' + document.title  + '</title>');

      mywindow.document.write('</head><body >');
     
      mywindow.document.write(document.getElementById(elem).innerHTML);
      mywindow.document.write('</body></html>');

      mywindow.document.close(); // necessary for IE >= 10
      mywindow.focus(); // necessary for IE >= 10*/

      mywindow.print();
      mywindow.close();

      return true;

}
        
function check_audit_activity(id,module)
{
   try
   {
      jQuery.ajax({
            url: "audit_individual_activity",
            type: "post",
            data: "id="+id+"&module="+module,
            success: function(data){
                  jQuery("#modalPrimary").html(data);
                  jQuery("#modalPrimary").modal('show');
            }
      });
   }
   catch(e)
   {
      alert(e);
   }
}

function check_related_records(id,module)
{
   try
   {
      jQuery.ajax({
          url: "check_dependency",
          type: "post",
          data: "id="+id+"&module="+module,
          success: function(data){
              jQuery("#modalPrimary").html(data);
              jQuery("#modalPrimary").modal('show');
          }
      });
   }
   catch(e)
   {
       alert(e);
   }
}

function displaysettings_activity(module)
{
   try
   {
      jQuery.ajax({
            url: "displaysettings_activity",
            type: "post",
            data: "module="+module,
            success: function(data){
                  jQuery("#modalPrimary").html(data);
                  jQuery("#modalPrimary").modal('show');
            }
      });
   }
   catch(e)
   {
      alert(e);
   }
}

$(document).ready(function() {
    $(".integer").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $(".negativeint").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            //Allow: '-'
            ((e.keyCode == 109 || e.keyCode == 189) && e.ctrlKey === true) ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) && (e.keyCode!=109 && e.keyCode!=189)) {
            e.preventDefault();
        }
    });
});
function urldecode (str) {
  return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}
function urlencode (str) {
  return encodeURIComponent(str);
}
function escapeHtml(text) {
    if (isNaN(text)) {
        return text.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"').replace(/&#039;/g, "'");
    } else {
        return text;
    }
}
unscaping = function (obj) {
    if ($.isArray(obj)) {
        var str = [];
    } else {
        var str = {};
    }
    var p;
    for (p in obj) {
        if (obj.hasOwnProperty(p)) {
            v = obj[p];
            if (v !== null && (typeof v === "object" || $.isArray(v))) {
                str[p] = unscaping(v);
            } else {
                str[p] = escapeHtml(v);
            }
        }
    }
    return str;
}
function HttpSendRequest($data, action) {
    $.ajax({
        type: 'POST',
        url: 'controller',
        data: $data,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            var rec = unscaping(data);
            return action(rec);
        }
    });
}

function CallAuthenticatedApi($url,$data,$headers,action)
{
    var authToken = jQuery.getAthtkn();
    $.ajax({
        type: 'POST',
        url: $url,
        data: $data,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(request) {
            request.setRequestHeader("Content-Type", "application/json");
            request.setRequestHeader("app-id", app_id);
            request.setRequestHeader("app-secret", app_secret);
            request.setRequestHeader("auth-token", authToken);
        },
        error: function(data) {
            if(data.status == 401)
            {
                jQuery.removeAuthtkn();
                window.location = siteUrl+"login.php";
            }
            var errObj = JSON.parse(data.responseText);
            callback_error(action,data);
        },
        success: function(data) {
            console.log("in succ..");
            var res = action(data);
            return res;
        },
    });
}
function validemail(email)
{
   var atpos = email.indexOf("@");
   var dotpos = email.lastIndexOf(".");
   if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
      return 0;
   }
   else{
      return 1;
   }
}
function load_report(rec){
   HttpSendRequest(rec, function(data) {
      if (data.Success=="True") {
          jQuery("#content").hide();
          jQuery("#report_content").show();
          jQuery("#print_content").html(data.Data);
      }
   });
}
function Exitinvoice(){
      jQuery("#content").show();
      jQuery("#report_content").hide();
      jQuery("#print_content").empty();
}

function validate_form(id) {

    var error = 0;
    $('.invalid').removeClass('invalid');
    $("#" + id).find(".notnull").each(function () {
        if ($(this).val() == "") {
            error = 1;
            $(this).addClass('invalid');
        }
    });
    $("#" + id).find(".dropdown").each(function () {
        if ($(this).val() == "" || $(this).val() == 0 || $(this).val() == null) {
            error = 1;
            $(this).addClass('invalid');
            $('.select2-selection').addClass('invalid');
        }
    });
    $("#" + id).find(".integer").each(function () {
        if ($(this).val() != "") {
            var regex = /^[0-9]+$/;
            var input = $(this).val();
            if (!regex.test(input)) {
                error = 1;
                $(this).addClass('invalid');
            }
        }
    });
    $("#" + id).find(".decimal").each(function () {
        if ($(this).val() != "") {
            //var dec = /^[-+]?[0-9]+\.[0-9]+$/;
            var regex = /^\d+(\.\d{1,10})?$/;
            var input = $(this).val();
            if (!regex.test(input)) {
                error = 1;
                $(this).addClass('invalid');
            }
        }
    });
    $("#" + id).find(".siteurl").each(function () {
        if ($(this).val() != "") {
            //var dec = /^[-+]?[0-9]+\.[0-9]+$/;
            var regex = /^(http?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
            var input = $(this).val();
            if (!regex.test(input)) {
                error = 1;
                $(this).addClass('invalid');
            }
        }
    });
    $("#" + id).find(".email").each(function () {
        if ($(this).val() != "") {
            var check = validemail($(this).val());
            if (check == 0) {
                error = 1;
                $(this).addClass('invalid');
            }
        }
    });
    if (error == 1)
        return false;
    else
        return true;
}

function remove(id,module,msg) {

   alertify.confirm(msg, function (e) {
        if (e) {
            var arr = {};
            arr['service'] = 'common';
            arr['opcode'] = 'remove';
            arr['id'] = id;
            arr['module'] = module;
            var str = urlencode(JSON.stringify(arr));
            if (arr['id'] != '') {
                HttpSendRequest(str, function (data) {
                    if (data['Success'] == "True") {
                        refreshlist();
                        alertify.success(data['Message']);
                    }
                    else {
                        alertify.error(data['Message']);
                    }
                });
            }
        } else {
            return false;
        }
    });
}

function toggleactive(id, module, value) {
    var arr = {};
    arr['service'] = 'common';
    arr['opcode'] = 'toggleststus';
    arr['module'] = module;
    arr['id'] = id;
    arr['value'] = value;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function (data) {
        if (data['Success'] == "True") {
            refreshlist();
            alertify.success(data['Message']);
        }
        else {
            alertify.error(data['Message']);
        }
    });
}
