<?php
class dashboard
{
    public $module='dashboard';
    public $log;
    public $encdec;
    
    public function __construct()
    {
        $this->log = new \util\logger();
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            global $commonurl;
			
			$ObjCommonDao = new \database\commondao();
			$cardval = $ObjCommonDao->getcardvalue();


            $ObjCompanyDao = new \database\companydao();
            $companylist = $ObjCompanyDao->loadcompanylist(5,0,'','','');
			$clist_arr = JSON_decode($companylist,1);
			
			$data = array();
			$template = $twig->loadTemplate('dashboard.html');
			$senderarr['commonurl'] = $commonurl;
			$senderarr['cardval'] = $cardval;
			$senderarr['clist'] = $clist_arr[0]['data'];
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	public function getDbList()
	{
		try
        {
            $this->log->logIt($this->module.' - getDbList');
			$ObjCommonDao = new \database\commondao();
			$dblist = $ObjCommonDao->getdatabaselistwithsize();
			
			return json_encode(array("Success"=>"True", "Data"=>$dblist));
		}
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getDbList - '.$e);
        }
	}
}
?>