<?php
    require_once "config/config.php";

    session_start();
   
    unset($_SESSION['su_prefix']);
    session_destroy();
    header("location:$commonurl"."su/index.php");
?>