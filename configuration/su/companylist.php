<?php
	require_once(dirname(__FILE__)."/common/s3fileUpload.php");
	
class companylist
{
    public $module='companylist';
    public $log;
    public $encdec;
    
    public function __construct()
    {
        $this->log = new \util\logger();
    }
    
	public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
		    global $twig;
            global $commonurl;
			$template = $twig->loadTemplate('companylist.html');
			$ObjCompanyDao = new \database\companydao();
			$company_list = $ObjCompanyDao->loadcompanylist(50,'0','','','');
			$senderarr['commonurl'] = $commonurl;
			$senderarr['datalist'] = $company_list;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            $cname = "";
            $cid = "";

            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            if(isset($data['cnm']) && $data['cnm']!="")
                $cname = $data['cnm'];
            if(isset($data['cid']) && $data['cid']!="")
                $cid = $data['cid'];

            $ObjCompanyDao = new \database\companydao();
            $data = $ObjCompanyDao->loadcompanylist($limit,$offset,$cname,$name,$cid);

            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	public function addcompany($data)
    {
        try
        {
        	
            $this->log->logIt($this->module.' - addcompany');

			$chk_null = \util\validate::check_notnull($data,array('business_name','contact_name','email','contact','select_country','select_database','location_count','paymenttype'));
			$chk_numeric = \util\validate::check_numeric($data,array('location_count'));
			if($chk_null=="true" && $chk_numeric=="true"){
				$ObjCompanyDao = new \database\companydao();
				$ticketId = substr(str_replace(' ','',microtime(FALSE)),2);

				//$bucket = 'pos-'.$ticketId.'-'.\util\util::str_slug($data['business_name']);
                $bucket='pos-hotel-deep';
				$ObjFile = new s3fileUpload();
                $Check_bucket_exist = $ObjFile->checkBucketExists($bucket);

                if($Check_bucket_exist['Success']=='True')
                {
                    if(!$Check_bucket_exist['status']=='')
                    {
                        $result = $ObjFile->createBucket($bucket);
                    }
                }
				$data['bucket_name'] = $bucket;
				$data = $ObjCompanyDao->addCompany($data);
           		return $data;
			}
			else{
				return json_encode(array("Success"=>"False","Message"=>"Record is missing"));
			}
		}
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addcompany - '.$e);
        }
    }
	public function toggleststus($data)
    {
        try
        {
            $this->log->logIt($this->module.' - toggleststus');
            $ObjCompanyDao = new \database\companydao();
			$data = $ObjCompanyDao->toggleststus($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - toggleststus - '.$e);
        }
    }
    public function addNewCompany()
    {
        try
        {
            $this->log->logIt($this->module.' - addNewCompany');
            global $twig,$commonurl;
            $ObjCommonDao = new \database\commondao();
            $ObjPaymentDao = new \database\pgatewaydao();
            $countrylist = $ObjCommonDao->getCountryList();
            $dblist = $ObjCommonDao->getDatabses();
            $paymenttypes = $ObjPaymentDao->gerecords();
            $template = $twig->loadTemplate('addcompany.html');
            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['countrylist'] = $countrylist;
            $senderarr['dblist'] = $dblist;
            $senderarr['paymenttypes'] = $paymenttypes;
            $rec = $template->render($senderarr);
            return json_encode(array("Data"=>$rec));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addNewCompany - '.$e);
        }
    }

    public function getapplicationsettinglist($data){
        try {
            $this->log->logIt($this->module . ' - getapplicationsettinglist');
            $dao = new \dao();
            $strSql = "SELECT z_applogid_pk,companyid,app_type,application,app_version,app_launched_date,app_setting,app_last_accessed_date,is_live,if(is_live=0,'Yes','No') as live_dis,CASE WHEN application=1 THEN 'Waiter' WHEN application=3 THEN 'POC' WHEN application=2
                       THEN 'User' END as App_dis FROM application_setting where companyid=".$data['appid']." order by z_applogid_pk DESC";
            $dao->initCommand($strSql);
            $res = $dao->executeQuery();
            return json_encode($res);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getapplicationsettinglist - ' . $e);
        }
    }


     public function getsinglerec($data){
            try {
                $this->log->logIt($this->module . ' - getsinglerec');
                $dao = new \dao();
                $strSql = "SELECT * FROM application_setting where z_applogid_pk=".$data['id'];
                $dao->initCommand($strSql);
                $res = $dao->executeRow();
                return json_encode($res);
            } catch (Exception $e) {
                $this->log->logIt($this->module . ' - getsinglerec - ' . $e);
            }
        }


    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
            $flag1 = \util\validate::check_numeric($data,array('app_ver'));
            $flag2 = \util\validate::check_notnull($data,array('app_type','application','app_ver','app_launch','companyid'));
            $appsettings=[];
            if(isset($data['key']) && $data['key']!='' && count($data['key'])>0){
                $keysarray=array_values($data['key']);
                $valsarray=array_values($data['key_val']);
                foreach ($keysarray as $key=>$val){
                    $appsettings[$val]=$valsarray[$key];
                }
            }

            if($flag1=='true' && $flag2=="true"){
                $reqarr = array(
                    "app_type" => $data['app_type'],
                    "application"=> $data['application'],
                    "app_ver"=> $data['app_ver'],
                    "app_launch"=> $data['app_launch'],
                    "app_access" => isset($data['app_access'])?$data['app_access']:'',
                    "id"=> $data['appid'],
                    "companyid"=>$data['companyid'],
                    "is_live"=>$data['rdo_status'],
                    "app_settings"=>!empty($appsettings)?json_encode($appsettings):'',
                    "module" => $this->module
                );


                $ObjPaymentTypeDao = new \database\companydao();
                $data = $ObjPaymentTypeDao->addAppData($reqarr);

                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>"Some field is missing!"));
        }catch(Exception $e){
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }

    public function loadMapping($data)
    {
        try
        {
            $this->log->logIt($this->module.' - loadMapping');
            global $twig,$commonurl;
            $ObjCompanyDao = new \database\companydao();
            $info = $ObjCompanyDao->getCompanyInfo($data['companyid']);
            $template = $twig->loadTemplate('company_mapping.html');

            $dao = new \dao();
            /*$strSql1 = "SELECT isactive,meta_value FROM syscompany_meta WHERE syscompanyid=:syscompanyid AND meta_key=:meta_key";
            $dao->initCommand($strSql1);
            $dao->addParameter(':syscompanyid',$data['companyid']);
            $dao->addParameter(':meta_key','is_banquet_available');
            $rec1 = $dao->executeRow();
            $banquet_value = $rec1['meta_value'];*/

            $strSql1 = "SELECT isactive,meta_value FROM syscompany_meta WHERE syscompanyid=:syscompanyid AND meta_key=:meta_key";
            $dao->initCommand($strSql1);
            $dao->addParameter(':syscompanyid',$data['companyid']);
            $dao->addParameter(':meta_key','total_store');
            $rec1 = $dao->executeRow();
            $total_store = $rec1['meta_value'];


            $ObjStaticArray = new \common\staticarray();
            $arr_lang = $ObjStaticArray->language_codes;

            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['cid'] = $data['companyid'];
          //  $senderarr['ti_intgrtn'] = $info['tabinsta_integration'];
          //  $senderarr['pms_intgrtn'] = $info['pms_integration'];
            $senderarr['loyalty_intgrtn'] = $info['loyalty_integration'];
            $senderarr['store_available'] = $info['store_available'];
            $senderarr['translate_languages'] = $info['translate_languages'];
            //$senderarr['banquet_value'] = $banquet_value;
            $senderarr['lang_list'] = $arr_lang;
            $senderarr['total_store'] = $total_store;
            $rec = $template->render($senderarr);
            return json_encode(array("Data"=>$rec));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loadMapping - '.$e);
        }
    }


    public function viewlogs($data)
    {
        try
        {
            $this->log->logIt($this->module.' - viewlogs');
            $dao = new \dao();
            $strSql1 = "SELECT * FROM company_audit_log";

            if(isset($data['companyid']) && $data['companyid']!='0'){
                $strSql1 .= " WHERE companyid=:companyid";
            }
            $strSql1 .= " ORDER BY id DESC";
            $dao->initCommand($strSql1);
            if(isset($data['companyid']) && $data['companyid']!='0'){
                $dao->addParameter(':companyid',$data['companyid']);
            }
            $data= $dao->executeQuery();

            return json_encode(array("Data"=>$data));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - viewlogs - '.$e);
        }
    }


    public function updateApiMapping($data)
    {
        try
        {
            $this->log->logIt($this->module.' - updateApiMapping');
            $ObjCompanyDao = new \database\companydao();
            $data = $ObjCompanyDao->updateApiMapping($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - updateApiMapping - '.$e);
        }
    }

}
?>