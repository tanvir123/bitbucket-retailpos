<?php
class systemNotification
{
    public $module='systemNotification';
    public $log;

    public function __construct()
    {
        $this->log = new \util\logger();

    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            global $commonurl;
            $ObjNotesDao = new \database\systemNotificationdao();

			$data = $ObjNotesDao->Notificationlist(50,'0','','','');
            $template = $twig->loadTemplate('systemNotification.html');
            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;

            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function addNotification($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addNotification');

            $flag = \util\validate::check_notnull($data,array('releasedate','version','shortdescription','briefdescription'));

            if($flag=='true'){
                $reqarr = array(
                    "releasedate"=>$data['releasedate'],
                    "version"=>$data['version'],
                    "shortdescription"=>$data['shortdescription'],
                    "briefdescription"=>$data['briefdescription'],
                    "active"=>$data['active'],
                    "groupid"=>$data['groupid'],
                    "module" => $this->module
                        );

                $Objsystem_NotificationDao = new \database\systemNotificationdao();
                $data = $Objsystem_NotificationDao->addNotification($reqarr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>'Add Missing Field !'));
                
        }catch(Exception $e){
            $this->log->logIt($this->module.' - addNotification - '.$e);
        }
    }

    public function getNotificationdetail($data)
	{
		try
		{
			$this->log->logIt($this->module." - getNotificationdetail");

			$Objsystem_NotificationDao = new \database\systemNotificationdao();
			$data = $Objsystem_NotificationDao->getNotificationdetail($data);
            return $data;
		}catch(Exception $e){
            $this->log->logIt($this->module." - getNotificationdetail");
			return false;
		}
	}
    
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $version = $releasedate = $shortdes = "";

            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['ver']) && $data['ver']!="")
                $version = $data['ver'];
            if(isset($data['rel']) && $data['rel']!="")
                $releasedate = $data['rel'];
            if(isset($data['shtds']) && $data['shtds']!="")
                $shortdes = $data['shtds'];
            $Objsystem_NotificationDao = new \database\systemNotificationdao();
			$res = $Objsystem_NotificationDao->Notificationlist($limit,$offset,$version,$releasedate,$shortdes);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - rec - '.$e);
        }
    }

}
?>