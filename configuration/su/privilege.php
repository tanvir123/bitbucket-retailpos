<?php
class privilege
{
    public $module='privilege';
    public $log;
    public function __construct()
    {
        $this->log = new \util\logger();

    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            global $commonurl;
            $ObjPrivilege = new \database\privilegedao();
            $privilegedata=$ObjPrivilege->grouplist(50,'0','');
            $template = $twig->loadTemplate('privilege.html');
            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $privilegedata;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function addgroup($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addgroup');

            $flag = \util\validate::check_notnull($data,array('privname'));
            if($flag=='true'){
                $reqarr = array(
                    "name"=>$data['privname'],
                    "privilegegroupid"=>$data['privilegegroupid'],
                    "groupid"=>$data['groupid'],
                    "module" => $this->module
                        );
                $ObjprivilegeDao = new \database\privilegedao();
                $data = $ObjprivilegeDao->addGroup($reqarr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>'Add Missing Field !'));
                
        }catch(Exception $e){
            $this->log->logIt($this->module.' - addgroup - '.$e);
        }
    }

    public function getprivilegename($data)
	{
		try
		{
			$this->log->logIt($this->module." - getprivilegename");
			$ObjprivilegeDao = new \database\privilegedao();
			$data = $ObjprivilegeDao->getprivilegename($data);

            return $data;
		}catch(Exception $e){
			return false;
		}
	}

	public function getprivilegegroup($data)
	{
		try
		{
			$this->log->logIt($this->module." - getprivilegegroup");
            $ObjPayTypeDao = new \database\privilege_groupdao();
            $res = $ObjPayTypeDao->grouplist(50,'0','',$data['type']);
            $res= json_decode($res,true);
            return json_encode(array("Success" => "True", "Data" => $res[0]['data']));
		}catch(Exception $e){
			return false;
		}
	}
    
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            $ObjUserDao = new \database\privilegedao();
			$res = $ObjUserDao->grouplist($limit,$offset,$name);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }


}
?>