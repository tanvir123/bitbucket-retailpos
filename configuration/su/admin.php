<?php
require_once "common/common.php";

$action = $_REQUEST['action'];
$ObjModule = new $action();
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$parts = parse_url($actual_link);
if(isset($parts['query']) && $parts['query']!=''){
    parse_str($parts['query'], $query);
    $ObjModule->load($query);
}else{
    $ObjModule->load();
}


?>