<?php
class taglinenotes
{
    public $module='taglinenotes';
    public $log;

    public function __construct()
    {
        $this->log = new \util\logger();

    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            global $commonurl;
            $ObjTagNotesDao = new \database\taglinenotesdao();
			$data = $ObjTagNotesDao->noteslist(50,'0','','','','');
            $template = $twig->loadTemplate('taglinenotes.html');
            $OBJCOMMONDAO = new \database\commondao();
            $countrylist = $OBJCOMMONDAO->getCountryList();
            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;
            $senderarr['countrylist'] = $countrylist;

            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function addTaglinenotes($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addtaglinenotes');

            $flag = \util\validate::check_notnull($data,array('displaystartdate','displayenddate','taglinenotes','countryid'));
            if(strtotime($data['displayenddate']) < strtotime($data['displaystartdate']))
            {
                return json_encode(array('Success'=>'False','Message'=>'End Date Must Be After Start Date!'));
            }
            if($flag=='true'){
                $reqarr = array(
                    "displaystartdate"=>$data['displaystartdate'],
                    "displayenddate"=>$data['displayenddate'],
                    "taglinenotes"=>$data['taglinenotes'],
                    "active"=>$data['active'],
                    "groupid"=>$data['groupid'],
                    "country"=>$data['countryid'],
                    "module" => $this->module
                        );
                $ObjTagline_notesDao = new \database\taglinenotesdao();
                $data = $ObjTagline_notesDao->addNotes($reqarr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>'Add Missing Field !'));
                
        }catch(Exception $e){
            $this->log->logIt($this->module.' - addtaglinenotes - '.$e);
        }
    }

    public function getNotesdetail($data)
	{
		try
		{
			$this->log->logIt($this->module." - getNotesdetail");
			$ObjTagline_notesDao = new \database\taglinenotesdao();
			$data = $ObjTagline_notesDao->getNotesdetails($data);
            return $data;
		}catch(Exception $e){
            $this->log->logIt($this->module." - getNotesdetail");
			return false;
		}
	}
    
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $displaystartdate = $displayenddate = $taglinenotes = $country ="";

            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['std']) && $data['std']!="")
                $displaystartdate = $data['std'];
            if(isset($data['end']) && $data['end']!="")
                $displayenddate = $data['end'];
            if(isset($data['nt']) && $data['nt']!="")
                $taglinenotes = $data['nt'];
            if(isset($data['ct']) && $data['ct']!="")
                $country = $data['ct'];
            $ObjNotesDao = new \database\taglinenotesdao();
			$res = $ObjNotesDao->noteslist($limit,$offset,$displaystartdate,$displayenddate,$taglinenotes,$country);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - rec - '.$e);
        }
    }

}
?>