<?php

/**
 * Created by PhpStorm.
 * User: romal
 * Date: 28/2/17
 * Time: 9:58 AM
 */
namespace middleware;

use database\authdao;

class ApiAuth
{
    protected $table = "auth_token";
    private $module = 'ApiAuthMiddleWare';
    private $log;

    public function __construct()
    {
        $this->log=new \util\logger();
    }

    public static function auth()
    {
        $auth = new authdao();
        return $auth->checkLogin();
    }
}