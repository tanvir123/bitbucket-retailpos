<?php
class common
{
    private $module='common';
    private $log;

    public function __construct()
    {
        $this->log = new \util\logger();
    }
    public function toggleststus($data)
	{
		try
		{
			$this->log->logIt($this->module.' - toggleststus');
			$ObjCommonDao = new \database\commondao();
			$res = $ObjCommonDao->toggleststus($data);
			return $res;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module.' - toggleststus - '.$e);
		}	
	}
	public function remove($data)
	{
		try
		{
			$this->log->logIt($this->module.' - remove');
			$ObjCommonDao = new \database\commondao();
			$res = $ObjCommonDao->remove($data);
			return $res;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module.' - remove - '.$e);
		}	
	}

}
?>