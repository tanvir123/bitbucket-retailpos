<?php

/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 21/8/17
 * Time: 3:53 PM
 */
class location
{
    public $module='location';
    public $log;
    public $encdec;

    public function __construct()
    {
        $this->log = new \util\logger();
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');

            global $twig;
            global $commonurl;
            $template = $twig->loadTemplate('location.html');
            $ObjCommonDao = new \database\commondao();
            $countryList = $ObjCommonDao->getCountryList();
            $ObjLocationDao = new \database\locationdao();
            $location_list = $ObjLocationDao->loadLocationList(50,'0','','','','');
            $ObjCompanyDao = new \database\companydao();
            $company_list = $ObjCompanyDao->getCompanyList();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['countrylist'] = $countryList;
            $senderarr['datalist'] = $location_list;
            $senderarr['companylist'] = $company_list;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            $cname = "";
            $cid = "";
            $lid = "";
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            if(isset($data['cid']) && $data['cid']!="")
                $cid = $data['cid'];
            if(isset($data['cname']) && $data['cname']!="")
                $cname = $data['cname'];
            if(isset($data['lid']) && $data['lid']!="")
                $lid = $data['lid'];
            $ObjLocationDao = new \database\locationdao();
            $data = $ObjLocationDao->loadLocationList($limit,$offset,$cid,$name,$cname,$lid);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    public function addlocation($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addlocation');
            $chk_null = \util\validate::check_notnull($data,array('slct_company','location_name','contact_name','email','location_contact','select_country'));
            $chk_combo = \util\validate::check_notnull($data,array('select_country'));
            if($chk_null=="true" || $chk_combo=="true"){
                $ObjLocationDao = new \database\locationdao();
                $data = $ObjLocationDao->addLocation($data);
                return $data;
            }
            else{
                return json_encode(array("Success"=>"False","Message"=>"Some field missing"));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addlocation - '.$e);
        }
    }


    public function loadMapping($data)
    {
        try
        {
            $this->log->logIt($this->module.' - loadMapping');
            global $twig,$commonurl;
            $template = $twig->loadTemplate('location_mapping.html');

            $dao = new \dao();
            $strSql1 = "SELECT is_night_audit_enable FROM syslocation WHERE syslocationid=:syslocationid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':syslocationid',$data['locationid']);
            $rec1 = $dao->executeRow();
//            $poc_enable_value = $rec1['is_poc_enable'];
//            $poc_nodes = $rec1['poc_nodes'];

            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['lid'] = $data['locationid'];
            /*$senderarr['poc_available'] = $poc_enable_value;
            $senderarr['poc_nodes'] = $poc_nodes;*/
            $senderarr['is_night_audit_enable'] = $rec1['is_night_audit_enable'];
           /* $senderarr['is_table_booking_enable'] = $rec1['is_table_booking_enable'];
            $senderarr['is_zomato'] = $rec1['is_zomato'];
            $senderarr['zomatoapikeys'] = $rec1['zomatoapikeys'];
            $senderarr['is_fiscal'] = $rec1['is_fiscal'];
            $senderarr['paccode'] = $rec1['paccode'];*/
            $rec = $template->render($senderarr);
            return json_encode(array("Data"=>$rec));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loadMapping - '.$e);
        }
    }

    public function loadpaymentsetting($data)
    {
        try
        {
            $this->log->logIt($this->module.' - loadpaymentsetting');

            $dao = new \dao();
            $strSql1 = "SELECT syscompany.payment_types,syscompany.databasename,syscompany.syscompanyid FROM syslocation
          LEFT JOIN syscompany ON syscompany.syscompanyid=syslocation.companyid
 WHERE  syslocationid=:syslocationid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':syslocationid',$data['locationid']);
            $rec1 = $dao->executeRow();

            if(!$rec1){
                return json_encode(array("Success"=>"False","Message"=>"Location Not Found!"));
            }
            $strSql1 = "SELECT paymenttypeunkid,shortcode,locationid,companyid,paymenttype,paymentgateway_value,isactive FROM ".$rec1['databasename'].".cfpaymenttype WHERE  locationid=:locationid AND companyid=:syscompanyid AND is_deleted=0 ";
            $dao->initCommand($strSql1);
            $dao->addParameter(':locationid',$data['locationid']);
            $dao->addParameter(':syscompanyid',$rec1['syscompanyid']);
            $paymenttypes = $dao->executeQuery();


            $strSql1 = "SELECT paymenttypeunkid,shortcode,companyid,paymenttype,type,isactive,systemdefined FROM syspaymenttype WHERE isdeleted=0 ";
            $dao->initCommand($strSql1);
            $dao->addParameter(':locationid',$data['locationid']);
            $dao->addParameter(':syscompanyid',$rec1['syscompanyid']);
            $supaymenttypes = $dao->executeQuery();

            return json_encode(array("Success"=>"True","Data"=>$paymenttypes,"SUData"=>$supaymenttypes));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loadpaymentsetting - '.$e);
        }
    }

    public function toggle_status($data)
    {
        try
        {
            $this->log->logIt($this->module.' - toggle_status');
            $ObjLocationDao = new \database\locationdao();
            $data = $ObjLocationDao->toggleStatus($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - toggle_status - '.$e);
        }
    }

    public function updateLocationApiMapping($data)
    {
        try
        {
            $this->log->logIt($this->module.' - updateApiMapping');
            $ObjLocationDao = new \database\locationdao();
            $data = $ObjLocationDao->updateLocationApiMapping($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - updateApiMapping - '.$e);
        }
    }
    public function addorupdatepayment($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addorupdatepayment');
            $ObjLocationDao = new \database\locationdao();
            $data = $ObjLocationDao->addorupdatepayment($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addorupdatepayment - '.$e);
        }
    }

    public function loadQuickbookMapping($data)
    {
        try
        {
            $this->log->logIt($this->module.' - loadQuickbookMapping');

            $dao = new \dao();
            $strSql1 = "SELECT syscompany.databasename,syscompany.syscompanyid FROM syslocation
          LEFT JOIN syscompany ON syscompany.syscompanyid=syslocation.companyid
 WHERE  syslocationid=:syslocationid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':syslocationid',$data['locationid']);
            $rec1 = $dao->executeRow();

            if(!$rec1){
                return json_encode(array("Success"=>"False","Message"=>"Location Not Found!"));
            }
            $strSql1 = "SELECT quickbookunkid,clientid,clientsecret,quickcompanyname,quickcompanyid,accesstoken,refreshtoken,accountid,accountname,isactive,locationid,companyid,isactive,currency_linked_account_tax_id FROM ".$rec1['databasename'].".cfquickbook WHERE locationid=:locationid AND companyid=:syscompanyid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':locationid',$data['locationid']);
            $dao->addParameter(':syscompanyid',$rec1['syscompanyid']);
            $quickbooksetting = $dao->executeQuery();
            return json_encode(array("Success"=>"True","Data"=>$quickbooksetting));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loadQuickbookMapping - '.$e);
        }
    }

    public function updateQuickbooksetting($data)
    {
        try
        {
            $this->log->logIt($this->module.' - updateQuickbooksetting');
            $ObjLocationDao = new \database\locationdao();
            $data = $ObjLocationDao->updateQuickbooksetting($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - updateQuickbooksetting - '.$e);
        }
    }
}
?>