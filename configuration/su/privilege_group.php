<?php
class privilege_group
{
    public $module='privilege_group';
    public $log;

    public function __construct()
    {
        $this->log = new \util\logger();

    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            global $commonurl;
            $ObjPayTypeDao = new \database\privilege_groupdao();
			$data = $ObjPayTypeDao->grouplist(50,'0','');
            $template = $twig->loadTemplate('privilege_group.html');

            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;

            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function addgroup($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addgroup');
            $flag = \util\validate::check_notnull($data,array('groupname','module_name','module_type'));
            if($flag=='true'){
                $reqarr = array(
                    "groupname"=>$data['groupname'],
                    "module_name"=>$data['module_name'],
                    "module_type"=>$data['module_type'],
                    "groupid"=>$data['groupid'],
                    "module" => $this->module
                        );
                $ObjPrivilege_groupDao = new \database\privilege_groupdao();
                $data = $ObjPrivilege_groupDao->addGroup($reqarr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>'Add Missing Field !'));
                
        }catch(Exception $e){
            $this->log->logIt($this->module.' - addgroup - '.$e);
        }
    }

    public function getgroupname($data)
	{
		try
		{
			$this->log->logIt($this->module." - getgroupname");


			$ObjPrivilege_groupDao = new \database\privilege_groupdao();
			$data = $ObjPrivilege_groupDao->getgroupname($data);
            return $data;
		}catch(Exception $e){
			return false;
		}
	}
    
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            $ObjUserDao = new \database\privilege_groupdao();
			$res = $ObjUserDao->grouplist($limit,$offset,$name);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }


}
?>