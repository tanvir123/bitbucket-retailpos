<?php
namespace database;

class privilege_groupdao
{
    public $module = 'DB_privilege_groupdao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function addGroup($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addGroup');
            $dao = new \dao();
            $strSqlcheckduplicate="SELECT privilegegroupunkid FROM sysprivilegegroup WHERE groupname=:groupname AND type=:type";
            if ($data['groupid'] != "" && $data['groupid'] !=0) {
                $strSqlcheckduplicate .= " AND  privilegegroupunkid !=" . $data['groupid'] . "";
            }
            $dao->initCommand($strSqlcheckduplicate);
            $dao->addParameter(':groupname',$data['groupname']);
            $dao->addParameter(':type',$data['module_type']);
            $checkduplicatename=$dao->executeQuery();
            if (count($checkduplicatename) > 0) {
                return json_encode(array('Success'=>'False','Message'=>'Please Provide Unique Name'));
            } else {
                if ($data['groupid'] == '0' || $data['groupid'] == 0) {
                    $strSql = "INSERT INTO sysprivilegegroup SET 
                            groupname=:groupname,module_name=:module_name,type=:type";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':groupname', $data['groupname']);
                    $dao->addParameter(':module_name', $data['module_name']);
                    $dao->addParameter(':type',$data['module_type']);
                    $dao->executeNonQuery();
                    return json_encode(array('Success' => 'True', 'Message' => 'Privileges group saved successfully'));
                } else {
                    $strSql = "UPDATE sysprivilegegroup SET groupname=:groupname,module_name=:module_name,type=:type
                               WHERE privilegegroupunkid=:privilegegroupunkid ";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':groupname', $data['groupname']);
                    $dao->addParameter(':module_name', $data['module_name']);
                    $dao->addParameter(':type',$data['module_type']);
                    $dao->addParameter(':privilegegroupunkid', $data['groupid']);
                    $dao->executeNonQuery();
                    return json_encode(array('Success' => 'True', 'Message' => 'Privileges group updated successfully'));
                }
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addGroup - '.$e);
        }
    }

    public function grouplist($limit,$offset,$name,$type='')
    {
        try
        {
            $this->log->logIt($this->module.' - grouplist - '.$name);
            $dao = new \dao;

            $strSql = "SELECT * FROM sysprivilegegroup WHERE 1 ";
            if($name!=""){
                $strSql .= " AND groupname LIKE '%".$name."%'";
            }if($type!=""){
                $strSql .= " AND type = '".$type."'";
            }
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $strSql .=" ORDER BY groupname ASC";
            $dao->initCommand($strSql);

            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);

            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return json_encode($retvalue);        
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return json_encode($retvalue);   
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - grouplist - '.$e);
        }
    }
    public function getgroupname($data)
    {
		try
        {
			$this->log->logIt($this->module." - getgroupname");
			$dao = new \dao();
            $privilegegroupunkid=(isset($data['groupid']))?$data['groupid']:"";
            $strSql = "SELECT groupname,module_name,type 
                        FROM sysprivilegegroup 
                        WHERE privilegegroupunkid=:privilegegroupunkid ";
            $dao->initCommand($strSql);
            $dao->addParameter(':privilegegroupunkid',$privilegegroupunkid);
            $res = $dao->executeRow();
            return json_encode(array("Success"=>"True","Data"=>$res));
        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getgroupname - ".$e);
			return false; 
		}
	}
}

?>