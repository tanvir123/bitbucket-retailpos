<?php
namespace database;
class parameter
{
    public $module = 'DB_parameter';
    public $log;
    public $dbconnect;
    
    function __construct()
    {
       // $this->log = new \util\logger();
    }
    
    public function getParameter($keyname,$companyid='',$loginid='',$dbname='')
    {
        try
        {
            //$this->log->logIt($this->module.' - getParameter = '.$keyname);

            $dao = new \dao();
//            $strSql = "SELECT * FROM ".$dbname.".cfparameter WHERE keyname = '".$keyname."' AND companyid = ".$companyid." AND locationid = ".$loginid." and type=1 ";

            $strSql = "SELECT * FROM ".$dbname.".cfparameter WHERE keyname = '".$keyname."' AND type=1 ";

            if($companyid != '')
            {
                $strSql.=" AND companyid = ".$companyid;
            }
            if($loginid != '')
            {
                $strSql.=" AND locationid = ".$loginid;
            }

            $dao->initCommand($strSql);
            $data = $dao->executeRow();
            return $data['keyvalue'];
        }
        catch(Exception $e)
        {
            //$this->log->logIt($this->module.' - getParameter - '.$e);
        }
    }
}
?>