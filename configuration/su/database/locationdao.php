<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 21/8/17
 * Time: 4:13 PM
 */
namespace database;
use util\util;

class locationdao
{
    public $module = 'DB_commondao';
    public $log;
    public $dbconnect;

    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function loadLocationList($limit="",$offset="",$cid="",$lc_name="",$c_name="",$lid="")
    {
        try
        {
            $this->log->logIt($this->module.' - loadLocationList');
            $dao = new \dao();
            $strSql = "SELECT SLC.syslocationid,DATE_FORMAT(SLC.createddatetime,'%d/%m/%Y') as created_date".
                      " ,TIME_FORMAT(SLC.createddatetime,'%h:%s:%i %p') as created_time,SLC.locationname".
                      " ,SLC.locationemail,SLC.contactperson,SLC.is_active as status,SC.companyname ".
                      " FROM syslocation AS SLC INNER JOIN syscompany AS SC ON SLC.companyid = SC.syscompanyid";
            if($lc_name!=""){
                $strSql .= " AND SLC.locationname LIKE '%".$lc_name."%'" ;
            }
            if($c_name!=""){
                $strSql .= " AND SLC.contactperson LIKE '%".$c_name."%'" ;
            }
            if($cid!="" && $cid!=0){
                $strSql .= " AND SLC.companyid = ".$cid ;
            }
            if($lid!=""){
                $strSql .= " AND SLC.syslocationid = ".$lid ;
            }
            $strSql .= " ORDER BY createddatetime DESC";

            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }

            $dao->initCommand($strSql);
            $data = $dao->executeQuery();

            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);

            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return json_encode($retvalue);
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return json_encode($retvalue);
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loadLocationList - '.$e);
        }
    }
    public function addLocation($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addLocation');
            $dao = new \dao();
            $ObjUtil = new \util\util();
            $api_auth_key = $ObjUtil->generateauthkey();
            $strSql1 = "SELECT COUNT(syslocationid) as cnt FROM `syslocation` WHERE companyid=:companyid";
            $dao->initCommand($strSql1);
            $dao->addParameter(":companyid",$data['slct_company']);
            $rec1 = $dao->executeRow();

            $strSql2 = "SELECT IFNULL(total_location,0) as total_loc,databasename,payment_types from syscompany WHERE syscompanyid=:companyid";
            $dao->initCommand($strSql2);
            $dao->addParameter(":companyid",$data['slct_company']);
            $rec2 = $dao->executeRow();
            $db_name = $rec2['databasename'];
            $payment_types = $rec2['payment_types'];


            $strSql15 = "SELECT shortcode,paymenttype,type,isactive FROM syspaymenttype WHERE companyid=0 AND paymenttypeunkid IN (".$payment_types.")";
            $dao->initCommand($strSql15);
            $dao->addParameter(":companyid", $data['slct_company']);
            $rec15 = $dao->executeQuery();

            if($rec1['cnt']>=$rec2['total_loc']){
                return json_encode(array("Success"=>"False","Message"=>"No more locations can be added"));
            }
            $strSql3 = "INSERT INTO syslocation SET locationname=:locationname,locationemail=:locationemail,
                        contactperson=:contactperson,is_active=1,companyid=:companyid,createddatetime=NOW(),api_auth_key=:api_auth_key";
            $dao->initCommand($strSql3);
            $dao->addParameter(":locationname",$data['location_name']);
            $dao->addParameter(":locationemail", $data['email']);
            $dao->addParameter(":contactperson",$data['contact_name']);
            $dao->addParameter(":api_auth_key",$api_auth_key);
            $dao->addParameter(":companyid",$data['slct_company']);
            $dao->executeNonQuery();
            $location_id = $dao->getLastInsertedId();


            $strSql12 = "SELECT currencyCode,countryCode FROM countrylist WHERE id=".$data['select_country'];
            $dao->initCommand($strSql12);
            $rec11 = $dao->executeRow();

            $hash1 = \util\util::gethash($data['slct_company']);
            $strSql13 = "INSERT INTO ".$db_name.".cfcurrencylist SET countryid='".$data['select_country']."', currency_code='".$rec11['currencyCode']."',currency_sign='".$rec11['currencyCode']."',currency_sign_pos=0,currency_rate=1, is_active=1, is_deleted=0, companyid=:companyid,locationid=:locationid,currency_is_default=1,hashkey='".$hash1."'";
            $dao->initCommand($strSql13);
            $dao->addParameter(":locationid",$location_id);
            $dao->addParameter(":companyid",$data['slct_company']);
            $dao->executeNonQuery();
            $currencyunkid = $dao->getLastInsertedId();


            $strSql4 = " INSERT INTO ".$db_name.".cflocation SET locationid=:locationid,companyid=:companyid, locationname=:locationname, 
                         locationemail=:locationemail,country=:country,currency_sign=:currency_sign,currencylnkid=:currencylnkid";
            $dao->initCommand($strSql4);
            $dao->addParameter(":locationid", $location_id);
            $dao->addParameter(":companyid", $data['slct_company']);
            $dao->addParameter(":locationname", $data['location_name']);
            $dao->addParameter(":locationemail", $data['email']);
            $dao->addParameter(":currency_sign", $rec11['currencyCode']);
            $dao->addParameter(":currencylnkid", $currencyunkid);
            $dao->addParameter(":country", $data['select_country']);
            $dao->executeNonQuery();


            $strSql5 = " INSERT INTO ".$db_name.".cfparameter (keyname, keyvalue, description, companyid,locationid,storeid,type) SELECT sys.keyname,sys.keyvalue,sys.description,:companyid,:locationid,0,1 FROM sysparameter AS sys WHERE sys.companyid=0 AND type=1";
            $dao->initCommand($strSql5);
            $dao->addParameter(":locationid", $location_id);
            $dao->addParameter(":companyid", $data['slct_company']);
            $dao->executeNonQuery();



            $strupdateNA = "UPDATE ".$db_name.".cfparameter SET keyvalue=:keyvalue WHERE `keyname` = 'night_audit_email' AND type=1 AND locationid=:locationid AND companyid=:companyid";
            $dao->initCommand($strupdateNA);
            $dao->addParameter(":keyvalue", $data['email']);
            $dao->addParameter(":locationid", $location_id);
            $dao->addParameter(":companyid", $data['slct_company']);
            $dao->executeNonQuery();

            $strupdate = "UPDATE ".$db_name.".cfparameter SET keyvalue=:keyvalue WHERE `keyname` IN ('country','nationality') AND type=1 AND locationid=:locationid AND companyid=:companyid";
            $dao->initCommand($strupdate);
            $dao->addParameter(":keyvalue", $data['select_country']);
            $dao->addParameter(":locationid", $location_id);
            $dao->addParameter(":companyid", $data['slct_company']);
            $dao->executeNonQuery();

            $LocalDate = gmdate("Y-m-d");
            $strupdateNA = "UPDATE ".$db_name.".cfparameter SET keyvalue=:keyvalue WHERE `keyname` = 'todaysdate' AND type=1 AND locationid=:locationid AND companyid=:companyid";
            $dao->initCommand($strupdateNA);
            $dao->addParameter(":keyvalue", $LocalDate);
            $dao->addParameter(":locationid", $location_id);
            $dao->addParameter(":companyid", $data['slct_company']);
            $dao->executeNonQuery();

            $strSql6 = " INSERT INTO ".$db_name.".cfdocnumber (keyname, prefix,main_start_no, startno,endno, reset_type,companyid,locationid, hashkey) SELECT sys.keyname, sys.prefix, sys.main_start_no,sys.startno,sys.endno, sys.reset_type,:companyid,:locationid, concat( lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0), lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0), lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0), lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0) ) FROM sysdocnumber AS sys WHERE sys.companyid=0";
            $dao->initCommand($strSql6);
            $dao->addParameter(":locationid", $location_id);
            $dao->addParameter(":companyid", $data['slct_company']);
            $dao->executeNonQuery();

            $strSql7 = " INSERT INTO ".$db_name.".fasmaster(name,mastertypeunkid,crdr,isactive,is_deleted,companyid,locationid) SELECT SYSM.name,SYSM.mastertypeunkid,SYSM.crdr,SYSM.is_active,0,:companyid,:locationid FROM sysmaster AS SYSM WHERE SYSM.companyid=0";
            $dao->initCommand($strSql7);
            $dao->addParameter(":locationid", $location_id);
            $dao->addParameter(":companyid", $data['slct_company']);
            $dao->executeNonQuery();

            $strSql8 = "SELECT GROUP_CONCAT(syslocationid) as location FROM syslocation WHERE companyid=:companyid";
            $dao->initCommand($strSql8);
            $dao->addParameter(":companyid", $data['slct_company']);
            $rec8 = $dao->executeRow();

            $strSql9 = "UPDATE sysuser SET locationid=:locationid WHERE user_type=1 AND sysdefined=1 AND isactive=1 AND is_deleted=0 AND companyid=:companyid";
            $dao->initCommand($strSql9);
            $dao->addParameter(":companyid", $data['slct_company']);
            $dao->addParameter(":locationid", $rec8['location']);
            $dao->executeNonQuery();

            //Insert Charges

            $hashkey1 = $data['slct_company'].'00'.md5(uniqid(date("Y-m-d H:i:s").'-'.rand(), true));
            $hashkey2 = $data['slct_company'].'00'.md5(uniqid(date("Y-m-d H:i:s").'-'.rand(), true));

            $strSql10 = "INSERT INTO ".$db_name.".`cfcharges` (`charge_type`, `apply_zomato`, `shortcode`, `charge`, `fasmastertype`, `is_default`,`applicable_on`,`isactive`, `is_deleted`, `hashkey`, `companyid`, `locationid`,`amount`,`type`) VALUES ";

            $strSql10.= "('1', '0', 'Packaging Charge', 'Packaging Charge','charge','1','1','1','0','".$hashkey1."','".$data['slct_company']."','".$location_id."','0','1'),";
            $strSql10.= "('2', '0', 'Delivery Charge', 'Delivery Charge','charge','1','1','1','0','".$hashkey2."','".$data['slct_company']."','".$location_id."','0','2')";
            $dao->initCommand($strSql10);
            $dao->executeNonQuery();

            if(isset($rec15)){
                $checkpt=0;
                foreach ($rec15 as $key=>$val){
                    $paymenthash = \util\util::gethash($location_id);
                    $strSql10 = "INSERT INTO ".$db_name.".cfpaymenttype SET shortcode=:shortcode,paymenttype=:paymenttype,type=:type,fasmastertype='paymenttype',isactive=:isactive,systemdefined='1',is_deleted='0',hashkey=:hashkey,companyid=:companyid,locationid=:locationid ";
                    //systemdefined=1
                    $dao->initCommand($strSql10);
                    $dao->addParameter(":shortcode", $val['shortcode']);
                    $dao->addParameter(":paymenttype", $val['paymenttype']);
                    $dao->addParameter(":type", $val['type']);
                    $dao->addParameter(":isactive", $val['isactive']);
                    $dao->addParameter(":locationid", $location_id);
                    $dao->addParameter(":companyid", $data['slct_company']);
                    $dao->addParameter(":hashkey", $paymenthash);
                    $dao->executeNonQuery();
                    $paymenttypeid = $dao->getLastInsertedId();


                    if($checkpt==0 && $val['isactive']==1 ){
                        $checkpt=1;
                        $strSql11 = "UPDATE ".$db_name.".cfparameter SET keyvalue=:keyvalue WHERE companyid=:companyid AND locationid=:locationid AND type=1 AND keyname = 'payment_type'";
                        $dao->initCommand($strSql11);
                        $dao->addParameter(":locationid", $location_id);
                        $dao->addParameter(":companyid", $data['slct_company']);
                        $dao->addParameter(":keyvalue", $paymenttypeid);
                        $dao->executeNonQuery();

                    }

                }
            }



            return json_encode(array("Success"=>"True","Message"=>"Location Created Successfully"));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addLocation - '.$e);
        }
    }
    public function toggleStatus($data)
    {
        try
        {
            $this->log->logIt($this->module.' - toggleStatus');
            $dao = new \dao();
            $strSql = "UPDATE syslocation SET is_active = IF(is_active=1,0,1) WHERE syslocationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(":locationid",$data['id']);
            $dao->executeNonQuery();
            return json_encode(array("Success"=>"True","Message"=>"Status changed successfully"));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - toggleStatus - '.$e);
        }
    }


    public function updateLocationApiMapping($data)
    {
        try
        {
            $this->log->logIt($this->module.' - updateLocationApiMapping');
            $dao = new \dao();
            $strSql2 = " UPDATE syslocation SET is_night_audit_enable=:is_night_audit_enable WHERE syslocationid=:syslocationid";
            $dao->initCommand($strSql2);
            $dao->addParameter(':syslocationid',$data['lid']);
            /*$dao->addParameter(':is_poc_enable',$data['poc_usr']);
            $dao->addParameter(':poc_nodes',$data['poc_node']);*/
            $dao->addParameter(':is_night_audit_enable',$data['is_night_audit_enable']);
           /* $dao->addParameter(':is_table_booking_enable',$data['is_table_booking_enable']);
            $dao->addParameter(':is_zomato',$data['is_zomato']);
            $dao->addParameter(':zomatoapikeys',$data['zomatoapikeys']);
            $dao->addParameter(':is_fiscal',$data['is_fiscal']);
            $dao->addParameter(':paccode',$data['paccode']);*/
            $dao->executeNonQuery();

            return json_encode(array("Success"=>"True","Message"=>"Settings changed successfully"));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - updateLocationApiMapping - '.$e);
        }
    }

    public function addorupdatepayment($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addorupdatepayment');
            unset($data['service']);
            unset($data['opcode']);
            $dao = new \dao();
            $strSql1 = "SELECT syscompany.databasename,syscompany.syscompanyid FROM syslocation
          LEFT JOIN syscompany ON syscompany.syscompanyid=syslocation.companyid
 WHERE  syslocationid=:syslocationid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':syslocationid',$data['lid']);
            $rec1 = $dao->executeRow();

            if(!$rec1){
                return json_encode(array('Success'=>'False','Message'=>'Location Not Found!'));
            }
            $locationid=isset($data['lid'])?$data['lid']:0;
            $status=isset($data['is_active'])?$data['is_active']:0;
            $rel_id=isset($data['rel_id'])?$data['rel_id']:0;
            $mainpaymenttype=isset($data['mainpaymenttype'])?$data['mainpaymenttype']:0;

            $strSql1 = "SELECT * FROM syspaymenttype
 WHERE  paymenttypeunkid=:paymenttypeunkid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':paymenttypeunkid',$mainpaymenttype);
            $paymentmaindata = $dao->executeRow();

            if(!$paymentmaindata){
                return json_encode(array('Success'=>'False','Message'=>'Main Payment Not Found!'));
            }

            $datetime=\util\util::getLocalDateTime($rec1['syscompanyid'],$locationid,$rec1['databasename']);
            unset($data['lid']);
            unset($data['rel_id']);
            unset($data['is_active']);
            unset($data['mainpaymenttype']);
            $json_object=isset($data)?json_encode($data):'';

            if($rel_id==0){
                $hashkey = \util\util::gethash($rec1['syscompanyid']);
                $strSql = "INSERT INTO ".$rec1['databasename'].".cfpaymenttype (shortcode, paymenttype, type, isactive,paymentgateway_value, createddatetime, fasmastertype, created_user, companyid,locationid, hashkey)
                            VALUES(:shortcode, :paymentmethod, :type, :status,:paymentgateway_value, :datetime, :fasmastertype, :userid, :companyid,:locationid, :hashkey)";
                $dao->initCommand($strSql);
                $dao->addParameter(':shortcode',$paymentmaindata['shortcode']);
                $dao->addParameter(':paymentmethod',$paymentmaindata['paymenttype']);
                $dao->addParameter(':type',$paymentmaindata['type']);
                $dao->addParameter(':status',$status);
                $dao->addParameter(':datetime',$datetime);
                $dao->addParameter(':paymentgateway_value',$json_object);
                $dao->addParameter(':fasmastertype','paymenttype');
                $dao->addParameter(':userid','');
                $dao->addParameter(':hashkey',$hashkey);
                $dao->addParameter(':companyid',$rec1['syscompanyid']);
                $dao->addparameter(':locationid',$locationid);
                $dao->executeNonQuery();
            }else{
                $strSqlcheckexist="SELECT paymenttypeunkid FROM ".$rec1['databasename'].".cfpaymenttype WHERE paymenttypeunkid=:paymenttypeunkid";
            $dao->initCommand($strSqlcheckexist);
            $dao->addParameter(':paymenttypeunkid',$rel_id);
            $checkexist=$dao->executeQuery();
            if (!$checkexist) {
                return json_encode(array('Success'=>'False','Message'=>'Payment ID is wrong!'));
            }

                $strSql2 = " UPDATE ".$rec1['databasename'].".cfpaymenttype SET paymentgateway_value=:paymentgateway_value,isactive=:isactive WHERE locationid=:locationid AND paymenttypeunkid=:paymenttypeunkid";
                $dao->initCommand($strSql2);
                $dao->addParameter(':locationid',$locationid);
                $dao->addParameter(':paymenttypeunkid',$rel_id);
                $dao->addParameter(':isactive',$status);
                $dao->addParameter(':paymentgateway_value',$json_object);
                $dao->executeNonQuery();
            }


            return json_encode(array("Success"=>"True","Message"=>"Settings changed successfully"));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addorupdatepayment - '.$e);
        }
    }

    public function updateQuickbooksetting($data)
    {
        try
        {
            $this->log->logIt($this->module.' - updateQuickbooksetting');
            unset($data['service']);
            unset($data['opcode']);
            $dao = new \dao();
            $strSql1 = "SELECT syscompany.databasename,syscompany.syscompanyid FROM syslocation
          LEFT JOIN syscompany ON syscompany.syscompanyid=syslocation.companyid
 WHERE  syslocationid=:syslocationid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':syslocationid',$data['lid']);
            $rec1 = $dao->executeRow();

            if(!$rec1){
                return json_encode(array('Success'=>'False','Message'=>'Location Not Found!'));
            }
            $locationid=isset($data['lid'])?$data['lid']:0;
            $status=isset($data['is_active'])?$data['is_active']:0;
            $rel_id=isset($data['rel_id'])?$data['rel_id']:0;

            $datetime=\util\util::getLocalDateTime($rec1['syscompanyid'],$locationid,$rec1['databasename']);
            unset($data['lid']);
            unset($data['rel_id']);
            unset($data['is_active']);

            if(isset($data) && count($data)>0)
            {
                if($rel_id==0){
                    $strSql = "INSERT INTO ".$rec1['databasename'].".cfquickbook (clientid, clientsecret, quickcompanyname, quickcompanyid,accesstoken,refreshtoken,accountid,accountname, isactive, createddatetime, companyid,locationid,currency_linked_account_tax_id)
                            VALUES(:clientid, :clientsecret,:quickcompanyname, :quickcompanyid, :accesstoken, :refreshtoken, :accountid, :accountname, :status, :datetime, :companyid,:locationid,:currency_linked_account_tax_id)";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':clientid',$data['clientid']);
                    $dao->addParameter(':clientsecret',$data['clientsecret']);
                    $dao->addParameter(':quickcompanyname',$data['quickcompanyname']);
                    $dao->addParameter(':quickcompanyid',$data['quickcompanyid']);
                    $dao->addParameter(':accesstoken',$data['accesstoken']);
                    $dao->addParameter(':refreshtoken',$data['refreshtoken']);
                    $dao->addParameter(':accountid',$data['accountid']);
                    $dao->addParameter(':accountname',$data['accountname']);
                    $dao->addParameter(':status',$status);
                    $dao->addParameter(':datetime',$datetime);
                    $dao->addParameter(':companyid',$rec1['syscompanyid']);
                    $dao->addparameter(':locationid',$locationid);
                    $dao->addparameter(':currency_linked_account_tax_id',$data['currency_linked_account_tax_id']);
                    $dao->executeNonQuery();
                }else{
                    $strSql2 = " UPDATE ".$rec1['databasename'].".cfquickbook SET 
                    clientid=:clientid,
                    clientsecret=:clientsecret,
                    quickcompanyname=:quickcompanyname,
                    quickcompanyid=:quickcompanyid,
                    accesstoken=:accesstoken,
                    refreshtoken=:refreshtoken,
                    accountid=:accountid,
                    accountname=:accountname,
                    clientid=:clientid,
                    isactive=:status,
                    currency_linked_account_tax_id=:currency_linked_account_tax_id,
                    modifieddatetime=:modifieddatetime WHERE locationid=:locationid AND quickbookunkid=:quickbookunkid";
                    $dao->initCommand($strSql2);
                    $dao->addParameter(':clientid',$data['clientid']);
                    $dao->addParameter(':clientsecret',$data['clientsecret']);
                    $dao->addParameter(':quickcompanyname',$data['quickcompanyname']);
                    $dao->addParameter(':quickcompanyid',$data['quickcompanyid']);
                    $dao->addParameter(':accesstoken',$data['accesstoken']);
                    $dao->addParameter(':refreshtoken',$data['refreshtoken']);
                    $dao->addParameter(':accountid',$data['accountid']);
                    $dao->addParameter(':accountname',$data['accountname']);
                    $dao->addParameter(':status',$status);
                    $dao->addParameter(':modifieddatetime',$datetime);
                    $dao->addParameter(':quickbookunkid',$rel_id);
                    $dao->addParameter(':locationid',$locationid);
                    $dao->addparameter(':currency_linked_account_tax_id',$data['currency_linked_account_tax_id']);
                    $dao->executeNonQuery();
                }

                $bulksql = " INSERT INTO  " . $rec1['databasename']. ".cfcompanylocation_meta (`meta_value`,`meta_key`,`companyid`,`locationid`) VALUES ('" . $data['accesstoken']. "','quickbook_access_token',:companyid,:locationid) ON DUPLICATE KEY  UPDATE `meta_value` = '" . $data['accesstoken'] . "';";
                $bulksql .= " INSERT INTO  " . $rec1['databasename']. ".cfcompanylocation_meta (`meta_value`,`meta_key`,`companyid`,`locationid`) VALUES ('" . $data['refreshtoken']. "','quickbook_refresh_token',:companyid,:locationid) ON DUPLICATE KEY  UPDATE `meta_value` = '" . $data['refreshtoken'] . "';";

                $dao->initCommand($bulksql);
                $dao->addParameter(':companyid',$rec1['syscompanyid']);
                $dao->addParameter(':locationid',$locationid);
                $dao->executeNonQuery();

            }

            return json_encode(array("Success"=>"True","Message"=>"Settings changed successfully"));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - updateQuickbooksetting - '.$e);
        }
    }
}
?>