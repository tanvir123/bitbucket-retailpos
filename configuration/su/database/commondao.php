<?php
namespace database;
class commondao
{
    public $module = 'DB_commondao';
    public $log;
    public $dbconnect;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
    
    public function getCountryList()
    {
        try
        {
            $this->log->logIt($this->module.' - getCountrylist');
           
            $dao = new \dao();
            
            $strSql = "SELECT id, countryName FROM countrylist";
            $dao->initCommand($strSql);
            $res = $dao->executeQuery();
            $retarray = array();
            foreach($res AS $key=>$value)
            {
                $retarray[$value['id']] = $value['countryName'];
            }
            
            return $retarray;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getCountryList - '.$e);
        }
    }
    public function getDatabses()
    {
        try
        {
            $this->log->logIt($this->module.' - getDatabses');
            $dao = new \dao();
            $strSql = "SELECT dbname,propertycount FROM sysdatabase;";
            $dao->initCommand($strSql);
            $res = $dao->executeQuery();
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getDatabses - '.$e);
        }
    }
    // Database List
    public function loaddblist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - loaddblist');
            $dao = new \dao();
            $strSql = "SELECT dbname, propertycount AS cnt FROM sysdatabase WHERE 1";
            if($name!="")
                $strSql .= "  AND dbname LIKE '%".$name."%'" ;
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $dao->initCommand($strSql);
            $data = $dao->executeQuery();
           
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);          
          
            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return json_encode($retvalue);        
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return json_encode($retvalue);   
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loaddblist - '.$e);
        }
    }
    
    public function addDatabase($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addDatabase');
            $dao = new \dao();
            $dbname = 'saas_cloudpos'.$data['database_name'];
            
            $strSql = "SELECT COUNT(*) AS cnt FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '".$dbname."'";
            $dao->initCommand($strSql);
            $data = $dao->executeRow();
            if($data['cnt']==0){
                $strSql = " CREATE DATABASE ".$dbname;
                $dao->initCommand($strSql);
                $dao->executeNonQuery();
                
                $strSql = " INSERT INTO saas_retailpossystem.sysdatabase SET dbname=:dbname, propertycount=0";
                $dao->initCommand($strSql);
                $dao->addParameter(":dbname", $dbname);
                $dao->executeNonQuery();
                
                $strSql = "CALL SP_addnewdb(:dbname,@sds)";
                $dao->initCommand($strSql);
                $dao->addParameter(":dbname", $dbname);
                $spdata = $dao->executeRow();
                if($spdata['finalstatus']=='committing'){
                    $strSql = "INSERT INTO `".$dbname."`.`countrylist` (`id`, `countryCode`, `countryName`, `currencyCode`, `isoNumeric`, `isoAlpha3`) VALUES
(1, 'AD', 'Andorra', 'EUR', '020', 'AND'),
(2, 'AE', 'United Arab Emirates', 'AED', '784', 'ARE'),
(3, 'AF', 'Afghanistan', 'AFN', '004', 'AFG'),
(4, 'AG', 'Antigua and Barbuda', 'XCD', '028', 'ATG'),
(5, 'AI', 'Anguilla', 'XCD', '660', 'AIA'),
(6, 'AL', 'Albania', 'ALL', '008', 'ALB'),
(7, 'AM', 'Armenia', 'AMD', '051', 'ARM'),
(8, 'AO', 'Angola', 'AOA', '024', 'AGO'),
(9, 'AQ', 'Antarctica', '', '010', 'ATA'),
(10, 'AR', 'Argentina', 'ARS', '032', 'ARG'),
(11, 'AS', 'American Samoa', 'USD', '016', 'ASM'),
(12, 'AT', 'Austria', 'EUR', '040', 'AUT'),
(13, 'AU', 'Australia', 'AUD', '036', 'AUS'),
(14, 'AW', 'Aruba', 'AWG', '533', 'ABW'),
(15, 'AX', 'Åland', 'EUR', '248', 'ALA'),
(16, 'AZ', 'Azerbaijan', 'AZN', '031', 'AZE'),
(17, 'BA', 'Bosnia and Herzegovina', 'BAM', '070', 'BIH'),
(18, 'BB', 'Barbados', 'BBD', '052', 'BRB'),
(19, 'BD', 'Bangladesh', 'BDT', '050', 'BGD'),
(20, 'BE', 'Belgium', 'EUR', '056', 'BEL'),
(21, 'BF', 'Burkina Faso', 'XOF', '854', 'BFA'),
(22, 'BG', 'Bulgaria', 'BGN', '100', 'BGR'),
(23, 'BH', 'Bahrain', 'BHD', '048', 'BHR'),
(24, 'BI', 'Burundi', 'BIF', '108', 'BDI'),
(25, 'BJ', 'Benin', 'XOF', '204', 'BEN'),
(26, 'BL', 'Saint Barthélemy', 'EUR', '652', 'BLM'),
(27, 'BM', 'Bermuda', 'BMD', '060', 'BMU'),
(28, 'BN', 'Brunei', 'BND', '096', 'BRN'),
(29, 'BO', 'Bolivia', 'BOB', '068', 'BOL'),
(30, 'BQ', 'Bonaire', 'USD', '535', 'BES'),
(31, 'BR', 'Brazil', 'BRL', '076', 'BRA'),
(32, 'BS', 'Bahamas', 'BSD', '044', 'BHS'),
(33, 'BT', 'Bhutan', 'BTN', '064', 'BTN'),
(34, 'BV', 'Bouvet Island', 'NOK', '074', 'BVT'),
(35, 'BW', 'Botswana', 'BWP', '072', 'BWA'),
(36, 'BY', 'Belarus', 'BYR', '112', 'BLR'),
(37, 'BZ', 'Belize', 'BZD', '084', 'BLZ'),
(38, 'CA', 'Canada', 'CAD', '124', 'CAN'),
(39, 'CC', 'Cocos [Keeling] Islands', 'AUD', '166', 'CCK'),
(40, 'CD', 'Democratic Republic of the Congo', 'CDF', '180', 'COD'),
(41, 'CF', 'Central African Republic', 'XAF', '140', 'CAF'),
(42, 'CG', 'Republic of the Congo', 'XAF', '178', 'COG'),
(43, 'CH', 'Switzerland', 'CHF', '756', 'CHE'),
(44, 'CI', 'Ivory Coast', 'XOF', '384', 'CIV'),
(45, 'CK', 'Cook Islands', 'NZD', '184', 'COK'),
(46, 'CL', 'Chile', 'CLP', '152', 'CHL'),
(47, 'CM', 'Cameroon', 'XAF', '120', 'CMR'),
(48, 'CN', 'China', 'CNY', '156', 'CHN'),
(49, 'CO', 'Colombia', 'COP', '170', 'COL'),
(50, 'CR', 'Costa Rica', 'CRC', '188', 'CRI'),
(51, 'CU', 'Cuba', 'CUP', '192', 'CUB'),
(52, 'CV', 'Cape Verde', 'CVE', '132', 'CPV'),
(53, 'CW', 'Curacao', 'ANG', '531', 'CUW'),
(54, 'CX', 'Christmas Island', 'AUD', '162', 'CXR'),
(55, 'CY', 'Cyprus', 'EUR', '196', 'CYP'),
(56, 'CZ', 'Czechia', 'CZK', '203', 'CZE'),
(57, 'DE', 'Germany', 'EUR', '276', 'DEU'),
(58, 'DJ', 'Djibouti', 'DJF', '262', 'DJI'),
(59, 'DK', 'Denmark', 'DKK', '208', 'DNK'),
(60, 'DM', 'Dominica', 'XCD', '212', 'DMA'),
(61, 'DO', 'Dominican Republic', 'DOP', '214', 'DOM'),
(62, 'DZ', 'Algeria', 'DZD', '012', 'DZA'),
(63, 'EC', 'Ecuador', 'USD', '218', 'ECU'),
(64, 'EE', 'Estonia', 'EUR', '233', 'EST'),
(65, 'EG', 'Egypt', 'EGP', '818', 'EGY'),
(66, 'EH', 'Western Sahara', 'MAD', '732', 'ESH'),
(67, 'ER', 'Eritrea', 'ERN', '232', 'ERI'),
(68, 'ES', 'Spain', 'EUR', '724', 'ESP'),
(69, 'ET', 'Ethiopia', 'ETB', '231', 'ETH'),
(70, 'FI', 'Finland', 'EUR', '246', 'FIN'),
(71, 'FJ', 'Fiji', 'FJD', '242', 'FJI'),
(72, 'FK', 'Falkland Islands', 'FKP', '238', 'FLK'),
(73, 'FM', 'Micronesia', 'USD', '583', 'FSM'),
(74, 'FO', 'Faroe Islands', 'DKK', '234', 'FRO'),
(75, 'FR', 'France', 'EUR', '250', 'FRA'),
(76, 'GA', 'Gabon', 'XAF', '266', 'GAB'),
(77, 'GB', 'United Kingdom', 'GBP', '826', 'GBR'),
(78, 'GD', 'Grenada', 'XCD', '308', 'GRD'),
(79, 'GE', 'Georgia', 'GEL', '268', 'GEO'),
(80, 'GF', 'French Guiana', 'EUR', '254', 'GUF'),
(81, 'GG', 'Guernsey', 'GBP', '831', 'GGY'),
(82, 'GH', 'Ghana', 'GHS', '288', 'GHA'),
(83, 'GI', 'Gibraltar', 'GIP', '292', 'GIB'),
(84, 'GL', 'Greenland', 'DKK', '304', 'GRL'),
(85, 'GM', 'Gambia', 'GMD', '270', 'GMB'),
(86, 'GN', 'Guinea', 'GNF', '324', 'GIN'),
(87, 'GP', 'Guadeloupe', 'EUR', '312', 'GLP'),
(88, 'GQ', 'Equatorial Guinea', 'XAF', '226', 'GNQ'),
(89, 'GR', 'Greece', 'EUR', '300', 'GRC'),
(90, 'GS', 'South Georgia and the South Sandwich Islands', 'GBP', '239', 'SGS'),
(91, 'GT', 'Guatemala', 'GTQ', '320', 'GTM'),
(92, 'GU', 'Guam', 'USD', '316', 'GUM'),
(93, 'GW', 'Guinea-Bissau', 'XOF', '624', 'GNB'),
(94, 'GY', 'Guyana', 'GYD', '328', 'GUY'),
(95, 'HK', 'Hong Kong', 'HKD', '344', 'HKG'),
(96, 'HM', 'Heard Island and McDonald Islands', 'AUD', '334', 'HMD'),
(97, 'HN', 'Honduras', 'HNL', '340', 'HND'),
(98, 'HR', 'Croatia', 'HRK', '191', 'HRV'),
(99, 'HT', 'Haiti', 'HTG', '332', 'HTI'),
(100, 'HU', 'Hungary', 'HUF', '348', 'HUN'),
(101, 'ID', 'Indonesia', 'IDR', '360', 'IDN'),
(102, 'IE', 'Ireland', 'EUR', '372', 'IRL'),
(103, 'IL', 'Israel', 'ILS', '376', 'ISR'),
(104, 'IM', 'Isle of Man', 'GBP', '833', 'IMN'),
(105, 'IN', 'India', 'INR', '356', 'IND'),
(106, 'IO', 'British Indian Ocean Territory', 'USD', '086', 'IOT'),
(107, 'IQ', 'Iraq', 'IQD', '368', 'IRQ'),
(108, 'IR', 'Iran', 'IRR', '364', 'IRN'),
(109, 'IS', 'Iceland', 'ISK', '352', 'ISL'),
(110, 'IT', 'Italy', 'EUR', '380', 'ITA'),
(111, 'JE', 'Jersey', 'GBP', '832', 'JEY'),
(112, 'JM', 'Jamaica', 'JMD', '388', 'JAM'),
(113, 'JO', 'Jordan', 'JOD', '400', 'JOR'),
(114, 'JP', 'Japan', 'JPY', '392', 'JPN'),
(115, 'KE', 'Kenya', 'KES', '404', 'KEN'),
(116, 'KG', 'Kyrgyzstan', 'KGS', '417', 'KGZ'),
(117, 'KH', 'Cambodia', 'KHR', '116', 'KHM'),
(118, 'KI', 'Kiribati', 'AUD', '296', 'KIR'),
(119, 'KM', 'Comoros', 'KMF', '174', 'COM'),
(120, 'KN', 'Saint Kitts and Nevis', 'XCD', '659', 'KNA'),
(121, 'KP', 'North Korea', 'KPW', '408', 'PRK'),
(122, 'KR', 'South Korea', 'KRW', '410', 'KOR'),
(123, 'KW', 'Kuwait', 'KWD', '414', 'KWT'),
(124, 'KY', 'Cayman Islands', 'KYD', '136', 'CYM'),
(125, 'KZ', 'Kazakhstan', 'KZT', '398', 'KAZ'),
(126, 'LA', 'Laos', 'LAK', '418', 'LAO'),
(127, 'LB', 'Lebanon', 'LBP', '422', 'LBN'),
(128, 'LC', 'Saint Lucia', 'XCD', '662', 'LCA'),
(129, 'LI', 'Liechtenstein', 'CHF', '438', 'LIE'),
(130, 'LK', 'Sri Lanka', 'LKR', '144', 'LKA'),
(131, 'LR', 'Liberia', 'LRD', '430', 'LBR'),
(132, 'LS', 'Lesotho', 'LSL', '426', 'LSO'),
(133, 'LT', 'Lithuania', 'EUR', '440', 'LTU'),
(134, 'LU', 'Luxembourg', 'EUR', '442', 'LUX'),
(135, 'LV', 'Latvia', 'EUR', '428', 'LVA'),
(136, 'LY', 'Libya', 'LYD', '434', 'LBY'),
(137, 'MA', 'Morocco', 'MAD', '504', 'MAR'),
(138, 'MC', 'Monaco', 'EUR', '492', 'MCO'),
(139, 'MD', 'Moldova', 'MDL', '498', 'MDA'),
(140, 'ME', 'Montenegro', 'EUR', '499', 'MNE'),
(141, 'MF', 'Saint Martin', 'EUR', '663', 'MAF'),
(142, 'MG', 'Madagascar', 'MGA', '450', 'MDG'),
(143, 'MH', 'Marshall Islands', 'USD', '584', 'MHL'),
(144, 'MK', 'Macedonia', 'MKD', '807', 'MKD'),
(145, 'ML', 'Mali', 'XOF', '466', 'MLI'),
(146, 'MM', 'Myanmar [Burma]', 'MMK', '104', 'MMR'),
(147, 'MN', 'Mongolia', 'MNT', '496', 'MNG'),
(148, 'MO', 'Macao', 'MOP', '446', 'MAC'),
(149, 'MP', 'Northern Mariana Islands', 'USD', '580', 'MNP'),
(150, 'MQ', 'Martinique', 'EUR', '474', 'MTQ'),
(151, 'MR', 'Mauritania', 'MRO', '478', 'MRT'),
(152, 'MS', 'Montserrat', 'XCD', '500', 'MSR'),
(153, 'MT', 'Malta', 'EUR', '470', 'MLT'),
(154, 'MU', 'Mauritius', 'MUR', '480', 'MUS'),
(155, 'MV', 'Maldives', 'MVR', '462', 'MDV'),
(156, 'MW', 'Malawi', 'MWK', '454', 'MWI'),
(157, 'MX', 'Mexico', 'MXN', '484', 'MEX'),
(158, 'MY', 'Malaysia', 'MYR', '458', 'MYS'),
(159, 'MZ', 'Mozambique', 'MZN', '508', 'MOZ'),
(160, 'NA', 'Namibia', 'NAD', '516', 'NAM'),
(161, 'NC', 'New Caledonia', 'XPF', '540', 'NCL'),
(162, 'NE', 'Niger', 'XOF', '562', 'NER'),
(163, 'NF', 'Norfolk Island', 'AUD', '574', 'NFK'),
(164, 'NG', 'Nigeria', 'NGN', '566', 'NGA'),
(165, 'NI', 'Nicaragua', 'NIO', '558', 'NIC'),
(166, 'NL', 'Netherlands', 'EUR', '528', 'NLD'),
(167, 'NO', 'Norway', 'NOK', '578', 'NOR'),
(168, 'NP', 'Nepal', 'NPR', '524', 'NPL'),
(169, 'NR', 'Nauru', 'AUD', '520', 'NRU'),
(170, 'NU', 'Niue', 'NZD', '570', 'NIU'),
(171, 'NZ', 'New Zealand', 'NZD', '554', 'NZL'),
(172, 'OM', 'Oman', 'OMR', '512', 'OMN'),
(173, 'PA', 'Panama', 'PAB', '591', 'PAN'),
(174, 'PE', 'Peru', 'PEN', '604', 'PER'),
(175, 'PF', 'French Polynesia', 'XPF', '258', 'PYF'),
(176, 'PG', 'Papua New Guinea', 'PGK', '598', 'PNG'),
(177, 'PH', 'Philippines', 'PHP', '608', 'PHL'),
(178, 'PK', 'Pakistan', 'PKR', '586', 'PAK'),
(179, 'PL', 'Poland', 'PLN', '616', 'POL'),
(180, 'PM', 'Saint Pierre and Miquelon', 'EUR', '666', 'SPM'),
(181, 'PN', 'Pitcairn Islands', 'NZD', '612', 'PCN'),
(182, 'PR', 'Puerto Rico', 'USD', '630', 'PRI'),
(183, 'PS', 'Palestine', 'ILS', '275', 'PSE'),
(184, 'PT', 'Portugal', 'EUR', '620', 'PRT'),
(185, 'PW', 'Palau', 'USD', '585', 'PLW'),
(186, 'PY', 'Paraguay', 'PYG', '600', 'PRY'),
(187, 'QA', 'Qatar', 'QAR', '634', 'QAT'),
(188, 'RE', 'Réunion', 'EUR', '638', 'REU'),
(189, 'RO', 'Romania', 'RON', '642', 'ROU'),
(190, 'RS', 'Serbia', 'RSD', '688', 'SRB'),
(191, 'RU', 'Russia', 'RUB', '643', 'RUS'),
(192, 'RW', 'Rwanda', 'RWF', '646', 'RWA'),
(193, 'SA', 'Saudi Arabia', 'SAR', '682', 'SAU'),
(194, 'SB', 'Solomon Islands', 'SBD', '090', 'SLB'),
(195, 'SC', 'Seychelles', 'SCR', '690', 'SYC'),
(196, 'SD', 'Sudan', 'SDG', '729', 'SDN'),
(197, 'SE', 'Sweden', 'SEK', '752', 'SWE'),
(198, 'SG', 'Singapore', 'SGD', '702', 'SGP'),
(199, 'SH', 'Saint Helena', 'SHP', '654', 'SHN'),
(200, 'SI', 'Slovenia', 'EUR', '705', 'SVN'),
(201, 'SJ', 'Svalbard and Jan Mayen', 'NOK', '744', 'SJM'),
(202, 'SK', 'Slovakia', 'EUR', '703', 'SVK'),
(203, 'SL', 'Sierra Leone', 'SLL', '694', 'SLE'),
(204, 'SM', 'San Marino', 'EUR', '674', 'SMR'),
(205, 'SN', 'Senegal', 'XOF', '686', 'SEN'),
(206, 'SO', 'Somalia', 'SOS', '706', 'SOM'),
(207, 'SR', 'Suriname', 'SRD', '740', 'SUR'),
(208, 'SS', 'South Sudan', 'SSP', '728', 'SSD'),
(209, 'ST', 'São Tomé and Príncipe', 'STD', '678', 'STP'),
(210, 'SV', 'El Salvador', 'USD', '222', 'SLV'),
(211, 'SX', 'Sint Maarten', 'ANG', '534', 'SXM'),
(212, 'SY', 'Syria', 'SYP', '760', 'SYR'),
(213, 'SZ', 'Swaziland', 'SZL', '748', 'SWZ'),
(214, 'TC', 'Turks and Caicos Islands', 'USD', '796', 'TCA'),
(215, 'TD', 'Chad', 'XAF', '148', 'TCD'),
(216, 'TF', 'French Southern Territories', 'EUR', '260', 'ATF'),
(217, 'TG', 'Togo', 'XOF', '768', 'TGO'),
(218, 'TH', 'Thailand', 'THB', '764', 'THA'),
(219, 'TJ', 'Tajikistan', 'TJS', '762', 'TJK'),
(220, 'TK', 'Tokelau', 'NZD', '772', 'TKL'),
(221, 'TL', 'East Timor', 'USD', '626', 'TLS'),
(222, 'TM', 'Turkmenistan', 'TMT', '795', 'TKM'),
(223, 'TN', 'Tunisia', 'TND', '788', 'TUN'),
(224, 'TO', 'Tonga', 'TOP', '776', 'TON'),
(225, 'TR', 'Turkey', 'TRY', '792', 'TUR'),
(226, 'TT', 'Trinidad and Tobago', 'TTD', '780', 'TTO'),
(227, 'TV', 'Tuvalu', 'AUD', '798', 'TUV'),
(228, 'TW', 'Taiwan', 'TWD', '158', 'TWN'),
(229, 'TZ', 'Tanzania', 'TZS', '834', 'TZA'),
(230, 'UA', 'Ukraine', 'UAH', '804', 'UKR'),
(231, 'UG', 'Uganda', 'UGX', '800', 'UGA'),
(232, 'UM', 'U.S. Minor Outlying Islands', 'USD', '581', 'UMI'),
(233, 'US', 'United States', 'USD', '840', 'USA'),
(234, 'UY', 'Uruguay', 'UYU', '858', 'URY'),
(235, 'UZ', 'Uzbekistan', 'UZS', '860', 'UZB'),
(236, 'VA', 'Vatican City', 'EUR', '336', 'VAT'),
(237, 'VC', 'Saint Vincent and the Grenadines', 'XCD', '670', 'VCT'),
(238, 'VE', 'Venezuela', 'VEF', '862', 'VEN'),
(239, 'VG', 'British Virgin Islands', 'USD', '092', 'VGB'),
(240, 'VI', 'U.S. Virgin Islands', 'USD', '850', 'VIR'),
(241, 'VN', 'Vietnam', 'VND', '704', 'VNM'),
(242, 'VU', 'Vanuatu', 'VUV', '548', 'VUT'),
(243, 'WF', 'Wallis and Futuna', 'XPF', '876', 'WLF'),
(244, 'WS', 'Samoa', 'WST', '882', 'WSM'),
(245, 'XK', 'Kosovo', 'EUR', '0', 'XKX'),
(246, 'YE', 'Yemen', 'YER', '887', 'YEM'),
(247, 'YT', 'Mayotte', 'EUR', '175', 'MYT'),
(248, 'ZA', 'South Africa', 'ZAR', '710', 'ZAF'),
(249, 'ZM', 'Zambia', 'ZMW', '894', 'ZMB'),
(250, 'ZW', 'Zimbabwe', 'ZWL', '716', 'ZWE');";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();


                    $strSql="CREATE DEFINER=`root`@`%` PROCEDURE `".$dbname."`.`SP_updateinventory`(
	IN `in_itemunkid` BIGINT(10),
    IN `in_quantity` INT(10),
    IN `in_itemunitid` INT(10),
    IN `in_recipetype` INT(10),
    IN `in_operation` boolean,
	IN `in_folioid` INT(10), 
	IN `in_detailid` INT(10),
    IN `in_isvoid` boolean,
	IN `in_companyid` INT(10), 
    IN `in_locationid` INT(10), 
    OUT `final_status` VARCHAR(100))
BEGIN
	DECLARE v_error INTEGER DEFAULT 0;
    DECLARE v_isrel INTEGER DEFAULT 0;
    DECLARE v_rawmatrialid BIGINT(20) DEFAULT 0;
    DECLARE v_raw_locid BIGINT(20) DEFAULT 0;
    DECLARE v_storeid INT(11) DEFAULT 0;
    DECLARE v_inventory DECIMAL(23,10) DEFAULT 0;
    DECLARE v_totalinventory DECIMAL(23,10) DEFAULT 0;
    DECLARE v_rawinventory DECIMAL(23,10) DEFAULT 0;
    DECLARE v_newinventory DECIMAL(23,10) DEFAULT 0;
    DECLARE v_Detailinventory DECIMAL(23,10) DEFAULT 0;
	DECLARE v_relitemqty INT(10) DEFAULT 0;
    DECLARE v_reltotalqty INT(10) DEFAULT 0;
    DECLARE v_unitid INT(11) DEFAULT 0;
    DECLARE v_done INT(1) DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET v_error = 1;

    START TRANSACTION; 
    
		SELECT COUNT(relationunkid) INTO v_isrel FROM cforder_inventory_rel WHERE lnkfoliodetailid = in_detailid AND lnkfolioid = in_folioid AND lnkitemid = in_itemunkid AND lnkitemunitid = in_itemunitid AND companyid = in_companyid AND locationid = in_locationid ;

   	IF v_isrel = 0 THEN

		BEGIN
		DECLARE r_detail CURSOR FOR SELECT lnkrawmaterialid,unit,lnkstoreid FROM cfmenu_item_recipe
								WHERE lnkitemid = in_itemunkid AND recipe_type = in_recipetype
                                AND companyid = in_companyid AND locationid = in_locationid AND lnkitemunitid=in_itemunitid;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done=1;
		SET v_done = 0;
        OPEN r_detail;
		REPEAT
		FETCH r_detail INTO v_rawmatrialid,v_inventory,v_storeid;

		IF NOT v_done THEN
			INSERT INTO cforder_inventory_rel(lnkfoliodetailid,lnkfolioid,lnkitemid,quantity,lnkitemunitid,recipe_type,lnkrawmaterialid,inventory,companyid,locationid,storeid,createddatetime)
            VALUE (in_detailid,in_folioid,in_itemunkid,in_quantity,in_itemunitid,in_recipetype,v_rawmatrialid,v_inventory,in_companyid,in_locationid,v_storeid,NOW());

            -- Manage crdb for inventory

            SELECT u.unitunkid INTO v_unitid FROM cfrawmaterial CFR
            INNER JOIN cfunit u ON u.measuretype = CFR.measuretype AND u.is_systemdefined=1 AND u.companyid = in_companyid
            WHERE CFR.id = v_rawmatrialid AND CFR.companyid = in_companyid limit 1;

            SELECT id INTO v_raw_locid FROM cfrawmaterial_loc
            WHERE lnkrawmaterialid = v_rawmatrialid AND storeid = v_storeid
            AND companyid = in_companyid;

            IF NOT v_raw_locid THEN

                INSERT INTO cfrawmaterial_loc(lnkrawmaterialid,inv_unitid,storeid,companyid)
                VALUE(v_rawmatrialid,v_unitid,v_storeid,in_companyid);
                SET v_raw_locid = LAST_INSERT_ID();

            END IF;
            SET v_totalinventory = in_quantity * v_inventory;

			INSERT INTO cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,locationid,companyid,storeid)
            VALUE(v_raw_locid,v_rawmatrialid,v_totalinventory,v_unitid,in_detailid,5,'db',0,NOW(),in_locationid,in_companyid,v_storeid);

			SELECT inventory INTO v_rawinventory FROM cfrawmaterial_loc
			WHERE lnkrawmaterialid = v_rawmatrialid AND storeid = v_storeid
			AND companyid = in_companyid;

			-- Update total rawinventory
			SET v_newinventory = v_rawinventory - v_totalinventory;

			UPDATE cfrawmaterial_loc SET inventory = v_newinventory
			WHERE lnkrawmaterialid = v_rawmatrialid
			AND storeid = v_storeid AND companyid = in_companyid;

       END IF;
        UNTIL v_done END REPEAT;
        CLOSE r_detail;
		END;
		ELSE

		BEGIN
		DECLARE rel_detail CURSOR FOR SELECT lnkrawmaterialid,inventory,quantity,storeid FROM cforder_inventory_rel
								WHERE lnkfoliodetailid = in_detailid AND lnkfolioid = in_folioid AND lnkitemid = in_itemunkid AND lnkitemunitid = in_itemunitid
                                AND companyid = in_companyid AND locationid = in_locationid ;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done=1;
		SET v_done = 0;
        OPEN rel_detail;
		REPEAT
		FETCH rel_detail INTO v_rawmatrialid,v_inventory,v_relitemqty,v_storeid;


		IF NOT v_done THEN

            SET v_totalinventory = in_quantity * v_inventory;

            -- Manage crdb for inventory

            SELECT u.unitunkid INTO v_unitid FROM cfrawmaterial CFR
            INNER JOIN cfunit u ON u.measuretype = CFR.measuretype AND u.is_systemdefined=1 AND u.companyid = in_companyid
            WHERE CFR.id = v_rawmatrialid AND CFR.companyid = in_companyid limit 1;

            SELECT id INTO v_raw_locid FROM cfrawmaterial_loc
            WHERE lnkrawmaterialid = v_rawmatrialid AND storeid = v_storeid
            AND companyid = in_companyid;

            IF NOT v_raw_locid THEN

                INSERT INTO cfrawmaterial_loc(lnkrawmaterialid,inv_unitid,storeid,companyid)
                VALUE(v_rawmatrialid,v_unitid,v_storeid,in_companyid);
                SET v_raw_locid = LAST_INSERT_ID();

            END IF;

            SELECT inventory INTO v_rawinventory FROM cfrawmaterial_loc
            WHERE lnkrawmaterialid = v_rawmatrialid AND storeid = v_storeid
            AND companyid = in_companyid;

			IF in_operation = 1 THEN
				SET v_newinventory = v_rawinventory + v_totalinventory;

                IF in_isvoid = 1 THEN
					DELETE FROM cforder_inventory_rel
					WHERE lnkrawmaterialid = v_rawmatrialid AND lnkfoliodetailid = in_detailid AND lnkfolioid = in_folioid
					AND lnkitemid = in_itemunkid AND lnkitemunitid = in_itemunitid AND companyid = in_companyid AND locationid = in_locationid AND storeid=v_storeid;

                    -- Manage crdb for inventory

                  	INSERT INTO cfinventorydetail(lnkrawmateriallocid,lnkrawmaterialid,inventory,inv_unitid,ref_id,inv_status,crdb,created_user,createddatetime,locationid,companyid,storeid)
                    VALUE(v_raw_locid,v_rawmatrialid,v_totalinventory,v_unitid,in_detailid,5,'cr',0,NOW(),in_locationid,in_companyid,v_storeid);

				ELSE
					SET v_reltotalqty = v_relitemqty - in_quantity;
					SET v_Detailinventory = v_reltotalqty * v_inventory;

					UPDATE cforder_inventory_rel SET quantity = v_reltotalqty
					WHERE lnkrawmaterialid = v_rawmatrialid AND lnkfoliodetailid = in_detailid AND lnkfolioid = in_folioid
					AND lnkitemid = in_itemunkid AND lnkitemunitid = in_itemunitid AND locationid = in_locationid AND companyid = in_companyid AND storeid=v_storeid;

                    -- Manage crdb for inventory

                    UPDATE cfinventorydetail SET inventory=v_Detailinventory,inv_unitid=v_unitid
                    WHERE lnkrawmaterialid = v_rawmatrialid AND ref_id = in_detailid AND inv_status=5 AND
                    locationid = in_locationid AND companyid = in_companyid AND storeid=v_storeid;

                END IF; -- END of is void

                UPDATE cfrawmaterial_loc SET inventory = v_newinventory
				WHERE lnkrawmaterialid = v_rawmatrialid
				AND storeid = v_storeid AND companyid = in_companyid;
            ELSE
				SET v_reltotalqty = v_relitemqty + in_quantity;
				SET v_Detailinventory = v_reltotalqty * v_inventory;

				UPDATE cforder_inventory_rel SET quantity = v_reltotalqty
				WHERE lnkrawmaterialid = v_rawmatrialid AND lnkfoliodetailid = in_detailid AND lnkfolioid = in_folioid
				AND lnkitemid = in_itemunkid AND lnkitemunitid = in_itemunitid AND locationid = in_locationid AND companyid = in_companyid AND storeid=v_storeid;

                -- Manage crdb for inventory

                UPDATE cfinventorydetail SET inventory=v_Detailinventory,inv_unitid=v_unitid
                WHERE lnkrawmaterialid = v_rawmatrialid AND ref_id = in_detailid AND inv_status=5 AND
                locationid = in_locationid AND companyid = in_companyid AND storeid=v_storeid;

				-- Update total rawinventory
				SET v_newinventory = v_rawinventory - v_totalinventory;

				UPDATE cfrawmaterial_loc SET inventory = v_newinventory
				WHERE lnkrawmaterialid = v_rawmatrialid
				AND storeid = v_storeid AND companyid = in_companyid;
			END IF;

        END IF;
        UNTIL v_done END REPEAT;
        CLOSE rel_detail;
		END;

    END IF; -- End of is in rel
    IF v_error=0
		THEN
			SET final_status='Commit';
            COMMIT;
		ELSE
			SET final_status='Rollback';
            ROLLBACK;
    END IF;
END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();


                    $strSql="CREATE DEFINER=`root`@`localhost` PROCEDURE `".$dbname."`.`SP_removefoliodata`(IN in_locationid INT(10), IN in_companyid INT(10), IN in_delete_type tinyint(1), IN in_start_date datetime, IN in_end_date datetime, OUT v_retstatus VARCHAR(100))
BEGIN
    DECLARE v_cnt INT(5) DEFAULT 0;
    DECLARE v_iserror INT(5) DEFAULT 0;
    DECLARE v_done INT(1) DEFAULT 0;
    DECLARE v_foliounkid INT(10) DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET v_iserror = 1;
    
  
    SELECT count(locationid) INTO v_cnt FROM cflocation WHERE locationid=in_locationid AND companyid=in_companyid;
    SET @Error_no1=v_iserror;
    IF v_cnt>0 THEN
    SET @Error_no2=v_iserror;
	

				BEGIN
			
		DECLARE f_detail CURSOR FOR SELECT foliounkid FROM fasfoliomaster
								WHERE opendate >= in_start_date AND opendate <= in_end_date
                                AND companyid = in_companyid AND locationid = in_locationid;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done=1;
    
		SET v_done = 0;
        OPEN f_detail;
		REPEAT
		FETCH f_detail INTO v_foliounkid;

		IF NOT v_done THEN
		insert into saas_cloudpos.tigger_log (`text`) values (v_foliounkid);
			IF in_delete_type = 1 THEN                
            
					UPDATE fasfoliomaster SET isvoid = 1,voiddatetime=now() WHERE opendate >= in_start_date AND opendate <= in_end_date AND locationid=in_locationid AND companyid = in_companyid AND foliounkid=v_foliounkid;				
					UPDATE fasfoliodetail SET isvoid = 1,voiddatetime=now() WHERE locationid=in_locationid AND companyid = in_companyid AND foliounkid=v_foliounkid;
                    
            ELSE
				
					DELETE FROM fasfoliomaster WHERE opendate >= in_start_date AND opendate <= in_end_date AND locationid=in_locationid AND companyid=in_companyid AND foliounkid=v_foliounkid;
					DELETE FROM fasfoliodetail WHERE locationid=in_locationid AND companyid=in_companyid AND foliounkid=v_foliounkid;
					
			END IF;
			
       END IF;
        UNTIL v_done END REPEAT;
        CLOSE f_detail;
		END;
		
           
SET @Error_no3=v_iserror;
          END IF;
    IF v_iserror = 0 THEN
     SET v_retstatus='committing';
     SELECT 'committing' AS finalstatus,@Error_no3 as Error_no3 , v_cnt as cnt,@Error_no2 as Error_no2 , @Error_no1 as Error_no1;
    ELSE
     SET v_retstatus='rollbacking';
     SELECT 'rollbacking' AS finalstatus,@Error_no3 as Error_no3 , v_cnt as cnt,@Error_no2 as Error_no2 , @Error_no1 as Error_no1;
    END IF;
END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();


                    $strSql = "CREATE TRIGGER `".$dbname."`.`cfdiscount_BEFORE_INSERT` BEFORE INSERT ON `cfdiscount` FOR EACH ROW
     BEGIN
          SET @mastertypeid = (SELECT mastertypeunkid FROM fasmastertype WHERE type=NEW.fasmastertype LIMIT 1);
     INSERT INTO fasmaster SET companyid = NEW.companyid,locationid=NEW.locationid, name = NEW.discount, mastertypeunkid = @mastertypeid, crdr = -1, isactive = NEW.isactive;
     SET NEW.lnkmasterunkid = LAST_INSERT_ID();
    END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();

                    $strSql = " CREATE TRIGGER `".$dbname."`.`cfdiscount_BEFORE_UPDATE` BEFORE UPDATE ON `cfdiscount` FOR EACH ROW
    BEGIN
        UPDATE fasmaster SET name = NEW.discount,is_deleted = NEW.is_deleted, isactive = NEW.isactive WHERE masterunkid=OLD.lnkmasterunkid;
    END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();

                    $strSql = " CREATE TRIGGER `".$dbname."`.`cftax_BEFORE_INSERT` BEFORE INSERT ON `cftax` FOR EACH ROW
    BEGIN
        SET @mastertypeid = (SELECT mastertypeunkid FROM fasmastertype WHERE type=NEW.fasmastertype LIMIT 1);
    
        INSERT INTO fasmaster SET companyid = NEW.companyid,locationid=NEW.locationid, name = NEW.tax, mastertypeunkid = @mastertypeid, crdr = '1', isactive = NEW.isactive;
        SET NEW.lnkmasterunkid = LAST_INSERT_ID();
    END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();

                    $strSql = " CREATE TRIGGER `".$dbname."`.`cftax_BEFORE_UPDATE` BEFORE UPDATE ON `cftax` FOR EACH ROW
    BEGIN
    UPDATE fasmaster SET name = NEW.tax,is_deleted = NEW.is_deleted, isactive = NEW.isactive WHERE masterunkid=OLD.lnkmasterunkid;
    END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();
                    
                    $strSql = "CREATE TRIGGER `".$dbname."`.`cfpaymenttype_BEFORE_INSERT` BEFORE INSERT ON `cfpaymenttype` FOR EACH ROW
    BEGIN
        SET @mastertypeid = (SELECT mastertypeunkid FROM fasmastertype WHERE type=NEW.fasmastertype LIMIT 1);
    
    INSERT INTO fasmaster SET companyid = NEW.companyid,locationid=NEW.locationid, name = NEW.paymenttype, mastertypeunkid = @mastertypeid, crdr = '1', isactive = NEW.isactive;
    SET NEW.lnkmasterunkid = LAST_INSERT_ID();
    
    END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();
                    
                    $strSql = "CREATE TRIGGER `".$dbname."`.`cfpaymenttype_BEFORE_UPDATE` BEFORE UPDATE ON `cfpaymenttype` FOR EACH ROW
    BEGIN
    UPDATE fasmaster SET name = NEW.paymenttype, isactive = NEW.isactive, is_deleted = NEW.is_deleted WHERE masterunkid=OLD.lnkmasterunkid;
    END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();

                    //add trigger for menu->item,modifier,modifier item and category
                    //start

                    $strSql = "CREATE TRIGGER `" . $dbname . "`.`cfmenu_category_AFTER_INSERT` AFTER INSERT ON `cfmenu_categories`
 FOR EACH ROW BEGIN

DECLARE parentcatsid text;
DECLARE menusid text;
SET parentcatsid=(SELECT GROUP_CONCAT(DISTINCT CONCAT(@pv:=categoryunkid,',',parent_id) SEPARATOR ',' ) as data
from cfmenu_categories
join (select @pv:=NEW.parent_id)tmp
where parent_id=@pv OR categoryunkid=@pv);

SET menusid=(SELECT GROUP_CONCAT(menuunkid) as data FROM cfmenu_categories WHERE FIND_IN_SET(categoryunkid,parentcatsid));

UPDATE cfmenu SET modifieddatetime = NEW.createddatetime WHERE  companyid=NEW.companyid AND locationid=NEW.locationid ;

UPDATE cfmenu SET modifieddatetime = NEW.createddatetime WHERE  menuunkid=NEW.menuunkid AND companyid=NEW.companyid AND locationid=NEW.locationid ;

END";
/*                    UPDATE cfmenu SET modifieddatetime = NEW.createddatetime WHERE  FIND_IN_SET(menuunkid,menusid) OR FIND_IN_SET(breakfast_category_id,parentcatsid) OR FIND_IN_SET(lunch_category_id,parentcatsid) OR FIND_IN_SET(dinner_category_id,parentcatsid)  AND companyid=NEW.companyid AND locationid=NEW.locationid ;*/

                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();


                    $strSql = "CREATE TRIGGER `" . $dbname . "`.`cfmenu_category_AFTER_UPDATE` AFTER UPDATE ON `cfmenu_categories`
 FOR EACH ROW BEGIN

DECLARE parentcatsid text;
DECLARE menusid text;
SET parentcatsid=(SELECT GROUP_CONCAT(DISTINCT CONCAT(@pv:=categoryunkid,',',parent_id) SEPARATOR ',' ) as data
from cfmenu_categories
join (select @pv:=NEW.parent_id)tmp
where parent_id=@pv OR categoryunkid=@pv);

SET menusid=(SELECT GROUP_CONCAT(menuunkid) as data FROM cfmenu_categories WHERE FIND_IN_SET(categoryunkid,parentcatsid));


UPDATE cfmenu SET modifieddatetime = NEW.createddatetime WHERE  companyid=NEW.companyid AND locationid=NEW.locationid ;

UPDATE cfmenu SET modifieddatetime = NEW.modifieddatetime WHERE  menuunkid=NEW.menuunkid AND companyid=NEW.companyid AND locationid=NEW.locationid ;

END";

                    /*UPDATE cfmenu SET modifieddatetime = NEW.createddatetime WHERE  companyid=NEW.companyid AND locationid=NEW.locationid ;*/
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();

                    $strSql = "CREATE TRIGGER `" . $dbname . "`.`cfmenu_item_AFTER_INSERT` AFTER INSERT ON `cfmenu_items`
 FOR EACH ROW BEGIN

UPDATE cfmenu_categories SET modifieddatetime = NEW.createddatetime WHERE  categoryunkid = NEW.categoryunkid AND companyid=NEW.companyid AND locationid=NEW.locationid ;

UPDATE cfmenu_itemtype SET modifieddatetime = NEW.createddatetime WHERE  typeunkid = NEW.itemtypeunkid AND companyid=NEW.companyid AND locationid=NEW.locationid ;

END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();

                    $strSql = "CREATE TRIGGER `" . $dbname . "`.`cfmenu_item_AFTER_UPDATE` AFTER UPDATE ON `cfmenu_items`
 FOR EACH ROW BEGIN

UPDATE cfmenu_categories SET modifieddatetime = NEW.modifieddatetime WHERE  categoryunkid = NEW.categoryunkid AND companyid=NEW.companyid AND locationid=NEW.locationid ;

UPDATE cfmenu_itemtype SET modifieddatetime = NEW.modifieddatetime WHERE  typeunkid = NEW.itemtypeunkid AND companyid=NEW.companyid AND locationid=NEW.locationid ;

END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();

                    $strSql = "CREATE TRIGGER `" . $dbname . "`.`cfmenu_modifier_AFTER_UPDATE` AFTER UPDATE ON `cfmenu_modifiers`
 FOR EACH ROW BEGIN
DECLARE itemid text;
SET itemid= (SELECT GROUP_CONCAT(lnkitemid) as data FROM fditem_modifier_relation WHERE lnkmodifierid=OLD.modifierunkid AND companyid=NEW.companyid AND locationid=NEW.locationid);

UPDATE cfmenu_items SET modifieddatetime = NEW.modifieddatetime WHERE   FIND_IN_SET(itemunkid,itemid) AND companyid=NEW.companyid AND locationid=NEW.locationid ;

END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();

                    $strSql = "CREATE TRIGGER `" . $dbname . "`.`cfmenu_modifier_item_AFTER_INSERT` AFTER INSERT ON `cfmenu_modifier_items`
 FOR EACH ROW BEGIN
SET @modifierid = (SELECT modifierunkid FROM cfmenu_modifier_items WHERE modifieritemunkid=NEW.modifieritemunkid AND companyid=NEW.companyid AND locationid=NEW.locationid LIMIT 1);

UPDATE cfmenu_modifiers SET modifieddatetime = NEW.createddatetime WHERE modifierunkid=@modifierid AND companyid=NEW.companyid AND locationid=NEW.locationid ;
END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();

                    $strSql = "CREATE TRIGGER `" . $dbname . "`.`cfmenu_modifier_item_AFTER_UPDATE` AFTER UPDATE ON `cfmenu_modifier_items`
 FOR EACH ROW BEGIN
SET @modifierid = (SELECT modifierunkid FROM cfmenu_modifier_items WHERE modifieritemunkid=OLD.modifieritemunkid AND companyid=NEW.companyid AND locationid=NEW.locationid LIMIT 1);

UPDATE cfmenu_modifiers SET modifieddatetime = NEW.modifieddatetime WHERE modifierunkid=@modifierid AND companyid=NEW.companyid AND locationid=NEW.locationid ;
END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();



                    $strSql = "CREATE TRIGGER `" . $dbname . "`.`cfnocharge_AFTER_UPDATE` AFTER UPDATE ON `cfnocharge`
 FOR EACH ROW BEGIN
UPDATE trcontact SET is_deleted = NEW.is_deleted,is_active = NEW.is_active WHERE companyid=OLD.companyid AND locationid=OLD.locationid AND contacttype=4 AND contactpersonunkid=NEW.nochargeunkid; 
END";
                    $dao->initCommand($strSql);
                    $dao->executeNonQuery();

                    //end

                    return json_encode(array("Success" => "True", "Message" => "Database created successfully"));
                } else {
                    return json_encode(array("Success" => "False", "Message" => "Internal Error. Contact to developer urgently."));
                }
            }else{
                return json_encode(array("Success"=>"False", "Message"=>"Same database already exists"));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addDatabase - '.$e);
        }
    }
    // Dashboard
    public function getdatabaselistwithsize()
    {
        try
        {
            $this->log->logIt($this->module.' - getdatabaselistwothsize');
            $dao = new \dao();
            $strSql = " SELECT INF.table_schema AS databaseweight, SUM(round(((INF.data_length + INF.index_length) / 1024 / 1024),2)) AS mb".
                    " FROM information_schema.TABLES AS INF INNER JOIN sysdatabase AS SYS ON SYS.dbname = INF.table_schema GROUP BY table_schema";
                    
            $dao->initCommand($strSql);
            $res = $dao->executeQuery();
            
            $strSql = "SELECT id, dbname, propertycount AS cnt FROM sysdatabase WHERE 1";
            $dao->initCommand($strSql);
            $res1 = $dao->executeQuery();
            $res_ar = \util\util::getkeyfieldarray($res1,'dbname');
            for($i=0; $i<count($res); $i++)
            {
                if(isset($res_ar[$res[$i]['databaseweight']]))
                {
                    $res[$i]['id'] = $res_ar[$res[$i]['databaseweight']]['id'];
                    $res[$i]['propertycount'] = $res_ar[$res[$i]['databaseweight']]['cnt'];
                }
            }
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getdatabaselistwothsize - '.$e);
        }
    }
    
    public function getcardvalue()
    {
        try
        {
            $this->log->logIt($this->module.' - getcardvalue');
            $dao = new \dao();
            $retarr = array();
            $strSql = "SELECT count(syscompanyid) AS cnt FROM saas_retailpossystem.syscompany WHERE isactive=1";
            $dao->initCommand($strSql);
            $res = $dao->executeRow();
            $retarr['restcnt'] =$res['cnt'];
            
            $strSql = "SELECT count(id) AS cnt FROM saas_retailpossystem.sysdatabase";
            $dao->initCommand($strSql);
            $res = $dao->executeRow();
            $retarr['dbcnt'] =$res['cnt'];

            $strSql = "SELECT dbname FROM saas_retailpossystem.sysdatabase";
            $dao->initCommand($strSql);
            $record = $dao->executeQuery();

            $order = 0;
            $itemsold = 0;
            foreach($record AS $val)
            {
                
                $strSql = " SELECT count(foliounkid) AS cnt FROM ".$val['dbname'].".fasfoliomaster".
                        " WHERE CAST(opendate AS DATE)=CAST(NOW() AS DATE) and isvoid=0";
                
                $dao->initCommand($strSql);
                
                $rec = $dao->executeRow();
                $order = $order + $rec['cnt'];
                
                $strSql = " SELECT count(FASFD.detailunkid) AS cnt FROM ".$val['dbname'].".fasfoliodetail AS FASFD".
                       " INNER JOIN ".$val['dbname'].".fasmaster AS FASM ON FASFD.masterunkid = FASM.masterunkid ".
                        " WHERE (FASM.mastertypeunkid=1 OR FASM.mastertypeunkid=8) AND FASFD.isvoid=0  AND CAST(trandate AS DATE) = CAST(NOW() AS DATE)";
                $dao->initCommand($strSql);
                $rec1 = $dao->executeRow();
                
                $itemsold = $itemsold + $rec1['cnt'];
            }
            $retarr['order'] = $order;
            $retarr['itemsold'] = $itemsold;

            return $retarr;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getcardvalue - '.$e);
        }
    }

    public function toggleststus($data)
    {
        try {
            $this->log->logIt($this->module . ' - toggleststus');
            $dao = new \dao();

            if (count($data) > 0 && isset($data['module']) && isset($data['id'])) {

                if($data['module'] == 'taglinenotes')
                {
                    $TaglineNoteDao = new \database\taglinenotesdao();
                    $data['groupid'] = $data['id'];
                    $notesdata = $TaglineNoteDao->getNotesdetails($data);

                    if($notesdata['is_active'] == 0)
                    {
                        $notesdata['groupid'] = $data['id'];
                        $dublicaterecords = $TaglineNoteDao->checkDublicatenote($notesdata);
                        if(count($dublicaterecords)>0)
                        {
                            return json_encode(array("Success"=>"False","Message"=>"Between this Date and Time OR For this Country Interval Notes Allready Exist!"));
                        }
                    }
                }
                $Objstaticarray = new \common\staticarray();
                $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $pd = $Objstaticarray->auditlogmodules[$data['module']]['pd'];
                $cn = $Objstaticarray->auditlogmodules[$data['module']]['status_cn'];

                $strSql = "UPDATE " . $table . " SET  modifieddatetime=NOW() , " . $cn . " = IF(" . $cn . "=1,0,1) WHERE " . $pd . " =:id";

                $dao->initCommand($strSql);
                $dao->addParameter(':id',$data['id']);
                $dao->executeNonQuery();

                return html_entity_decode(json_encode(array("Success" => "True", "Message" => "Status changed successfully.")));
            } else
                return html_entity_decode(json_encode(array("Success" => "False", "Message" => "Internal Error !")));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - toggleststus - ' . $e);
        }
    }

    public function remove($data)
    {
        try {
            $this->log->logIt($this->module . ' - remove');
            $dao = new \dao();

            if (count($data) > 0 && isset($data['module']) && isset($data['id'])) {

                $Objstaticarray = new \common\staticarray();
                $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $pd = $Objstaticarray->auditlogmodules[$data['module']]['pd'];
                $cn = $Objstaticarray->auditlogmodules[$data['module']]['delete_cn'];

                    $strSql = "UPDATE " . $table . " SET " . $cn . "=1,modifieddatetime=NOW() WHERE  " . $pd . " =:id ";

                $dao->initCommand($strSql);
                $dao->addParameter(":id", $data['id']);
                $dao->executeNonQuery();

                return html_entity_decode(json_encode(array("Success" => "True", "Message" =>"Record removed successfully.")));
            } else
                return html_entity_decode(json_encode(array("Success" => "False", "Message" =>"Internal Error !")));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - remove - ' . $e);
        }
    }

}
?>