<?php
namespace database;
use function PHPSTORM_META\type;
use util\util;

class systemNotificationdao
{
    public $module = 'DB_systemNotificationdao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function addNotification($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addNotification');

            $dao = new \dao();

            if ($data['groupid'] == 0) {

                    $strSql = "INSERT INTO system_notification SET 
                            version=:version,releasedate=:releasedate,shortdescription=:shortdescription,is_active=:isactive,createddatetime=NOW(),briefdescription=:briefdescription";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':version',$data['version']);
                    $dao->addParameter(':releasedate',$data['releasedate']);
                    $dao->addParameter(':shortdescription',$data['shortdescription']);
                    $dao->addParameter(':briefdescription',$data['briefdescription']);
                    $dao->addParameter(':isactive',$data['active']);
                    $dao->executeNonQuery();
                    $lastinsertedid = $dao->getLastInsertedId();

                    if($lastinsertedid != '')
                    {
                        $strSql1 = "CALL SP_insert_notification(:isactive,:notificationid)";
                        $dao->initCommand($strSql1);
                        $dao->addParameter(":isactive", $data['active']);
                        $dao->addParameter(":notificationid", $lastinsertedid);
                        $dao->executeNonQuery();
                    }

                    return json_encode(array('Success' => 'True', 'Message' => 'Notification Saved Successfully'));
                } else {

                $strSql = "UPDATE system_notification SET 
                            version=:version,releasedate=:releasedate,shortdescription=:shortdescription,is_active=:isactive,modifieddatetime=NOW(),briefdescription=:briefdescription
                               WHERE notificationid=:notificationid ";
                $dao->initCommand($strSql);
                $dao->addParameter(':version',$data['version']);
                $dao->addParameter(':releasedate',$data['releasedate']);
                $dao->addParameter(':shortdescription',$data['shortdescription']);
                $dao->addParameter(':briefdescription',$data['briefdescription']);
                $dao->addParameter(':isactive',$data['active']);
                $dao->addParameter(':notificationid', $data['groupid']);
                $dao->executeNonQuery();

                return json_encode(array('Success' => 'True', 'Message' => 'Notification Updated Successfully'));
                }

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addNotification - '.$e);
        }
    }

    public function Notificationlist($limit,$offset,$version='',$releasedate='',$shortdes='')
    {
        try
        {
            $this->log->logIt($this->module.' - Notificationlist - ');
            $dao = new \dao;

            $strSql = "SELECT notificationid,version,releasedate,shortdescription,is_active FROM system_notification WHERE is_deleted=:is_deleted";
            if($releasedate!="" ){
                $releasedate2 = date('Y-m-d',strtotime($releasedate));
                $strSql .= " AND  releasedate LIKE '%".$releasedate2."%'";
            }
            if($version!=""){
                $strSql .= " AND version LIKE '%".$version."%'";
            }
            if($shortdes!=""){
                $strSql .= " AND shortdescription LIKE '%".$shortdes."%'";
            }
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $strSql .=" ORDER BY notificationid DESC";

            $dao->initCommand($strSql);
            $dao->addParameter('is_deleted',0);
            $data = $dao->executeQuery();

            if($limit!="" && ($offset!="" || $offset!=0))
               $dao->initCommand($strSql.$strSqllmt);
            else
               $dao->initCommand($strSql);

            $dao->addParameter('is_deleted',0);
            $rec = $dao->executeQuery();

            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return json_encode($retvalue);        
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return json_encode($retvalue);   
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - Notificationlist - '.$e);
        }
    }

    public function getNotificationdetail($data)
    {
		try
        {
			$this->log->logIt($this->module." - getNotificationdetail");
			$dao = new \dao();
            $notificationid=(isset($data['groupid']))?$data['groupid']:"";
            $strSql = "SELECT version,releasedate,shortdescription,is_active,briefdescription FROM system_notification WHERE notificationid=:notificationid ";
            $dao->initCommand($strSql);
            $dao->addParameter(':notificationid',$notificationid);
            $res = $dao->executeRow();
            return json_encode(array("Success"=>"True","Data"=>$res));
        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getNotificationdetail - ".$e);
			return false; 
		}
	}
}

?>