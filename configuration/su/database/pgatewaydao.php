<?php
namespace database;

class pgatewaydao
{
    public $module = 'DB_pgatewaydao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function addGroup($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addGroup');
            $dao = new \dao();
            $strSqlcheckduplicate="SELECT paymenttypeunkid FROM syspaymenttype WHERE (paymenttype=:paymenttype OR shortcode=:shortcode)";
            if ($data['groupid'] != "" && $data['groupid'] !=0) {
                $strSqlcheckduplicate .= " AND  paymenttypeunkid !=" . $data['groupid'] . "";
            }
            $dao->initCommand($strSqlcheckduplicate);
            $dao->addParameter(':paymenttype',$data['paymentmethod']);
            $dao->addParameter(':shortcode',$data['shortcode']);
            $checkduplicatename=$dao->executeQuery();
            if (count($checkduplicatename) > 0) {
                return json_encode(array('Success'=>'False','Message'=>'Please Provide Unique Name OR Slug'));
            } else {
                if ($data['groupid'] == '0' || $data['groupid'] == 0) {

                    $strSql = "INSERT INTO syspaymenttype SET 
                            paymenttype=:paymenttype,shortcode=:shortcode,type=:type,isactive=:isactive";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':paymenttype',$data['paymentmethod']);
                    $dao->addParameter(':shortcode',$data['shortcode']);
                    $dao->addParameter(':type',$data['type']);
                    $dao->addParameter(':isactive',$data['active']);
                    $dao->executeNonQuery();
                    return json_encode(array('Success' => 'True', 'Message' => 'Payment type saved successfully'));
                } else {
                    $strSql = "UPDATE syspaymenttype SET paymenttype=:paymenttype,shortcode=:shortcode,type=:type,isactive=:isactive
                               WHERE paymenttypeunkid=:paymenttypeunkid ";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':paymenttype',$data['paymentmethod']);
                    $dao->addParameter(':shortcode',$data['shortcode']);
                    $dao->addParameter(':type',$data['type']);
                    $dao->addParameter(':isactive',$data['active']);
                    $dao->addParameter(':paymenttypeunkid', $data['groupid']);
                    $dao->executeNonQuery();

                    $strSql1 = "CALL SP_update_paymenttypes(:paymentmethod,:type,:active,:shortcode)";
                    $dao->initCommand($strSql1);
                    $dao->addParameter(":paymentmethod", $data['paymentmethod']);
                    $dao->addParameter(":type", $data['type']);
                    $dao->addParameter(":active", $data['active']);
                    $dao->addParameter(":shortcode", $data['shortcode']);
                    $dao->executeNonQuery();


                    return json_encode(array('Success' => 'True', 'Message' => 'Payment type updated successfully'));
                }
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addGroup - '.$e);
        }
    }

    public function grouplist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - grouplist - '.$name);
            $dao = new \dao;

            $strSql = "SELECT * FROM syspaymenttype WHERE 1 ";
            if($name!=""){
                $strSql .= " AND paymenttype LIKE '%".$name."%'";
            }
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $strSql .=" ORDER BY paymenttype ASC";
            $dao->initCommand($strSql);

            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);

            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return json_encode($retvalue);        
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return json_encode($retvalue);   
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - grouplist - '.$e);
        }
    }

    public function gerecords()
    {
        try
        {
            $this->log->logIt($this->module.' - gerecords - ');
            $dao = new \dao;

            $strSql = "SELECT * FROM syspaymenttype WHERE isactive=1 ";

            $strSql .=" ORDER BY paymenttype ASC";
            $dao->initCommand($strSql);

            $rec = $dao->executeQuery();

            return $rec;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - gerecords - '.$e);
        }
    }

    public function getgroupname($data)
    {
		try
        {
			$this->log->logIt($this->module." - getpaymenttype");
			$dao = new \dao();
            $privilegegroupunkid=(isset($data['groupid']))?$data['groupid']:"";
            $strSql = "SELECT * 
                        FROM syspaymenttype 
                        WHERE paymenttypeunkid=:paymenttypeunkid ";
            $dao->initCommand($strSql);
            $dao->addParameter(':paymenttypeunkid',$privilegegroupunkid);
            $res = $dao->executeRow();
            return json_encode(array("Success"=>"True","Data"=>$res));
        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getpaymenttype - ".$e);
			return false; 
		}
	}
}

?>