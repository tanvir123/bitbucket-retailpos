<?php
namespace database;
use util\util;

class taglinenotesdao
{
    public $module = 'DB_taglinenotesdao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function addNotes($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addNotes');

            $dao = new \dao();

            if($data['country'] == 'null')
            {
                $data['country'] = '';
            }

            $checkdublicate =  SELF::checkDublicatenote($data);

            if(count($checkdublicate) >0)
            {
                return json_encode(array("Success"=>"False","Message"=>"Between this Date and Time OR For this Country Interval Notes Allready Exist!"));
            }

            if ($data['groupid'] == 0) {

                    $strSql = "INSERT INTO tagline_notes SET 
                            taglinenotes=:taglinenotes,displaystartdate=:displaystartdate,displayenddate=:displayenddate,is_active=:isactive,createddatetime=NOW(),country=:country";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':taglinenotes',$data['taglinenotes']);
                    $dao->addParameter(':displaystartdate',$data['displaystartdate']);
                    $dao->addParameter(':displayenddate',$data['displayenddate']);
                    $dao->addParameter(':isactive',$data['active']);
                    $dao->addParameter(':country',$data['country']);
                    $dao->executeNonQuery();
                    return json_encode(array('Success' => 'True', 'Message' => 'Notes Saved Successfully'));
                } else {
                    $strSql = "UPDATE tagline_notes SET 
                            taglinenotes=:taglinenotes,displaystartdate=:displaystartdate,displayenddate=:displayenddate,is_active=:isactive,modifieddatetime=NOW(),country=:country
                               WHERE taglinenoteid=:taglinenoteid ";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':taglinenotes',$data['taglinenotes']);
                    $dao->addParameter(':displaystartdate',$data['displaystartdate']);
                    $dao->addParameter(':displayenddate',$data['displayenddate']);
                    $dao->addParameter(':isactive',$data['active']);
                    $dao->addParameter(':taglinenoteid', $data['groupid']);
                    $dao->addParameter(':country',$data['country']);
                    $dao->executeNonQuery();

                    return json_encode(array('Success' => 'True', 'Message' => 'Notes Updated Successfully!'));
                }

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addNotes - '.$e);
        }
    }

    public function noteslist($limit,$offset,$startdate='',$enddate='',$notes='',$country='')
    {
        try
        {
            $this->log->logIt($this->module.' - noteslist - ');
            $dao = new \dao;

            $strSql = "SELECT taglinenoteid,taglinenotes,displaystartdate,displayenddate,is_active,GROUP_CONCAT(c.countryName ORDER BY c.id) AS country FROM tagline_notes INNER JOIN countrylist c ON FIND_IN_SET(c.id,country) > 0 WHERE is_deleted=:is_deleted";
            if($startdate!="" && $enddate!=""){
                $strSql .= " AND  (displaystartdate  BETWEEN '".$startdate."'  AND '".$enddate."' ) OR (displayenddate  BETWEEN '".$startdate."'  AND '".$enddate."')";
            }
            if($startdate!="" && $enddate==""){
                $strSql .= " AND  displaystartdate  >= '".$startdate."'";
            }
            if($notes!=""){
                $strSql .= " AND taglinenotes LIKE '%".$notes."%'";
            }
            if($country!=""){
                $strSql .= " AND find_in_set('".$country."',country) ";
            }
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $strSql .=" GROUP BY taglinenoteid ORDER BY taglinenoteid ASC";
            $dao->initCommand($strSql);
            $dao->addParameter('is_deleted',0);
            $data = $dao->executeQuery();

            if($limit!="" && ($offset!="" || $offset!=0))
            $dao->initCommand($strSql.$strSqllmt);
        else
            $dao->initCommand($strSql);

            $dao->addParameter('is_deleted',0);
            $rec = $dao->executeQuery();

            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return json_encode($retvalue);        
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return json_encode($retvalue);   
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - noteslist - '.$e);
        }
    }

    public function getNotesdetails($data)
    {
		try
        {
			$this->log->logIt($this->module." - getNotesdetails");
			$dao = new \dao();
            $taglinenoteid=(isset($data['groupid']))?$data['groupid']:"";
            $strSql = "SELECT taglinenotes,displaystartdate,displayenddate,is_active,country FROM tagline_notes WHERE taglinenoteid=:taglinenoteid ";
            $dao->initCommand($strSql);
            $dao->addParameter(':taglinenoteid',$taglinenoteid);
            $res = $dao->executeRow();
            if($data['opcode'] == 'toggleststus')
            {
                return $res;
            }
            else{
                return json_encode(array("Success"=>"True","Data"=>$res));
            }

        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getNotesdetails - ".$e);
			return false; 
		}
	}

    public function checkDublicatenote($data)
    {
        try
        {
            $this->log->logIt($this->module." - checkDublicatenote");
            $dao = new \dao();

            $data['country'] = str_replace(',','|',$data['country']);

            $strSql = "SELECT taglinenoteid FROM tagline_notes WHERE is_active=:is_active AND is_deleted=:is_deleted ";

            $strSql.= " AND (('".$data['displaystartdate']."' BETWEEN displaystartdate AND displayenddate) OR ('".$data['displayenddate']."' BETWEEN displaystartdate AND displayenddate) OR (displaystartdate >= '".$data['displaystartdate']."' AND displayenddate <= '".$data['displayenddate']."')) AND CONCAT(',',country,',') REGEXP \",(".$data['country']."),\" ";

            if ($data['groupid'] != 0) {
                $strSql .= " AND taglinenoteid != '".$data['groupid']."'";
            }


            $dao->initCommand($strSql);
            $dao->addParameter(':is_active', '1');
            $dao->addParameter(':is_deleted', '0');
            $data = $dao->executeQuery();

            return $data;

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - checkDublicatenote - ".$e);
            return false;
        }
    }
}

?>