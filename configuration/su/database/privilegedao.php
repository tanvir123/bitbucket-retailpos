<?php
namespace database;

class privilegedao
{
    public $module = 'DB_privilegedao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function addGroup($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addGroup');
            $dao = new \dao();

            $strSqlcheckduplicate="SELECT id FROM sysprivileges WHERE name=:name";
            if ($data['groupid'] != "" && $data['groupid'] !=0) {
                $strSqlcheckduplicate .= " AND  id != " . $data['groupid'] . "";
            }
            if ($data['privilegegroupid'] != "" && $data['privilegegroupid'] !=0) {
                $strSqlcheckduplicate .= " AND  privilegegroupid = " . $data['privilegegroupid'] . "";
            }
            $dao->initCommand($strSqlcheckduplicate);
            $dao->addParameter(':name',$data['name']);
            $checkduplicatename=$dao->executeQuery();

            if (count($checkduplicatename) > 0) {
                return json_encode(array('Success'=>'False','Message'=>'Please Provide Unique Name'));
            } else {
                if($data['groupid']=='0' || $data['groupid']==0)
                {
                    $strSql = "INSERT INTO sysprivileges SET
                        privilegegroupid=:privilegegroupid, 
                            name=:name";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':name',$data['name']);
                    $dao->addParameter(':privilegegroupid',$data['privilegegroupid']);
                    $dao->executeNonQuery();
                    return json_encode(array('Success'=>'True','Message'=>'Privilege saved successfully'));
                }
                else{
                    $strSql = "UPDATE sysprivileges SET name=:name, privilegegroupid=:privilegegroupid
                WHERE id=:id ";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':name',$data['name']);
                    $dao->addParameter(':privilegegroupid',$data['privilegegroupid']);
                    $dao->addParameter(':id',$data['groupid']);
                     $dao->executeNonQuery();

                    return json_encode(array('Success'=>'True','Message'=>'Privilege updated successfully'));
                }
            }


        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addGroup - '.$e);
        }
    }

    public function grouplist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - grouplist - '.$name);
            $dao = new \dao;

            $strSql = "SELECT P.name,G.groupname,P.id FROM sysprivileges AS P 
                        LEFT JOIN sysprivilegegroup AS G ON P.privilegegroupid = G.privilegegroupunkid WHERE 1 ";
            if($name!=""){
                $strSql .= " AND P.name LIKE '%".$name."%'";
            }
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                 $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $strSql .= "  ORDER BY G.groupname ASC ";
            $dao->initCommand($strSql);

            $data = $dao->executeQuery();
            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);

            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return json_encode($retvalue);
            }
            else{
                 $retvalue = array(array("cnt"=>0,"data"=>[]));
                 return json_encode($retvalue);
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - grouplist - '.$e);
        }
    }
    public function getprivilegename($data)
    {
		try
        {
			$this->log->logIt($this->module." - getprivilegename");
			$dao = new \dao();
            $privilegegroupunkid=(isset($data['groupid']))?$data['groupid']:"";
            $strSql = "SELECT P.name,G.privilegegroupunkid,G.type,P.id FROM sysprivileges AS P 
LEFT JOIN sysprivilegegroup AS G ON P.privilegegroupid=G.privilegegroupunkid
                        WHERE id=:id ";
            $dao->initCommand($strSql);
            $dao->addParameter(':id',$privilegegroupunkid);
            $res = $dao->executeRow();
            return json_encode(array("Success"=>"True","Data"=>$res));

        }
        catch(Exception $e)
        {
			$this->log->logIt($this->module." - getprivilegename - ".$e);
			return false; 
		}
	}



}

?>