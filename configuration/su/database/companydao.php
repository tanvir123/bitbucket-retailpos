<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 25/8/17
 * Time: 4:35 PM
 */

namespace database;
use util\util;
class companydao
{
    public $module = 'DB_companydao';
    public $log;
    public $dbconnect;

    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function loadcompanylist($limit,$offset,$cname,$name,$cid)
    {
        try
        {
            $this->log->logIt($this->module.' - loadcompanylist');
            $dao = new \dao();
            $strSql = "SELECT SCO.syscompanyid,SCO.total_location,DATE_FORMAT(SCO.createddate,'%d/%m/%Y') as created_date".
                " ,TIME_FORMAT(SCO.createddate,'%h:%s:%i %p') as created_time,SCO.companyname".
                " ,IF(SCO.isdemoaccount=1,'Yes','No') as acnt_type".
                " ,SCO.companyemail,SCO.contactperson,SC.contactno, SCO.isactive,SCO.app_id,SCO.app_secret ".
                " FROM syscompany AS SCO INNER JOIN sysclient AS SC ON SC.clientid = SCO.clientid  WHERE 1";
            if($cname!="")
                $strSql .= " AND SCO.companyname LIKE '%".$cname."%'" ;
            if($name!="")
                $strSql .= " AND SCO.contactperson LIKE '%".$name."%'" ;
            if($cid!="")
                $strSql .= " AND SCO.syscompanyid = ".$cid ;

            $strSql .= " ORDER BY createddate DESC";

            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }

            $dao->initCommand($strSql);
            $data = $dao->executeQuery();

            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);

            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return json_encode($retvalue);
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return json_encode($retvalue);
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loadcompanylist - '.$e);
        }
    }
    public function addCompany($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addCompany');
            $dao = new \dao();
            $ObjUtil = new \util\util();
            $api_auth_key = $ObjUtil->generateauthkey();
            //$datetime=\util\util::getLocalDateTime();

            $dbname = $data['select_database'];
            $demo = (isset($data['isdemo']))?1:0;

            $strSql = "INSERT INTO syscompany SET companyname=:companyname, companyemail=:companyemail,bucket_name =:bucket_name,payment_types =:payment_types,contactperson=:contactperson,total_location=:total_location,databasename=:databasename, hostname='', isdemoaccount=:isdemoaccount, createddate=NOW(),api_auth_key=:api_auth_key,app_id=:app_id,app_secret=:app_secret";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyname",$data['business_name']);
            $dao->addParameter(":companyemail", $data['email']);
            $dao->addParameter(":bucket_name", $data['bucket_name']);
            $dao->addParameter(":payment_types", $data['paymenttype']);
            $dao->addParameter(":contactperson",$data['contact_name']);
            $dao->addParameter(":total_location",$data['location_count']);
            $dao->addParameter(":databasename",$dbname);
            $dao->addParameter(":isdemoaccount",$demo);
            $dao->addParameter(":api_auth_key",$api_auth_key);
            $dao->addParameter(":app_id",$ObjUtil->gethash());
            $dao->addParameter(":app_secret",$ObjUtil->gethash());
            $dao->executeNonQuery();
            $companyid = $dao->getLastInsertedId();

            /*$strSql = "INSERT INTO sysstoremaster SET storename=:storename,contactperson=:contactperson,companyid=:companyid,createddatetime=NOW()";
            $dao->initCommand($strSql);
            $dao->addParameter(":storename",$data['business_name']);
            $dao->addParameter(":contactperson",$data['contact_name']);
            $dao->addParameter(":companyid",$companyid);
            $dao->executeNonQuery();
            $s_storeid = $dao->getLastInsertedId();*/

            $strSqlbucket = "INSERT INTO sysbucketmaster SET companyid=:companyid,name=:name,createddatetime=NOW()";
            $dao->initCommand($strSqlbucket);
            $dao->addParameter(":companyid",$companyid);
            $dao->addParameter(":name",$data['bucket_name']);
            $dao->executeNonQuery();

            $strSql1 = "INSERT INTO sysclient SET name=:name, email=:email, contactno=:contactno, companyid=:companyid";
            $dao->initCommand($strSql1);
            $dao->addParameter(":name",$data['contact_name']);
            $dao->addParameter(":email",$data['email']);
            $dao->addParameter(":contactno",$data['contact']);
            $dao->addParameter(":companyid",$companyid);
            $dao->executeNonQuery();
            $clientid = $dao->getLastInsertedId();

            $strSql2 = "UPDATE syscompany SET clientid=:clientid WHERE syscompanyid=:companyid";
            $dao->initCommand($strSql2);
            $dao->addParameter(":clientid", $clientid);
            $dao->addParameter(":companyid", $companyid);
            $dao->executeNonQuery();
            $strSql3 = "UPDATE sysdatabase SET propertycount=propertycount+1 where dbname=:dbname";
            $dao->initCommand($strSql3);
            $dao->addParameter(":dbname", $dbname);
            $dao->executeNonQuery();
            $user='admin';
            $password = \util\util::random_password(8);
            $hash = \util\util::gethash($companyid);



            $strSql31 = "INSERT INTO ".$dbname.".trcontact SET name=:name, business_name=:business_name, mobile=:mobile, email=:email, contacttype=2, country=:country, companyid=:companyid";
            $dao->initCommand($strSql31);
            $dao->addParameter(":name",$data['contact_name']);
            $dao->addParameter(":business_name",$data['business_name']);
            $dao->addParameter(":email",$data['email']);
            $dao->addParameter(":mobile",$data['contact']);
            $dao->addParameter(":country",$data['select_country']);
            $dao->addParameter(":companyid",$companyid);
            $dao->executeNonQuery();
            $contact = $dao->getLastInsertedId();


            $strSql7 = "INSERT INTO ".$dbname.".cfstoremaster SET storename=:storename,contactperson=:contactperson,companyid=:companyid,email=:email,createddatetime=NOW()";
            $dao->initCommand($strSql7);
            $dao->addParameter(":storename",$data['business_name']);
            $dao->addParameter(":contactperson",$data['contact_name']);
            $dao->addParameter(":email",$data['email']);
            $dao->addParameter(":companyid",$companyid);
            $dao->executeNonQuery();
            $s_storeid = $dao->getLastInsertedId();

            $strSql4 = "INSERT INTO sysuser SET username=:username, password=:password, companyid=:companyid, isactive=1, is_deleted=0, hashkey=:hashkey,user_type=1,sysdefined=1, contactunkid=:contactunkid,storeid=:storeid";
            $dao->initCommand($strSql4);
            $dao->addParameter(":username", $user);
            $dao->addParameter(":password", md5($password));
            $dao->addParameter(":companyid", $companyid);
            $dao->addParameter(":hashkey", $hash);
            $dao->addParameter(":contactunkid", $contact);
            $dao->addParameter(":storeid", $s_storeid);
            $dao->executeNonQuery();
            $userid = $dao->getLastInsertedId();

            $this->log->logIt('===============================================================================Username and Password');
            $this->log->logIt('===========uesername========'.$user);
            $this->log->logIt('===========password========'.$password);
            $this->log->logIt('=========contact name========'.$data['contact_name']);
            $this->log->logIt('=========companyid========'.$companyid);

          //  $this->sendCredentials($data['contact_name'],$data['email'],$user,$password,$companyid);

            //Add Privileges
            $userhash = \util\util::gethash($userid);
            $strSqlUser = " INSERT INTO ".$dbname.".cfuserrole SET user_role='Admin', is_active=1, is_deleted=0, createddatetime=NOW(),created_user=:created_user,companyid=:companyid,locationid=0,hashkey=:hashkey ";
            $dao->initCommand($strSqlUser);
            $dao->addParameter(":companyid", $companyid);
            $dao->addParameter(":created_user", $userid);
            $dao->addParameter(":hashkey", $userhash);
            $dao->executeNonQuery();
            $roleid = $dao->getLastInsertedId();

            $dao->initCommand("SELECT * FROM sysprivileges");
            $allPrevisSqlcnt = $dao->executeQuery();

            if (count($allPrevisSqlcnt) > 0) {
                $sqlRole = " INSERT INTO ".$dbname.".cfroleprivileges(lnkroleid,lnkprivilegegroupid,lnkprivilegeid,lnkuserid,companyid) SELECT :roleid,SYSP.privilegegroupid,SYSP.id,:userid,:companyid FROM sysprivileges AS SYSP";
                $dao->initCommand($sqlRole);
                $dao->addParameter(":companyid", $companyid);
                $dao->addParameter(":userid", $userid);
                $dao->addParameter(":roleid", $roleid);
                $dao->executeNonQuery();
            }
            $strUser = "UPDATE sysuser SET roleunkid=:roleid WHERE sysdefined = 1 AND companyid=:companyid";
            $dao->initCommand($strUser);
            $dao->addParameter(":roleid", $roleid);
            $dao->addParameter(":companyid",$companyid);
            $dao->executeNonQuery();

            $strSql5 = " INSERT INTO ".$dbname.".cfcompany SET companyid=:companyid, companyname=:companyname, companyemail=:companyemail, country=:country,total_location=:total_location";
            $dao->initCommand($strSql5);
            $dao->addParameter(":companyid", $companyid);
            $dao->addParameter(":companyname", $data['business_name']);
            $dao->addParameter(":companyemail", $data['email']);
            $dao->addParameter(":country", $data['select_country']);
            $dao->addParameter(":total_location",$data['location_count']);
            $dao->executeNonQuery();

            $strSql10 = " INSERT INTO ".$dbname.".cfunit (name,shortcode,unit,measuretype,is_systemdefined,is_active,is_deleted,hashkey,companyid,locationid) SELECT SYSU.name,SYSU.shortcode,SYSU.unit,SYSU.measuretype,:is_systemdefined,1,0,concat( lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0), lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0), lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0), lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0) ),:companyid,:locationid FROM sysunit AS SYSU";
            $dao->initCommand($strSql10);
            $dao->addParameter(":is_systemdefined", 1);
            $dao->addParameter(":companyid", $companyid);
            $dao->addParameter(":locationid", 0);
            $dao->executeNonQuery();

            $strSqlIndent = " INSERT INTO ".$dbname.".cfindentnumber(keyname,displayname,prefix,startno,reset_type,storeid,companyid,hashkey)
             SELECT SI.keyname,SI.displayname,SI.prefix,SI.startno,SI.reset_type,:storeid,:companyid,
            concat( lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0), lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0),
             lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0), lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0) )   FROM sysindentnumber AS SI where type=1";
            $dao->initCommand($strSqlIndent);
            $dao->addParameter(":companyid", $companyid);
            $dao->addParameter(":storeid", $s_storeid);
            $dao->executeNonQuery();

            $strPara = " INSERT INTO ".$dbname.".cfparameter (keyname, keyvalue, description, companyid,locationid,storeid,type) SELECT sys.keyname,sys.keyvalue,sys.description,:companyid,0,:storeid,2 FROM sysparameter AS sys WHERE sys.companyid=0 AND type=2";
            $dao->initCommand($strPara);
            $dao->addParameter(":storeid", $s_storeid);
            $dao->addParameter(":companyid", $companyid);
            $dao->executeNonQuery();

            $strupdate = "UPDATE ".$dbname.".cfparameter SET keyvalue=:keyvalue WHERE `keyname` IN ('country','nationality') AND type=2 AND storeid=:storeid AND companyid=:companyid";
            $dao->initCommand($strupdate);
            $dao->addParameter(":keyvalue", $data['select_country']);
            $dao->addParameter(":storeid", $s_storeid);
            $dao->addParameter(":companyid", $companyid);
            $dao->executeNonQuery();

            //banquet management
            $strSqlbmdoc = " INSERT INTO ".$dbname.".bmdocnumber(keyname,prefix,startno,reset_type,companyid,hashkey)
             SELECT SI.keyname,SI.prefix,SI.startno,SI.reset_type,:companyid,
            concat( lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0), lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0),
             lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0), lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0) )   FROM sysindentnumber AS SI where type=2";
            $dao->initCommand($strSqlbmdoc);
            $dao->addParameter(":companyid", $companyid);
            $dao->executeNonQuery();

            $strbmsetting = " INSERT INTO ".$dbname.".bmdisplaysettings(keyname, keyvalue,companyid) SELECT sys.keyname,sys.keyvalue,:companyid FROM sysparameter AS sys WHERE sys.companyid=0 AND type=3";
            $dao->initCommand($strbmsetting);
            $dao->addParameter(":companyid", $companyid);
            $dao->executeNonQuery();
            return json_encode(array("Success"=>"True","Message"=>"Company Created Successfully"));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addCompany - '.$e);
        }
    }
    public function sendCredentials($contact_name,$email,$user,$password,$companyid)
    {
        try
        {
            $this->log->logIt($this->module.' - sendCredentials');
            $to = 'pureipos365@gmail.com,'.$email;
            $subject = 'Pure iPOS Credentials';
            $from = 'info@pureweblopment.com';

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            global $commonurl;

            $headers .= 'From: '.$from."\r\n".
                'Reply-To: '.$from."\r\n" .
                'X-Mailer: PHP/' . phpversion();
            
            $configurl='http://www.tabinsta.com/pos/index.php';
            $message=' <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                            <title>Pure iPOS Credentials</title>
                        </head>
                        <body style="margin: 0; padding: 0;">
                            <table class="container" style="width:100%;padding: 10px;font-family:\'Lucida Grande\',Arial,Verdana,Helvetica,sans-serif;color:#242424;margin:0 auto;" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <td width="100%" align="center" valign="top" style="text-align: left;background-color: #091a59;padding: 10px 20px;">
                                            <img src="http://www.tabinsta.com/pos/su/themes/img/header-logo.png" alt="logo" style="max-height:100px;width:auto;display: block;margin: 0 auto";>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%;" align="center" valign="top" style="text-align: left;background-color: #f8f8f8;padding:25px 20px">
                                            <p style="margin: 0;font-size: 16px;color: #091a59;line-height: 22px;">
                                                <span style="display: block;margin: 0 auto;color: #091a59;font-size: 18px;margin-bottom: 15px;">Hi! '.$contact_name.',</span>
                                               Thanks for choosing Pure Ipos - Restaurant POS.
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" valign="top" style="text-align: center;padding: 30px 0;border-top: 0;">
                                            <span style="display: block;width: 100%;">
                                                <span style="display: block;width: 360px;background-color: #091a59;margin: 0 auto;padding: 15px 20px;border-radius: 3px;">
                                                    <h3 style="margin: 0;font-size: 22px;color: #fff;text-align: center;font-weight: 100;padding: 0 20px 10px;">
                                                        Configuration login details :
                                                    </h3>
                                                    <table style="width: 100%; text-align: left;letter-spacing: .5px;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="padding-left: 20px;border-bottom: 1px solid #d9d9d980;"><p style="color: #fff;font-size: 17px;;
                                                            margin: 15px 0;">User Name<span style="float: right;">:</span></p></td>
                                                            <td style="padding-left: 20px;border-bottom: 1px solid #d9d9d980;"><p style="color: #fff;font-size: 17px;;
                                                            margin: 15px 0;">'.$user.'</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-left: 20px;border-bottom: 1px solid #d9d9d980;"><p style="color: #fff;font-size: 17px;;
                                                            margin: 15px 0;">Password<span style="float: right;">:</span></p></td>
                                                            <td style="padding-left: 20px;border-bottom: 1px solid #d9d9d980;"><p style="color: #fff;font-size: 17px;;
                                                            margin: 15px 0;">'.$password.'</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding-left: 20px;"><p style="color: #fff;font-size: 17px;
                                                            margin: 15px 0;">Company Id<span style="float: right;">:</span></p></td>
                                                            <td style="padding-left: 20px;"><p style="color: #fff;font-size: 17px;
                                                            margin: 15px 0;">'.$companyid.'</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="text-align: center;">
                                                                <a href="'.$configurl.'" style="color: #091a59;font-size: 14px;letter-spacing: 0.4px;font-weight: 600;display: inline-block;text-transform: uppercase;padding: 10px 20px;background-color: #fff;border-radius: 3px;text-decoration: none;margin: 15px 0 5px;">
                                                                    Take me to pure ipos
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </span>
                                            </span>
                                        </td>
                                    </tr>
                               </tbody>
                            </table>
                        </body>
                    </html>';
                    
            // Sending email

            if(mail($to, $subject, $message, $headers)){
                return 1;
            } else{
                return 0;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - sendCredentials - '.$e);
        }
    }
    public function toggleststus($data)
    {
        try
        {
            $this->log->logIt($this->module.' - toggleststus');
            $dao = new \dao();

            $strSql = "SELECT IF(isactive=1,0,1) as new_status,isactive as old_status from syscompany WHERE syscompanyid=:syscompanyid";
            $dao->initCommand($strSql);
            $dao->addParameter(":syscompanyid",$data['id']);
            $statusget=$dao->executeRow();

            $strSql = "UPDATE syscompany SET isactive = IF(isactive=1,0,1) WHERE syscompanyid=:syscompanyid";
            $dao->initCommand($strSql);
            $dao->addParameter(":syscompanyid",$data['id']);
            $dao->executeNonQuery();


            $strSql2 = "INSERT INTO company_audit_log SET  companyid=:companyid,created_date=:created_date,old_value=:old_value,new_value=:new_value,title=:title";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid',$data['id']);
            $dao->addParameter(':created_date', date("Y-m-d H:i:s"));
            $dao->addParameter(':old_value',isset($statusget)?json_encode(array('old_status'=>$statusget['old_status'])):'');
            $dao->addParameter(':new_value',isset($statusget)?json_encode(array('new_status'=>$statusget['new_status'])):'');
            $dao->addParameter(':title','Change Status');
            $dao->executeNonQuery();

            return json_encode(array("Success"=>"True","Message"=>"Status changed successfully"));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - toggleststus - '.$e);
        }
    }
    public function getCompanyList()
    {
        try
        {
            $this->log->logIt($this->module.' - getCompanyList');
            $dao = new \dao();
            $strSql = "SELECT SCO.syscompanyid,SCO.companyname FROM syscompany AS SCO INNER JOIN sysclient AS SC ON SC.clientid = SCO.clientid  WHERE isactive=1";
            $dao->initCommand($strSql);
            $data = $dao->executeQuery();
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getCompanyList - '.$e);
        }
    }
    public function updateApiMapping($data)
    {
        try
        {
            $this->log->logIt($this->module.' - updateApiMapping');
            $dao = new \dao();
            $strSql1 = "SELECT databasename FROM syscompany WHERE syscompanyid=:syscompanyid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':syscompanyid',$data['cid']);
            $rec1 = $dao->executeRow();
            $db_name = $rec1['databasename'];


            if(isset($data['translate_language']) && $data['translate_language']!=''){
                $t_languages=implode(',',$data['translate_language']);
            }


            $olddata=$this->getlogdata($data['cid'],$db_name);

            $strSqllang = " UPDATE syscompany SET translate_languages=:translate_languages WHERE syscompanyid=:companyid";
            $dao->initCommand($strSqllang);
            $dao->addParameter(':companyid',$data['cid']);
            $dao->addParameter(':translate_languages',isset($t_languages)?$t_languages:'');
            $dao->executeNonQuery();



            $strSql2 = " UPDATE ".$db_name.".cfcompany SET loyalty_integration=:loyalty_integration,store_available=:store_available WHERE companyid=:companyid";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid',$data['cid']);
          //  $dao->addParameter(':tabinsta_integration',$data['api_check1']);
          //  $dao->addParameter(':pms_integration',$data['api_check2']);
            $dao->addParameter(':loyalty_integration',$data['api_check5']);
            $dao->addParameter(':store_available',$data['is_store']);
            $dao->executeNonQuery();


            $strSql2 = " UPDATE ".$db_name.".cflocation SET loyalty_integration=:loyalty_integration WHERE companyid=:companyid";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid',$data['cid']);
            //$dao->addParameter(':pms_integration',$data['api_check2']);
            $dao->addParameter(':loyalty_integration',$data['api_check5']);
            $dao->executeNonQuery();

            $strSql2 = "INSERT INTO syscompany_meta SET   meta_value=:meta_value,meta_key=:meta_key,syscompanyid=:syscompanyid
                 ON DUPLICATE KEY UPDATE meta_value=:meta_value";
            $dao->initCommand($strSql2);
            $dao->addParameter(':syscompanyid',$data['cid']);
            $dao->addParameter(':meta_key','is_banquet_available');
            $dao->addParameter(':meta_value',$data['is_banquet']);
            $dao->executeNonQuery();

            if(isset($data['is_store']) && $data['is_store'] == 1)
            {
                $strSql2 = "INSERT INTO syscompany_meta SET  meta_value=:meta_value,meta_key=:meta_key,syscompanyid=:syscompanyid
                 ON DUPLICATE KEY UPDATE meta_value=:meta_value";
                $dao->initCommand($strSql2);
                $dao->addParameter(':syscompanyid',$data['cid']);
                $dao->addParameter(':meta_key','total_store');
                $dao->addParameter(':meta_value',$data['total_store']);
                $dao->executeNonQuery();
            }

            $newdata=$this->getlogdata($data['cid'],$db_name);

            $strSql2 = "INSERT INTO company_audit_log SET  companyid=:companyid,created_date=:created_date,old_value=:old_value,new_value=:new_value,title=:title";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid',$data['cid']);
            $dao->addParameter(':created_date', date("Y-m-d H:i:s"));
            $dao->addParameter(':old_value',isset($olddata)?json_encode($olddata):'');
            $dao->addParameter(':new_value',isset($newdata)?json_encode($newdata):'');
            $dao->addParameter(':title','Update');
            $dao->executeNonQuery();


            return json_encode(array("Success"=>"True","Message"=>"Settings changed successfully"));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - updateApiMapping - '.$e);
        }
    }


    public function getlogdata($companyid,$db_name)
    {
        try
        {
            $this->log->logIt($this->module.' - getlogdata');
            $dao = new \dao();

            $strSql2 = " SELECT companyid,store_available from ".$db_name.".cfcompany WHERE companyid=:companyid";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid',$companyid);
            $log_info=$dao->executeRow();


            $strSql2 = " SELECT meta_key,meta_value FROM syscompany_meta WHERE `meta_key` IN ('is_banquet_available','total_store') AND syscompanyid=:companyid";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid',$companyid);
            $meta_info=$dao->executeQuery();

            if($meta_info){
                $res=util::getkeyvaluearray($meta_info,'meta_key','meta_value');

                $log_info=array_merge($log_info,$res);
            }

            return $log_info;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getlogdata - '.$e);
        }
    }



    public function getCompanyInfo($companyid)
    {
        try
        {
            $this->log->logIt($this->module.' - getCompanyInfo');
            $dao = new \dao();
            $strSql1 = "SELECT databasename,IFNULL(translate_languages,'') as translate_languages FROM syscompany WHERE syscompanyid=:syscompanyid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':syscompanyid',$companyid);
            $rec1 = $dao->executeRow();
            $db_name = $rec1['databasename'];

            $strSql2 = "SELECT * FROM ".$db_name.".cfcompany WHERE companyid=:companyid";
            $dao->initCommand($strSql2);
            $dao->addParameter(':companyid',$companyid);
            $rec2 = $dao->executeRow();
            if(isset($rec2)){
                $rec2['translate_languages']=$rec1['translate_languages'];
            }
            return $rec2;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getCompanyInfo - '.$e);
        }
    }

    public function addAppData($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addTax');
            $dao = new \dao();


            if($data['id']=='0' || $data['id']==0)
            {

                $dao->initCommand("SELECT * FROM application_setting where app_type='".$data['app_type']."' AND application='".$data['application']."' AND app_version=".$data['app_ver']." AND companyid=".$data['companyid']);
                $checkduplicate = $dao->executeQuery();
                if($checkduplicate){
                    return json_encode(array("Success"=>"False","Message"=>"Record already found!"));
                }

                $strSql = "INSERT INTO application_setting (app_type,application, app_version, app_launched_date,app_setting, app_last_accessed_date, is_live, companyid)
                            VALUE(:app_type,:application, :app_version, :app_launched_date, :app_setting, :app_last_accessed_date, :is_live, :companyid)";
                $dao->initCommand($strSql);
                $dao->addParameter(':app_type',$data['app_type']);
                $dao->addParameter(':application',$data['application']);
                $dao->addParameter(':app_version',$data['app_ver']);
                $dao->addParameter(':app_launched_date',$data['app_launch']);
                $dao->addParameter(':app_setting',$data['app_settings']);
                $dao->addParameter(':app_last_accessed_date',$data['app_access']);
                $dao->addParameter(':is_live',$data['is_live']);
                $dao->addParameter(':companyid',$data['companyid']);
                $dao->executeNonQuery();

                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>"Record added successfully!")));
            }
            else
            {

                $dao->initCommand("SELECT * FROM application_setting where app_type='".$data['app_type']."' AND application='".$data['application']."' AND app_version=".$data['app_ver']." AND companyid=".$data['companyid']." AND z_applogid_pk!=".$data['id']);
                $checkduplicate = $dao->executeQuery();
                if($checkduplicate){
                    return json_encode(array("Success"=>"False","Message"=>"Record already found!"));
                }


                $strSql = "UPDATE application_setting SET app_type=:app_type, application=:application,app_version=:app_version,  app_launched_date=:app_launched_date, app_setting=:app_setting,app_last_accessed_date=:app_last_accessed_date,is_live=:is_live WHERE z_applogid_pk=:id AND companyid=:companyid ";

                $dao->initCommand($strSql);
                $dao->addParameter(':app_type',$data['app_type']);
                $dao->addParameter(':application',$data['application']);
                $dao->addParameter(':app_version',$data['app_ver']);
                $dao->addParameter(':app_launched_date',$data['app_launch']);
                $dao->addParameter(':app_setting',$data['app_settings']);
                $dao->addParameter(':app_last_accessed_date',$data['app_access']);
                $dao->addParameter(':is_live',$data['is_live']);
                $dao->addParameter(':id',$data['id']);
                $dao->addParameter(':companyid',$data['companyid']);


                $dao->executeNonQuery();

                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>"Record updated successfully!")));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addTax - '.$e);
        }
    }
}
?>