<?php
	
class folio_filter
{
    public $module='folio_filter';
    public $log;
    public $encdec;
    
    public function __construct()
    {
        $this->log = new \util\logger();
    }
    
	public function load($data='')
    {
        try
        {
            $this->log->logIt($this->module.' - load');

            if(isset($data['secret_key']) && $data['secret_key']!='$@CR@$tt'){
                $dashboard=new dashboard();
                $dashboard->load();
            }

		    global $twig;
            global $commonurl;
			$template = $twig->loadTemplate('folio_filter.html');
			$senderarr['commonurl'] = $commonurl;
            $locationDao=new \database\locationdao();
            $locationlist=$locationDao->loadLocationList();
            $locationlist=json_decode($locationlist,1);
			$senderarr['locationlist'] = $locationlist[0]['data'];
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function folio_clear($data){
        try {
            $this->log->logIt($this->module . ' - folio_clear');
            $dao = new \dao();
            $is_hard=isset($data['is_hard'])?$data['is_hard']:1;
            $displaystartdate=$data['displaystartdate'];
            $displayenddate=$data['displayenddate'];
            $locationid=$data['locationid'];

            $squery="select companyid from syslocation where syslocationid=".$locationid;
            $dao->initCommand($squery);
            $res = $dao->executeRow();
            $companyid=$res['companyid'];

            $strSql1 = "CALL `SP_DeleteLocation_Folio_Data`(:location_id,:company_id,:is_hard,:displaystartdate,:displayenddate)";
            $dao->initCommand($strSql1);
            $dao->addParameter(":location_id", $locationid);
            $dao->addParameter(":company_id", $companyid);
            $dao->addParameter(":is_hard", $is_hard);
            $dao->addParameter(":displaystartdate", $displaystartdate);
            $dao->addParameter(":displayenddate", $displayenddate);
            $dao->executeNonQuery();
            return json_encode(array("Success"=>"True","Message"=>"Folio's Upated Sucessfully!"));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - folio_clear - ' . $e);
        }
    }

}
?>