<?php
class device
{
    public $module = 'device';
    public $log;
    private $language, $lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(21,$this->module);

            $Objdevicedao = new \database\devicedao();
            $data = $Objdevicedao->devicelist(50, "0", '');
            $this->log->logIt(json_decode($data));

            $template = $twig->loadTemplate('device.html');

            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;
            $senderarr['langlist'] = $this->lang_arr;
            $senderarr['default_langlist'] = $this->default_lang_arr;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');


            $arr_check = array('devicename','select_device');
            if(isset($data['devicename'])){
                $arr_check[] = "devicename";
            }
            if(isset($data['select_device'])){
                $arr_check[] = "select_device";
            }

            $flag1 = \util\validate::check_notnull($data,$arr_check);
            if($flag1=='true'){
                $reqarr = array(
                    "devicename" => $data['devicename'],
                    "type" => $data['select_device'],
                    "rdo_status"=>$data['rdo_status'],
                    "id"=>$data['id'],
                    "module"=>$this->module
                );
                $ObjDao = new \database\devicedao();
                $data = $ObjDao->adddevice($reqarr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>'Some field is missing'));

        }catch(Exception $e){
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }

    public function editdevicelist($data)
    {
        try
        {
            $this->log->logIt($this->module." - editdevicelist");
            $ObjDao = new \database\devicedao();
            $data = $ObjDao->editdevicelist($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - editdevicelist - ".$e);
            return false;
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');
            $limit = 50;
            $offset = 0;
            $name = "";
            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['nm']) && $data['nm'] != "")
                $name = $data['nm'];
            $ObjUserDao = new \database\devicedao();
            $res = $ObjUserDao->devicelist($limit, $offset, $name);
            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $arr = array(
                "LANG1" => "Device",
                "LANG2" => "Name",
                "LANG3" => "Device Type",
                "LANG4" => "KOT",
                "LANG5" => "Reciept",
            );
            $this->lang_arr = $this->language->loadlanguage($arr);

            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }

}
?>