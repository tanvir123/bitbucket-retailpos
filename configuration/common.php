<?php
class common
{
    private $module='common';
    private $log;
    private $encdec;
    private $language,$default_lang_arr,$defaultlanguageArr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language();
        $this->default_lang_arr = $this->language->loaddefaultlanguage();
        $this->defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
        $this->defaultlanguageArr = json_decode($this->defaultlanguageArr);
    }
    public function toggleststus($data)
	{
		try
		{
			$this->log->logIt($this->module.' - toggleststus');
			$ObjCommonDao = new \database\commondao();
			$res = $ObjCommonDao->toggleststus($data);
			return $res;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module.' - toggleststus - '.$e);
		}	
	}
    public function toggleststusBycompany($data)
    {
        try
        {
            $this->log->logIt($this->module.' - toggleststusBycompany');
            $ObjCommonDao = new \database\commondao();
            $res = $ObjCommonDao->toggleststusBycompany($data);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - toggleststusBycompany - '.$e);
        }
    }
	public function remove($data)
	{
		try
		{
			$this->log->logIt($this->module.' - remove');
			$ObjCommonDao = new \database\commondao();
			$res = $ObjCommonDao->remove($data);
			return $res;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module.' - remove - '.$e);
		}	
	}

    public function removeByCompany($data)
    {
        try
        {
            $this->log->logIt($this->module.' - removeByCompany');
            $ObjCommonDao = new \database\commondao();
            $res = $ObjCommonDao->removeByCompany($data);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - removeByCompany - '.$e);
        }
    }

	/*function for remove table module data  */
    public function removetable($data)
    {
        try
        {
            $this->log->logIt($this->module.' - removetable');
            $ObjCommonDao = new \database\commondao();
            $res = $ObjCommonDao->removetable($data);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - removetable - '.$e);
        }
    }

    public function check_store_indent($data)
    {
        try {
            $this->log->logIt($this->module . " - check_store_indent");
            global $twig;
            global $commonurl;
            $ObjCommmonIndentDao = new \database\commonindentdao();
            $indent_data=0;
            if ($data['module'] == 1) {
                $indent_data = $ObjCommmonIndentDao->GetIndentRequestDetailonId($data['id']);
            }
            if ($data['module'] == 2) {
                $indent_data = $ObjCommmonIndentDao->GetRecievedIndentDetailId($data['id']);
            }
            if ($data['module'] == 3) {
                $indent_data = $ObjCommmonIndentDao->getissuedetail($data['id'],json_encode($this->defaultlanguageArr));
            }
            if ($data['module'] == 4) {
                $indent_data = $ObjCommmonIndentDao->GetRecievedVoucherDetail($data['id']);
            }
            if ($data['module'] == 5) {
                $indent_data = $ObjCommmonIndentDao->GetPurchaseOrderDetail($data['id'],json_encode($this->defaultlanguageArr));
            }
            if ($data['module'] == 6) {
                $indent_data = $ObjCommmonIndentDao->GetGoodsReturnNoteDetail($data['id'],json_encode($this->defaultlanguageArr));
            }
            if ($data['module'] == 7) {
                $indent_data = $ObjCommmonIndentDao->GetGoodsReturnDetail($data['id'],json_encode($this->defaultlanguageArr));
            }
            $template = $twig->loadTemplate('storeindentlist.html');
            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['data'] = $indent_data;
            $senderarr['type'] = $data['type'];
            $senderarr['default_langlist'] = $this->defaultlanguageArr;
            $rec = $template->render($senderarr);
            return json_encode(array("Data" => $rec));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - check_store_indent - " . $e);
        }
    }
    public function switch_outlet($data)
    {
        try
        {
            $this->log->logIt($this->module . " - switch_outlet");
            $oid = $data['oid'];
            $ObjCommonDao = new \database\commondao();
            $function = new \common\functions();

            $rec = $ObjCommonDao->validateUserForSwitch($oid,$data['type']);
            if($rec['Success']=='True')
            {
                $rec = $function->updateAuthToken($oid,$data['type']);
                if($rec==1){
                    return json_encode(array("Success"=>"True"));
                }
                else{
                    return json_encode(array("Success"=>"False", "Message"=>"There is internal error"));
                }
            }else{
                return json_encode(array("Success"=>"False", "Message"=>$rec['Message']));
            }
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - switch_outlet - " . $e);
            return false;
        }
    }
    public function getallstorelist()
    {
        try
        {
            $this->log->logIt($this->module.' - getallstorelist');
            $ObjCommonDao = new \database\commondao();
            $res = $ObjCommonDao->getallstorelist();
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getallstorelist - '.$e);
        }
    }
}
?>