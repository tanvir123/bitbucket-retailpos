<?php

class purchaseorder
{
    public $module = 'purchaseorder';
    public $log;
    private $language, $lang_arr, $default_lang_arr;
    public $ObjPurchaseDao;
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->ObjPurchaseDao = new \database\purchaseorderdao();
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(56, $this->module);
            $ObjCommonDao = new \database\commondao();

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(56);
            $arr_purchase_orders = $this->ObjPurchaseDao->purchaselist(50,'0', '', '', '', '', '');

            $template = $twig->loadTemplate('purchaseorder.html');

            $vendor_list = $this->ObjPurchaseDao->getAllVendorList();

            $currency_sign =$ObjCommonDao->getCurrencySignCompany();
            $indentNo = $ObjCommonDao->getIndentDocumentNumbering('purchase_order', '');
            
            $ObjCategoryDao = new \database\raw_categorydao();
            $catData = $ObjCategoryDao->getAllCategory();
            $cat_list = json_decode($catData, 1);
            
            $js_date = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $this->loadLang();
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['login_type'] = CONFIG_LOGINTYPE;
            $sender_arr['module'] = $this->module;
            $sender_arr['datalist'] = $arr_purchase_orders;
            $sender_arr['indentid'] = $indentNo;
            $sender_arr['category'] = $cat_list['Data'];
            $sender_arr['vendor_list'] = $vendor_list;
            $sender_arr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $sender_arr['langlist'] = json_decode($languageArr);
            $sender_arr['jsdateformat'] = $js_date;
            $sender_arr['roundoff'] = $round_off;
            $sender_arr['currencysign'] = $currency_sign;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $sender_arr['default_langlist'] = json_decode($defaultlanguageArr);
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            $sender_arr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($sender_arr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');

            $limit = 50;
            $offset = 0;
            $fromdate = "";
            $todate = "";
            $orderno = "";
            $vendorid = "";
            $statusid = "";

            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['fromdate']) && $data['fromdate'] != "")
                $fromdate = $data['fromdate'];
            if (isset($data['todate']) && $data['todate'] != "")
                $todate = $data['todate'];
            if (isset($data['orderno']) && $data['orderno'] != "")
                $orderno = $data['orderno'];
            if (isset($data['vendorid']) && $data['vendorid'] != "")
                $vendorid = $data['vendorid'];
            if (isset($data['statusid']) && $data['statusid'] != "")
                $statusid = $data['statusid'];

            $data =  $this->ObjPurchaseDao->purchaselist($limit, $offset, $fromdate, $todate, $orderno, $vendorid,$statusid);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function loadPurchaseOrder($data)
    {
        try {
            $this->log->logIt($this->module." - loadPurchaseOrder");
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(56, $this->module);

            $ObjCommonDao = new \database\commondao();
            $privilegeList = $ObjCommonDao->getuserprivongroup(56);

            $vendor_list = $this->ObjPurchaseDao->getAllVendorList();

            $ObjCommonDao = new \database\commondao();
            $currency_sign =$ObjCommonDao->getCurrencySignCompany();

            if($data['id']=='' || $data['id'] ==0) {
                $orderNo = $ObjCommonDao->getIndentDocumentNumbering('purchase_order', '');
            }
            else{ $orderNo=''; }
            $applicable_taxes = $this->ObjPurchaseDao->getAppliedTax();


            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $js_date = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $template = $twig->loadTemplate('purchaseorderrequest.html');
            $this->loadLang();
            $sender_arr = array();
            $sender_arr['commonurl'] = CONFIG_COMMON_URL;
            $sender_arr['grpprivlist'] = CONFIG_GID;
            $sender_arr['store'] = CONFIG_IS_STORE;
            $sender_arr['module'] = $this->module;
            $sender_arr['datalist'] = $data;
            $sender_arr['id'] = $data['id'];
            $sender_arr['currencysign'] = $currency_sign;
            $sender_arr['orderno'] = $orderNo;
            $sender_arr['vendor_list'] = $vendor_list;
            $sender_arr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $sender_arr['langlist'] = json_decode($languageArr);
            $sender_arr['applicable_taxes'] = $applicable_taxes;
            $sender_arr['jsdateformat'] = $js_date;
            $sender_arr['roundoff'] = $round_off;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $sender_arr['default_langlist'] = json_decode($defaultlanguageArr);
            $sender_arr['user_type'] = CONFIG_USR_TYPE;
            echo json_encode(array("Success" => "True", "Data" => $template->render($sender_arr)));
        } catch (Exception $e) {
            $this->log->logIt($this->module." - loadPurchaseOrder - ".$e);
            return false;
        }
    }
    public function addPurChaseOrder($data)
    {
        try {
            $this->log->logIt($this->module.' - addPurChaseOrder');
            $req_arr = array(
                "po_items" => $data['po_items'],
                "total_amount" => $data['total_amount'],
                "po_date" => $data['po_date'],
                //"issue_date" => $data['po_date'],
                "pstoreid" => CONFIG_SID,
                "po_sel_vendor" => $data['vendor_id'],
                "po_remarks" => $data['po_remarks'],
                "id" => $data['id'],
                "module" => $this->module
            );
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr=json_decode($defaultlanguageArr);
            $result = $this->ObjPurchaseDao->addPurchase($req_arr,$defaultlanguageArr);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module.' - addPurChaseOrder - '.$e);
        }
    }
    public function getPoDetail($data)
    {
        try {
            $this->log->logIt($this->module." - getPoDetail");
            $result = $this->ObjPurchaseDao->getPoRecordsbyID($data);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module." - getPoDetail - ".$e);
            return false;
        }
    }
    public function getVendorCategory($data)
    {
        try {
            $this->log->logIt($this->module . " - getVendorCategory");
            $result = $this->ObjPurchaseDao->getVendorCategory($data);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getVendorCategory - " . $e);
            return false;
        }
    }
    public function getRawMaterialByCategory($data)
    {
        try {
            $this->log->logIt($this->module . ' - getRawMaterialByCategory');
            $result = $this->ObjPurchaseDao->getRawMaterialFromCategory($data);
            return $result;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getRawMaterialByCategory - ' . $e);
        }
    }
    public function getUnitRate($data)
    {
        try {
            $this->log->logIt($this->module . " - getUnitRate");
            $resObj = $this->ObjPurchaseDao->getUnitRate($data);
            return $resObj;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getUnitRate - " . $e);
            return false;
        }
    }

    public function authorize_po($data){
        try{
            $this->log->logIt($this->module . ' - authorize_po');
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr=json_decode($defaultlanguageArr);
            $result = $this->ObjPurchaseDao->authorize_po($data,$defaultlanguageArr);
            return $result;
        }catch (Exception $e) {
            $this->log->logIt($this->module . ' - authorize_po - ' . $e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$purchaseorder;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }

    public function printPO($data)
    {
        try
        {
            $this->log->logIt($this->module.' - printPO');
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $id = $data['id'];
            $ObjIndentDao = new \database\purchaseorderdao();
            $headerData = $ObjIndentDao->printPO($id,$defaultlanguageArr);
            if($headerData){

                $headerData['default_langlist'] = json_decode($defaultlanguageArr);         $senderarr['user_type'] = CONFIG_USR_TYPE;
                $indentlanguage = new \util\language('AuthorizeReceivedindent');
                $llang_arr = \common\staticlang::$AuthorizeReceivedindent;
                $languageArr=html_entity_decode(json_encode($indentlanguage->loadlanguage($llang_arr)),ENT_QUOTES);
                $headerData['langlist'] = json_decode($languageArr);
            }

            $PrintReport = new \common\printreport();

            $PrintReport->addTemplate('PO');
            $width = '793px';
            $height = '1122px';

            $PrintReport->addRecord($headerData);
            $data = $PrintReport->Request();
            return json_encode(array("Success"=>"True","Data"=>$data, "Height"=>$height, "Width"=>$width));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - printPO - '.$e);
        }
    }
}

?>