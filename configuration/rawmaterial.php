<?php
class rawmaterial
{
    public $module='rawmaterial';
    public $log;
    private $language,$lang_arr,$default_lang_arr;
    
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }
	public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(28,$this->module);
            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(28);

            $ObjUserDao = new \database\rawmaterialdao();
            $data = $ObjUserDao->rawmateriallist(50,'0','',0,'');
			$template = $twig->loadTemplate('rawmaterial.html');

            $Objlocationdao = new \database\locationdao();
            $locData = $Objlocationdao->getAllLocation();
            $loclist=json_decode($locData,1);

            $Objcategorydao = new \database\raw_categorydao();
            $catData = $Objcategorydao->getAllCategory();
            $catlist=json_decode($catData,1);

            $Objvendordao = new \database\vendordao();
            $vendorData = $Objvendordao->getAllVendor();
            $vendorlist=json_decode($vendorData,1);

            $roundoff= \database\parameter::getParameter('digitafterdecimal');

            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
			$senderarr['module'] = $this->module;
			$senderarr['datalist'] = $data;
            $senderarr['category'] = $catlist['Data'];
            $senderarr['vendor'] = $vendorlist['Data'];
            $senderarr['location'] = $loclist['Data'];
            $senderarr['roundoff']=$roundoff;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	
	public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
            $flag = \util\validate::check_notnull($data,array('name','code','category','measuretype'));
            $mflag='true';
            $uflag=$rflag1=$rflag2='true';
            $unit_arr = $recipe_arr = array();
            /*if($data['quantity']!='' || $data['price']!='' || $data['unit']!=''){
                $qflag = \util\validate::check_notnull($data,array('quantity','price','unit'));
                if($data['quantity']==0 || $data['price']==0){
                    $qflag='false';
                }
            }*/

            if(($data['minlimit']!='' && $data['munit']=='')||($data['minlimit']=='' && $data['munit']!='')){
                $mflag='false';
                if($data['minlimit']==0){
                    $mflag='false';
                }
            }
            $bind_unit = isset($data['unit_detail']) ? $data['unit_detail'] : '';
            if ($bind_unit != '') {
                $bind_unit_arr = explode(',', $bind_unit);
                foreach ($bind_unit_arr AS $sub_unit) {
                    if(isset($data['conversation_rate_'.$sub_unit]) && $data['conversation_rate_'.$sub_unit]=="")
                        $uflag = 'false';
                    else
                        $unit_arr[$sub_unit]['conversation_rate'] = $data['conversation_rate_' . $sub_unit];
                }
            }

            $bind_rawid = isset($data['old_rawmaterial']) ? $data['old_rawmaterial'] : '';
            $is_recipe = isset($data['is_recipe'])?$data['is_recipe']:"";

            if ($bind_rawid != '') {
                $bind_rawid_arr = explode(',', $bind_rawid);
                foreach ($bind_rawid_arr AS $sub_bind_rawid) {
                    if(isset($data['itemunit_'.$sub_bind_rawid]) && $data['itemunit_'.$sub_bind_rawid]=="" && $data['itemunit_'.$sub_bind_rawid]==0)
                        $rflag1 = 'false';
                    else
                        $recipe_arr[$sub_bind_rawid]['itemunit'] = $data['itemunit_' . $sub_bind_rawid];

                if(isset($data['itemQty_'.$sub_bind_rawid]) && $data['itemQty_'.$sub_bind_rawid]=="" && $data['itemQty_'.$sub_bind_rawid]==0)
                        $rflag2 = 'false';
                    else
                        $recipe_arr[$sub_bind_rawid]['itemQty'] = $data['itemQty_' . $sub_bind_rawid];
                }
            }else{
                if($is_recipe == 1)
                     return json_encode(array('Success'=>'False','Message'=>$this->default_lang_arr['Missing_Field']));
            }


            if($flag=='true' && $mflag!='false' && $uflag!='false')
            {
                $reqarr = array(
                            'name'=>$data['name'],
                            'code'=>$data['code'],
                            'category'=>$data['category'],
                            'measuretype'=>$data['measuretype'],
                            'rdo_status'=>$data['rdo_status'],
                            'minlimit'=>$data['minlimit'],
                            "id"=>$data['id'],
                            "locid"=>$data['locid'],
                            "munit"=>$data['munit'],
                            "unitval"=>$data['unitval'],
                            "bind_unit"=>$unit_arr,
                            "is_recipe"=>isset($data['is_recipe_avalible'])?$data['is_recipe_avalible']:0,
                           /* 'quantity'=>$data['quantity'],
                            "iunitval"=>$data['iunitval'],
                            'price'=>$data['price'],
                            "unit"=>$data['unit'],*/
                            "module" => $this->module
                        );
                $this->loadLang();
                $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);
				$ObjUnitDao = new \database\rawmaterialdao();
				$data = $ObjUnitDao->addRawmaterial($reqarr,$languageArr,$defaultlanguageArr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>$this->default_lang_arr['Missing_Field']));
        } catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }

    public function addInventoryfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addInventoryfrm');
            $flag = \util\validate::check_notnull($data,array('quantity','price','unit'));
            if($flag=='true')
            {
                $reqarr = array(
                    "vendor"=>$data['vendor'],
                    'quantity'=>$data['quantity'],
                    'price'=>$data['price'],
                    'rawhash'=>$data['rawhash'],
                    'measureval'=>$data['measureval'],
                    "iunitval"=>$data['iunitval'],
                    "unit"=>$data['unit'],
                    "module" => $this->module
                );
                $this->loadLang();
                $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $ObjUnitDao = new \database\rawmaterialdao();
                $data = $ObjUnitDao->addInventory($reqarr,$languageArr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>$this->default_lang_arr['Missing_Field']));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addInventoryfrm - '.$e);
        }
    }

    public function getInventoryRec($data)
    {
        try
        {
            $this->log->logIt($this->module." - getInventoryRec");
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $ObjUnitDao = new \database\rawmaterialdao();
            $data = $ObjUnitDao->getInventoryRec($data,$languageArr,$defaultlanguageArr);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getInventoryRec - ".$e);
            return false;
        }
    }

    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            global $twig;
            $limit=50;
            $offset=0;
            $name="";
            $category="";

            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            if(isset($data['cat']) && $data['cat']!="" && $data['cat']!=0)
                $category = $data['cat'];
            $ObjUserDao = new \database\rawmaterialdao();
			$data = $ObjUserDao->rawmateriallist($limit,$offset,$name,0,$category);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function getRawmaterialRec($data)
    {
        try
        {
            $this->log->logIt($this->module." - getRawmaterialRec");
            $ObjUnitDao = new \database\rawmaterialdao();
            $data = $ObjUnitDao->getRawmaterialRec($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getRawmaterialRec - ".$e);
            return false;
        }
    }

    public function getUnitsBYmeasureType($data)
    {
        try
        {
            $this->log->logIt($this->module." - getUnitsBYmeasureType");
            $ObjUnitDao = new \database\rawmaterialdao();
            $data = $ObjUnitDao->getUnitsBYmeasureType($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUnitsBYmeasureType - ".$e);
            return false;
        }
    }

    public function getRawdetailByCat($data)
    {
        try
        {
            $this->log->logIt($this->module." - getRawdetailByCat");
            $ObjUnitDao = new \database\rawmaterialdao();
            $data = $ObjUnitDao->getRawdetailByCat($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getRawdetailByCat - ".$e);
            return false;
        }
    }

    public function getUnitsBYrawmaterial($data)
    {
        try
        {
            $this->log->logIt($this->module." - getUnitsBYrawmaterial");
            $ObjUnitDao = new \database\rawmaterialdao();
            $data = $ObjUnitDao->getUnitsBYrawmaterial($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUnitsBYrawmaterial - ".$e);
            return false;
        }
    }
    public function getUnitByStore($data)
    {
        try
        {
            $this->log->logIt($this->module." - getUnitByStore");
            $ObjUnitDao = new \database\rawmaterialdao();
            $data = $ObjUnitDao->getUnitByStore($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUnitByStore - ".$e);
            return false;
        }
    }

    public function removematerial($data)
    {
        try
        {
            $this->log->logIt($this->module.' - removematerial');
            $ObjCommonDao = new \database\rawmaterialdao();
            $data['module'] = $this->module;
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);
            $res = $ObjCommonDao->removematerial($data,$defaultlanguageArr);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - removematerial - '.$e);
        }
    }

    public function removeinventory($data)
    {
        try
        {
            $this->log->logIt($this->module." - removeinventory");
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);
            $ObjRawDao = new \database\rawmaterialdao();
            $data = $ObjRawDao->removeinventory($data,$languageArr,$defaultlanguageArr);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - removeinventory - '.$e);
        }
    }

    public function loadLang()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$rawmaterial;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }

    public function getAllrawmaterials($data)
    {
        try
        {
            $this->log->logIt($this->module." - getAllrawmaterials");
            $ObjRawDao = new \database\rawmaterialdao();
            $data = $ObjRawDao->getAllrawmaterials($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getAllrawmaterials - '.$e);
        }
    }

    public function getRawMaterialUnits($data)
    {
        try
        {
            $this->log->logIt($this->module." - getRawMaterialUnits");
            $ObjRawDao = new \database\rawmaterialdao();
            $data = $ObjRawDao->getRawMaterialUnits($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getRawMaterialUnits - '.$e);
        }
    }

}
?>