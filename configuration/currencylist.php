<?php
class currencylist
{
    public $module = 'currencylist';
    public $log;
    private $language, $lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(29,$this->module);
            $ObjCurrencylistDao = new \database\currencylistdao();
            $data = $ObjCurrencylistDao->currencylist(50,'0' ,'');
            $countrydata = $ObjCurrencylistDao->countrylist();


            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(29);

            $Quickbooksetting = $OBJCOMMONDAO->getQuickBooksetting();

            $countrylist = json_decode($countrydata,true);
            $record = $ObjCurrencylistDao->getbasecurrency();
            $template = $twig->loadTemplate('currencylist.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;
            $languageArr=html_entity_decode(json_encode($this->lang_arr));
            $senderarr['langlist'] = json_decode($languageArr);
            $senderarr['recordlist'] = $record;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['countrylistdata'] = $countrylist[0]['data'];
            $senderarr['countrylist'] = $countrydata;
            $senderarr['Quickbooksetting'] = $Quickbooksetting;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
            $arr_check = array('txtsymbol','txtfixrate','symbol_pos');
            if(isset($data['txtcode'])){
                $arr_check[] = "txtcode";
            }
            if(isset($data['txtrate'])){
                $arr_check[] = "txtrate";
            }
            if(isset($data['select_country'])){
                $arr_check[] = "select_country";
            }
            $flag1 = \util\validate::check_notnull($data,$arr_check);
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
            $default_langlist = json_decode($defaultlanguageArr);
            if($flag1=='true'){
                $reqarr = array(
                    "code" => isset($data['txtcode'])?$data['txtcode']:'',
                    "currencyCode" => $data['currencyCode'],
                    "symbol" => $data['txtsymbol'],
                    "fixrate" => $data['txtfixrate'],
                    "selectcountry" => isset($data['select_country'])?$data['select_country']:'',
                    "countryId" => $data['countryId'],
                    "rate" => isset($data['txtrate'])?$data['txtrate']:'',
                    "currencyRate" => $data['currencyRate'],
                    "symbol_pos" => $data['symbol_pos'],
                    "rdo_status"=>$data['rdo_status'],
                    "id"=>$data['id'],
                    "isdefault"=>$data['isdefault'],
                    "qb_currencyaccountid"=>isset($data['qb_currencyaccountid'])?$data['qb_currencyaccountid']:NULL,
                    "qb_currencyaccountname"=>isset($data['qb_currencyaccountname'])?$data['qb_currencyaccountname']:NULL,
                    "module"=>$this->module
                );
                $ObjDao = new \database\currencylistdao();
                $data = $ObjDao->addcurrency($reqarr,$default_langlist);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>$default_langlist->SOME_FIELD_MISSING));

        }catch(Exception $e){
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }

    public function editcurrencylist($data)
    {
        try
        {
            $this->log->logIt($this->module." - editcurrencylist");
            $ObjDao = new \database\currencylistdao();
            $data = $ObjDao->editcurrencylist($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - editcurrencylist - ".$e);
            return false;
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');
            $limit = 50;
            $offset = 0;
            $name = "";
            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['nm']) && $data['nm'] != "")
                $name = $data['nm'];
            $ObjUserDao = new \database\currencylistdao();
            $res = $ObjUserDao->currencylist($limit, $offset, $name);
            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$config_currencylist;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }

}
?>