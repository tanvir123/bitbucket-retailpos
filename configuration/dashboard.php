<?php

class dashboard
{
    public $module = 'dashboard';
    public $log;
    public $encdec;
    private $language, $lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_dashboard');
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module. ' - load');
            global $twig;
            $ObjCommonDao = new \database\commondao();
            $switch_list = $ObjCommonDao->getListForSwitch();
            $objcompanyinfo = new \database\storereportsdao();
            $company_detail = $objcompanyinfo->loadcompanyinformation();
            $company_logo=html_entity_decode($company_detail['company_logo'],ENT_QUOTES);

            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
            $defaultlanguageArr=json_decode($defaultlanguageArr);

            if(CONFIG_LOGINTYPE==1){

                $privilegeList = $ObjCommonDao->getuserprivongroup(1);
                $privileges = $privilegeList['lnkprivilegegroupid'];
                //$todaydate = \util\util::getLocalDate();
                $todaydate = \database\parameter::getParameter('todaysdate');

                if(!$todaydate || $todaydate == '')
                {
                    $todaydate = \util\util::getlocalDate();
                    \database\parameter::setParameter('todaysdate',$todaydate);
                }
                $todaydatetime = \util\util::getLocalDateTime();
                $lastweekdate = \util\util::removeDaysinDate($todaydate, 7);
                $range = \util\util::generateDateRange($lastweekdate, $todaydate);
                $ObjOrderDao = new \database\orderdao();
                $orderdata = $ObjOrderDao->orderslist(5, '0', '', \util\util::convertMySqlDate($todaydate), \util\util::convertMySqlDate($todaydate),'',$defaultlanguageArr,'today','dash');
                $orderlist = json_decode($orderdata, 1);
                $order_data = $orderlist[0]['data'];

                $ObjDashboardDao = new \database\dashboarddao();
                $headerdata = $ObjDashboardDao->getdashboardheaderdata();
                $last7daysarr = $ObjDashboardDao->getrevenuedays($lastweekdate, $todaydate);
                $round_off = \database\parameter::getParameter('digitafterdecimal');
                $formatted_range = array();
                $revenuebydate = array();
                foreach ($range AS $value) {
                    $formatted_range[] = \util\util::convertMySqlDate($value);
                    $revenuebydate[] = (isset($last7daysarr[$value])) ? round($last7daysarr[$value], $round_off) : round(0, $round_off);
                }
                $topsalingproduct = $ObjDashboardDao->gettopsalingItem();
                $template = $twig->loadTemplate('dashboard.html');

            }else{
                $headerdata = array();
                $privilegeList = $ObjCommonDao->getuserprivongroup(49);
                $privileges = $privilegeList['lnkprivilegegroupid'];
                $template = $twig->loadTemplate('storedashboard.html');
            }

            $data = array();

            if(isset($company_logo) && $company_logo!='')
            {
                $data['company_logo'] = CONFIG_BASE_URL."companyadmin/assets/company_logo/".$company_logo;
            }
            else{
                $data['company_logo'] ='';
            }
	    $getOEM = CONFIG_OEM_LOGO;
	    if(isset($getOEM) && $getOEM!=''){
                $data['pure_logo'] = CONFIG_BASE_URL."companyadmin/assets/images/".$getOEM.'-logo.png';
            }else{
                $data['pure_logo'] = CONFIG_BASE_URL."companyadmin/assets/company_logo/pureITES.jpg";
            }

            $data['uname'] = CONFIG_UNM;
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['accountdata'] = json_encode($data);
            $senderarr['chain_list'] = $switch_list['chain_list'];
            $senderarr['sid'] = CONFIG_SID;
            $senderarr['lid'] = CONFIG_LID;
            $senderarr['cid'] = CONFIG_CID;
            $senderarr['lname'] = $switch_list['lname'];
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $senderarr['default_langlist'] = $defaultlanguageArr;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['PRIVLIST'] = $privileges;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            if($privileges=="")
            {
                $senderarr['not_access_url'] = CONFIG_BASE_URL."companyadmin/assets/block-website.png";
            }
            if(CONFIG_LOGINTYPE==1){
                $senderarr['headerdata'] = $headerdata;
                $senderarr['chartdays'] = json_encode($formatted_range);
                $senderarr['revenuebydate'] = json_encode($revenuebydate);
                $senderarr['topsalingproduct'] = $topsalingproduct;
                $senderarr['orderlist'] = $order_data;
            }
            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_dashboard;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }

}

?>