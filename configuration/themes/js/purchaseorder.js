/**
 * Created by parghi2 on 17/4/18.
 */
function vendorChangeEvent() {
    var vendorId = jQuery("#po_sel_vendor").val();
    jQuery('#po_sel_category').empty();
    jQuery('#po_sel_category').html('<option value="0">'+plz_select+'</option>');
    jQuery('#po_sel_item').empty();
    jQuery('#po_sel_item').append('<option value="0">'+plz_select+'</option>');
    jQuery("#po_item_qty").val('');
    jQuery("#po_sel_unit").empty();
    jQuery('#po_sel_unit').append('<option value="0">'+plz_select+'</option>');
    jQuery("#po_rate_pu").val('');
    jQuery("#po_total_rate").val('');
    if (vendorId == '' || vendorId == 0) {
        jQuery("#po_sel_vendor").addClass('invalid');
        $('#po_add_modal [data-toggle="tooltip"], .tooltip').tooltip('show');
        /*Disable Elements*/
        jQuery("#po_sel_category").prop("disabled", true);
        jQuery("#po_sel_item").prop("disabled", true);
        jQuery("#po_item_qty").prop("disabled", true);
        jQuery("#po_sel_unit").prop("disabled", true);
        jQuery("#po_rate_pu").prop("disabled", true);
        /*Disable Elements*/
    } else {
        /*Enable Elements*/
        jQuery("#po_sel_vendor").removeClass('invalid');
        $('#po_add_modal [data-toggle="tooltip"], .tooltip').tooltip('destroy');
        jQuery("#po_sel_category").prop("disabled", false);
        jQuery("#po_sel_item").prop("disabled", false);
        jQuery("#po_item_qty").prop("disabled", false);
        jQuery("#po_sel_unit").prop("disabled", false);
        jQuery("#po_rate_pu").prop("disabled", false);
        /*Enable Elements*/
        getVendorCategory(vendorId);
    }
}

function getVendorCategory(vendorId) {
    if (vendorId != '' && vendorId != 0) {
        var arr = {};
        arr['service'] = "purchaseorder";
        arr['opcode'] = "getVendorCategory";
        arr['vendorid'] = vendorId;
        var str = urlencode(JSON.stringify(arr));
        HttpSendRequest(str, function (data) {
            var ObjData_table = data.categories;
            if (typeof ObjData_table != 'undefined' && ObjData_table != '' && ObjData_table.length > 0) {
                for (var i = 0; i < ObjData_table.length; i++) {
                    jQuery('#po_sel_category').append('<option value="' + ObjData_table[i]['lnkcategoryid'] + '">' + ObjData_table[i]['category_name'] + ' </option>');
                }
            }
        });
    }
}

function getRawMaterial(itemId, unitId, unitRate) {
    var catId = jQuery("#po_sel_category").val();
    jQuery('#po_sel_item').empty();
    jQuery('#po_sel_item').append('<option value="0">'+plz_select+'</option>');
    jQuery("#po_sel_unit").empty();
    jQuery('#po_sel_unit').append('<option value="0">'+plz_select+'</option>');
    jQuery("#po_item_qty").val('');
    jQuery("#po_rate_pu").val('');
    jQuery("#po_total_rate").val('');
    var arr = {};
    arr['service'] = "purchaseorder";
    arr['opcode'] = "getRawMaterialByCategory";
    arr['rawcatid'] = catId;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function (data) {
        var ObjData_table = data['Data'];
        for (var i = 0; i < ObjData_table.length; i++) {
            if (itemId != '' && itemId == ObjData_table[i]['id']) {
                jQuery('#po_sel_item').append('<option value="' + ObjData_table[i]['id'] + '" selected>' + ObjData_table[i]['name'] + ' </option>');
            } else {
                jQuery('#po_sel_item').append('<option value="' + ObjData_table[i]['id'] + '">' + ObjData_table[i]['name'] + ' </option>');
            }
            if (i == (ObjData_table.length - 1) && unitId != '') {
                getItemUnits(unitId, unitRate);
            }
        }
    });
}

function getItemUnits(unitId, rate) {
    var itemId = jQuery("#po_sel_item").val();
    var vendorId = jQuery("#po_sel_vendor").val();

    jQuery('#po_sel_unit').empty();
    jQuery('#po_sel_unit').append('<option value="0">'+plz_select+'</option>');

    if (unitId == '') {
        jQuery("#po_item_qty").val('');
        jQuery("#po_rate_pu").val('');
    }
    jQuery("#po_total_rate").val('');

    if (itemId != '' && itemId != 0 && vendorId != '') {
        var arr = {};
        arr['service'] = "purchaseorder";
        arr['opcode'] = "getUnitRate";
        arr['itemid'] = itemId;
        var str = urlencode(JSON.stringify(arr));
        HttpSendRequest(str, function (data) {
            var ObjData_table = data['Data'];
            for (var i = 0; i < ObjData_table.length; i++) {
                if (unitId != '' && unitId != 0 && unitId == ObjData_table[i]['unitunkid']) {
                    jQuery('#po_sel_unit').append('<option value="' + ObjData_table[i]['unitunkid'] + '" data-attr="' + ObjData_table[i]['unit'] + '" data-rate="' + ObjData_table[i]['rateperunit'] + '" selected>' + ObjData_table[i]['name'] + ' </option>');
                } else {
                    jQuery('#po_sel_unit').append('<option value="' + ObjData_table[i]['unitunkid'] + '" data-attr="' + ObjData_table[i]['unit'] + '" data-rate="' + ObjData_table[i]['rateperunit'] + '">' + ObjData_table[i]['name'] + ' </option>');
                }
            }
            if (rate == '') {
                if (ObjData_table.length > 0 && ObjData_table[0].rateperunit != '') {
                    jQuery("#po_actual_rate").val(ObjData_table[0].rateperunit);
                }
            }
        });
        if (rate != '') {
            jQuery("#po_actual_rate").val(rate);
            calculateItemRate();
        }
    }
}

function setUnitRate() {
    var unit_val = jQuery('#po_sel_unit').find(':selected').attr('data-attr');
    var item_rate = 0;
    if (typeof unit_val != 'undefined') {
        var rate = jQuery("#po_actual_rate").val();
        item_rate = parseFloat(unit_val * rate);
    }
    jQuery("#po_rate_pu").val(item_rate.toFixed(round_off_digit));
    calculateItemRate();
}

function calculateItemRate() {
    var rate = jQuery("#po_rate_pu").val();
    var quantity = jQuery("#po_item_qty").val();
    if (jQuery.isNumeric(rate) && quantity > 0) {
        var total = parseFloat(rate * quantity);
        jQuery("#po_total_rate").val(total.toFixed(round_off_digit));
    } else {
        jQuery("#po_total_rate").val('');
    }
    updateTaxDiscount();
}

function calculateTotalAmount() {
    var totalAmount = 0;
    jQuery("#po_total_amount").val(totalAmount.toFixed(round_off_digit));
    var list = sessionStorage.getItem('PurchaseOrderItemList');
    var item_cnt = parseInt(jQuery("#po_item_cnt").val());
    var cnt = 0;
    if (list != null) {
        var arr_item = jQuery.parseJSON(list);
        jQuery.each(arr_item, function (key, value) {
            totalAmount = totalAmount + parseFloat(value['item_total_amt']);
            cnt++;
            if (cnt == item_cnt) {
                jQuery("#po_total_amount").val(totalAmount.toFixed(round_off_digit));
            }
        });
    }
}

function update_item_tax(current) {
    var currentKey = jQuery(current).attr('data-key');
    if (jQuery(current).is(':checked')) {
        jQuery('#item_tax_' + currentKey + '_amount').removeAttr('disabled');
        jQuery('#item_tax_' + currentKey + '_tax_total').removeAttr('disabled');
    } else {
        jQuery('#item_tax_' + currentKey + '_amount').attr('disabled', 'disabled').prop('disabled', true);
        jQuery('#item_tax_' + currentKey + '_tax_total').attr('disabled', 'disabled').prop('disabled', 'disabled');
    }
}

function addItemToPO() {
    if (validate_form('po_order_form')) {
        var purchaseorder_id = jQuery("#po_id").val();
        var item_cnt = parseInt(jQuery("#po_item_cnt").val());
        var last_row = parseInt(jQuery("#po_last_item").val());
        var new_cnt = item_cnt + 1;
        var rowNo = last_row + 1;
        var total_tax_amt = 0;
        var item_disc = 0;
        var item_amt = parseFloat(jQuery("#po_rate_pu").val() * jQuery("#po_item_qty").val());
        if (jQuery("#po_discount_per").val() != '' && jQuery("#po_discount_per").val() > 0) {
            item_disc = parseFloat(item_amt * (jQuery("#po_discount_per").val() / 100));
        }

        /*Add Tax*/
        var arr_tax = {};
        jQuery.each(jQuery("input[name='po_item_tax[]']:checked"), function () {
            var tax_amt = 0;
            var tax_id = $(this).val();
            var tax_key = $(this).attr('data-key');
            var posting_rule = jQuery("#item_tax_" + tax_key + "_postingrule").val();
            var tax_apply_after = jQuery("#item_tax_" + tax_key + "_taxapplyafter").val();
            var tax_name = jQuery("#item_tax_" + tax_key + "_taxname").val();
            var tax_val = jQuery("#item_tax_" + tax_key + "_amount").val();
            var tax_detId = jQuery("#item_tax_" + tax_key + "_taxdetailunkid").val();
            if (jQuery.isNumeric(tax_val) == true && tax_val > 0) {
                arr_tax[tax_id] = {};
                var per_tax_amt = jQuery("#item_tax_" + tax_key + "_tax_total").val();
                if (posting_rule == 1) {
                    if (tax_apply_after == 1) {
                        tax_amt = (((item_amt - item_disc) * tax_val) / 100);
                    } else {
                        tax_amt = (item_amt * tax_val) / 100;
                    }
                } else {
                    tax_amt = tax_val;
                }
                arr_tax[tax_id]['tax_id'] = tax_id;
                arr_tax[tax_id]['tax_detailId'] = tax_detId;
                arr_tax[tax_id]['tax_value'] = tax_val;
                arr_tax[tax_id]['tax_amt'] = parseFloat(tax_amt).toFixed(round_off_digit);
                arr_tax[tax_id]['tax_apply_after'] = tax_apply_after;
                arr_tax[tax_id]['tax_name'] = tax_name;
                arr_tax[tax_id]['posting_rule'] = posting_rule;
                total_tax_amt = total_tax_amt + parseFloat(tax_amt);
            }
        });
        /*Add Tax*/
        /*Append Row*/
        var final_item_amt = item_amt - item_disc + total_tax_amt;
        var detail = "";
        detail += '<tr id="po_item_row' + rowNo + '" row_no="' + rowNo + '">';
        detail += '<td align="left" style="width: 5%" id="item_no_' + rowNo + '">' + rowNo + '</td>';
        detail += '<td align="left" style="width: 25%" id="item_name_' + rowNo + '">' + jQuery("#po_sel_item option:selected").text();
        +'</td>';
        detail += '<td align="left" style="width: 5%" id="item_qty_' + rowNo + '">' + jQuery("#po_item_qty").val();
        +'</td>';
        detail += '<td align="left" style="width: 10%" id="item_unit_' + rowNo + '">' + jQuery("#po_sel_unit option:selected").text();
        +'</td>';
        detail += '<td align="left" style="width: 10%" id="item_rpu_' + rowNo + '">' + jQuery("#po_rate_pu").val();
        +'</td>';
        detail += '<td align="left" style="width: 10%" id="item_disc_' + rowNo + '">' + item_disc.toFixed(round_off_digit) + '</td>';
        detail += '<td align="left" style="width: 10%" id="item_tax_' + rowNo + '">' + total_tax_amt.toFixed(round_off_digit) + '</td>';
        detail += '<td align="left" style="width: 15%" id="item_amt_' + rowNo + '">' + final_item_amt.toFixed(round_off_digit) + '</td>';
        if (purchaseorder_id != 0) {
            if (EDIT_PURCHASE_ORDER != -1) {
                detail += '<td align="left" style="width: 5%" id="item_edit_' + rowNo + '"><a class="update_ico" onclick="getSelectedItem(' + rowNo + ')" >' +
                    '<i class="fa fa-pencil"></i></a></td>';
            }
        }
        else {
            detail += '<td align="left" style="width: 5%" id="item_edit_' + rowNo + '"><a class="update_ico" onclick="getSelectedItem(' + rowNo + ')" >' +
                '<i class="fa fa-pencil"></i></a></td>';
        }
        if (purchaseorder_id != 0) {
            if (DELETE_PURCHASE_ORDER != -1) {
                detail += '<td align="left" style="width: 5%" id="item_del_' + rowNo + '"><a class="update_ico" onclick="removeItem(' + rowNo + ')" >' +
                    '<i class="fa fa-trash-o"></i></a></td>';
            }
        }
        else {
            detail += '<td align="left" style="width: 5%" id="item_del_' + rowNo + '"><a class="update_ico" onclick="removeItem(' + rowNo + ')" >' +
                '<i class="fa fa-trash-o"></i></a></td>';
        }
        detail += '</td></tr>';
        jQuery("#po_items_list").append(detail);
        jQuery("#po_item_cnt").val(new_cnt);
        jQuery("#po_last_item").val(rowNo);
        /*Append Row*/
        /*Add item to session Storage*/
        var list = sessionStorage.getItem('PurchaseOrderItemList');
        if (list != null) {
            var arr_item = jQuery.parseJSON(list);
        } else {
            var arr_item = {};
        }
        arr_item[rowNo] = {};
        arr_item[rowNo]['itm_no'] = rowNo;
        arr_item[rowNo]['itm_detId'] = 0;
        arr_item[rowNo]['itm_category'] = jQuery("#po_sel_category").val();
        arr_item[rowNo]['item_id'] = jQuery("#po_sel_item").val();
        arr_item[rowNo]['item_unit'] = jQuery("#po_sel_unit").val();
        arr_item[rowNo]['item_qty'] = jQuery("#po_item_qty").val();
        arr_item[rowNo]['item_base_rate'] = jQuery("#po_actual_rate").val();
        arr_item[rowNo]['item_rpu'] = jQuery("#po_rate_pu").val();
        arr_item[rowNo]['item_total_amt'] = final_item_amt.toFixed(round_off_digit);
        arr_item[rowNo]['item_disc_per'] = jQuery("#po_discount_per").val();
        arr_item[rowNo]['item_disc_amt'] = item_disc.toFixed(round_off_digit);
        arr_item[rowNo]['item_tax_amt'] = total_tax_amt.toFixed(round_off_digit);
        arr_item[rowNo]['item_tax_det'] = arr_tax;
        var write_arr = JSON.stringify(arr_item);
        sessionStorage.setItem('PurchaseOrderItemList', write_arr);
        /*Add item to session Storage*/
        if (new_cnt > 0) {
            jQuery("#po_btn_submit").removeAttr("disabled");
        }
        resetItem();
        calculateTotalAmount();
    }

    else {
        if (jQuery("#po_date").val() == '') {
            jQuery("#po_date").addClass('invalid');
        } else {
            alertify.alert(lblerrormsg,lblok);
        }
    }
}

function resetItem() {
    var row_cnt = jQuery("#po_item_cnt").val();
    if (row_cnt > 0) {
        jQuery("#po_sel_vendor").prop("disabled", true);
        jQuery("#po_date").prop("disabled", true);
    } else {
        jQuery("#po_btn_submit").prop("disabled", true);
        jQuery("#po_sel_vendor").removeAttr("disabled");
        jQuery("#po_date").removeAttr("disabled");
        jQuery("#po_sel_vendor").val(0);
        vendorChangeEvent();
    }
    jQuery("#po_selected_item").val(0);
    jQuery("#po_edit_item").hide();
    var AddId = jQuery("#po_id").val();
    if (AddId != 0) {
        if (ADD_PURCHASE_ORDER != -1) {
            jQuery("#po_add_item").hide();
        }
    }
    else {
        jQuery("#po_add_item").show();
    }
    jQuery("#po_cancel_item").hide();
    jQuery(".invalid").removeClass('invalid');
    jQuery("#po_sel_category").val(0);
    jQuery("#po_sel_item").empty();
    jQuery('#po_sel_item').append('<option value="0">'+plz_select+'</option>');
    jQuery("#po_sel_unit").empty();
    jQuery('#po_sel_unit').append('<option value="0">'+plz_select+'</option>');
    jQuery("#po_item_qty").val('');
    jQuery("#po_rate_pu").val('');
    jQuery("#po_total_rate").val('');
    jQuery("#po_actual_rate").val('');
    emptyTaxDiscount();
}

function getSelectedItem(Id) {
    var list = sessionStorage.getItem('PurchaseOrderItemList');
    jQuery("#po_selected_item").val(Id);
    emptyTaxDiscount();
    if (typeof list != 'undefined') {
        var arr_item = jQuery.parseJSON(list);
        var arr_detail = arr_item[Id];

        jQuery("#po_edit_item").show();
        jQuery("#po_add_item").hide();
        jQuery("#po_cancel_item").show();

        /*set selected all values*/
        jQuery("#po_sel_category").val(arr_detail['itm_category']);
        jQuery("#po_item_qty").val(arr_detail['item_qty']);
        jQuery("#po_rate_pu").val(arr_detail['item_rpu']);
        jQuery.when(getRawMaterial(arr_detail['item_id'], arr_detail['item_unit'], arr_detail['item_base_rate'])).then(
            jQuery("#po_item_qty").val(arr_detail['item_qty']),
            jQuery("#po_rate_pu").val(arr_detail['item_rpu'])
        );
        /*set selected all values*/
        /*set Discount*/
        jQuery("#po_discount_per").val(arr_detail['item_disc_per']);
        jQuery("#po_discount_amt").val(arr_detail['item_disc_amt']);
        /*set Discount*/

        /*set tax*/
        if (typeof arr_detail['item_tax_det'] != 'undefined') {
            var arr_tax = arr_detail['item_tax_det'];
            jQuery.each(arr_tax, function (tax_key, tax_value) {
                var key_no = jQuery("input[name='po_item_tax[]'][value='" + tax_value['tax_id'] + "']").attr('data-key');
                if (typeof key_no != 'undefined') {
                    jQuery("input[name='po_item_tax[]'][value='" + tax_value['tax_id'] + "']").trigger('click');
                    jQuery("#item_tax_" + key_no + "_amount").val(tax_value['tax_value']);
                    jQuery("#item_tax_" + key_no + "_tax_total").val(tax_value['tax_amt']);
                }
            });
        }
        /*set tax*/
    }
}

function editItem() {
    if (validate_form('po_order_form')) {
        var current_row = jQuery("#po_selected_item").val();
        var list = sessionStorage.getItem('PurchaseOrderItemList');
        if (list == null) {
            var arr = {};
            arr[current_row] = {};
        } else {
            var arr = jQuery.parseJSON(list);
            arr[current_row] = arr[current_row];
        }

        var total_tax_amt = 0;
        var item_disc = 0;
        var item_amt = parseFloat(jQuery("#po_rate_pu").val() * jQuery("#po_item_qty").val());
        if (jQuery("#po_discount_per").val() != '' && jQuery("#po_discount_per").val() > 0) {
            item_disc = parseFloat(item_amt * (jQuery("#po_discount_per").val() / 100));
        }
        /*Update Tax*/
        var arr_tax = {};
        jQuery.each(jQuery("input[name='po_item_tax[]']:checked"), function () {
            var tax_amt = 0;
            var tax_id = $(this).val();
            var tax_key = $(this).attr('data-key');
            var posting_rule = jQuery("#item_tax_" + tax_key + "_postingrule").val();
            var tax_apply_after = jQuery("#item_tax_" + tax_key + "_taxapplyafter").val();
            var tax_name = jQuery("#item_tax_" + tax_key + "_taxname").val();
            var tax_val = jQuery("#item_tax_" + tax_key + "_amount").val();
            var tax_detId = jQuery("#item_tax_" + tax_key + "_taxdetailunkid").val();
            if (jQuery.isNumeric(tax_val) == true && tax_val > 0) {
                arr_tax[tax_id] = {};
                if (posting_rule == 1) {
                    if (tax_apply_after == 1) {
                        tax_amt = (((item_amt - item_disc) * tax_val) / 100);
                    } else {
                        tax_amt = (item_amt * tax_val) / 100;
                    }
                } else {
                    tax_amt = tax_val;
                }
                arr_tax[tax_id]['tax_id'] = tax_id;
                arr_tax[tax_id]['tax_detailId'] = tax_detId;
                arr_tax[tax_id]['tax_value'] = tax_val;
                arr_tax[tax_id]['tax_amt'] = parseFloat(tax_amt).toFixed(round_off_digit);
                arr_tax[tax_id]['tax_apply_after'] = tax_apply_after;
                arr_tax[tax_id]['tax_name'] = tax_name;
                arr_tax[tax_id]['posting_rule'] = posting_rule;
                total_tax_amt = total_tax_amt + parseFloat(tax_amt);
            }
        });
        /*Update Tax*/
        /*Update row */
        var final_item_amt = item_amt - item_disc + total_tax_amt;
        jQuery("#item_name_" + current_row).html(jQuery("#po_sel_item option:selected").text());
        jQuery("#item_qty_" + current_row).html(jQuery("#po_item_qty").val());
        jQuery("#item_unit_" + current_row).html(jQuery("#po_sel_unit option:selected").text());
        jQuery("#item_rpu_" + current_row).html(jQuery("#po_rate_pu").val());
        jQuery("#item_disc_" + current_row).html(item_disc.toFixed(round_off_digit));
        jQuery("#item_tax_" + current_row).html(total_tax_amt.toFixed(round_off_digit));
        jQuery("#item_amt_" + current_row).html(final_item_amt.toFixed(round_off_digit));
        /*Update row */
        /*update item to session Storage*/
        arr[current_row]['itm_category'] = jQuery("#po_sel_category").val();
        arr[current_row]['item_id'] = jQuery("#po_sel_item").val();
        arr[current_row]['item_unit'] = jQuery("#po_sel_unit").val();
        arr[current_row]['item_qty'] = jQuery("#po_item_qty").val();
        arr[current_row]['item_base_rate'] = jQuery("#po_actual_rate").val();
        arr[current_row]['item_rpu'] = jQuery("#po_rate_pu").val();
        arr[current_row]['item_total_amt'] = final_item_amt.toFixed(round_off_digit);
        arr[current_row]['item_disc_per'] = jQuery("#po_discount_per").val();
        arr[current_row]['item_disc_amt'] = item_disc.toFixed(round_off_digit);
        arr[current_row]['item_tax_amt'] = total_tax_amt.toFixed(round_off_digit);
        arr[current_row]['item_tax_det'] = arr_tax;
        var write_arr = JSON.stringify(arr);
        sessionStorage.setItem('PurchaseOrderItemList', write_arr);
        /*Update item to session Storage*/
        resetItem();
        calculateTotalAmount();
    }
}

function removeItem(Id) {
    jQuery("#po_item_row" + Id).remove();
    var item_cnt = parseInt(jQuery("#po_item_cnt").val());
    var last_row = parseInt(jQuery("#po_last_item").val());

    var start = Id + 1;
    var list = sessionStorage.getItem('PurchaseOrderItemList');
    if (list != null) {
        var arr = jQuery.parseJSON(list);
    } else {
        var arr = {};
    }
    if (arr.hasOwnProperty(Id) == true) {
        //delete arr[Id];
        arr[Id] = {};
    }
    for (var i = start; i <= item_cnt; i++) {
        var j = i - 1;
        jQuery("#po_item_row" + i).attr("id", "po_item_row" + j);
        jQuery("#item_no_" + i).html(j);
        jQuery("#item_no_" + i).attr("id", "item_no_" + j);
        jQuery("#item_name_" + i).attr("id", "item_name_" + j);
        jQuery("#item_qty_" + i).attr("id", "item_qty_" + j);
        jQuery("#item_unit_" + i).attr("id", "item_unit_" + j);
        jQuery("#item_rpu_" + i).attr("id", "item_rpu_" + j);
        jQuery("#item_disc_" + i).attr("id", "item_disc_" + j);
        jQuery("#item_tax_" + i).attr("id", "item_tax_" + j);
        jQuery("#item_amt_" + i).attr("id", "item_amt_" + j);

        jQuery("#item_edit_" + i + " a").attr("onclick", "getSelectedItem('" + j + "')");
        jQuery("#item_edit_" + i).attr("id", "item_edit_" + j);

        jQuery("#item_del_" + i + " a").attr("onclick", "removeItem('" + j + "')");
        jQuery("#item_del_" + i).attr("id", "item_del_" + j);

        arr[j] = arr[i];
    }
    if (arr.hasOwnProperty(item_cnt) == true) {
        delete arr[item_cnt];
    }
    var new_cnt = item_cnt - 1;
    var key = JSON.stringify(arr);
    sessionStorage.setItem('PurchaseOrderItemList', key);
    jQuery("#po_item_cnt").val(new_cnt);
    jQuery("#po_last_item").val(new_cnt);
    calculateTotalAmount();
    resetItem();
}

function calculateItemTax(obj) {
    var key_no = $(obj).attr('data-key');
    var tax_value = jQuery("#item_tax_" + key_no + "_amount").val();
    jQuery("#item_tax_" + key_no + "_tax_total").val(0);
    if (jQuery.isNumeric(tax_value) == true && tax_value >= 0) {
        var item_disc = 0;
        var tax_amt = 0;
        var item_amt = parseFloat(jQuery("#po_rate_pu").val() * jQuery("#po_item_qty").val());
        var posting_rule = jQuery("#item_tax_" + key_no + "_postingrule").val();
        var tax_apply_after = jQuery("#item_tax_" + key_no + "_taxapplyafter").val();
        if (jQuery("#po_discount_per").val() != '' && jQuery("#po_discount_per").val() > 0) {
            item_disc = parseFloat(item_amt * (jQuery("#po_discount_per").val() / 100));
        }
        if (posting_rule == 1) {
            if (tax_apply_after == 1) {
                tax_amt = (((item_amt - item_disc) * tax_value) / 100);
            } else {
                tax_amt = (item_amt * tax_value) / 100;
            }
        } else {
            tax_amt = tax_value;
        }
        tax_amt = parseFloat(tax_amt);
        tax_amt = tax_amt.toFixed(round_off_digit);
        jQuery("#item_tax_" + key_no + "_tax_total").val(tax_amt);
    }
}

function calculateItemDiscount() {
    if (jQuery("#po_discount_per").val() > 100) {
        jQuery("#po_discount_per").val(100);
    }
    var item_amt = parseFloat(jQuery("#po_rate_pu").val() * jQuery("#po_item_qty").val());
    var disc_per = jQuery("#po_discount_per").val();
    var item_disc = 0;
    jQuery("#po_discount_amt").val(0);
    if (jQuery.isNumeric(disc_per) == true && disc_per >= 0) {
        if (jQuery.isNumeric(item_amt) == true) {
            item_disc = parseFloat(item_amt * (disc_per / 100));
            jQuery("#po_discount_amt").val(item_disc.toFixed(round_off_digit));
        }
    }
}

function updateTaxDiscount() {
    var item_amt = parseFloat(jQuery("#po_rate_pu").val() * jQuery("#po_item_qty").val());
    /*Update Discount*/
    var disc_per = jQuery("#po_discount_per").val();
    var item_disc = 0;
    if (jQuery.isNumeric(disc_per) == true && disc_per > 0) {
        if (jQuery.isNumeric(item_amt) == true) {
            item_disc = parseFloat(item_amt * (disc_per / 100));
            jQuery("#po_discount_amt").val(item_disc.toFixed(round_off_digit));
        }
    }
    /*Update Discount*/
    /*Update Tax*/
    jQuery.each(jQuery("input[name='po_item_tax[]']:checked"), function () {
        var tax_amt = 0;
        var tax_key = $(this).attr('data-key');
        var posting_rule = jQuery("#item_tax_" + tax_key + "_postingrule").val();
        var tax_apply_after = jQuery("#item_tax_" + tax_key + "_taxapplyafter").val();
        var tax_val = jQuery("#item_tax_" + tax_key + "_amount").val();
        if (jQuery.isNumeric(tax_val) == true && tax_val > 0) {
            if (posting_rule == 1) {
                if (tax_apply_after == 1) {
                    tax_amt = (((item_amt - item_disc) * tax_val) / 100);
                } else {
                    tax_amt = (item_amt * tax_val) / 100;
                }
            } else {
                tax_amt = tax_val;
            }
            tax_amt = parseFloat(tax_amt);
            jQuery("#item_tax_" + tax_key + "_tax_total").val(tax_amt.toFixed(round_off_digit));
        }
    });
    /*Update Tax*/
}

function emptyTaxDiscount() {
    jQuery("input[name='po_item_tax[]']").prop('checked', false);
    jQuery(".cl_tax_amt").val('');
    jQuery(".cl_tax_amt").prop("disabled", true);
    jQuery("#po_discount_per").val('');
    jQuery("#po_discount_amt").val('');
}

var checkopened = 0;

function addPurchaseOrder() {
    if (checkopened == 0) {
        var po_items_list = sessionStorage.getItem('PurchaseOrderItemList');
        var list_items = jQuery.parseJSON(po_items_list);
        var arr = {};
        arr['service'] = "purchaseorder";
        arr['opcode'] = "addPurChaseOrder";
        arr['id'] = jQuery("#po_id").val();
        arr['vendor_id'] = jQuery("#po_sel_vendor").val();
        arr['po_remarks'] = jQuery("#po_remarks").val();
        arr['po_date'] = jQuery("#po_date").val();
        arr['total_amount'] = jQuery("#po_total_amount").val();
        arr['po_items'] = list_items;
        var str = urlencode(JSON.stringify(arr));
        HttpSendRequest(str, function (data) {
            if (data['Success'] == 'True') {
                var modalId = jQuery("#po_add_modal").parent().attr("modal_id");
                sessionStorage.removeItem('PurchaseOrderItemList');
                closeModal(modalId);
                alertify.success(data['Message']);
                refreshlist();
            } else {
                alertify.error(data['Message']);
            }
        });
    }
    checkopened++;
}

function getPODetail() {
    var po_id = jQuery("#po_id").val();
    if (po_id != '' && po_id > 0) {
        var arr = {};
        arr['service'] = "purchaseorder";
        arr['opcode'] = "getPoDetail";
        arr['id'] = po_id;
        var str = urlencode(JSON.stringify(arr));
        HttpSendRequest(str, function (data) {
            if (data['Success'] == "True") {
                if (typeof data['order'] != 'undefined') {
                    /*Bind order detail*/
                    var po_detail = data['order'];
                    jQuery("#po_order_no").val(po_detail['order_doc_num']);
                    jQuery("#po_date").val(po_detail['order_date']);
                    jQuery("#po_date").attr('disabled', 'true');

                    jQuery("#po_sel_vendor").val(po_detail['lnkvendorid']);
                    jQuery("#po_sel_vendor").attr('disabled', 'true');
                    jQuery("#po_remarks").val(po_detail['remarks']);
                    vendorChangeEvent();

                    if (ADD_PURCHASE_ORDER != -1) {
                        jQuery("#po_add_item").show();
                    }
                    else {
                        jQuery("#po_add_item").hide();
                    }
                    /*Bind order detail*/
                    /*Bind Purchase Order Items*/
                    var arr_items = {};
                    var po_items = data['orderDetail'];
                    if (jQuery.isArray(po_items) == true && po_items.length > 0) {
                        jQuery("#po_item_cnt").val(po_items.length);
                        jQuery("#po_btn_submit").removeAttr('disabled');
                        var rowNo = 0;
                        jQuery.each(po_items, function (key_item, value_item) {
                            /*Create object for session storage*/
                            rowNo++;
                            var final_item_amt = parseFloat(value_item['rate'] * value_item['qty']) - parseFloat(value_item['discount_amount']) + parseFloat(value_item['tax_amount']);
                            final_item_amt = final_item_amt.toFixed(round_off_digit);
                            arr_items[rowNo] = {};
                            arr_items[rowNo]['itm_no'] = rowNo;
                            arr_items[rowNo]['itm_detId'] = value_item['orderdetailunkid'];
                            arr_items[rowNo]['itm_category'] = value_item['lnkrawcatid'];
                            arr_items[rowNo]['item_id'] = value_item['lnkrawid'];
                            arr_items[rowNo]['item_unit'] = value_item['lnkunitid'];
                            arr_items[rowNo]['item_qty'] = value_item['qty'];
                            arr_items[rowNo]['item_base_rate'] = "0.00";
                            arr_items[rowNo]['item_rpu'] = value_item['rate'];
                            arr_items[rowNo]['item_total_amt'] = final_item_amt;
                            arr_items[rowNo]['item_disc_per'] = value_item['discount_per'];
                            arr_items[rowNo]['item_disc_amt'] = value_item['discount_amount'];
                            arr_items[rowNo]['item_tax_amt'] = value_item['tax_amount'];
                            if (typeof (value_item['tax_description']) != "undefined" && value_item['tax_description'] != '') {
                                arr_items[rowNo]['item_tax_det'] = JSON.parse(value_item['tax_description']);
                            } else {
                                arr_items[rowNo]['item_tax_det'] = {};
                            }
                            /*Create object for session storage*/
                            /*Bind row to table*/
                            var detail = "";
                            detail += '<tr id="po_item_row' + rowNo + '" row_no="' + rowNo + '">';
                            detail += '<td align="left" style="width: 5%" id="item_no_' + rowNo + '">' + rowNo + '</td>';
                            detail += '<td align="left" style="width: 25%" id="item_name_' + rowNo + '">' + value_item['storeitem'] + '</td>';
                            detail += '<td align="left" style="width: 5%" id="item_qty_' + rowNo + '">' + value_item['qty'] + '</td>';
                            detail += '<td align="left" style="width: 10%" id="item_unit_' + rowNo + '">' + value_item['unit'] + '</td>';
                            detail += '<td align="left" style="width: 10%" id="item_rpu_' + rowNo + '">' + value_item['rate'] + '</td>';
                            detail += '<td align="left" style="width: 10%" id="item_disc_' + rowNo + '">' + value_item['discount_amount'] + '</td>';
                            detail += '<td align="left" style="width: 10%" id="item_tax_' + rowNo + '">' + value_item['tax_amount'] + '</td>';
                            detail += '<td align="left" style="width: 15%" id="item_amt_' + rowNo + '">' + final_item_amt + '</td>';
                            if (EDIT_PURCHASE_ORDER != -1) {
                                detail += '<td align="left" style="width: 5%" id="item_edit_' + rowNo + '"><a class="update_ico" onclick="getSelectedItem(' + rowNo + ')" >' +
                                    '<i class="fa fa-pencil"></i></a></td>';
                            }
                            if (DELETE_PURCHASE_ORDER != -1) {
                                detail += '<td align="left" style="width: 5%" id="item_del_' + rowNo + '"><a class="update_ico" onclick="removeItem(' + rowNo + ')" >' +
                                    '<i class="fa fa-trash-o"></i></a></td>';
                            }
                            detail += '</td></tr>';
                            jQuery("#po_items_list").append(detail);
                            /*Bind row to table*/
                            /*Add to session storage*/
                            if (rowNo == po_items.length) {
                                jQuery("#po_last_item").val(rowNo);
                                var write_arr = JSON.stringify(arr_items);
                                sessionStorage.setItem('PurchaseOrderItemList', write_arr);
                                calculateTotalAmount();
                            }
                            /*Add to session storage*/
                        });
                    }
                    /*Bind Purchase Order Items*/
                }
            }
        });
    }
}