
//Recipe
function loaditem_rawlist(unit_hash,bind_recipe) {
    var rawobj_sub =rawdetailobj;
    var raw_detail_str='';

    jQuery('#rawmaterial_sel_'+unit_hash+'').select2();
    var store=rawobj_sub.store;
    for (var i=0;i<bind_recipe.length;i++) {
        var raw_hash = bind_recipe[i]['rawhash'];
        var unit_data=rawobj_sub[raw_hash]['unit'];

        if(raw_detail_str=='')
            raw_detail_str += raw_hash;
        else
            raw_detail_str +=","+raw_hash;
        var quantity = bind_recipe[i]['quantity'];
        if(typeof bind_recipe[i]['raw_quantity']!='undefined' && bind_recipe[i]['raw_quantity']!=null && bind_recipe[i]['raw_quantity']!=''){
            var quantity = bind_recipe[i]['raw_quantity'];
        }
        else{
            var quantity = bind_recipe[i]['quantity'];
        }

        var card = '<div id="rawmaterial_'+unit_hash+'_' + raw_hash + '" class="col-xs-4" style="margin-bottom: 15px;"><div class="card">';
        card += '<div class="card-header collapsed" data-toggle="collapse"  data-target="#collapse-recipe_' + unit_hash + '_' + raw_hash + '" href="#collapse-recipe_' + unit_hash + '_' + raw_hash + '" style="cursor: pointer">';
        card +='<div class="card-title"> <span id="frmtitle_' +unit_hash+'_' + raw_hash + '" class="title">' + bind_recipe[i]['name']+ '</span>';
        card += '</div></div><div class="card-body panel-collapse out collapse" id="collapse-recipe_' + unit_hash + '_' + raw_hash + '">';
        card +='<div class="form-group clearfix"><div><div class="controls">';
        card+='<select name="store_'+unit_hash+'_'+raw_hash+'" class="form-control" id="store_'+unit_hash+'_'+raw_hash+'" onchange=getrateUnits("'+unit_hash+'",'+ raw_hash +',0);>';
        card += '<option class="optstore_'+unit_hash+'_'+raw_hash+'" disabled="disabled" selected="selected" value="0" >'+SELECT_STORE+'</option>';
        jQuery.each(store, function(key, value) {
            card += '<option class="optstore_'+unit_hash+'_'+raw_hash+'" value="' + key + '" >' + value['storename'] + '</option>';
        });
        card+= '</select></div></div></div>';
        card += '<div class="form-group clearfix"><div><div class="controls">';
        card += '<select name="unit_' +unit_hash+'_' + raw_hash + '" class="form-control" id="unit_' +unit_hash+'_' + raw_hash + '" onchange=getunit("'+unit_hash+'",'+ raw_hash +');>';
        card += '</select>';
        card += '<input type="hidden" name="unitval_'+unit_hash+'_' + raw_hash +'" id="unitval_'+unit_hash+'_' + raw_hash +'" value=""/>';
        card += '<input type="hidden" name="rate_'+unit_hash+'_' + raw_hash +'" id="rate_'+unit_hash+'_' + raw_hash +'" value=""/>';
        card += '<input type="hidden" class="cost_val_'+unit_hash+'" name="cost_'+unit_hash+'_' + raw_hash +'" id="cost_'+unit_hash+'_' + raw_hash +'" value=""/></div></div></div>';
        card += '<div class="form-group clearfix"><div><div class="controls"><input id="quantity_'+unit_hash+'_'  + raw_hash + '" name="quantity_'+unit_hash+'_'  + raw_hash + '" onkeypress="return isFloat(event,this.value)" class="form-control" type="text" maxlength="10" placeholder="Quantity" value="'+quantity+'" onkeyup=getcost("'+unit_hash+'",'+ raw_hash +');></div>';
        card += '<span class="hide noinventory" id="noinventory_'+unit_hash+'_'  + raw_hash + '" style="color: red;">'+No_INVENTORY+'</span></div></div></div></div></div>';
        jQuery(".option_raw_"+unit_hash+"_" +raw_hash).prop('selected',true);
        jQuery('#rawdetail_list_'+unit_hash).append(card);
        jQuery('#store_'+unit_hash+'_' + raw_hash).val(bind_recipe[i]['lnkstoreid']);

        var unitid=bind_recipe[i]['unitunkid'];
        getrateUnits(unit_hash,raw_hash,unitid);
    }
    jQuery('#rawmaterial_sel_'+unit_hash).change();
    jQuery('#raw_detail_'+unit_hash).val(raw_detail_str);
    jQuery('#old_raw_'+unit_hash).val(raw_detail_str);
}

function getRawmaterial()
{
    var req={};
    req['service'] = 'menu_item';
    req['opcode'] = 'getRawmaterial';
    var str = JSON.stringify(req);
    HttpSendRequest(str,function(data) {
        if (data['Success'] == 'True') {
            var data = data['Data'];
            var rec=data.rawmaterial;
            var arr = {};

            for(var i=0; i<rec.length; i++)
            {
                var hashkey = rec[i]['id'];
                var name = rec[i]['name'];
                arr[hashkey] = {};
                arr[hashkey]['name'] = name;
            }
            var store=data.store;
            arr['store'] = {};
            for(var i=0; i<store.length; i++)
            {
                var storeid = store[i]['storeunkid'];
                var storename = store[i]['storename'];
                arr['store'][storeid] = {};
                arr['store'][storeid]['storename'] = storename;
            }
            rawdetailobj=arr;
        }
    });
}

function selectAllRawmaterial(current)
{
    var unit = jQuery(current).attr('unit');
    jQuery('#rawmaterial_sel_'+unit).find('option').each(function(){
        jQuery(this).prop('selected',true);
    });
    jQuery('#rawmaterial_sel_'+unit).change();
    return false;
}

function selectNoneRawmaterial(current)
{
    var unit = jQuery(current).attr('unit');
    jQuery('#rawmaterial_sel_'+unit).find('option').prop( 'selected',false );
    jQuery('#rawmaterial_sel_'+unit).change();
    return false;
}

function getTotalCost(unit){
    var totalcost=0;
    jQuery(".cost_val_"+unit).each(function(){
        totalcost+=parseFloat($(this).val());
    });
    jQuery('#avg_cost_'+unit).html(totalcost.toFixed(2));
}

function getcost(unit,hashkey)
{
    var unit_val=jQuery('#unitval_'+unit+'_'+hashkey).val();
    var quantity=jQuery('#quantity_'+unit+'_'+hashkey).val();

    var rate=jQuery('#rate_'+unit+'_'+hashkey).val();
    var cost=(unit_val*quantity*rate);

    if(isNaN(cost)){
        cost = 0;
    }
    jQuery('#cost_'+unit+'_'+hashkey).val(cost);
    getTotalCost(unit);
}

function getunit(unit,hashkey)
{
    var unitval=jQuery('#unit_'+unit+'_'+hashkey).find(':selected').attr('data-unit');
    //var unitval=jQuery('#unitval_'+unit+'_'+hashkey).val();
    jQuery('#unitval_'+unit+'_'+hashkey).val(unitval);
    getcost(unit,hashkey);
}

function getrateUnits(unit,rawhash,unitid)
{
    jQuery('#noinventory_' +unit+'_'+rawhash).addClass('hide');
    jQuery('#rate_'+unit+'_'+rawhash).val('');
    var storeid=jQuery('#store_'+unit+'_'+rawhash).val();
    jQuery('#unit_'+unit+'_'+rawhash).empty();
    jQuery('#unit_'+unit+'_'+rawhash).append('<option class="optunit_'+unit+'_'+rawhash+'" disabled="disabled" selected="selected" value="0" >'+SELECT_UNIT+'</option>');

    if(storeid !='' && rawhash!=''){
        var arr = {};
        arr['service'] = "rawmaterial";
        arr['opcode'] = "getUnitByStore";
        arr['rawid'] = rawhash;
        arr['storeid'] = storeid;
        var str = urlencode(JSON.stringify(arr));
        HttpSendRequest(str, function (data) {
            var unitData = data['Data'];
            jQuery.each(unitData, function(key, value) {
                jQuery('#unit_'+unit+'_'+rawhash).append('<option class="optunit_'+unit+'_'+rawhash+'"  data-unit="' + value['unit'] + '" value="' + value['unitunkid'] + '" >' + value['name'] + '</option>');
                jQuery('#unitval_'+unit+'_'+rawhash).val(value['unit']);
                jQuery('#rate_'+unit+'_'+rawhash).val(value['rateperunit']);
                if(value['inventory'] == null || value['inventory']== 0){
                    jQuery('#noinventory_' +unit+'_'+rawhash).removeClass('hide');
                }

            });
            jQuery('#unit_'+unit+'_' + rawhash).val(unitid);
            getunit(unit,rawhash);
        });
    }
}


function loadRawmaterialDetail(current)
{
    var unit = jQuery(current).attr('unit');
    var rawDetail = jQuery('#rawmaterial_sel_'+unit).val();
    var old_raw = jQuery('#old_raw_'+unit).val();
    var oldrawDetail=old_raw.split(',');

    var difference = jQuery(rawDetail).not(oldrawDetail).get();

    var raw_detail_str='';
    var rawobj_sub =rawdetailobj;

    //Delete
    var newDiff=$(oldrawDetail).not(rawDetail).get();
    if(newDiff!=null){
        for (var i=0;i<newDiff.length;i++) {
            var hashkey = newDiff[i];
            jQuery("#rawmaterial_"+unit+"_"+hashkey).remove();
        }
    }
    var cost=0;
    var store=rawobj_sub.store;

    if (difference!=null) {
        for (var i=0;i<difference.length;i++) {
            var raw_hash = difference[i];
            var unit_data=rawobj_sub[raw_hash]['unit'];

            if (raw_detail_str=='')
                raw_detail_str += raw_hash;
            else
                raw_detail_str +=","+raw_hash;

            var card='<div id="rawmaterial_'+unit+'_'+raw_hash+'" class="col-xs-4" style="margin-bottom: 15px;"><div class="card">';
            card += '<div class="card-header collapsed" data-toggle="collapse"  data-target="#collapse-recipe_' + unit + '_' + raw_hash + '" href="#collapse-recipe_' + unit + '_' + raw_hash + '" style="cursor: pointer">';
            card +='<div class="card-title"> <span id="frmtitle_'+unit+'_'+raw_hash+'" class="title">'+rawobj_sub[raw_hash]['name']+'</span>';
            card +='</div></div><div class="card-body panel-collapse collapse out in" id="collapse-recipe_' + unit + '_' + raw_hash + '">';
            card +='<div class="form-group clearfix"><div><div class="controls">';
            card+='<select name="store_'+unit+'_'+raw_hash+'" class="form-control" id="store_'+unit+'_'+raw_hash+'" onchange=getrateUnits("'+unit+'",'+ raw_hash +',0);>';
            card += '<option class="optstore_'+unit+'_'+raw_hash+'" disabled="disabled" selected="selected" value="0" >'+SELECT_STORE+'</option>';
            jQuery.each(store, function(key, value) {
                card += '<option class="optstore_'+unit+'_'+raw_hash+'" value="' + key + '" >' + value['storename'] + '</option>';
            });
            card+= '</select></div></div></div>';
            card +='<div class="form-group clearfix"><div><div class="controls">';
            card+='<select name="unit_'+unit+'_'+raw_hash+'" class="form-control" id="unit_'+unit+'_'+raw_hash+'" onchange=getunit("'+unit+'",'+ raw_hash +');>';
            card += '<option class="optunit_'+unit+'_'+raw_hash+'" disabled="disabled" selected="selected" value="0" >'+SELECT_UNIT+'</option>';
            card+= '</select>'+'';
            card+='<input type="hidden" name="unitval_'+unit+'_'+raw_hash+'" id="unitval_'+unit+'_'+raw_hash+'" />';
            card+='<input type="hidden" name="rate_'+unit+'_'+ raw_hash +'" id="rate_'+unit+'_'+ raw_hash +'" value=""/>';
            card+='<input type="hidden" class="cost_val_'+unit+'" name="cost_'+unit+'_'+ raw_hash +'" id="cost_'+unit+'_'+ raw_hash +'" value="'+cost+'"/></div></div></div>';
            card +='<div class="form-group clearfix"><div><div class="controls"><input id="quantity_'+unit+'_'+raw_hash+'" name="quantity_'+unit+'_'+raw_hash+'" class="form-control" type="text" onkeypress="return isFloat(event,this.value)" placeholder="'+QUNTITY+'" maxlength="10" onkeyup=getcost("'+ unit +'",'+raw_hash+'); required></div>';
            card +='<span class="hide noinventory" id="noinventory_' +unit+'_'+raw_hash + '" style="color: red;">'+No_INVENTORY+'</span></div></div></div></div></div>';
            jQuery('#rawdetail_list_'+unit).append(card);
        }
        getTotalCost(unit);
        jQuery('#old_raw_'+unit).val(rawDetail);
        jQuery('#raw_detail_'+unit).val(rawDetail);
    }
    else{
        jQuery('#raw_detail_'+unit).val('');
    }
}
