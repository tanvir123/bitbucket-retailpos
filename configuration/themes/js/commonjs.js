var pageno=1;
var pagereset=true;

$(document).ready(function () {
    jQuery("#logout").click(function () {
        jQuery.ajax({
            url: "controller",
            type: "post",
            data: "service=logout&opcode=load",
            success: function (data) {
                if (data == 1) {
                    window.location = "../index.php";
                }
            }
        });
    });
});


/*function showjQloading() {
    jQuery("#content").addClass('loader');
}

function hidejQloading() {
    jQuery("#content").removeClass('loader');
}*/

function showjQloading() {
    jQuery(".loader1").show();
}

function hidejQloading() {
    jQuery(".loader1").fadeOut("slow");
}

function PrintElem(elem) {
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('<html><head><title>' + document.title + '</title>');

    mywindow.document.write('</head><body >');

    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;

}

function check_audit_activity(id, module) {
    try {
        var arr = {};
        arr['service'] = 'audit_individual_activity';
        arr['opcode'] = 'load';
        arr['id'] = id;
        arr['module'] = module;
        var str = urlencode(JSON.stringify(arr));
        HttpSendRequest(str, function (data) {
            jQuery("#modalPrimary").html(data.Data.replace('&lsquo;',"'"));
            jQuery("#modalPrimary").modal('show');
        });
    }
    catch (e) {
        alert(e);
    }
}

function check_order_activity(id, module) {
    try {
        var arr = {};
        arr['service'] = 'orderslogs';
        arr['opcode'] = 'load';
        arr['id'] = id;
        arr['module'] = module;
        var str = urlencode(JSON.stringify(arr));
        HttpSendRequest(str, function (data) {
            jQuery("#modalPrimary").html(data.Data);
            jQuery("#modalPrimary").modal('show');
        });
    }
    catch (e) {
        alert(e);
    }
}

/*function check_related_records(id,module)
{
   try
   {
      jQuery.ajax({
          url: "check_dependency",
          type: "post",
          data: "id="+id+"&module="+module,
          success: function(data){
              jQuery("#modalPrimary").html(data);
              jQuery("#modalPrimary").modal('show');
          }
      });
   }
   catch(e)
   {
       alert(e);
   }
}*/
function check_related_records(id, module) {

    var arr = {};
    arr['service'] = 'check_dependency';
    arr['opcode'] = 'load';
    arr['id'] = id;
    arr['module'] = module;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function (data) {
        jQuery("#modalPrimary").html(data.Data);
        jQuery("#modalPrimary").modal('show');
    });

}

function related_records_withLoc(id, module) {

    var arr = {};
    arr['service'] = 'check_dependency';
    arr['opcode'] = 'loadwithLocation';
    arr['id'] = id;
    arr['module'] = module;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function (data) {
        jQuery("#modalPrimary").html(data.Data);
        jQuery("#modalPrimary").modal('show');
    });
}

function displaysettings_activity(module) {
    try {
        var arr = {};
        arr['service'] = 'displaysettings_activity';
        arr['opcode'] = 'load';
        arr['module'] = module;
        var str = urlencode(JSON.stringify(arr));
        HttpSendRequest(str, function (data) {
            jQuery("#modalPrimary").html(data.Data);
            jQuery("#modalPrimary").modal('show');
        });
    }
    catch (e) {
        alert(e);
    }
}

$(document).ready(function () {
    $(".integer").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $(".decimal").keypress(function (event) {
        var value = $(this).val();
        if (event.which == 45 || event.which > 58 || event.which == 47 || event.which == 42 || event.which == 43) {
            return false;
            event.preventDefault();
        }
        if (event.which == 46 && value.indexOf('.') != -1) {
            return false;
            event.preventDefault();
        }
        return true;
    });
    $(".negativeint").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            //Allow: '-'
            ((e.keyCode == 109 || e.keyCode == 189) && e.ctrlKey === true) ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) && (e.keyCode != 109 && e.keyCode != 189)) {
            e.preventDefault();
        }
    });
});

function isFloat(event, value) {
    if (event.which == 45 || event.which > 58 || event.which == 47 || event.which == 42 || event.which == 43) {
        return false;
        event.preventDefault();
    }
    if (event.which == 46 && value.indexOf('.') != -1) {
        return false;
        event.preventDefault();
    }
    return true;
}

/*function isFloatWithRange(event, value, min, max) {

    var currentVal = jQuery(value).val();

    if (parseFloat(currentVal) < parseFloat(min)) {
        return false;
        event.preventDefault();
    }


    if (parseFloat(currentVal) > parseFloat(max)) {
        if (typeof round_off_digit != 'undefined' && round_off_digit != '') {
            if (typeof jQuery(value).attr('data-max_length') != 'undefined' && parseInt(jQuery(value).attr('data-max_length')) > 0) {
                var max_length = jQuery(value).attr('data-max_length');
                if (currentVal.length >= max_length) {
                    max = currentVal.substr(0, max_length);

                }
            }
            jQuery(value).val(parseFloat(max).toFixed(round_off_digit));
        } else {
            if (typeof jQuery(value).attr('data-max_length') != 'undefined' && parseInt(jQuery(value).attr('data-max_length')) > 0) {
                var max_length = jQuery(value).attr('data-max_length');
                if (currentVal.length >= max_length) {
                    var newVal = currentVal.substr(0, max_length);
                    jQuery(value).val(newVal);
                } else {
                    jQuery(value).val(max);
                }
            }
        }
        return false;
        event.preventDefault();
    }

    if (event.which == 45 || event.which > 58 || event.which == 47 || event.which == 42 || event.which == 43) {

        if (typeof round_off_digit != 'undefined' && round_off_digit != '' && parseFloat(currentVal) > 0 && currentVal.indexOf('.') != -1) {
            var nArray = currentVal.split('.');
            if (typeof nArray[1] != 'undefined' && nArray[1].length > round_off_digit) {
                jQuery(value).val(parseFloat(currentVal).toFixed(round_off_digit));
            }
        }
        return false;
        event.preventDefault();
    }

    if (event.which == 46 && currentVal.indexOf('.') != -1) {

        return false;
        event.preventDefault();
    }

    if (typeof round_off_digit != 'undefined' && round_off_digit != '' && parseFloat(currentVal) > 0 && currentVal.indexOf('.') != -1) {
        var nArray = currentVal.split('.');
        if (typeof nArray[1] != 'undefined' && nArray[1].length > round_off_digit) {
            jQuery(value).val(parseFloat(currentVal).toFixed(round_off_digit));
        }
    }

    return true;
}*/
function isFloatWithRange(event, value,min,max) {

    var currentVal = jQuery(value).val();

    if(parseFloat(currentVal) < parseFloat(min))
    {
        jQuery(value).val(min);
        return false;
        event.preventDefault();
    }


    if( parseFloat(currentVal) > parseFloat(max))
    {
        if(typeof round_off_digit != 'undefined' && round_off_digit !='') {
            jQuery(value).val(parseFloat(max).toFixed(round_off_digit));
        }else{
            jQuery(value).val(max);
        }
        return false;
        event.preventDefault();
    }

    if (event.which == 45 || event.which > 58 || event.which == 47 || event.which == 42 || event.which == 43) {

        if(typeof round_off_digit != 'undefined' && round_off_digit !='' && parseFloat(currentVal) > 0 && currentVal.indexOf('.')!= -1)
        {
            var nArray = currentVal.split('.');
            if(typeof nArray[1] != 'undefined' && nArray[1].length > round_off_digit)
            {
                jQuery(value).val(parseFloat(currentVal).toFixed(round_off_digit));
            }
        }
        return false;
        event.preventDefault();
    }

    if (event.which == 46 && currentVal.indexOf('.') != -1) {

        return false;
        event.preventDefault();
    }

    if(typeof round_off_digit != 'undefined' && round_off_digit !='' && parseFloat(currentVal) > 0 && currentVal.indexOf('.')!= -1)
    {
        var nArray = currentVal.split('.');
        if(typeof nArray[1] != 'undefined' && nArray[1].length > round_off_digit)
        {
            jQuery(value).val(parseFloat(currentVal).toFixed(round_off_digit));
        }
    }

    return true;
}

function urldecode(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}

function urlencode(str) {
    return encodeURIComponent(str);
}

function HttpSendRequest($data, action) {
    $.ajax({
        type: 'POST',
        url: 'controller',
        data: $data,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.Success == 'Expired') {
                location.reload();
            } else {
                var rec = unscaping(data);
                return action(rec);
            }
        },
    });
}

function CallAuthenticatedApi($url, $data, $headers, action) {
    var authToken = jQuery.getAthtkn();
    $.ajax({
        type: 'POST',
        url: $url,
        data: $data,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function (request) {
            request.setRequestHeader("Content-Type", "application/json");
            request.setRequestHeader("app-id", app_id);
            request.setRequestHeader("app-secret", app_secret);
            request.setRequestHeader("auth-token", authToken);
        },
        error: function (data) {
            if (data.status == 401) {
                jQuery.removeAuthtkn();
                window.location = siteUrl + "login.php";
            }
            var errObj = JSON.parse(data.responseText);
            callback_error(action, data);
        },
        success: function (data) {
            var res = action(data);
            return res;
        },
    });
}

$(document).ready(function () {
    jQuery("#logout").click(function () {
        jQuery.ajax({
            url: "controller",
            type: "post",
            data: "service=logout&opcode=load",
            success: function (data) {
                if (data == 1) {
                    window.location = "../index.php";
                }
            }
        });
    });
});
//
// function showjQloading() {
//
//     jQuery("#content").addClass('loader');
// }
//
// function hidejQloading() {
//     jQuery("#content").removeClass('loader');
// }


function showjQloading() {
    jQuery(".loader1").removeClass('hide');
    jQuery(".loader1").show();
}

function hidejQloading() {
    jQuery(".loader1").fadeOut("slow");
    jQuery(".loader1").addClass('hide');

}

function PrintElem(elem) {
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('<html><head><title>' + document.title + '</title>');

    mywindow.document.write('</head><body >');

    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}

$(document).ready(function () {
    $(".integer").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 45 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(".decimal").keypress(function (event) {
        var value = $(this).val();
        if (event.which == 45 || event.which > 58 || event.which == 47 || event.which == 42 || event.which == 43) {
            return false;
            event.preventDefault();
        } // prevent if not number/dot
        if (event.which == 46 && value.indexOf('.') != -1) {
            return false;
            event.preventDefault();
        } // prevent if already dot
        return true;
    });
});

function escapeHtml(text) {
    if (isNaN(text)) {
        return text.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"').replace(/&#039;/g, "'");
    } else {
        return text;
    }
}

unscaping = function (obj) {
    if ($.isArray(obj)) {
        var str = [];
    } else {
        var str = {};
    }
    var p;
    for (p in obj) {
        if (obj.hasOwnProperty(p)) {
            v = obj[p];
            if (v !== null && (typeof v === "object" || $.isArray(v))) {
                str[p] = unscaping(v);
            } else {
                str[p] = escapeHtml(v);
            }
        }
    }
    return str;
}

function urldecode(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}

function urlencode(str) {
    return encodeURIComponent(str);
}

function validemail(email) {
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        return 0;
    }
    else {
        return 1;
    }
}


function cleanstring(rec) {

    var data = rec.replace(/(\r\n|\n|\r)/gm, "\\n");
    return data;
}

Object.size = function (obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function loadModal(data, module) {
    var modal = parseInt(jQuery("#lid_modal").val());
    var modalid = modal + 1;
    jQuery("#lid_modal").val(modal + 1);
    jQuery("#modal_block").append('<div data-backdrop="static" data-keyboard="false" id="modal_container_' + (modal + 1) + '" class="modal fade" modal_id="' + (modal + 1) + '" modal_module="' + module + '"></div>');
    jQuery("#modal_container_" + (modal + 1)).html(data);
    jQuery("#modal_container_" + (modal + 1)).append('<input type="hidden" name="modalid" id="modalid" value="' + (modal + 1) + '"/>');
    jQuery("#modal_container_" + (modal + 1)).modal('show');
    jQuery("#modal_container_" + (modal + 1)).find(".close").attr("onclick", "closeModal('" + (modal + 1) + "')");
    //loadModalList(module, modalid);
}

function loadModalList(module, modalid) {

    var key = modalid + '-' + module;
    var list = sessionStorage.getItem('POSFrontListModal');
    if (list == null) {
        var arr = {};
        arr[modalid] = module;
    } else {
        var arr = jQuery.parseJSON(list);
        arr[modalid] = module;
    }
    var key = JSON.stringify(arr);
}

var checkopened = 0;
var checkopened1 = 0;

function closeModal(modalid) {
    checkopened = 0;
    checkopened1 = 0;
    var modal = parseInt(modalid);
    jQuery("#lid_modal").val(modal - 1);
    jQuery("#modal_container_" + (modal)).modal('hide');
    setTimeout(function () {
        jQuery("#modal_container_" + (modal)).remove()
    }, 500);

}

function validemail(email) {
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        return 0;
    }
    else {
        return 1;
    }
}

function load_report(rec) {
    HttpSendRequest(rec, function (data) {

        if (data.Success == "True") {
            jQuery("#content").hide();
            jQuery("#report_content").show();
            jQuery("#print_content").css({"width": data.Width, "min-height": data.Height});
            jQuery("#print_content").html(data.Data);
            var data= jQuery.parseJSON(rec);
            if(typeof data.type!='undefined' && data.type!=null && data.type!='') {
                jQuery('.export_csv').removeClass('hide');
                jQuery('.export_csv').attr('onclick', 'ExportCSV("' + data.type + '")');
            }
        }
    });
}

function download_csv(data,filename) {
    var csv = data.reportname;
    csv += "\n\n";

    if(typeof data.loc_name!='undefined' && data.loc_name!='' && data.loc_name!=null) {
        csv += data.loc_name;
    }
    if(data['loc_email']!= undefined && data['loc_email']!= '') {
        csv += ","+EMAIL+" : " + data['loc_email'];
    }
    if(data['loc_phone']!= undefined && data['loc_phone']!= '') {
        csv += ","+PHONE+" : " + data['loc_phone'];
    }
    csv += "\n";
    if (data['fromdate'] != undefined && data['fromdate'] != '') {
        csv += FROM_DATE + " : " + data['fromdate'];
    }
    if(data['todate']!= undefined && data['todate']!= '') {
        csv += ","+TO_DATE+" : " + data['todate'];
    }
    if(data['currency_symbol']!= undefined && data['currency_symbol']!= '') {
        csv += ","+CURRENCY_SYMBOL+" : " + data['currency_symbol'];
    }
    csv += "\n\n";
    if(data['key']!=undefined && data['key'] != 'null'){
        csv += data['key'].join(",");
    }
    csv += "\n";
    $.each(data['record'], function (index, value) {
        var array = $.map(value, function (val, ind) {
            return [val];
        });
        csv += array.join(',');
        csv += "\n";
    });
    var blob= new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    var hiddenElement = document.createElement('a');
    document.body.appendChild(hiddenElement);
    hiddenElement.setAttribute("type", "hidden");
    var url = URL.createObjectURL(blob);
    hiddenElement.setAttribute("href",url);
    hiddenElement.target = '_blank';
    hiddenElement.download = filename;
    hiddenElement.click();
}


function Exitinvoice() {
    jQuery("#content").show();
    jQuery("#report_content").hide();
    jQuery("#print_content").empty();
}

function cleanstring(rec) {
    var data = rec.replace(/(\r\n|\n|\r)/gm, "\\n");
    return data;
}

function toggleactive(id, module, value) {
    var arr = {};
    arr['service'] = 'common';
    arr['opcode'] = 'toggleststus';
    arr['module'] = module;
    arr['id'] = id;
    arr['value'] = value;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function (data) {
        if (data['Success'] == "True") {
            refreshlist();
            alertify.success(data['Message']);
        }
        else {
            alertify.error(data['Message']);
        }
    });
}

function togglezomato(id, module, value) {
    var arr = {};
    arr['service'] = 'charges';
    arr['opcode'] = 'togglezomato';
    arr['module'] = module;
    arr['id'] = id;
    arr['value'] = value;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function (data) {
        if (data['Success'] == "True") {
            refreshlist();
            alertify.success(data['Message']);
        }
        else {
            alertify.error(data['Message']);
        }
    });
}

function toggleactivebycompany(id, module, value) {
    var arr = {};
    arr['service'] = 'common';
    arr['opcode'] = 'toggleststusBycompany';
    arr['module'] = module;
    arr['id'] = id;
    arr['value'] = value;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function (data) {
        if (data['Success'] == "True") {
            refreshlist();
            alertify.success(data['Message']);
        }
        else {
            alertify.error(data['Message']);
        }
    });
}

function remove(id,module,msg,cancel,ok) {
    var labelsprint = {
        ok     : ok,
        cancel : cancel
    };
    alertify.confirm(msg,labelsprint, function (e) {
        if (e) {
            var arr = {};
            arr['service'] = 'common';
            arr['opcode'] = 'remove';
            arr['id'] = id;
            arr['module'] = module;
            var str = urlencode(JSON.stringify(arr));
            if (arr['id'] != '') {
                HttpSendRequest(str, function (data) {
                    if (data['Success'] == "True") {
                        refreshlist();
                        alertify.success(data['Message']);
                    }
                    else {
                        alertify.error(data['Message']);
                    }
                });
            }
        } else {
            return false;
        }
    });
}

function removebycompany(id, module,msg,cancel,ok) {
    var labelsprint = {
            ok     : ok,
            cancel : cancel
    };
    alertify.confirm(msg,labelsprint,function (e) {
        if (e) {
            var arr = {};
            arr['service'] = 'common';
            arr['opcode'] = 'removeByCompany';
            arr['id'] = id;
            arr['module'] = module;
            var str = urlencode(JSON.stringify(arr));
            if (arr['id'] != '') {
                pagereset=true;
                HttpSendRequest(str, function (data) {
                    if (data['Success'] == "True") {
                        refreshlist();
                        alertify.success(data['Message']);
                    }
                    else {
                        alertify.error(data['Message']);
                    }
                });
            }
        } else {
            return false;
        }
    });
}


function removetable(id, module, msg, cancel, ok) {

    var labelsprint = {

        ok: ok,
        cancel: cancel
    };
    alertify.confirm(msg,labelsprint, function (e) {


        if (e) {
            var arr = {};
            arr['service'] = 'common';
            arr['opcode'] = 'removetable';
            arr['id'] = id;
            arr['module'] = module;
            var str = urlencode(JSON.stringify(arr));
            if (arr['id'] != '') {
                HttpSendRequest(str, function (data) {
                    if (data['Success'] == "True") {
                        refreshlist();
                        alertify.success(data['Message']);
                    }
                    else {
                        alertify.error(data['Message']);
                    }
                });
            }
        } else {
            return false;
        }
    });
}

function validate_form(id) {
    var error = 0;
    $('.invalid').removeClass('invalid');
    $("#" + id).find(".notnull").each(function () {
        if ($(this).val() == "") {
            error = 1;
            $(this).addClass('invalid');
        }
    });
    $("#" + id).find(".dropdown").each(function () {
        if ($(this).val() == "" || $(this).val() == 0) {
            error = 1;
            $(this).addClass('invalid');
        }
    });
    $("#" + id).find(".integer").each(function () {
        if ($(this).val() != "") {
            var regex = /^[0-9]+$/;
            var input = $(this).val();
            if (!regex.test(input)) {
                error = 1;
                $(this).addClass('invalid');
            }
        }
    });
    $("#" + id).find(".decimal").each(function () {
        if ($(this).val() != "") {
            //var dec = /^[-+]?[0-9]+\.[0-9]+$/;
            var regex = /^\d+(\.\d{1,10})?$/;
            var input = $(this).val();
            if (!regex.test(input)) {
                error = 1;
                $(this).addClass('invalid');
            }
        }
    });
    $("#" + id).find(".email").each(function () {
        if ($(this).val() != "") {
            var check = validemail($(this).val());
            if (check == 0) {
                error = 1;
                $(this).addClass('invalid');
            }
        }
    });
    if (error == 1)
        return false;
    else
        return true;
}

function escapeHtml(text) {
    if (isNaN(text)) {
        return text.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"').replace(/&#039;/g, "'").replace(/&euro;/g, "€");
    } else {
        return text;
    }
}

unscaping = function (obj) {
    if ($.isArray(obj)) {
        var str = [];
    } else {
        var str = {};
    }
    var p;
    for (p in obj) {
        if (obj.hasOwnProperty(p)) {
            v = obj[p];
            if (v !== null && (typeof v === "object" || $.isArray(v))) {
                str[p] = unscaping(v);
            } else {
                str[p] = escapeHtml(v);
            }
        }
    }
    return str;
}

function setChainToLocal(chain_list) {
    var data_enc = $('<textarea />').html(chain_list).text();
    localStorage.setItem('POSConfigChain', data_enc);
}

function switch_outlet(id, type) {
    var arr = {};
    arr['service'] = "common";
    arr['opcode'] = "switch_outlet";
    arr['oid'] = id;
    arr['type'] = type;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function (data) {
        if (data['Success'] == 'Expired') {
            location.reload();
        }
        if (data['Success'] == "True") {
            window.location = "dashboard";
        }
        else {
            alertify.error(data['Message']);
            //var hid = localStorage.getItem('FRONT_HID');
            //jQuery("#hotel_chain_switch").val(hid);
        }
    });
}

function check_store_indent(id, type,module) {
   var arr = {};
    arr['service'] = 'common';
    arr['opcode'] = 'check_store_indent';
    arr['id'] = id;
    arr['type'] = type;
    arr['module'] = module;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function (data) {
        jQuery("#modalPrimary").html(data.Data);
        jQuery("#modalPrimary").modal('show');
    });

}

/*-- Loader --*/
$(document).ready(function () {
    // Animate loader off screen
    $(".loader1").fadeOut("slow");
});