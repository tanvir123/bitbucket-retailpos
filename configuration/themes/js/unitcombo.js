
//Unit
function loaditem_unitlist(bind_unit) {

    jQuery("#unit_div").removeClass('hide');
    var unit_detail_str='';
    var serve_unit='';
    jQuery('#serve_unit').html('');
    serve_unit +='<option value="0" selected="selected">'+LABEL_PLEASE_SELECT+'</option>';

            jQuery('#unit_sel').select2();

            for (var i=0;i<bind_unit.length;i++) {

                var unit_hash = bind_unit[i]['unithash'];

                if (unit_detail_str=='')
                    unit_detail_str += unit_hash;
                else
                    unit_detail_str +=","+unit_hash;
                jQuery('#item_sel_' + unit_hash).select2();
                var card = '<div id="unit_' + unit_hash + '" class="col-sm-12" style="margin-bottom: 15px;"><div class="card">';
                card += '<div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-unit_' + unit_hash + '" href="#collapse-unit_' + unit_hash + '" style="cursor: pointer">';
                card +='<div class="card-title"> <span id="frmtitle_' + unit_hash + '" class="title">' + bind_unit[i]['name'] + '</span>';
                card += '</div></div><div class="card-body panel-collapse out collapse" id="collapse-unit_' + unit_hash + '">';
                card += '<div class="form-group col-md-2">';
                card += '<label>'+RATE_1+'</label>';
                card += '<input id="rate1_' + unit_hash + '" name="rate1_' + unit_hash + '" class="form-control" type="text" value=" '+ bind_unit[i]['rate1'] +'" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_1+'" maxlength="20" required>';
                card += '</div>';
                card += '<div class="form-group col-md-2">';
                card += '<label>'+RATE_2+'</label>';
                card += '<input id="rate2_' + unit_hash + '" name="rate2_' + unit_hash + '" class="form-control" type="text" value=" '+ bind_unit[i]['rate2'] +'" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_2+'" maxlength="20" required>';
                card += '</div>';
                card += '<div class="form-group col-md-2">';
                card += '<label>'+RATE_3+'</label>';
                card += '<input id="rate3_' + unit_hash + '" name="rate3_' + unit_hash + '" class="form-control" type="text" value=" '+ bind_unit[i]['rate3'] +'" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_3+'" maxlength="20" required>';
                card += '</div>';
                card += '<div class="form-group col-md-2">';
                card += '<label>'+RATE_4+'</label>';
                card += '<input id="rate4_' + unit_hash + '" name="rate4_' + unit_hash + '" class="form-control" type="text" value="'+ bind_unit[i]['rate4'] +'" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_4+'" maxlength="20" required>';
                card += '</div>';
                card += '<div class="form-group col-md-3">';
                card += '<label>'+DEFAULT_RATE+'</label>';
                card += '<select name="defaultrate_' + unit_hash + '" class="form-control" id="defaultrate_' + unit_hash + '">';
                card += '<option class="optrate_' + unit_hash + '" value="1" >'+RATE_1+'</option>';
                card += '<option class="optrate_' + unit_hash + '" value="2" >'+RATE_2+'</option>';
                card += '<option class="optrate_' + unit_hash + '" value="3" >'+RATE_3+'</option>';
                card += '<option class="optrate_' + unit_hash + '" value="4" >'+RATE_4+'</option>';
                card += '</select></div><div class="clearfix"></div>';

                card += '<div class="tab-content">';
                card += '<div class="card" style="border-radius: 10px !important;">';
                // card += '<span class="title" style="display: block;font-weight: bold;font-size: 16px;color: #091a59;text-align:center;margin-top: -8px;">'+ADDITEMS+'</span>';

                card += '<div style="text-align:right"><a class="btn btn-primary" txt="' + unit_hash + '"onclick="AddMoreItems(this)" style="border-radius: 10px !important;">'+ADDITEMS+'</a></div>';
                card += '<input type="hidden" name="itemgroups_' + unit_hash + '" id="itemgroups_' + unit_hash + '" value="0">';

                card += ' <div id="itemGroup_' + unit_hash + '" ></div>';

                card += '</div></div></div></div>';

                // card += ' <li class="active"><a data-toggle="tab" href="#item_' + unit_hash + '">'+ITEM+'</a></li>';
                //
                // card += ' </ul>';
                // card += '<div class="tab-content">';
                //
                // card +='<div id="item_' + unit_hash + '" class="tab-pane fade in active">';
                // card +='<div class="card">';
                // card +='<div class="card-header"><div class="card-title"><span class="title">'+ITEM+'</span></div></div>';
                // card+='<div class="card-body">';
                //

                // card += '<div class="form-group clearfix"><label class="col-sm-2 textalignleft">'+ITEM+'</label>';
                // card += '<div class="col-md-10" id="select2div"><select id="item_sel_' + unit_hash + '" class="rawmaterial_sel_' + unit_hash + '" multiple="multiple" style="width:100%">';
                // jQuery.each(itemdetailobj, function(key, value) {
                //     if(key != ''){
                //         card += '<option class="option_raw_' + unit_hash + '_' + key + '" value="' + key + '">' + value['itemname'] + '</option>';
                //     }
                // });
                // card += '</select>';
                // card += '<input type="hidden" class="form-control" id="item_detail_'+unit_hash+'" class="raw_detail" name="item_detail_'+unit_hash+'" />';
                // card += '<input type="hidden" class="form-control" id="old_item_'+unit_hash+'" name="old_item_'+unit_hash+'" /></div>';
                // card +='<div class="form-group clearfix"></div><label class="col-sm-3 control-label textalignleft "></label>';
                // card +=' <div class="col-sm-9">';
                // card +='<button type="button" class="button btn" unit="'+ unit_hash +'" style="margin-right: 10px" onclick="selectAllItem(this);">'+SELECT_ALL+'</button>';
                // card +='<button style="margin-right: 10px" unit="'+ unit_hash +'" type="button" class="button btn" onclick="selectNoneItem(this);">'+SELECT_NONE+'</button>';
                // card +='<button  style="float: right" unit="'+ unit_hash +'"  type="button" class="btn btn-primary" onclick="loadItemUnit(this);">'+LOAD+'</button>';
                // card += '</div><div class="form-group clearfix"></div>';
                // card += '<div id="itemdetail_list_'+unit_hash+'"></div>';
                // card += '</div></div></div></div>';
                // card +='</div></div></div></div>';


                jQuery(".option_unit_"+unit_hash).prop('selected',true);
                jQuery('#unit_list').append(card);
                jQuery("#defaultrate_"+unit_hash).val(bind_unit[i]['default_rate']);
                serve_unit += '<option value="' + unit_hash + '">' + bind_unit[i]['name'] + '</option>';

                loadItemGroups(unit_hash,bind_unit[i]['bind_itemgroups']);

            }
       jQuery('#serve_unit').append(serve_unit);
       jQuery('#unit_sel').change();
       jQuery('#unit_detail').val(unit_detail_str);
       jQuery('#old_unit').val(unit_detail_str);
}

function loadItemGroups(unit_hash1,bind_itemgroup)
{
    var unit_detail_str=0;

    if(typeof bind_itemgroup != 'undefined' && bind_itemgroup.length >0)
    {
        for(var i=0;i<bind_itemgroup.length;i++)
        {
            var newunit = bind_itemgroup[i]['itemgroupid'];

            var unit_hash = unit_hash1+'_'+newunit;

            if (unit_detail_str == 0)
                unit_detail_str = newunit;
            else
                unit_detail_str += "," + newunit;


            var card = '<div class="tab-content" id="item_' + unit_hash + '">';
            card +='<div  class="tab-pane fade in active">';
            card +='<div class="card">';
            card +='<div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-unit_' + unit_hash + '" href="#collapse-unit_' + unit_hash + '" style="cursor: pointer"><div class="card-title"><span class="title">'+ITEM+' X '+GROUP+' '+newunit+'</span></div><a unithashkey="' + unit_hash1 + '" unit="'+newunit+'" onclick="closeGroupitem(this)" style="float:right;cursor: pointer;font-size: 15px;" class="card-title"><i class="fa fa-close"></i></a></div>';
            card+='<div class="card-body panel-collapse collapse out in" id="collapse-unit_' + unit_hash + '">';
            card += '<div class="form-group clearfix"><label class="col-sm-2 textalignleft">'+ITEM+'</label>';
            card += '<div class="col-md-10" id="select2div"><select id="item_sel_' + unit_hash + '" class="rawmaterial_sel_' + unit_hash + '" multiple="multiple" style="width:100%">';
            jQuery.each(itemdetailobj, function(key, value) {
                if(key != ''){
                    card += '<option class="option_raw_' + unit_hash + '_' + key + '" value="' + key + '">' + value['itemname'] + '</option>';
                }
            });
            card += '</select>';
            card += '<input type="hidden" class="form-control" id="item_detail_'+unit_hash+'" class="raw_detail" name="item_detail_'+unit_hash+'" />';
            card += '<input type="hidden" class="form-control" id="old_item_'+unit_hash+'" name="old_item_'+unit_hash+'" /></div>';
            card +='<div class="form-group clearfix"></div><label class="col-sm-3 control-label textalignleft "></label>';
            card +=' <div class="col-sm-9">';
            card +='<button type="button" class="button btn" unit="'+ unit_hash +'" style="margin-right: 10px" onclick="selectAllItem(this);">'+SELECT_ALL+'</button>';
            card +='<button style="margin-right: 10px" unit="'+ unit_hash +'" type="button" class="button btn" onclick="selectNoneItem(this);">'+SELECT_NONE+'</button>';
            card +='<button  style="float: right" unit="'+ unit_hash +'"  type="button" class="btn btn-primary" onclick="loadItemUnit(this);">'+LOAD+'</button>';
            card += '</div><div class="form-group clearfix"></div>';
            card += '<div id="itemdetail_list_'+unit_hash+'"></div>';
            card += '</div></div></div></div>';

            jQuery('#itemGroup_'+unit_hash1).append(card);

            jQuery('#item_sel_' + unit_hash).select2();

            loaditemslist(unit_hash,bind_itemgroup[i]['bind_items'],bind_itemgroup[i]['defaultItem']);

        }
        jQuery('#itemgroups_'+unit_hash1).val(unit_detail_str);
    }

   // jQuery('#itemgroups_'+unit_hash1).val(unit_detail_str);
}

function loaditemslist(unit,bind_item,defaultItem)
{
    var unit_detail_str='';
    jQuery('#item_sel_'+unit).select2();

    if(typeof bind_item != 'undefined' && bind_item !='' && bind_item !=null)
    {
        $.each(bind_item, function (key,i) {

            //  for (var i = 0; i < bind_item.length; i++) {               // this will not work here

            var unit_hash = i.itemhash;
            var extracharge  = i.extracharge;

            if (unit_detail_str == '')
                unit_detail_str += unit_hash;
            else
                unit_detail_str += "," + unit_hash;

            var item_txt = jQuery('#item_sel_'+unit+' option[value="'+unit_hash+'"]').html(); // my method to get name

            //var item_txt = jQuery('#item_sel_' + unit + " option:selected").text(); // this will not work here


            var card ='<span class="title orSpan" style="text-align: center;" id="orSpan_'+unit+'_'+unit_hash+'"></span><div class="card" id="itemunitdiv_'+unit+'_'+unit_hash+'">';

            card +='<div class="card-header"><div class="card-title col-md-12 col-sm-12">';
            var checked = '';
            var displaynone = '';
            if(unit_hash == defaultItem)
            {
                checked = 'checked';
                displaynone = 'display:none;';
            }

            card +=  '<div style="float: left;" class="col-md-6 col-sm-6"><div class="radio3 radio-check radio-inline"><input type="radio" name="defaultItem_'+unit+'" id="defaultItem_'+unit+'_'+unit_hash+'" value="'+unit_hash+'"  '+checked+' onchange="hideExtracharge(this)" unit="'+unit+'" unit-hash="'+unit_hash+'">';

            card += '<label class="title iradio-label form-label" for="defaultItem_'+unit+'_'+unit_hash+'">'+ item_txt +'</label></div></div><div style="float: right;'+displaynone+'" class="col-md-6 col-sm-6 div_extraCharge_'+unit+'" id="div_extraCharge_'+unit+'_'+unit_hash+'" ><div class="col-md-4 col-sm-4"><span class="title">'+EXTRACGARGE+'</span></div><div class="col-md-8 col-sm-6"><input type="text" name="extraCharge_'+unit+'_'+unit_hash+'" id="extraCharge_'+unit+'_'+unit_hash+'" class="form-control" onkeypress="return isFloat(event,this.value)" maxlength="20" value="'+extracharge+'"></div></div></div></div>';

            card +='<div class="card-body panel-collapse collapse out in" id="collapse-unit_' + unit_hash + '">';
            card += '<div class="form-group clearfix"><label class="col-sm-2 col-md-2 textalignleft">'+Item_Unit+'</label>';
            card += '<div class="col-md-4 col-sm-4"><select id="itemunit_sel_'+unit+'_'+ unit_hash + '" class="form-control">';
            card += '<option selected="selected" value="0">'+SELECT_UNIT+'</option>';
            jQuery.each(itemunitdetailobj[unit_hash], function(key, value) {
                if(key != ''){
                    card += '<option class="option_'+unit+'_'+unit_hash+'_'+value['hashkey']+'" value="' + value['hashkey'] + '">' + value['itemunit'] + '</option>';
                }
            });
            card += '</select>';
            card += '<input type="hidden" class="form-control" id="oldunit_item_'+unit+'_'+unit_hash+'" name="oldunit_item_'+unit+'_'+unit_hash+'" /></div>';

            card +=' <div class="col-sm-6 col-md-6">';
            card +='<button  style="float: left" unit="'+unit+'_'+ unit_hash +'"  type="button" class="btn btn-primary" itemid="'+unit_hash+'" onclick="loadItemDetails(this);" >'+ADD+'</button>';
            card += '</div></div><div class="form-group clearfix"></div>';
            card += '<div id="modifier_list_'+unit+'_'+unit_hash+'"></div>';
            card += '</div></div></div></div>';
            card +='</div></div></div></div>';

            jQuery('#itemdetail_list_'+unit).append(card);
            jQuery(".option_raw_"+unit+'_'+unit_hash).prop('selected',true);

            var unisdetails = i.itemunitsdetails;

            for(var t=0; t<unisdetails.length; t++)
            {
                var bind_modifier = '';

                if(unisdetails[t]['is_include'] == 1)
                {
                    bind_modifier = unisdetails[t]['bind_modifier'];
                }

                loaditem_modlist(unit+'_'+unit_hash+'_'+unisdetails[t]['itemunithash'],bind_modifier,unit+'_'+unit_hash,unisdetails[t]['itemunithash'],unisdetails[t]['quantity'],11);
            }
            //}
        });
        jQuery('#item_sel_'+unit).change();
        jQuery('#item_detail_'+unit).val(unit_detail_str);
        jQuery('#old_item_'+unit).val(unit_detail_str);
    }


}

function getUnit()
{
    jQuery('#unit_sel').select2();
    var req={};
    req['service'] = 'menu_item';
    req['opcode'] = 'getAllItemunit';
    var str = JSON.stringify(req);
    HttpSendRequest(str,function(data) {
        if (data['Success'] == 'True') {
            var rec = data['Data'];
            var arr = {};
            for(var i=0; i<rec.length; i++)
            {
                var hashkey = rec[i]['hashkey'];
                var name = rec[i]['name'];
                arr[hashkey] = {};
                arr[hashkey]['name'] = name;
            }
            unitdetailobj=arr;
        }
    });
}

function selectAllUnit()
{
    jQuery('#unit_sel').find('option').each(function(){
        jQuery(this).prop('selected',true);
    });
    jQuery('#unit_sel').change();
    return false;
}

function selectNoneUnit()
{
    jQuery('#unit_sel').find('option').prop('selected',false);
    jQuery('#unit_sel').change();
    return false;
}

//Add
function loadUnitDetail()
{
    var unitDetail = jQuery('#unit_sel').val();
    var old_unit = jQuery('#old_unit').val();

    var oldunitDetail=old_unit.split(',');
    var difference = $(unitDetail).not(oldunitDetail).get();
    var unit_detail_str='';
    var unitobj_sub =unitdetailobj;
    var serve_unit='';

    //Delete
    var newDiff=$(oldunitDetail).not(unitDetail).get();
    if(newDiff!=null){
        for (var i=0;i<newDiff.length;i++) {
            var hashkey = newDiff[i];
            jQuery("#unit_"+hashkey).remove();
            jQuery('#serve_unit option[value="'+hashkey+'"]').remove();            // remaining  its for serve unit
        }
    }
    if (difference!=null || typeof difference != 'undefined') {
                for (var i = 0; i < difference.length; i++) {
                    var unit_hash = difference[i];
                    var name = unitobj_sub[unit_hash]['name'];

                    if (unit_detail_str == '')
                        unit_detail_str += unit_hash;
                    else
                        unit_detail_str += "," + unit_hash;

                    var card = '<div id="unit_' + unit_hash + '" class="col-sm-12" style="margin-bottom: 15px;"><div class="card">';
                    card += '<div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-unit_' + unit_hash + '" href="#collapse-unit_' + unit_hash + '" style="cursor: pointer">';
                    card +='<div class="card-title"><span id="frmtitle_' + unit_hash + '" class="title">' + unitobj_sub[unit_hash]['name'] + '</span>';
                    card += '</div></div><div class="card-body panel-collapse collapse out in" id="collapse-unit_' + unit_hash + '">';
                    card += '<div class="form-group col-md-2">';
                    card += '<label>'+RATE_1+'</label>';
                    card += '<input id="rate1_' + unit_hash + '" name="rate1_' + unit_hash + '" class="form-control" type="text" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_1+'" maxlength="20" required>';
                    card += '</div>';
                    card += '<div class="form-group col-md-2">';
                    card += '<label>'+RATE_2+'</label>';
                    card += '<input id="rate2_' + unit_hash + '" name="rate2_' + unit_hash + '" class="form-control" type="text" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_2+'" maxlength="20" required>';
                    card += '</div>';
                    card += '<div class="form-group col-md-2">';
                    card += '<label>'+RATE_3+'</label>';
                    card += '<input id="rate3_' + unit_hash + '" name="rate3_' + unit_hash + '" class="form-control" type="text" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_3+'" maxlength="20" required>';
                    card += '</div>';
                    card += '<div class="form-group col-md-2">';
                    card += '<label>'+RATE_4+'</label>';
                    card += '<input id="rate4_' + unit_hash + '" name="rate4_' + unit_hash + '" class="form-control" type="text" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_4+'" maxlength="20" required>';
                    card += '</div>';
                    card += '<div class="form-group col-md-3">';
                    card += '<label>'+DEFAULT_RATE+'</label>';
                    card += '<select name="defaultrate_' + unit_hash + '" class="form-control" id="defaultrate_' + unit_hash + '">';
                    card += '<option class="optrate_' + unit_hash + '" selected="selected" value="1" >'+RATE_1+'</option>';
                    card += '<option class="optrate_' + unit_hash + '" value="2" >'+RATE_2+'</option>';
                    card += '<option class="optrate_' + unit_hash + '" value="3" >'+RATE_3+'</option>';
                    card += '<option class="optrate_' + unit_hash + '" value="4" >'+RATE_4+'</option>';
                    card += '</select></div><div class="clearfix"></div>';

                    card += '<div class="tab-content">';
                    card += '<div class="card" style="border-radius: 10px !important;">';
                    // card += '<span class="title" style="display: block;font-weight: bold;font-size: 16px;color: #091a59;text-align:center;margin-top: -8px;" >'+ADDITEMS+'</span>';

                    // card += '<ul class="nav">';
                    // card += ' <li class="active"><a data-toggle="tab" href="#item_' + unit_hash + '">'+ADDITEMS+'</a></li>';
                    // card += ' <li class="active" style="float:right"><a class="btn btn-primary" data-toggle="tab" href="javascript:void(0)" txt="' + unit_hash + '"onclick="AddMoreItems(this)">'+ADDMORE+'</a></li>';
                    card += ' <div style="text-align:right;"><a class="btn btn-primary" txt="' + unit_hash + '"onclick="AddMoreItems(this)" style="border-radius: 10px !important;">'+ADDITEMS+'</a></div>';
                    card += '<input type="hidden" name="itemgroups_' + unit_hash + '" id="itemgroups_' + unit_hash + '" value="0">';
                    // card += ' </ul>';
                    card += ' <div id="itemGroup_' + unit_hash + '" ></div>';

                    card += '</div></div></div></div>';
                    jQuery('#unit_list').append(card);
                    serve_unit += '<option value="' + unit_hash + '">' + name + '</option>';
                }

        jQuery('#old_unit').val(unitDetail);
        jQuery('#unit_detail').val(unitDetail);
        jQuery('#serve_unit').append(serve_unit);
    }
    else {
        jQuery('#unit_detail').val('');
    }

}

function AddMoreItems(current)
{
    var unit_hash1 = jQuery(current).attr('txt');

    var unitDetail = jQuery('#itemgroups_'+unit_hash1).val();

    var oldunitDetail = unitDetail.split(',');

    var oldunit = oldunitDetail[oldunitDetail.length-1];

    var newunit = parseInt(oldunit)+1;

    var unit_hash = unit_hash1+'_'+newunit;

    var card = '<div class="tab-content" id="item_' + unit_hash + '">';
    card +='<div  class="tab-pane fade in active">';
    card +='<div class="card">';
    card +='<div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-unit_' + unit_hash + '" href="#collapse-unit_' + unit_hash + '" style="cursor: pointer"><div class="card-title"><span class="title">'+ITEM+' X '+GROUP+' '+newunit+'</span></div><a unithashkey="' + unit_hash1 + '" unit="'+newunit+'" onclick="closeGroupitem(this)" style="float:right;cursor: pointer;font-size: 15px;" class="card-title"><i class="fa fa-close"></i></a></div>';
    card+='<div class="card-body panel-collapse collapse out in" id="collapse-unit_' + unit_hash + '">';
    card += '<div class="form-group clearfix"><label class="col-sm-2 textalignleft">'+ITEM+'</label>';
    card += '<div class="col-md-10" id="select2div"><select id="item_sel_' + unit_hash + '" class="rawmaterial_sel_' + unit_hash + '" multiple="multiple" style="width:100%">';
    jQuery.each(itemdetailobj, function(key, value) {
        if(key != ''){
            card += '<option class="option_raw_' + unit_hash + '_' + key + '" value="' + key + '">' + value['itemname'] + '</option>';
        }
    });
    card += '</select>';
    card += '<input type="hidden" class="form-control" id="item_detail_'+unit_hash+'" class="raw_detail" name="item_detail_'+unit_hash+'" />';
    card += '<input type="hidden" class="form-control" id="old_item_'+unit_hash+'" name="old_item_'+unit_hash+'" /></div>';
    card +='<div class="form-group clearfix"></div><label class="col-sm-3 control-label textalignleft "></label>';
    card +=' <div class="col-sm-9">';
    card +='<button type="button" class="button btn" unit="'+ unit_hash +'" style="margin-right: 10px" onclick="selectAllItem(this);">'+SELECT_ALL+'</button>';
    card +='<button style="margin-right: 10px" unit="'+ unit_hash +'" type="button" class="button btn" onclick="selectNoneItem(this);">'+SELECT_NONE+'</button>';
    card +='<button  style="float: right" unit="'+ unit_hash +'"  type="button" class="btn btn-primary" onclick="loadItemUnit(this);">'+LOAD+'</button>';
    card += '</div><div class="form-group clearfix"></div>';
    card += '<div id="itemdetail_list_'+unit_hash+'"></div>';
    card += '</div></div></div></div>';

    jQuery('#itemGroup_'+unit_hash1).append(card);
    jQuery('#item_sel_' + unit_hash).select2();
    if(oldunit == 0 || oldunit == '0')
    {
       jQuery('#itemgroups_'+unit_hash1).val(newunit);

    }else
    {
        unitDetail = unitDetail + ',' + newunit;
        jQuery('#itemgroups_'+unit_hash1).val(unitDetail);
    }

}

function validate_unit(unit_str) {
    var flag = 1;

    if(typeof unit_str != 'undefined' && unit_str != null && unit_str != '') {
        var unit_arr = unit_str.split(',');
        if(unit_arr.length > 0)
        {
            for (var i = 0; i < unit_arr.length; i++) {
                var unit = unit_arr[i];

                var sel_rate = jQuery("#defaultrate_" + unit + " :selected").val();

                if ((jQuery("#rate" + sel_rate + "_" + unit).val() == "") || (jQuery("#rate" + sel_rate + "_" + unit).val() == 0)) {
                    jQuery("#rate" + sel_rate + "_" + unit).addClass('invalid');
                    flag = 0;
                }

                // ITEM GROUP

                var item_group_str = jQuery("#itemgroups_" + unit).val();

                var item_group = item_group_str.split(',');

                if (typeof item_group != 'undefined' && item_group != null && item_group != '') {
                    if(item_group.length > 0) {
                        for (var f = 0; f < item_group.length; f++) {
                            var grp = item_group[f];

                            //ITEM
                            var item_arr = jQuery("#item_sel_" + unit + "_" + grp).val();


                            if (typeof item_arr != 'undefined' && item_arr != null && item_arr != '' ) {
                                if(item_arr.length > 0)
                                {
                                    var defaultItem = jQuery('input[name=defaultItem_'+ unit + '_'+ grp + ']:checked').val();

                                    if(defaultItem == "")
                                    {
                                        alertify.alert(DEFAULTITEM,LABEL_OK);
                                        flag = 0;
                                    }

                                    for (var k = 0; k < item_arr.length; k++) {
                                        var mdf = item_arr[k];

                                        //ITEM UNIT
                                        var itemunit_str = jQuery("#oldunit_item_" + unit + "_" + grp + "_" + mdf).val();


                                        if (typeof itemunit_str != 'undefined' && itemunit_str != null && itemunit_str != '' ) {

                                            var itemunit_arr = itemunit_str.split(',');

                                            if (itemunit_arr.length > 0) {
                                                for (var w = 0; w < itemunit_arr.length; w++) {
                                                    var iun = itemunit_arr[w];

                                                    if (parseInt(jQuery("#itemqty_" + unit + "_" + grp + "_" + mdf + "_" + iun).val()) == 0) {
                                                        alertify.alert(QTYNOTZERO,LABEL_OK);
                                                    }
                                                    if (jQuery("#itemqty_" + unit + "_" + grp + "_" + mdf + "_" + iun).val() == "" || jQuery("#itemqty_" + unit + "_" + grp + "_" + mdf + "_" + iun).val() == 0) {
                                                        jQuery("#itemqty_" + unit + "_" + grp + "_" + mdf + "_" + iun).addClass('invalid');
                                                        flag = 0;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            alertify.alert(SELECT_ITEMUNIT,LABEL_OK);
                                            jQuery("#itemunit_sel_" + unit + "_" + grp + "_" + mdf).addClass('invalid');
                                            flag = 0;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                alertify.alert(SELECT_ITEM,LABEL_OK);
                                flag = 0;
                            }


                        }
                    }
                }

            }
        }
    }
    return flag;
}

function getItemlist()
{
    var req={};
    req['service'] = 'combo_product';
    req['opcode'] = 'getItemlist';
    var str = JSON.stringify(req);
    HttpSendRequest(str,function(data) {
        if (data['Success'] == 'True') {
            var item = data['Data'].item;

            var arr = {};
            var arr2 = {};

            for(var i=0; i<item.length; i++)
            {
                var hashkey = item[i]['hashkey'];
                var name = item[i]['itemname'];
                arr[hashkey] = {};
                arr[hashkey]['itemname'] = name;
            }
            itemdetailobj=arr;

            var itemunit = data['Data'].itemunit;

            for(var j=0; j<itemunit.length; j++)
            {
                var name = itemunit[j]['itemunit'].split(",");
                var hashkey = itemunit[j]['hashkey'].split(",");
                var unitid = itemunit[j]['unitunkid'].split(",");
                var id = itemunit[j]['itemunkid'];

                arr2[id] = {};
                for(var k=0; k<unitid.length; k++)
                {
                    arr2[id][k] = {};

                    arr2[id][k]['itemunit'] = name[k];
                    arr2[id][k]['hashkey'] = hashkey[k];
                    arr2[id][k]['unitunkid'] = unitid[k];
                }
            }
            itemunitdetailobj=arr2;
        }

    });
}

function selectAllItem(id)
{
    var unit = jQuery(id).attr('unit');
    jQuery('#item_sel_'+unit).find('option').each(function(){
        jQuery(this).prop('selected',true);
    });
    jQuery('#item_sel_'+unit).change();
    return false;
}

function selectNoneItem(id)
{
    var unit = jQuery(id).attr('unit');
    jQuery('#item_sel_'+unit).find('option').prop( 'selected',false );
    jQuery('#item_sel_'+unit).change();
    return false;
}

function loadItemUnit(current)
{
    var unit = jQuery(current).attr('unit');

    var unitDetail = jQuery('#item_sel_'+unit).val();
    var old_unit = jQuery('#old_item_'+unit).val();
    var oldunitDetail=old_unit.split(',');

    var difference = $(unitDetail).not(oldunitDetail).get();
    var unit_detail_str='';

    //Delete
    var newDiff=$(oldunitDetail).not(unitDetail).get();
    if(newDiff!=null){
        for (var i=0;i<newDiff.length;i++) {
            var hashkey = newDiff[i];
            jQuery("#itemunitdiv_"+unit+"_"+hashkey).remove();
            jQuery("#orSpan_"+unit+"_"+hashkey).remove();

        }
    }
     var OR = 'OR';
    //
    // if(old_unit != '')
    // {
    //    OR = 'OR';
    // }

    if (difference!=null || typeof difference != 'undefined') {
        for (var i = 0; i < difference.length; i++) {
            var unit_hash = difference[i];

            if (unit_detail_str == '')
                unit_detail_str += unit_hash;
            else
                unit_detail_str += "," + unit_hash;

            // if(unit_hash != unitDetail[0])
            // {
            //     OR = 'OR';
            // }

            var item_txt = jQuery('#item_sel_'+unit+' option[value="'+unit_hash+'"]').html(); // my method to get name
            //var item_txt = jQuery('#item_sel_' + unit + " option:selected").text(); // this will not work here

            var card ='<div class="card" id="itemunitdiv_'+unit+'_'+unit_hash+'">';
            card +='<div class="card-header"><div class="card-title col-md-12 col-sm-12" >';

            card +=  '<div style="float: left;" class="col-md-6 col-sm-6"><div class="radio3 radio-check radio-inline"><input type="radio" name="defaultItem_'+unit+'" id="defaultItem_'+unit+'_'+unit_hash+'" value="'+unit_hash+'" class="radio3 radio-check radio-inline" onclick="hideExtracharge(this)" unit="'+unit+'" unit-hash="'+unit_hash+'" >';

            card += '<label class="title iradio-label form-label" for="defaultItem_'+unit+'_'+unit_hash+'">'+ item_txt +'</label></div></div><div style="float: right" class="col-md-6 col-sm-6 div_extraCharge_'+unit+'" id="div_extraCharge_'+unit+'_'+unit_hash+'" ><div class="col-md-4 col-sm-4"><span class="title">'+EXTRACGARGE+'</span></div><div class="col-md-8 col-sm-8"><input type="text" name="extraCharge_'+unit+'_'+unit_hash+'" id="extraCharge_'+unit+'_'+unit_hash+'" class="form-control" onkeypress="return isFloat(event,this.value)"  maxlength="20"></div></div></div></div>';
            card +='<div class="card-body">';
            card += '<div class="form-group clearfix"><label class="col-sm-2 col-md-2 textalignleft">'+Item_Unit+'</label>';
            card += '<div class="col-md-4 col-sm-4"><select id="itemunit_sel_'+unit+'_'+ unit_hash + '" class="form-control">';
            card += '<option selected="selected" value="0">'+SELECT_UNIT+'</option>';
            jQuery.each(itemunitdetailobj[unit_hash], function(key, value) {
                if(key != ''){
                    card += '<option class="option_'+unit+'_'+unit_hash+'_'+value['hashkey']+'" value="' + value['hashkey'] + '">' + value['itemunit'] + '</option>';
                }
            });
            card += '</select>';
            card += '<input type="hidden" class="form-control" id="oldunit_item_'+unit+'_'+unit_hash+'" name="oldunit_item_'+unit+'_'+unit_hash+'" /></div>';
            card +=' <div class="col-sm-6 col-md-6">';
            card +='<button  style="float: left" unit="'+unit+'_'+ unit_hash +'"  type="button" class="btn btn-primary" itemid="'+unit_hash+'" onclick="loadItemDetails(this);" >'+ADD+'</button>';
            card += '</div></div><div class="form-group clearfix"></div>';
            card += '<div id="modifier_list_'+unit+'_'+unit_hash+'"></div>';
            card += '</div></div></div></div>';
            card +='</div></div></div></div><span class="title orSpan" style="display: block;font-weight: bold;font-size: 16px;color: #091a59;text-align:center;margin-top: -8px;" id="orSpan_'+unit+'_'+unit_hash+'">'+OR+'</span>';

            jQuery('#itemdetail_list_'+unit).append(card);
            jQuery('.div_extraCharge_'+unit).show();
            jQuery('#div_extraCharge_'+unit+'_'+unit_hash).hide();
        }
        jQuery('#item_detail_'+unit).val(unitDetail);
        jQuery('#old_item_'+unit).val(unitDetail);
        jQuery('input:radio[name=defaultItem_'+unit+']').prop('checked', true);

    }
    else {
        jQuery('#item_detail_'+unit).val('');
    }
    // if(unitDetail.length > 0)
    // {
    //     jQuery("#orSpan_"+unit+"_"+unitDetail[0]).remove();
    // }

// if(typeof unitDetail != 'undefined' && unitDetail!=null && unitDetail.length > 0)
//    {
//        for(var i=0;i<unitDetail.length;i++)
//        {
//            jQuery("#groupCount_"+unit+"_"+unitDetail[i]).html(i);
//        }
//    }
}

function hideExtracharge(current) {

    var id= jQuery(current).attr('unit');
    var itemid= jQuery(current).attr('unit-hash');
    jQuery('.div_extraCharge_'+id).show();
    jQuery('#div_extraCharge_'+id+'_'+itemid).hide();

}

function loadItemDetails(current)
{
    var unit = jQuery(current).attr('unit');
    var itemid = jQuery(current).attr('itemid');

    var unitDetail = jQuery('#itemunit_sel_'+unit).val();
    if (unitDetail == "0") {
        return false;
    }
                /* selecting unit wise item modifier */

    var req={};
    req['service'] = 'combo_product';
    req['opcode'] = 'getItemModifier';
    req['itemid'] = itemid;
    req['itemunitkey'] = unitDetail;
    var str = JSON.stringify(req);
    HttpSendRequest(str,function(data) {
        if (data['Success'] == 'True') {
            var rec = data['Data'];

            loaditem_modlist(unit+'_'+unitDetail,rec[0]['bind_modifier'],unit,unitDetail,1,'');

        }
    });

}

function loaditem_modlist(unithash,bind_modifier,unit,unithashkey,qty,modeoftask)
{
    jQuery('#modifier_list_' + unit).html('');            // add when only select one unit if multiple unit than remove it

    var card = '<div id="unitmoddiv_' + unithash +'" class="card" style="margin-top:15px"><div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-one_' + unithash +'" href="#collapse-one_' + unithash + '" style="cursor: pointer">';
    card += '<div class="card-title"><span id="frmtitle_' + unithash + '" class="title"></span><input type="hidden" id="allmodifier_' + unithash + '" name="allmodifier_' + unithash +'""></div>';
    card += '<a unithashkey="' + unithashkey + '"  unit="' + unit + '" onclick="closeunit(this)" style="float:right;cursor: pointer;font-size: 15px;" class="card-title"> <i class="fa fa-close"></i></a></div><div class="card-body panel-collapse collapse out in" id="collapse-one_' + unithash +'"><div class="form-group clearfix"></div><div class="form-group clearfix">';
    card += '<label class="col-sm-3 control-label textalignleft" for="name">' + QUNTITY + '</label>';
    card += '<div class="col-sm-9"><div class="controls">';
    card += '<input id="itemqty_' + unithash + '" name="itemqty_' + unithash + '" class="integer form-control" type="text"  onkeypress="return isFloat(event,this.value)" required="" style="width:47%;margin-right:5%;display:inline-block" maxlength="10" value="'+qty+'">';
    card += '</div></div></div><div class="form-group clearfix">';

    var modifier_str= '';

    if(bind_modifier != '')
    {
        for (var i=0;i<bind_modifier.length;i++) {
            var min = bind_modifier[i]['mod_min'];
            var max = bind_modifier[i]['mod_max'];
            var is_included = bind_modifier[i]['mod_is_included'];
            var modifier = bind_modifier[i]['mod_hash'];
            var modifier_txt = bind_modifier[i]['mod_name'];
            var modifier_unit = bind_modifier[i]['mod_unit'];
            var modifieritem = bind_modifier[i]['mi_hashkeys'];
            var modunitname = bind_modifier[i]['mod_unitname'];

            if (modifier_str=='')
                modifier_str += modifier;
            else
                modifier_str += ","+modifier;

            card += '<div id="modifier_' + unithash + '_' + modifier + '" data-included="' + is_included + '" class="card" style="margin-top:15px"><div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-one_' + unithash + '_' + modifier + '" href="#collapse-one_' + unithash + '_' + modifier + '" style="cursor: pointer">';
            card += '<div class="card-title"> <span id="frmtitle_' + unithash + '_' + modifier + '" class="title">' + modifier_txt + '</span> <input type="hidden" id="sub_modifier_' + unithash + '_' + modifier + '" name="sub_modifier_' + unithash + '_' + modifier + '" value="' + modifier + '"> <input type="hidden" id="sub_modifieritem_' + unithash + '_' + modifier + '" name="sub_modifieritem_' + unithash + '_' + modifier + '" value="' + modifieritem + '"></div>';
            card += '<a mod="' + modifier + '" unit="' + unithash + '" onclick="closemod(this)" style="float:right;cursor: pointer;font-size: 15px;" class="card-title"> <i class="fa fa-close"></i></a></div><div class="card-body panel-collapse collapse out in" id="collapse-one_' + unithash + '_' + modifier + '">';

            if (is_included == 0) {

                card += '<div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft" for="name">' + QUNTITY + '</label>';
                card += '<div class="col-sm-9"><div class="controls">';
                card += '<select name="moqty_' + unithash + '_' + modifier + '" class="form-control" id="moqty_' + unithash + '_' + modifier + '" style="width:47%;margin-right:5%;display:inline-block">';
                var selectedqty = '';
                if(modeoftask == 11)
                {
                    var modqty = bind_modifier[i]['mod_qty'];
                }
                for (var r = min; r <= max ; r++) {
                    if(modeoftask == 11)
                    {
                        if(modqty == r)
                        {
                            selectedqty ='selected="selected"';
                        }
                        else
                        {
                            selectedqty = '';
                        }
                    }
                    card += '<option class="optmoqty_' + unithash + '_' + modifier + '_' + r + '" value="' + r + '"   '+selectedqty+' >' + r + '</option>';
                }


                card += '</select>';
                card += '</div></div></div>';

                card += '<div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft ">' + SALEUNIT + '</label>';
                card += '<div class="col-sm-9"><div class="controls">';
                card += '<select name="mounit_' + unithash + '_' + modifier + '" class="form-control" id="mounit_' + unithash + '_' + modifier + '" style="width:47%;margin-right:5%;display:inline-block" readonly="readonly">';

                card += '<option class="optmounit_' + unithash + '_' + modifier + '" value="' + modifier_unit + '"  style="display: none;" selected="selected">' + modunitname + '</option>';

                card += '</select>';
                card += '</div></div>';
            }
            else {
                card += '<div class="form-group clearfix"></div>';
                var card2 ='<div id="modifier_item_div_' + unithash + '_' + modifier + '" class="col-xs-12 col-md-12">';
                var modiitem = bind_modifier[i]['modifieritem'];
                for (var k = 0; k < modiitem.length; k++) {
                    var moditem_hash = modiitem[k]['mi_hashkey'];
                    var itemunitname = modiitem[k]['mi_unitname'];
                    var itemunit = modiitem[k]['mi_unit'];
                    var itemname = modiitem[k]['mi_name'];


                    card2 += '<div id="modifier_item_' + unithash + '_' + moditem_hash + '" class="col-xs-6 col-md-6" style="margin-bottom: 15px;"><div class="card">';
                    card2 += '<div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-item_' + unithash + '_' + moditem_hash + '" href="#collapse-item_' + unithash + '_' + moditem_hash + '" style="cursor: pointer">';
                    card2 += '<div class="card-title"> <span id="frmtitle_' + unithash + '_' + moditem_hash + '" class="title">' + itemname + '</span><input type="hidden" id="sub_modifier_item_' + unithash + '_' + moditem_hash + '" name="sub_modifier_item_' + unithash + '_' + moditem_hash + '" value="' + moditem_hash + '">';
                    card2 += '</div><a moditem="' + moditem_hash + '" unit="' + unithash + '" mod="' + modifier + '" onclick="closemoditem(this)" style="float:right;cursor: pointer;font-size: 15px;" class="card-title"> <i class="fa fa-close"></i></a></div><div class="card-body panel-collapse collapse out in" id="collapse-item_' + unithash + '_' + moditem_hash + '"><div class="form-group clearfix">';
                    card2 += '<label class="col-sm-5 control-label textalignleft" for="name">' + QUNTITY + '</label>';
                    card2 += '<div class="col-sm-7"><div class="controls">';

                    card2 += '<select name="moitemqty_' + unithash + '_' + moditem_hash + '" class="form-control" id="moitemqty_' + unithash + '_' + moditem_hash + '" style="width:47%;margin-right:5%;display:inline-block">';
                    var selectedqty2 = '';
                    if(modeoftask == 11)
                    {
                        var modqty2 = modiitem[k]['mi_qty'];
                    }
                    for (var d = min; d <= max ; d++) {
                        if(modeoftask == 11) {
                            if (modqty2 == d) {
                                selectedqty2 = 'selected="selected"';
                            }
                            else {
                                selectedqty2 = '';
                            }
                        }
                        card2 += '<option class="optmoqty_' + unithash + '_' + moditem_hash + '_' + d + '" value="' + d + '"  '+selectedqty2+'>' + d + '</option>';
                    }

                    card2 += '</select></div></div></div>';
                    card2 += '<div class="form-group clearfix">';
                    card2 += '<label class="col-sm-5 control-label textalignleft" for="name">' + SALEUNIT + '</label>';
                    card2 += '<div class="col-sm-7"><div class="controls">';
                    card2 += '<select name="moitemunit_' + unithash + '_' + moditem_hash + '" class="form-control" id="moitemunit_' + unithash + '_' + moditem_hash + '" style="width:100%;margin-right:5px;display:inline-block"  readonly="readonly">';

                    card2 += '<option class="optmounit_' + unithash + '_' + moditem_hash + '" value="' + itemunit + '" selected="selected" style="display: none;">' + itemunitname + '</option>';
                    card2 += '</select>';
                    card2 += '</div></div></div>';
                    card2 += '<div class="form-group clearfix"></div></div></div></div>';

                }
                card2 +='</div>';
                card += card2;
            }
            card += '</div></div>';
            card += '</div>';
        }
    }

    card += '</div></div></div>';

    jQuery('#modifier_list_' + unit).append(card);

    var unit_txt = jQuery('#itemunit_sel_'+unit+' option[value="'+unithashkey+'"]').html();

    jQuery('#frmtitle_' + unithash).text(unit_txt);

    jQuery("#allmodifier_" + unithash).val(modifier_str);

  //  jQuery(".option_" + unithash).attr("disabled", "disabled");       // remove when only one unit select (all coments)

  //  var unithashkey2 = jQuery("#oldunit_item_" + unit).val();

   // if (unithashkey2 == "")
       jQuery("#oldunit_item_" + unit).val(unithashkey);
    //else
     //   jQuery("#oldunit_item_" + unit).val(unithashkey2 + "," + unithashkey);

    jQuery('#itemunit_sel_' + unit).val('0');
}

function closemod(obj) {
    var  modifier = jQuery(obj).attr('mod');
    var  unit = jQuery(obj).attr('unit');
    var bid_modifier = jQuery("#allmodifier_"+unit).val();
    jQuery("#modifier_"+unit+"_"+modifier).remove();
   var ObjMod = bid_modifier.split(',');
   var i = ObjMod.indexOf(modifier);
   if(i != -1) {
       ObjMod.splice(i, 1);
    }
   var StrMod = ObjMod.toString();
   jQuery("#allmodifier_"+unit).val(StrMod);
}
function closeunit(obj) {
    var  unithashkey = jQuery(obj).attr('unithashkey');
    var  unit = jQuery(obj).attr('unit');
    var itemunit  = jQuery("#oldunit_item_"+unit).val();
    jQuery("#unitmoddiv_"+unit+'_'+unithashkey).remove();
    var ObjMod = itemunit.split(',');
    var i = ObjMod.indexOf(unithashkey);
    if(i != -1) {
       ObjMod.splice(i, 1);
    }
    var StrMod = ObjMod.toString();
    jQuery("#oldunit_item_"+unit).val(StrMod);
    jQuery(".option_"+unit+"_"+unithashkey).removeAttr("disabled");
}
function closemoditem(obj) {
    var  modifier = jQuery(obj).attr('mod');
    var  modifieritem = jQuery(obj).attr('moditem');
    var  unit = jQuery(obj).attr('unit');

    jQuery("#modifier_item_"+unit+"_"+modifieritem).remove();
    var bid_modifier = jQuery("#sub_modifieritem_"+unit+"_"+modifier).val();
    var ObjMod = bid_modifier.split(',');
    var i = ObjMod.indexOf(modifieritem);
    if(i != -1) {
        ObjMod.splice(i, 1);
     }
    var StrMod = ObjMod.toString();
    if(StrMod == '')
    {
        var all_modifier = jQuery("#allmodifier_"+unit).val();
        jQuery("#modifier_"+unit+"_"+modifier).remove();
        var ObjMod2 = all_modifier.split(',');
        var io = ObjMod2.indexOf(modifier);
        if(io != -1) {
            ObjMod2.splice(io, 1);
        }
        var StrMod2 = ObjMod2.toString();
        jQuery("#allmodifier_"+unit).val(StrMod2);
    }
    else
    {
        jQuery("#sub_modifieritem_"+unit+"_"+modifier).val(StrMod);
    }
}
function closeGroupitem(obj) {
    var  itemgroup = jQuery(obj).attr('unithashkey');
    var  number = jQuery(obj).attr('unit');
    var bid_item = jQuery("#itemgroups_"+itemgroup).val();
    jQuery("#item_"+itemgroup+'_'+number).remove();
    var ObjMod = bid_item.split(',');
    var i = ObjMod.indexOf(number);
    if(i != -1) {
        ObjMod.splice(i, 1);
    }
    var StrMod = ObjMod.toString();
    if(StrMod == '')
    {
        jQuery("#itemgroups_"+itemgroup).val(0);
    }
    else
    {
        jQuery("#itemgroups_"+itemgroup).val(StrMod);
    }
}