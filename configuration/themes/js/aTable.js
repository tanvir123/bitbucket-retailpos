/*
 * abstable - client-side table,psging and sorting
 * author : RJ
 * Date: 25 Dec 2016
 * Version: 1.0
 */
(function (jQuery) {
	
	jQuery.jqotetag("@");
	
	var absconfig = {};
	var table = null;
	var init = true;
	var btn = 1;
	
	var methods = {							
			setConfig:function(p){
					// console.log("setConfig");
					absconfig = jQuery.extend({ 
						url: false, 
						opcode: '',
						service:'',
						pageno: 1, 
						pageidx: 0, 
						offset: 0, 
						total: 1,
						pagesize: 10, 
						pagesizes: [5, 10, 15, 20, 50], 
						limit: 10, 
						nop: 1, 
						page_id:'p_first',
						nomsg: 'No items',
						template : '',
						csearch : {},
						csort : {},
						sortkey : '',
						sortorder : '',
						lstRecPerPage: 'lstRecPerPage',
						lstGoTo: 'lstGoTo',
						usepager: true,
						usesorting: true,
						showprogress: false,
						tabledata: null,
						recnofound:null,
						resetpaging:false
					}, p);
				},
			init:function(){
					if(absconfig.usepager)
					{
						jQuery("span[class='aTablePager']").show();
						jQuery("#"+absconfig.lstRecPerPage+"").empty();
						for(idx=0;idx<absconfig.pagesizes.length;idx++)
						{
							jQuery("#"+absconfig.lstRecPerPage+"").append("<option value='"+absconfig.pagesizes[idx]+"'>"+absconfig.pagesizes[idx]+"</option>");	
						}						
						jQuery("#"+absconfig.lstRecPerPage+"").val(absconfig.pagesize>parseInt(jQuery("#"+absconfig.lstRecPerPage+" option:last").val())?parseInt(jQuery("#"+absconfig.lstRecPerPage+" option:last").val()):absconfig.pagesize); //Falguni Rana - 1.0.49.54 - 15th Jul 2016, Purpose - New UI, Allow maximum 50 records per page
						
						jQuery("#"+absconfig.lstRecPerPage+"").change(methods.pagesizechange);
					
						jQuery("#"+absconfig.lstRecPerPage+"").val(absconfig.pagesize>parseInt(jQuery("#"+absconfig.lstRecPerPage+" option:last").val())?parseInt(jQuery("#"+absconfig.lstRecPerPage+" option:last").val()):absconfig.pagesize); //Falguni Rana - 1.0.49.54 - 15th Jul 2016, Purpose - New UI, Allow maximum 50 records per page
						
						jQuery("#"+absconfig.lstGoTo+"").change(methods.selectpage);
						
						jQuery(".pgr").click(methods.pagelinkclick);
					}
					else
					{
						jQuery("div[class='aTablePager']").hide();	
					}
								
					if(absconfig.usesorting)
					{
						if(absconfig.csort)
						{
							absconfig.sortkey = absconfig.csort.sortkey;
							absconfig.sortorder = absconfig.csort.sortorder;
						}
						jQuery(".abssortcol").click(methods.sortcolclick);
					}
					methods.refreshTable();
				},
			resetPager:function(){
					if(absconfig.nop<=1)
					{
						jQuery("#"+absconfig.lstGoTo+"").attr('disabled',true);
						jQuery(".pgr").attr('disabled',true);
						var stritem="";
						for(idx=1;idx<=1;idx++)
						{
							stritem += "<option value='"+idx+"'>"+idx+"</option>";	
						}
						jQuery("#"+absconfig.lstGoTo+"").html(stritem);
						jQuery("#"+absconfig.lstGoTo+"").val('1');
						jQuery("#"+absconfig.lstRecPerPage+"").val(absconfig.pagesize>parseInt(jQuery("#"+absconfig.lstRecPerPage+" option:last").val())?parseInt(jQuery("#"+absconfig.lstRecPerPage+" option:last").val()):absconfig.pagesize); //Falguni Rana - 1.0.49.54 - 15th Jul 2016, Purpose - New UI, Allow maximum 50 records per page
						
						hidejQloading();
						return;	
					}
					else
					{
						jQuery("#"+absconfig.lstGoTo+"").attr('disabled',"");
						jQuery(".pgr").attr('disabled',"");
					}
					
					jQuery("#"+absconfig.lstGoTo+"").empty();
					var stritem="";
					for(idx=1;idx<=absconfig.nop;idx++)
					{
						stritem += "<option value='"+idx+"'>"+idx+"</option>";	
					}
					
					jQuery("#"+absconfig.lstGoTo+"").html(stritem);
					jQuery("#"+absconfig.lstGoTo+"").val(absconfig.pageno);
					
					methods.pagechange();
					
					jQuery("#"+absconfig.lstRecPerPage+"").val(absconfig.pagesize);
					hidejQloading();
				},
				refreshTable : function() { 
					//console.log("refreshTable");
					//console.log(absconfig);
					
					if(absconfig.showprogress) showjQloading();
					
					// Normal operation code
					var reqdata =  {opcode:absconfig.opcode,service:absconfig.service};
					
					// Paging
					if(absconfig.usepager)
					{
						//console.log("123");
						absconfig.pageidx = absconfig.pageno-1;
						absconfig.offset = absconfig.pageidx * absconfig.pagesize;
						absconfig.limit = absconfig.pagesize;
						
						//console.log(absconfig.offset+"|"+absconfig.limit);
						reqdata =  jQuery.extend(reqdata,{offset:absconfig.offset,limit:absconfig.limit});
					}
					
					// Sorting
					if(absconfig.usesorting)
					{
						reqdata =  jQuery.extend(reqdata,absconfig.csort);
					}
					
					// Searching
					reqdata =  jQuery.extend(reqdata,absconfig.csearch);
					//console.log("Reset Paging :"+absconfig.resetpaging);
					
					if(absconfig.resetpaging)
					{
						absconfig.pageidx = 0;
						absconfig.pageno = 1;
						absconfig.offset = 0;
						reqdata = jQuery.extend(reqdata,{offset:absconfig.offset,limit:absconfig.limit});
						absconfig.resetpaging=false;
					}
					
					//console.log(absconfig.tabledata);
					
					if(absconfig.tabledata)
					{
						res = eval(absconfig.tabledata);
						jQuery(".aTablePager").show();
     							jQuery(".norecord").hide();
						if(!parseInt(res[0].cnt))
						{
							if(typeof absconfig.recnofound=="function")
							{
								absconfig.recnofound();
							}
							else
							{
								// alert(absconfig.nomsg);	
							}
							if(absconfig.showprogress) hidejQloading();
						}
						table.empty();
						jQuery(table).jqoteapp("#"+absconfig.template, eval(res[0].data));
						init=false;
						if(absconfig.usepager)
						{
							absconfig.total = res[0].cnt;
							jQuery(".totalcnt").text(absconfig.total); 
							jQuery(".noofrecordsinpage").text(absconfig.pagesize);
							absconfig.nop = Math.ceil(absconfig.total/absconfig.pagesize);
							methods.resetPager();
						}
						if(absconfig.showprogress) hidejQloading();							
						absconfig.tabledata=null;
						setupcnt();
						return;
					}
					var str = urlencode(JSON.stringify(reqdata));
					
					jQuery.ajax({
						async: true,
						type : 'POST',
						data: str,
						dataType: 'json',
						url: absconfig.url,
						success: function(jresponse) {
							res = eval(jresponse);
							jQuery(".aTablePager").show();
     							jQuery(".norecord").hide();
							if(!parseInt(res[0].cnt))
							{
								if(typeof absconfig.recnofound=="function")
								{
									absconfig.recnofound();
								}
								else
								{
									// alert(absconfig.nomsg);	
								}
							}
							table.empty();
							jQuery(table).jqoteapp("#"+absconfig.template, eval(res[0].data));
							
							if(absconfig.usepager)
							{
								absconfig.total = res[0].cnt;
								jQuery(".totalcnt").text(absconfig.total); 
								jQuery(".noofrecordsinpage").text(absconfig.pagesize);
								absconfig.nop = Math.ceil(absconfig.total/absconfig.pagesize);
								if(init)
								{
									init=false;
									methods.resetPager();
								}
								else
								{
									methods.pagechange();
								}
							}
							if(absconfig.showprogress) hidejQloading();
							setupcnt();
						}
					});		
				},
			pagesizechange : function(){
					absconfig.pageno = 1;
					absconfig.pagesize = jQuery(this).val();
					init = true;
					absconfig.page_id='p_first';
					methods.refreshTable();
				},
			selectpage : function(){
					absconfig.pageno = jQuery(this).val();
					jQuery("#p_first").attr('title',parseInt(1));
					jQuery("#p_prev").attr('title',parseInt(absconfig.pageno)-btn);
					jQuery("#p_next").attr('title',parseInt(absconfig.pageno)+btn);
					jQuery("#p_last").attr('title',parseInt(absconfig.nop));
					methods.refreshTable();
					if(absconfig.showprogress) hidejQloading();
				},
			pagechange : function(){
					if(absconfig.csearch!={})
					{
						jQuery("#"+absconfig.lstGoTo+"").empty();
						var stritem="";
						for(idx=1;idx<=absconfig.nop;idx++)
						{
							stritem += "<option value='"+idx+"'>"+idx+"</option>";	
						}
						jQuery("#"+absconfig.lstGoTo+"").html(stritem);
					}
					if(parseInt(absconfig.nop)>0)
					{
						jQuery("#"+absconfig.lstGoTo+"").attr('disabled',false);
						jQuery("#"+absconfig.lstGoTo+"").val(absconfig.pageno);
					}
					else
						jQuery("#"+absconfig.lstGoTo+"").attr('disabled',true);
						
					if(absconfig.pageno<=btn)
					{
						jQuery("#p_first").attr('disabled','disabled');
						jQuery("#p_first").removeClass('activebtn');
						jQuery("#p_first").addClass('inactivebtn');
						jQuery("#p_prev").attr('disabled','disabled');
						jQuery("#p_prev").removeClass('activebtn');
						jQuery("#p_prev").addClass('inactivebtn');
					}
					else
					{
						jQuery("#p_first").removeAttr('disabled');
						jQuery("#p_first").removeClass('inactivebtn');
						jQuery("#p_first").addClass('activebtn');
						jQuery("#p_prev").removeAttr('disabled');
						jQuery("#p_prev").removeClass('inactivebtn');
						jQuery("#p_prev").addClass('activebtn');
					}
					
					if((parseInt(absconfig.pageno)+parseInt(btn))>parseInt(absconfig.nop))
					{
						jQuery("#p_last").attr('disabled','disabled');
						jQuery("#p_last").removeClass('activebtn');
						jQuery("#p_last").addClass('inactivebtn');
						jQuery("#p_next").attr('disabled','disabled');
						jQuery("#p_next").removeClass('activebtn');
						jQuery("#p_next").addClass('inactivebtn');
					}
					else
					{
						jQuery("#p_last").removeAttr('disabled');
						jQuery("#p_last").removeClass('inactivebtn');
						jQuery("#p_last").addClass('activebtn');
						jQuery("#p_next").removeAttr('disabled');
						jQuery("#p_next").removeClass('inactivebtn');
						jQuery("#p_next").addClass('activebtn');
					}

					if(absconfig.page_id=="p_next" || 
					   absconfig.page_id=="p_prev" || 
					   absconfig.page_id=="p_first" || 
					   absconfig.page_id=="p_last")
					{
						jQuery("#p_first").attr('title',parseInt(1));
						jQuery("#p_prev").attr('title',parseInt(absconfig.pageno)-btn);
						jQuery("#p_next").attr('title',parseInt(absconfig.pageno)+btn);
						jQuery("#p_last").attr('title',parseInt(absconfig.nop));
					}
					
					absconfig.page_id="";
				},
			pagelinkclick : function(){
					if(absconfig.pageno == jQuery(this).attr('title'))return;
					absconfig.page_id = jQuery(this).attr('id');
					absconfig.pageno = jQuery(this).attr('title');
					methods.refreshTable();
					if(absconfig.showprogress) hidejQloading();
				},
			sortcolclick: function(){
					// RJ - 1.0.49.54 - 09 May 2016 - Start
					// Added sorting icon for new ui
					absconfig.pageno = 1;
					absconfig.page_id='p_first';
					var column = jQuery(this).attr('id');
					browserurl = window.location.href;
					if (browserurl.indexOf('frontoffice') > -1){
						jQuery('.descending-sort').removeAttr("style");
						jQuery('.ascending-sort').removeAttr("style");
						if(absconfig.sortkey != column)
						{
							absconfig.sortorder='asort';
						}
						else
						{
						  if(absconfig.sortorder=='asort')
							  absconfig.sortorder='dsort';
						  else
							  absconfig.sortorder='asort';
						}
						absconfig.sortkey = column;
						if( absconfig.sortorder=='asort')
						{
						  jQuery("#icodesc-"+absconfig.sortkey).attr("style","border-color: transparent transparent white");
						  jQuery("#icoasc-"+absconfig.sortkey).attr("style","border-color: #000000 transparent transparent");
						}
						else
						{
						  jQuery("#icoasc-"+absconfig.sortkey).attr("style","border-color: white transparent transparent");
						  jQuery("#icodesc-"+absconfig.sortkey).attr("style","border-color: transparent transparent #000000 ");
						}
					}
					else{
						if(absconfig.sortkey != column)
						{
							absconfig.sortorder='asort';
						}
						else if(jQuery("span[id*='"+column+"']").length>0)
						{
							sortOrder=jQuery("span[id*='"+column+"']").attr('class');
							if(absconfig.sortorder=='asort')
								absconfig.sortorder='dsort';
							else
								absconfig.sortorder='asort';
						}
						absconfig.sortkey = column;
						jQuery("span[class='asort']").remove();
						jQuery("span[class='dsort']").remove();
						jQuery("a[id='"+absconfig.sortkey+"']").append("<span id='"+absconfig.sortkey+"' class='"+absconfig.sortorder+"'></span>");
					}
					absconfig.csort={'sortkey':absconfig.sortkey,'sortorder':absconfig.sortorder};
					
					methods.refreshTable();
					if(absconfig.showprogress) hidejQloading();
					// RJ - 1.0.49.54 - 09 May 2016 - End
				},
				renderStart:function(fun){jQuery.jqoteRenderStart(fun);},
				renderEnd:function(fun){if(absconfig.showprogress) hidejQloading();	jQuery.jqoteRenderEnd(fun);},
				rowStart:function(fun){jQuery.jqoteRowStart(fun);},
				rowEnd:function(fun){jQuery.jqoteRowEnd(fun);}
		};
	
	jQuery.fn.aTable = function (p,q) {
		table = this;
		//console.log(p);
		//console.log(q);
		
		if(methods[p] && typeof q == 'object')
		{
			methods.setConfig(q);			
			methods[p].apply(this); 
			return absconfig;
		}
		else if(methods[p] && typeof q == 'function')
		{
			//console.log(2);
			methods[p].call(this,q); 	
		}
		//methods.setConfig(p);
		if(p.load) methods.init();
	}; 
})(jQuery);
