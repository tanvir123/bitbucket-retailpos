
function removematerial(id,module,msg,cancel,ok){
    var labelsprint = {
        ok     : ok,
        cancel : cancel
    };
    alertify.confirm(msg,labelsprint, function (e) {
        if (e) {
            var arr = {};
            arr['service']= 'rawmaterial';
            arr['opcode']= 'removematerial';
            arr['id']= id;
            arr['module']= module;
            var str = urlencode(JSON.stringify(arr));
            if (arr['id'] != '') {
                HttpSendRequest(str,function(data){
                    if (data['Success'] == "True") {
                        refreshlist();
                        alertify.success(data['Message']);
                    }
                    else{
                        alertify.error(data['Message']);
                    }
                });
            }
        } else {
            return false;
        }
    });
}

function removeinventory(id){
    var arr = {};
    arr['service']= 'rawmaterial';
    arr['opcode']= 'removeinventory';
    arr['id']= id;
    var str = urlencode(JSON.stringify(arr));
    if (arr['id'] != '') {
        HttpSendRequest(str,function(data){
            if (data['Success'] == "True") {
                getinventory();
                alertify.success(data['Message']);
            }
            else{
                alertify.error(data['Message']);
            }
        });
    }
}

function manageinventory(id,measureval,name){
    refreshfrm();
    jQuery("#modalInventory").modal('show');
    jQuery('#location').val('');
    jQuery("#tinventorylist").hide();
    jQuery("#tinorecord").show();

    jQuery('.inv_unit').val(measureval);
    jQuery(".rawhash").val(id);
    jQuery('#rawitemlabel').html('');
    jQuery('#rawitemlabel').append(' -'+' '+name);

    var rawhash=jQuery(".rawhash").val();
    var myForm = document.getElementById('frminventory');
    var ajaxData = new FormData(myForm);
    ajaxData.append('service','rawmaterial');
    ajaxData.append('opcode', 'getUnitsBYrawmaterial');
    ajaxData.append('measureval', measureval);
    HttpSendRequest(ajaxData,function(data)
    {
        if (data['Success'] == 'True') {
            jQuery('.inunit').html('');
            jQuery('.inunit').append('<option value="">'+plz_select+'</option>');
            var unitData= data['Data'];
            if(unitData.length > 0) {
                if (typeof unitData != 'undefined' && unitData != null && unitData != '') {
                    jQuery.each(unitData, function (key, value) {
                        jQuery('.inunit').append('<option value="' + value["unitid"] + '" data-unit="'+value["unitrate"]+'">' + value["name"] + '</option>');
                    });
                }
            }
        }
        return false;
    });

    var vendorData = new FormData(myForm);
    vendorData.append('service','vendor');
    vendorData.append('opcode', 'getAllVendor');
    HttpSendRequest(vendorData,function(data)
    {
        if (data['Success'] == 'True') {
            jQuery('.vendor').html('');
            jQuery('.vendor').append('<option value="">'+plz_select+'</option>');
            jQuery.each(data['Data'], function (key, value) {
                jQuery('.vendor').append('<option value="'+ value["vandorunkid"] +'">'+value["name"]+'</option>');
            });
        }
        return false;
    });

    getinventory();
}

function getinventory(){
    var rawhash=jQuery("#irawhash").val();
    //Inventory Data
    var arr={};
    arr['service'] = "rawmaterial";
    arr['opcode'] = "getInventoryRec";
    arr['rawhash'] = rawhash;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function(data){
        if(data.Success=='True') {
            var rec = data.Data;
            if(rec.length>0) {
                jQuery("#inorecord").hide();
                jQuery("#inventorylist").show();
                $('.hide_display').html('');
                $('#total_inv').append(' : '+rec[0]['total_inventory']+' '+rec[0]['base_code']);
                $('#rate_per_unit').append(' : '+rec[0]['rateperunit']);
                jQuery('#rateperunit').val(rec[0]['rateperunit']);
                jQuery.each(rec, function (key, value) {
                    if(typeof value['raw_inventory']!='undefined' && value['raw_inventory']!='' && value['raw_inventory']!=null){
                        var inventory = value['raw_inventory'];
                    }
                    else{
                        var inventory = value['inventory'];
                    }

                    if(value['is_deleted']==1) {
                        $('#detailslist').append('<tr><td align="left"><strike>' + value['vendor_name']+'<strike></td><td align="left"><strike>' + inventory + ' ' + value['shortcode'] + '</strike></td><td align="left"><strike>' + value['rate'] + '</strike></td><td align="left"><strike>' + value['createduser'] + '<br/>' + value['created_date'] + '</strike></td><td align="left"><strike>' + value['remark'] + '</strike></td>' +
                            '</tr>');
                    }
                    else{
                        // User :Nikita Removed to display inventory updates for indent management. Date  : 09/08/18.
                        //1: ADD 2: Issue Voucher 3 : Good Receipt Note 4 : Goods Return 5: order 6 : Banquet
                      //  if(value['inv_status']==1) {
                        if(value['inv_status']==1 ||value['inv_status']==2 ||value['inv_status']==3 ) {
                            var Delete_inventory= '<tr><td align="left">' + value['vendor_name']+'</td><td align="left">' + inventory + ' ' + value['shortcode'] + '</td><td align="left">' + value['rate'] + '</td><td align="left">' + value['createduser'] + '<br/>' + value['created_date'] + '</td><td align="left">' + value['remark'] + '</td>';
                            if(Delete_Inventory != -1)
                            {

                                Delete_inventory+=   '<td align="left"><a class="update_ico" onclick="removeinventory(' + value['id'] + ')" ondblclick="return false;" title="'+lblremove+'">' +
                                    '<i class="fa fa-trash-o"></i></a></td>';
                            }
                            Delete_inventory +='</tr>';
                            $('#detailslist').append(Delete_inventory);

                        }
                    }
                });
            }
            else{
                jQuery("#inventorylist").hide();
                jQuery("#inorecord").show();
            }
        }
    });
}

function loadunit(unitid){
    var vunitid=unitid;
    jQuery('#unit_sel').select2();
    jQuery('#unit_sel').empty();
    jQuery("#unit_list").empty();
    jQuery('#unit_detail').val('');
    jQuery('#old_unit').val('');
    jQuery('#unit_code').html('');
    jQuery('.select2-selection__rendered').empty();

    var measureval=$('#measuretype').val();
    if(measureval>0) {
        var myForm = document.getElementById('rawform');
        var ajaxData = new FormData(myForm);
        ajaxData.append('service', 'rawmaterial');
        ajaxData.append('opcode', 'getUnitsBYmeasureType');
        ajaxData.append('measureval', measureval);
        HttpSendRequest(ajaxData, function (data) {
            if (data['Success'] == 'True') {
                jQuery('.unit').html('');
                jQuery('.unit').append('<option value="">'+plz_select+'</option>');
                jQuery.each(data['Data']['unit'], function (key, value) {
                    jQuery('#munit').append('<option value="' + value["unitunkid"] + '" data-unit="' + value["unit"] + '">' + value["name"] + '</option>');
                    jQuery('#iunit').append('<option value="' + value["unitunkid"] + '" data-unit="' + value["unit"] + '">' + value["name"] + '</option>');
                });
                jQuery('#munit').val(vunitid);
                jQuery('#unit_code').html(data['Data']['code']['shortcode']);

                var rec = data['Data']['allunits'];
                var arr = {};
                if (typeof rec != 'undefined' && typeof rec != '' && typeof rec != null) {
                    for (var i = 0; i < rec.length; i++) {
                        var unitid = rec[i]['unitunkid'];
                        var name = rec[i]['name'];
                        var unit = rec[i]['unit'];
                        arr[unitid] = {};
                        arr[unitid]['name'] = name;
                        arr[unitid]['unit'] = unit;
                    }
                }
                storeunitobj = arr;
                var card = '';
                jQuery.each(storeunitobj, function (key, value) {
                    if (!isNaN(key)) {
                        card += '<option class="option_unit_' + key + '" value="' + key + '">' + value['name'] + '</option>';
                    }
                });
                jQuery('#unit_sel').append(card);
            }
        });
    }
}

//For unit conversation
function selectAllUnit()
{
    jQuery('#unit_sel').find('option').each(function(){
        jQuery(this).prop('selected',true);
    });
    jQuery('#unit_sel').change();
    return false;
}

function selectNoneUnit()
{
    jQuery('#unit_sel').find('option').prop( 'selected',false );
    jQuery('#unit_sel').change();
    return false;
}

function loadstore_unitlist(bind_unit) {

    jQuery("#unit_div").removeClass('hide');
    var unit_detail_str='';

    $('#unit_sel').select2({
        allowClear: true,
        minimumResultsForSearch: -1,
    });

    $('#unit_sel').on('select2:open', function () {
        var values = $(this).val();
        var pop_up_selection = $('.select2-results__options');
        if (values != null ) {
            jQuery('.select2-results__options').find("li[aria-selected=true]").hide();
        } else {
            jQuery('.select2-results__options').find("li[aria-selected=true]").show();
        }
    });

    var unitobj_sub =storeunitobj;
    for (var i=0;i<bind_unit.length;i++) {

        var unit_id = bind_unit[i]['lnkunitid'];
        var name = unitobj_sub[unit_id]['name'];
        var unit = bind_unit[i]['conversation_rate'];

        if (unit_detail_str == '')
            unit_detail_str += unit_id;
        else
            unit_detail_str += "," + unit_id;

        var card='<div id="unit_'+unit_id+'" class="col-xs-3"><div class="card">';
        card += '<div class="card-header collapsed" data-toggle="collapse"  data-target="#collapse-unit_' + unit_id +'" href="#collapse-unit_' + unit_id + '" style="cursor: pointer">';
        card +='<div class="card-title card-title-full"> <span id="frmtitle_'+unit_id+'" class="title">'+unitobj_sub[unit_id]['name']+'</span>';
        card +='</div></div><div class="card-body panel-collapse collapse out in" id="collapse-unit_' + unit_id + '">';
        card +='<div class="form-group clearfix"><div><div class="controls"><input id="conversation_rate_'+unit_id+'" name="conversation_rate_'+unit_id+'" class="form-control" type="text" onkeypress="return isFloat(event,this.value)" placeholder="Conversation Rate" maxlength="10" value="'+unit+'" readonly></div>';
        card +='</div></div></div></div></div>';

        jQuery(".option_unit_"+unit_id).prop('selected',true);
        jQuery('#unit_list').append(card);
    }
    jQuery('#unit_sel').change();
    jQuery('#unit_detail').val(unit_detail_str);
    jQuery('#old_unit').val(unit_detail_str);

    jQuery('.select2-selection__choice .select2-selection__choice__remove').addClass('hide');
    jQuery('.select2-selection__clear').addClass('hide');
}

//Add
function loadUnitDetail()
{
    var unitDetail = jQuery('#unit_sel').val();
    var old_unit = jQuery('#old_unit').val();

    var oldunitDetail=old_unit.split(',');
    var difference = $(unitDetail).not(oldunitDetail).get();
    var unit_detail_str='';
    var unitobj_sub =storeunitobj;

    //Delete
    var newDiff=$(oldunitDetail).not(unitDetail).get();
    if(newDiff!=null){
        for (var i=0;i<newDiff.length;i++) {
            var hashkey = newDiff[i];
            jQuery("#unit_"+hashkey).remove();
        }
    }
    if (difference!=null || typeof difference != 'undefined') {
        for (var i = 0; i < difference.length; i++) {
            var unit_id = difference[i];
            var name = unitobj_sub[unit_id]['name'];
            var unit = unitobj_sub[unit_id]['unit'];

            if (unit_detail_str == '')
                unit_detail_str += unit_id;
            else
                unit_detail_str += "," + unit_id;

            var card='<div id="unit_'+unit_id+'" class="col-xs-3"><div class="card">';
            card += '<div class="card-header collapsed" data-toggle="collapse"  data-target="#collapse-unit_' + unit_id +'" href="#collapse-unit_' + unit_id + '" style="cursor: pointer">';
            card +='<div class="card-title card-title-full"> <span id="frmtitle_'+unit_id+'" class="title">'+unitobj_sub[unit_id]['name']+'</span>';
            card +='</div></div><div class="card-body panel-collapse collapse out in" id="collapse-unit_' + unit_id + '">';
            card +='<div class="form-group clearfix"><div><div class="controls"><input id="conversation_rate_'+unit_id+'" name="conversation_rate_'+unit_id+'" class="form-control" type="text" onkeypress="return isFloat(event,this.value)" placeholder="Conversation Rate" maxlength="10" value="'+unit+'"></div>';
            card +='</div></div></div></div></div>';
            jQuery('#unit_list').append(card);
        }
        jQuery('#old_unit').val(unitDetail);
        jQuery('#unit_detail').val(unitDetail);
    }
    else {
        jQuery('#unit_detail').val('');
    }
}
function validate_store_unit(unit_str) {
    var flag = 1;

    if(jQuery('#minlimit').val()!='' && jQuery('#minlimit').val()!=0){
        if (jQuery("#munit :selected").val() == 0) {
            jQuery("#munit").addClass('invalid');
            flag = 0;
        }
    }
    if(jQuery("#munit :selected").val() !=0){
        if (jQuery('#minlimit').val() =='' || jQuery('#minlimit').val()==0) {
            jQuery("#minlimit").addClass('invalid');
            flag = 0;
        }
    }

    if(typeof unit_str != 'undefined' && unit_str != null && unit_str != '') {
        var unit_arr = unit_str.split(',');
        if(unit_arr.length > 0) {
            for (var i = 0; i < unit_arr.length; i++) {
                var unit = unit_arr[i];

                if ((jQuery("#conversation_rate_" + unit).val() == "") || (jQuery("#conversation_rate_" + unit).val() == 0)) {
                    jQuery("#conversation_rate_" + unit).addClass('invalid');
                    flag = 0;
                }
            }
        }
    }

    if(jQuery('#is_recipe_avalible').is(':checked') == true)
    {
        var recipe_rawid = jQuery('#old_rawmaterial').val();
        if(typeof recipe_rawid != 'undefined' && recipe_rawid != null && recipe_rawid != '') {
            var recipe_rawid_arr = recipe_rawid.split(',');
            if(recipe_rawid_arr.length > 0) {
                for (var i = 0; i < recipe_rawid_arr.length; i++) {
                    var recipe = recipe_rawid_arr[i];

                    if ((jQuery("#itemunit_" + recipe).val() == "") || (jQuery("#itemunit_" + recipe).val() == 0)) {
                        jQuery("#itemunit_" + recipe).addClass('invalid');
                        flag = 0;
                    }
                    if ((jQuery("#itemQty_" + recipe).val() == "") || (jQuery("#itemQty_" + recipe).val() == 0)) {
                        jQuery("#itemQty_" + recipe).addClass('invalid');
                        flag = 0;
                    }
                }
            }else{
                flag=0;
            }
        }else{
            flag=0;
        }
    }
    return flag;
}


//For Recipe Selection
function selectAllRawMaterial()
{
    jQuery('#rawmaterial_sel').find('option').each(function(){
        jQuery(this).prop('selected',true);
    });
    jQuery('#rawmaterial_sel').change();
    return false;
}

function selectNoneRawMaterial()
{
    jQuery('#rawmaterial_sel').find('option').prop( 'selected',false );
    jQuery('#rawmaterial_sel').change();
    return false;
}

function loadRawMaterialDetail()
{
    var unitDetail = jQuery('#rawmaterial_sel').val();
    var old_unit = jQuery('#old_rawmaterial').val();

    var oldunitDetail=old_unit.split(',');
    var difference = $(unitDetail).not(oldunitDetail).get();
    var unit_detail_str='';

    //Delete
    var newDiff=$(oldunitDetail).not(unitDetail).get();
    if(newDiff!=null){
        for (var i=0;i<newDiff.length;i++) {
            var hashkey = newDiff[i];
            jQuery("#unit_"+hashkey).remove();
        }
    }

    var arr={};
    arr['service'] = "rawmaterial";
    arr['opcode'] = "getRawMaterialUnits";
    arr['raw_mat_id'] = unitDetail;
    var str = urlencode(JSON.stringify(arr));

    HttpSendRequest(str, function (data) {
        if (data['Success'] == 'True') {
           var allunitlist = data['Data'];

            if (difference!=null || typeof difference != 'undefined') {
                for (var i = 0; i < difference.length; i++) {
                    var item_id = difference[i];

                    var name = jQuery('#rawmaterial_sel option[value="'+item_id+'"]').html(); // my method to get name

                    var itemunitlist = allunitlist[item_id];

                    if (unit_detail_str == '')
                        unit_detail_str += item_id;
                    else
                        unit_detail_str += "," + item_id;

                    var card='<div id="unit_'+item_id+'" class="col-xs-6"><div class="card">';
                    card += '<div class="card-header collapsed" data-toggle="collapse"  data-target="#collapse-unit_' + item_id +'" href="#collapse-unit_' + item_id + '" style="cursor: pointer">';
                    card +='<div class="card-title card-title-full"> <span id="frmtitle_'+item_id+'" class="title">'+name+'</span>';
                    card +='</div></div><div class="card-body panel-collapse collapse out in" id="collapse-unit_' + item_id + '">';
                    card +='<div class="form-group clearfix"><div>';

                    card += '<div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft" for="name">'+SELECT_UNIT+'</label>';
                    card += '<div class="col-sm-9"><div class="controls">';
                    card += '<select name="itemunit_' + item_id + '" class="form-control" id="itemunit_' + item_id + '" style="width:47%;margin-right:5%;display:inline-block">';

                    if(typeof itemunitlist != 'undefiend'  && itemunitlist.length>0)
                    {
                        for(var j=0;j<itemunitlist.length;j++)
                        {
                            var unitkey = itemunitlist[j]['lnkunitid'];
                            var name = itemunitlist[j]['unit_name'];

                            card += '<option class="optitemunit_' + item_id +'_' + unitkey +'" value="' + unitkey + '" >' + name + '</option>';
                        }
                    }
                    card += '</select>';
                    card += '</div></div></div>';
                    card += '<div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft" for="name">'+QUANTITY+'</label>';
                    card += '<div class="col-sm-9"><div class="controls"><input id="itemQty_'+item_id+'" name="itemQty_'+item_id+'" class="form-control" type="text" onkeypress="return isFloat(event,this.value)" maxlength="30"></div></div></div>';
                    card +='</div></div></div></div></div></div>';
                    jQuery('#rawmaterial_list').append(card);
                }
                jQuery('#old_rawmaterial').val(unitDetail);
            }
            else {
                jQuery('#old_rawmaterial').val('');
            }

        }
    });


}



