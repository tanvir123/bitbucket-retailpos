
//Unit
function loaditem_unitlist(bind_unit) {

    jQuery("#unit_div").removeClass('hide');
    var unit_detail_str='';
    jQuery('#unit_sel').select2();

    for (var i=0;i<bind_unit.length;i++) {

        var unit_hash = bind_unit[i]['unithash'];

        if (unit_detail_str=='')
            unit_detail_str += unit_hash;
        else
            unit_detail_str +=","+unit_hash;
        jQuery('#rawmaterial_sel_' + unit_hash).select2();
        var card = '<div id="unit_' + unit_hash + '" class="col-sm-12" style="margin-bottom: 15px;"><div class="card">';
        card += '<div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-unit_' + unit_hash + '" href="#collapse-unit_' + unit_hash + '" style="cursor: pointer">';
        card +='<div class="card-title"> <span id="frmtitle_' + unit_hash + '" class="title">' + bind_unit[i]['name'] + '</span><span  class="title" id="avg_cost_'+unit_hash+'" style="padding-left: 820px;"></span>';
        card += '</div></div><div class="card-body panel-collapse out collapse" id="collapse-unit_' + unit_hash + '">';
        card += '<div class="form-group col-md-2">';
        card += '<label>'+RATE_1+'</label>';
        card += '<input id="rate1_' + unit_hash + '" name="rate1_' + unit_hash + '" class="form-control" type="text" value=" '+ bind_unit[i]['rate1'] +'" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_1+'" maxlength="10" required>';
        card += '</div>';
        card += '<div class="form-group col-md-2">';
        card += '<label>'+RATE_2+'</label>';
        card += '<input id="rate2_' + unit_hash + '" name="rate2_' + unit_hash + '" class="form-control" type="text" value=" '+ bind_unit[i]['rate2'] +'" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_2+'" maxlength="10" required>';
        card += '</div>';
        card += '<div class="form-group col-md-2">';
        card += '<label>'+RATE_3+'</label>';
        card += '<input id="rate3_' + unit_hash + '" name="rate3_' + unit_hash + '" class="form-control" type="text" value=" '+ bind_unit[i]['rate3'] +'" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_3+'" maxlength="10" required>';
        card += '</div>';
        card += '<div class="form-group col-md-2">';
        card += '<label>'+RATE_4+'</label>';
        card += '<input id="rate4_' + unit_hash + '" name="rate4_' + unit_hash + '" class="form-control" type="text" value="'+ bind_unit[i]['rate4'] +'" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_4+'" maxlength="10" required>';
        card += '</div>';
        card += '<div class="form-group col-md-3">';
        card += '<label>'+DEFAULT_RATE+'</label>';
        card += '<select name="defaultrate_' + unit_hash + '" class="form-control" id="defaultrate_' + unit_hash + '">';
        card += '<option class="optrate_' + unit_hash + '" value="1" >'+RATE_1+'</option>';
        card += '<option class="optrate_' + unit_hash + '" value="2" >'+RATE_2+'</option>';
        card += '<option class="optrate_' + unit_hash + '" value="3" >'+RATE_3+'</option>';
        card += '<option class="optrate_' + unit_hash + '" value="4" >'+RATE_4+'</option>';
        card += '</select></div><div class="clearfix"></div>';

        //Recipe
        if(is_store == 1) {
        card +='<div class="card">';
        card +='<div class="card-header"><div class="card-title"><span class="title">'+LABEL_RECIPE+'</span></div></div>';
        card+='<div class="card-body">';
        card += '<div class="form-group clearfix"><label class="col-sm-2 textalignleft">'+RAW_MATERIALS+'</label>';
        card += '<div class="col-md-10"><select id="rawmaterial_sel_' + unit_hash + '" class="rawmaterial_sel_' + unit_hash + '" multiple="multiple" style="width:100%">';
        jQuery.each(rawdetailobj, function(key, value) {
            if(!isNaN(key)) {
                card += '<option class="option_raw_' + unit_hash + '_' + key + '" value="' + key + '">' + value['name'] + '</option>';
            }
        });

        card += '</select>';
        card += '<input type="hidden" class="form-control" id="raw_detail_'+unit_hash+'" class="raw_detail" name="raw_detail_'+unit_hash+'" />';
        card += '<input type="hidden" class="form-control" id="old_raw_'+unit_hash+'" name="old_raw_'+unit_hash+'" /></div>';
        card +='<div class="form-group clearfix"></div><label class="col-sm-3 control-label textalignleft "></label>';
        card +=' <div class="col-sm-9">';
        card +='<button type="button" class="button btn" unit="'+ unit_hash +'" style="margin-right: 10px" onclick="selectAllRawmaterial(this);">'+SELECT_ALL+'</button>';
        card +='<button style="margin-right: 10px" unit="'+ unit_hash +'" type="button" class="button btn" onclick="selectNoneRawmaterial(this);">'+SELECT_NONE+'</button>';
        card +='<button  style="float: right" unit="'+ unit_hash +'"  type="button" class="btn btn-primary" onclick="loadRawmaterialDetail(this);">'+LOAD+'</button>';
        card += '</div><div class="form-group clearfix"></div>';
        card += '<div id="rawdetail_list_'+unit_hash+'"></div>';
        card += '</div></div></div></div>';
        }
        card +='</div></div>';

        jQuery(".option_unit_"+unit_hash).prop('selected',true);
        jQuery('#unit_list').append(card);
        jQuery("#defaultrate_"+unit_hash).val(bind_unit[i]['default_rate']);
        if(is_store == 1) {
        loaditem_rawlist(unit_hash,bind_unit[i]['bind_recipe']);
        }
    }
    jQuery('#unit_sel').change();
    jQuery('#unit_detail').val(unit_detail_str);
    jQuery('#old_unit').val(unit_detail_str);
}

function getUnit()
{
    jQuery('#unit_sel').select2();
    var req={};
    req['service'] = 'menu_item';
    req['opcode'] = 'getAllItemunit';
    var str = JSON.stringify(req);
    HttpSendRequest(str,function(data) {
        if (data['Success'] == 'True') {
            var rec = data['Data'];
            var arr = {};
            for(var i=0; i<rec.length; i++)
            {
                var hashkey = rec[i]['hashkey'];
                var name = rec[i]['name'];
                arr[hashkey] = {};
                arr[hashkey]['name'] = name;
            }
            unitdetailobj=arr;
        }
    });
}

function selectAllUnit()
{
    jQuery('#unit_sel').find('option').each(function(){
        jQuery(this).prop('selected',true);
    });
    jQuery('#unit_sel').change();
    return false;
}

function selectNoneUnit()
{
    jQuery('#unit_sel').find('option').prop( 'selected',false );
    jQuery('#unit_sel').change();
    return false;
}

function loadUnitDetail()
{
    var unitDetail = jQuery('#unit_sel').val();
    var old_unit = jQuery('#old_unit').val();

    var oldunitDetail=old_unit.split(',');
    var difference = $(unitDetail).not(oldunitDetail).get();
    var unit_detail_str='';
    var unitobj_sub =unitdetailobj;

    //Delete
    var newDiff=$(oldunitDetail).not(unitDetail).get();
    if(newDiff!=null){
        for (var i=0;i<newDiff.length;i++) {
            var hashkey = newDiff[i];
            jQuery("#unit_"+hashkey).remove();
        }
    }
    if (difference!=null) {
        for (var i = 0; i < difference.length; i++) {
            var unit_hash = difference[i];
            var name = unitobj_sub[unit_hash]['name'];

            if (unit_detail_str == '')
                unit_detail_str += unit_hash;
            else
                unit_detail_str += "," + unit_hash;

            var card = '<div id="unit_' + unit_hash + '" class="col-sm-12" style="margin-bottom: 15px;"><div class="card">';
            card += '<div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-unit_' + unit_hash + '" href="#collapse-unit_' + unit_hash + '" style="cursor: pointer">';
            card +='<div class="card-title"> <span id="frmtitle_' + unit_hash + '" class="title">' + unitobj_sub[unit_hash]['name'] + '</span><span  class="title" id="avg_cost_'+unit_hash+'" style="padding-left: 820px;">0.00</span>';
            card += '</div></div><div class="card-body panel-collapse collapse out in" id="collapse-unit_' + unit_hash + '">';
            card += '<div class="form-group col-md-2">';
            card += '<label>'+RATE_1+'</label>';
            card += '<input id="rate1_' + unit_hash + '" name="rate1_' + unit_hash + '" class="form-control" type="text" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_1+'" maxlength="10" required>';
            card += '</div>';
            card += '<div class="form-group col-md-2">';
            card += '<label>'+RATE_2+'</label>';
            card += '<input id="rate2_' + unit_hash + '" name="rate2_' + unit_hash + '" class="form-control" type="text" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_2+'" maxlength="10" required>';
            card += '</div>';
            card += '<div class="form-group col-md-2">';
            card += '<label>'+RATE_3+'</label>';
            card += '<input id="rate3_' + unit_hash + '" name="rate3_' + unit_hash + '" class="form-control" type="text" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_3+'" maxlength="10" required>';
            card += '</div>';
            card += '<div class="form-group col-md-2">';
            card += '<label>'+RATE_4+'</label>';
            card += '<input id="rate4_' + unit_hash + '" name="rate4_' + unit_hash + '" class="form-control" type="text" onkeypress="return isFloat(event,this.value)" placeholder="'+RATE_4+'" maxlength="10" required>';
            card += '</div>';
            card += '<div class="form-group col-md-3">';
            card += '<label>'+DEFAULT_RATE+'</label>';
            card += '<select name="defaultrate_' + unit_hash + '" class="form-control" id="defaultrate_' + unit_hash + '">';
            card += '<option class="optrate_' + unit_hash + '" selected="selected" value="1" >'+RATE_1+'</option>';
            card += '<option class="optrate_' + unit_hash + '" value="2" >'+RATE_2+'</option>';
            card += '<option class="optrate_' + unit_hash + '" value="3" >'+RATE_3+'</option>';
            card += '<option class="optrate_' + unit_hash + '" value="4" >'+RATE_4+'</option>';
            card += '</select></div><div class="clearfix"></div>';

            //Recipe
            if(is_store == 1) {
            card +='<div class="card">';
            card +='<div class="card-header"><div class="card-title"><span class="title">'+LABEL_RECIPE+'</span></div></div>';
            card+='<div class="card-body">';
            card += '<div class="form-group clearfix"><label class="col-sm-2 textalignleft">'+RAW_MATERIALS+'</label>';
            card += '<div class="col-md-10"><select id="rawmaterial_sel_' + unit_hash + '" class="rawmaterial_sel_' + unit_hash + '" multiple="multiple" style="width:100%">';
            jQuery.each(rawdetailobj, function(key, value) {
                if(!isNaN(key)) {
                    card += '<option class="option_raw_' + unit_hash + '_' + key + '" value="' + key + '">' + value['name'] + '</option>';
                }
            });
            card += '</select>';
            card += '<input type="hidden" class="form-control" id="raw_detail_'+unit_hash+'" class="raw_detail" name="raw_detail_'+unit_hash+'" />';
            card += '<input type="hidden" class="form-control" id="old_raw_'+unit_hash+'" name="old_raw_'+unit_hash+'" /></div>';
            card +='<div class="form-group clearfix"></div><label class="col-sm-3 control-label textalignleft "></label>';
            card +=' <div class="col-sm-9">';
            card +='<button type="button" class="button btn" unit="'+ unit_hash +'" style="margin-right: 10px" onclick="selectAllRawmaterial(this);">'+SELECT_ALL+'</button>';
            card +='<button style="margin-right: 10px" unit="'+ unit_hash +'" type="button" class="button btn" onclick="selectNoneRawmaterial(this);">'+SELECT_NONE+'</button>';
            card +='<button  style="float: right" unit="'+ unit_hash +'"  type="button" class="btn btn-primary" onclick="loadRawmaterialDetail(this);">'+LOAD+'</button>';
            card += '</div><div class="form-group clearfix"></div>';
            card += '<div id="rawdetail_list_'+unit_hash+'"></div>';
            card += '</div></div></div></div>';
            }
            card +='</div></div>';
            jQuery('#unit_list').append(card);
            jQuery('#rawmaterial_sel_' + unit_hash).select2();
        }
        jQuery('#old_unit').val(unitDetail);
        jQuery('#unit_detail').val(unitDetail);
    }
    else {
        jQuery('#unit_detail').val('');
    }
}

function validate_unit(unit_str) {
    var flag = 1;

    if(typeof unit_str != 'undefined' && unit_str != null && unit_str != '') {
        var unit_arr = unit_str.split(',');
        if(unit_arr.length > 0) {
            for (var i = 0; i < unit_arr.length; i++) {
                var unit = unit_arr[i];
                var sel_rate = jQuery("#defaultrate_" + unit + " :selected").val();

                if ((jQuery("#rate" + sel_rate + "_" + unit).val() == "") || (jQuery("#rate" + sel_rate + "_" + unit).val() == 0)) {
                    jQuery("#rate" + sel_rate + "_" + unit).addClass('invalid');
                    flag = 0;
                }
                var racipe_str = jQuery('#raw_detail_' + unit).val();

                if (typeof racipe_str != 'undefined' && racipe_str != null && racipe_str != '') {
                    var recipe_arr = racipe_str.split(',');
                    if(recipe_arr.length > 0) {
                        for (var j = 0; j < recipe_arr.length; j++) {
                            var recipe = recipe_arr[j];
                            if (jQuery("#store_" + unit + "_" + recipe + " :selected").val() == 0) {
                                jQuery("#store_" + unit + "_" + recipe).addClass('invalid');
                                flag = 0;
                            }
                            if (jQuery("#unit_" + unit + "_" + recipe + " :selected").val() == 0) {
                                jQuery("#unit_" + unit + "_" + recipe).addClass('invalid');
                                flag = 0;
                            }
                            if ((jQuery("#quantity_" + unit + "_" + recipe).val() == "") || (jQuery("#quantity_" + unit + "_" + recipe).val() == 0)) {
                                jQuery("#quantity_" + unit + "_" + recipe).addClass('invalid');
                                flag = 0;
                            }
                        }
                    }
                }
            }
        }
    }
    return flag;
}
