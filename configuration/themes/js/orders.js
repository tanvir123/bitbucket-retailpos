$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });

});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}

function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

$(document).ready(function () {
    var divHeight = $('.splitsearch-body').height();
    $('.split-btn').css('height', divHeight + 'px');
});

function getPaymentTypes() {
    var pay_mode = jQuery("#od_pay_mode").val();
    var arr={};
    arr['service'] = "paymenttype";
    arr['opcode'] = "getPaymentTypes";
    arr['payment_mode'] = pay_mode;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function(resObj){
        if(resObj.Success=='True') {
            var result = resObj['Data'];
            if(result.length>0){
                jQuery("#od_pay_type").empty();
                jQuery("#od_pay_type").append('<option value="0">'+PLEASE_SELECT+' </option>');
                jQuery.each(result,function (key,value) {
                    jQuery("#od_pay_type").append('<option value="'+value['paymenttypeunkid']+'">'+value['paymenttype']+'</option>');
                });
            }
        }
    });
}
function cancelOrder(id,module){
    alertify.confirm("Are you sure? You want to cancel this order.", function (e) {
        if (e) {
            var arr = {};
            arr['service']= 'orders';
            arr['opcode']= 'cancelOrder';
            arr['id']= id;
            arr['module']= module;
            var str = urlencode(JSON.stringify(arr));
            if (arr['id'] != '') {
                HttpSendRequest(str,function(data){
                    if (data['Success'] == "True") {
                        refreshlist();
                        alertify.success(data['Message']);
                    }
                    else{
                        alertify.error(data['Message']);
                    }
                });
            }
        } else {
            return false;
        }
    });
}