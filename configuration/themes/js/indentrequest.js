var date = new Date();

var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

calculateTotalAmount();

var editid = "{{id}}";

if (editid > 0) {
    getindentrecords();
    jQuery("#sel_location").removeClass('invalid');

}
else {
    checklocationnotnull();
}

function checklocationnotnull() {
    var location = jQuery("#sel_location").val();

    if (location == '' || location == 0) {
        jQuery("#sel_location").addClass('invalid');
        $('[data-toggle="tooltip"], .tooltip').tooltip('show');
        disableelement();
    }
    else {
        jQuery("#sel_location").removeClass('invalid');
        enablelments();
    }
}

function disableelement() {
    jQuery("#sel_rm_cat").val(0);
    jQuery("#sel_rm").val(0);
    jQuery("#qty").val('');
    jQuery("#irate").val('');
    jQuery("#sel_iunit").val(0);
    jQuery("#sel_rm_cat").prop("disabled", true);
    jQuery("#sel_rm").prop("disabled", true);
    jQuery("#qty").prop("disabled", true);
    jQuery("#sel_iunit").prop("disabled", true);
    jQuery("#irate").prop("disabled", true);

}

function enablelments() {
    $('[data-toggle="tooltip"], .tooltip').tooltip('destroy');
    jQuery("#sel_rm_cat").prop("disabled", false);
    jQuery("#sel_rm").prop("disabled", false);
    jQuery("#qty").prop("disabled", false);
    jQuery("#sel_iunit").prop("disabled", false);
    jQuery("#irate").prop("disabled", false);
    jQuery("#sel_rm_cat").val(0);

    jQuery("#qty").val('');
    jQuery("#irate").val('');
    jQuery('#sel_rm').empty();
    jQuery('#sel_rm').append('<option value="0">Please Select Raw Material</option>');
    jQuery("#sel_iunit").empty();
    jQuery('#sel_iunit').append('<option value="0">Please Select Unit</option>');
}

jQuery('.date').datepicker({
    autoclose: true,
    startDate: date,
    todayHighlight: true,
    format: '{{ jsdateformat }}'
});

$('.date').datepicker('setDate', today);

function select_raw_material(rawmaterialid) {
    var rawcatid = jQuery("#sel_rm_cat").val();
    jQuery('#sel_rm').empty();
    jQuery('#sel_rm').append('<option value="0">Please Select Raw Material</option>');
    jQuery("#sel_iunit").empty();
    jQuery('#sel_iunit').append('<option value="0">Please Select Unit</option>');
    jQuery("#qty").val('');
    jQuery("#irate").val('');
    jQuery("#itotal").val('');
    var arr = {};
    arr['service'] = "forwardindent";
    arr['opcode'] = "getrawmaterialoncategory";
    arr['rawcatid'] = rawcatid;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function (data) {
        var ObjData_table = data['Data'];
        for (var i = 0; i < ObjData_table.length; i++) {
            jQuery('#sel_rm').append('<option  value="' + ObjData_table[i]['id'] + '">' + ObjData_table[i]['name'] + ' </option>');
        }
        if (rawmaterialid != '') {
            jQuery("#sel_rm").val(rawmaterialid);
        }
    });
}

function getunit(Unit, rate) {

    var rawid = jQuery("#sel_rm").val();
    var locationid = jQuery("#sel_location").val();
    jQuery('#sel_iunit').empty();
    jQuery('#sel_iunit').append('<option value="0">Please Select Unit</option>');

    jQuery("#qty").val('');
    jQuery("#irate").val('');
    jQuery("#itotal").val('');


    if (rawid != '' && locationid != '') {
        var arr = {};
        arr['service'] = "forwardindent";
        arr['opcode'] = "getUnitRate";
        arr['rawid'] = rawid;
        arr['locationid'] = locationid;
        var str = urlencode(JSON.stringify(arr));
        HttpSendRequest(str, function (data) {
            var ObjData_table = data['Data'];
            for (var i = 0; i < ObjData_table.length; i++) {
                jQuery('#sel_iunit').append('<option  value="' + ObjData_table[i]['unitunkid'] + '" data-attr="' + ObjData_table[i]['unit'] + '">' + ObjData_table[i]['name'] + ' </option>');
                if (Unit != '') {
                    jQuery("#sel_iunit").val(Unit);
                }
            }
            if (rate == '') {
                if (ObjData_table.length > 0 && ObjData_table[0].rateperunit != '') {
                    jQuery("#setrateonunit").val(ObjData_table[0].rateperunit);
                    countrowamount();
                }
            }
        });
        if (rate != '') {
            jQuery("#setrateonunit").val(rate);
            countrowamount();
        }
    }
}

function setunitrate() {
    var unitval = jQuery('#sel_iunit').find(':selected').attr('data-attr');
    var rate = jQuery("#setrateonunit").val();
    var finalrate = unitval * rate;
    jQuery("#irate").val(finalrate);
    countrowamount();
}

function resetfrmindent() {

    jQuery("#sel_location").prop("disabled", true);
    jQuery("#indate").prop("disabled", true);
    jQuery("#editrequest").prop("disabled", true);
    jQuery("#editrequest").hide();
    jQuery("#addrequest").show();
    jQuery("#cancelupdate").hide();
    jQuery(".invalid").removeClass('invalid');
    jQuery("#sel_rm_cat").val(0);
    jQuery("#sel_rm").empty();
    jQuery('#sel_rm').append('<option value="0">Please Select Raw Material</option>');
    jQuery("#sel_iunit").empty();
    jQuery('#sel_iunit').append('<option value="0">Please Select Unit</option>');
    jQuery("#qty").val('');
    jQuery("#irate").val('');
    jQuery("#itotal").val('');
}

$("#irate").keyup(function () {
    countrowamount();
});

$("#qty").keyup(function () {
    countrowamount();
});

function countrowamount() {
    var rate = jQuery("#irate").val();
    var quantity = jQuery("#qty").val();
    if (rate !== '' && quantity != '') {
        var total = rate * quantity;
        jQuery("#itotal").val(total);
    } else {
        jQuery("#itotal").val('');
    }
}

jQuery("#addrequest").click(function () {
    var uniqid = jQuery("#detailslist tr:last .getlasttr").html();
    if (uniqid == undefined) {
        uniqid = 1;
    }
    else {
        uniqid++;
    }
    if (validate_form('frmindent')) {
        showjQloading();
        jQuery("#btn_indent").prop('disabled', false);
        jQuery("#btn_indent").show();
        var indentno = jQuery("#indentnum").val();
        var indate = jQuery("#indate").val();
        var sel_location = jQuery("#sel_location").val();
        var selectedlocation = $("#sel_location option:selected").text();
        var sel_rm_cat = jQuery("#sel_rm_cat").val();
        var sel_rm = jQuery("#sel_rm").val();
        var selectedrm = $("#sel_rm option:selected").text();
        var qty = jQuery("#qty").val();
        var sel_iunit = jQuery("#sel_iunit").val();
        var selectedunit = $("#sel_iunit option:selected").text();
        var irate = jQuery("#irate").val();
        var itotal = jQuery("#itotal").val();

        hidejQloading();
        $('#detailslist').append('<tr id= "abc_' + uniqid + '">' +
            '<td align="left" style="display: none" id="indentno_' + uniqid + '">' + indentno + '</td>' +
            '<td align="left" class="getlasttr"id="eindentno_' + uniqid + '">' + uniqid + '</td>' +
            '<td align="left" style="display: none" id="eindate_' + uniqid + '">' + indate + '</td>' +
            '<td align="left" style="display: none" id="esel_location_' + uniqid + '">' + sel_location + '</td>' +
            '<td align="left" id="eselectedrm_' + uniqid + '">' + selectedrm + '</td>' +
            '<td align="left" style="display: none" id="esel_rm_cat_' + uniqid + '">' + sel_rm_cat + '</td>' +
            '<td align="left" style="display: none"  id="esel_iunit_' + uniqid + '">' + sel_iunit + '</td>' +
            '<td align="left" style="display: none" id="esel_rm_' + uniqid + '">' + sel_rm + '</td>' +
            '<td align="left" id="eqty_' + uniqid + '">' + qty + '</td>' +
            '<td align="left" id="eselectedunit_' + uniqid + '">' + selectedunit + '</td>' +
            '<td align="left" id="eirate_' + uniqid + '">' + irate + '</td>' +
            '<td align="left" id="eitotal_' + uniqid + '">' + itotal + '</td>' +
            '<td align="left" style="display: none" id="elocation_' + uniqid + '">' + selectedlocation + '</td>' +
            '<td align="left"  style="display: none" id="indentrequnkid_' + editid + '">' + 0 + '</td>' +
            '<td align="left" id="deletepopup_' + uniqid + '"><a class="update_ico"  onclick="removefrompopup(' + uniqid + ')" >' +
            '<i class="fa fa-trash-o"></i></a></td>' +
            '<td align="left" id="editpopup_' + uniqid + '"><a class="update_ico"  onclick="editfrompopup(' + uniqid + ')" >' +
            '<i class="fa fa-pencil"></i></a></td>' +
            '</tr>');
        resetfrmindent();
        calculateTotalAmount();
    }
    else {
        if (jQuery("#indate").val() == '')
            jQuery("#indate").addClass('invalid');
        else
            alertify.alert(lblerrormsg,lblok);
    }

});

jQuery("#editrequest").click(function () {
    if (validate_form('frmindent')) {
        $('.invalid').removeClass('invalid');
        var uniqid = jQuery("#editrequest").val();
        var indentno = jQuery("#indentnum").val();
        var indate = jQuery("#indate").val();
        var sel_location = jQuery("#sel_location").val();
        var selectedlocation = $("#sel_location option:selected").text();
        var sel_rm_cat = jQuery("#sel_rm_cat").val();
        var sel_rm = jQuery("#sel_rm").val();
        var selectedrm = $("#sel_rm option:selected").text();
        var qty = jQuery("#qty").val();
        var sel_iunit = jQuery("#sel_iunit").val();
        var selectedunit = $("#sel_iunit option:selected").text();
        var irate = jQuery("#irate").val();
        var itotal = jQuery("#itotal").val();
        jQuery("#eindate_" + uniqid).html(indate);
        jQuery("#esel_location_" + uniqid).html(sel_location);
        jQuery("#elocation_" + uniqid).html(selectedlocation);
        jQuery("#esel_rm_cat_" + uniqid).html(sel_rm_cat);
        jQuery("#eselectedrm_" + uniqid).html(selectedrm);
        jQuery("#esel_rm_" + uniqid).html(sel_rm);
        jQuery("#esel_iunit_" + uniqid).html(sel_iunit);
        jQuery("#eselectedunit_" + uniqid).html(selectedunit);
        jQuery("#eqty_" + uniqid).html(qty);
        jQuery("#eirate_" + uniqid).html(irate);
        jQuery("#eitotal_" + uniqid).html(itotal);
        resetfrmindent();
        calculateTotalAmount();
    }
    else{
        alertify.alert("Please Enter all the Details");
    }
});

function editfrompopup(uniqid) {
    $('.invalid').removeClass('invalid');
    jQuery("#editrequest").prop("disabled", false);
    jQuery("#editrequest").show();
    jQuery("#cancelupdate").show();
    jQuery("#addrequest").hide();
    jQuery("#editrequest").val(uniqid);
    var eindate = jQuery("#eindate_" + uniqid).html();
    var esel_location = jQuery("#esel_location_" + uniqid).html();
    var esel_rm_cat = jQuery("#esel_rm_cat_" + uniqid).html();
    var esel_rm = jQuery("#esel_rm_" + uniqid).html();
    var esel_iunit = jQuery("#esel_iunit_" + uniqid).html();
    var eqty = jQuery("#eqty_" + uniqid).html();
    var eirate = jQuery("#eirate_" + uniqid).html();
    var eitotal = jQuery("#eitotal_" + uniqid).html();
    jQuery("#indate").val(eindate);
    jQuery("#sel_location").val(esel_location);
    jQuery("#sel_rm_cat").val(esel_rm_cat);
    select_raw_material(esel_rm);
    setTimeout(function () {
        jQuery.when(getunit(esel_iunit, eirate)).then(
            jQuery("#qty").val(eqty),
            jQuery("#irate").val(eirate),
            jQuery("#itotal").val(eitotal)
        )
    }, 50);
    calculateTotalAmount();
}

function removefrompopup(uniqid) {
    $("#abc_" + uniqid).remove();
    calculateTotalAmount();
    resetnumbers(uniqid);
}

function calculateTotalAmount() {
    var TotalAmount = 0;
    $('#detailslist tr').each(function () {
        if ($(this).find("td:first").length > 0) {
            var Amount = $(this).find("td:eq(11)").html();
            TotalAmount += parseFloat(Amount);
        }
    });
    if (TotalAmount == '')
        jQuery("#totalamount").val('0');
    else
        jQuery("#totalamount").val(TotalAmount);
}

var checkopened = 0;
jQuery("#btn_indent").click(function () {
    if (checkopened == 0) {
        var indentrecords = {};
        var pindate;
        var premarks = jQuery("#remarks").val();
        var plocationid;
        var TotalAmount = jQuery("#totalamount").val();
        var id = jQuery("#getid").val();
        $('#detailslist tr').each(function () {
            if ($(this).find("td:first").length > 0) {
                var pin = $(this).find("td:eq(0)").html();
                var pindentno = $(this).find("td:eq(1)").html();
                pindate = $(this).find("td:eq(2)").html();
                plocationid = $(this).find("td:eq(3)").html();
                var pcatid = $(this).find("td:eq(5)").html();
                var prawmaterialid = $(this).find("td:eq(7)").html();
                var pqty = $(this).find("td:eq(8)").html();
                var punitid = $(this).find("td:eq(6)").html();
                var prate = $(this).find("td:eq(10)").html();
                var ptotal = $(this).find("td:eq(11)").html();
                var indentrequnkid = $(this).find("td:eq(13)").html();
                indentrecords[pindentno] = {};
                indentrecords[pindentno]['pin'] = pin;
                indentrecords[pindentno]['pindentno'] = pindentno;
                indentrecords[pindentno]['pcatid'] = pcatid;
                indentrecords[pindentno]['prawmaterialid'] = prawmaterialid;
                indentrecords[pindentno]['pqty'] = pqty;
                indentrecords[pindentno]['punitid'] = punitid;
                indentrecords[pindentno]['prate'] = prate;
                indentrecords[pindentno]['ptotal'] = ptotal;
                indentrecords[pindentno]['indentrequnkid'] = indentrequnkid;
            }
        });
        var arr = {};
        arr['service'] = "forwardindent";
        arr['opcode'] = "addindentrequest";
        arr['indentrecords'] = indentrecords;
        arr['totalamount'] = TotalAmount;
        arr['indate'] = pindate;
        arr['premarks'] = premarks;
        arr['plocationid'] = plocationid;
        arr['id'] = id;
        var str = urlencode(JSON.stringify(arr));
        HttpSendRequest(str, function (data) {
            hidejQloading();
            if (data['Success'] == 'True') {
                closeindentmodal();
                alertify.success(data['Message']);

                refreshaddedit();
                refreshlist();
            } else {
                alertify.error(data['Message']);
            }
            return false;
        });
    }
    checkopened++;

});

function getindentrecords() {
    var id = jQuery("#getid").val();
    jQuery("#btn_indent").prop("disabled", false);
    onedit();
    var arr = {};
    arr['service'] = 'forwardindent';
    arr['opcode'] = 'getIndentRecordsonID';
    arr['id'] = id;
    jQuery(".edit_hide").hide();
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function (recObj) {
        if (recObj.Success == 'True') {

            var indent = recObj.indent;
            var indentdetail = recObj.indentdetail;
            jQuery("#indentnum").val(indent.indent_doc_num);
            jQuery("#indate").val(indent.indentformated_date);
            jQuery("#sel_location").val(indent.recstoreid);
            jQuery("#remarks").val(indent.remarks);
            jQuery("#totalamount").val(indent.totalamount);

            jQuery.each(indentdetail, function (key, value) {
                var editid = jQuery("#detailslist tr:last .getlasttr").html();

                if (editid == undefined) {
                    editid = 1;
                }
                else {
                    editid++;
                }

                $('#detailslist').append('<tr id= "abc_' + editid + '">' +
                    '<td align="left" style="display: none" id="indentno_' + editid + '">' + value.lnkindentid + '</td>' +
                    '<td align="left" class="getlasttr"id="eindentno_' + editid + '">' + editid + '</td>' +
                    '<td align="left" style="display: none" id="eindate_' + editid + '">' + indent.indentformated_date + '</td>' +
                    '<td align="left" style="display: none" id="esel_location_' + editid + '">' + indent.recstoreid + '</td>' +
                    '<td align="left" id="eselectedrm_' + editid + '">' + value.selectedrm + '</td>' +
                    '<td align="left" style="display: none" id="esel_rm_cat_' + editid + '">' + value.lnkrawcatid + '</td>' +
                    '<td align="left" style="display: none"  id="esel_iunit_' + editid + '">' + value.lnkunitid + '</td>' +
                    '<td align="left" style="display: none" id="esel_rm_' + editid + '">' + value.lnkrawid + '</td>' +
                    '<td align="left" id="eqty_' + editid + '">' + value.qty + '</td>' +
                    '<td align="left" id="eselectedunit_' + editid + '">' + value.selectedunit + '</td>' +
                    '<td align="left" id="eirate_' + editid + '">' + value.rate + '</td>' +
                    '<td align="left" id="eitotal_' + editid + '">' + value.total + '</td>' +
                    '<td align="left"  style="display: none" id="elocation_' + editid + '">' + indent.storename + '</td>' +
                    '<td align="left"  style="display: none" id="indentrequnkid_' + editid + '">' + value.indentrequnkid + '</td>' +
                    '<td align="left" ><a class="update_ico" id="deletepopup_' + editid + '" onclick="removefrompopup(' + editid + ')" >' +
                    '<i class="fa fa-trash-o"></i></a></td>' +
                    '<td align="left" ><a class="update_ico" id="editpopup_' + editid + '" onclick="editfrompopup(' + editid + ')" >' +
                    '<i class="fa fa-pencil"></i></a></td>' +
                    '</tr>');
                calculateTotalAmount();
            });
        }
    });
}

function onedit() {
    jQuery("#sel_location").prop("disabled", true);
    jQuery("#indate").prop("disabled", true);
    jQuery("#editrequest").prop("disabled", true);
    jQuery("#editrequest").hide();
    jQuery("#addrequest").show();
    jQuery("#addrequest").show();
}

function resetnumbers(id) {
    var lastid = jQuery("#detailslist tr:last .getlasttr").html();

    if (lastid != undefined) {
        for (var i = id; i <= lastid; i++) {
            var j = i - 1;
            jQuery("#abc_" + i).attr("id", "abc_" + j);
            jQuery("#indentno_" + i).attr("id", "indentno_" + j);
            jQuery("#eindentno_" + i).html(j);
            jQuery("#eindentno_" + i).attr("id", "eindentno_" + j);
            jQuery("#eindate_" + i).attr("id", "eindate_" + j);
            jQuery("#esel_location_" + i).attr("id", "esel_location_" + j);
            jQuery("#eremarks_" + i).attr("id", "eremarks_" + j);
            jQuery("#eselectedrm_" + i).attr("id", "eselectedrm_" + j);
            jQuery("#esel_rm_cat_" + i).attr("id", "esel_rm_cat_" + j);
            jQuery("#esel_iunit_" + i).attr("id", "esel_iunit_" + j);
            jQuery("#esel_rm_" + i).attr("id", "esel_rm_" + j);
            jQuery("#eqty_" + i).attr("id", "eqty_" + j);
            jQuery("#eselectedunit_" + i).attr("id", "eselectedunit_" + j);
            jQuery("#eirate_" + i).attr("id", "eirate_" + j);
            jQuery("#eitotal_" + i).attr("id", "eitotal_" + j);
            jQuery("#elocation_" + i).attr("id", "elocation_" + j);
            jQuery("#deletepopup_" + i).attr("onclick", "removefrompopup(" + j + ")");
            jQuery("#editpopup_" + i).attr("onclick", "editfrompopup(" + j + ")");
            jQuery("#deletepopup_" + i).attr("id", "deletepopup_" + j);
            jQuery("#editpopup_" + i).attr("id", "editpopup_" + j);
        }
    }
    else {

        if (editid < 0 || editid == '') {

            $('[data-toggle="tooltip"]').tooltip();
            jQuery("#sel_rm_cat").prop("disabled", false);
            jQuery("#sel_location").prop("disabled", false);
            jQuery("#sel_rm").prop("disabled", false);
            jQuery("#qty").prop("disabled", false);
            jQuery("#sel_iunit").prop("disabled", false);
            jQuery("#irate").prop("disabled", false);
            jQuery("#sel_rm_cat").val(0);
            jQuery("#qty").val('');
            jQuery("#irate").val('');
            jQuery('#sel_rm').empty();
            jQuery('#sel_rm').append('<option value="0">Please Select Raw Material</option>');
            jQuery("#sel_iunit").empty();
            jQuery('#sel_iunit').append('<option value="0">Please Select Unit</option>');
        }
    }

}

function closeindentmodal() {
    var modalid = jQuery("#indent_modal").parent().attr("modal_id");
    closeModal(modalid);
}

