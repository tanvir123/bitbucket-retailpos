function updateAttributes(unithash) {

    var modifier = jQuery('#txtmodifier_' + unithash).val();
    if (modifier == "0") {
        return false;
    }
    var modifier_txt = jQuery('#txtmodifier_' + unithash + " option:selected").text();
    var req = {};
    req['service'] = 'menu_item';
    req['opcode'] = 'getmodifieritem';
    req['modifierid'] = modifier;
    var str = JSON.stringify(req);
    var min = modifierobj[modifier]['min'];
    var max = modifierobj[modifier]['max'];
    var sale_amount = modifierobj[modifier]['sale_amount'];
    var is_included = modifierobj[modifier]['is_included'];

    HttpSendRequest(str, function (data) {
        if (data['Success'] == 'True') {
            var rec = data['Data'];
            var card = '<div id="modifier_' + unithash + '_' + modifier + '" data-included="'+is_included+'" class="card" style="margin-top:15px"><div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-one_' + unithash + '_' + modifier + '" href="#collapse-one_' + unithash + '_' + modifier + '" style="cursor: pointer">';
            card += '<div class="card-title"> <span id="frmtitle_' + unithash + '_' + modifier + '" class="title">' + modifier_txt + '</span> <input type="hidden" id="sub_modifier_item_' + unithash + '_' + modifier + '" name="sub_modifier_item_' + unithash + '_' + modifier + '"> </div>';
            card += '<a mod="' + modifier + '" unit="' + unithash + '" onclick="closemod(this)" style="float:right;cursor: pointer;font-size: 15px;" class="card-title"> <i class="fa fa-close"></i></a></div><div class="card-body panel-collapse collapse out in" id="collapse-one_' + unithash + '_' + modifier + '"><div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft" for="name">'+QUNTITY+'</label>';
            card += '<div class="col-sm-9"><div class="controls"><input id="min_' + unithash + '_' + modifier + '" name="min_' + unithash + '_' + modifier + '" class="integer form-control"  value="' + min + '" type="text" placeholder="'+MIN+'" onkeypress="return isFloat(event,this.value)" required="" style="width:47%;margin-right:5%;display:inline-block" maxlength="10">';
            card += '<input id="max_' + unithash + '_' + modifier + '" name="max_' + unithash + '_' + modifier + '" class="integer form-control" type="text" value="' + max + '" placeholder="'+MAX+'" onkeypress="return isFloat(event,this.value)" required="" style="width:47%;display:inline-block" maxlength="10">';
            card += '</div></div></div>';
            if (is_included == 0) {

                card += '<div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft ">'+SALE_AMOUNT+'</label>';
                card += '<div class="col-sm-9"><div class="controls">';
                card += '<select name="mounit_' +unithash+'_' + modifier + '" class="form-control" id="mounit_' +unithash+'_' + modifier + '" onchange=getRate("'+unithash+'",'+ modifier +'); style="width:47%;margin-right:5%;display:inline-block">';
                card += '<option class="optmounit_' + unithash + '_' + modifier + '" disabled="disabled" selected="selected" value="0">Select unit</option>';
                jQuery.each(modifierunitobj, function(key, value) {
                    if(value['modifierunkid']==modifier && value['unitunkid']!=null) {
                        card += '<option class="optmounit_' + unithash + '_' + modifier + '" data-rate="'+value['rate']+'" value="' + value['unitunkid'] + '">' + value['itemunit'] + '</option>';
                    }
                });
                card += '</select>';
                card += '<input id="sale_amount_' + unithash + '_' + modifier + '" name="sale_amount_' + unithash + '_' + modifier + '" class="decimal form-control" maxlength="10" type="text" placeholder="'+SALE_AMOUNT+'" onkeypress="return isFloat(event,this.value)" required="" style="width:47%;display:inline-block"></div>';
            }
            else {
                card += '<div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft ">'+MODIFIER_ITEM+'</label>';
                card += '<div class="col-sm-9"><select id="modifier_sel_' + unithash + '_' + modifier + '" class="Modifier_Items_' + unithash + '_' + modifier + ' form-control" multiple="multiple" style="width:100%">';
                var arr = {};
                var modiitem = rec['modifieritem'];
                for (var i = 0; i < modiitem.length; i++) {
                    var hashkey = modiitem[i]['hashkey'];
                    var itemname = modiitem[i]['itemname'];
                    card += '<option class="option_moditem_'+ unithash + '_' + hashkey + '" value="' + hashkey + '">' + itemname + '</option>';
                    arr[hashkey] = {};
                    arr[hashkey]['name'] = itemname;
                    arr[hashkey]['desc'] = modiitem[i]['long_desc'];
                    arr[hashkey][itemname] = {};
                    jQuery.each(rec['modifieritm_unit'], function(key, value) {
                        if(value['hashkey']==hashkey) {
                            if(value['unitunkid']!=null) {
                                arr[hashkey][itemname][key] = {};
                                arr[hashkey][itemname][key]['iunitid'] = value['unitunkid'];
                                arr[hashkey][itemname][key]['iunitname'] = value['itemunit'];
                                arr[hashkey][itemname][key]['irate'] = value['rate'];
                            }
                        }
                    });
                }
                modifieritemobj[modifier] = arr;
                card += '</select></div></div><div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft "></label>';
                card += '<div class="col-sm-9"><button mod="' + modifier + '" unit="' + unithash + '" type="button" class="button btn" style="margin-right: 10px" onclick="selectAllAttributes(this);">'+SELECT_ALL+'</button><button style="margin-right: 10px" mod="' + modifier + '" unit="' + unithash + '" type="button" class="button btn" onclick="selectNoneAttributes(this);">'+SELECT_NONE+'</button><button  style="float: right" mod="' + modifier + '" unit="' + unithash + '" type="button" class="btn btn-primary" onclick="loadmodifieritems(this);">'+LOAD+'</button>';
                card += '<input type="hidden" id="sub_modifier_'+unithash +'_' + modifier + '" name="sub_modifier_'+unithash +'_' + modifier + '" /><input type="hidden" id="old_modifier_' + unithash + '_' + modifier + '" name="old_modifier_' + unithash + '_' + modifier + '" />';
                card += '</div></div><div id="modifier_item_div_' + unithash + '_' + modifier + '"></div>';
            }
            card += '</div></div>';

            jQuery('#modifier_list_' + unithash + '').append(card);
            jQuery('#modifier_sel_' + unithash + '_' + modifier).select2();
            var bid_modifier = jQuery("#bind_modifiers_" + unithash).val();
            if (bid_modifier == "")
                jQuery("#bind_modifiers_" + unithash).val(modifier);
            else
                jQuery("#bind_modifiers_" + unithash).val(bid_modifier + "," + modifier);

            jQuery(".option_" + unithash + "_" + modifier).attr("disabled", "disabled");
            jQuery('#txtmodifier_' + unithash).val('0');
        }
    });
}
function loadmodifieritems(current)
{
    var modifier = jQuery(current).attr('mod');
    var unit = jQuery(current).attr('unit');
    var sub_modifier = jQuery('#modifier_sel_'+unit+'_'+modifier).val();

    var old_modifier = jQuery('#old_modifier_'+unit+'_'+modifier).val();
    var oldmodiDetail=old_modifier.split(',');
    var mdifference = jQuery(sub_modifier).not(oldmodiDetail).get();

    var sub_modifier_obj = JSON.stringify(sub_modifier);
    jQuery("#sub_modifier_"+unit+"_"+modifier).val(sub_modifier_obj);
    var modifier_item_str = '';
    var modifierobj_sub = modifieritemobj[modifier];

    //Delete
    var newmDiff=jQuery(oldmodiDetail).not(sub_modifier).get();
    if(newmDiff!=null){
        for (var i=0;i<newmDiff.length;i++) {
            var mhashkey = newmDiff[i];
            jQuery("#modifier_item_"+unit+"_"+mhashkey).remove();
        }
    }

    if (mdifference!=null) {
        for (var i=0;i<mdifference.length;i++) {
            var moditem_hash = mdifference[i];
            var itemname=modifierobj_sub[moditem_hash]['name'];
            var iunit_data=modifierobj_sub[moditem_hash][itemname];

            if (modifier_item_str=='')
                modifier_item_str += moditem_hash;
            else
                modifier_item_str += ","+moditem_hash;

            var card='<div id="modifier_item_'+unit+'_'+moditem_hash+'" class="col-xs-4" style="margin-bottom: 15px;"><div class="card">';
            card +='<div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-item_' + unit + '_' + moditem_hash + '" href="#collapse-item_' + unit + '_' + moditem_hash + '" style="cursor: pointer">';
            card +='<div class="card-title"> <span id="frmtitle_'+unit+'_'+moditem_hash+'" class="title">'+modifierobj_sub[moditem_hash]['name']+'</span>';
            card +='</div></div><div class="card-body panel-collapse collapse out in" id="collapse-item_' + unit + '_' + moditem_hash + '"><div class="form-group clearfix">';
            card +='<div><div class="controls">';
            card +='<select name="mounit_' +unit+'_' + moditem_hash + '" class="form-control" id="mounit_' +unit+'_' + moditem_hash + '" onchange=getRate("'+unit+'",'+ moditem_hash +'); style="width:48%;margin-right:5px;display:inline-block">';
            card += '<option value="0" selected="selected" class="optmounit_' + unit + '_' + moditem_hash + '" disabled="disabled">'+LABEL_PLEASE_SELECT+'</option>';
            jQuery.each(iunit_data, function(key, value) {
                    card += '<option class="optmounit_' + unit + '_' + moditem_hash + '" data-rate="'+value['irate']+'" value="' + value['iunitid'] + '">' + value['iunitname'] + '</option>';
            });
            card += '</select>';
            card +='<input id="sale_amount_'+unit+'_'+moditem_hash+'" name="sale_amount_'+unit+'_'+moditem_hash+'" class="decimal form-control" type="text" placeholder="'+SALE_AMOUNT+'" maxlength="10" onkeypress="return isFloat(event,this.value)" required="" style="width:48%;display:inline-block" ></div></div></div>';
            card +='<div class="form-group clearfix"><div><div class="controls"><textarea id="description_'+unit+'_'+moditem_hash+'" name="description_'+unit+'_'+moditem_hash+'" class="form-control" type="text" placeholder="'+DESCRIPTION+'" required="">'+modifierobj_sub[moditem_hash]['desc']+'</textarea></div>';
            card +='</div></div></div></div></div>';

            jQuery('#modifier_item_div_'+unit+'_'+modifier).append(card);
        }
        jQuery('#old_modifier_'+unit+'_'+modifier).val(sub_modifier);
        jQuery('#sub_modifier_item_'+unit+'_'+modifier).val(sub_modifier);
    }
    else{
        jQuery('#sub_modifier_item_'+unit+'_'+modifier).val('');
        jQuery("#sub_modifier_"+unit+'_'+modifier).val('');
    }
}

function getRate(unit,modifier){
    var rate=jQuery('#mounit_'+unit+'_'+modifier).find(':selected').attr('data-rate');
    if(typeof rate !='undefined' && rate!=null && rate!='') {
        jQuery('#sale_amount_' + unit + '_' + modifier).val(rate);
    }
}

function selectAllAttributes(current)
{
    var modifier = jQuery(current).attr('mod');
    var unit = jQuery(current).attr('unit');
    jQuery('#modifier_sel_'+unit+'_'+modifier).find('option').each(function(){
       jQuery(this).prop('selected',true);
    });
    jQuery('#modifier_sel_'+unit+'_'+modifier).change();
    return false;
}

function selectNoneAttributes(current)
{
    var modifier = jQuery(current).attr('mod');
    var unit = jQuery(current).attr('unit');
    jQuery('#modifier_sel_'+unit+'_'+modifier).find('option').prop( 'selected',false );
    jQuery('#modifier_sel_'+unit+'_'+modifier).change();
    return false;
}

//Update
function loaditem_modlist(unit_hash,bind_modifier) {

    for (var i=0;i<bind_modifier.length;i++)
    {
        (function(i){
            var modifier = bind_modifier[i]['mod_hash'];
            var modifier_txt =  bind_modifier[i]['mod_name'];
            var req={};
            req['service'] = 'menu_item';
            req['opcode'] = 'getmodifieritem';
            req['modifierid'] = modifier;
            var str = JSON.stringify(req);
           
            var min = bind_modifier[i]['mod_min'];
            var max = bind_modifier[i]['mod_max'];
            var unit = bind_modifier[i]['mod_unit'];
            var sale_amount = bind_modifier[i]['mod_sale_amount'];
            var is_included = bind_modifier[i]['mod_is_included'];

            var sub_modifier = bind_modifier[i]['modifieritem'];
            var mi_hashkeys = bind_modifier[i]['mi_hashkeys'];
            HttpSendRequest(str,function(data){
                if(data['Success']=='True')
                {
                    var rec = data['Data'];
                    var card ='<div id="modifier_'+unit_hash +'_'+modifier +'" data-included="'+is_included+'" class="card" style="margin-top:15px"><div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-one_' + unit_hash + '_' + modifier + '" href="#collapse-one_' + unit_hash + '_' + modifier + '" style="cursor: pointer">';
                    card +='<div class="card-title"><span id="frmtitle_'+unit_hash +'_'+modifier+'" class="title">'+modifier_txt+'</span> <input type="hidden" id="sub_modifier_item_'+unit_hash +'_'+modifier+'" name="sub_modifier_item_'+unit_hash +'_'+modifier+'"> </div>';
                    card +='<a mod="'+modifier +'" unit="' + unit_hash + '" onclick="closemod(this)" style="float:right;cursor: pointer;font-size: 15px;" class="card-title"> <i class="fa fa-close"></i></a></div><div class="card-body panel-collapse out collapse" id="collapse-one_' + unit_hash + '_' + modifier + '"><div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft" for="name">'+QUNTITY+'</label>';
                    card +='<div class="col-sm-9"><div class="controls"><input id="min_'+unit_hash +'_'+modifier +'" name="min_'+unit_hash +'_'+modifier +'" class="integer form-control"  value="'+min+'" type="text" placeholder="'+MIN+'" required="" onkeypress="return isFloat(event,this.value)" style="width:47%;margin-right:5%;display:inline-block">';
                    card +='<input id="max_'+unit_hash +'_'+modifier +'" name="max_'+unit_hash +'_'+modifier +'" class="integer form-control" type="text" value="'+max+'" placeholder="'+MAX+'" onkeypress="return isFloat(event,this.value)" required="" style="width:47%;display:inline-block">';
                    card +='</div></div></div>';
                    if(is_included==0){
                        card += '<div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft ">'+SALE_AMOUNT+'</label>';
                        card += '<div class="col-sm-9"><div class="controls">';
                        card += '<select name="mounit_' +unit_hash+'_' + modifier + '" class="form-control" id="mounit_' +unit_hash+'_' + modifier + '" onchange=getRate("'+unit_hash+'",'+ modifier +'); style="width:47%;margin-right:5%;display:inline-block">';
                        card += '<option class="optmounit_' + unit_hash + '_' + modifier + '" disabled="disabled" selected="selected" value="0"> Select unit</option>';
                        jQuery.each(modifierunitobj, function(key, value) {
                            if(value['modifierunkid']==modifier && value['unitunkid']!=null) {
                                card += '<option class="optmounit_' + unit_hash + '_' + modifier + '" data-rate="'+value['rate']+'" value="' + value['unitunkid'] + '">' + value['itemunit'] + '</option>';
                            }
                        });
                        card += '</select>';
                        card += '<input id="sale_amount_'+unit_hash +'_'+modifier +'" name="sale_amount_'+unit_hash +'_'+modifier +'" class="decimal form-control" maxlength="10" value="'+sale_amount+'" type="text" placeholder="'+SALE_AMOUNT+'" onkeypress="return isFloat(event,this.value)" required="" style="width:47%;display:inline-block"></div>';
                    }
                    else {
                        card += '<div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft ">'+MODIFIER_ITEM+'</label>';
                        card += '<div class="col-sm-9"><select id="modifier_sel_' +unit_hash +'_'+ modifier + '" class="Modifier_Items_' +unit_hash +'_'+ modifier + ' form-control" multiple="multiple" style="width:100%">';
                        var arr = {};
                        var modiitem = rec['modifieritem'];
                        for (var i = 0; i < modiitem.length; i++) {
                            var hashkey = modiitem[i]['hashkey'];
                            var itemname = modiitem[i]['itemname'];
                            card += '<option class="option_moditem_'+ unit_hash + '_' + hashkey + '" class="" value="' + hashkey + '">' + itemname + '</option>';
                            arr[hashkey] = {};
                            arr[hashkey]['name'] = itemname;
                            arr[hashkey]['desc'] = '';
                            arr[hashkey][itemname] = {};
                            jQuery.each(rec['modifieritm_unit'], function(key, value) {
                                if(value['hashkey']==hashkey) {
                                    if(value['unitunkid']!=null) {
                                        arr[hashkey][itemname][key] = {};
                                        arr[hashkey][itemname][key]['iunitid'] = value['unitunkid'];
                                        arr[hashkey][itemname][key]['iunitname'] = value['itemunit'];
                                        arr[hashkey][itemname][key]['irate'] = value['rate'];
                                    }
                                }
                            });
                        }
                        modifieritemobj[modifier] = arr;
                        card += '</select></div></div><div class="form-group clearfix"><label class="col-sm-3 control-label textalignleft "></label>';
                        card += '<div class="col-sm-9"><button mod="' + modifier + '" unit="' + unit_hash + '" type="button" class="button btn" style="margin-right: 10px" onclick="selectAllAttributes(this);">'+SELECT_ALL+'</button><button style="margin-right: 10px" mod="' + modifier + '" unit="' + unit_hash + '" type="button" class="button btn" onclick="selectNoneAttributes(this);">'+SELECT_NONE+'</button><button  style="float: right" mod="' + modifier + '" unit="' + unit_hash + '" type="button" class="btn btn-primary" onclick="loadmodifieritems(this);">'+LOAD+'</button>';
                        card +=' <input type="hidden" id="sub_modifier_'+unit_hash +'_' + modifier + '" name="sub_modifier_'+unit_hash +'_' + modifier + '" /><input type="hidden" id="old_modifier_'+unit_hash +'_' + modifier + '" name="old_modifier_'+unit_hash +'_' + modifier + '" />';
                        card += '</div></div><div id="modifier_item_div_'+unit_hash +'_' + modifier + '"></div>';
                    }
                    card+='</div></div>';

                    jQuery('#modifier_list_'+unit_hash+ '').append(card);
                    jQuery("#mounit_"+unit_hash +'_'+modifier).val(unit);
                    jQuery('#modifier_sel_'+unit_hash +'_'+modifier).select2();
                    var bid_modifier = jQuery("#bind_modifiers_"+unit_hash).val();
                    if (bid_modifier=="")
                        jQuery("#bind_modifiers_"+unit_hash ).val(modifier);
                    else
                        jQuery("#bind_modifiers_"+unit_hash ).val(bid_modifier+","+modifier);
                    
                    jQuery(".option_"+unit_hash +'_'+modifier).attr("disabled","disabled");

                    //jQuery("#sub_modifier_"+unit_hash +'_'+modifier).val(mi_hashkeys);
                    jQuery("#modifier_item_div_"+unit_hash +'_'+modifier).empty();
                    var modifierobj_sub = modifieritemobj[modifier];

                    var modifier_item_str = '';
                    for (var i=0;i<sub_modifier.length;i++) {
                        var moditem_hash = sub_modifier[i]['mi_hashkey'];
                        var itemname=modifierobj_sub[moditem_hash]['name'];
                        var iunit_data=modifierobj_sub[moditem_hash][itemname];
                        if (modifier_item_str=='')
                            modifier_item_str += moditem_hash;
                        else
                            modifier_item_str +=","+moditem_hash;

                        var card='<div id="modifier_item_'+unit_hash +'_'+moditem_hash+'" class="col-xs-4" style="margin-bottom: 15px;"><div class="card">';
                        card +='<div class="card-header collapsed" data-toggle="collapse" data-target="#collapse-item_' + unit_hash + '_' + moditem_hash + '" href="#collapse-item_' + unit_hash + '_' + moditem_hash + '" style="cursor: pointer">';
                        card +='<div class="card-title"> <span id="frmtitle_'+unit_hash +'_'+moditem_hash+'" class="title">'+itemname+'</span>';
                        card +='</div></div><div class="card-body panel-collapse out collapse" id="collapse-item_' + unit_hash + '_' + moditem_hash + '"><div class="form-group clearfix">';
                        card +='<div><div class="controls">';
                        card +='<select name="mounit_' +unit_hash+'_' + moditem_hash + '" class="form-control" id="mounit_' +unit_hash+'_' + moditem_hash + '" onchange=getRate("'+unit_hash+'",'+ moditem_hash +'); style="width:48%;margin-right:5px;display:inline-block">';
                        card += '<option class="optmounit_' + unit_hash + '_' + sub_modifier + '" disabled="disabled" selected="selected" value="0">Select unit</option>';
                        jQuery.each(iunit_data, function(key, value) {
                            card += '<option class="optmounit_' + unit_hash + '_' + moditem_hash + '" data-rate="'+value['irate']+'" value="' + value['iunitid'] + '">' + value['iunitname'] + '</option>';
                        });
                        card += '</select>';
                        card +='<input id="sale_amount_'+unit_hash +'_'+moditem_hash+'" name="sale_amount_'+unit_hash +'_'+moditem_hash+'" class="decimal form-control" type="text" maxlength="10" placeholder="'+SALE_AMOUNT+'" onkeypress="return isFloat(event,this.value)" required="" style="width:47%;display:inline-block" value="'+sub_modifier[i]['mi_sale_amount']+'"></div></div></div>';
                        card +='<div class="form-group clearfix"><div><div class="controls"><textarea id="description_'+unit_hash +'_'+moditem_hash+'" name="description_'+unit_hash +'_'+moditem_hash+'" class="form-control" type="text" placeholder="'+DESCRIPTION+'" required="">'+sub_modifier[i]['mi_description']+'</textarea></div>';
                        card +='</div></div></div></div></div>';

                        jQuery(".option_moditem_"+unit_hash +'_'+moditem_hash).prop('selected',true);
                        jQuery('#modifier_item_div_'+unit_hash +'_'+modifier).append(card);
                        jQuery("#mounit_"+unit_hash +'_'+moditem_hash).val(sub_modifier[i]['mi_unit']);
                    }
                    jQuery('#modifier_sel_'+unit_hash +'_'+modifier).change();
                    jQuery("#sub_modifier_"+unit_hash +'_'+modifier).val(modifier_item_str);
                    jQuery('#sub_modifier_item_'+unit_hash +'_'+modifier).val(modifier_item_str);
                    jQuery('#old_modifier_'+unit_hash +'_'+modifier).val(modifier_item_str);
                }
            });
        })(i);
    }
}
function closemod(obj) {
    var  modifier = jQuery(obj).attr('mod');
    var  unit = jQuery(obj).attr('unit');
    var bid_modifier = jQuery("#bind_modifiers_"+unit).val();
    jQuery("#modifier_"+unit+"_"+modifier).remove();
    var ObjMod = bid_modifier.split(',');
    var i = ObjMod.indexOf(modifier);
    if(i != -1) {
        ObjMod.splice(i, 1);
    }
    var StrMod = ObjMod.toString();
    jQuery("#bind_modifiers_"+unit).val(StrMod);
    jQuery(".option_"+unit+"_"+modifier).removeAttr("disabled");
}

