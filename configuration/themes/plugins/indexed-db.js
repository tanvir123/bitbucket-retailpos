/* Title - Indexed-db.js
 Author - RJ Lumbhani
 Detail - Simplify the Use of indexed-db in client javascript language.*/

if(!isIndexedEnable()){
    alert('Your Browser is not supporting this system.');
}
function isIndexedEnable() {
    // In the following line, you should include the prefixes of implementations you want to test.
    window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    // DON'T use "var indexedDB = ..." if you're not in a function.
    // Moreover, you may need references to some window.IDB* objects:
    window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || {READ_WRITE: "readwrite"}; // This line should only be needed if it is needed to support the object's constants for older browsers
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
    // (Mozilla has never prefixed these objects, so we don't need window.mozIDB*)
    if (!window.indexedDB) {
        return 0;
    }
    else{
        return 1;
    }
}

function OpenNewDb(dbname,obj_data,callback) {
    var request = indexedDB.open(dbname);
    
    request.onerror = function(event) {
        alert("Not able to connect");
        return 0;
    };
    
    request.onupgradeneeded = function(event) {
        indexed_db = event.target.result;
        // Create an objectStore to hold information about data. And give key to access further

        obj_data.forEach(function(rec_ind) {

            var object = indexed_db.createObjectStore(rec_ind.tblname, { keyPath: rec_ind.key });
            // Not unique indexes.
            var non_u = rec_ind.nonunique_key;
            
            non_u.forEach(function(id) {
                object.createIndex(id, id, { unique: false });
            });
            
            // Unique indexes
            var uni_u = rec_ind.unique_key;
            uni_u.forEach(function(id) {
                object.createIndex(id, id, { unique: true });
            });
        });
        //callback(indexed_db);
    }
    request.onsuccess = function(event) {
        indexed_db = event.target.result;
        callback(indexed_db);
    }
}

function AddDataToDb(indexed_db,data,tablename,key,flag) {
    var obj_ret = [];
    indexed_db.transaction(tablename).objectStore(tablename).onerror = function(event){
        console.log('Got');
    }
    var transaction = indexed_db.transaction([tablename],"readwrite");
    var store = transaction.objectStore(tablename);
    var objectStoreRequest = store.clear();
    objectStoreRequest.onsuccess = function(event) {
        data.forEach(function(resource) {
            if (flag==1) {
                obj = { label:resource.itemname,category:resource.categoryunkid,id: resource.itemunkid, value: resource.itemname };
                obj_ret.push(obj);
            }
            else{
                obj = { label:resource.comboname,id: resource.hashkey, value: resource.comboname };
                obj_ret.push(obj);
            }
           
            store.add(resource);
        });
        
    };
    return obj_ret;
}

function getIDBobject(tablename, id, action) {
   var transaction = indexed_db.transaction([tablename], "readwrite");
   var myRecord;
   transaction.onerror = function(event) {
       alert('Transaction not opened due to error: ' + transaction.error );
       return 0;
   };
   var objectStore = transaction.objectStore(tablename);

   var objectStoreRequest = objectStore.get(id);
 
   objectStoreRequest.onsuccess = function(event) {
       myRecord = objectStoreRequest.result;
       action(myRecord);
   };
   objectStoreRequest.onerror = function(event) {
       alert('Transaction not opened due to error');
       return 0;
   }
}