<?php
class charges
{
    public $module='charges';
    public $log;
    private $language,$lang_arr,$default_lang_arr;
    public $jsdateformat;
    
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_charges');
    }

    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(92,$this->module);
            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(92);

            $jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $ObjChargesDao = new \database\chargesdao();
			$data = $ObjChargesDao->chargeslist(50,"0",'');

            $Objlocationdao = new \database\locationdao();
            $locationdata = $Objlocationdao->loadLocationInfo();

            $Zomatosetting = $OBJCOMMONDAO->getZomatosetting();

            $roundoff = \database\parameter::getParameter('digitafterdecimal');


            $template = $twig->loadTemplate('charges.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['datalist'] = $data;
            $senderarr['location'] = $locationdata;
            $senderarr['module'] = $this->module;
            $senderarr['jsdateformat'] = $jsdate;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['is_zomato'] = $Zomatosetting;
            $senderarr['roundoff'] = $roundoff;
//            $senderarr['user_type'] = CONFIG_USR_TYPE;
//            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
            $flag1 = \util\validate::check_numeric($data,array('amount'));
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            $flag3 = $flag4 = $flag5 = $flag6 = $flag7 = $flag8 = $flag9 = $flag10 = $flag11 = $flag12 = false;

            $flag2 = \util\validate::check_notnull($data,array('shortname','chargename','appliesfrom','amount'));

            if(isset($data['is_charge_always_applicable']) && $data['is_charge_always_applicable'] == 0)
            {
                $flag11 = \util\validate::check_notnull($data,array('charge_applicable_below_order_amount'));
                $flag12 = \util\validate::check_numeric($data,array('charge_applicable_below_order_amount'));

                if($flag11 == false || $flag12 == false)
                {
                    return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
                }
            }

           
            if($flag1=='true' && $flag2=="true"){
				$reqarr = array(
                            "shortname" => $data['shortname'],
                            "chargename"=> $data['chargename'],
                            "appliesfrom"=> $data['appliesfrom'],
                            "chargerule"=> $data['chargerule'],
                            "amount"=> $data['amount'],
                            "id"=> $data['id'],
                            "rdo_status"=>$data['rdo_status'],
                            "module" => $this->module,
                            "applicable_on" => ($data['applicable_on'])?$data['applicable_on']:0,
                            "charge_always_applicable" => isset($data['is_charge_always_applicable'])?1:0,
                            "charge_applicable_below_order_amount" => isset($data['charge_applicable_below_order_amount'])?$data['charge_applicable_below_order_amount']:NULL
                        );

                
                $ObjChargesDao = new \database\chargesdao();
                $data = $ObjChargesDao->addCharges($reqarr,$languageArr,$defaultlanguageArr);

                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
        }catch(Exception $e){
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }

    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
			$ObjChargesDao = new \database\chargesdao();
			$data = $ObjChargesDao->chargeslist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

	public function getChargeRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getChargeRec");
			$ObjChargesDao = new \database\chargesdao();
			$data = $ObjChargesDao->getChargeRec($data);
			return $data;
		}catch(Exception $e)
        {
			$this->log->logIt($this->module." - getChargeRec - ".$e);
			return false; 
		}
	}

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_charges;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }

    public function togglezomato($data)
    {
        try
        {
            $this->log->logIt($this->module.' - togglezomato');
            $ObjChargesDao = new \database\chargesdao();

            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);
            $res = $ObjChargesDao->togglezomato($data,$defaultlanguageArr);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - togglezomato - '.$e);
        }
    }

}
?>