<?php

class storereports
{
    public $module = 'storereports';
    public $log;
    public $jsdateformat;
    private $language, $lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(65,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(65);

            $Objvendordao = new \database\vendordao();
            $vendorData = $Objvendordao->getAllVendor();
            $vendorlist = json_decode($vendorData, 1);

            $jsdate = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            $template = $twig->loadTemplate('storereports.html');

            $ObjIdent = new \database\indentrequestdao();
            $storeData = $ObjIdent->getallstorelist();
            $storelist = json_decode($storeData, 1);


            $Objcategorydao = new \database\raw_categorydao();
            $catData = $Objcategorydao->getAllCategory();
            $catlist = json_decode($catData, 1);

            $m = date('m/Y');

            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
            $senderarr['jsdateformat'] = $jsdate;
            $senderarr['month_year'] = $m;
            $senderarr['category'] = $catlist['Data'];
            $senderarr['vendorlist'] = $vendorlist['Data'];
            $senderarr['storelist'] = $storelist['Data'];
            $current_date = \util\util::getLocalDate();
            $senderarr['current_date'] = $current_date;
            $senderarr['user'] = CONFIG_USR_TYPE;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];

            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function generatereport($data)
    {
        try {
            $this->log->logIt($this->module . ' - generatereport');
            $ObjReportsDao = new \database\storereportsdao();
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            if ($data['type'] == 1) {
                $indentlanguage = new \util\language('AuthorizeReceivedindent');
                $llang_arr = \common\staticlang::$AuthorizeReceivedindent;
                $languageArr=html_entity_decode(json_encode($indentlanguage->loadlanguage($llang_arr)),ENT_QUOTES);

                $report_data = $ObjReportsDao->getGRNRecords($data,json_decode($languageArr));
                if($report_data){
                    $report_data['default_langlist'] = json_decode($defaultlanguageArr);
                    $report_data['langlist'] = json_decode($languageArr);
                }
                $template = 'GRN';
                $width = '793px';
                $height = '1122px';
            }
            if ($data['type'] == 2) {
                $indentlanguage = new \util\language('AuthorizeReceivedindent');
                $llang_arr = \common\staticlang::$AuthorizeReceivedindent;
                $languageArr=html_entity_decode(json_encode($indentlanguage->loadlanguage($llang_arr)),ENT_QUOTES);
                $report_data = $ObjReportsDao->createdVoucherList($data,json_decode($languageArr));
                if($report_data){
                    $report_data['default_langlist'] = json_decode($defaultlanguageArr);
                    $report_data['langlist'] = json_decode($languageArr);
                }
                $template = 'issuedvoucher';
                $width = '793px';
                $height = '1122px';
            }
            if ($data['type'] == 3) {

                $indentlanguage = new \util\language('AuthorizeReceivedindent');
                $llang_arr = \common\staticlang::$AuthorizeReceivedindent;
                $languageArr=html_entity_decode(json_encode($indentlanguage->loadlanguage($llang_arr)),ENT_QUOTES);

                $report_data = $ObjReportsDao->itemStockledger($data,json_decode($languageArr));
                if($report_data){
                    $report_data['default_langlist'] = json_decode($defaultlanguageArr);
                   $report_data['langlist'] = json_decode($languageArr);
                }
                $template = 'itemstockledger';
                $width = '793px';
                $height = '1122px';
            }
            $PrintReport = new \common\printstorereport();
            $PrintReport->addTemplate($template);
            $PrintReport->addRecord($report_data);
            $data = $PrintReport->Request();

            return json_encode(array("Success" => "True", "Data" => $data,"Height"=>$height, "Width"=>$width));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - generatereport - ' . $e);
        }
    }
    public function exportreportData($data)
    {
        try {
            $this->log->logIt($this->module . ' - exportreportData');

            $dao = new \dao();
            $ObjReportsDao = new \database\storereportsdao();
            $this->loadLang();
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr=json_decode($defaultlanguageArr);
            $indentlanguage = new \util\language('AuthorizeReceivedindent');
            $llang_arr = \common\staticlang::$AuthorizeReceivedindent;
            $languageArr=html_entity_decode(json_encode($indentlanguage->loadlanguage($llang_arr)),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $vaoucher_no=$defaultlanguageArr->VOUCHER.' '.$defaultlanguageArr->NO;
            $GRNregisterheader=array(
                $languageArr->LANG2,
                $vaoucher_no,
                $languageArr->LANG19,
                $defaultlanguageArr->VENDOR,
                $languageArr->LANG26,
                $defaultlanguageArr->TAX,
                $defaultlanguageArr->DISCOUNT
            );

            $IssueRegisterheader=array(
                $languageArr->LANG9,
                $languageArr->LANG2,
                $vaoucher_no,
                $defaultlanguageArr->RECEIVING_STORE,
                $languageArr->LANG11
            );

            $ItemStockLedgerheader=array(
                $languageArr->LANG9,
                $languageArr->LANG4,
                $languageArr->LANG22,
                $languageArr->LANG13,
                $languageArr->LANG2,
                $languageArr->LANG23,
                $languageArr->LANG24,
                $languageArr->LANG25,
                $languageArr->LANG11
            );

            if ($data['type'] == 1) {
                $report_data = $ObjReportsDao->getGRNRecords($data,json_decode($languageArr));
                $dataArr = array();
                if($report_data) {
                    $cnt = 0;
                    foreach ($report_data['grnRecords'] as $key => $value){
                        $dataArr[$key]['grn_date'] = $value['grn_date'];
                        $dataArr[$key]['voucher_no'] = $value['voucher_no'];
                        $dataArr[$key]['grn_doc_num'] = $value['grn_doc_num'];
                        $dataArr[$key]['Vendor_name'] = $value['Vendor_name'];
                        $dataArr[$key]['totalamount'] = $value['totalamount'];
                        $dataArr[$key]['TA'] = $value['TA'];
                        $dataArr[$key]['DA'] = $value['DA'];
                        $cnt++;
                    }
                    $dataArr[$cnt]['grn_date']='';
                    $dataArr[$cnt]['voucher_no']='';
                    $dataArr[$cnt]['grn_doc_num']='';
                    $dataArr[$cnt]['Vendor_name']=$defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['totalamount']=str_replace( ',', '',$report_data['Grand_Total']);
                    $dataArr[$cnt]['TA']=str_replace( ',', '',$report_data['Tax_Total']);
                    $dataArr[$cnt]['DA']=str_replace( ',', '',$report_data['DIS_Total']);

                    $headers = $GRNregisterheader;
                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $headers;
                    $rec_arr['fromdate'] = $data['fromdate'];
                    $rec_arr['todate'] = $data['todate'];
                    $rec_arr['currency_symbol'] = $report_data['currency_sign'];
                    $rec_arr['reportname'] = $defaultlanguageArr->GOODRECEIPTNOTEREG;
                    $rec_arr['loc_name'] = $report_data['company_name'];
                    $rec_arr['loc_email'] = $report_data['email'];
                    $rec_arr['loc_phone'] = $report_data['phone'];
                    $rec_arr['filename'] = $defaultlanguageArr->GOODRECEIPTNOTEREG."_".date("Ymd").rand().'.csv';
                    return json_encode(array("Success"=>"True","Data"=>$rec_arr));
                }
                else {
                    return json_encode(array('Success' => 'False','Message' => $defaultlanguageArr->REC_NOT_FOUND));
                }
            }

            if ($data['type'] == 2) {
                $report_data = $ObjReportsDao->createdVoucherList($data,json_decode($languageArr));
                $dataArr = array();
                if($report_data) {
                    $cnt = 0;
                    foreach ($report_data['issuedetail'] as $key => $value){
                        $dataArr[$key]['sr'] = $cnt;
                        $dataArr[$key]['issueDate'] = $value['issueDate'];
                        $dataArr[$key]['issue_doc_num'] = $value['issue_doc_num'];
                        $dataArr[$key]['storename'] = $value['storename'];
                        $dataArr[$key]['totalamount'] = $value['totalamount'];
                        $cnt++;
                    }
                    $dataArr[$cnt]['sr']='';
                    $dataArr[$cnt]['issueDate']='';
                    $dataArr[$cnt]['issue_doc_num']='';
                    $dataArr[$cnt]['storename']=$defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['totalamount']=str_replace( ',', '',$report_data['Grand_total']);

                    $headers = $IssueRegisterheader;
                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $headers;
                    $rec_arr['fromdate'] = $data['ifromdate'];
                    $rec_arr['todate'] = $data['itodate'];
                    $rec_arr['currency_symbol'] = $report_data['currency_sign'];
                    $rec_arr['reportname'] = $defaultlanguageArr->ISSUEREGISTER;
                    $rec_arr['loc_name'] = $report_data['company_name'];
                    $rec_arr['loc_email'] = $report_data['email'];
                    $rec_arr['loc_phone'] = $report_data['phone'];
                    $rec_arr['filename'] = $defaultlanguageArr->ISSUEREGISTER."_".date("Ymd").rand().'.csv';
                    return json_encode(array("Success"=>"True","Data"=>$rec_arr));
                }
                else {
                    return json_encode(array('Success' => 'False','Message' => $defaultlanguageArr->REC_NOT_FOUND));
                }
            }
            if ($data['type'] == 3) {
                $report_data = $ObjReportsDao->itemStockledger($data,json_decode($languageArr));
                $dataArr = array();
                if($report_data) {
                    $icnt=0;
                    foreach ($report_data['Item'] as $key => $value){
                            $dataArr[$icnt]['sr'] = $icnt+1;
                            $dataArr[$icnt]['ItemName'] = $value['ItemName'];
                            $dataArr[$icnt]['Cat_name'] = $value['Cat_name'];
                            $dataArr[$icnt]['Unit_name'] = $value['Unit_name'];
                            if(isset($value['ItemDetail']) && $value['ItemDetail']!='') {
                                $inc=0;
                                foreach ($value['ItemDetail'] as $dkey => $dvalue) {
                                    if($inc==0) {
                                        $dataArr[$icnt]['INV_Date'] = $dvalue['INV_Date'];
                                        $dataArr[$icnt]['Stat'] = $dvalue['Stat'];
                                        if ($dvalue['inv_status'] == '1' || $dvalue['inv_status'] == '3') {
                                            $dataArr[$icnt]['inv_status'] = $dvalue['Vendor_business'];
                                        } elseif ($dvalue['inv_status'] == '2') {
                                            if ($dvalue['crdb'] == 'db') {
                                                $dataArr[$icnt]['inv_status'] = $dvalue['Store_name'] . ' -> ' . $dvalue['REL_Storename'];
                                            } else {
                                                $dataArr[$icnt]['inv_status'] = $dvalue['REL_Storename'] . ' -> ' . $dvalue['Store_name'];
                                            }
                                        } else {
                                            $dataArr[$icnt]['inv_status'] = '';
                                        }
                                        $dataArr[$icnt]['inventory'] = $dvalue['inventory'];
                                        $dataArr[$icnt]['Rate'] = $dvalue['Rate'];
                                        $inc++;
                                        $icnt++;
                                    }
                                    else{
                                        $dataArr[$icnt]['sr'] = '';
                                        $dataArr[$icnt]['ItemName'] = '';
                                        $dataArr[$icnt]['Cat_name'] = '';
                                        $dataArr[$icnt]['Unit_name'] = '';
                                        $dataArr[$icnt]['INV_Date'] = $dvalue['INV_Date'];
                                        $dataArr[$icnt]['Stat'] = $dvalue['Stat'];
                                        if ($dvalue['inv_status'] == '1' || $dvalue['inv_status'] == '3') {
                                            $dataArr[$icnt]['inv_status'] = $dvalue['Vendor_business'];
                                        } elseif ($dvalue['inv_status'] == '2') {
                                            if ($dvalue['crdb'] == 'db') {
                                                $dataArr[$icnt]['inv_status'] = $dvalue['Store_name'] . ' -> ' . $dvalue['REL_Storename'];
                                            } else {
                                                $dataArr[$icnt]['inv_status'] = $dvalue['REL_Storename'] . ' -> ' . $dvalue['Store_name'];
                                            }
                                        } else {
                                            $dataArr[$icnt]['inv_status'] = '';
                                        }
                                        $dataArr[$icnt]['inventory'] = $dvalue['inventory'];
                                        $dataArr[$icnt]['Rate'] = $dvalue['Rate'];
                                        $icnt++;
                                    }
                                }
                            }
                        $icnt++;
                    }
                    $dataArr[$icnt]['sr']='';
                    $dataArr[$icnt]['ItemName']='';
                    $dataArr[$icnt]['Cat_name']='';
                    $dataArr[$icnt]['Unit_name']='';
                    $dataArr[$icnt]['INV_Date']='';
                    $dataArr[$icnt]['Stat']='';
                    $dataArr[$icnt]['inv_status']='';
                    $dataArr[$icnt]['inventory']=$defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$icnt]['Rate']=str_replace( ',', '',$report_data['Grand_total']);

                    $headers = $ItemStockLedgerheader;
                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $headers;
                    $rec_arr['fromdate'] = isset($data['ifromdate'])?$data['ifromdate']:'';
                    $rec_arr['todate'] = isset($data['itodate'])?$data['itodate']:'';
                    $rec_arr['currency_symbol'] = $report_data['currency_sign'];
                    $rec_arr['reportname'] = $defaultlanguageArr->ITEMSTOCKLEDGER;
                    $rec_arr['loc_name'] = $report_data['company_name'];
                    $rec_arr['loc_email'] = $report_data['email'];
                    $rec_arr['loc_phone'] = $report_data['phone'];
                    $rec_arr['filename'] = $defaultlanguageArr->ITEMSTOCKLEDGER."_".date("Ymd").rand().'.csv';
                    return json_encode(array("Success"=>"True","Data"=>$rec_arr));
                }
                else {
                    return json_encode(array('Success' => 'False','Message' => $defaultlanguageArr->REC_NOT_FOUND));
                }
            }
        }
        catch (Exception $e) {
            $this->log->logIt($this->module . ' - exportreportData - ' . $e);
        }
    }

    public function dailyreport($data)
    {
        try {
            $this->log->logIt($this->module . ' - dailyreport');
            global $twig;
            global $commonurl;
            $ObjReportsDao = new \database\reportsdao();
            $report_data = $ObjReportsDao->getdailyrec($data);
            $PrintReport = new \common\printreport();
            $PrintReport->addTemplate('dailyrevenue');
            $PrintReport->addRecord($report_data);
            $data = $PrintReport->Request();
            return json_encode(array("Success" => "True", "Data" => $data));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - dailyreport - ' . $e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$storereports;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}

?>