<?php

/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 19/5/17
 * Time: 5:49 PM
 */
class reports
{
    public $module = 'reports';
    public $log;
    public $jsdateformat;
    private $language, $lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();

    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(22, $this->module);
            $ObjCategoryDao = new \database\menu_categorydao();

            $ObjMenuDao = new \database\menu_menudao();
            $menu_data = $ObjMenuDao->menulist('','','',1);
            $menulist = json_decode($menu_data,true);

            $CategoryList = $ObjCategoryDao->categorylist('', '', '', 1);
            $category_list = json_decode($CategoryList, 1);

            $ObjComboDao = new \database\combo_productdao();
            $ComboList = $ObjComboDao->comboproductlist('', '', '', 0);
            $combo_list = json_decode($ComboList, 1);

            $ObjRawDao = new \database\rawmaterialdao();
            $RawList = $ObjRawDao->rawmateriallist('', '', '', '1');
            $raw_list = json_decode($RawList, 1);

            $ObjItemDao = new \database\menu_itemdao();
            $Itemlist = $ObjItemDao->itemlist(50, 0, '');
            $item_list = json_decode($Itemlist, 1);

            $ObjRawcatDao = new \database\raw_categorydao();
            $Rawcatlist = $ObjRawcatDao->categorylist(50, 0, '', '1');
            $rawcat_list = json_decode($Rawcatlist, 1);

            $ObjModiDao = new \database\menu_modifierdao();
            $Modilist = $ObjModiDao->modifierlist(50, 0, '', '1');
            $modi_list = json_decode($Modilist, 1);

            $Objvendordao = new \database\vendordao();
            $vendorData = $Objvendordao->getAllVendor();
            $vendorlist = json_decode($vendorData, 1);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(22);

          /*  $ObjNochargeDao = new \database\nochargedao();
            $NoChargeList = $ObjNochargeDao->getnochargelist();
            $NoCharge_list = json_decode($NoChargeList, 1);*/

           /* $WaiterList = $OBJCOMMONDAO->waiterlist();
            $Waiter_list = json_decode($WaiterList, 1);*/

            $current_working_date = \database\parameter::getParameter('todaysdate');
            $shiftmanagement= \database\parameter::getParameter('cashdrawer_popup_at_login_logouttime');

            if (!$current_working_date || $current_working_date == '') {
                $current_working_date = \util\util::getlocalDate();
                \database\parameter::setParameter('todaysdate', $current_working_date);
            }
            $current_working_month = date('Y-m', strtotime($current_working_date));
            $current_working_year = date('Y', strtotime($current_working_date));
            $current_working_month = date('Y-m', strtotime($current_working_date));
            $jsdate = \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];

            $template = $twig->loadTemplate('reports.html');
            $m = date('Y-m');
            $this->language = new \util\language('config_reports');
            $this->loadLang('config_reports');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['module'] = $this->module;
            $senderarr['shift_management'] = $shiftmanagement;
            $senderarr['jsdateformat'] = $jsdate;
            $senderarr['month_year'] = $m;
            $senderarr['current_working_date'] = $current_working_date;
            $senderarr['current_working_month'] = $current_working_month;
            $senderarr['current_working_year'] = $current_working_year;
            $senderarr['menulist'] = $menulist[0]['data'];
            $senderarr['categorylist'] = $category_list[0]['data'];
            /*$senderarr['nochargelist'] = $NoCharge_list['Data'];
            $senderarr['waiterlist'] = $Waiter_list['Data'];*/
            $senderarr['combolist'] = $combo_list[0]['data'];
            $senderarr['rawlist'] = $raw_list[0]['data'];
            $senderarr['rawcatlist'] = $rawcat_list[0]['data'];
            $senderarr['vendorlist'] = $vendorlist['Data'];
            $senderarr['itemlist'] = $item_list[0]['data'];
            $senderarr['modilist'] = $modi_list[0]['data'];
            $senderarr['user'] = CONFIG_USR_TYPE;
            $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function generatereport($data)
    {
        try {
            $this->log->logIt($this->module . ' - generatereport');
            $ObjReportsDao = new \database\reportsdao();
            $roundoffdigit = \database\parameter::getParameter('digitafterdecimal');
            if ($data['type'] == 1) {
                $report_data = $ObjReportsDao->getmonthlyrec($data);
                $this->language = new \util\language('config_MonthlyStatisticsReport');
                $this->loadLang('config_MonthlyStatisticsReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr));
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr));
                $this->default_lang_arr = $this->language->loaddefaultlanguage();

                $template = 'monthlystatistics';
                $width = '793px';
                $height = '1122px';
            }
            if ($data['type'] == 2) {
                $this->language = new \util\language('config_DailyRevenueReport');
                $this->loadLang('config_DailyRevenueReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr));
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr));
                $this->default_lang_arr = $this->language->loaddefaultlanguage();
                $report_data = $ObjReportsDao->getdailyrec($data,json_decode($languageArr));

                $template = 'dailyrevenue';
                $width = '793px';
                $height = '1122px';
            }
            if ($data['type'] == 3) {
                $report_data = $ObjReportsDao->getitemsales($data);
                $this->language = new \util\language('config_ItemSalesReport');
                $this->loadLang('config_ItemSalesReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr));
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr));
                $this->default_lang_arr = $this->language->loaddefaultlanguage();

                $template = 'itemsales';
                $width = '793px';
                $height = '1122px';
            }

            if ($data['type'] == 4) {
                $report_data = $ObjReportsDao->getcombosales($data);
                $this->language = new \util\language('config_ComboReport');
                $this->loadLang('config_ComboReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr));
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
                $this->default_lang_arr = $this->language->loaddefaultlanguage();

                $template = 'ComboReport';
                $width = '793px';
                $height = '1122px';
            }

            /* if ($data['type'] == 4) {
                 $report_data = $ObjReportsDao->getcombosales($data);

                 $template = 'comboproducts';
                 $width = '793px';
                 $height = '1122px';
             }
            if ($data['type'] == 6) {
                 $report_data = $ObjReportsDao->getrawmaterial($data);
                 $template = 'rawmaterial';
                 $width = '793px';
                 $height = '1122px';
             }
             if ($data['type'] == 7) {
                 $report_data = $ObjReportsDao->getvendor($data);
                 $template = 'vendor';
                 $width = '793px';
                 $height = '1122px';
             }
             if ($data['type'] == 8) {
                 $report_data = $ObjReportsDao->getrecipe($data);
                 $template = 'recipe';
                 $width = '793px';
                 $height = '1122px';
             }
             if ($data['type'] == 9) {
                 $report_data = $ObjReportsDao->getInventory($data);
                 $template = 'inventory';
                 $width = '793px';
                 $height = '1122px';
             }*/
            if ($data['type'] == 5) {
                $report_data = $ObjReportsDao->getitemlocationwise($data);
                $this->language = new \util\language('config_DailyRevenueReport');
                $this->loadLang('config_DailyRevenueReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr));
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr));
                $this->default_lang_arr = $this->language->loaddefaultlanguage();

                $template = 'dailyitemrevenue';
                $width = '793px';
                $height = '1122px';
            }
            if ($data['type'] == 10) {
                $report_data = $ObjReportsDao->getdailystatistics($data);
                //ds.html for thermal print and dailystatistics.html for A4 size
                $this->language = new \util\language('config_DailyStatisticsReport');
                $this->loadLang('config_DailyStatisticsReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr));
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr));
                $this->default_lang_arr = $this->language->loaddefaultlanguage();

                $template = 'ds';
                $width = '302px';
                $height = '793px';
            }

            if ($data['type'] == 11) {
                $this->language = new \util\language('config_PaymentTypeReport');
                $this->loadLang('config_PaymentTypeReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr));
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr));
                $this->default_lang_arr = $this->language->loaddefaultlanguage();
                $report_data = $ObjReportsDao->GetPaymentTypeReport($data,json_decode($languageArr));

                $template = 'PaymentTypeReport';
                $width = '793px';
                $height = '1122px';
            }
           /* if ($data['type'] == 12) {
                $this->language = new \util\language('config_NoChargeReport');
                $this->loadLang('config_NoChargeReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr));
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
                $this->default_lang_arr = $this->language->loaddefaultlanguage();
                $report_data = $ObjReportsDao->GetNoChargeReport($data,json_decode($languageArr));

                $template = 'NoChargeReport';
                $width = '793px';
                $height = '1122px';
            }*/
            if ($data['type'] == 13) {
                $this->language = new \util\language('config_CashDrawerReport');
                $this->loadLang('config_CashDrawerReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr));
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
                $this->default_lang_arr = $this->language->loaddefaultlanguage();
                $report_data = $ObjReportsDao->GetCashDrawerReport($data);

                $template = 'CashDrawerReport';
                $width = '793px';
                $height = '1122px';
            }
            if ($data['type'] == 14) {
                $this->language = new \util\language('config_ComplementaryReport');
                $this->loadLang('config_ComplementaryReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr));
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
                $this->default_lang_arr = $this->language->loaddefaultlanguage();
                $report_data = $ObjReportsDao->GetComplementaryReport($data);

                $template = 'ComplementaryReport';
                $width = '793px';
                $height = '1122px';
            }
            if ($data['type'] == 15) {
                $this->language = new \util\language('config_GratuityReport');
                $this->loadLang('config_GratuityReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr));
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
                $this->default_lang_arr = $this->language->loaddefaultlanguage();
                $report_data = $ObjReportsDao->GetGratuityReport($data,json_decode($languageArr));

                $template = 'GratuityReport';
                $width = '793px';
                $height = '1122px';
            }
            /*if ($data['type'] == 16) {
                $this->language = new \util\language('config_WaiterSaleReport');
                $this->loadLang('config_WaiterSaleReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr));
                $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr));
                $this->default_lang_arr = $this->language->loaddefaultlanguage();
                $report_data = $ObjReportsDao->GetWaiterSaleReport($data,json_decode($languageArr));
                $template = 'WaiterSaleReport';
                $width = '793px';
                $height = '1122px';
            }*/
            $report_data['rfrmt']=$roundoffdigit;
            $PrintReport = new \common\printreport();
            $PrintReport->addTemplate($template);
            $PrintReport->addRecord($report_data);
            $PrintReport->Language($languageArr);
            $PrintReport->FrontLanguage($defaultlanguageArr);
            $data = $PrintReport->Request();

            return json_encode(array("Success" => "True", "Data" => $data, "Height" => $height, "Width" => $width));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - generatereport - ' . $e);
        }
    }

    public function dailyreport($data)
    {
        try {
            $this->log->logIt($this->module . ' - dailyreport');
            global $twig;
            global $commonurl;
            $ObjReportsDao = new \database\reportsdao();
            $report_data = $ObjReportsDao->getdailyrec($data);
            $PrintReport = new \common\printreport();
            $PrintReport->addTemplate('dailyrevenue');
            $PrintReport->addRecord($report_data);
            $data = $PrintReport->Request();
            return json_encode(array("Success" => "True", "Data" => $data));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - dailyreport - ' . $e);
        }
    }

    public function GetPaymentTypeList($data)
    {
        try {
            $this->log->logIt($this->module . " - GetPaymentTypeList");
            $ObjPaymentType = new \database\reportsdao();
            $res = $ObjPaymentType->getPaymentTypes($data);

            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - GetPaymentTypeList - " . $e);
            return false;
        }

    }

    public function exportreportData($data)
    {
        try {
            $this->log->logIt($this->module . ' - exportreportData');
            // $dao = new \dao();
            $ObjReportsDao = new \database\reportsdao();

            if ($data['type'] == 1) {
                $report_data = $ObjReportsDao->getmonthlyrec($data);
                $this->language = new \util\language('config_MonthlyStatisticsReport');
                $this->loadLang('config_MonthlyStatisticsReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);
                $dataArr = array();
                if ($report_data) {
                    $cnt = 0;
                    foreach ($report_data['data'] as $key => $value) {
                        $dataArr[$key]['order_date'] = $value['order_date'];
                        $dataArr[$key]['total_order'] = $value['total_order'];
                        $dataArr[$key]['itemsold'] = $value['itemsold'];
                        $dataArr[$key]['itemprice'] = $value['itemprice'];
                        $dataArr[$key]['taxamt'] = $value['taxamt'];
                        $dataArr[$key]['discamt'] = $value['discamt'];
                        $dataArr[$key]['finalamt'] = $value['finalamt'];
                        $cnt++;
                    }
                    $dataArr[$cnt]['order_date'] = $defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['total_order'] = $report_data['total_order'];
                    $dataArr[$cnt]['itemsold'] = $report_data['items_sold'];
                    $dataArr[$cnt]['itemprice'] = str_replace(',', '', $report_data['item_price']);
                    $dataArr[$cnt]['taxamt'] = str_replace(',', '', $report_data['item_tax']);
                    $dataArr[$cnt]['discamt'] = str_replace(',', '', $report_data['item_discount']);
                    $dataArr[$cnt]['finalamt'] = str_replace(',', '', $report_data['order_amount']);

                    $label_array = array($defaultlanguageArr->DATE, $languageArr->LANG2, $languageArr->LANG3, $languageArr->LANG4, $defaultlanguageArr->TAX, $defaultlanguageArr->DISCOUNT, $defaultlanguageArr->TOTAL_PRICE);

                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $label_array;
                    $rec_arr['fromdate'] = $data['date'];
                    $rec_arr['currency_symbol'] = $report_data['currency_symbol'];
                    $rec_arr['reportname'] = $languageArr->LANG5;
                    $rec_arr['loc_name'] = $report_data['loc_name'];
                    $rec_arr['loc_email'] = $report_data['loc_email'];
                    $rec_arr['loc_phone'] = $report_data['loc_phone'];
                    $rec_arr['filename'] = $languageArr->LANG5 . date("Ymd") . rand() . '.csv';
                    return json_encode(array("Success" => "True", "Data" => $rec_arr));
                } else {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->NO_RECORDS));
                }
            }
            if ($data['type'] == 2) {
                $this->language = new \util\language('config_DailyRevenueReport');
                $this->loadLang('config_DailyRevenueReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);

                $report_data = $ObjReportsDao->getdailyrec($data,$languageArr);
                $dataArr = array();
                if ($report_data) {
                    $cnt = 0;
                    foreach ($report_data['data'] as $key => $value) {
                        $dataArr[$key]['date'] = $value['trandate'];
                        $dataArr[$key]['foliono'] = $value['foliono'];
                        $dataArr[$key]['itemprice'] = $value['itemprice'];
                        $dataArr[$key]['moditemprice'] = $value['moditemprice'];
                        $dataArr[$key]['taxamt'] = $value['taxamt'];
                        $dataArr[$key]['discamt'] = $value['discamt'];
                        $dataArr[$key]['finalamt'] = $value['finalamt'];
                        $dataArr[$key]['ispostedtopms'] = $value['ispostedtopms'];
                        $cnt++;
                    }
                    $dataArr[$cnt]['date'] = $defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['foliono'] = '';
                    $dataArr[$cnt]['itemprice'] = str_replace(',', '', $report_data['item_price']);
                    $dataArr[$cnt]['moditemprice'] = str_replace(',', '', $report_data['modifier_item_price']);
                    $dataArr[$cnt]['taxamt'] = str_replace(',', '', $report_data['item_tax']);
                    $dataArr[$cnt]['discamt'] = str_replace(',', '', $report_data['item_discount']);
                    $dataArr[$cnt]['finalamt'] = str_replace(',', '', $report_data['order_amount']);
                    $dataArr[$cnt]['ispostedtopms'] = '';

                    $label_array = array($defaultlanguageArr->DATE,$languageArr->LANG1, $languageArr->LANG2, $languageArr->LANG3, $defaultlanguageArr->TAX, $defaultlanguageArr->DISCOUNT, $defaultlanguageArr->TOTAL_PRICE,$defaultlanguageArr->REMARK);

                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $label_array;
                    $rec_arr['fromdate'] = $data['date'];
                    $rec_arr['currency_symbol'] = $report_data['currency_symbol'];
                    $rec_arr['reportname'] = $languageArr->LANG4;
                    $rec_arr['loc_name'] = $report_data['loc_name'];
                    $rec_arr['loc_email'] = $report_data['loc_email'];
                    $rec_arr['loc_phone'] = $report_data['loc_phone'];
                    $rec_arr['filename'] = $languageArr->LANG4 . date("Ymd") . rand() . '.csv';
                    return json_encode(array("Success" => "True", "Data" => $rec_arr));
                } else {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->NO_RECORDS));
                }
            }
            if ($data['type'] == 3) {
                $this->language = new \util\language('config_ItemSalesReport');
                $this->loadLang('config_ItemSalesReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);

                $report_data = $ObjReportsDao->getitemsales($data);
                $dataArr = array();
                if ($report_data) {
                    $cnt = 0;
                    foreach ($report_data['data'] as $key => $value) {
                        $dataArr[$key]['categoryname'] = $value['categoryname'];
                        $dataArr[$key]['itemname'] = $value['itemname'].'('.$value['unitName'].')';
                        $dataArr[$key]['quantity'] = $value['quantity'];
                        $dataArr[$key]['rate'] = ($value['itemprice']/$value['quantity']);
                        $dataArr[$key]['itemprice'] = $value['itemprice'];
                        $dataArr[$key]['moditemprice'] = $value['moditemprice'];
                        $dataArr[$key]['taxamt'] = $value['taxamt'];
                        $dataArr[$key]['itemdiscount'] = $value['itemdiscount'];
                        $dataArr[$key]['itemamt'] = $value['itemamt'];
                        $cnt++;
                    }
                    $dataArr[$cnt]['categoryname'] = '';
                    $dataArr[$cnt]['itemname'] = $defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['quantity'] ='';
                    $dataArr[$cnt]['rate'] ='';
                    $dataArr[$cnt]['itemprice'] = str_replace(',', '', $report_data['item_price']);
                    $dataArr[$cnt]['moditemprice'] = str_replace(',', '', $report_data['modifier_item_price']);
                    $dataArr[$cnt]['taxamt'] = str_replace(',', '', $report_data['item_tax']);
                    $dataArr[$cnt]['itemdiscount'] = str_replace(',', '', $report_data['item_discount']);
                    $dataArr[$cnt]['itemamt'] = str_replace(',', '', $report_data['order_amount']);

                    //$headers = \common\staticarray::$headers['ItemSaleStatistic'];
                    $label_array = array($languageArr->LANG1, $languageArr->LANG4,$languageArr->LANG9, $languageArr->LANG2
                    , $languageArr->LANG3, $defaultlanguageArr->TAX, $defaultlanguageArr->DISCOUNT, $defaultlanguageArr->TOTAL_PRICE);

                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $label_array;
                    $rec_arr['fromdate'] = $data['fromdate'];
                    $rec_arr['todate'] = $data['todate'];
                    $rec_arr['currency_symbol'] = $report_data['currency_symbol'];
                    $rec_arr['reportname'] = $languageArr->LANG5;
                    $rec_arr['loc_name'] = $report_data['loc_name'];
                    $rec_arr['loc_email'] = $report_data['loc_email'];
                    $rec_arr['loc_phone'] = $report_data['loc_phone'];
                    $rec_arr['filename'] = $languageArr->LANG5 . date("Ymd") . rand() . '.csv';
                    return json_encode(array("Success" => "True", "Data" => $rec_arr));
                } else {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->NO_RECORDS));
                }
            }
            if ($data['type'] == 4) {
                $this->language = new \util\language('config_ComboReport');
                $this->loadLang('config_ComboReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);

                $report_data = $ObjReportsDao->getcombosales($data);
                $dataArr = array();
                if ($report_data) {
                    $cnt = 0;
                    foreach ($report_data['data'] as $key => $value) {
                        $dataArr[$key]['comboname'] = $value['comboname'];
                        $dataArr[$key]['qty'] = $value['qty'];
                        $dataArr[$key]['base_amount'] = $value['base_amount'];
                        $dataArr[$key]['tax'] = $value['tax'];
                        $dataArr[$key]['order_amount'] = str_replace(',', '', $value['order_amount']);
                        $cnt++;
                    }

                    $dataArr[$cnt]['comboname'] = $defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['qty'] = str_replace(',', '', $report_data['total_qty']);
                    $dataArr[$cnt]['base_amount'] = str_replace(',', '', $report_data['total_base_amount']);
                    $dataArr[$cnt]['tax'] = str_replace(',', '', $report_data['total_tax']);
                    $dataArr[$cnt]['order_amount'] = str_replace(',', '', $report_data['total_order_amount']);

                    //$headers = \common\staticarray::$headers['ItemSaleStatistic'];
                    $label_array = array($languageArr->LANG1,$languageArr->LANG2,$languageArr->LANG3,$defaultlanguageArr->TAX,$defaultlanguageArr->TOTAL_PRICE);

                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $label_array;
                    $rec_arr['fromdate'] = $data['fromdate'];
                    $rec_arr['todate'] = $data['todate'];
                    $rec_arr['currency_symbol'] = $report_data['currency_symbol'];
                    $rec_arr['reportname'] = $languageArr->LANG5;
                    $rec_arr['loc_name'] = $report_data['loc_name'];
                    $rec_arr['loc_email'] = $report_data['loc_email'];
                    $rec_arr['loc_phone'] = $report_data['loc_phone'];
                    $rec_arr['filename'] = $languageArr->LANG5 . date("Ymd") . rand() . '.csv';
                    return json_encode(array("Success" => "True", "Data" => $rec_arr));
                } else {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->NO_RECORDS));
                }
            }
            if ($data['type'] == 5) {
                $this->language = new \util\language('config_DailyRevenueReport');
                $this->loadLang('config_DailyRevenueReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);

                $report_data = $ObjReportsDao->getitemlocationwise($data);
                $dataArr = array();

                if ($report_data) {
                    $cnt = 0;

                    foreach ($report_data['data'] as $key => $value) {
                        $dataArr[$key]['locationname'] = $value['locationname'];
                        $dataArr[$key]['foliono'] = $value['foliono'];
                        $dataArr[$key]['itemprice'] = $value['itemprice'];
                        $dataArr[$key]['moditemprice'] = $value['moditemprice'];
                        $dataArr[$key]['taxamt'] = $value['taxamt'];
                        $dataArr[$key]['discamt'] = $value['discamt'];
                        $dataArr[$key]['finalamt'] = $value['finalamt'];
                        $cnt++;
                    }
                    $dataArr[$cnt]['locationname'] = '';
                    $dataArr[$cnt]['foliono'] =  $defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['itemprice'] = str_replace(',', '', $report_data['item_price']);
                    $dataArr[$cnt]['moditemprice'] = str_replace(',', '', $report_data['modifier_item_price']);
                    $dataArr[$cnt]['taxamt'] = str_replace(',', '', $report_data['item_tax']);
                    $dataArr[$cnt]['discamt'] = str_replace(',', '', $report_data['item_discount']);
                    $dataArr[$cnt]['finalamt'] = str_replace(',', '', $report_data['order_amount']);

                    $label_array = array($languageArr->LANG1, $languageArr->LANG2, $languageArr->LANG3, $defaultlanguageArr->TAX, $defaultlanguageArr->DISCOUNT, $defaultlanguageArr->TOTAL_PRICE);
                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $label_array;
                    $rec_arr['fromdate'] = $data['fromdate'];
                    $rec_arr['currency_symbol'] = $report_data['currency_symbol'];
                    $rec_arr['reportname'] = $languageArr->LANG4;
                    $rec_arr['filename'] = $languageArr->LANG4 . date("Ymd") . rand() . '.csv';
                    return json_encode(array("Success" => "True", "Data" => $rec_arr));
                } else {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->NO_RECORDS));
                }
            }
            if ($data['type'] == 10) {
                $this->language = new \util\language('config_DailyStatisticsReport');
                $this->loadLang('config_DailyStatisticsReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);

                $report_data = $ObjReportsDao->getdailystatistics($data);
                $dataArr = array();
                if ($report_data) {
                    $dataArr[0]['total_order'] = $languageArr->LANG1;
                    $dataArr[1]['items_sold'] = $languageArr->LANG2;
                    $dataArr[2]['item_price'] = $languageArr->LANG3;
                    $dataArr[3]['modifier_item_price'] = $languageArr->LANG4;
                    $dataArr[4]['item_tax'] = $defaultlanguageArr->TAX;
                    $dataArr[5]['item_discount'] = $defaultlanguageArr->DISCOUNT;
                    $dataArr[6]['order_amount'] = $defaultlanguageArr->GRAND_TOTAL;

                    $dataArr[0]['total_order1'] = $report_data['total_order'];
                    $dataArr[1]['items_sold1'] = $report_data['items_sold'];
                    $dataArr[2]['item_price1'] = str_replace(',', '', $report_data['item_price']);
                    $dataArr[3]['modifier_item_price1'] = str_replace(',', '', $report_data['modifier_item_price']);
                    $dataArr[4]['item_tax1'] = str_replace(',', '', $report_data['item_tax']);
                    $dataArr[5]['item_discount1'] = str_replace(',', '', $report_data['item_discount']);
                    $dataArr[6]['order_amount1'] = str_replace(',', '', $report_data['order_amount']);

                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['fromdate'] = $data['date'];
                    $rec_arr['currency_symbol'] = $report_data['currency_symbol'];
                    $rec_arr['reportname'] = $languageArr->LANG5;
                    $rec_arr['loc_name'] = $report_data['loc_name'];
                    $rec_arr['loc_email'] = $report_data['loc_email'];
                    $rec_arr['loc_phone'] = $report_data['loc_phone'];
                    $rec_arr['filename'] = $languageArr->LANG5. date("Ymd") . rand() . '.csv';
                    return json_encode(array("Success" => "True", "Data" => $rec_arr));
                } else {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->NO_RECORDS));
                }
            }
            if ($data['type'] == 11) {
                $this->language = new \util\language('config_PaymentTypeReport');
                $this->loadLang('config_PaymentTypeReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);
                $report_data = $ObjReportsDao->GetPaymentTypeReport($data);
                $dataArr = array();
                if ($report_data) {
                    $cnt = 0;
                    foreach ($report_data['Paymentdetail'] as $key => $value) {
                        $dataArr[$cnt]['Date'] = $value['Date'];
                        $dataArr[$cnt]['FolioName'] = $value['FolioName'];
                        if ($value['PaymenDetail'] != '') {
                            $inc = 0;
                            foreach ($value['PaymenDetail'] as $key2 => $value2) {
                                if ($inc == 0) {
                                    $dataArr[$cnt]['PaymentType'] = $value2['FolioNo'];
                                    $dataArr[$cnt]['BaseAmount'] = str_replace(',', '', $value2['BaseAmount']);
                                    $inc++;
                                    $cnt++;

                                } else {
                                    $dataArr[$cnt]['Date'] = '';
                                    $dataArr[$cnt]['FolioName'] = '';
                                    $dataArr[$cnt]['PaymentType'] = $value2['FolioNo'];
                                    $dataArr[$cnt]['BaseAmount'] = str_replace(',', '', $value2['BaseAmount']);
                                    $inc++;
                                    $cnt++;
                                }
                            }
                        }
                        $cnt++;
                    }

                    $dataArr[$cnt]['Date'] = '';
                    $dataArr[$cnt]['FolioName'] = '';
                    $dataArr[$cnt]['PaymentType'] = $defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['BaseAmount'] = str_replace(',', '', $report_data['PaymentTotal']);

                    $label_array = array($defaultlanguageArr->DATE,$languageArr->LANG1,$languageArr->LANG2,$defaultlanguageArr->TOTAL_PRICE);
                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $label_array;
                    $rec_arr['fromdate'] = isset($data['current_date'])?$data['current_date']:'';
                    $rec_arr['currency_symbol'] = $report_data['currency_symbol'];
                    $rec_arr['reportname'] = $languageArr->LANG3;
                    $rec_arr['loc_name'] = $report_data['loc_name'];
                    $rec_arr['loc_email'] = $report_data['loc_email'];
                    $rec_arr['loc_phone'] = $report_data['loc_phone'];
                    $rec_arr['filename'] = $languageArr->LANG3 . date("Ymd") . rand() . '.csv';
                    return json_encode(array("Success" => "True", "Data" => $rec_arr));
                } else {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->NO_RECORDS));
                }

            }
           /* if ($data['type'] == 12) {
                $this->language = new \util\language('config_NoChargeReport');
                $this->loadLang('config_NoChargeReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);
                $report_data = $ObjReportsDao->GetNoChargeReport($data,$languageArr);
                $dataArr = array();
                if ($report_data) {
                    $cnt = 0;
                    foreach ($report_data['data'] as $key => $value) {
                        $dataArr[$key]['NochargeName'] = $value['NochargeName'];
                        $dataArr[$key]['FolioDate'] = $value['FolioDate'];
                        $dataArr[$key]['credit_reset_type'] = $value['credit_reset_type'];
                        $dataArr[$key]['LmtAmt'] = $value['LmtAmt'];
                        $dataArr[$key]['FolioAmt'] = $value['FolioAmt'];
                        $cnt++;
                    }
                    $dataArr[$cnt]['NochargeName'] = '';
                    $dataArr[$cnt]['FolioDate'] = '';
                    $dataArr[$cnt]['credit_reset_type'] = '';
                    $dataArr[$cnt]['LmtAmt'] = $defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['FolioAmt'] = str_replace(',', '', $report_data['total_folioamt']);

                    $label_array = array($languageArr->LANG1,$defaultlanguageArr->DATE, $languageArr->LANG2, $languageArr->LANG3, $languageArr->LANG4);

                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $label_array;
                    $rec_arr['fromdate'] = $report_data['report_date'];
                    $rec_arr['currency_symbol'] = $report_data['currency_symbol'];
                    $rec_arr['reportname'] = $languageArr->LANG5;
                    $rec_arr['loc_name'] = $report_data['loc_name'];
                    $rec_arr['loc_email'] = $report_data['loc_email'];
                    $rec_arr['loc_phone'] = $report_data['loc_phone'];
                    $rec_arr['filename'] = $languageArr->LANG5 . date("Ymd") . rand() . '.csv';
                    return json_encode(array("Success" => "True", "Data" => $rec_arr));
                } else {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->NO_RECORDS));
                }
            }*/

            if ($data['type'] == 13) {
                $this->language = new \util\language('config_CashDrawerReport');
                $this->loadLang('config_CashDrawerReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);
                $report_data = $ObjReportsDao->GetCashDrawerReport($data);
                $dataArr = array();
                if ($report_data) {
                    $cnt = 0;
                    foreach ($report_data['data'] as $key => $value) {
                        $dataArr[$key]['username'] = $value['username'];
                        $dataArr[$key]['timein'] = $value['timein'];
                        $dataArr[$key]['timeout'] = $value['timeout'];
                        $dataArr[$key]['openingbalance'] = $value['openingbalance'];
                        $dataArr[$key]['closingbalance'] = $value['closingbalance'];
                       // $dataArr[$key]['total_bank_cash'] = $value['total_bank_cash'];
                        $cnt++;
                    }
                    $dataArr[$cnt]['username'] = '';
                    $dataArr[$cnt]['timein'] = '';
                    $dataArr[$cnt]['timeout'] = $defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['openingbalance'] =  str_replace(',', '', $report_data['total_openingbalance']);
                    $dataArr[$cnt]['closingbalance'] = str_replace(',', '', $report_data['total_closingbalance']);
                    //$dataArr[$cnt]['total_bank_cash'] = str_replace(',', '', $report_data['total_bankcashbalance']);

                    $label_array = array($languageArr->LANG1,$languageArr->LANG2, $languageArr->LANG3, $languageArr->LANG4,$languageArr->LANG9);

                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $label_array;
                    $rec_arr['fromdate'] = $report_data['report_date'];
                    $rec_arr['currency_symbol'] = $report_data['currency_symbol'];
                    $rec_arr['reportname'] = $languageArr->LANG5;
                    $rec_arr['loc_name'] = $report_data['loc_name'];
                    $rec_arr['loc_email'] = $report_data['loc_email'];
                    $rec_arr['loc_phone'] = $report_data['loc_phone'];
                    $rec_arr['filename'] = $languageArr->LANG5 . date("Ymd") . rand() . '.csv';
                    return json_encode(array("Success" => "True", "Data" => $rec_arr));
                } else {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->NO_RECORDS));
                }
            }
            if ($data['type'] == 14) {
                $this->language = new \util\language('config_ComplementaryReport');
                $this->loadLang('config_ComplementaryReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);
                $report_data = $ObjReportsDao->GetComplementaryReport($data,$languageArr);
                $report_data = html_entity_decode(json_encode($report_data),ENT_QUOTES);
                $report_data = json_decode($report_data,1);
                $dataArr = array();
                if ($report_data) {
                    $cnt = 0;
                    foreach ($report_data['data'] as $key => $value) {
                        $dataArr[$key]['FolioDate'] = $value['FolioDate'];
                        $dataArr[$key]['foliono'] = $value['foliono'];
                        $dataArr[$key]['totalitemqty'] = $value['totalitemqty'];
                        $dataArr[$key]['totalmodqty'] = $value['totalmodqty'];
                        $dataArr[$key]['baseamount'] = $value['baseamount'];
                        $cnt++;
                    }
                    $dataArr[$cnt]['FolioDate'] = '';
                    $dataArr[$cnt]['foliono'] = $defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['totalitemqty'] = str_replace(',', '', $report_data['total_itemqty']);
                    $dataArr[$cnt]['totalmodqty'] = str_replace(',', '', $report_data['total_modqty']);
                    $dataArr[$cnt]['baseamount'] =  str_replace(',', '', $report_data['total_folioamt']);

                    $label_array = array($defaultlanguageArr->DATE,$languageArr->LANG1, $languageArr->LANG3, $languageArr->LANG4, $languageArr->LANG2);

                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $label_array;
                    $rec_arr['fromdate'] = $report_data['report_date'];
                    $rec_arr['currency_symbol'] = $report_data['currency_symbol'];
                    $rec_arr['reportname'] = $languageArr->LANG5;
                    $rec_arr['loc_name'] = $report_data['loc_name'];
                    $rec_arr['loc_email'] = $report_data['loc_email'];
                    $rec_arr['loc_phone'] = $report_data['loc_phone'];
                    $rec_arr['filename'] = $languageArr->LANG5 . date("Ymd") . rand() . '.csv';
                    return json_encode(array("Success" => "True", "Data" => $rec_arr));
                } else {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->NO_RECORDS));
                }
            }
            if ($data['type'] == 15) {
                $this->language = new \util\language('config_GratuityReport');
                $this->loadLang('config_GratuityReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);
                $report_data = $ObjReportsDao->GetGratuityReport($data,$languageArr);
                $dataArr = array();
                if ($report_data) {
                    $cnt = 0;
                    foreach ($report_data['data'] as $key => $value) {
                        //$dataArr[$key]['waitername'] = $value['waitername'];
                        $dataArr[$key]['FolioDate'] = $value['FolioDate'];
                        $dataArr[$key]['foliono'] = $value['foliono'].$value['waitername'];
                        $dataArr[$key]['tipamount'] = $value['tipamount'];
                        $cnt++;
                    }
                    //$dataArr[$cnt]['waitername'] = '';
                    $dataArr[$cnt]['FolioDate'] = '';
                    $dataArr[$cnt]['foliono'] = $defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['tipamount'] =  str_replace(',', '', $report_data['total_tipamount']);

                    $label_array = array($defaultlanguageArr->DATE,$languageArr->LANG2, $languageArr->LANG3);

                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $label_array;
                    $rec_arr['fromdate'] = $report_data['report_date'];
                    $rec_arr['currency_symbol'] = $report_data['currency_symbol'];
                    $rec_arr['reportname'] = $languageArr->LANG5;
                    $rec_arr['loc_name'] = $report_data['loc_name'];
                    $rec_arr['loc_email'] = $report_data['loc_email'];
                    $rec_arr['loc_phone'] = $report_data['loc_phone'];
                    $rec_arr['filename'] = $languageArr->LANG5 . date("Ymd") . rand() . '.csv';
                    return json_encode(array("Success" => "True", "Data" => $rec_arr));
                } else {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->NO_RECORDS));
                }
            }
           /* if ($data['type'] == 16) {
                $this->language = new \util\language('config_WaiterSaleReport');
                $this->loadLang('config_WaiterSaleReport');
                $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
                $languageArr = json_decode($languageArr);
                $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
                $defaultlanguageArr = json_decode($defaultlanguageArr);
                $report_data = $ObjReportsDao->GetWaiterSaleReport($data,$languageArr);
                $report_data = html_entity_decode(json_encode($report_data),ENT_QUOTES);
                $report_data = json_decode($report_data,1);
                $dataArr = array();
                if ($report_data) {
                    $cnt = 0;
                    foreach ($report_data['data'] as $key => $value) {
                        $dataArr[$key]['waitername'] = $value['waiter_name'];
                        $dataArr[$key]['itemname'] = $value['itemname'].'('.$value['unitName'].')';
                        $dataArr[$key]['quantity'] = $value['quantity'];
                        $dataArr[$key]['rate'] = ($value['itemprice']/$value['quantity']);
                        $dataArr[$key]['itemprice'] = $value['itemprice'];
                        $dataArr[$key]['moditemprice'] = $value['moditemprice'];
                        $dataArr[$key]['taxamt'] = $value['taxamt'];
                        $dataArr[$key]['itemdiscount'] = $value['itemdiscount'];
                        $dataArr[$key]['itemamt'] = $value['itemamt'];
                        $cnt++;
                    }
                    $dataArr[$cnt]['waitername'] = '';
                    $dataArr[$cnt]['itemname'] = $defaultlanguageArr->GRAND_TOTAL;
                    $dataArr[$cnt]['quantity'] = '';
                    $dataArr[$cnt]['rate'] = '';
                    $dataArr[$cnt]['itemprice'] = str_replace(',', '', $report_data['item_price']);
                    $dataArr[$cnt]['moditemprice'] = str_replace(',', '', $report_data['modifier_item_price']);
                    $dataArr[$cnt]['taxamt'] = str_replace(',', '', $report_data['item_tax']);
                    $dataArr[$cnt]['itemdiscount'] = str_replace(',', '', $report_data['item_discount']);
                    $dataArr[$cnt]['itemamt'] = str_replace(',', '', $report_data['order_amount']);

                    $label_array = array($languageArr->LANG1, $languageArr->LANG2, $languageArr->LANG3,$languageArr->LANG4,$languageArr->LANG9,$languageArr->LANG10, $defaultlanguageArr->TAX, $defaultlanguageArr->DISCOUNT, $defaultlanguageArr->TOTAL_PRICE);
                    $rec_arr = array();
                    $rec_arr['record'] = $dataArr;
                    $rec_arr['key'] = $label_array;
                    $rec_arr['fromdate'] = $data['fromdate'];
                    $rec_arr['todate'] = $data['todate'];
                    $rec_arr['currency_symbol'] = $report_data['currency_symbol'];
                    $rec_arr['reportname'] = $languageArr->LANG8;
                    $rec_arr['loc_name'] = $report_data['loc_name'];
                    $rec_arr['loc_email'] = $report_data['loc_email'];
                    $rec_arr['loc_phone'] = $report_data['loc_phone'];
                    $rec_arr['filename'] = $languageArr->LANG8 . date("Ymd") . rand() . '.csv';
                    return json_encode(array("Success" => "True", "Data" => $rec_arr));
                } else {
                    return json_encode(array('Success' => 'False', 'Message' => $defaultlanguageArr->NO_RECORDS));
                }
            }*/
            //return json_encode(array("Success" => "True", "Data" => $report_data));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - exportreportData - ' . $e);
        }
    }

    public function loadLang($operation)
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            if ($operation == 'config_reports') {
                $default_lang_arr = \common\staticlang::$config_reports;

            }
            if ($operation == 'config_MonthlyStatisticsReport') {
                $default_lang_arr = \common\staticlang::$config_MonthlyStatisticsReport;
            }
            if ($operation == 'config_DailyRevenueReport') {
                $default_lang_arr = \common\staticlang::$config_DailyRevenueReport;
            }
            if ($operation == 'config_ItemSalesReport') {
                $default_lang_arr = \common\staticlang::$config_ItemSalesReport;
            }
            if ($operation == 'config_DailyStatisticsReport') {
                $default_lang_arr = \common\staticlang::$config_DailyStatisticsReport;
            }
            if ($operation == 'config_PaymentTypeReport') {
                $default_lang_arr = \common\staticlang::$config_PaymentTypeReport;
            }
           /* if ($operation == 'config_NoChargeReport') {
                $default_lang_arr = \common\staticlang::$config_NoChargeReport;
            }*/
            if ($operation == 'config_CashDrawerReport') {
                $default_lang_arr = \common\staticlang::$config_CashDrawerReport;
            }
            if ($operation == 'config_ComplementaryReport') {
                $default_lang_arr = \common\staticlang::$config_ComplementaryReport;
            }
            if ($operation == 'config_GratuityReport') {
                $default_lang_arr = \common\staticlang::$config_GratuityReport;
            }
	    /*if ($operation == 'config_WaiterSaleReport') {
                $default_lang_arr = \common\staticlang::$config_WaiterSaleReport;
            }*/
            if ($operation == 'config_ComboReport') {
                $default_lang_arr = \common\staticlang::$config_ComboReport;
            }

            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}

?>