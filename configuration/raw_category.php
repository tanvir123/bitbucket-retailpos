<?php
class raw_category
{
    public $module='raw_category';
    public $log;
    private $language,$lang_arr,$default_lang_arr;
    
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }
	public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(27,$this->module);
            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(27);

            $ObjUserDao = new \database\raw_categorydao();
            $data = $ObjUserDao->categorylist(50,'0','');
			$template = $twig->loadTemplate('raw_category.html');
            $this->loadLang();	
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
			$senderarr['module'] = $this->module;
			$senderarr['datalist'] = $data;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	
	public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
	                    $this->loadLang();
            $flag = \util\validate::check_notnull($data,array('name','sortkey'));
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            if($flag=='true')
            {
                $reqarr = array(
                            'name'=>$data['name'],
                            'sortkey'=>$data['sortkey'],
                            'rdo_status'=>$data['rdo_status'],
                            "id"=>$data['id'],
                            "module" => $this->module
                        );
                $ObjUnitDao = new \database\raw_categorydao();
				$data = $ObjUnitDao->addCategory($reqarr,$languageArr,$defaultlanguageArr);
                return $data;
            }else
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }
	
	public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            global $twig;
            $limit=50;
            $offset=0;
            $name="";
            
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
			
            $ObjUserDao = new \database\raw_categorydao();
			$data = $ObjUserDao->categorylist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
	
	public function getCategoryRec($data)
	{
		try
		{
            $this->log->logIt($this->module." - getCategoryRec");
            $ObjUnitDao = new \database\raw_categorydao();
			$data = $ObjUnitDao->getCategoryRec($data);
            return $data;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." - getCategoryRec - ".$e);
			return false; 
		}
	}

    public function getrelatedRecords($data)
    {
        try {
            $this->log->logIt($this->module . " - getrelatedRecords");
            global $twig;
            global $commonurl;
            $flag = 0;
            if (!isset($data) || !isset($data['id']) || !isset($data['module'])) {
                $flag == 1;
                $Obj = "";
            } else {
                $ObjrawDao = new \database\raw_categorydao();
                $res = $ObjrawDao->getrelatedRecords($data);
                $bj = json_decode($res, 1);
                $Obj = $bj['Data'];
            }
            $template = $twig->loadTemplate('dependency.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = $commonurl;
            $senderarr['flag'] = $flag;
            $senderarr['rec'] = $Obj;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $ret = array('data' => $template->render($senderarr));
            return json_encode($ret);
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getrelatedRecords - " . $e);
            return false;
        }
    }

    public function getAllCategory($data)
    {
        try
        {
            $this->log->logIt($this->module." - getAllCategory");
            $ObjUnitDao = new \database\raw_categorydao();
            $data = $ObjUnitDao->getAllCategory($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getAllCategory - ".$e);
            return false;
        }
    }
     public function loadLang()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$raw_category;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}
?>