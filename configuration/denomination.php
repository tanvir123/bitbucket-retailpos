<?php


class denomination
{
    public $module='denomination';
    public $log;
    private $language,$lang_arr,$default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('config_denomination');
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;

            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(75,$this->module);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(75);
            $jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getParameter('dateformat')];
            
            $ObjUserDao = new \database\denominationdao();
            $data = $ObjUserDao->denominationlist(50,'0','',0);

            $Objtabinstadao = new \database\synctabinstadao();
            $tbdata = $Objtabinstadao->tabint();
            $this->loadLang();
            $template = $twig->loadTemplate('denomination.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['tran_langs'] = CONFIG_TRANSLATE_LANGUAGES;
            $senderarr['login_type'] = CONFIG_LOGINTYPE;
            $senderarr['datalist'] = $data;
            $senderarr['tablist'] = $tbdata;
			$senderarr['module'] = $this->module;
            $senderarr['jsdateformat'] = $jsdate;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    
    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');
			$flag = \util\validate::check_notnull($data,array('amount','rdo_status'));
			$this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $defaultlanguageArr = json_decode($defaultlanguageArr);

            if($flag=='true')
            {
                $reqarr = array(
					"cash_amount" => $data['amount'],
					"remarks" => isset($data['remarks'])?$data['remarks']:'',
					"rdo_status"=>$data['rdo_status'],
					"id"=>$data['id'],
					"module" => $this->module
				);
                $this->loadLang();

                $ObjDenominationDao = new \database\denominationdao();
				$data = $ObjDenominationDao->adddenominationRec($reqarr,$languageArr,$defaultlanguageArr);
				return $data;
            }else{
                return json_encode(array('Success'=>'False','Message'=>$defaultlanguageArr->SOME_FIELD_MISSING));
            }
		}
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }
	
    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            $ObjUserDao = new \database\denominationdao();
			$data = $ObjUserDao->denominationlist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    
	public function getdenominationRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getdenominationRec");
			$ObjDenominationDao = new \database\denominationdao();
			$data = $ObjDenominationDao->getdenominationRec($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getdenominationRec - ".$e);
			return false; 
		}
	}

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $default_lang_arr = \common\staticlang::$config_denomination;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }


}


?>