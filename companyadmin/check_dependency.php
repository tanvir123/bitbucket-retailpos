<?php

class check_dependency
{
    private $module='check_dependency';
    private $log;
    private $encdec ,$lang_arr, $default_lang_arr;
    
    function __construct()
    {
        try
        {
            $this->log = new \util\logger();
            $this->language = new \util\language($this->module);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
    public function load($data)
    {
        try
        {
            $this->log->logIt($this->module." - load");
            global $twig;
            $flag = 0;
            if(!isset($data['id']) || !isset($data['module']))
            {
                $flag==1;
                $Obj="";
            }
            else{
                $id = (isset($data['id']))?$data['id']:"";
                $ObjStaticArray =new \common\staticarray();
                $table = $ObjStaticArray->auditlogmodules[$data['module']]['table'];
                $pd = $ObjStaticArray->auditlogmodules[$data['module']]['pd'];
                $ObjCommonDao = new \database\commondao();
                $primary = $ObjCommonDao->getprimarykey($table,$id,$pd);
                $ObjDependencyDao = new \database\dependencydao();
                $data = $ObjDependencyDao->checkdependency($data['module'],$primary,2);
                $bj = json_decode($data,1);
                $Obj = $bj['Data'];
            }
            $this->loadlaguage();
            $template = $twig->loadTemplate('dependency.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['flag'] = $flag;
            $senderarr['rec'] = $Obj;
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $rec = $template->render($senderarr);
            return json_encode(array("Data"=>$rec));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - load - ".$e);
        }
    }

    public function loadlaguage()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}
?>