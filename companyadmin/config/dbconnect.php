<?php
#Author: Umesh Lumbhani  - 1 June 2015

class dbconnect
{
	private $module = 'desk_pms';
	private $log;
	
	public function connect($db_name)
	{	
		try
		{
			global $connection;
			
			$dsnNew="mysql:host=".CONFIG_MYSQL_HOST.";dbname=".$db_name."";	
										
			$connection=new PDO($dsnNew,CONFIG_MYSQL_USER,CONFIG_MYSQL_PWD);
			@$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$connection->setAttribute(PDO::ATTR_TIMEOUT,30);
			$connection->exec("SET NAMES utf8");
			return 'Success';
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
	public function config_connect()
	{	
		try
		{
			global $connection;
			
			$dsnNew="mysql:host=".CONFIG_MYSQL_HOST.";dbname=".CONFIG_MYSQL_DBNAME."";	
										
			$connection=new PDO($dsnNew,CONFIG_MYSQL_USER,CONFIG_MYSQL_PWD);
			@$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$connection->setAttribute(PDO::ATTR_TIMEOUT,30);
			$connection->exec("SET NAMES utf8");
			return 'Success';
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
}
?>