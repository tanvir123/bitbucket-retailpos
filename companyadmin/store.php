<?php

class store
{
    public $module = 'store';
    public $log;
    private $language, $lang_arr, $default_lang_arr,$objFunctions;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->objFunctions = new \common\functions();
        $this->objFunctions->check_store_avilable();
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $ObjCommonDao = new \database\commondao();
            $ObjStoreDao = new \database\storedao();

            $this->objFunctions->checkModuleAccess(47);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(47);

            $data = $ObjStoreDao->storelist(50,'0' ,'', 0);
            $countryList = $ObjCommonDao->countrylist();

            $this->loadLang();
            $template = $twig->loadTemplate('store.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['banquet'] = CONFIG_IS_BANQUET;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['datalist'] = $data;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['module'] = $this->module;
            $senderarr['countrylist'] = $countryList;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);

            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module . ' - addeditfrm');

            $flag = \util\validate::check_notnull($data, array('storename', 'contactperson', 'rdo_status', 'select_country'));
             //  'email','city','contact','address'));
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $langlist = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);

            if ($flag == 'true') {
                $reqarr = array(
                    "storename" => $data['storename'],
                    "contactperson" => $data['contactperson'],
                    "rdo_status" => $data['rdo_status'],
                    "country" => $data['select_country'],
                    "email" => $data['email'],
                    "city" => $data['city'],
                    "contact" => $data['contact'],
                    "address" => $data['address'],
                    "id" => $data['id'],
                    "countryname" => $data['country'],
                    "module" => $this->module
                );
                $ObjMenuDao = new \database\storedao();
                $data = $ObjMenuDao->addStore($reqarr,$langlist,$default_langlist);
                return $data;
            } else {
                return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $default_langlist->Missing_Field)));
            }


        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addeditfrm - ' . $e);
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');
            $limit = 50;
            $offset = 0;
            $name = "";

            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['nm']) && $data['nm'] != "")
                $name = $data['nm'];
            $ObjUserDao = new \database\storedao();
            $data = $ObjUserDao->storelist($limit, $offset, $name);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function editclass($data)
    {
        try {
            $this->log->logIt($this->module . " - editclass");
            $ObjMenuDao = new \database\storedao();
            $data = $ObjMenuDao->getstoreRec($data);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - editclass - " . $e);
            return false;
        }
    }
    public function getStoreCount()
    {
        try {
            $this->log->logIt($this->module . " - getStoreCount");
            $ObjStoreDao = new \database\storedao();
            $data = $ObjStoreDao->getStoreCount();
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getStoreCount - " . $e);
            return false;
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$store;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}

?>