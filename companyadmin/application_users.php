<?php
class application_users
{
    public $module='application_users';
    public $log;
    public $jsdateformat;
    private $language,$lang_arr,$default_lang_arr;
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;

            $jsdate = 'd-m-y';
            $jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getBanquetParameter('dateformat')];
            $ObjStaticArray = new \common\staticarray();
            $arr_lang = $ObjStaticArray->language_codes;
            $ObjDao = new \database\application_usersdao();

            $ObjDao1 = new \database\userroledao();
            $menulist = $ObjDao1->privileges();

            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(78);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(78);

            $data = $ObjDao->userlist(50,'0','','','','');

            $template = $twig->loadTemplate('application_users.html');
            $this->loadLang();
            $senderarr = array();

            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['banquet'] = CONFIG_IS_BANQUET;
           // $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['datalist'] = $data;
            $senderarr['module'] = $this->module;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
           // $senderarr['userrolelist'] = $usersrole;
           // $senderarr['storelist'] = $storelist;
           // $senderarr['location_list'] = $ObjDao->getLocationList();
            $senderarr['menulist'] = $menulist;
            $senderarr['jsdateformat'] = $jsdate;
           // $senderarr['sysdefined'] = $usr_detail['sysdefined'];
            //$senderarr['user_type'] = $usr_detail['user_type'];
            $senderarr['loginuser'] = CONFIG_UID;
            $senderarr['lang_list'] = $arr_lang;
            $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            //die(json_encode($senderarr['default_langlist']));

            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['user_type'] = CONFIG_USR_TYPE;

            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $fname="";
            $lname="";
            $username="";
            $email="";
            $mobile="";
            $status="";

            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['un']) && $data['un']!="")
                $username = $data['un'];
            if(isset($data['em']) && $data['em']!="")
                $email = $data['em'];
            if(isset($data['mn']) && $data['mn']!="")
                $mobile = $data['mn'];
            if(isset($data['st']) && $data['st']!="")
                $status = $data['st'];

            $ObjDao = new \database\application_usersdao();
			$data = $ObjDao->userlist($limit,$offset,$username,$email,$mobile,$status);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$application_users;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }

    public function order_history($data)
    {
        try
        {
            $this->log->logIt($this->module.' - Application User Order History');

            $this->loadLang();

            $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
            $langlist = json_decode($languageArr);

            $ObjDao = new \database\application_usersdao();

            $dataorder = $ObjDao->appuserorderlist(50,'0',$data,$langlist);

            return $dataorder;

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - Application User Order History -'.$e);
        }
    }

    public function reco($data)
    {
        try
        {
            $this->log->logIt($this->module.' - reco');
            $limit=50;
            $offset=0;

            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];

            $this->loadLang();

            $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
            $langlist = json_decode($languageArr);

            $ObjDao = new \database\application_usersdao();

            $dataorder = $ObjDao->appuserorderlist($limit,$offset,$data,$langlist);

            return $dataorder;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function item_history($data)
    {
        try
        {
            $this->log->logIt($this->module.' - Order Item Details');

            $this->loadLang();

            $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
            $langlist = json_decode($languageArr);

            $ObjDao = new \database\application_usersdao();

            $itemlist = $ObjDao->itemlist($data,$langlist);

            return $itemlist;



        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - Order Item Details -'.$e);
        }
    }


}
?>