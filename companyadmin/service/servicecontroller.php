<?php
include("common_service.php");

$request = urldecode(file_get_contents("php://input"));

if($request=="") exit(0);

$json_request = json_decode($request,1);

$json_request = \util\util::cleanVariables($json_request);

$basePath = dirname(__FILE__);

define("BASEPATH",$basePath);

if(!count($json_request)) exit(0);

if(!isset($json_request['service'])) exit(0);

if(!isset($json_request['opcode'])) exit(0);

$opcode = $json_request['opcode'];

if(!file_exists($basePath."/".$json_request['service'].".php"))
{
	die("Invalid Service");	
}

include_once($basePath."/".$json_request['service'].".php");

$request='';

$obj = new $json_request['service']();
if(!method_exists($obj,$opcode))
{
	die("Invalid Opcode");	
}

if(isset($opcode))
{
    $prefix = explode("_",$opcode);
	if(isset($json_request['companyid']))
		define('CONFIG_CID',$json_request['companyid']);
	$result=$obj->$opcode($json_request);
	echo $result;
}
?>