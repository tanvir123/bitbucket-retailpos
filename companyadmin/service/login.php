<?php

class login
{
    public $module = 'companyadmin_login';
    public $log;
    public $dbconnection;

    public function __construct()
    {
        $this->log = new \util\logger();
    }

    public function checkcredentials($data)
    {
        try {
            $this->log->logIt($this->module . " - checkcredentials");
            $uname = $data['uname'];
            $pwd = $data['pwd'];
            $companyid = CONFIG_CID;

            $dao = new \dao();
            $strSql = "SELECT USR.*,SC.is_oem FROM sysuser AS USR INNER JOIN syscompany AS SC ON USR.companyid=SC.syscompanyid AND SC.isactive=1  where USR.username=:username AND USR.password=:password AND USR.companyid=:companyid AND USR.isactive=1 AND USR.is_deleted=0 ";
            $dao->initCommand($strSql);
            $dao->addParameter(':username', $uname);
            $dao->addParameter(':password', md5($pwd));
            $dao->addParameter(':companyid', $companyid);
            $rec = $dao->executeQuery();

            $strSql1 = "SELECT meta_value FROM syscompany_meta WHERE syscompanyid=:syscompanyid AND meta_key=:meta_key";
            $dao->initCommand($strSql1);
            $dao->addParameter(':syscompanyid', $data['companyid']);
            $dao->addParameter(':meta_key', 'is_banquet_available');
            $banquet = $dao->executeRow();

            if (count($rec) > 0 && isset($rec[0]['userunkid'])) {
                $strSql1 = "SELECT databasename,bucket_name,companyname FROM syscompany WHERE syscompanyid=:companyid AND isactive=1";
                $dao->initCommand($strSql1);
                $dao->addParameter(':companyid', $companyid);
                $company = $dao->executeQuery();
                if (count($company) == 0) {
                    return json_encode(array("Success" => "False", "Message" => "Invalid Company Code"));
                }
                $dbname = $company[0]['databasename'];
                /* Add Auth Token to redis */
                $systemdefined = $rec[0]['sysdefined'];
                if ($systemdefined != 1) {
                    $strSql = "SELECT GROUP_CONCAT(DISTINCT(CRP.lnkprivilegegroupid),'') as lnkprivilegegroupid 
                              FROM " . $dbname . ".cfroleprivileges as CRP 
                              INNER JOIN sysprivilegegroup as SPG ON CRP.lnkprivilegegroupid=SPG.privilegegroupunkid 
                             WHERE  CRP.companyid=:companyid AND type=1 AND lnkroleid=:lnkroleid";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':lnkroleid', $rec[0]['roleunkid']);
                    $dao->addParameter(':companyid', $companyid);
                    $groupid = $dao->executeRow();
                } else {
                    $strSql = "SELECT  GROUP_CONCAT(DISTINCT(privilegegroupunkid),'') as lnkprivilegegroupid
                              FROM sysprivilegegroup WHERE type=1";

                    $dao->initCommand($strSql);
                    $groupid = $dao->executeRow();
                }

                $storeSql = "SELECT store_available FROM " . $dbname . ".cfcompany WHERE companyid=:companyid ";
                $dao->initCommand($storeSql);
                $dao->addParameter(':companyid', $companyid);
                $store_rec = $dao->executeRow();


                $default_array = \common\staticlang::$company_default;
                $all_default_array = [];
                if (isset($default_array)) {
                    foreach ($default_array as $key => $value) {
                        $all_default_array[$key] = $value;
                    }
                }
                if (isset($banquet['meta_value']) && $banquet['meta_value'] == 1) {
                    $banquet_array = \common\staticlang::$banquet_default;
                    if (isset($banquet_array)) {
                        foreach ($banquet_array as $key => $value) {
                            $all_default_array[$key] = $value;
                        }
                    }
                }
                if (isset($store_rec['store_available']) && $store_rec['store_available'] == 1) {
                    $store_array = \common\staticlang::$store_default;
                    if (isset($store_array)) {
                        foreach ($store_array as $key => $value) {
                            $all_default_array[$key] = $value;
                        }
                    }

                }

                $oemDetails  = array('name' => '','logo' => '','title' =>'','id' => '');
                if($rec[0]['is_oem'] > 0)
                {
                    $dao->initCommand("SELECT * FROM oem_settings WHERE id = :id");
                    $dao->addParameter(":id",$rec[0]['is_oem']);
                    $oemDetails = $dao->executeRow();
                }

                $token = \util\util::getauthtoken($companyid);
                $arr_token = array(
                    "access_token" => $token,
                    "dbname" => $dbname,
                    "username" => $rec[0]['username'],
                    "userid" => $rec[0]['userunkid'],
                    "sysdefined" => $rec[0]['sysdefined'],
                    "user_type" => $rec[0]['user_type'],
                    "bucket_name" => $company[0]['bucket_name'],
                    "lang_type" => $rec[0]['lang_type'],
                    "lang_code" => $rec[0]['lang_code'],
                    "companyid" => $companyid,
                    "groupid" => $groupid['lnkprivilegegroupid'],
                    "roleid" => $rec[0]['roleunkid'],
                    "banquet" => $banquet['meta_value'],
                    "store" => $store_rec['store_available'],
                    "all_default_array" => json_encode($all_default_array),
                    'oem_name' => $oemDetails['name'],
                    'oem_logo' => $oemDetails['logo'],
                    'oem_title' => $oemDetails['title'],
                    'oem_id' => $oemDetails['id']
                );
                $ObjFunction = new \common\functions();
                $res_token = $ObjFunction->addAuthToken($arr_token);

                /* Add Auth Token to redis */
                return json_encode(array("Success" => "True", "Message" => "Logged in successfully", "Data" => $res_token));
            } else
                return json_encode(array("Success" => "False", "Message" => "Invalid login credentials"));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - checkcredentials -" . $e);
            return 0;
        }
    }

    public function checktoken($data)
    {
        try {
            $this->log->logIt($this->module . " - checktoken");
            $access_token = $data['token'];
            $ObjEncDec = new \common\encdec;
            $un = $ObjEncDec->openssl('decrypt', $data['un']);
            $ObjFunction = new \common\functions();
            $rec = $ObjFunction->checkToken($access_token, $un);
            if ($rec == 0)
                return json_encode(array("Success" => "False"));
            else
                return json_encode(array("Success" => "True", "Data" => $rec, "cnt" => 1));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - checktoken -" . $e);
            return json_encode(array("Success" => "False"));
        }
    }
}

?>