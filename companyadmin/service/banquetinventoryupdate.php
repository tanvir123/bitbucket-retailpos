<?php

class banquetinventoryupdate
{
    public $module = 'banquetinventoryupdate';
    public $log;
    public $dbconnection;

    public function __construct()
    {
        $this->log=new \util\logger();
    }
    public function banquetinventoryupdate($data)
    {
        try {
            $this->log->logIt($this->module . ' - banquetinventoryupdate');

                $Obj = new \banquet_booking();
                $result = $Obj->updateBanquetItemInventory($data);
                return $result;
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . ' - banquetinventoryupdate - ' . $e);
        }
    }
}
?>
