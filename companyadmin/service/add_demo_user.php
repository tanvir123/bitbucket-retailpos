<?php

class add_demo_user
{
    public $module = 'add_demo_user';
    public $log;
    public $dbconnection;

    public function __construct()
    {
        $this->log=new \util\logger();
    }
    public function add_demo_user($data)
    {
        try
        {

            $this->log->logIt($this->module.' - add_demo_user');
            $t=time();
            $reqarr = array(
                "shortcode" => $t,
                "name" => isset($data['name'])?$data['name']:'',
                "username" => str_replace(" ","",$data['name']).'_'.$t,
                "password" => 'pureipos@123',
                "emailid" => isset($data['emailid'])?$data['emailid']:'',
                "mobileno" => isset($data['mobileno'])?$data['mobileno']:'',
                "id"=>0,
                "storeid"=>'',
                "roleunkid"=>29,
                "usr_type"=>2,
                "usr_location"=>array(32),
                "lang_type"=>0,
                "lang_code"=>'en',
		"sys_defined"=>"0",
                "module" => 'users'
            );
            $ObjDao = new \database\userdao();
            //define('CONFIG_DBN','saas_cloudpos');   //local db
            define('CONFIG_DBN','saas_stag_cloudpos'); //live db
            $res = $ObjDao->addUser($reqarr);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - add_demo_user - '.$e);
        }
    }
}
?>
