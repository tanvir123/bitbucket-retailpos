<?php

class account_info
{
    public $module = 'ac_information';
    public $log;
    private $language, $lang_arr, $default_lang_arr,$objFunctions;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->objFunctions = new \common\functions();
        $this->objFunctions->check_banquet_avilable();
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $this->objFunctions->checkModuleAccess(39);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(39);

            $ObjUserDao = new \database\account_infodao();
            $data = $ObjUserDao->accountinfolist(50,'0', '', 0);

            $this->loadLang();
            $template = $twig->loadTemplate('account_info.html');

            $countrylist = $OBJCOMMONDAO->getCountryList();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['banquet'] = CONFIG_IS_BANQUET;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['countrylist'] = $countrylist;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['currency_sign'] = $OBJCOMMONDAO->getCurrencySign();
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['datalist'] = $data;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['module'] = $this->module;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);

            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function addeditfrm($data)
    {
        try {
            $this->log->logIt($this->module . ' - addeditfrm');

           $flag = \util\validate::check_notnull($data, array('name','alias','fname','lname','rdo_status'));
           $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $langlist = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);
            //  'email','city','contact','address'));
            if ($flag == 'true') {
                $reqarr = array(
                    "name" => $data['name'],
                    "alias" => $data['alias'],
                    "account_type" => $data['account_type'],
                    "ac_type_val" => $data['ac_type_val'],
                    "accountno" => $data['accountno'],
                    "fname" => $data['fname'],
                    "lname" => $data['lname'],
                    "phone" => $data['phone'],
                    "rdo_status" => $data['rdo_status'],
                    "remark" => $data['remark'],
                    "card_no" => $data['card_no'],
                    "reg_no2" => $data['reg_no2'],
                    "reg_no1" => $data['reg_no1'],
                    "reg_no" => $data['reg_no'],
                    "discount_percentage" => $data['discount_percentage'],
                    "discount_amount" => $data['discount_amount'],
                    "payment_term" => $data['payment_term'],
                    "opening_balance" => $data['opening_balance'],
                    "credit_limit" => $data['credit_limit'],
                    "email" => $data['email'],
                    "fax" => $data['fax'],
                    "country" => $data['country'],
                    "zip" => $data['zip'],
                    "state" => $data['state'],
                    "city" => $data['city'],
                    "address2" => $data['address2'],
                    "address1" => $data['address1'],
                    "id" => $data['id'],
                    "module" => $this->module
                );
                $ObjMenuDao = new \database\account_infodao();
                $data = $ObjMenuDao->addeditaccountinfo($reqarr,$langlist,$default_langlist);
                return $data;
            } else {
                return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $default_langlist->Missing_Field)));
            }


        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addeditfrm - ' . $e);
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');
            $limit = 50;
            $offset = 0;
            $name = "";

            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['nm']) && $data['nm'] != "")
                $name = $data['nm'];
            $ObjUserDao = new \database\account_infodao();
            $data = $ObjUserDao->accountinfolist($limit, $offset, $name);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function editclass($data)
    {
        try {
            $this->log->logIt($this->module . " - editclass");
            $ObjMenuDao = new \database\account_infodao();
            $data = $ObjMenuDao->getcatRec($data);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - editclass - " . $e);
            return false;
        }
    }


    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$ac_information;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}

?>