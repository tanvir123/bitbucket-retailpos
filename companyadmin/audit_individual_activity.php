<?php

class audit_individual_activity
{
    private $module='audit_individual_activity';
    private $log;
    private $encdec;
    private $language,$default_lang_arr,$lang_arr;

    function __construct()
    {
        try
        {
            $this->log = new \util\logger();
            $this->language = new \util\language($this->module);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
    public function load($data)
    {
        try
        {
            $this->log->logIt($this->module." - load");
            global $twig;
            $flag = 0;
            $this->loadlaguage();
            //$module_name='$'.$data['module'];
            $this->language = new \util\language($data['module']);
            $Old_lang_arr = \common\staticlang::${$data['module']};
            $this->lang_arr = $this->language->loadlanguage($Old_lang_arr);
            $NewlanguageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $NewlanguageArr=json_decode($NewlanguageArr);

            $default_dis_languageArr=html_entity_decode(json_encode($this->default_lang_arr));
            $default_dis_languageArr=(array)json_decode($default_dis_languageArr);
            if(!isset($data) || !isset($data['id']) || !isset($data['module']))
            {
                $flag==1;
                $Obj="";
            }
            else{
                $id = (isset($data['id']))?$data['id']:"";
                $Objstaticarray =new \common\staticarray();
                $module = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $masterfield = $Objstaticarray->auditlogmodules[$data['module']]['masterfield'];
                $pd = $Objstaticarray->auditlogmodules[$data['module']]['pd'];
                
                $ObjactivityDao = new \database\auditlogdao();
                $data1 = $ObjactivityDao->loadindividualauditlogs($id,$module,$masterfield,$pd,$Old_lang_arr,$NewlanguageArr,$default_dis_languageArr);

                $bj = json_decode($data1,1);
                $Obj = $bj['Data'];
            }
               $this->loadlaguage();
            $template = $twig->loadTemplate('audit_individual_activity.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['flag'] = $flag;
            $senderarr['rec'] = $Obj;
            $senderarr['langlist'] = $this->lang_arr;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['default_langlist'] = $this->default_lang_arr;
            $rec = $template->render($senderarr);
            return json_encode(array("Data"=>$rec));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - load - ".$e);
        }
    }

     public function loadlaguage()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }
   
}
?>