<?php

class user
{
    public $module = 'user';
    public $log;
    public $jsdateformat;
    private $language, $lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $jsdate = 'd-m-y';
            $jsdate = \common\staticarray::$jsdateformat[\database\parameter::getBanquetParameter('dateformat')];
            $ObjStaticArray = new \common\staticarray();
            $arr_lang = $ObjStaticArray->language_codes;
            $ObjDao = new \database\userdao();

            $usr_detail = $ObjDao->getUserDetail();
            $userrole = $ObjDao->getUserRole();
            $storelist = $ObjDao->getstorelist();

            $ObjDao1 = new \database\userroledao();
            $menulist = $ObjDao1->privileges();

            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(23);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(23);

            //$fiscal_setting = $OBJCOMMONDAO->getFiscalsetting();

            $data = $ObjDao->userlist(50, '0', '');
            $template = $twig->loadTemplate('user.html');
            $this->loadLang();
            $senderarr = array();

            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['banquet'] = CONFIG_IS_BANQUET;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['datalist'] = $data;
            $senderarr['module'] = $this->module;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['userrolelist'] = $userrole;
            $senderarr['storelist'] = $storelist;
            $senderarr['location_list'] = $ObjDao->getLocationList();
            $senderarr['menulist'] = $menulist;
            $senderarr['jsdateformat'] = $jsdate;
            $senderarr['sysdefined'] = $usr_detail['sysdefined'];
            $senderarr['user_type'] = $usr_detail['user_type'];
            $senderarr['loginuser'] = CONFIG_UID;
            $senderarr['lang_list'] = $arr_lang;
            $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            //$senderarr['fiscal_setting'] = $fiscal_setting;
            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function addeditfrm($data)
    {
        try {
            $this->log->logIt($this->module . ' - addeditfrm');
            $flag1 = \util\validate::check_notnull($data, array('shortcode', 'name', 'username', 'password', 'emailid', 'mobileno'));

            $this->loadLang();
            $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
            $langlist = json_decode($languageArr);
            $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);

            if ($flag1 == 'true') {
                $reqarr = array(
                    "shortcode" => $data['shortcode'],
                    "name" => $data['name'],
                    "username" => $data['username'],
                    "password" => $data['password'],
                    "emailid" => $data['emailid'],
                    "mobileno" => $data['mobileno'],
                    "id" => $data['id'],
                    "sys_defined" => $data['sys_defined'],
                    "storeid" => isset($data['select_store']) ? $data['select_store'] : 0,
                    "roleunkid" => isset($data['roleunkid'])?$data['roleunkid']:0,
                    "usr_location" => isset($data['slct_location']) ? $data['slct_location'] : 0,
                    "lang_type" => $data['rdo_lang_type'],
                    "lang_code" => ($data['rdo_lang_type'] == 1) ? $data['select_language'] : 'en',
                    "module" => $this->module,
                    "fiscal_tinno" => isset($data['fiscal_tinno'])?$data['fiscal_tinno']:""
                );
                $ObjDao = new \database\userdao();
                $data = $ObjDao->addUser($reqarr, $langlist, $default_langlist);
                return $data;

            } else
                return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $default_langlist->Missing_Field)),ENT_QUOTES);

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addeditfrm - ' . $e);
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');
            $limit = 50;
            $offset = 0;
            $name = "";

            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['nm']) && $data['nm'] != "")
                $name = $data['nm'];
            $ObjDao = new \database\userdao();
            $data = $ObjDao->userlist($limit, $offset, $name);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

   /* code not in use ! User : Nikita Date :24/07/2018
   public function displayPrivileges($data)
    {
        try {
            $this->log->logIt($this->module . " - displayPrivileges");

            $ObjUserDao = new \database\userdao();
            $res = $ObjUserDao->displayPrivileges($data);
            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - displayPrivileges - " . $e);
            return false;
        }
    }*/

    public function getUserRec($data)
    {
        try {
            $this->log->logIt($this->module . " - getUserRec");
            $ObjDao = new \database\userdao();
            $data = $ObjDao->getUserRec($data);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getUserRec - " . $e);
            return false;
        }
    }

    public function change_password($data)
    {
        try {
            $this->log->logIt($this->module . " - change_password");
            $flag1 = \util\validate::check_notnull($data, array('newpwd', 'id'));
            $this->loadLang();
            $languageArr = html_entity_decode(json_encode($this->lang_arr), ENT_QUOTES);
            $langlist = json_decode($languageArr);
            $defaultlanguageArr = html_entity_decode(json_encode($this->default_lang_arr), ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);
            $data['module'] = $this->module;
            if ($flag1 == 'true') {
                $ObjUserDao = new \database\userdao();
                $res = $ObjUserDao->changePassword($data, $langlist, $default_langlist);
                return $res;
            } else {
                return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $default_langlist->Missing_Field)),ENT_QUOTES);
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - change_password - " . $e);
            return false;
        }
    }


    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$user;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}

?>
