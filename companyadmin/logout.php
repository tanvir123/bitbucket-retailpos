<?php
    require_once "config/config.php";
    require_once "util/logger.php";
    require_once "common/functions.php";
    require_once "common/encdec.php";
    $domain = ($_SERVER['HTTP_HOST'] != '192.168.0.42') ? $_SERVER['HTTP_HOST'] : false;
    $ObjFunctions = new \common\functions();
    $rec = $ObjFunctions->removeToken($_COOKIE['ADMIN_CONFIG_ATID']);

    setcookie("ADMIN_CONFIG_ATID","", time()-3600, "/",$domain,false);
    setcookie("ADMIN_CONFIG_UN","", time()-3600, "/",$domain,false);
    header("location:index.php");
?>