$(document).ready(function(){

  $("input:radio[name=radio-navbar]").bind("click", function() {
    var value;

      
    value = $(this).val();

    if (value === "0") {
      return $("#navbar").addClass("navbar-default").removeClass("navbar-inverse");
    } else if (value === "1") {
      return $("#navbar").removeClass("navbar-default").addClass("navbar-inverse");
    }
  });
  
  $("input:radio[name=radio-sidebar]").bind("click", function() {
    var value;
    value = $(this).val();
    if (value === "0") {
      return $("#sidebar").removeClass("sidebar-inverse");
    } else if (value === "1") {
      return $("#sidebar").addClass("sidebar-inverse");
    }
  });
  
  $("input:radio[name=radio-color]").bind("click", function() {

      var value;
      value = $(this).val();

      $("body").removeAttr("class");
      $("body").addClass("flat-"+value);
  });
  
  $("input:radio[name=radio-expand]").bind("click", function() {
    var value;
    value = $(this).val();
    if (value === "0") {
        $(".app-container").removeClass("expanded");
        $(".navbar-expand-toggle").removeClass("fa-rotate-90");
    } else if (value === "1") {
        $(".app-container").addClass("expanded");
        $(".navbar-expand-toggle").addClass("fa-rotate-90");
    }
  });
  
  
});

