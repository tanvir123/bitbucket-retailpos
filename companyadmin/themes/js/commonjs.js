var default_maxNumFiles = default_minNumFiles = default_maxTotalSize = default_fileTypes = default_maxFileSize = default_minFileSize = default_preview_container  = default_minWidth = default_minHeight = default_maxWidth = default_maxHeight = false;

var pageno=1;
var pagereset=true;
$(document).ready(function(){
    jQuery("#logout").click(function(){
        jQuery.ajax({
            url: "controller",
            type: "post",
            data: "service=logout&opcode=load",
            success: function(data){
                if(data == 1)
                {
                    window.location = "../index.php";
                }
            }
        });
    });
    jQuery('body').on('change','input[type="file"]',function(event){

        var files = event.target.files;

        if(files.length > 0){

            var inputFile = jQuery(this);
            var fileTypes    = jQuery(this).data('file-types') || default_fileTypes;
            var maxFileSize  = jQuery(this).data('max-file-size') || default_maxFileSize;
            var minFileSize  = jQuery(this).data('min-file-size') || default_minFileSize;
            var maxTotalSize = jQuery(this).data('max-total-size') || default_maxTotalSize;
            var maxNumFiles  = jQuery(this).data('max-num-files') || default_maxNumFiles;
            var minNumFiles  = jQuery(this).data('min-num-files') || default_minNumFiles;
            var minWidth  = jQuery(this).data('min-width') || default_minWidth;
            var minHeight  = jQuery(this).data('min-height') || default_minHeight;
            var maxWidth  = jQuery(this).data('max-width') || default_maxWidth;
            var maxHeight  = jQuery(this).data('max-height') || default_maxHeight;

            var preview_container  = jQuery(this).data('preview-container') || default_preview_container;
            var thumb_container  = jQuery(this).data('thumb-preview') || default_preview_container;
            var related_input  = jQuery(this).data('related-input') || default_preview_container;

            jQuery(inputFile).attr('data-base64','');
            if(preview_container && jQuery(preview_container).length > 0)
            {
                jQuery(preview_container).html('');
            }
            if(related_input && jQuery('input[name^="'+related_input+'"]').length > 0)
            {
                jQuery('input[name^="'+related_input+'"]').val('');
            }
            var currentName = inputFile[0].id;
            if(jQuery(thumb_container).length > 0)
            {
                jQuery(thumb_container).attr('src',siteUrl+'assets/img/placeholder.png');
            }
            if(maxNumFiles && files.length > maxNumFiles) {
                this.value = '';
                $(this).trigger('fileSelectionError', maxNumFiles +' files are allowed at most!',this);
                return abort(inputFile);
            }

            // Check min file number
            if(minNumFiles && this.files.length < minNumFiles) {
                this.value = '';
                $(this).trigger('fileSelectionError', 'Have to select at least '+ minNumFiles +' files!',this);
                return abort(inputFile);
            }

            // Check max total size
            if(maxTotalSize) {
                var totalSize = 0;
                for (var x = 0; x < this.files.length; x++) {
                    totalSize += this.files[x].size;
                }
                if(totalSize > maxTotalSize) {
                    this.value = '';
                    $(this).trigger('fileSelectionError', 'Total size of selected files should not exceed '+ maxTotalSize +' byte!',this);
                    return abort(inputFile);
                }
            }

            // Check file-wise restrictions
            for (var x = 0; x < this.files.length; x++) {

                var file = this.files[x];

                // Check file type
                if((fileTypes && fileTypes.indexOf(file.type) < 0) || file.type=='' ) {
                    this.value = '';
                    $(this).trigger('fileSelectionError', file.name +' : File type is not allowed!',this);
                    return abort(inputFile);
                }

                // Check max size

                if(maxFileSize && (file.size/1024).toFixed(2) > maxFileSize) {
                    this.value = '';
                    $(this).trigger('fileSelectionError', file.name +' : Exceeding maximum allowed file size!',this);
                    return abort(inputFile);
                }

                // Check min size
                if(minFileSize && (file.size/1024).toFixed(2) < minFileSize) {
                    this.value = '';
                    $(this).trigger('fileSelectionError', file.name +' : Smaller than minimum allowed file size!',this);
                    return abort(inputFile);
                }
                if(minWidth)
                {
                    var image = new Image();

                    image.onload = function() {
                        if(this.width < minWidth)
                        {

                            inputFile.value = '';
                            $(inputFile).trigger('fileSelectionError', file.name +' : Image need minimum '+minWidth+'px width !',inputFile);
                            return abort(inputFile);
                        }

                    };
                    image.src = _URL.createObjectURL(file);

                }

                if(minHeight )
                {
                    var image = new Image();
                    image.onload = function() {
                        if(this.height < minHeight)
                        {

                            inputFile.value = '';

                            $(inputFile).trigger('fileSelectionError', file.name +' : Image need minimum '+minHeight+'px height !',inputFile);
                            return abort(inputFile);
                            return false;
                        }

                    };
                    image.src = _URL.createObjectURL(file);

                }

                if(maxWidth )
                {
                    var image = new Image();
                    image.onload = function() {
                        if(this.width > maxWidth)
                        {

                            inputFile.value = '';

                            $(inputFile).trigger('fileSelectionError', file.name +' : Image can be maximum '+maxWidth+'px height !',inputFile);
                            return abort(inputFile);
                            return false;
                        }

                    };
                    image.src = _URL.createObjectURL(file);

                }

                if(maxHeight && (file.height < maxHeight))
                {
                    var image = new Image();
                    image.onload = function() {
                        if(this.height > maxHeight)
                        {

                            inputFile.value = '';

                            $(inputFile).trigger('fileSelectionError', file.name +' : Image can be maximum '+maxHeight+'px height !',inputFile);
                            return abort(inputFile);
                            return false;
                        }

                    };
                    image.src = _URL.createObjectURL(file);
                }
            }
            var totalFiles = files.length;
            var currentFile = 0;
            var tempArray = {};
            for(var i=0; i< files.length; i++)
            {
                var reader = new FileReader();

                var tempName  = files[i].name;
                reader._NAME = tempName;
                reader.onload = function(e) {

                    tempArray[e.target._NAME] = e.target.result;
                    if(currentFile==0 && jQuery('#preview_'+currentName).length > 0)
                    {
                        jQuery('#preview_'+currentName).attr('src',e.target.result);
                    }
                    if(jQuery(thumb_container).length > 0)
                    {
                        jQuery(thumb_container).attr('src',e.target.result);
                    }
                    if(preview_container && jQuery(preview_container).length > 0 )
                    {
                        jQuery(preview_container).append('<div  class="inner_preview_box" data-key-name="'+e.target._NAME+'" data-parent-input="'+currentName+'"><img src="'+e.target.result+'" width="100" height="100" /><a href="javascript:void(0);" class="removeSelectedImage"><i class="fa fa-close"></i></a></div>');
                    }
                    currentFile++;
                    if(currentFile==totalFiles)
                    {
                        jQuery(inputFile).attr('data-base64',JSON.stringify(tempArray));

                    }
                };
                reader.readAsDataURL(files[i]);
                reader.onerror = function() {

                };
            }
        }
    }) ;



    jQuery('body').on('fileSelectionError','input[type="file"]', fileSelectionErrorHandler);
    jQuery('body').on('click','.removeSelectedImage',function(){
        var closestform = jQuery(this).closest('form');
        var parent = jQuery(this).parent();
        var keyName = jQuery(closestform).find(parent).data('key-name');
        var parentName = jQuery(closestform).find(parent).data('parent-input');
        if(jQuery('#'+parentName).length > 0)
        {
            var base64Data = jQuery(closestform).find('#'+parentName).data('base64');
            if(typeof base64Data == 'string')
            {
                base64Data = JSON.parse(base64Data);
            }

            if(base64Data.hasOwnProperty(keyName))
            {
                delete base64Data[keyName];
            }

            if(jQuery.isEmptyObject(base64Data) == true){
                jQuery(closestform).find('#'+parentName).val("");
            }
            jQuery(closestform).find('#'+parentName).attr('data-base64',JSON.stringify(base64Data));
            jQuery(closestform).find(parent).remove();
        }
    });
});

function handleFileSelect(evt) {
    var files = evt.target.files;
    var inputFile = jQuery(this);
    var currentName = inputFile[0].id;
    for (var i = 0, f; f = files[i]; i++) {
        if (!f.type.match('image.*')) {
            continue;
        }
     //   jQuery('#preview_div_simple_image').html('');
        var reader = new FileReader();
        reader.onload = (function (theFile) {
            return function (e) {
                jQuery('#preview_div_simple_image').append('<span class="newimageadd" id="span_'+theFile.name+'"><img class="thumb" height="100" width="100" src="'+e.target.result+'"/><i class="fa fa-close" onClick="remove_current_img(this);" data-id="'+theFile.name+'" data-image-name="'+theFile.name+'" data-name="'+currentName+'"></i></span>');
            };
        })(f);
        reader.readAsDataURL(f);
    }
}
//end of textarea pluding code

function remove_img(current){
    var img_number = jQuery(current).attr('data-num');
    jQuery(current).parents('span').remove();

    if(typeof img_number != 'undefined'){
        var img_name_prefix = jQuery(current).attr('data-prefix');
        jQuery('#'+img_name_prefix+img_number).remove();
    }
}


function attachProperties (target, properties, value)
{
    var currentTarget = target;
    var propertiesNum = properties.length;
    var lastIndex = propertiesNum - 1;

    for (var i = 0; i < propertiesNum; ++i)
    {
        currentProperty = properties[i];

        if (currentTarget[currentProperty] === undefined)
        {
            currentTarget[currentProperty] = (i === lastIndex) ? value : {};
        }

        currentTarget = currentTarget[currentProperty];
    }
}


function remove_current_img(current){
    var img_name=jQuery(current).attr('data-name');
    var data_img_name=decodeURIComponent(jQuery(current).attr('data-image-name'));
    if(typeof img_name != 'undefined' && typeof data_img_name != 'undefined') {
        var get_val = jQuery('#'+img_name).attr('data-base64');
        if(typeof get_val != 'undefined'){
            var get_arr= jQuery.parseJSON(get_val);
            delete get_arr[data_img_name];
            deletedimage.push(data_img_name);
            jQuery(current).parents('span').remove();
            jQuery('#'+img_name).attr('data-base64',JSON.stringify(get_arr));
        }
    }
}


function extractFieldNames (fieldName, expression, keepFirstElement)
{
    expression = expression || /([^\]\[]+)/g;
    keepFirstElement = true;

    var elements = [];
    while((searchResult = expression.exec(fieldName)))
    {   elements.push(searchResult[0]);
    }

    if (!keepFirstElement && elements.length > 0) elements.shift();

    return elements;
}

function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

function abort (input) {

    jQuery(input).attr('data-base64','');
    var preview_container  = jQuery(input).data('preview-container') || default_preview_container;
    if(preview_container && jQuery(preview_container).length > 0)
    {
        jQuery(preview_container).html('');
    }
    var currentName = jQuery(input).attr('id');
    if( jQuery('#preview_'+currentName).length > 0)
    {
        var current_src=jQuery('#preview_'+currentName).attr('src');
        if(current_src==''){
            jQuery('#preview_'+currentName).attr('src',siteUrl+'assets/img/placeholder.png');
        }
    }
    jQuery(input).val('');
    return false;
};

function fileSelectionErrorHandler (event, message,inputFile) {
    alertify.error(message);
    //abort(inputFile);
}

function showjQloading() {
    jQuery(".loader1").show();
}
function hidejQloading(){
    jQuery(".loader1").hide();
}

function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('<html><head><title>' + document.title  + '</title>');

    mywindow.document.write('</head><body >');

    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}

function check_audit_activity(id,module)
{
    try
    {
        var arr = {};
        arr['service']= 'audit_individual_activity';
        arr['opcode']= 'load';
        arr['id']= id;
        arr['module']= module;
        var str = urlencode(JSON.stringify(arr));
        HttpSendRequest(str,function(data)
        {
            jQuery("#modalPrimary").html(data.Data);
            jQuery("#modalPrimary").modal('show');
        });
    }
    catch(e)
    {
        alert(e);
    }
}

function check_related_records(id,module)
{
    var arr = {};
    arr['service']= 'check_dependency';
    arr['opcode']= 'load';
    arr['id']= id;
    arr['module']= module;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str,function(data)
    {
        jQuery("#modalPrimary").html(data.Data);
        jQuery("#modalPrimary").modal('show');
    });
}
function displaysettings_activity(module)
{
    try
    {
        var arr = {};
        arr['service']= 'displaysettings_activity';
        arr['opcode']= 'load';
        arr['module']= module;
        var str = urlencode(JSON.stringify(arr));
        HttpSendRequest(str,function(data)
        {
            jQuery("#modalPrimary").html(data.Data);
            jQuery("#modalPrimary").modal('show');
        });
    }
    catch(e)
    {
        alert(e);
    }
}
$(document).ready(function() {
    $(".integer").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 45 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(".decimal").keypress(function (event) {
        var value = $(this).val();
        if (event.which == 45 || event.which > 58 || event.which == 47 || event.which == 42 || event.which == 43) {
            return false;
            event.preventDefault();
        } // prevent if not number/dot
        if (event.which == 46 && value.indexOf('.') != -1) {
            return false;
            event.preventDefault();
        } // prevent if already dot
        return true;
    });
});
function escapeHtml(text) {
    if(isNaN(text)){
        return text.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">")
            .replace(/&quot;/g, '"').replace(/&#039;/g, "'").replace(/&pound;/g, "£")
            .replace(/&euro;/g,"€");
    }else{
        return text;
    }
}

unscaping = function(obj) {
    if ($.isArray(obj)) {
        var str = [];
    } else {
        var str = {};
    }
    var  p;
    for(p in obj) {
        if (obj.hasOwnProperty(p)) {
            v = obj[p];
            if(v !== null && (typeof v === "object" || $.isArray(v))){
                str[p] = unscaping(v);
            }else{
                str[p] = escapeHtml(v);
            }
        }
    }
    return str;
}

function urldecode (str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}
function urlencode (str) {
    return encodeURIComponent(str);
}
function HttpSendRequest($data,action)
{
    $.ajax({
        type: 'POST',
        url: 'controller',
        data: $data,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        success: function(data) {
            if(data.Success=='Expired'){
                location.reload();
            }else{
                var rec = unscaping(data);
                return action(rec);
            }
        },
    });
}
function CallAuthenticatedApi($url,$data,$headers,action)
{
    var authToken = jQuery.getAthtkn();
    $.ajax({
        type: 'POST',
        url: $url,
        data: $data,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(request) {
            request.setRequestHeader("Content-Type", "application/json");
            request.setRequestHeader("app-id", app_id);
            request.setRequestHeader("app-secret", app_secret);
            request.setRequestHeader("auth-token", authToken);
        },
        error: function(data) {
            if(data.status == 401)
            {
                jQuery.removeAuthtkn();
                window.location = siteUrl+"login.php";
            }
            var errObj = JSON.parse(data.responseText);
            callback_error(action,data);
        },
        success: function(data) {

            var res = action(data);
            return res;
        },
    });
}
function validemail(email){
    var regex = /^([a-zA-Z0-9_\.])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.exec(email)) {
        return 0;
    }
    else {
        return 1;
    }
    // var atpos = email.indexOf("@");
    // var dotpos = email.lastIndexOf(".");
    // if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
    //    return 0;
    // }
    // else{
    //    return 1;
    // }
}

function toggleactive(id,module,value) {
    var arr = {};
    arr['service']= 'common';
    arr['opcode']= 'toggleststus';
    arr['module']= module;
    arr['id']= id;
    arr['value']= value;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str,function(data)
    {
        if (data['Success'] == "True") {
            refreshlist();
            alertify.success(data['Message']);
        }
        else{
            alertify.error(data['Message']);
        }
    });
}

function remove(id,module,msg,cancel,ok){
    var labelsprint = {
        ok     : ok,
        cancel : cancel
    };
    alertify.confirm(msg,labelsprint, function (e) {
        if (e) {
            var arr = {};
            arr['service']= 'common';
            arr['opcode']= 'remove';
            arr['id']= id;
            arr['module']= module;
            var str = urlencode(JSON.stringify(arr));
            if (arr['id'] != '') {
                HttpSendRequest(str,function(data){
                    if (data['Success'] == "True") {
                        refreshlist();
                        alertify.success(data['Message']);
                    }
                    else{
                        alertify.error(data['Message']);
                    }
                });
            }
        } else {
            return false;
        }
    });
}

function validate_form(id){
    var error = 0;
    $('.invalid').removeClass('invalid');
    $("#"+id).find(".notnull").each(function(){
        if($(this).val()==""){
            error = 1;
            $(this).addClass('invalid');
        }
    });
    $("#"+id).find(".dropdown").each(function(){
        if($(this).val()=="" || $(this).val()==0){
            error = 1;
            $(this).addClass('invalid');
        }
    });
    $("#"+id).find(".integer").each(function(){
        if($(this).val()!=""){
            var regex = /^[0-9]+$/;
            var input = $(this).val();
            if(!regex.test(input)){
                error = 1;
                $(this).addClass('invalid');
            }
        }
    });
    $("#"+id).find(".decimal").each(function(){
        if ($(this).val()!="") {
            //var dec = /^[-+]?[0-9]+\.[0-9]+$/;
            var regex = /^\d+(\.\d{1,10})?$/;
            var input = $(this).val();
            if (!regex.test(input)) {
                error = 1;
                $(this).addClass('invalid');
            }
        }
    });
    $("#"+id).find(".email").each(function(){
        if($(this).val()!=""){
            var check = validemail($(this).val());
            if(check==0){
                error = 1;
                $(this).addClass('invalid');
            }
        }
    });
    if (error == 1)
        return false;
    else
        return true;
}
function cleanstring(rec) {
    var data = rec.replace(/(\r\n|\n|\r)/gm,"\\n");
    return data;
}
function loadreportmodal(data) {
    var str = '<div class="modal-dialog modal-lg"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> ';
    str += ' <span aria-hidden="true">&times;</span> </button> <h4 class="modal-title" id="myModalLabel">Print</h4>';
    str += ' </div> <div class="col-xs-12 modal-body" style="max-height: 550px;overflow-y: auto;"><div class="row"> <button class="btn btn-primary pull-right mr10 mb10" onclick="PrintElem(\'print_content\')">Download</button></div><div class="card" id="print_content"> ';
    str += data;
    str += ' </div> </div> </div> </div> ';
    jQuery("#modalPrimary").html(str);
    jQuery("#modalPrimary").modal('show');
}
function setchaitolocal(chain,chainhotel)
{
    localStorage.setItem('chain',chain);
    var data_enc = $('<textarea />').html(chainhotel).text();
    localStorage.setItem('hotelchain',data_enc);
}
function switch_hotel(id){
    var arr = {};
    arr['service'] = "common";
    arr['opcode'] = "switch_hotel";
    arr['companyid'] = id;
    var str = urlencode(JSON.stringify(arr));
    HttpSendRequest(str, function (data) {
        if (data['Success']=="True") {
            window.location= "amenities";
        }
        else{
            alertify.error(data['Message']);
            var companyid = localStorage.getItem('FRONT_HID');
            jQuery("#hotel_chain_switch").val(companyid);
        }
    });
}
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function tagvalidate() {

}

function loadModal(data, module) {
    var modal = parseInt(jQuery("#lid_modal").val());
    var modalid = modal + 1;
    jQuery("#lid_modal").val(modal + 1);
    jQuery("#modal_block").append('<div data-backdrop="static" data-keyboard="false" id="modal_container_' + (modal + 1) + '" class="modal fade" modal_id="' + (modal + 1) + '" modal_module="' + module + '"></div>');
    jQuery("#modal_container_" + (modal + 1)).html(data);
    jQuery("#modal_container_" + (modal + 1)).append('<input type="hidden" name="modalid" id="modalid" value="' + (modal + 1) + '"/>');
    jQuery("#modal_container_" + (modal + 1)).modal('show');
    jQuery("#modal_container_" + (modal + 1)).find(".close").attr("onclick", "closeModal('" + (modal + 1) + "')");
    //loadModalList(module, modalid);
}

function closeModal(modalid) {
    modalCnt = 0;
    var modal = parseInt(modalid);
    jQuery("#lid_modal").val(modal - 1);
    jQuery("#modal_container_" + (modal)).modal('hide');
    setTimeout(function () {
        jQuery("#modal_container_" + (modal)).remove()
    }, 500);

}

function isFloat(event, value) {
    if (event.which == 45 || event.which > 58 || event.which == 47 || event.which == 42 || event.which == 43) {
        return false;
        event.preventDefault();
    }
    if (event.which == 46 && value.indexOf('.') != -1) {
        return false;
        event.preventDefault();
    }
    return true;
}
function isFloatWithRange(event, value,min,max) {

    var currentVal = jQuery(value).val();

    if(parseFloat(currentVal) < parseFloat(min))
    {
        jQuery(value).val(min);
        return false;
        event.preventDefault();
    }


    if( parseFloat(currentVal) > parseFloat(max))
    {
        if(typeof round_off_digit != 'undefined' && round_off_digit !='') {
            jQuery(value).val(parseFloat(max).toFixed(round_off_digit));
        }else{
            jQuery(value).val(max);
        }
        return false;
        event.preventDefault();
    }

    if (event.which == 45 || event.which > 58 || event.which == 47 || event.which == 42 || event.which == 43) {

        if(typeof round_off_digit != 'undefined' && round_off_digit !='' && parseFloat(currentVal) > 0 && currentVal.indexOf('.')!= -1)
        {
            var nArray = currentVal.split('.');
            if(typeof nArray[1] != 'undefined' && nArray[1].length > round_off_digit)
            {
                jQuery(value).val(parseFloat(currentVal).toFixed(round_off_digit));
            }
        }
        return false;
        event.preventDefault();
    }

    if (event.which == 46 && currentVal.indexOf('.') != -1) {

        return false;
        event.preventDefault();
    }

    if(typeof round_off_digit != 'undefined' && round_off_digit !='' && parseFloat(currentVal) > 0 && currentVal.indexOf('.')!= -1)
    {
        var nArray = currentVal.split('.');
        if(typeof nArray[1] != 'undefined' && nArray[1].length > round_off_digit)
        {
            jQuery(value).val(parseFloat(currentVal).toFixed(round_off_digit));
        }
    }

    return true;
}

/*-- Loader --*/
$(document).ready(function () {
    $(".loader1").fadeOut("slow");
});


function load_report(rec) {
    HttpSendRequest(rec, function (data) {
        if (data.Success == "True") {
            jQuery(window).scrollTop(0);
            jQuery("#content").hide();
            jQuery("#report_content").show();
            jQuery("#print_content").css({"width": data.Width, "min-height": data.Height});
            jQuery("#print_content").html(data.Data);
        }
    });
}

function Exitinvoice() {
    jQuery("#content").show();
    jQuery("#report_content").hide();
    jQuery("#print_content").empty();
}

function toggle_status_onchange(id,module,oldvalue,newvalue,msg,cancel,ok,ths,active,inactive,banned)
{
    var labelsprint = {
        ok     : ok,
        cancel : cancel
    };

    if(newvalue == 1)
    {
        var m = active;
    }
    if(newvalue == 0)
    {
        var m = inactive;
    }
    if(newvalue == 2)
    {
        var m = banned;
    }
    var msgk = msg+" "+m+" "+ths;

    alertify.confirm(msgk,labelsprint, function (e) {
        if(e) {
            var arr = {};
            arr['service'] = 'common';
            arr['opcode'] = 'statusonchnage';
            arr['module'] = module;
            arr['id'] = id;
            arr['oldvalue'] = oldvalue;
            arr['newvalue'] = newvalue;
            var str = urlencode(JSON.stringify(arr));

            HttpSendRequest(str, function (data) {
                if (data['Success'] == "True") {
                    refreshlist();
                    alertify.success(data['Message']);
                }
                else {
                    alertify.error(data['Message']);
                }
                return false;
            });
        }

        else {
            refreshlist();
            return false;
        }
    });

}