<?php
class displaysettings   
{
    public $module='bmdisplaysettings';
    public $log;
    public $jsdateformat;
    private $language,$lang_arr,$default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(45);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(45);

            $ObjDao = new \database\displaysettingsdao();
			$data = $ObjDao->loaddisplaysettingslist();
            $data_arr = json_decode($data,true);
            $ObjStaticArray =new \common\staticarray();

            $timezoneslist = $ObjStaticArray->timezones;
            $default_dateformat = $ObjStaticArray->default_dateformat;
            $default_timeformat = $ObjStaticArray->default_timeformat;

            $template = $twig->loadTemplate('displaysettings.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['banquet'] = CONFIG_IS_BANQUET;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['timezoneslist'] = $timezoneslist;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['default_dateformat'] = $default_dateformat;
            $senderarr['default_timeformat'] = $default_timeformat;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['module'] = $this->module;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function loaddisplaysettingslist($data)
    {
        try {
            $this->log->logIt($this->module . " - editclass");
            $ObjMenuDao = new \database\displaysettingsdao();
            $data = $ObjMenuDao->loaddisplaysettingslist($data);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - editclass - " . $e);
            return false;
        }
    }

    public function editdisplaysettings($data)
    {
        try
        {
            $this->log->logIt($this->module.' - editdisplaysettings');
            if(CONFIG_IS_BANQUET==1){
                $flag = \util\validate::check_notnull($data, array('interval','timeformat','dateformat','timezone','digitafterdecimal'));
            }
            else{
                $flag = \util\validate::check_notnull($data, array('timeformat','dateformat','timezone','digitafterdecimal'));
            }
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $languageArr = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);
            if ($flag == 'true') {
                $reqarr = array(
                    "interval"=>isset($data['interval'])?$data['interval']:"",
                    "location_status"=>isset($data['location_status'])?$data['location_status']:"",
                    "timeformat"=>isset($data['timeformat'])?$data['timeformat']:"",
                    "dateformat"=>isset($data['dateformat'])?$data['dateformat']:"",
                    "timezone"=>isset($data['timezone'])?$data['timezone']:"",
                    "discount"=>isset($data['discount'])?$data['discount']:"",
                    "digitafterdecimal"=>isset($data['digitafterdecimal'])?$data['digitafterdecimal']:"",
                     "userid"=> CONFIG_UID,
                     "companyid"=> CONFIG_CID,
                     "module" => $this->module
                    );
                $ObjDao = new \database\displaysettingsdao();
                $result = $ObjDao->editDisplaysettings($reqarr,$languageArr);

				return $result;
            } else {
                return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $default_langlist->Missing_Field)));
            }
             
        }catch(Exception $e){
            $this->log->logIt($this->module.' - editdisplaysettings - '.$e);
        }
    }

     public function loadLang()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$bmdisplaysettings;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }


}
?>