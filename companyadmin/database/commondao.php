<?php

namespace database;
class commondao
{
    public $module = 'DB_commondao';
    public $log;
    public $dbconnect;
    private $language, $default_lang_arr,$defaultlanguageArr;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language();
        $this->default_lang_arr = $this->language->loaddefaultlanguage();
        $this->defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
        $this->defaultlanguageArr = json_decode($this->defaultlanguageArr);
    }

    public function getCountryList()
    {
        try {
            $this->log->logIt($this->module . ' - getCountrylist');
            $dao = new \dao();
            $strSql = "SELECT id, countryName FROM " . CONFIG_DBN . ".vwcountry";
            $dao->initCommand($strSql);
            $res = $dao->executeQuery();
            $retarray = array();
            foreach ($res AS $key => $value) {
                $retarray[$value['id']] = $value['countryName'];
            }

            return $retarray;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getCountryList - ' . $e);
        }
    }

    public function toggleStatus($data)
    {
        try {
            $this->log->logIt($this->module . ' - toggleststus');
            $dao = new \dao();

            $ObjAuditDao = new \database\auditlogdao();
            if (count($data) > 0 && isset($data['module']) && isset($data['id'])) {
                $datetime = \util\util::getLocalDateTime();
                $Objstaticarray = new \common\staticarray();
                $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $pd = $Objstaticarray->auditlogmodules[$data['module']]['pd'];
                $cn = $Objstaticarray->auditlogmodules[$data['module']]['status_cn'];
                $ObjDependencyDao = new \database\dependencydao();
                $id = $this->getprimarykey($table, $data['id'], $pd);
                $object='';
                if(isset($data['object']) && $data['object']!=''){
                    $object=$data['object'];
                }

                if ($data['value'] == 1) {
                    $check = $ObjDependencyDao->checkstatusdependency($data['module'], $id, 1, 1);
                    if ($check > 0) {
                        return json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->PLZ_CHANGE_STATUS_REL_REC));
                    }
                }
                $arr_log = array(
                    'Old Value' => ($data['value'] == 1) ? 'Active' : 'Inactive',
                    'New Value' => ($data['value'] == 1) ? 'InActive' : 'Active',
                );
                $json_data = json_encode($arr_log);
                $strSql = "UPDATE ".CONFIG_DBN.".".$table. " SET modified_user=:modified_user, modifieddatetime=:modifieddatetime, " . $cn . " = IF(" . $cn . "=1,0,1) WHERE hashkey=:id AND companyid=:companyid ";
                $dao->initCommand($strSql);
                $dao->addParameter(":id", $data['id']);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->executeNonQuery();
               /* if($data['module']=='store'){
                    $strSql = "UPDATE sysstoremaster SET is_active = IF(is_active=1,0,1) WHERE sysstoreid=:id AND companyid=:companyid ";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":id", $id);
                    $dao->addParameter(":companyid", CONFIG_CID);
                    $dao->executeNonQuery();
                }*/

                $ObjAuditDao->addactivitylog($data['module'], 'Change Status', $data['id'], $json_data,$object);
                return json_encode(array("Success" => "True", "Message" => $this->defaultlanguageArr->STATUS_CHANGE_SUCCESS));
            } else
                return json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->INTERNAL_ERROR));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - toggleststus - ' . $e);
        }
    }

    public function removeRecord($data)
    {
        try {
            $this->log->logIt($this->module . ' - removeRecord');
            $dao = new \dao();
            $ObjDependencyDao = new \database\dependencydao();
            $ObjAuditDao = new \database\auditlogdao();
            if (count($data) > 0 && isset($data['module']) && isset($data['id'])) {
                $datetime = \util\util::getLocalDateTime();
                $Objstaticarray = new \common\staticarray();
                $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $pd = $Objstaticarray->auditlogmodules[$data['module']]['pd'];
                $cn = $Objstaticarray->auditlogmodules[$data['module']]['delete_cn'];
                $id = $this->getprimarykey($table, $data['id'], $pd);
                $check = $ObjDependencyDao->checkdependency($data['module'], $id,1);
                $object='';
                if(isset($data['object']) && $data['object']!=''){
                    $object=$data['object'];
                }
                if ($check > 0) {
                    return json_encode(array("Success" => "False", "Message" =>$this->defaultlanguageArr->PLZ_REM_REL_RECORD));
                }
                $strSql = "UPDATE " . CONFIG_DBN . "." . $table . " SET " . $cn . "=1,modified_user=:modified_user,modifieddatetime=:modifieddatetime WHERE hashkey=:id AND companyid=:companyid ";
                $dao->initCommand($strSql);
                $dao->addParameter(":id", $data['id']);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'], 'Delete Record', $data['id'], '',$object);
                return json_encode(array("Success" => "True", "Message" => $this->defaultlanguageArr->REM_REC_SUC));
            } else
                return json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->INTERNAL_ERROR));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - removeRecord - ' . $e);
        }
    }

    public function getprimarykey($tablename, $hashkey, $field)
    {
        try {
            $this->log->logIt($this->module . ' - getprimarykey');
            $dao = new \dao;
            

            $strSql = "SELECT " . $field . " AS field FROM " . CONFIG_DBN . "." . $tablename . " WHERE hashkey=:hashkey AND companyid=:companyid ";

            $dao->initCommand($strSql);
            $dao->addParameter(":hashkey", $hashkey);
            $dao->addParameter(':companyid', CONFIG_CID);
            $data = $dao->executeRow();

            return $data['field'];
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getprimarykey - ' . $e);
        }

    }

    public function getFieldValue($table, $column, $field, $value)
    {
        try {
            $this->log->logIt($this->module . ' - getFieldValue');
            $dao = new \dao;
            $strSql = "SELECT " . $column . " as field FROM " . CONFIG_DBN . "." . $table . " WHERE " . $field . "=:value AND companyid=:companyid ";
            $dao->initCommand($strSql);
            $dao->addParameter(":value", $value);
            $dao->addParameter(':companyid', CONFIG_CID);

            $data = $dao->executeRow();
            return $data['field'];
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getFieldValue - ' . $e);
        }
    }

    public function getuserprivongroup($groupidforprivleges)
    {
        try {
            $this->log->logIt($this->module . ' - getuserprivongroup');
            $dao = new \dao();

            if (CONFIG_USR_TYPE != 1) {
                $strSql = "SELECT GROUP_CONCAT(DISTINCT(CRP.lnkprivilegeid),'') as lnkprivilegegroupid 
                        FROM " . CONFIG_DBN . ".cfroleprivileges as CRP 
                       INNER JOIN sysprivilegegroup as SPG ON CRP.lnkprivilegegroupid=SPG.privilegegroupunkid 
                      WHERE  CRP.companyid=:companyid
                      AND lnkprivilegegroupid=:lnkprivilegegroupid AND lnkroleid=:lnkroleid AND SPG.type=1";

                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':lnkroleid', CONFIG_RID);
                $dao->addParameter(':lnkprivilegegroupid', $groupidforprivleges);
                $res = $dao->executeRow();
            }
            else{
                $strSql = "SELECT GROUP_CONCAT(DISTINCT(SP.id),'') as lnkprivilegegroupid 
                        FROM sysprivileges as SP 
                       INNER JOIN sysprivilegegroup as SPG ON SP.privilegegroupid=SPG.privilegegroupunkid 
                      WHERE privilegegroupid=:lnkprivilegegroupid AND SPG.type=1";

                $dao->initCommand($strSql);
                $dao->addParameter(':lnkprivilegegroupid', $groupidforprivleges);
                $res = $dao->executeRow();
            }

            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getuserprivongroup - ' . $e);
        }

    }

    public function countryList()
    {
        try {
            $this->log->logIt($this->module.' - countryList ');
            $dao = new \dao;
            $strSql = "SELECT * FROM ".CONFIG_DBN.".vwcountry ORDER BY countryName ASC";
            $dao->initCommand($strSql);

            $data = $dao->executeQuery();
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module.' - countryList - '.$e);
        }
    }

    public function getCurrencySign()
    {
        try {
            $this->log->logIt($this->module . ' - getCurrencySign');
            $dao = new \dao();
            $strSql = "SELECT currency_sign FROM " . CONFIG_DBN . ".cfcompany WHERE companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $res = $dao->executeRow();
            return $res['currency_sign'];
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getCurrencySign - ' . $e);
        }
    }

   /* public function getBanquetInvoiceNumber($action = '')
    {
        try {
            $this->log->logIt($this->module . ' - getBanquetInvoiceNumber');
            $dao = new \dao();
            $strSql = "SELECT prefix, startno,reset_type FROM ".CONFIG_DBN.".bmdocnumber WHERE keyname='BANQUETINVOICE' AND companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $rec = $dao->executeRow();
            $today_date = \util\util::getLocalDate();//\util\util::getTodaysDate();
            $reset_type = $rec['reset_type'];
            $flag = 0;
            $dao = new \dao;
            if ($reset_type == '1') {
                $strSql = "SELECT count(bmbookunkid) AS cnt FROM ".CONFIG_DBN.".bmbanquet_booking WHERE companyid=:companyid AND CAST(createddatetime AS DATE) = :todaysdate";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':todaysdate', $today_date);
                $data = $dao->executeRow();
                if ($data['cnt'] == '0')
                    $flag = 1;
            } elseif ($reset_type == '2') {
                $date = \DateTime::createFromFormat("Y-m-d", $today_date);
                $day = $date->format("d");
                if ($day == '01') {
                    $strSql = "SELECT count(bmbookunkid) AS cnt FROM ".CONFIG_DBN.".bmbanquet_booking WHERE companyid=:companyid AND CAST(createddatetime AS DATE) = :todaysdate";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':todaysdate', $today_date);
                    $data = $dao->executeRow();
                    if ($data['cnt'] == '0')
                        $flag = 1;
                }
            } elseif ($reset_type == '3') {
                $date = \DateTime::createFromFormat("Y-m-d", $today_date);
                $day = $date->format("d");
                $month = $date->format("m");
                if ($day == '01' && $month == '04') {
                    $strSql = "SELECT count(bmbookunkid) AS cnt FROM ".CONFIG_DBN.".bmbanquet_booking WHERE companyid=:companyid AND CAST(createddatetime AS DATE) = :todaysdate";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addParameter(':todaysdate', $today_date);
                    $data = $dao->executeRow();
                    if ($data['cnt'] == '0')
                        $flag = 1;
                }
            }
            if ($flag == 0) {
                $no = $rec['prefix'] . $rec['startno'];
                $rec['last_no'] = $rec['startno'];
            } else {
                $no = $rec['prefix'] . '1';
                $rec['last_no'] = 1;
            }
            if ($action == 'inc') {
                $updated_no = $rec['last_no'] + 1;
                $strSql = "UPDATE ".CONFIG_DBN.".bmdocnumber SET startno=:startno WHERE companyid=:companyid";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':startno', $updated_no);
                $dao->executeNonQuery();
            }
            $rec['invoice_no'] = $no;
            return $rec;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getBanquetInvoiceNumber - ' . $e);
        }
    }*/


    public function getInvoiceNumber($action = '',$locationid='')
    {
        try {
            $this->log->logIt($this->module . ' - getInvoiceNumber');
            $dao = new \dao();
            $strSql = "SELECT prefix, startno,reset_type,main_start_no FROM ".CONFIG_DBN.".cfdocnumber WHERE keyname='INVOICE' AND companyid=:companyid AND locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', $locationid);
            $rec = $dao->executeRow();
            $today_date = \util\util::getTodaysDate($locationid); //\util\util::getLocalDate();
            $reset_type = $rec['reset_type'];
            $flag = 0;
            $dao = new \dao;
            if ($reset_type == '1') {
                $strSql = "SELECT count(foliounkid) AS cnt FROM ".CONFIG_DBN.".fasfoliomaster WHERE companyid=:companyid AND locationid=:locationid AND CAST(servedate AS DATE) = :todaysdate";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', $locationid);
                $dao->addParameter(':todaysdate', $today_date);
                $data = $dao->executeRow();
                if ($data['cnt'] == '0')
                    $flag = 1;
            } elseif ($reset_type == '2') {
                $date = \DateTime::createFromFormat("Y-m-d", $today_date);
                $day = $date->format("d");
                if ($day == '01') {
                    $strSql = "SELECT count(foliounkid) AS cnt FROM ".CONFIG_DBN.".fasfoliomaster WHERE companyid=:companyid AND locationid=:locationid AND CAST(servedate AS DATE) = :todaysdate";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', $locationid);
                    $dao->addParameter(':todaysdate', $today_date);
                    $data = $dao->executeRow();
                    if ($data['cnt'] == '0')
                        $flag = 1;
                }
            } elseif ($reset_type == '3') {
                $date = \DateTime::createFromFormat("Y-m-d", $today_date);
                $day = $date->format("d");
                $month = $date->format("m");
                if ($day == '01' && $month == '04') {
                    $strSql = "SELECT count(foliounkid) AS cnt FROM ".CONFIG_DBN.".fasfoliomaster WHERE companyid=:companyid AND locationid=:locationid AND CAST(servedate AS DATE) = :todaysdate";
                    $dao->initCommand($strSql);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->addparameter(':locationid', $locationid);
                    $dao->addParameter(':todaysdate', $today_date);
                    $data = $dao->executeRow();
                    if ($data['cnt'] == '0')
                        $flag = 1;
                }
            }
            if ($flag == 0) {
                $no = $rec['prefix'] . $rec['startno'];
                $rec['last_no'] = $rec['startno'];
            } else {
                $no = $rec['prefix'].''.$rec['main_start_no'];
                $rec['last_no'] = $rec['main_start_no'];
            }
            if ($action == 'inc') {
                $updated_no = $rec['last_no'] + 1;
                $strSql = "UPDATE ".CONFIG_DBN.".cfdocnumber SET startno=:startno WHERE companyid=:companyid AND locationid=:locationid";
                $dao->initCommand($strSql);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addparameter(':locationid', $locationid);
                $dao->addParameter(':startno', $updated_no);
                $dao->executeNonQuery();
            }
            $rec['invoice_no'] = $no;
            return $rec;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getInvoiceNumber - ' . $e);
        }
    }

    public function getMasterId($type,$locationid)
    {
        try
        {
            $this->log->logIt($this->module.' - getMasterId');
            $dao = new \dao();
            $strSql = "SELECT masterunkid,crdr FROM ".CONFIG_DBN.".fasmaster WHERE mastertypeunkid=:type AND companyid=:companyid AND locationid=:locationid and isactive=1 and is_deleted=0";
            $dao->initCommand($strSql);
            $dao->addParameter(":type",$type);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addparameter(':locationid',$locationid);
            $data = $dao->executeRow();
            return $data['masterunkid'];
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getMasterId - '.$e);
        }
    }

    public function statusOnchnage($data)
    {
        try {
            $this->log->logIt($this->module . ' - statusOnchnage');
            $dao = new \dao();

            $ObjAuditDao = new \database\auditlogdao();
            if (count($data) > 0 && isset($data['module']) && isset($data['id'])) {
                $datetime = \util\util::getLocalDateTime();
                $Objstaticarray = new \common\staticarray();
                $table = $Objstaticarray->auditlogmodules[$data['module']]['table'];
                $pd = $Objstaticarray->auditlogmodules[$data['module']]['pd'];
                $cn = $Objstaticarray->auditlogmodules[$data['module']]['status_cn'];
                $ms = $Objstaticarray->auditlogmodules[$data['module']]['masterfield'];
                $masterfield = "SELECT " . $ms ."  As MField FROM " . CONFIG_DBN . "." . $table . " WHERE user_id=:userid AND companyid=:companyid ";

                $dao->initCommand($masterfield);
                $dao->addParameter(":userid",$data['id']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $field = $dao->executeRow();
                $object = $field['MField'];

                if($data['oldvalue'] == 1)
                {
                    $oldvalue = "Active";
                }
                elseif ($data['oldvalue'] == 0)
                {
                    $oldvalue = "Inactive";
                }
                elseif($data['oldvalue'] == 2)
                {
                    $oldvalue = "Banned";
                }
                else
                {
                    $oldvalue = "";
                }
                if($data['newvalue'] == 1)
                {
                    $newvalue = "Active";
                }
                elseif ($data['newvalue'] == 0)
                {
                    $newvalue = "Inactive";
                }
                elseif($data['newvalue'] == 2)
                {
                    $newvalue = "Banned";
                }
                else
                {
                    $newvalue = "";
                }
                $arr_log = array(
                    'Old Value' => $oldvalue,
                    'New Value' => $newvalue
                );
                $json_data = json_encode($arr_log);
                $strSql = "UPDATE ".CONFIG_DBN.".".$table. " SET modified_by=:modified_by, modified_on=:modifiedon,status=:status WHERE user_id=:id AND companyid=:companyid ";
                $dao->initCommand($strSql);
                $dao->addParameter(":id", $data['id']);
                $dao->addParameter(':modified_by', CONFIG_UID);
                $dao->addParameter(':status',$data['newvalue']);
                $dao->addParameter(':modifiedon', $datetime);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'], 'Change Status', $data['id'],$json_data,$object);
                return json_encode(array("Success" => "True", "Message" => $this->defaultlanguageArr->STATUS_CHANGE_SUCCESS));
            } else
                return json_encode(array("Success" => "False", "Message" => $this->defaultlanguageArr->INTERNAL_ERROR));
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - statusOnchnage - ' . $e);
        }
    }

    /*public function getFiscalsetting()
    {
        try
        {
            $this->log->logIt($this->module." - getFiscalsetting");
            $dao = new \dao();

            $strSql1 = "SELECT GROUP_CONCAT(is_fiscal) AS is_fiscal FROM syslocation WHERE companyid=:companyid AND FIND_IN_SET(1,is_fiscal)";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyid',CONFIG_CID);
            $res = $dao->executeQuery();

            if(isset($res[0]['is_fiscal']) && $res[0]['is_fiscal'] != null)
            {
                return 1;
            }else
            {
                return 0;
            }

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getFiscalsetting - ".$e);
        }
    }*/


}

?>