<?php
namespace database;

class business_sourcedao
{
    public $module = 'DB_business_sourcedao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addeditbusinesssource($data,$langlist='',$default_langlist='')
    {
        try {
            $this->log->logIt($this->module . ' - addeditbusinesssource');
            $dao = new \dao();

            $ObjCommonDao = new \database\commondao();

            $datetime = \util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();
            $tablename = "bmbusinesssource";
            $ObjDependencyDao = new \database\dependencydao();

            $arr_log = array(
                'Company Name' => $data['name'],
                'Alias' => $data['alias'],
                'Market Place' => $data['market_place_val'],
                'Account Number' => $data['accountno'],
                'First Name' => $data['fname'],
                'Last Name' => $data['lname'],
                'Phone' => $data['phone'],
                'Address1' => $data['address1'],
                'Address2' => $data['address2'],
                'City' => $data['city'],
                'State' => $data['state'],
                'Zip' => $data['zip'],
                'Country' => $data['country_val'],
                'Fax' => $data['fax'],
                'E-Mail' => $data['email'],
                'Plan' => $data['plan_val'],
                'Commission Value' => $data['commission_value'],
                'Payment Term' => $data['terms'],
                'IATA No.' => $data['iata_no'],
                'Reg. No.' => $data['reg_no'],
                'Reg. No. 1' => $data['reg_no1'],
                'Reg. No. 2' => $data['reg_no2'],
                'Description' => $data['description'],
                'Status' => ($data['rdo_status'] == 1) ? 'Active' : 'Inactive',
            );
            $marketplace_id = $ObjCommonDao->getprimarykey('bmmarket_place',$data['market_place'],'marketplaceunkid');
            $json_data = html_entity_decode(json_encode($arr_log));
            $hashkey = \util\util::gethash();
            if ($data['id'] == 0) {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "companyname", $data['name']);

                if ($chk_name == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG30)));
                }
                $chk_alias = $ObjDependencyDao->checkduplicaterecord($tablename, "alias", $data['alias']);

                    if ($chk_alias == 1) {
                        return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG31)));
                    }

                $title = "Add Record";

                $strSql = "INSERT INTO " . CONFIG_DBN . ".bmbusinesssource SET
                companyname=:companyname,
                alias=:alias,
                firstname=:firstname,
                lastname=:lastname,
                description=:description,
                account_number=:account_number,
                lnkmarketplaceunkid=:lnkmarketplaceunkid,
                commission_value=:commission_value,
                terms=:terms,
                address1=:address1,
                address2=:address2,
                city=:city,
                state=:state,
                zip=:zip,
                country=:country,
                phone=:phone,
                fax=:fax,
                email=:email,
                website=:website,
                iata_no=:iata_no,
                reg_no=:reg_no,
                reg_no1=:reg_no1,
                reg_no2=:reg_no2,
                plan=:plan,
                hashkey=:hashkey,
                companyid=:companyid,
                is_active=:is_active,
                created_user=:created_user,
                createddatetime=:createddatetime";

                $dao->initCommand($strSql);

                $dao->addParameter(':companyname', $data['name']);
                $dao->addParameter(':alias', $data['alias']);
                $dao->addParameter(':firstname', $data['fname']);
                $dao->addParameter(':lastname', $data['lname']);
                $dao->addParameter(':description', $data['description']);
                $dao->addParameter(':account_number', $data['accountno']);
                $dao->addParameter(':lnkmarketplaceunkid', $marketplace_id);
                $dao->addParameter(':commission_value', $data['commission_value']);
                $dao->addParameter(':terms', $data['terms']);
                $dao->addParameter(':address1', $data['address1']);
                $dao->addParameter(':address2', $data['address2']);
                $dao->addParameter(':city', $data['city']);
                $dao->addParameter(':state', $data['state']);
                $dao->addParameter(':zip', $data['zip']);
                $dao->addParameter(':country', $data['country']);
                $dao->addParameter(':phone', $data['phone']);
                $dao->addParameter(':fax', $data['fax']);
                $dao->addParameter(':email', $data['email']);
                $dao->addParameter(':website', $data['website']);
                $dao->addParameter(':iata_no', $data['iata_no']);
                $dao->addParameter(':reg_no', $data['reg_no']);
                $dao->addParameter(':reg_no1', $data['reg_no1']);
                $dao->addParameter(':reg_no2', $data['reg_no2']);
                $dao->addParameter(':plan', isset($data['plan'])?$data['plan']:'');
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':is_active', $data['rdo_status']);
                $dao->addParameter(':created_user', CONFIG_UID);
                $dao->addParameter(':createddatetime', $datetime);

                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'], $title, $hashkey, $json_data);

                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $default_langlist->REC_ADD_SUC)));
            } else {


                $id = $ObjCommonDao->getprimarykey('bmbusinesssource', $data['id'], 'businesssourceunkid');

                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "companyname", $data['name'], $id);
                if ($chk_name == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG30)));
                }

                $chk_alias = $ObjDependencyDao->checkduplicaterecord($tablename, "alias", $data['alias'],$id);

                if ($chk_alias == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG31)));
                }


                $title = "Edit Record";

                $strSql = "Update " . CONFIG_DBN . ".bmbusinesssource SET
                companyname=:companyname,
                alias=:alias,
                firstname=:firstname,
                lastname=:lastname,
                description=:description,
                account_number=:account_number,
                lnkmarketplaceunkid=:lnkmarketplaceunkid,
                commission_value=:commission_value,
                terms=:terms,
                address1=:address1,
                address2=:address2,
                city=:city,
                state=:state,
                zip=:zip,
                country=:country,
                phone=:phone,
                fax=:fax,
                email=:email,
                website=:website,
                iata_no=:iata_no,
                reg_no=:reg_no,
                reg_no1=:reg_no1,
                reg_no2=:reg_no2,
                plan=:plan,      
                modified_user=:modified_user,
                modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND businesssourceunkid=:businesssourceunkid";

                $dao->initCommand($strSql);

                $dao->addParameter(':companyname', $data['name']);
                $dao->addParameter(':alias', $data['alias']);
                $dao->addParameter(':firstname', $data['fname']);
                $dao->addParameter(':lastname', $data['lname']);
                $dao->addParameter(':description', $data['description']);
                $dao->addParameter(':account_number', $data['accountno']);
                $dao->addParameter(':lnkmarketplaceunkid', $marketplace_id);
                $dao->addParameter(':commission_value', $data['commission_value']);
                $dao->addParameter(':terms', $data['terms']);
                $dao->addParameter(':address1', $data['address1']);
                $dao->addParameter(':address2', $data['address2']);
                $dao->addParameter(':city', $data['city']);
                $dao->addParameter(':state', $data['state']);
                $dao->addParameter(':zip', $data['zip']);
                $dao->addParameter(':country', $data['country']);
                $dao->addParameter(':phone', $data['phone']);
                $dao->addParameter(':fax', $data['fax']);
                $dao->addParameter(':email', $data['email']);
                $dao->addParameter(':website', $data['website']);
                $dao->addParameter(':iata_no', $data['iata_no']);
                $dao->addParameter(':reg_no', $data['reg_no']);
                $dao->addParameter(':reg_no1', $data['reg_no1']);
                $dao->addParameter(':reg_no2', $data['reg_no2']);
                $dao->addParameter(':plan', $data['plan']);
                $dao->addParameter(':businesssourceunkid', $id);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);

                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'], $title, $data['id'], $json_data);

                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $default_langlist->REC_UP_SUC)));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addeditbusinesssource - ' . $e);
        }
    }

    public function businesssourcelist($limit, $offset, $name, $isactive = 0)
    {
        try {
            $this->log->logIt($this->module . ' - businesssourcelist - ' . $name);

            $dao = new \dao;
            $dateformat = \database\parameter::getBanquetParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $roundoff = \database\parameter::getBanquetParameter('digitafterdecimal');
            $strSql = "SELECT Cf.*,IFNULL(CFU1.username,'') AS createduser,
                                    IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(Cf.createddatetime,'" . $mysqlformat . "'),'') as created_date,
                                    IFNULL(DATE_FORMAT(Cf.modifieddatetime,'" . $mysqlformat . "'),'') as modified_date
                                    FROM " . CONFIG_DBN . ".bmbusinesssource AS Cf
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU1 ON Cf.created_user=CFU1.userunkid AND Cf.companyid=CFU1.companyid
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU2 ON Cf.modified_user=CFU2.userunkid AND Cf.modified_user=CFU2.userunkid AND Cf.companyid=CFU2.companyid
                                    
                                    
                                    WHERE Cf.companyid=:companyid AND Cf.is_deleted=0";
            if ($name != "")
                $strSql .= " AND (companyname LIKE '%" . $name . "%' OR alias LIKE '%" . $name . "%' OR email LIKE '%" . $name . "%' OR fax LIKE '%" . $name . "%' OR website LIKE '%" . $name . "%' OR phone LIKE '%" . $name . "%' ) ";

            if ($isactive == 1) {
                $strSql .= " AND is_active=1 ";
            }
            $strSql .= " ORDER BY Cf.businesssourceunkid DESC";

            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $data = $dao->executeQuery();
            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);

            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec ,"roundoff" => $roundoff));
                return html_entity_decode(json_encode($retvalue));
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - businesssourcelist - ' . $e);
        }
    }

    public function getcatRec($data)
    {
        try {
            $this->log->logIt($this->module . " - getcatRec");

            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $id = $ObjCommonDao->getprimarykey('bmbusinesssource', $data['id'], 'businesssourceunkid');
            $roundoff = \database\parameter::getBanquetParameter('digitafterdecimal');

            $strSql = "SELECT CFC.*,IF(CFC.commission_value>0,ROUND(CFC.commission_value," . $roundoff . "),'') AS commission_value_dis,IFNULL(BMP.hashkey,'') AS marketplaceunkid
                      FROM " . CONFIG_DBN . ". bmbusinesssource AS CFC
                      LEFT JOIN " . CONFIG_DBN . ".bmmarket_place as BMP ON CFC.lnkmarketplaceunkid=BMP.marketplaceunkid AND CFC.companyid=BMP.companyid AND BMP.is_active=1 AND BMP.is_deleted=0 
                      WHERE CFC.businesssourceunkid=:businesssourceunkid AND
                      CFC.companyid=:companyid  ";
            $dao->initCommand($strSql);

            $dao->addParameter(':businesssourceunkid', $id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res)));

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getcatRec - " . $e);
            return false;
        }
    }


}

?>

