<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 30/1/18
 * Time: 4:11 PM
 */
namespace database;
class banquet_inventorydao
{
    public $module = 'DB_banquet_inventorydao';
    public $log;
    public $dbconnect;
    private $ObjCommonDao;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->ObjCommonDao = new \database\commondao();
    }

    public function updateBanquetItemInventory($item_id, $itemqty,$itmunitid,$itemtype,$operation,$folio_id,$detailid,$isvoid,$isupdate,$dbName,$companyid,$location_id)
    {
        $this->log->logIt($this->module.' - updateBanquetItemInventory');
        $dao = new \dao();
        $recipe_type = 1;
        $strRecipe = "SELECT count(recipeunkid) AS cnt FROM ".$dbName.".cfmenu_item_recipe WHERE companyid=:companyid AND locationid=:locationid AND lnkitemid=:itemid AND lnkitemunitid=:itemunitid AND recipe_type=:recipe_type";
        $dao->initCommand($strRecipe);
        $dao->addParameter(':companyid', $companyid);
        $dao->addparameter(':locationid', $location_id);
        $dao->addparameter(':itemid', $item_id);
        $dao->addparameter(':itemunitid', $itmunitid);
        $dao->addparameter(':recipe_type', $recipe_type);
        $recipe_data = $dao->executeRow();

        if($recipe_data['cnt']>0) {

            if ($itemtype == 1) {

                    $strSql = "CALL " . $dbName . ".SP_updateinventory(" . $item_id . "," . $itemqty . "," . $itmunitid . "," . $recipe_type . "," . $operation . "," . $folio_id . "," . $detailid . "," . $isvoid . "," . $companyid . ",'" . $location_id . "',@final_status);";
                $dao->initCommand($strSql);
                $dao->executeNonQuery();
                $strSql = "SELECT @final_status as finalstatus";
                $dao->initCommand($strSql);
                $result = $dao->executeRow();
                return $result;
            }
        }else{
            $result=array();
            $result['finalstatus']='Commit';
            return $result;
        }
    }

}
?>