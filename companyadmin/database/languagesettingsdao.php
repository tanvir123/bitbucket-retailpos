<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 15/7/17
 * Time: 9:56 AM
 */

namespace database;
class languagesettingsdao
{
    public $module = 'DB_languagesettingsdao';
    public $log;
    function __construct()
    {
        $this->log = new \util\logger();
        $this->redis = new \Redis();
        $this->redis->connect('127.0.0.1', 6379);
        $this->redis->select(15);
    }
    public function loadLangParameters($data)
    {
        try
        {
            $this->log->logIt($this->module.' - loadLangParameters');
            $redis_0 = new \Redis();
            $redis_0->connect('127.0.0.1', 6379);
            $redis_0->select(0);
            $arr_lang_label = array();
            $i =0;
            $default_lang_arr = json_decode($redis_0->get($data['module_name']."_en"),true);
            $custom_lang_arr = json_decode($this->redis->get($data['module_name']."_".CONFIG_LANG),true);
            foreach ($default_lang_arr as $key=>$value){
                $arr_lang_label[$i]['key'] = $key;
                $arr_lang_label[$i]['default_label'] = $value;
                if(isset($custom_lang_arr[$key])){
                    $arr_lang_label[$i]['custom_label'] = $custom_lang_arr[$key];
                }else{
                    $arr_lang_label[$i]['custom_label'] = '';
                }
                $i++;
            }
            return html_entity_decode(json_encode(array("Success"=>'True',"Data"=>$arr_lang_label)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loadLangParameters - '.$e);
        }
    }
    public function updateCustomLangLabel($data)
    {
        try
        {
            $this->log->logIt($this->module.' - updateCustomLangLabel');
            $this->redis->set($data['module_name']."_".CONFIG_LANG,json_encode($data['customlanglabel']));
            return 1;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - updateCustomLangLabel - '.$e);
        }
    }
}
?>