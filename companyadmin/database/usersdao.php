<?php
namespace database;

class usersdao
{
    public $module = 'DB_usersdao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addUser($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addUser');
            $dao = new \dao();

            $datetime=\util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $arr_log = array(
                'Short Code'=>$data['shortcode'],
                'Name'=>$data['name'],
                'User Name'=>$data['username'],
                'Email ID'=>$data['emailid'],
                'Mobile No'=>$data['mobileno'],
                'Role Id'=>$data['roleunkid'],

            );
            $json_data = json_encode($arr_log);
            $res = $data;
           /* if($data['usr_type']==2 && $data['usr_location']!='' && count($data['usr_location'])>0){
                $location_id = implode(',',$data['usr_location']);
            }*/
            $location_id = implode(',',$data['usr_location']);

            $storeid=implode(',',$data['storeid']);

            if($res['id']=='0' || $res['id']==0)
            {
                $title = "Add User";
                $hashkey = \util\util::gethash();
                $sql = "SELECT * FROM ".CONFIG_DBN.".cfuser AS USR WHERE USR.companyid=:companyid AND
                 USR.username=:username AND is_deleted=0";
                $dao->initCommand($sql);
                $dao->addParameter(':username',$res['username']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $userdata =$dao->executeQuery();

                if(count($userdata) !== 0)
                {
                    return json_encode(array('Success'=>'false','Message'=>'User already exists.'));
                }
                else
                {
                    $strSql = "INSERT INTO ".CONFIG_DBN.".trcontact(
                                                                    shortcode,
                                                                    name,                                                                     
                                                                    email, 
                                                                    mobile,
                                                                    contacttype,
                                                                    companyid,                                                                    
                                                                    hashkey
                                                                    ) 
                                                            VALUES(
                                                                    :shortcode,
                                                                    :name,                                                                    
                                                                    :emailid, 
                                                                    :mobileno,
                                                                    :contacttype,
                                                                    :companyid,                                                                    
                                                                    :hashkey
                                                                    )";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':shortcode',$res['shortcode']);
                    $dao->addParameter(':name',$res['name']);
                    $dao->addParameter(':emailid',$res['emailid']);
                    $dao->addParameter(':mobileno',$res['mobileno']);
                    $dao->addParameter(':contacttype',1);
                    $dao->addParameter(':hashkey',$hashkey);
                    $dao->addParameter(':companyid',CONFIG_CID);

                    $dao->executeNonQuery();
                    $userdata = $dao->getLastInsertedId();



                    $hashkey1 = \util\util::gethash();
                    $strSql1 = "INSERT INTO sysuser(username,
                                                    password,
                                                    user_type,
                                                    roleunkid,
                                                    lang_type,
                                                    lang_code,
                                                    contactunkid,
                                                    companyid,                                                    
                                                    storeid,
                                                    locationid,
                                                    isactive,
                                                    created_user,
                                                    createddatetime,
                                                    hashkey
                                                    ) 
                                            VALUES( :username,
                                                    :password, 
                                                    :user_type,
                                                    :roleunkid,
                                                    :lang_type,
                                                    :lang_code,
                                                    :contactunkid,
                                                    :companyid,                                                    
                                                    :storeid,
                                                    :locationid,
                                                    :isactive,
                                                    :userid,
                                                    :createddatetime,
                                                    :hashkey
                                                   ) ";
                    $dao->initCommand($strSql1);
                    $dao->addParameter(':username',$res['username']);
                    $dao->addParameter(':password',md5($res['password']));
                    $dao->addParameter(':user_type',$res['usr_type']);
                    $dao->addParameter(':roleunkid',$data['roleunkid']);
                    $dao->addParameter(':lang_type',$res['lang_type']);
                    $dao->addParameter(':lang_code',$res['lang_code']);
                    $dao->addParameter(':userid',CONFIG_UID);
                    $dao->addParameter(':storeid',$storeid);
                    $dao->addParameter(':locationid',$location_id);
                    $dao->addParameter(':isactive',1);
                    $dao->addParameter(':createddatetime',$datetime);
                    $dao->addParameter(':contactunkid',$userdata);
                    $dao->addParameter(':hashkey',$hashkey1);
                    $dao->addParameter(':companyid',CONFIG_CID);

                    $dao->executeNonQuery();
                    $contactid=$dao->getLastInsertedId();

                    $strpriv ="SELECT id from sysprivileges";
                    $dao->initCommand($strpriv);
                    $privi = $dao->executeQuery();

                    
                    foreach ($privi as $key)
                    {
                        $value= $key['id'];


                        $Strforgroup= "SELECT privilegegroupid from sysprivileges WHERE id=:id";
                        $dao->initCommand($Strforgroup);
                        $dao->addParameter(':id',$value);

                        $lnkprivilegegroupid=$dao->executeRow();


                        $strSQLRELATION= "INSERT INTO ".CONFIG_DBN.".cfroleprivileges SET  lnkroleid=:lnkroleid,
                lnkprivilegegroupid=:lnkprivilegegroupid, lnkuserid=:lnkuserid,
                lnkprivilegeid=:lnkprivilegeid,companyid=:companyid";

                        $dao->initCommand($strSQLRELATION);
                        $dao->addParameter(':lnkroleid',$data['roleunkid']);
                        $dao->addParameter(':companyid',CONFIG_CID);
                        $dao->addParameter(':lnkprivilegegroupid',$lnkprivilegegroupid['privilegegroupid']);
                        $dao->addParameter(':lnkprivilegeid',$value);
                        $dao->addParameter(':lnkuserid',$contactid);
                        $dao->executeNonQuery();
                    }



                    $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey1,$json_data);
                    return json_encode(array('Success'=>'True','Message'=>'User added successfully'));
                }
            }
            else
            {
                $title = "Edit User";

                $id = $ObjCommonDao->getprimarykey('cfuser',$res['id'],'userunkid');


                $strSql="UPDATE sysuser SYS 
                        INNER JOIN ".CONFIG_DBN.".trcontact TRC 
                        ON (SYS.contactunkid=TRC.contactunkid)
                            SET 
                                 SYS.username=:username,
                                 SYS.roleunkid=:roleunkid,
                                 SYS.storeid=:storeid,
                                 SYS.locationid=IF(SYS.sysdefined=1,SYS.locationid,'".$location_id."'),
                                 SYS.user_type=IF(SYS.sysdefined=1,SYS.user_type,'".$res['usr_type']."'),
                                 lang_type=:lang_type,lang_code=:lang_code,
                                 TRC.name=:name,
                                 TRC.email=:emailid,
                                 TRC.mobile=:mobileno,
                                 TRC.shortcode=:shortcode,
                                 TRC.modifieddatetime=:modifieddatetime,
                                 TRC.modified_user=:modified_user
                        WHERE SYS.hashkey=:userunkid";

                $dao->initCommand($strSql);
                $dao->addParameter(':shortcode',$res['shortcode']);
                $dao->addParameter(':name',$res['name']);
                $dao->addParameter(':lang_type',$res['lang_type']);
                $dao->addParameter(':lang_code',$res['lang_code']);
                $dao->addParameter(':mobileno',$res['mobileno']);
                $dao->addParameter(':emailid',$res['emailid']);
                $dao->addParameter(':storeid',$storeid);
                $dao->addParameter(':userunkid',$res['id']);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':username',$res['username']);
                $dao->addParameter(':roleunkid',$res['roleunkid']);
                $dao->executeNonQuery();


                return json_encode(array('Success'=>'True','Message'=>'User details updated successfully'));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addUser - '.$e);
        }
    }

    public function userlist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - userlist - '.$name);

            $dao = new \dao;
            $dateformat = \database\parameter::getBanquetParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT USR.userunkid as userid,USR.sysdefined,USR.username, IFNULL(USR1.username,'') AS created_user, IFNULL(USR2.username,'') AS modified_user, IFNULL(TRC.mobile,'') as mobile".
                " ,IFNULL(DATE_FORMAT(USR.createddatetime,'".$mysqlformat."'),'') as created_date".
                " ,IFNULL(DATE_FORMAT(USR.modifieddatetime,'".$mysqlformat."'),'') as modified_date".
                " ,USR.hashkey,USR.isactive
                        FROM ".CONFIG_DBN.".cfuser AS USR
                        LEFT JOIN ".CONFIG_DBN.".trcontact AS TRC
                            ON USR.contactunkid=TRC.contactunkid AND TRC.companyid=:companyid
                        LEFT JOIN ".CONFIG_DBN.".cfuser as USR1
                            ON USR1.userunkid = USR.created_user AND USR1.companyid=:companyid
                        LEFT JOIN ".CONFIG_DBN.".cfuser as USR2
                            ON USR2.userunkid=USR.modified_user AND USR2.companyid=:companyid
                        WHERE USR.companyid=:companyid  AND USR.is_deleted=0";
            if($name!="")
                $strSql .= " AND USR.username LIKE '%".$name."%'";
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $data = $dao->executeQuery();

            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return json_encode($retvalue);
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return json_encode($retvalue);
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - userlist - '.$e);
        }
    }

    public function getUserRec($data)
    {
        try
        {
            $this->log->logIt($this->module." - getUserRec");
            $dao = new \dao();
            $id=$data['id'];   


            $strSql = "SELECT
USR.username,USR.user_type,IFNULL(USR.locationid,'') as usr_location,
USR.password, USR.isactive, TRC.shortcode, TRC.name, TRC.email, 
TRC.mobile, USR.lang_type,USR.lang_code ,USR.roleunkid,USR.storeid
                       FROM ".CONFIG_DBN.".cfuser AS USR 
                       INNER JOIN ".CONFIG_DBN.".trcontact AS TRC 
                       ON USR.contactunkid = TRC.contactunkid 
                       
                       WHERE USR.companyid=:companyid AND USR.hashkey=:userunkid ";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':userunkid',$id);

            $res = $dao->executeRow();


            return json_encode(array("Success"=>"True","Data"=>$res));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUserRec - ".$e);
            return false;
        }
    }

    public function changePassword($data)
    {
        try
        {
            $this->log->logIt($this->module.' - changePassword');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $res = $data;
            $current_date = \util\util::getLocalDateTime();
            if($res['id']!='' || $res['id']!=0)
            {
                //$id = $ObjCommonDao->getprimarykey('cfuser',$res['id'],'userunkid');
                $strSql="UPDATE sysuser SYS SET 
                          SYS.password=:password,SYS.modifieddatetime=:modifieddatetime,SYS.modified_user=:modified_user
                          WHERE SYS.hashkey=:userunkid";
                $dao->initCommand($strSql);
                $dao->addParameter(':password',md5($res['newpwd']));
                $dao->addParameter(':userunkid',$res['id']);
                $dao->addParameter(':modifieddatetime',$current_date);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],'Change Password',$data['id']);
                return json_encode(array('Success'=>'True','Message'=>'Password Changed successfully'));
            }else{
                return json_encode(array('Success'=>'False','Message'=>'Error'));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - changePassword - '.$e);
        }
    }

    public function getUserDetail()
    {
        try
        {
            $this->log->logIt($this->module." - getUserDetail");
            $dao = new \dao();
            $strSql = "SELECT username,sysdefined,user_type FROM ".CONFIG_DBN.".cfuser 
            WHERE companyid=:companyid AND userunkid=:userunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':userunkid',CONFIG_UID);
            $res = $dao->executeRow();
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUserDetail - ".$e);
            return false;
        }
    }
    public function getUserRole()
    {
        try
        {
            $this->log->logIt($this->module." - getUserRole");
            $dao = new \dao();
            $strSql = "SELECT user_role,roleunkid FROM ".CONFIG_DBN.".cfuserrole 
            WHERE companyid=:companyid 
            AND is_active=1 and is_deleted=0 ORDER BY user_role ASC";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);

            $res = $dao->executeQuery();

            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUserRole - ".$e);
            return false;
        }
    }
    public function getLocationList()
    {
        try
        {
            $this->log->logIt($this->module.' - getLocationList');
            $dao = new \dao();
            $strSql = "SELECT syslocationid, locationname FROM syslocation WHERE is_deleted=0 AND companyid=:companyid ORDER BY syslocationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $rec = $dao->executeQuery();
            return $rec;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getLocationList - '.$e);
        }
    }
    public function getstorelist()
    {
        try {
            $this->log->logIt($this->module . " - getstorelist");
            $dao = new \dao();
            $strSql = "SELECT storename,storeunkid FROM " . CONFIG_DBN . ".cfstoremaster 
            WHERE companyid=:companyid 
            AND is_active=1 and is_deleted=0 ORDER BY storename ASC";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);

            $res = $dao->executeQuery();


            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getstorelist - " . $e);
            return false;
        }
    }

}
?>