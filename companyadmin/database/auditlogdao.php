<?php
namespace database;

class auditlogdao
{
    public $module = 'DB_auditlogdao';
    public $log;
    public $dbconnect;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->mongo_con = new \MongoDB\Driver\Manager(CONFIG_MONGO_HOST);
    }
    public function addactivitylog($module,$title,$id,$json,$object="")
    {
        try
        {
            $this->log->logIt($this->module.' - activitylog');
            $dao = new \dao();
            $ObjUtil = new \util\util;
            $ObjStaticArray =new \common\staticarray();
            $ObjCommonDao = new \database\commondao();
            $localdatetime = $ObjUtil->getLocalDateTime_ISO();
            $userip =  $ObjUtil->VisitorIP();
            $m = $this->mongo_con;
            $masterfield = $ObjStaticArray->auditlogmodules[$module]['masterfield'];
            $table = $ObjStaticArray->auditlogmodules[$module]['table'];
            if($object!=""){
                $object = $object;
            }else{
                $object = $ObjCommonDao->getprimarykey($table,$id,$masterfield);
            }
            if($table!="" && $title!="" && isset($m))
            {
                $rec = json_decode($json,1);
                $array = array(
                    "module" => $module,
                    "tablename" => $table,
                    "object" => $object,
                    "userid" => CONFIG_UID,
                    "username"=> CONFIG_UNM,
                    "unkid" => $id,
                    "title" => $title,
                    "jsondata" => $json,
                    "currenttime" => $localdatetime,
                    "companyid" => CONFIG_CID,
                    "userip" => $userip);
                $bulk = new \MongoDB\Driver\BulkWrite;
                $bulk->insert($array);
                $result = $m->executeBulkWrite(CONFIG_DBN.".activity_logs", $bulk);
                return 1;
            }
            else
            {
                return 0;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - activitylog - '.$e);
        }
    }
    public function loadindividualauditlogs($id="",$module="",$masterfield="",$pd="",$Old_lang_arr="",$NewlanguageArr="",$default_dis_languageArr="")
    {
        try
        {
            $this->log->logIt($this->module.' - loadindividualauditlogs');
            $dao = new \dao();
            if($module!="" && $masterfield!="" && $pd!="")
            {
                $resObj = array();
                $dateformat = \database\parameter::getBanquetParameter('dateformat');
                $timeformat = \database\parameter::getBanquetParameter('timeformat');
                $con = $this->mongo_con;
                $filter = [
                        'tablename' => $module,
                        'companyid' => CONFIG_CID,
                ];
                if($id!=""){
                    $filter['unkid'] = $id;
                }
                $options = [
                    'sort' => ['currenttime' => -1],
                ];
                $cnt = 0;
                 $query = new \MongoDB\Driver\Query($filter, $options);

                $cursor = $con->executeQuery(CONFIG_DBN.'.activity_logs', $query);
                foreach ($cursor as $document){
                    $title=$document->title;
                    if($document->title=='Add Record'){
                        $title=$default_dis_languageArr['ADD_RECORD'];
                    }elseif ($document->title=='Edit Record'){
                        $title=$default_dis_languageArr['EDIT_RECORD'];
                    }elseif ($document->title=='Delete Record'){
                        $title=$default_dis_languageArr['DEL_RECORD'];
                    }elseif ($document->title=='Change Status'){
                        $title=$default_dis_languageArr['CHANGE_STATUS'];
                    }elseif ($document->title=='Change Password'){
                        $title=$default_dis_languageArr['CHNG_PASSWORD'];
                    }
                    $object_dis=$document->object;
                    if($object_dis=='General Information'){
                        $object_dis=$NewlanguageArr->LANG1;
                    }
                    $resObj[$cnt]['date'] = gmdate($dateformat,$document->currenttime);
                    $resObj[$cnt]['time'] = gmdate($timeformat,$document->currenttime);
                    $resObj[$cnt]['objct'] = $object_dis;
                    $resObj[$cnt]['title'] = $title;
                    $resObj[$cnt]['uname'] = $document->username;
                    $resObj[$cnt]['userip'] = $document->userip;
                    $info_arr=(array)json_decode($document->jsondata);
                    $new_array=[];
                    if($info_arr){
                        foreach ($info_arr as $key=>$val){
                            if($key=="Status"){
                                if($val=="Active"){
                                    $dis_status=$default_dis_languageArr['ACTIVE'];
                                }else{
                                    $dis_status=$default_dis_languageArr['INACTIVE'];
                                }
                                $status_dis=$default_dis_languageArr['STATUS'];
                                $new_array[$status_dis]=$dis_status;
                            }elseif($key=="Old Value"){

                                if($val=="Active"){
                                    $dis_status=$default_dis_languageArr['ACTIVE'];
                                }
                                elseif($val=="Banned"){
                                    $dis_status=$default_dis_languageArr['BANNED'];
                                }
                                else{
                                    $dis_status=$default_dis_languageArr['INACTIVE'];
                                }
                                $status_dis=$default_dis_languageArr['OLD_VAL'];
                                $new_array[$status_dis]=$dis_status;
                            }elseif($key=="New Value"){
                                if($val=="Active"){
                                    $dis_status=$default_dis_languageArr['ACTIVE'];
                                }
                                elseif($val=="Banned"){
                                    $dis_status=$default_dis_languageArr['BANNED'];
                                }
                                else{
                                    $dis_status=$default_dis_languageArr['INACTIVE'];
                                }
                                $status_dis=$default_dis_languageArr['NEW_VAL'];
                                $new_array[$status_dis]=$dis_status;
                            }else{
                                $old_key=array_search($key,$Old_lang_arr,true);
                                if($old_key){
                                    $new_key_name=$NewlanguageArr->$old_key;
                                    $new_array[$new_key_name] = $info_arr[$key];
                                }
                            }
                        }
                    }
                    $resObj[$cnt]['info'] = $new_array;
                    $cnt++;
                }
                return json_encode(array("Success"=>"True","Data"=>$resObj));
            }
            else{
                return json_encode(array("Success"=>"False","Data"=>""));
            }
        }
        catch(\MongoDB\Driver\Exception $e)
        {
            $this->log->logIt($this->module.' - loadindividualauditlogs - '.$e);
        }
    }
    public function addsettingslog($keyname,$old_value,$new_value)
    {
        try
        {
            $this->log->logIt($this->module.' - addsettingslog');
            $ObjUtil = new \util\util;
            $localdatetime = $ObjUtil->getLocalDateTime_ISO();
            $userip =  $ObjUtil->VisitorIP();
            $m = $this->mongo_con;
            if($keyname!="" && isset($m))
            {
                $array = array(
                    "keyname" => $keyname,
                    "oldvalue" => $old_value,
                    "newvalue" => $new_value,
                    "userid" => CONFIG_UID,
                    "username"=> CONFIG_UNM,
                    "companyid" => CONFIG_CID,
                    "currenttime" => $localdatetime,
                    "userip" => $userip
                );
                $bulk = new \MongoDB\Driver\BulkWrite;
                $bulk->insert($array);
                $result = $m->executeBulkWrite(CONFIG_DBN.".settings_logs", $bulk);
                return 1;
            }
            else{
                return 0;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addsettingslog - '.$e);
        }
    }

}

?>