<?php
namespace database;

class offersdao
{
    public $module = 'DB_offersdao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addeditoffer($data,$langlist='',$default_langlist='')
    {
        try {
            $this->log->logIt($this->module . ' - addeditoffer');
            $dao = new \dao();

            $ObjCommonDao = new \database\commondao();

            $datetime = \util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();
            $tablename = "offer_mst";
            $ObjDependencyDao = new \database\dependencydao();
            $arr_log = array(
                'Name' => $data['title'],
                'Slug' => $data['slug'],
                'Start Datetime' => $data['startdate'],
                'End Datetime' => $data['enddate'],
                'Description' => $data['description'],
                'Status' => ($data['rdo_status'] == 1) ? 'Active' : 'Inactive',
            );
            $json_data = html_entity_decode(json_encode($arr_log));
            $hashkey = \util\util::gethash();
            $datetime = \util\util::getLocalDateTime();


            if ($data['id'] == 0) {

                if(strtotime($datetime) > strtotime($data['startdate'])){
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG9)));
                }

                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "title", $data['title']);

                if ($chk_name == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG21)));
                }

                $chk_alias = $ObjDependencyDao->checkduplicaterecord($tablename, "slug", $data['slug']);

                if ($chk_alias == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG20)));
                }

                $title = "Add Record";

                $strSql = "INSERT INTO " . CONFIG_DBN . ".offer_mst SET
                startdate=:startdate,
                enddate=:enddate,
                title=:title,
                slug=:slug,
                image=:image,
                description=:description,
                hashkey=:hashkey,
                companyid=:companyid,
                is_active=:is_active,
                created_user=:created_user,
                createddatetime=:createddatetime";

                $dao->initCommand($strSql);
                $dao->addParameter(':startdate', $data['startdate']);
                $dao->addParameter(':enddate', $data['enddate']);
                $dao->addParameter(':title', $data['title']);
                $dao->addParameter(':slug', $data['slug']);
                $dao->addParameter(':image', $data['image']);
                $dao->addParameter(':description', $data['description']);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':is_active', $data['rdo_status']);
                $dao->addParameter(':created_user', CONFIG_UID);
                $dao->addParameter(':createddatetime', $datetime);

                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'], $title, $hashkey, $json_data);

                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $default_langlist->REC_ADD_SUC)));
            } else {
                $sdate = $ObjCommonDao->getFieldValue('offer_mst','startdate','startdate', $data['startdate']);
                if(strtotime($sdate) != strtotime($data['startdate'])){
                    if(strtotime($datetime) > strtotime($data['startdate'])){
                        return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG9)));
                    }
                }
                $id = $ObjCommonDao->getprimarykey('offer_mst', $data['id'], 'offerunkid');
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "title", $data['title'], $id);
                if ($chk_name == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG21)));
                }

                $chk_alias = $ObjDependencyDao->checkduplicaterecord($tablename, "slug", $data['slug'], $id);
                if ($chk_alias == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG20)));
                }


                $title = "Edit Record";

                $strSql = "Update " . CONFIG_DBN . ".offer_mst SET
                startdate=:startdate,
                enddate=:enddate,         
                description=:description,         
                title=:title,         
                slug=:slug,         
                image=:image,         
                modified_user=:modified_user,
                modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND offerunkid=:offerunkid";

                $dao->initCommand($strSql);

                $dao->addParameter(':startdate', $data['startdate']);
                $dao->addParameter(':enddate', $data['enddate']);
                $dao->addParameter(':description', $data['description']);
                $dao->addParameter(':title', $data['title']);
                $dao->addParameter(':slug', $data['slug']);
                $dao->addParameter(':image', $data['image']);
                $dao->addParameter(':offerunkid', $id);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);

                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'], $title, $data['id'], $json_data);

                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $default_langlist->REC_UP_SUC)));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addeditoffer - ' . $e);
        }
    }

    public function offerlist($limit, $offset, $name,$from_date = "", $to_date = "", $isactive = 0)
    {
        try {
            $this->log->logIt($this->module . ' - offerlist - ' . $name);

            $dao = new \dao;
            $dateformat = \database\parameter::getBanquetParameter('dateformat');
            $timeformat = \database\parameter::getBanquetParameter('timeformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $mysqltimeformat = \common\staticarray::$mysqltimeformat[$timeformat];
            $strSql = "SELECT Cf.*,IFNULL(CFU1.username,'') AS createduser,
                                    IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(Cf.createddatetime,'" . $mysqlformat . "'),'') as created_date,
                                    IFNULL(DATE_FORMAT(Cf.modifieddatetime,'" . $mysqlformat . "'),'') as modified_date,
                                    IFNULL(DATE_FORMAT(Cf.startdate,'" . $mysqlformat ." ".$mysqltimeformat. "'),'') as startdatetime_dis,
                                    IFNULL(DATE_FORMAT(Cf.enddate,'" . $mysqlformat ." ".$mysqltimeformat. "'),'') as enddatetime_dis
                                    FROM " . CONFIG_DBN . ".offer_mst AS Cf
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU1 ON Cf.created_user=CFU1.userunkid AND Cf.companyid=CFU1.companyid
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU2 ON Cf.modified_user=CFU2.userunkid AND Cf.modified_user=CFU2.userunkid AND Cf.companyid=CFU2.companyid
                                   
                                    WHERE Cf.companyid=:companyid AND Cf.is_deleted=0";
            if ($name != ""){

                $strSql .= " AND (Cf.title LIKE '%" . $name . "%')";
            }
            if ($from_date != "" && $to_date != "") {
                $strSql .= " AND startdate >= '".$from_date."' AND  enddate <=  '".$to_date."' ";
            }else if ($from_date != "") {
                $strSql .= " AND startdate ='".$from_date."' ";
            }else if ($to_date != "") {
                $strSql .= " AND enddate='".$to_date."' ";
            }

            if ($isactive == 1) {
                $strSql .= " and is_active=1 ";
            }
            $strSql .= " ORDER BY Cf.offerunkid DESC";
            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $data = $dao->executeQuery();
            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);

            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return html_entity_decode(json_encode($retvalue));
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - offerlist - ' . $e);
        }
    }

    public function getcatRec($data)
    {
        try {
            $this->log->logIt($this->module . " - getcatRec");

            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $id = $ObjCommonDao->getprimarykey('offer_mst', $data['id'], 'offerunkid');

            $strSql = "SELECT CFC.startdate,CFC.enddate,CFC.description,CFC.title,CFC.slug,CFC.image
                      FROM " . CONFIG_DBN . ". offer_mst AS CFC
                      WHERE CFC.offerunkid=:offerunkid AND
                      CFC.companyid=:companyid  ";

            $dao->initCommand($strSql);

            $dao->addParameter(':offerunkid', $id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res)));

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getcatRec - " . $e);
            return false;
        }
    }


}

?>

