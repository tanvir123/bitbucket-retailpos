<?php

namespace database;

class userroledao{

    public $module ='DB_userrole';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function adduserdetail($data,$langlist='',$default_langlist='')
    {
        try
        {
            $this->log->logIt($this->module.' - adduserdetail - ');

            $res = $data;
            $dao=new \dao();
            $datetime=\util\util::getLocalDateTime();
            $hashkey = \util\util::gethash();
            $ObjAuditDao = new \database\auditlogdao();
            $ObjDependencyDao = new \database\dependencydao();
            $arr_log = array(
                'User Role'=>$data['user_role'],
            );
            if($data['id']=='0' || $data['id']==0){
                $arr_log['Status'] = ($data['rdo_status']==1)?'Active':'Inactive';
            }
            $json_data = json_encode($arr_log);
//            $index = array_search('Reports',$data['report_privilg']);
//            if($index !== FALSE){
//                unset($data['report_privilg'][$index]);
//            }
          /*  if($data['report_privilg'] != '') {

                $privilegecombined = array_merge($data['lnkprivilegeid'], $data['report_privilg']);
            }
            else{*/

                $privilegecombined= $data['lnkprivilegeid'];
           // }

            if($res['id']=='0' || $res['id']==0)
            {
                $chk_existing = $ObjDependencyDao->checkduplicaterecord('cfuserrole','user_role',$data['user_role'],$data['id']);


                if($chk_existing == 1){
                    return json_encode(array('Success'=>'False','Message'=>$langlist->LANG12));
                }
                $title = "Add Record";
                $q="INSERT into ".CONFIG_DBN.".cfuserrole SET 
                user_role=:user_role,
                is_active=:is_active,
                createddatetime=:createddatetime,
                created_user=:created_user,
                companyid=:companyid,
              
                hashkey=:hashkey";

                $dao->initCommand($q);
                $dao->addParameter(':user_role',$data['user_role']);
                $dao->addParameter(':is_active',$data['rdo_status']);
                $dao->addParameter(':createddatetime',$datetime);
                $dao->addParameter(':created_user',CONFIG_UID);
                $dao->addParameter(':companyid',CONFIG_CID);

                $dao->addParameter(':hashkey',$hashkey);
                $dao->executeNonQuery();
                $lnkroleid=$dao->getLastInsertedId();
                $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey,$json_data);

                foreach ($privilegecombined as $key=>$value)
                {

                    $Strforgroup= "SELECT privilegegroupid from sysprivileges WHERE id=:id";
                    $dao->initCommand($Strforgroup);
                    $dao->addParameter(':id',$value);
                    $lnkprivilegegroupid=$dao->executeRow();


                    $strSQLRELATION= "INSERT INTO ".CONFIG_DBN.".cfroleprivileges SET  lnkroleid=:lnkroleid,
                lnkprivilegegroupid=:lnkprivilegegroupid, 
                lnkprivilegeid=:lnkprivilegeid,companyid=:companyid";

                    $dao->initCommand($strSQLRELATION);//lnkprivilegegroupid
                    $dao->addParameter(':lnkroleid',$lnkroleid);
                    $dao->addParameter(':lnkprivilegegroupid',$lnkprivilegegroupid['privilegegroupid']);
                    $dao->addParameter(':lnkprivilegeid',$value);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->executeNonQuery();
                }
                return json_encode(array('Success'=>'True','Message'=>$default_langlist->REC_ADD_SUC));
            }
            else
            {
                $privilegearray =[];

                $strgetroleid="SELECT roleunkid FROM ".CONFIG_DBN.".cfuserrole WHERE hashkey=:hashkey 
                AND companyid=:companyid ";
                $dao->initCommand($strgetroleid);
                $dao->addParameter(':companyid',CONFIG_CID);

                $dao->addParameter(':hashkey',$data['id']);
                $rec=$dao->executeRow();
                $roleunkid=$rec['roleunkid'];

                $chk_existing = $ObjDependencyDao->checkduplicaterecord('cfuserrole','user_role',$data['user_role'],$roleunkid);
                if($chk_existing == 1){
                    return json_encode(array('Success'=>'False','Message'=>$langlist->LANG12));
                }

                $title = "Edit Record";

                $upq="UPDATE ".CONFIG_DBN.".cfuserrole SET
                    user_role=:user_role,
                    modified_user=:modified_user,
                    modifieddatetime=:modifieddatetime
                     WHERE hashkey=:hashkey";
                $dao->initCommand($upq);
                $dao->addParameter(':user_role',$data['user_role']);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':hashkey',$data['id']);
                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                $strgetprivleges= "SELECT lnkprivilegeid FROM ".CONFIG_DBN.".cfroleprivileges WHERE lnkroleid=:lnkroleid AND companyid=:companyid AND lnkuserid=0";

                $dao->initCommand($strgetprivleges);
                $dao->addParameter(':lnkroleid',$roleunkid);
                $dao->addParameter(':companyid',CONFIG_CID);

                $priv=$dao->executeQuery();
                $lnkprivilegeid= \util\util::getoneDarray($priv,'lnkprivilegeid');


                if($privilegecombined!=''){
                    foreach ($privilegecombined as $key=>$value){
                        $Strforgroup= "SELECT privilegegroupid from sysprivileges WHERE id=:id";
                        $dao->initCommand($Strforgroup);
                        $dao->addParameter(':id',$value);
                        $lnkprivilegegroupid=$dao->executeRow();

                        if (in_array($value,$lnkprivilegeid )) {

                        }
                        else{

                            $strSQLRELATION= "INSERT INTO ".CONFIG_DBN.".cfroleprivileges SET  lnkroleid=:lnkroleid,
                lnkprivilegegroupid=:lnkprivilegegroupid, 
                lnkprivilegeid=:lnkprivilegeid,companyid=:companyid";

                            $dao->initCommand($strSQLRELATION);
                            $dao->addParameter(':lnkroleid',$roleunkid);
                            $dao->addParameter(':lnkprivilegegroupid',$lnkprivilegegroupid['privilegegroupid']);
                            $dao->addParameter(':lnkprivilegeid',$value);
                            $dao->addParameter(':companyid',CONFIG_CID);
                            $dao->executeNonQuery();
                        }
                        $privilegearray[] = $value;
                    }
                }

                $strsqlforprivi = "DELETE FROM  " . CONFIG_DBN . ".cfroleprivileges                   
                WHERE lnkroleid=:lnkroleid AND companyid=:companyid AND lnkuserid=0 AND lnkprivilegeid  NOT IN ('".implode("','",$privilegearray)."') ";
                $dao->initCommand($strsqlforprivi);
                $dao->addParameter(':lnkroleid',$roleunkid);
                $dao->addParameter(':companyid',CONFIG_CID);
                $dao->executeNonQuery();

                return json_encode(array('Success'=>'True','Message'=>$default_langlist->REC_UP_SUC));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - adduserdetail - '.$e);
        }
    }
    public function userRolelist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - userRolelist - ');
            $dao=new \dao();
            $dateformat = \database\parameter::getBanquetParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $q="select usr.roleunkid,usr.user_role,IFNULL(CFU1.username,'') as created_user,IFNULL(CFU2.username,'') as modifiy_user".
                ",IFNUll(DATE_FORMAT(usr.createddatetime,'".$mysqlformat."'),'') as creted_date".
                ",IFNULL(DATE_FORMAT(usr.modifieddatetime,'".$mysqlformat."'),'') as modified_date".
                ",usr.hashkey,usr.is_active 
                from ".CONFIG_DBN.".cfuserrole as usr
                LEFT JOIN ".CONFIG_DBN.".cfuser as CFU1 ON usr.created_user=CFU1.userunkid AND usr.companyid=CFU1.companyid 
            LEFT JOIN ".CONFIG_DBN.".cfuser as CFU2 ON usr.modified_user=CFU2.userunkid AND usr.modified_user=CFU2.userunkid AND usr.companyid=CFU2.companyid
            
          WHERE usr.companyid=:companyid AND usr.is_deleted=0";
            if($name!='')
                $q .= " AND usr.user_role LIKE '%".$name."%'";
            if($limit!='' && ($offset!='' || $offset!=0))
            {
                $qlimit =" LIMIT ".$limit." OFFSET ".$offset;
            }
            $dao->initCommand($q);
            $dao->addParameter(':companyid',CONFIG_CID);

            $data=$dao->executeQuery();

            if($limit!='' && ($offset!='' || $offset!=0))
            {
                $dao->initCommand($q.$qlimit);
            }
            else
                $dao->initCommand($q);
            $dao->addParameter(':companyid',CONFIG_CID);

            $rec = $dao->executeQuery();

            if(count($data)!=0)
            {
                $setvalue = array(array("cnt"=>count($data),"data"=>$rec));
            }
            else
                $setvalue = array(array("cnt"=>0,"data"=>[]));
            return json_encode($setvalue);

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - userRolelist - '.$e);
        }
    }

    public function filluserRolelist()
    {
        try
        {
            $this->log->logIt($this->module.' - filluserRolelist - ');
            $dao=new \dao();

            $q="select usr.roleunkid,usr.user_role,usr.hashkey,usr.is_active 
                from ".CONFIG_DBN.".cfuserrole as usr 
                 WHERE usr.companyid=:companyid AND usr.is_deleted=0 AND usr.is_active=1";
            $dao->initCommand($q);
            $dao->addParameter(':companyid',CONFIG_CID);
            $data=$dao->executeQuery();
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - filluserRolelist - '.$e);
        }
    }

    public function getUserRoleRec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - getUserRoleRec - ');
            $dao= new \dao();

            $id=$data['id'];
            $str="SELECT user_role,roleunkid FROM  ".CONFIG_DBN.".cfuserrole 
            WHERE companyid=:companyid AND hashkey=:hashkey ";
            $dao->initCommand($str);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':hashkey',$id);
            $rec2=$dao->executeRow();
            $q= "SELECT DISTINCT  lnkprivilegegroupid AS lnkprivilegegroupid,
              lnkprivilegeid AS operational_privileges                 
                 FROM ".CONFIG_DBN.".cfroleprivileges 
                 WHERE companyid=:companyid AND lnkroleid=:lnkroleid  AND lnkuserid=0";
            $dao->initCommand($q);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':lnkroleid',$rec2['roleunkid']);
            $rec=$dao->executeQuery();

            return json_encode(array("Success"=>"True","Data"=>$rec,"UserRole"=>$rec2));
        }
        catch(Exeception $e)
        {
            $this->log->logIt($this->module.' - getUserRoleRec - '.$e);
        }
    }

    public function privileges()
    {
        try
        {
            $this->log->logIt($this->module.' - privileges - ');
            $dao= new \dao();
/*  Date: 18/07/2018    Nikita Since this modules are not in use. they are hidden
 * 13 - Combo products
 * 20 - device
 * 21 - Terminal
 * 19 - Currency List
*/
            $strSql2 = "SELECT id,privilegegroupid,name,groupname,type FROM sysprivileges SYS1 
INNER JOIN sysprivilegegroup SYS2 ON SYS1.privilegegroupid = SYS2.privilegegroupunkid 
WHERE privilegegroupid NOT IN (20,21)";

            if(CONFIG_IS_BANQUET==0) {
                $strSql2 .=" and SYS2.module_name not like 'bm_%'";
            }
            if(CONFIG_IS_STORE==0) {
                $strSql2 .=" and SYS2.type!=2 and SYS2.module_name not like 'company_store'";
            }

            $dao->initCommand($strSql2);
            $rec2 = $dao->executeQuery();

            $result = array();
            foreach ($rec2 as $key2=>$value2){
               $result[$value2['type']][$value2['groupname']][$value2['id']]= $value2['name'];
            }
            return $result;
        }
        catch(Exeception $e)
        {
            $this->log->logIt($this->module.' - privileges - '.$e);
        }
    }
}

?>