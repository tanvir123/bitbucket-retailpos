<?php
namespace database;

class dashboarddao
{
    private $module = 'DB_dashboarddao';
    private $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
    
    public function getaccountdetail()
    {
        try
        {
            $this->log->logIt($this->module.' - getaccountdetail - '.CONFIG_DBN);
           
            return 1;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getaccountdetail - '.$e);
        }
    }
}

?>