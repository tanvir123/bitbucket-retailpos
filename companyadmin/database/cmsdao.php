<?php
namespace database;

class cmsdao
{
    public $module = 'DB_cmsdao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addeditcms($data,$langlist='',$default_langlist='')
    {
        try {
            $this->log->logIt($this->module . ' - addeditcms');
            $dao = new \dao();

            $ObjCommonDao = new \database\commondao();

            $datetime = \util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();
            $tablename = "cms_mst";
            $ObjDependencyDao = new \database\dependencydao();

            $arr_log = array(
                "Title" => $data['title'],
                "Slug" => $data['slug'],
                "SEO Title" => $data['seotitle'],
                "SEO Meta" => $data['seometa'],
                "SEO Description" => $data['seodesc'],
                "Description" => $data['description'],
                'Status' => ($data['rdo_status'] == 1) ? 'Active' : 'Inactive',
            );
            $json_data = html_entity_decode(json_encode($arr_log));
            $hashkey = \util\util::gethash();

            if ($data['id'] == 0) {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "title", $data['title']);

                if ($chk_name == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG21)));
                }

                $chk_alias = $ObjDependencyDao->checkduplicaterecord($tablename, "slug", $data['slug']);

                if ($chk_alias == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG20)));
                }

                $title = "Add Record";

                $strSql = "INSERT INTO " . CONFIG_DBN . ".cms_mst SET
                title=:title,
                slug=:slug,
                description=:description,
                seotitle=:seotitle,
                seodesc=:seodesc,
                seometa=:seometa,
                icon=:icon,
                hashkey=:hashkey,
                companyid=:companyid,
                is_active=:is_active,
                created_user=:created_user,
                createddatetime=:createddatetime";

                $dao->initCommand($strSql);

                $dao->addParameter(':slug', $data['slug']);
                $dao->addParameter(':title', $data['title']);
                $dao->addParameter(':description', $data['description']);
                $dao->addParameter(':seotitle', $data['seotitle']);
                $dao->addParameter(':seodesc', $data['seodesc']);
                $dao->addParameter(':seometa', $data['seometa']);
                $dao->addParameter(':icon', $data['image']);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':is_active', $data['rdo_status']);
                $dao->addParameter(':created_user', CONFIG_UID);
                $dao->addParameter(':createddatetime', $datetime);

                $dao->executeNonQuery();
                $banquetid=$dao->getLastInsertedId();
                $ObjAuditDao->addactivitylog($data['module'], $title, $hashkey, $json_data);


                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $default_langlist->REC_ADD_SUC)));
            } else {


                $id = $ObjCommonDao->getprimarykey('cms_mst', $data['id'], 'z_cmsid_pk');

                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "title", $data['title'], $id);
                if ($chk_name == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG21)));
                }

                $chk_alias = $ObjDependencyDao->checkduplicaterecord($tablename, "slug", $data['slug'], $id);
                if ($chk_alias == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG20)));
                }

                $title = "Edit Record";
                $this->log->logIt($id);

                $strSql = "Update " . CONFIG_DBN . ".cms_mst SET
                slug=:slug,
                title=:title,
                description=:description,
                seotitle=:seotitle,
                seodesc=:seodesc,
                seometa=:seometa,
                icon=:icon,             
                modified_user=:modified_user,
                modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND z_cmsid_pk=:z_cmsid_pk";

                $dao->initCommand($strSql);

                $dao->addParameter(':slug', $data['slug']);
                $dao->addParameter(':title', $data['title']);
                $dao->addParameter(':seotitle', $data['seotitle']);
                $dao->addParameter(':seodesc', $data['seodesc']);
                $dao->addParameter(':seometa', $data['seometa']);
                $dao->addParameter(':description', $data['description']);
                $dao->addParameter(':icon', $data['image']);
                $dao->addParameter(':z_cmsid_pk', $id);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);

                $dao->executeNonQuery();

//pass amenities category id also


                $ObjAuditDao->addactivitylog($data['module'], $title, $data['id'], $json_data);

                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $default_langlist->REC_UP_SUC)));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addeditcms - ' . $e);
        }
    }

    public function cmslist($limit, $offset, $name, $isactive = 0)
    {
        try {
            $this->log->logIt($this->module . ' - cmslist - ' . $name);

            $dao = new \dao;
            $dateformat = \database\parameter::getBanquetParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = "SELECT Cf.*,IFNULL(CFU1.username,'') AS createduser,
                                    IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(Cf.createddatetime,'" . $mysqlformat . "'),'') as created_date,
                                    IFNULL(DATE_FORMAT(Cf.modifieddatetime,'" . $mysqlformat . "'),'') as modified_date
                                    FROM " . CONFIG_DBN . ".cms_mst AS Cf
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU1 ON Cf.created_user=CFU1.userunkid AND Cf.companyid=CFU1.companyid
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU2 ON Cf.modified_user=CFU2.userunkid AND Cf.modified_user=CFU2.userunkid AND Cf.companyid=CFU2.companyid
                                  
                                    WHERE Cf.companyid=:companyid AND Cf.is_deleted=0";
            if ($name != "")
                $strSql .= " AND (title LIKE '%" . $name . "%' OR slug LIKE '%" . $name . "%' )";

            if ($isactive == 1) {
                $strSql .= " and is_active=1 ";
            }
            $strSql .= " ORDER BY Cf.z_cmsid_pk DESC";

            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $data = $dao->executeQuery();
            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);

            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return json_encode($retvalue);
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return json_encode($retvalue);
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - cmslist - ' . $e);
        }
    }

    public function getcatRec($data)
    {
        try {
            $this->log->logIt($this->module . " - getcatRec");

            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $id = $ObjCommonDao->getprimarykey('cms_mst', $data['id'], 'z_cmsid_pk');

            $strSql = "SELECT CFC.*
                      FROM " . CONFIG_DBN . ". cms_mst AS CFC
                      WHERE CFC.z_cmsid_pk=:z_cmsid_pk AND
                      CFC.companyid=:companyid  ";
            $dao->initCommand($strSql);

            $dao->addParameter(':z_cmsid_pk', $id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res)));

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getcatRec - " . $e);
            return false;
        }
    }


}

?>

