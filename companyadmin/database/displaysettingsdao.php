<?php
namespace database;

class displaysettingsdao
{
    public $module = 'DB_bmdisplaysettingsdao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
   
    public function loaddisplaysettingslist()
    {
        try
        {
            $this->log->logIt($this->module.' - loaddisplaysettingslist');
            $dao = new \dao();

            $query = "SELECT keyvalue,keyname FROM ".CONFIG_DBN.".bmdisplaysettings WHERE companyid=:companyid";
            $dao->initCommand($query);
            $dao->addParameter(':companyid', CONFIG_CID);
            $res=$dao->executeQuery();
            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loaddisplaysettingslist - '.$e);
        }
    }
    public function editDisplaysettings($data,$languageArr='')
    {
        try
        {
            $this->log->logIt($this->module.' - editDisplaysettings');
            $dao = new \dao;
            $ObjAuditDao = new \database\auditlogdao();
            $arr_log=[];
            $module_name=$data['module'];
            foreach ($data as $key=>$val){
                if($key!='userid' && $key!='companyid' && $key!='module'){

                    if($key=='timezone') {
                        $val = substr(urldecode($val), 4, 6);
                    }
                    $query = "INSERT INTO ".CONFIG_DBN.".bmdisplaysettings (keyvalue,keyname,companyid) values(:keyvalue,:keyname,:companyid) ON DUPLICATE KEY UPDATE keyvalue=:keyvalue";
                    $dao->initCommand($query);
                    $dao->addParameter(':keyname', $key);
                    $dao->addParameter(':keyvalue', $val);
                    $dao->addParameter(':companyid', CONFIG_CID);
                    $dao->executeNonQuery();

                    if($key=='location_status'){
                        if($val > 0){
                            $loc_val='Yes';
                        }else{
                            $loc_val='No';
                        }
                        $arr_log['Location Wise Order Placed ?']=$loc_val;
                    }elseif($key=='interval'){
                        $arr_log['Hour Interval']=$val;
                    }elseif($key=='timeformat'){
                        $arr_log['Time Format']=$val;
                    }elseif($key=='dateformat'){
                        $arr_log['Date Format']=$val;
                    }elseif($key=='timezone'){
                        $arr_log['Timezone']=$val;
                    }elseif($key=='discount'){
                        $arr_log['Discount Limit']=$val;
                    }elseif($key=='digitafterdecimal'){
                        $arr_log['Digit After decimal']=$val;
                    }
                }
            }
            if(CONFIG_IS_BANQUET==0) {
                unset($arr_log['interval'],$arr_log['Location Wise Order Placed ?']);
            }

            if($data['timezone']!=''){

                $query = "INSERT INTO ".CONFIG_DBN.".bmdisplaysettings (keyvalue,keyname,companyid) values(:keyvalue,:keyname,:companyid) ON DUPLICATE KEY UPDATE keyvalue=:keyvalue";
                $dao->initCommand($query);
                $dao->addParameter(':keyname', 'timezone_main');
                $dao->addParameter(':keyvalue', $data['timezone']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->executeNonQuery();
            }
            $json_data = html_entity_decode(json_encode($arr_log));
            $ObjAuditDao->addactivitylog($module_name, 'Edit Record', '', $json_data,'General Information');

			return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$languageArr->LANG11)));
		}
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - disclist - '.$e);
        }
    }
}

?>