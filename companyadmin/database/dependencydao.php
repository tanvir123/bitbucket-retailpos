<?php

namespace database;

use common\staticarray;

class dependencydao
{
    public $module = 'DB_dependencydao';
    public $log;
    private $default_lang_arr;

    function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language('');
    }

    public function loadrelatedrecords($id = "", $module = "")
    {
        try {
            $this->log->logIt($this->module . ' - loadrelatedrecords');
            $dao = new \dao();
            $data = array();
            $ObjStaticArray = new \common\staticarray();
            if ($module != "" && $id != "") {
                $rel = isset($ObjStaticArray->relatedtables[$module]) ? $ObjStaticArray->relatedtables[$module] : "";
                if ($rel != "" && count($rel) > 0) {
                    foreach ($rel as $key => $value) {
                        $modulename = \common\staticarray::$getmodulename[$key]['Module_Name'];
                        $masterfield = $ObjStaticArray->auditlogmodules[$modulename]['masterfield'];
                        $PrimaryKey = $ObjStaticArray->auditlogmodules[$modulename]['pd'];
                        $strSql = "SELECT GROUP_CONCAT(" . $masterfield . ") AS subject FROM " . CONFIG_DBN . "." . $key . " WHERE " . $value . " = '" . $id . "' AND is_deleted=0 AND companyid=:companyid  ORDER BY " . $PrimaryKey . " DESC LIMIT 3";
                        $dao->initCommand($strSql);
                        $dao->addParameter(":companyid", CONFIG_CID);;
                        $resobj = $dao->executeRow();
                        $data[$key]['subject'] = $resobj['subject'];
                        $data[$key]['module'] = \common\staticarray::$getmodulename[$key]['Display_Name'];
                    }
                }
                return json_encode(array("Success" => "True", "Data" => $data));
            } else {
                return json_encode(array("Success" => "False", "Data" => ""));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - loadrelatedrecords - ' . $e);
        }
    }

    public function checkdependencyold($module, $id)
    {
        try {
            $this->log->logIt($this->module . ' - checkdependency');
            $dao = new \dao;
            $ObjStaticArray = new \common\staticarray();
            $rel = isset($ObjStaticArray->relatedtables[$module]) ? $ObjStaticArray->relatedtables[$module] : "";
            $flag = 0;
            if ($rel != "" && count($rel) > 0) {
                foreach ($rel as $key => $value) {
                    $strSql = "SELECT * FROM " . CONFIG_DBN . "." . $key . " WHERE " . $value . " = '" . $id . "' AND is_deleted=0 AND companyid=:companyid ";
                    $dao->initCommand($strSql);
                    $dao->addParameter(":companyid", CONFIG_CID);;
                    $resobj = $dao->executeQuery();
                    if (count($resobj) > 0) {
                        $flag++;
                    }
                }
            }
            return $flag;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkdependency - ' . $e);
        }
    }

    public function checkdependency($module, $id, $return_type)
    {
        try {
            $this->log->logIt($this->module . ' - checkdependency');
            
            $ObjStaticArray = new \common\staticarray();
            $rel = isset($ObjStaticArray->dependency_check_method[$module]) ? $ObjStaticArray->dependency_check_method[$module] : "";
            $data = "";
            if ($rel != "") {
                $data = $this->$rel($module, $id, $return_type);
            }
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkdependency - ' . $e);
        }
    }

    public function checkcommondependency($module, $id, $return_type, $active = 0)
    {
        try {
            $this->log->logIt($this->module . ' - checkcommondependency');
            $dao = new \dao;
            $ObjStaticArray = new \common\staticarray();
            $rel = isset($ObjStaticArray->relatedtables[$module]) ? $ObjStaticArray->relatedtables[$module] : "";
            $this->log->logIt("Rel :::" . json_encode($rel));
            $flag = 0;
            $data = array();
            if ($rel != "" && count($rel) > 0) {
                foreach ($rel as $key => $value) {
                    $modulename = \common\staticarray::$getmodulename[$key]['Module_Name'];
                    $masterfield = $ObjStaticArray->auditlogmodules[$modulename]['masterfield'];
                    $PrimaryKey = $ObjStaticArray->auditlogmodules[$modulename]['pd'];
                    $cn = $ObjStaticArray->auditlogmodules[$modulename]['status_cn'];
                    $strSql = "SELECT $PrimaryKey FROM " . CONFIG_DBN . "." . $key . " WHERE " . $value . " = '" . $id . "' AND is_deleted=0 AND companyid=:companyid ";
                    if ($active == 1) {
                        $strSql .= " AND " . $cn . "=1";
                    }
                    $dao->initCommand($strSql);
                    $dao->addParameter(":companyid", CONFIG_CID);

                    $resObj = $dao->executeQuery();
                    if (count($resObj) > 0) {
                        $flag++;
                    }
                    if ($return_type == 2) {
                        $strSql2 = "SELECT COUNT($PrimaryKey) as cnt,GROUP_CONCAT(" . $masterfield . ") AS subject FROM " . CONFIG_DBN . "." . $key . " WHERE " . $value . " = '" . $id . "' AND is_deleted=0 AND companyid=:companyid  ORDER BY " . $PrimaryKey . " DESC LIMIT 3";
                        $dao->initCommand($strSql2);
                        $dao->addParameter(":companyid", CONFIG_CID);

                        $resObj2 = $dao->executeRow();
                        if ($resObj2['cnt'] > 0) {
                            $data[$key]['subject'] = $resObj2['subject'];
                            $old_module=\common\staticarray::$getmodulename[$key]['Display_Name'];
                            $module_name_dis=$this->getConvertedLabel($old_module);
                            $data[$key]['module'] = $module_name_dis;
                        }
                    }
                }
            }
            if ($return_type == 1) {
                return $flag;
            }
            if ($return_type == 2) {
                return json_encode(array("Success" => "True", "Data" => $data));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkcommondependency - ' . $e);
        }
    }

    public function checkduplicaterecord($table, $field, $value, $id = "")
    {
        try {
            $this->log->logIt($this->module . ' - checkduplicaterecord');
            $dao = new \dao;
            $ObjStaticArray = new \common\staticarray();
            $modulename = \common\staticarray::$getmodulename[$table]['Module_Name'];
            $PrimaryKey = $ObjStaticArray->auditlogmodules[$modulename]['pd'];
            $strSql = "SELECT " . $PrimaryKey . " FROM " . CONFIG_DBN . "." . $table . " WHERE " . $field . "='" . $value . "' and is_deleted=0 AND companyid=:companyid ";
            if ($id != "") {
                $strSql .= " AND " . $PrimaryKey . "!=" . $id . "";
            }
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $resobj = $dao->executeQuery();
            if (count($resobj) > 0) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkduplicaterecord - ' . $e);
        }
    }

    public function checkstatusdependency($module, $id, $return_type, $active)
    {
        try {
            $this->log->logIt($this->module . ' - checkstatusdependency');
            $dao = new \dao;
            $ObjStaticArray = new \common\staticarray();
            $rel = isset($ObjStaticArray->dependency_check_method[$module]) ? $ObjStaticArray->dependency_check_method[$module] : "";
            $data = "";
            if ($rel != "") {
                $data = $this->$rel($module, $id, $return_type, $active);
            }
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - checkstatusdependency - ' . $e);
        }
    }

    public function getamenitiesrelateditems($module, $id, $return_type, $active = 0)
    {
        try {
            $this->log->logIt($this->module . ' - getamenitiesrelateditems');
            $dao = new \dao();
            $flag = 0;
            $data = array();
            $result = array();
            $strSql = "SELECT COUNT(BMB.banquetunkid) as cnt,GROUP_CONCAT(BMB.banquetname) AS subject FROM " . CONFIG_DBN . ".bmbanquet BMB INNER JOIN " . CONFIG_DBN . ".bmamenities_banquet_rel as REL on REL.banquetunkid=BMB.banquetunkid 
                            WHERE FIND_IN_SET('" . $id . "',REL.amenitiesunkid) AND BMB.is_deleted=0 AND BMB.companyid=:companyid  
                            AND REL.is_deleted=0 AND REL.companyid=:companyid";
            if ($active == 1) {
                $strSql .= " AND BMB.is_active=1";
            }
            $strSql .= " ORDER BY BMB.banquetunkid";

            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":id", $id);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $old_module=\common\staticarray::$getmodulename['bmbanquet']['Display_Name'];
                    $module_name_dis=$this->getConvertedLabel($old_module);
                    $data[$module]['module'] = $module_name_dis;
                    $result[] = $data[$module];
                }
            }


            if ($return_type == 1) {
                return $flag;
            }
            if ($return_type == 2) {
                return json_encode(array("Success" => "True", "Data" => $result));
            }

        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getamenitiesrelateditems - ' . $e);
        }
    }

    public function getBanquetRelSeatingplan($module, $id, $return_type, $active = 0)
    {
        try {
            $this->log->logIt($this->module . ' - getBanquetRelSeatingplan');
            $dao = new \dao();
            $flag = 0;
            $data = array();
            $result = array();
            $dateformat = \database\parameter::getBanquetParameter('dateformat');
            $timeformat = \database\parameter::getBanquetParameter('timeformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $mysqltimeformat = \common\staticarray::$mysqltimeformat[$timeformat];

            $strSql = "SELECT COUNT(BMB.seatingplanunkid) as cnt,GROUP_CONCAT(BMB.name) AS subject FROM " . CONFIG_DBN . ".bmseating_plan BMB INNER JOIN " . CONFIG_DBN . ".bmseatingplan_banquet_rel as REL on REL.seatingplanunkid=BMB.seatingplanunkid
                            WHERE FIND_IN_SET('" . $id . "',REL.banquetunkid) AND BMB.is_deleted=0 AND BMB.companyid=:companyid  
                            AND REL.is_deleted=0 AND REL.companyid=:companyid";
            if ($active == 1) {
                $strSql .= " AND BMB.is_active=1";
            }
            $strSql .= " ORDER BY BMB.seatingplanunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addParameter(":id", $id);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $old_module=\common\staticarray::$getmodulename['bmseating_plan']['Display_Name'];
                    $module_name_dis=$this->getConvertedLabel($old_module);
                    $data[$module]['module'] =$module_name_dis;
                    $result[] = $data[$module];
                }
            }

            $strSql2 = "SELECT COUNT(BMB.blockrequnkid) as cnt,GROUP_CONCAT(DATE_FORMAT(BMB.startdatetime,'" . $mysqlformat ." ".$mysqltimeformat. "'),' To ',DATE_FORMAT(BMB.enddatetime,'" . $mysqlformat ." ".$mysqltimeformat. "') SEPARATOR '<br>') AS subject FROM " . CONFIG_DBN . ".bmblock_banquet BMB 
                            WHERE FIND_IN_SET('" . $id . "',BMB.banquetunkid) AND BMB.is_deleted=0 AND BMB.companyid=:companyid";
            if ($active == 1) {
                $strSql2 .= " AND BMB.is_active=1";
            }
            $strSql2 .= " ORDER BY BMB.blockrequnkid";

            $dao->initCommand($strSql2);
            $dao->addParameter(":companyid", CONFIG_CID);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $old_module=\common\staticarray::$getmodulename['bmblock_banquet']['Display_Name'];
                    $module_name_dis=$this->getConvertedLabel($old_module);
                    $data[$module]['module'] = $module_name_dis;
                    $result[] = $data[$module];
                }
            }

            $strSql2 = "SELECT COUNT(BB.bmbookunkid) as cnt,GROUP_CONCAT(DATE_FORMAT(BB.startdate,'" . $mysqlformat . "'),' ',DATE_FORMAT(BB.starttime,'" . $mysqltimeformat . "'),' To ',DATE_FORMAT(BB.enddate,'" . $mysqlformat. "'),' ',DATE_FORMAT(BB.endtime,'" . $mysqltimeformat . "') SEPARATOR '<br>') AS subject FROM " . CONFIG_DBN . ".bmbanquet_booking BB 
                            WHERE FIND_IN_SET('" . $id . "',BB.lnkbanquetunkid) AND BB.is_deleted=0 AND BB.companyid=:companyid";
            if ($active == 1) {
                $strSql2 .= " AND BB.is_active=1";
            }
            $strSql2 .= " ORDER BY BB.bmbookunkid";

            $dao->initCommand($strSql2);
            $dao->addParameter(":companyid", CONFIG_CID);
            $resObj = $dao->executeRow();
            if ($resObj['cnt'] > 0) {
                $flag++;
            }
            if ($return_type == 2) {
                if ($resObj['cnt'] > 0) {
                    $data[$module]['subject'] = $resObj['subject'];
                    $old_module=\common\staticarray::$getmodulename['bmbanquet_booking']['Display_Name'];
                    $module_name_dis=$this->getConvertedLabel($old_module);
                    $data[$module]['module'] = $module_name_dis;
                    $result[] = $data[$module];
                }
            }



            if($return_type==1){
                return $flag;
            }
            if($return_type==2) {
                return json_encode(array("Success"=>"True","Data"=>$result));
            }



        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - getBanquetRelSeatingplan - ' . $e);
        }
    }




    public function loadlaguage()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }

    public function getConvertedLabel($modulename='')
    {
        $default_arr = \common\staticlang::$banquet_default;
        $this->loadlaguage();
        $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
        $default_langlist = json_decode($defaultlanguageArr);
        $oldlng_arr_search_key=array_search($modulename,$default_arr);
        $converted_module=$default_langlist->$oldlng_arr_search_key;
        if($converted_module){
            return $converted_module;
        }
        return $modulename;
    }


}

?>