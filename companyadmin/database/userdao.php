<?php
namespace database;

class userdao
{
    public $module = 'DB_userdao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }
   /*  code not in use ! User : Nikita Date :24/07/2018 public function displayPrivileges($data)
    {
        try {
            $this->log->logIt($this->module . " - displayPrivileges");
            $dao = new \dao();
            $strSql = "SELECT DISTINCT lnkprivilegegroupid,lnkprivilegeid as priv
              FROM " . CONFIG_DBN . ".cfroleprivileges WHERE lnkroleid=:lnkroleid AND lnkuserid=0";

            $dao->initCommand($strSql);
            $dao->addParameter(':lnkroleid', $data['userroleid']);

            $res = $dao->executeQuery();


            return json_encode(array("Data" => $res));
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - displayPrivileges - " . $e);
            return false;
        }
    }*/
    public function addUser($data,$langlist='',$default_langlist='')
    {
        try
        {
            $this->log->logIt($this->module.' - addUser');
            $dao = new \dao();
            $datetime=\util\util::getLocalDateTime();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $arr_log = array(
                'Name'=>$data['name'],
                'Username'=>$data['username'],
                'E-Mail ID'=>$data['emailid'],
                'Mobile No'=>$data['mobileno'],
                'Short code'=>$data['shortcode'],
            );
            $json_data = json_encode($arr_log);
            $res = $data;
            $location_id=0;
            $user_type=0;


            if($data['sys_defined']!=0)
            {
                 $str="SELECT GROUP_CONCAT(syslocationid,'') AS syslocationid FROM syslocation WHERE companyid=:companyid";
                $dao->initCommand($str);
                $dao->addParameter(':companyid', CONFIG_CID);
                $location_id = $dao->executeRow();

                $sql = "SELECT GROUP_CONCAT(storeunkid,'') AS sysstoreid FROM ".CONFIG_DBN.".cfstoremaster WHERE companyid=:companyid";
                $dao->initCommand($sql);
                $dao->addParameter(':companyid',CONFIG_CID);
                $userstore =$dao->executeRow();

                $user_type=1;
                $storeid=$userstore['sysstoreid'];
                $location_id=$location_id['syslocationid'];
            }
            else{
                $user_type=2;

                if($data['usr_location']!='' && count($data['usr_location'])>0){
                    $location_id = implode(',',$data['usr_location']);
                }

                if(isset($data['storeid']) && $data['storeid']!=''){

                    $storeid=implode(',',$data['storeid']);
                }
            }
            $storeid=isset($storeid)?$storeid:0;
            $location_id=isset($location_id)?$location_id:0;

            if($data['id']=='0' || $data['id']==0)
            {

                $title = "Add Record";
                $hashkey = \util\util::gethash();
                $sql = "SELECT * FROM ".CONFIG_DBN.".cfuser AS USR WHERE USR.companyid=:companyid AND
                 USR.username=:username AND is_deleted=0";
                $dao->initCommand($sql);
                $dao->addParameter(':username',$res['username']);
                $dao->addParameter(':companyid',CONFIG_CID);
                $userdata =$dao->executeQuery();
                if(count($userdata) !== 0)
                {
                    return html_entity_decode(json_encode(array('Success'=>'false','Message'=>$langlist->LANG27)));
                }
                else
                {
                    $strSql = "INSERT INTO ".CONFIG_DBN.".trcontact(
                                                                    shortcode,
                                                                    name,                                                                     
                                                                    email, 
                                                                    mobile,
                                                                    contacttype,
                                                                    companyid,                                                                    
                                                                    hashkey
                                                                    ) 
                                                            VALUES(
                                                                    :shortcode,
                                                                    :name,                                                                    
                                                                    :emailid, 
                                                                    :mobileno,
                                                                    :contacttype,
                                                                    :companyid,                                                                    
                                                                    :hashkey
                                                                    )";

                    $dao->initCommand($strSql);
                    $dao->addParameter(':shortcode',$res['shortcode']);
                    $dao->addParameter(':name',$res['name']);
                    $dao->addParameter(':emailid',$res['emailid']);
                    $dao->addParameter(':mobileno',$res['mobileno']);
                    $dao->addParameter(':contacttype',1);
                    $dao->addParameter(':hashkey',$hashkey);
                    $dao->addParameter(':companyid',CONFIG_CID);

                    $dao->executeNonQuery();
                    $userdata = $dao->getLastInsertedId();


                    $hashkey1 = \util\util::gethash();
                    $strSql1 = "INSERT INTO sysuser(username,
                                                    password,
                                                    user_type,
                                                    roleunkid,
                                                    lang_type,
                                                    lang_code,
                                                    contactunkid,
                                                    companyid,                                                    
                                                    storeid,
                                                    locationid,
                                                    isactive,
                                                    created_user,
                                                    createddatetime,
                                                    hashkey,
                                                    fiscal_tinno
                                                    ) 
                                            VALUES( :username,
                                                    :password, 
                                                    :user_type,
                                                    :roleunkid,
                                                    :lang_type,
                                                    :lang_code,
                                                    :contactunkid,
                                                    :companyid,                                                    
                                                    :storeid,
                                                    :locationid,
                                                    :isactive,
                                                    :userid,
                                                    :createddatetime,
                                                    :hashkey,
                                                    :fiscal_tinno
                                                   ) ";
                    $dao->initCommand($strSql1);
                    $dao->addParameter(':username',$res['username']);
                    $dao->addParameter(':password',md5($res['password']));
                    $dao->addParameter(':user_type',$user_type);
                    $dao->addParameter(':roleunkid',$data['roleunkid']);
                    $dao->addParameter(':lang_type',$res['lang_type']);
                    $dao->addParameter(':lang_code',$res['lang_code']);
                    $dao->addParameter(':userid',CONFIG_UID);
                    $dao->addParameter(':storeid',$storeid);
                    $dao->addParameter(':locationid',$location_id);
                    $dao->addParameter(':isactive',1);
                    $dao->addParameter(':createddatetime',$datetime);
                    $dao->addParameter(':contactunkid',$userdata);
                    $dao->addParameter(':hashkey',$hashkey1);
                    $dao->addParameter(':companyid',CONFIG_CID);
                    $dao->addParameter(':fiscal_tinno',$data['fiscal_tinno']);
                    $dao->executeNonQuery();

                    $ObjAuditDao->addactivitylog($data['module'],$title,$hashkey1,$json_data);
                    return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$default_langlist->REC_ADD_SUC,'Username'=>$data['username'])));
                }
            }
            else
            {
                $title = "Edit Record";

                $strSql="UPDATE sysuser SYS 
                        INNER JOIN ".CONFIG_DBN.".trcontact TRC 
                        ON (SYS.contactunkid=TRC.contactunkid)
                            SET 
                                 SYS.username=:username,SYS.modifieddatetime=:modifieddatetime,SYS.modified_user=:modified_user,";

                                 if(CONFIG_USR_TYPE==1){
                                     $strSql.="SYS.roleunkid=:roleunkid,
                                            SYS.storeid=:storeid,
                                            SYS.locationid=:locationid,";
                                 }

                $strSql.="SYS.user_type=:user_type,SYS.fiscal_tinno=:fiscal_tinno,
                                 lang_type=:lang_type,lang_code=:lang_code,
                                 TRC.name=:name,
                                 TRC.email=:emailid,
                                 TRC.mobile=:mobileno,                                 
                                 TRC.shortcode=:shortcode,
                                 TRC.modifieddatetime=:modifieddatetime,
                                 TRC.modified_user=:modified_user
                        WHERE SYS.hashkey=:userunkid";

                $dao->initCommand($strSql);
                $dao->addParameter(':shortcode',$res['shortcode']);
                $dao->addParameter(':name',$res['name']);
                $dao->addParameter(':lang_type',$res['lang_type']);
                $dao->addParameter(':lang_code',$res['lang_code']);
                $dao->addParameter(':mobileno',$res['mobileno']);
                $dao->addParameter(':emailid',$res['emailid']);
                if(CONFIG_USR_TYPE==1) {
                    $dao->addParameter(':storeid',isset($storeid)?$storeid:'');
                    $dao->addParameter(':locationid',$location_id);
                    $dao->addParameter(':roleunkid',isset($res['roleunkid'])?$res['roleunkid']:0);
                }
                $dao->addParameter(':user_type',$user_type);
                $dao->addParameter(':userunkid',$res['id']);
                $dao->addParameter(':modifieddatetime',$datetime);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->addParameter(':username',$res['username']);
                $dao->addParameter(':fiscal_tinno',$data['fiscal_tinno']);

                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'],$title,$data['id'],$json_data);
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$default_langlist->REC_UP_SUC)));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - addUser - '.$e);
        }
    }

    public function userlist($limit,$offset,$name)
    {
        try
        {
            $this->log->logIt($this->module.' - userlist - '.$name);

            $dao = new \dao;
            $dateformat = \database\parameter::getBanquetParameter('dateformat');
            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];

            $strSql = "SELECT USR.userunkid as userid,USR.sysdefined,USR.username, IFNULL(USR1.username,'') AS created_user, IFNULL(USR2.username,'') AS modified_user, IFNULL(TRC.mobile,'') as mobile".
                " ,IFNULL(DATE_FORMAT(USR.createddatetime,'".$mysqlformat."'),'') as created_date".
                " ,IFNULL(DATE_FORMAT(USR.modifieddatetime,'".$mysqlformat."'),'') as modified_date".
                " ,USR.hashkey,USR.isactive
                        FROM ".CONFIG_DBN.".cfuser AS USR
                        LEFT JOIN ".CONFIG_DBN.".trcontact AS TRC
                            ON USR.contactunkid=TRC.contactunkid AND TRC.companyid=:companyid
                        LEFT JOIN ".CONFIG_DBN.".cfuser as USR1
                            ON USR1.userunkid = USR.created_user AND USR1.companyid=:companyid
                        LEFT JOIN ".CONFIG_DBN.".cfuser as USR2
                            ON USR2.userunkid=USR.modified_user AND USR2.companyid=:companyid
                        WHERE USR.companyid=:companyid  AND USR.is_deleted=0";
            if($name!="")
                $strSql .= " AND USR.username LIKE '%".$name."%'";
            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $data = $dao->executeQuery();

            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return json_encode($retvalue);
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return json_encode($retvalue);
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - userlist - '.$e);
        }
    }

    public function getUserRec($data)
    {
        try
        {
            $this->log->logIt($this->module." - getUserRec");
            $dao = new \dao();
            $id=$data['id'];
            $ObjCommonDao = new \database\commondao();
            $user_id = $ObjCommonDao->getprimarykey('cfuser',$id,'userunkid');

           
            $strSql = "SELECT USR.userunkid,USR.sysdefined,USR.username,USR.user_type,IFNULL(USR.locationid,'') as usr_location,
                       USR.password, USR.isactive, TRC.shortcode, TRC.name, TRC.email, 
                       TRC.mobile, USR.lang_type,USR.lang_code ,USR.roleunkid,USR.storeid,USR.hashkey,USR.fiscal_tinno
                       FROM ".CONFIG_DBN.".cfuser AS  USR
                       INNER JOIN ".CONFIG_DBN.".trcontact AS TRC 
                       ON USR.contactunkid = TRC.contactunkid 
                      WHERE USR.companyid=:companyid AND USR.hashkey=:userunkid ";

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':userunkid',$id);

            $res = $dao->executeRow();
           /* $str="SELECT lnkprivilegeid as pirv,lnkprivilegegroupid,lnkuserid FROM  ".CONFIG_DBN.".cfroleprivileges
            WHERE lnkroleid=:lnkroleid AND companyid=:companyid AND lnkuserid IN(0,$user_id) ";
            $dao->initCommand($str);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':lnkroleid',$res['roleunkid']);


            $res2 = $dao->executeQuery();*/


            return html_entity_decode(json_encode(array("Success"=>"True","Data"=>$res)));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUserRec - ".$e);
            return false;
        }
    }

    public function changePassword($data,$langlist='',$default_langlist='')
    {
        try
        {
            $this->log->logIt($this->module.' - changePassword');
            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $ObjAuditDao = new \database\auditlogdao();
            $res = $data;
            $current_date = \util\util::getLocalDateTime();
            if($res['id']!='' || $res['id']!=0)
            {
                //$id = $ObjCommonDao->getprimarykey('cfuser',$res['id'],'userunkid');
                $strSql="UPDATE sysuser SYS SET 
                          SYS.password=:password,SYS.modifieddatetime=:modifieddatetime,SYS.modified_user=:modified_user
                          WHERE SYS.hashkey=:userunkid";
                $dao->initCommand($strSql);
                $dao->addParameter(':password',md5($res['newpwd']));
                $dao->addParameter(':userunkid',$res['id']);
                $dao->addParameter(':modifieddatetime',$current_date);
                $dao->addParameter(':modified_user',CONFIG_UID);
                $dao->executeNonQuery();
                $ObjAuditDao->addactivitylog($data['module'],'Change Password',$data['id'],'');
                return html_entity_decode(json_encode(array('Success'=>'True','Message'=>$langlist->LANG28)));
            }else{
                return html_entity_decode(json_encode(array('Success'=>'False','Message'=>$default_langlist->INTERNAL_ERROR)));
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - changePassword - '.$e);
        }
    }

    public function getUserDetail()
    {
        try
        {
            $this->log->logIt($this->module." - getUserDetail");
            $dao = new \dao();
            $strSql = "SELECT username,sysdefined,user_type FROM ".CONFIG_DBN.".cfuser 
            WHERE companyid=:companyid AND userunkid=:userunkid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':userunkid',CONFIG_UID);
            $res = $dao->executeRow();
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUserDetail - ".$e);
            return false;
        }
    }
    public function getUserRole()
    {
        try
        {
            $this->log->logIt($this->module." - getUserRole");
            $dao = new \dao();
            $strSql = "SELECT user_role,roleunkid FROM ".CONFIG_DBN.".cfuserrole 
            WHERE companyid=:companyid 
            AND is_active=1 and is_deleted=0 ORDER BY user_role ASC";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);

            $res = $dao->executeQuery();

            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - getUserRole - ".$e);
            return false;
        }
    }
    public function getLocationList()
    {
        try
        {
            $this->log->logIt($this->module.' - getLocationList');
            $dao = new \dao();
            $strSql = "SELECT syslocationid, locationname FROM syslocation WHERE is_deleted=0 AND companyid=:companyid ORDER BY syslocationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $rec = $dao->executeQuery();
            return $rec;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - getLocationList - '.$e);
        }
    }
    public function getstorelist()
    {
        try {
            $this->log->logIt($this->module . " - getstorelist");
            $dao = new \dao();
            $strSql = "SELECT storename,storeunkid FROM " . CONFIG_DBN . ".cfstoremaster 
            WHERE companyid=:companyid 
            AND is_active=1 and is_deleted=0 ORDER BY storename ASC";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);

            $res = $dao->executeQuery();


            return $res;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getstorelist - " . $e);
            return false;
        }
    }

}
?>
