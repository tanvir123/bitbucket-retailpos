<?php
namespace database;

class orderlogdao
{
    public $module = 'DB_orderlogdao';
    public $log;
    public $dbconnect;

    function __construct()
    {
        $this->log = new \util\logger();
    }


    public function addOrderLog($folio_id,$operation,$order_data)
    {
        try
        {
            $this->log->logIt($this->module.'-addOrderLog');
            $datetime = \util\util::getLocalDateTime();
            $dao = new \dao();
            $user_ip = \util\util::VisitorIP();
            $arr_detail = array();
            if($operation=='ADD_ORDER')
            {
                if(isset($order_data) && !empty($order_data))
                {
                    $arr_detail['FOLIO_NO'] = $order_data['folio_no'];
                    $arr_detail['NAME'] = $order_data['guest_name'];
                    $arr_detail['DESCRIPTION'] = "Add Order";
                }
            }
            /*if($operation=='MERGE_ORDER'){
                $arr_detail['DESCRIPTION'] = "Merge Order From folio's : ".$order_data['folio_nos'];
            }
            if($operation=='SPLIT_BY_FOLIO'){
                $arr_detail['DESCRIPTION'] = "Split charges into folio : ".$order_data['split_into'];
            }
            if($operation=='SPLIT_BY_NAME'){
                $arr_detail['DESCRIPTION'] = "Split charges into folio : ".$order_data['split_into'];
            }
            if($operation=='CREATE_FOLIO_FROM_SPLIT'){
                $arr_detail['DESCRIPTION'] = "Folio created by split from : ".$order_data['split_from'];
            }
            if($operation=='TRANSFER_KOT'){
                $arr_detail['DESCRIPTION'] = "Order transferred from table ".$order_data['old_table']." to ".$order_data['new_table'];
            }
            if($operation=='KOT_ITEM_TRANSFER'){
                if($order_data['transfer_mode']==1){
                    $arr_detail['DESCRIPTION'] = "Item transferred to folio ".$order_data['des_folio'];
                }
                if($order_data['transfer_mode']==2){
                    $arr_detail['DESCRIPTION'] = "Item transferred from folio ".$order_data['src_folio'];
                }
            }*/
            if(is_array($arr_detail) && count($arr_detail)>0){
                $arr_json = json_encode($arr_detail);
            }else{
                $arr_json = '';
            }
            $strSql = "INSERT INTO ".CONFIG_DBN.".cfauditlog SET
                       lnkfolioid=:lnkfolioid,operation=:operation,description=:description,
                       userid=:userid,user_ip=:user_ip,username=:username,datetime=:datetime,
                       companyid=:companyid,locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(':lnkfolioid',$folio_id);
            $dao->addParameter(':operation',$operation);
            $dao->addParameter(':description',$arr_json);
            $dao->addParameter(':userid',CONFIG_UID);
            $dao->addParameter(':user_ip',$user_ip);
            $dao->addParameter(':username',CONFIG_UNM);
            $dao->addParameter(':datetime',$datetime);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':locationid',$order_data['locationid']);
            $dao->executeNonQuery();
            return 1;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.'-addOrderLog-'.$e);
        }
    }
}

?>