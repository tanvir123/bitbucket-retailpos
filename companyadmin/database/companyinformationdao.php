<?php
namespace database;

class companyinformationdao
{
    public $module = 'DB_companyinformationdao';
    public $log;
    
    function __construct()
    {
        $this->log = new \util\logger();
    }
    public function loadcompanyinformationlist()
    {
        try
        {
			
            $this->log->logIt($this->module.' - loadcompanyinformationlist');
            $dao = new \dao();
            $res['country'] = \database\parameter::getParameter('country');
            $strSql = "SELECT * FROM ".CONFIG_DBN.".cfcompany WHERE companyid=:companyid";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $res = $dao->executeRow();
            return json_encode(array("Success"=>"True","Data"=>$res));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - loadcompanyinformationlist - '.$e);
        }
    }

    public function countrylist()
    {
        try
        {
            $this->log->logIt($this->module.' - countrylist ');
            $dao = new \dao;
            $strSql = "SELECT * FROM ".CONFIG_DBN.".vwcountry";
            $dao->initCommand($strSql);
			$data = $dao->executeQuery();
			if(count($data) != 0){
              $retvalue = array(array("cnt"=>count($data),"data"=>$data));
              return json_encode($retvalue);
            }
            else{
               $retvalue = array(array("cnt"=>0,"data"=>[]));
				return json_encode($retvalue);
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - countrylist - '.$e);
        }
    }
	
	public function editCompanyInformation($data,$default_langlist='')
    {
        try
        {
            $this->log->logIt($this->module.' - editCompanyInformation');
            $dao = new \dao();
            $strSql = "UPDATE ".CONFIG_DBN.".cfcompany
                        SET 
                            companyname=:txtcompanyname,
                            address1=:txtaddress1,
                            address2=:txtaddress2,
                            country=:select_country,
                            state=:txtstate,
                            city=:txtcity,
                            zipcode=:txtzipcode,
                            phone=:txtphoneno,
                            reservation_contact_no=:txtrescontactno,
                            fax=:txtfaxno,
                            companyemail=:txtemail,
                            property_type=:txtpropertytype,
                            website=:txtwebsite,
                            currency_sign=:txtsymbol,
                            main_currency=:main_currency,
                            fractional_currency=:fractional_currency,
                            registration_no1=:txtregno1,
                            registration_no2=:txtregno2,
                            registration_no3=:txtregno3,
                            logo=:upload_img
                            where 
                            companyid=:companyid";

            $dao->initCommand($strSql);
            $dao->addParameter(':txtcompanyname',$data['companyname']);
            $dao->addParameter(':txtaddress1',$data['address1']);
            $dao->addParameter(':txtaddress2',$data['address2']);
            $dao->addParameter(':select_country',$data['country']);
            $dao->addParameter(':txtstate',$data['state']);
            $dao->addParameter(':txtcity',$data['city']);
            $dao->addParameter(':txtzipcode',isset($data['zipcode'])?$data['zipcode']:'');
            $dao->addParameter(':txtphoneno',$data['phoneno']);
            $dao->addParameter(':txtrescontactno',$data['reservation_contact_no']);
            $dao->addParameter(':txtfaxno',$data['fax']);
            $dao->addParameter(':txtemail',$data['email']);
            $dao->addParameter(':txtpropertytype',$data['property_type']);
            $dao->addParameter(':txtwebsite',$data['website']);
            $dao->addParameter(':txtregno1',isset($data['registration_no1'])?$data['registration_no1']:'');
            $dao->addParameter(':txtregno2',$data['registration_no2']);
            $dao->addParameter(':txtregno3',$data['registration_no3']);
            $dao->addParameter(':txtsymbol',$data['currency_sign']);
            $dao->addParameter(':main_currency',$data['main_currency']);
            $dao->addParameter(':fractional_currency',$data['fractional_currency']);
            $dao->addParameter(':upload_img',$data['image']);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->executeNonQuery();
            $strSql1 = "Update syscompany SET companyname=:companyname,companyemail=:companyemail where syscompanyid=:syscompanyid";
            $dao->initCommand($strSql1);
            $dao->addParameter(':companyname',$data['companyname']);
            $dao->addParameter(':companyemail',$data['email']);
            $dao->addParameter(':syscompanyid',CONFIG_CID);
            $dao->executeNonQuery();
            return json_encode(array('Success'=>'True','Message'=>$default_langlist->REC_UP_SUC));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - editCompanyInformation - '.$e);
            return 0;
        }
    }
}
?>