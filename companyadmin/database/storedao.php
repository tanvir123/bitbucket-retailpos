<?php
namespace database;

class storedao
{
    public $module = 'DB_storedao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addStore($data,$langlist='',$default_langlist='')
    {
        try {
            $this->log->logIt($this->module . ' - addStore');
            $dao = new \dao();

            $ObjCommonDao = new \database\commondao();

            $datetime = \util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();
            $tablename = "cfstoremaster";
            $ObjDependencyDao = new \database\dependencydao();
            //storename
            $arr_log = array(
                'Store Name' => $data['storename'],
                'Contact Person' => $data['storename'],
                'Country' => $data['countryname'],
                'Address' => $data['address'],
                'Email' => $data['email'],
                'City' => $data['city'],
                'Contact No' => $data['contact'],
                'Status' => ($data['rdo_status'] == 1) ? 'Active' : 'Inactive',
            );

            $json_data = html_entity_decode(json_encode($arr_log));
            $hashkey = \util\util::gethash();
            if ($data['id'] == 0) {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "storename", $data['storename']);

                if ($chk_name == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG12)));
                }

                $title = "Add Record";

              /*  $strSql = "INSERT INTO sysstoremaster SET storename=:storename,contactperson=:contactperson,
                           companyid=:companyid,createddatetime=NOW()";

                $dao->initCommand($strSql);
                $dao->addParameter(":storename", $data['storename']);
                $dao->addParameter(":contactperson", $data['contactperson']);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->executeNonQuery();
                $storeid = $dao->getLastInsertedId();*/

                $data1 = json_decode($this->getStoreCount(), 1);

                $actual_store = $data1['Actual_Store_Count'];
                $current_store_count = $data1['Current_Store_Count'];

                if ($actual_store != '' || $actual_store != 0) {
                    if ($actual_store <= $current_store_count) {
                        return html_entity_decode(json_encode(array('Success' => 'false', 'Message' => $langlist->LANG13)));
                    }

                }

                $strSql = "INSERT INTO " . CONFIG_DBN . ".cfstoremaster SET
                           storename=:storename,
                            contactperson=:contactperson, country=:country,
                            is_mainstore=:is_mainstore,address=:address,
                            city=:city,email=:email,
                            hashkey=:hashkey,contact=:contact,
                            companyid=:companyid,is_active=:is_active,
                            created_user=:created_user,createddatetime=:createddatetime";
                $dao->initCommand($strSql);
                
                $dao->addParameter(':storename', $data['storename']);
                $dao->addParameter(':contactperson', $data['contactperson']);
                $dao->addParameter(':country', $data['country']);
                $dao->addParameter(':is_mainstore', 0);
                $dao->addParameter(':address', $data['address']);
                $dao->addParameter(':city', $data['city']);
                $dao->addParameter(':email', $data['email']);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':contact', $data['contact']);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':is_active', $data['rdo_status']);
                $dao->addParameter(':created_user', CONFIG_UID);
                $dao->addParameter(':createddatetime', $datetime);
                $dao->executeNonQuery();
                $storeid = $dao->getLastInsertedId();

                $strforadmin = "SELECT userunkid FROM sysuser WHERE sysdefined=1 and isactive=1 and is_deleted=0 and companyid=:companyid";
                $dao->initCommand($strforadmin);
                $dao->addParameter(':companyid', CONFIG_CID);
                $adminid = $dao->executeRow();

                $strforadmin = "SELECT GROUP_CONCAT(storeunkid,'') as storeid FROM ".CONFIG_DBN.".cfstoremaster WHERE  is_active=1 and is_deleted=0 and companyid=:companyid";

                $dao->initCommand($strforadmin);
                $dao->addParameter(':companyid', CONFIG_CID);
                $cfstoreid = $dao->executeRow();

                $strupdateadminstore = "UPDATE sysuser SET storeid=:storeid WHERE companyid=:companyid AND userunkid=:userunkid";
                $dao->initCommand($strupdateadminstore);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':userunkid', $adminid['userunkid']);
                $dao->addParameter(':storeid', $cfstoreid['storeid']);
                $dao->executeNonQuery();

                /*Add default indent numbers*/
                    $strSqlIndent = " INSERT INTO ".CONFIG_DBN.".cfindentnumber (keyname,displayname,prefix,startno,reset_type,storeid,companyid,hashkey)
                                    SELECT SI.keyname,SI.displayname,SI.prefix,SI.startno,SI.reset_type,:storeid,:companyid,
                                    concat(lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0),lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0),
                                    lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0), lpad(conv(floor(rand()*pow(36,8)), 10, 36), 8, 0) ) FROM sysindentnumber AS SI WHERE SI.type=1";
                    $dao->initCommand($strSqlIndent);
                    $dao->addParameter(":companyid", CONFIG_CID);
                    $dao->addParameter(":storeid", $storeid);
                    $dao->executeNonQuery();
                /*Add default indent numbers*/

                /*Add parameters*/
                    $strSql5 = " INSERT INTO ".CONFIG_DBN.".cfparameter (keyname, keyvalue, description, companyid,locationid,storeid,type) SELECT sys.keyname,sys.keyvalue,sys.description,:companyid,0,:storeid,2 FROM sysparameter AS sys WHERE sys.companyid=0 AND type=2";
                    $dao->initCommand($strSql5);
                    $dao->addParameter(":storeid", $storeid);
                    $dao->addParameter(":companyid", CONFIG_CID);
                    $dao->executeNonQuery();

                /*Add parameters*/
                $ObjAuditDao->addactivitylog($data['module'], $title, $hashkey, $json_data);

                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $default_langlist->REC_ADD_SUC)));
            } else {


                $id = $ObjCommonDao->getprimarykey('cfstoremaster', $data['id'], 'storeunkid');

                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "storename", $data['storename'], $id);
                if ($chk_name == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG12)));
                }

                $title = "Edit Record";


              /*  $strSql = "UPDATE  sysstoremaster SET storename=:storename,contactperson=:contactperson 
WHERE companyid=:companyid AND sysstoreid=:sysstoreid";

                $dao->initCommand($strSql);
                $dao->addParameter(":storename", $data['storename']);
                $dao->addParameter(":contactperson", $data['contactperson']);
                $dao->addParameter(":companyid", CONFIG_CID);
                $dao->addParameter(":sysstoreid", $id);
                $dao->executeNonQuery();*/

                $strSql = "Update " . CONFIG_DBN . ".cfstoremaster SET
                storename=:storename,
                contactperson=:contactperson,
                country=:country,
                is_mainstore=:is_mainstore,
                address=:address,
                city=:city,
                email=:email,
                hashkey=:hashkey,
                contact=:contact,           
                modified_user=:modified_user,
                modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND storeunkid=:storeunkid";

                $dao->initCommand($strSql);

                $dao->addParameter(':storename', $data['storename']);
                $dao->addParameter(':contactperson', $data['contactperson']);
                $dao->addParameter(':country', $data['country']);
                $dao->addParameter(':is_mainstore', 0);
                $dao->addParameter(':address', $data['address']);
                $dao->addParameter(':city', $data['city']);
                $dao->addParameter(':email', $data['email']);
                $dao->addParameter(':contact', $data['contact']);
                $dao->addParameter(':hashkey', $data['id']);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':storeunkid', $id);

                $dao->executeNonQuery();

//pass store id also


                $ObjAuditDao->addactivitylog($data['module'], $title, $data['id'], $json_data);

                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $default_langlist->REC_UP_SUC)));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addStore - ' . $e);
        }
    }

    public function storelist($limit, $offset, $name, $isactive = 0)
    {
        try {
            $this->log->logIt($this->module . ' - storelist - ' . $name);

            $dao = new \dao;
            $dateformat = \database\parameter::getBanquetParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $strSql = "SELECT Cf.*,IFNULL(CFU1.username,'') AS createduser,IFNULL(Cf.contact,'') AS contactnum,
                                    IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(Cf.createddatetime,'" . $mysqlformat . "'),'') as created_date,
                                    IFNULL(DATE_FORMAT(Cf.modifieddatetime,'" . $mysqlformat . "'),'') as modified_date
                                    FROM " . CONFIG_DBN . ".cfstoremaster AS Cf
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU1 ON Cf.created_user=CFU1.userunkid AND Cf.companyid=CFU1.companyid
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU2 ON Cf.modified_user=CFU2.userunkid AND Cf.modified_user=CFU2.userunkid AND Cf.companyid=CFU2.companyid
                                    
                                    
                                    WHERE Cf.companyid=:companyid AND Cf.is_deleted=0";
            if ($name != "")
                $strSql .= " AND storename LIKE '%" . $name . "%'";

            if ($isactive == 1) {
                $strSql .= " and is_active=1 ";
            }
            $strSql .= " ORDER BY Cf.storeunkid DESC";

            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $data = $dao->executeQuery();
            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);

            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec));
                return html_entity_decode(json_encode($retvalue));
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - storelist - ' . $e);
        }
    }

    public function getstoreRec($data)
    {
        try {
            $this->log->logIt($this->module . " - getstoreRec");

            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $id = $ObjCommonDao->getprimarykey('cfstoremaster', $data['id'], 'storeunkid');

            $strSql = "SELECT CFC.storename,CFC.contactperson,CFC.country,CFC.address,CFC.city,CFC.email,CFC.contact
                      FROM " . CONFIG_DBN . ". cfstoremaster AS CFC
                      WHERE CFC.storeunkid=:storeunkid AND
                      CFC.companyid=:companyid  ";

            $dao->initCommand($strSql);

            $dao->addParameter(':storeunkid', $id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $res = $dao->executeQuery();

            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res)));

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getstoreRec - " . $e);
            return false;
        }
    }
    public function getStoreCount()
    {
        try {
            $this->log->logIt($this->module . " - getStoreCount");

            $dao = new \dao();
            $str = "SELECT meta_value FROM syscompany_meta WHERE syscompanyid=:companyid AND meta_key='total_store'";
            $dao->initCommand($str);
            $dao->addParameter(':companyid', CONFIG_CID);
            $res2 = $dao->executeRow();

            $strSql = "SELECT COUNT(storeunkid) AS total_company_store FROM " . CONFIG_DBN . ".cfstoremaster WHERE is_deleted=0 AND companyid=:companyid  ";
            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $res = $dao->executeRow();

            return json_encode(array('Success' => 'True','Actual_Store_Count' => $res2['meta_value'] ,'Current_Store_Count' => $res['total_company_store']));

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getStoreCount - " . $e);
            return false;
        }
    }
}

?>

