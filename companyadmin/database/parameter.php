<?php
namespace database;
class parameter
{
    public $module = 'DB_parameter';
    public $log;
    public $dbconnect;

    function __construct()
    {
         $this->log = new \util\logger();
    }

    public function getParameter($keyname,$companyid="",$location_id="")
    {
        try
        {
            //$this->log->logIt($this->module.' - getParameter = '.$keyname);
            if($companyid=="")
            {
                $companyid=CONFIG_CID;
            }

            $dao = new \dao();
            $strSql = "SELECT * FROM ".CONFIG_DBN.".cfparameter WHERE keyname = '".$keyname."' AND companyid = ".$companyid." ";

            if($location_id!=''){
                $strSql .= " AND locationid=".$location_id." ";
            }
            $dao->initCommand($strSql);
            $data = $dao->executeRow();
            return $data['keyvalue'];
        }
        catch(Exception $e)
        {
            //$this->log->logIt($this->module.' - getParameter - '.$e);
        }
    }
    public function setParameter($keyname,$keyvalue,$companyid='',$location_id='')
    {
        try
        {
            //$this->log->logIt($this->module.' - setParameter = '.$keyname.' = '.$keyvalue);
            if($companyid=="")
            {
                $companyid=CONFIG_CID;
            }
            if($location_id!="")
            {
                $location_id = $location_id;
            }


            if($keyname==="" || $keyvalue==="")
            {
                return 0;
            }
            $dao = new \dao();
            $ObjAuditDao = new \database\auditlogdao();
            $current_value = self::getParameter($keyname,$companyid,$location_id);
            if($current_value!=$keyvalue)
            {
                $strSql = "UPDATE ".CONFIG_DBN.".cfparameter SET keyvalue='".$keyvalue."' WHERE keyname = '".$keyname."' AND companyid = ".$companyid." AND locationid = ".$location_id." ";
                $dao->initCommand($strSql);
                $dao->executeNonQuery();
                $ObjAuditDao->addsettingslog($keyname,$current_value,$keyvalue);
            }
            return 1;
        }
        catch(Exception $e)
        {
            // $this->log->logIt($this->module.' - setParameter - '.$e);
        }
    }

    public function getBanquetParameter($keyname,$companyid="",$dbname="")
    {
        try
        {
            //$this->log->logIt($this->module.' - getParameter = '.$keyname);
            if($companyid=="")
            {
                $companyid=CONFIG_CID;
            }
            if($dbname==""){
                $dbname=CONFIG_DBN;
            }

            $dao = new \dao();
            $strSql = "SELECT * FROM ".$dbname.".bmdisplaysettings WHERE keyname = '".$keyname."' AND companyid = ".$companyid." ";
            $dao->initCommand($strSql);
            $data = $dao->executeRow();

            return $data['keyvalue'];

        }
        catch(Exception $e)
        {
            //$this->log->logIt($this->module.' - getParameter - '.$e);
        }
    }
}
?>