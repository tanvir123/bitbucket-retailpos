<?php
namespace database;

class application_usersdao
{
    public $module = 'DB_application_usersdao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function userlist($limit,$offset,$username,$email,$mobile,$status)
    {
        try
        {
            $this->log->logIt($this->module.' - application_userlist - '.$username);

            $dao = new \dao;
            $dateformat = \database\parameter::getBanquetParameter('dateformat');
            $timeformat = \database\parameter::getBanquetParameter('timeformat');

            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $mysqltimeformat =  \common\staticarray::$mysqltimeformat[$timeformat];

            $strSql = "SELECT USR.user_id as userid,IFNULL(USR.firstname,'') as firstname,IFNULL(USR.lastname,'') as lastname, IFNULL(USR.phoneno,'') as phoneno,IFNULL(USR.phone_country_code,'') as phone_country_code,USR.email,USR.username,IFNULL(DATE_FORMAT(USR.added_on,'".$mysqlformat."'),'') as created_date,IFNULL(TIME_FORMAT(USR.added_on,'".$mysqltimeformat."'),'') as created_time,IFNULL(DATE_FORMAT(USR.modified_on,'".$mysqlformat."'),'') as modified_date, IFNULL(TIME_FORMAT(USR.modified_on,'".$mysqltimeformat."'),'') as modified_time,USR.status , IFNULL(USR.country,'') as country FROM ".CONFIG_DBN.".user_mst USR WHERE USR.deleted_on IS NULL AND USR.deleted_by IS NULL AND companyid=:companyid  ";
            
            if($username!="")
                $strSql .= " AND USR.username LIKE '%".$username."%'";
            if($email!="")
                $strSql .= " AND USR.email LIKE '%".$email."%'";
            if($mobile!="")
                $strSql .= " AND USR.phoneno LIKE '%".$mobile."%'";
            if($status!="")
                $strSql .= " AND USR.status LIKE '%".$status."%'";

            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $data = $dao->executeQuery();

            if($limit!="" && ($offset!="" || $offset!=0))
                $dao->initCommand($strSql.$strSqllmt);
            else
                $dao->initCommand($strSql);

            $dao->addParameter(':companyid',CONFIG_CID);
            $rec = $dao->executeQuery();
            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec));
                return json_encode($retvalue);
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[]));
                return json_encode($retvalue);
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - application_userlist - '.$e);
        }
    }

    public function appuserorderlist($limit,$offset,$datauser,$langlist)
    {
        try
        {
            $this->log->logIt($this->module.' - app_user_orderlist - ');

            $dao = new \dao;
            $dateformat = \database\parameter::getBanquetParameter('dateformat');
            $timeformat = \database\parameter::getBanquetParameter('timeformat');

            $mysqlformat =  \common\staticarray::$mysqldateformat[$dateformat];
            $mysqltimeformat =  \common\staticarray::$mysqltimeformat[$timeformat];

            $hashid = $datauser['id'];

            $langmylist = $langlist;

            $round_off = \database\parameter::getBanquetParameter('digitafterdecimal');

            $strSql = "SELECT 
CONCAT(IFNULL(firstname,''),' ',IFNULL(lastname,'')) as name FROM ".CONFIG_DBN.".user_mst WHERE user_id=:hashkey";
            $dao->initCommand($strSql);
            $dao->addParameter(':hashkey',$hashid);

            $name = $dao->executeRow();

            $strSql = "SELECT IFNULL(CNT.email,'') as email,IFNULL(LOC.currency_sign,'') as currency_sign ,IFNULL(CNT.phone,'') as phone ,FOL.hashkey as hashkey,FOL.foliono as folionumber,
            IFNULL(DATE_FORMAT(FOL.opendate,'".$mysqlformat."'),'') as created_date,IFNULL(TIME_FORMAT(FOL.opendate,'".$mysqltimeformat."'),'') as created_time,
            IFNULL(ROUND(SUM(CASE WHEN MTYP.mastertypeunkid IN (1,2,3,4,8,9,10) THEN (DTL.baseamount*MAS.crdr*DTL.quantity) ELSE 0 END),".$round_off."),0) as order_product_amount,
             IFNULL(SUM(CASE WHEN MTYP.mastertypeunkid IN (8) THEN (DTL.quantity) ELSE 0 END),0) as order_product_quantity,
             FOL.foliostatus as Orderstatus,            
            IFNULL(CASE FOL.foliostatus 
            WHEN ".FOLIO_STATUS_PENDING." THEN '".$langmylist->FOLIO_STATUS_PENDING."' 
             WHEN ".FOLIO_STATUS_CURRENT." THEN '".$langmylist->FOLIO_STATUS_CURRENT."'  
             WHEN ".FOLIO_STATUS_DELIVERED." THEN '".$langmylist->FOLIO_STATUS_COMPLETED."'  
             WHEN ".FOLIO_STATUS_CANCELLED." THEN '".$langmylist->FOLIO_STATUS_CANCEL."' 
             WHEN ".FOLIO_STATUS_VOID." THEN '".$langmylist->FOLIO_STATUS_VOID."'  
            WHEN ".FOLIO_STATUS_IN_PREPARING." THEN '".$langmylist->FOLIO_STATUS_PREPARING."'  
             WHEN ".FOLIO_STATUS_PLACED." THEN '".$langmylist->FOLIO_STATUS_PLACED."' 
             WHEN ".FOLIO_STATUS_READY." THEN '".$langmylist->FOLIO_STATUS_READY."'
            ELSE ''
            END ,'') as foliostatus,
            IFNULL(CASE FOL.foliostatus 
            WHEN ".FOLIO_STATUS_PENDING." THEN '#b3b300' 
             WHEN ".FOLIO_STATUS_CURRENT." THEN '#b3b300'  
             WHEN ".FOLIO_STATUS_DELIVERED." THEN 'green'  
             WHEN ".FOLIO_STATUS_CANCELLED." THEN 'red' 
             WHEN ".FOLIO_STATUS_VOID." THEN 'red'  
            WHEN ".FOLIO_STATUS_IN_PREPARING." THEN 'green'  
             WHEN ".FOLIO_STATUS_PLACED." THEN '#b3b300' 
             WHEN ".FOLIO_STATUS_READY." THEN 'green'
            ELSE ''
            END ,'') as foliostatuscolour,                         
            USR.status FROM ".CONFIG_DBN.".user_mst As USR 
            INNER JOIN ".CONFIG_DBN.".trcontact As CNT ON CNT.contactpersonunkid=USR.z_userid_pk 
            INNER JOIN ".CONFIG_DBN.".fasfoliomaster As FOL ON FOL.lnkcontactid=CNT.contactunkid AND CNT.contacttype=:contacttype
            INNER JOIN ".CONFIG_DBN.".cflocation As LOC ON LOC.locationid=FOL.locationid 
            INNER JOIN ".CONFIG_DBN.".fasfoliodetail As DTL ON DTL.foliounkid=FOL.foliounkid AND DTL.isvoid=:isvoid
            LEFT JOIN ".CONFIG_DBN.".fasmaster As MAS ON MAS.masterunkid=DTL.masterunkid
            LEFT JOIN ".CONFIG_DBN.".fasmastertype As MTYP ON MTYP.mastertypeunkid=MAS.mastertypeunkid
            WHERE FOL.isvoid=:isvoid AND FOL.companyid=:companyid AND USR.user_id=:hashkey GROUP BY CNT.contactunkid ORDER BY FOL.lnkcontactid DESC ";

            if($limit!="" && ($offset!="" || $offset!=0))
            {
                $strSqllmt =" LIMIT ".$limit." OFFSET ".$offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':contacttype','7');
            $dao->addParameter(':hashkey',$hashid);
            $dao->addParameter(':isvoid','0');

            $data = $dao->executeQuery();

            if($limit!="" && ($offset!="" || $offset!=0))
                  $dao->initCommand($strSql.$strSqllmt);
            else
                  $dao->initCommand($strSql);

            $dao->addParameter(':companyid',CONFIG_CID);
            $dao->addParameter(':contacttype','7');
            $dao->addParameter(':hashkey',$hashid);
            $dao->addParameter(':isvoid','0');
            $rec = $dao->executeQuery();



            if(count($data) != 0){
                $retvalue = array(array("cnt"=>count($data),"data"=>$rec,"uinfo"=>$name['name'],'orderuserid'=> $hashid));
                return json_encode($retvalue);
            }
            else{
                $retvalue = array(array("cnt"=>0,"data"=>[],"uinfo"=>$name['name'],'orderuserid'=> $hashid));
                return json_encode($retvalue);
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - app_user_orderlist - '.$e);
        }
    }
    public function itemlist($datauser,$langlist)
    {
        try
        {
            $this->log->logIt($this->module.' - order item list - ');

            $dao = new \dao;
            $ObjCommonDao = new \database\commondao();

            $dateformat = \database\parameter::getBanquetParameter('dateformat');
            $timeformat = \database\parameter::getBanquetParameter('timeformat');

            $mysql_format =  \common\staticarray::$mysqldateformat[$dateformat];
            $mysqltimeformat =  \common\staticarray::$mysqltimeformat[$timeformat];

            $hashid = $datauser['id'];

            $langmylist = $langlist;

            $round_off = \database\parameter::getBanquetParameter('digitafterdecimal');
//            $mastertypeid = array(8,9,10);
//            $mastertypeid = implode(',',$mastertypeid);

            $folio_id = $ObjCommonDao->getprimarykey('fasfoliomaster',$hashid,'foliounkid');
            $locationid = $ObjCommonDao->getprimarykey('fasfoliomaster',$hashid,'locationid');

            $post_amount = self::getamount($hashid,$locationid);

            $strSql = "SELECT IFNULL(LOC.currency_sign,'') as currency_sign , FASM.name as Uname,FFM.ordertype,FASFD.hashkey,FASFD.detailunkid,CASE WHEN FASM.mastertypeunkid=1 THEN CFCOMBO.comboname WHEN FASM.mastertypeunkid=8 THEN CFI.itemname  WHEN FASM.mastertypeunkid=9 THEN CFMI.itemname WHEN FASM.mastertypeunkid=10 THEN CMM.modifiername ELSE FASM.name END AS particular".
                " ,IFNULL(FASFD.description,'') as comment,FASFD.isvoid,ROUND((FASFD.baseamount*FASM.crdr*FASFD.quantity),".$round_off.") AS Baseamt,ROUND(FASFD.quantity) as quantity".
                " ,IFNULL(CASE WHEN FASM.mastertypeunkid=1 THEN '{{ commonurl }}assets/noimage.png' WHEN FASM.mastertypeunkid=8 THEN CFI.image  WHEN FASM.mastertypeunkid=9 THEN CFMI.image WHEN FASM.mastertypeunkid=10 THEN CMM.image ELSE '{{ commonurl }}assets/noimage.png' END,'{{ commonurl }}assets/noimage.png') As itemimage".
                ", IFNULL(CASE   
            WHEN ( FASFD.folioitem_status=".FOLIO_STATUS_PENDING." AND FASM.mastertypeunkid=8) THEN '".$langmylist->FOLIO_STATUS_PENDING."' 
             WHEN ( FASFD.folioitem_status=".FOLIO_STATUS_CURRENT." AND FASM.mastertypeunkid=8) THEN '".$langmylist->FOLIO_STATUS_CURRENT."'  
             WHEN ( FASFD.folioitem_status=".FOLIO_STATUS_DELIVERED." AND FASM.mastertypeunkid=8) THEN '".$langmylist->FOLIO_STATUS_COMPLETED."'  
             WHEN ( FASFD.folioitem_status=".FOLIO_STATUS_CANCELLED." AND FASM.mastertypeunkid=8) THEN '".$langmylist->FOLIO_STATUS_CANCEL."' 
             WHEN ( FASFD.folioitem_status=".FOLIO_STATUS_VOID." AND FASM.mastertypeunkid=8) THEN '".$langmylist->FOLIO_STATUS_VOID."'  
            WHEN ( FASFD.folioitem_status=".FOLIO_STATUS_IN_PREPARING." AND FASM.mastertypeunkid=8) THEN '".$langmylist->FOLIO_STATUS_PREPARING."'  
             WHEN ( FASFD.folioitem_status=".FOLIO_STATUS_PLACED." AND FASM.mastertypeunkid=8) THEN '".$langmylist->FOLIO_STATUS_PLACED."' 
             WHEN ( FASFD.folioitem_status=".FOLIO_STATUS_READY." AND FASM.mastertypeunkid=8) THEN '".$langmylist->FOLIO_STATUS_READY."'
            ELSE ''
            END ,'') as folioitemstatus".
                " ,IFNULL(CASE  
             WHEN (FASFD.folioitem_status = ".FOLIO_STATUS_PENDING." AND FASM.mastertypeunkid=8) THEN '#b3b300' 
             WHEN (FASFD.folioitem_status =".FOLIO_STATUS_CURRENT." AND FASM.mastertypeunkid=8) THEN '#b3b300'  
             WHEN (FASFD.folioitem_status =".FOLIO_STATUS_DELIVERED." AND FASM.mastertypeunkid=8) THEN 'green'  
             WHEN (FASFD.folioitem_status =".FOLIO_STATUS_CANCELLED." AND FASM.mastertypeunkid=8) THEN 'red' 
             WHEN (FASFD.folioitem_status =".FOLIO_STATUS_VOID." AND FASM.mastertypeunkid=8) THEN 'red'  
             WHEN (FASFD.folioitem_status =".FOLIO_STATUS_IN_PREPARING." AND FASM.mastertypeunkid=8) THEN 'green'  
             WHEN (FASFD.folioitem_status =".FOLIO_STATUS_PLACED." AND FASM.mastertypeunkid=8) THEN '#b3b300' 
             WHEN (FASFD.folioitem_status =".FOLIO_STATUS_READY." AND FASM.mastertypeunkid=8) THEN 'green'
             ELSE ''
            END,'') as folioitemstatuscolour".
             ",IFNULL(CASE  WHEN FASM.mastertypeunkid=1 THEN ''
                            WHEN FASM.mastertypeunkid=8 THEN CFI.short_desc 
                            WHEN FASM.mastertypeunkid=9 THEN CFMI.short_desc 
                            WHEN FASM.mastertypeunkid=10 THEN CMM.short_desc
                            ELSE ''
            END,'') As itemdescription".
            ",IFNULL(DATE_FORMAT(trandate,'".$mysql_format."'),'') as orderdate,FASFD.parentid,FASM.mastertypeunkid,FASFD.masterunkid FROM ".CONFIG_DBN.".fasfoliodetail AS FASFD".
                " INNER JOIN ".CONFIG_DBN.".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid".
                " AND FASM.companyid=:companyid AND FASM.locationid=:locationid LEFT JOIN ".CONFIG_DBN.".cfmenu_items AS CFI".
                " ON CFI.itemunkid=FASFD.lnkmappingunkid AND CFI.companyid=:companyid AND CFI.locationid=:locationid".
                " LEFT JOIN ".CONFIG_DBN.".cfmenu_combo AS CFCOMBO ON CFCOMBO.combounkid=FASFD.lnkmappingunkid AND CFCOMBO.companyid=:companyid AND CFCOMBO.locationid=:locationid" .
                " LEFT JOIN ".CONFIG_DBN.".cfmenu_modifier_items AS CFMI ON CFMI.modifieritemunkid=FASFD.lnkmappingunkid AND CFMI.companyid=:companyid AND CFMI.locationid=:locationid" .
                " LEFT JOIN ".CONFIG_DBN.".cfmenu_modifiers AS CMM ON CMM.modifierunkid=FASFD.lnkmappingunkid AND CMM.locationid=:locationid AND CMM.companyid=:companyid" .
                " LEFT JOIN " . CONFIG_DBN . ".fasfoliomaster AS FFM ON FFM.foliounkid=FASFD.foliounkid AND FFM.companyid=:companyid AND FFM.locationid=:locationid".
                " LEFT JOIN " . CONFIG_DBN . ".cflocation AS LOC ON LOC.locationid=FFM.locationid".
                " WHERE FASFD.foliounkid=:folioid AND FASFD.isvoid=0 AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid ORDER BY FASFD.detailunkid ASC";
            $dao->initCommand($strSql);
            $dao->addParameter(':folioid', $folio_id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', $locationid);
            $res = $dao->executeQuery();

            $resarr = array();
            $retArr = array();
            $tracarr = array();
            $gratuity = $tender = $adjustment = $subamt = $amt = $tax = $disc = $pytype = $price = 0;
            foreach ($res AS $val) {
                if ($val['parentid'] == 0 && ($val['mastertypeunkid'] == 1 || $val['mastertypeunkid'] == 8)) {
                    $retArr[$val['detailunkid']]['item'] = $val;
                    $tracarr[$val['detailunkid']] = $ObjCommonDao->getprimarykey('fasfoliodetail', $val['hashkey'], 'detailunkid');
                    $retArr[$val['detailunkid']]['item']['total'] = $val['Baseamt'];
                }
                if ($val['parentid'] != 0 && ($val['mastertypeunkid'] == 9 || $val['mastertypeunkid'] == 10)) {
                    $retArr[$val['parentid']]['modifier'][] = $val;
                    $retArr[$val['parentid']]['item']['total'] = $retArr[$val['parentid']]['item']['total'] + $val['Baseamt'];
                }
                if ($val['parentid'] != 0 && $val['mastertypeunkid'] == 3) {
                    $retArr[$val['parentid']]['discount'][] = $val;
                }
                if ($val['mastertypeunkid'] == 4)
                    $adjustment = $adjustment + $val['Baseamt'];
                if ($val['mastertypeunkid'] == 11)
                    $gratuity = $gratuity + $val['Baseamt'];
                if ($val['mastertypeunkid'] == 12)
                    $tender = $tender + $val['Baseamt'];
                if ($val['ordertype'] ==2)
                    $disc = $disc + (-$val['discount']);
                if ($val['mastertypeunkid'] == 3)
                    $disc = $disc + $val['Baseamt'];
                if ($val['mastertypeunkid'] == 8 || $val['mastertypeunkid'] == 9 || $val['mastertypeunkid'] == 10 || $val['mastertypeunkid'] == 1)
                    $amt = $amt + $val['Baseamt'];
                if ($val['mastertypeunkid'] == 2)
                    $tax = $tax + $val['Baseamt'];


            }
            $resarr['itemarr'] = $retArr;
            $resarr['amt'] = number_format($amt, $round_off);
            $resarr['disc'] = number_format($disc, $round_off);
            $resarr['tax'] = number_format($tax, $round_off);
            $resarr['adjustment'] = number_format($adjustment, $round_off);
            $resarr['gratuity'] = number_format($gratuity, $round_off);
            $resarr['tender'] = number_format($tender, $round_off);
            $resarr['pytype'] = number_format($pytype, $round_off);
            $resarr['paymentytpe'] = isset($val['Uname'])?$val['Uname']:'';


            /*Get posted payments Name */
            $str6 = "SELECT FASM.name,ROUND(SUM(FASFD.baseamount),".$round_off.") AS amt FROM ".CONFIG_DBN.".fasfoliodetail AS FASFD 
                          LEFT JOIN ".CONFIG_DBN.".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid
                          AND FASM.companyid=:companyid AND FASM.locationid=:locationid WHERE 
                          FASFD.foliounkid=:folioid AND FASM.mastertypeunkid=7
                          AND FASFD.isvoid=0 AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid GROUP BY FASM.masterunkid ";
            $dao->initCommand($str6);
            $dao->addParameter(':folioid', $folio_id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', $locationid);
            $resPayment = $dao->executeQuery();

            foreach ($resPayment AS $key => $value) {
                $price += abs($value['amt']);
            }

            /*Get posted payments Name */
            $strSql1 = "SELECT ctx.is_gst, FASM.name AS applied_tax,SUM((FASFD.baseamount*FASM.crdr*FASFD.quantity)) AS amt" .
                " FROM ".CONFIG_DBN.".fasfoliodetail AS FASFD" .
                " INNER JOIN ".CONFIG_DBN.".fasmaster AS FASM ON FASM.masterunkid=FASFD.masterunkid" .
                " AND FASM.companyid=:companyid AND FASM.locationid=:locationid" .
                " LEFT JOIN ".CONFIG_DBN.".cftax AS ctx ON ctx.lnkmasterunkid=FASM.masterunkid AND ctx.companyid=:companyid AND ctx.locationid=:locationid " .
                " WHERE FASFD.foliounkid=:folioid AND FASM.mastertypeunkid=2 AND FASFD.isvoid=0 AND FASFD.companyid=:companyid AND FASFD.locationid=:locationid";
            $strSql1 .= " GROUP BY ctx.lnkmasterunkid ORDER BY ctx.lnkmasterunkid DESC";
            $dao->initCommand($strSql1);
            $dao->addParameter(':folioid', $folio_id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $dao->addparameter(':locationid', $locationid);
            $res1 = $dao->executeQuery();


            if (count($res1) == 0) {
                $totaltax = 1;
            } else {
                $totaltax = count($res1);
            }

            $resarr['tax_applied'] = $res1;
            $taxvalue = number_format($resarr['tax'] / $totaltax, $round_off);

            $resarr['txvl'] = $taxvalue;
            $resarr['rfrmt'] = $round_off;


            $strSql = " SELECT LOC.locationname,IFNULL(LOC.currency_sign,'') as currency_sign,FM.foliono,FM.ispostedtopms, IFNULL(DATE_FORMAT(FM.opendate,'".$mysql_format."'),'') AS invoice_date,IFNULL(FM.locationmetalnkid,0) AS locationmetalnkid,IFNULL(WM.name,'') AS waiter_name,IFNULL(FM.no_of_person,'') AS no_of_person,".
                " TRC.name , IFNULL(GROUP_CONCAT(CFT.tablename), '') AS tablename FROM ".CONFIG_DBN.".fasfoliomaster AS FM ".
                " LEFT JOIN " . CONFIG_DBN . ".cflocation AS LOC ON LOC.locationid=FM.locationid".
                " LEFT JOIN ".CONFIG_DBN.".trcontact AS TRC ON TRC.contactunkid = FM.lnkcontactid ".
                " LEFT JOIN ".CONFIG_DBN.".waiter_mst AS WM ON WM.z_waiterid_pk = FM.lnkwaiterid ".
                " LEFT JOIN ".CONFIG_DBN.".cfclasstable AS CFT ON find_in_set(CFT.tableunkid,FM.lnktableunkid)  WHERE FM.foliounkid=:folioid AND FM.companyid=:companyid AND FM.locationid=:locationid";
            $dao->initCommand($strSql);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', $locationid);
            $dao->addParameter(':folioid', $folio_id);
            $rec3 = $dao->executeRow();
            $resarr['folioinfo'] = $rec3;
            if($rec3['ispostedtopms']=='1'){
                $resarr['post_amount'] = $post_amount['orderAmt'];
            }
            else{
                $resarr['post_amount'] = 0;
            }
            $resarr['order_total'] = number_format($disc + $amt + $tax, $round_off);
            $resarr['total'] = number_format($disc + $amt + $tax + $adjustment - $price + $gratuity + $tender - $resarr['post_amount'], $round_off);


            $resarr['paymnttype'] = $resPayment;
            $resarr['Language'] = (array)$langlist;


            $resarr = array("data"=>$resarr);
            return json_encode($resarr);

        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - order item list - '.$e);
        }
    }

    public function getamount($folio_id,$locationid)
    {
        try {
            $this->log->logIt($this->module . ' - getamount');
            $dao = new \dao();
            $round_off = \database\parameter::getParameter('digitafterdecimal');
            $strSql = "SELECT IFNULL(ROUND(SUM(ffd.baseamount*fm.crdr*ffd.quantity)," . $round_off . "),0) AS orderAmt , ffm.foliounkid, ffm.foliono 
                       FROM " . CONFIG_DBN . ".fasfoliomaster AS ffm 
                       LEFT JOIN " . CONFIG_DBN . ".fasfoliodetail AS ffd ON ffd.foliounkid = ffm.foliounkid AND ffd.companyid=:companyid AND ffd.locationid=:locationid
                       LEFT JOIN " . CONFIG_DBN . ".fasmaster AS fm ON fm.masterunkid = ffd.masterunkid AND fm.companyid=:companyid AND fm.locationid=:locationid
                       WHERE ffm.hashkey=:hashkey AND ffm.companyid=:companyid AND ffm.locationid=:locationid AND ffd.isvoid=0";
            $dao->initCommand($strSql);
            $dao->addParameter(":hashkey", $folio_id);
            $dao->addParameter(":companyid", CONFIG_CID);
            $dao->addparameter(':locationid', $locationid);
            $res = $dao->executeRow();
            return $res;
        } catch (\Exception $e) {
            $this->log->logIt($this->module . '- getamount -' . $e);

        }
    }

}
?>