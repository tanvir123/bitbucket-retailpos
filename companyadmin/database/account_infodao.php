<?php
namespace database;

class account_infodao
{
    public $module = 'DB_account_infodao';
    public $log;

    function __construct()
    {
        $this->log = new \util\logger();
    }

    public function addeditaccountinfo($data,$langlist='',$default_langlist='')
    {
        try {
            $this->log->logIt($this->module . ' - addeditaccountinfo');
            $dao = new \dao();

            $ObjCommonDao = new \database\commondao();

            $datetime = \util\util::getLocalDateTime();
            $ObjAuditDao = new \database\auditlogdao();
            $tablename = "bmaccountinfo";
            $ObjDependencyDao = new \database\dependencydao();

            $arr_log = array(
                'Account Name' => $data['name'],
                'Alias' => $data['alias'],
                'Account Type' => $data['ac_type_val'],
                'Account Number' => $data['accountno'],
                'First Name' => $data['fname'],
                'Last Name' => $data['lname'],
                'Phone' => $data['phone'],
                'Address1' => $data['address1'],
                'Address2' => $data['address2'],
                'City' => $data['city'],
                'State' => $data['state'],
                'Zip' => $data['zip'],
                'Country' => $data['country'],
                'Fax' => $data['fax'],
                'E-Mail' => $data['email'],
                'Credit Limit' => $data['credit_limit'],
                'Opening Balance' => $data['opening_balance'],
                'Payment Term' => $data['payment_term'],
                'Discount Amount' => $data['discount_amount'],
                'Discount Percentage' => $data['discount_percentage'],
                'Reg. No.' => $data['reg_no'],
                'Reg. No. 1' => $data['reg_no1'],
                'Reg. No. 2' => $data['reg_no2'],
                'Card No.' => $data['card_no'],
                'Remark' => $data['remark'],
                'Status' => ($data['rdo_status'] == 1) ? 'Active' : 'Inactive',
            );
            $json_data = html_entity_decode(json_encode($arr_log));
            $hashkey = \util\util::gethash();
            if ($data['id'] == 0) {
                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "accountname", $data['name']);

                if ($chk_name == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG31)));
                }
                $chk_alias = $ObjDependencyDao->checkduplicaterecord($tablename, "alias", $data['alias']);

                    if ($chk_alias == 1) {
                        return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG32)));
                    }

                $title = "Add Record";

                $strSql = "INSERT INTO " . CONFIG_DBN . ".bmaccountinfo SET
                accountname=:accountname,
                alias=:alias,
                firstname=:firstname,
                lastname=:lastname,
                remark=:remark,
                account_number=:account_number,
                credit_limit=:credit_limit,
                opening_balance=:opening_balance,
                payment_term=:payment_term,
                discount_amount=:discount_amount,
                discount_percentage=:discount_percentage,
                address1=:address1,
                address2=:address2,
                city=:city,
                state=:state,
                zip=:zip,
                country=:country,
                phone=:phone,
                fax=:fax,
                email=:email,
                reg_no=:reg_no,
                reg_no1=:reg_no1,
                reg_no2=:reg_no2,
                card_no=:card_no,
                contact_no=:contact_no,
                account_type=:account_type,
                hashkey=:hashkey,
                companyid=:companyid,
                is_active=:is_active,
                created_user=:created_user,
                createddatetime=:createddatetime";

                $dao->initCommand($strSql);

                $dao->addParameter(':accountname', $data['name']);
                $dao->addParameter(':alias', $data['alias']);
                $dao->addParameter(':firstname', $data['fname']);
                $dao->addParameter(':lastname', $data['lname']);
                $dao->addParameter(':remark', $data['remark']);
                $dao->addParameter(':account_number', $data['accountno']);
                $dao->addParameter(':credit_limit', $data['credit_limit']);
                $dao->addParameter(':opening_balance', $data['opening_balance']);
                $dao->addParameter(':payment_term', $data['payment_term']);
                $dao->addParameter(':discount_amount', $data['discount_amount']);
                $dao->addParameter(':discount_percentage', $data['discount_percentage']);
                $dao->addParameter(':address1', $data['address1']);
                $dao->addParameter(':address2', $data['address2']);
                $dao->addParameter(':city', $data['city']);
                $dao->addParameter(':state', $data['state']);
                $dao->addParameter(':zip', $data['zip']);
                $dao->addParameter(':country', $data['country']);
                $dao->addParameter(':phone', $data['phone']);
                $dao->addParameter(':fax', $data['fax']);
                $dao->addParameter(':email', $data['email']);
                $dao->addParameter(':reg_no', $data['reg_no']);
                $dao->addParameter(':reg_no1', $data['reg_no1']);
                $dao->addParameter(':reg_no2', $data['reg_no2']);
                $dao->addParameter(':card_no', $data['card_no']);
                $dao->addParameter(':contact_no', $data['phone']);
                $dao->addParameter(':account_type', $data['account_type']);
                $dao->addParameter(':hashkey', $hashkey);
                $dao->addParameter(':companyid', CONFIG_CID);
                $dao->addParameter(':is_active', $data['rdo_status']);
                $dao->addParameter(':created_user', CONFIG_UID);
                $dao->addParameter(':createddatetime', $datetime);

                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'], $title, $hashkey, $json_data);

                return json_encode(array('Success' => 'True', 'Message' => $default_langlist->REC_ADD_SUC));
            } else {


                $id = $ObjCommonDao->getprimarykey('bmaccountinfo', $data['id'], 'accountunkid');

                $chk_name = $ObjDependencyDao->checkduplicaterecord($tablename, "accountname", $data['name'], $id);
                if ($chk_name == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG31)));
                }

                $chk_alias = $ObjDependencyDao->checkduplicaterecord($tablename, "alias", $data['alias'],$id);

                if ($chk_alias == 1) {
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG32)));
                }


                $title = "Edit Record";

                $strSql = "Update " . CONFIG_DBN . ".bmaccountinfo SET
                accountname=:accountname,
                alias=:alias,
                firstname=:firstname,
                lastname=:lastname,
                account_number=:account_number,
                credit_limit=:credit_limit,
                opening_balance=:opening_balance,
                payment_term=:payment_term,
                discount_amount=:discount_amount,
                discount_percentage=:discount_percentage,
                address1=:address1,
                address2=:address2,
                city=:city,
                state=:state,
                zip=:zip,
                country=:country,
                phone=:phone,
                fax=:fax,
                email=:email,
                reg_no=:reg_no,
                reg_no1=:reg_no1,
                reg_no2=:reg_no2,
                card_no=:card_no,
                contact_no=:contact_no,
                remark=:remark,         
                account_type=:account_type,         
                modified_user=:modified_user,
                modifieddatetime=:modifieddatetime WHERE companyid=:companyid AND accountunkid=:accountunkid";

                $dao->initCommand($strSql);

                $dao->addParameter(':accountname', $data['name']);
                $dao->addParameter(':alias', $data['alias']);
                $dao->addParameter(':firstname', $data['fname']);
                $dao->addParameter(':lastname', $data['lname']);
                $dao->addParameter(':remark', $data['remark']);
                $dao->addParameter(':account_number', $data['accountno']);
                $dao->addParameter(':credit_limit', $data['credit_limit']);
                $dao->addParameter(':opening_balance', $data['opening_balance']);
                $dao->addParameter(':payment_term', $data['payment_term']);
                $dao->addParameter(':discount_amount', $data['discount_amount']);
                $dao->addParameter(':discount_percentage', $data['discount_percentage']);
                $dao->addParameter(':address1', $data['address1']);
                $dao->addParameter(':address2', $data['address2']);
                $dao->addParameter(':city', $data['city']);
                $dao->addParameter(':state', $data['state']);
                $dao->addParameter(':zip', $data['zip']);
                $dao->addParameter(':country', $data['country']);
                $dao->addParameter(':phone', $data['phone']);
                $dao->addParameter(':fax', $data['fax']);
                $dao->addParameter(':email', $data['email']);
                $dao->addParameter(':reg_no', $data['reg_no']);
                $dao->addParameter(':reg_no1', $data['reg_no1']);
                $dao->addParameter(':reg_no2', $data['reg_no2']);
                $dao->addParameter(':card_no', $data['card_no']);
                $dao->addParameter(':contact_no', $data['phone']);
                $dao->addParameter(':account_type', $data['account_type']);
                $dao->addParameter(':accountunkid', $id);
                $dao->addParameter(':modifieddatetime', $datetime);
                $dao->addParameter(':modified_user', CONFIG_UID);
                $dao->addParameter(':companyid', CONFIG_CID);

                $dao->executeNonQuery();

                $ObjAuditDao->addactivitylog($data['module'], $title, $data['id'], $json_data);

                return html_entity_decode(json_encode(array('Success' => 'True', 'Message' => $default_langlist->REC_UP_SUC)));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addeditaccountinfo - ' . $e);
        }
    }

    public function accountinfolist($limit, $offset, $name, $isactive = 0,$account_type='')
    {
        try {
            $this->log->logIt($this->module . ' - accountinfolist - ' . $name);

            $dao = new \dao;
            $dateformat = \database\parameter::getBanquetParameter('dateformat');
            $mysqlformat = \common\staticarray::$mysqldateformat[$dateformat];
            $roundoff = \database\parameter::getBanquetParameter('digitafterdecimal');
            $strSql = "SELECT Cf.*,IFNULL(CFU1.username,'') AS createduser,IF(Cf.opening_balance>0,ROUND(Cf.credit_limit," . $roundoff . "),'') AS credit_limit_dis,IF(Cf.payment_term>0,Cf.payment_term,'') AS payment_term_dis,IF(Cf.opening_balance>0,ROUND(Cf.opening_balance," . $roundoff . "),'') AS opening_balance_dis,
                                    IFNULL(CFU2.username,'') AS modifieduser,IFNULL(DATE_FORMAT(Cf.createddatetime,'" . $mysqlformat . "'),'') as created_date,
                                    IFNULL(DATE_FORMAT(Cf.modifieddatetime,'" . $mysqlformat . "'),'') as modified_date
                                    FROM " . CONFIG_DBN . ".bmaccountinfo AS Cf
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU1 ON Cf.created_user=CFU1.userunkid AND Cf.companyid=CFU1.companyid
                                    LEFT JOIN " . CONFIG_DBN . ".cfuser as CFU2 ON Cf.modified_user=CFU2.userunkid AND Cf.modified_user=CFU2.userunkid AND Cf.companyid=CFU2.companyid
                                    
                                    
                                    WHERE Cf.companyid=:companyid AND Cf.is_deleted=0";
            if ($name != "")
                $strSql .= " AND (accountname LIKE '%" . $name . "%' OR alias LIKE '%" . $name . "%' OR credit_limit LIKE '%" . $name . "%' OR opening_balance LIKE '%" . $name . "%' OR payment_term LIKE '%" . $name . "%' ) ";

            if ($isactive == 1) {
                $strSql .= " AND is_active=1 ";
            }

            if ($account_type != '') {
                $strSql .= " AND account_type=".$account_type." ";
            }
            $strSql .= " ORDER BY Cf.accountunkid DESC";

            if ($limit != "" && ($offset != "" || $offset != 0)) {
                $strSqllmt = " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);
            $data = $dao->executeQuery();
            if ($limit != "" && ($offset != "" || $offset != 0))
                $dao->initCommand($strSql . $strSqllmt);
            else
                $dao->initCommand($strSql);
            $dao->addParameter(':companyid', CONFIG_CID);

            $rec = $dao->executeQuery();

            if (count($data) != 0) {
                $retvalue = array(array("cnt" => count($data), "data" => $rec ,"roundoff" => $roundoff));
                return html_entity_decode(json_encode($retvalue));
            } else {
                $retvalue = array(array("cnt" => 0, "data" => []));
                return html_entity_decode(json_encode($retvalue));
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - accountinfolist - ' . $e);
        }
    }

    public function getcatRec($data)
    {
        try {
            $this->log->logIt($this->module . " - getcatRec");

            $dao = new \dao();
            $ObjCommonDao = new \database\commondao();
            $id = $ObjCommonDao->getprimarykey('bmaccountinfo', $data['id'], 'accountunkid');

            $strSql = "SELECT CFC.*
                      FROM " . CONFIG_DBN . ". bmaccountinfo AS CFC
                      WHERE CFC.accountunkid=:accountunkid AND
                      CFC.companyid=:companyid  ";

            $dao->initCommand($strSql);

            $dao->addParameter(':accountunkid', $id);
            $dao->addParameter(':companyid', CONFIG_CID);
            $res = $dao->executeQuery();
            return html_entity_decode(json_encode(array("Success" => "True", "Data" => $res)));

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - getcatRec - " . $e);
            return false;
        }
    }


}

?>

