<?php
class companyinformation   
{
    private $module='companyinformation';
    private $log;
    private $language,$lang_arr,$default_lang_arr;
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(17);
            $ObjDao = new \database\companyinformationdao();
            // $data = htmlspecialchars_decode($ObjDao->loadcompanyinformationlist(), ENT_QUOTES);
            $data = $ObjDao->loadcompanyinformationlist();
            $data_arr = json_decode($data,true);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(17);

			$ObjUserDao = new \database\companyinformationdao();
			$countrydata = $ObjUserDao->countrylist();
            $countrylist = json_decode($countrydata,true);
            $this->loadLang();
            $template = $twig->loadTemplate('companyinformation.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['banquet'] = CONFIG_IS_BANQUET;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['datalist'] = $data_arr['Data'];
            $senderarr['countrylist'] = $countrylist[0]['data'];
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo \util\util::convert_html_specials($template->render($senderarr));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    
    public function countrylist()
    {
        try
        {
            $this->log->logIt($this->module.' - countrylist');
            if(isset($_POST) && $_POST['countryName']!="" && $_POST['id']!="")
            {
				$reqarr = array(
					"countryName" => (isset($_POST['countryName']))?$_POST['countryName']:"",         
					"id" => (isset($_POST['id']))?$_POST['id']:""
				);
				$ObjSendRequest = new \common\sendrequest();
				$ObjSendRequest->addService('configuration');
				$ObjSendRequest->addOpcode('countrylist');
				$data = $ObjSendRequest->Request();
				return $data;
            }
            else
				return json_encode(array('Success'=>'False','Message'=>'Some field is missing'));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - Countrylist - '.$e);
        }
    }
       
    public function editCompanyInformation($data)
    {
        try
        {
            $this->log->logIt($this->module.' - editCompanyInformation');
            $flag = \util\validate::check_notnull($data,array('txtcompanyname',
                'txtaddress1',
                'select_country',
                'txtstate',
                'txtcity',
                'txtphoneno',
                'txtrescontactno',
                'txtemail',
                'txtwebsite'
            ));
	
            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $langlist = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);

            if($flag=='true')   
            {
				$flag_file = isset($_FILES['upload_img'])?$_FILES['upload_img']:"";
				$img_flag="";
                if($flag_file['tmp_name']!="")
				{
                    $imagename = $data['imagephoto'];
                    $path = 'assets/company_logo/'.$imagename;
                    if(file_exists($path))
                    {
                        unlink($path);
                    }
				    $temporary = explode(".", $flag_file["name"]);
					$file_extension = end($temporary);
					$flag_name = time().'-'.$flag_file["name"];
					$flag_dirname = dirname(__FILE__).'/assets/company_logo/'.$flag_name;
					$flag_redirname = dirname(__FILE__).'/assets/company_logo/'."re-".$flag_name;
					$flag_db = "";
					
					if ((($flag_file["type"] == "image/png") || ($flag_file["type"] == "image/jpg") || ($flag_file["type"] == "image/jpeg"))
					&& ($flag_file["size"] < 24800000))
					{
						move_uploaded_file($flag_file["tmp_name"],$flag_dirname);
						$flag_resizedFile = "re-".$flag_name;
						$Objutil = new \util\util();
						$newimg = $Objutil->smart_resize_image($flag_dirname,null, 200 , 200 , false , $flag_redirname , true , false ,100);
						
						if($newimg)
								$flag_db = $flag_resizedFile;
						else
								$flag_db = $flag_name;
					}         
                    $img_flag = (file_exists("assets/company_logo/$flag_db"))?$flag_db:"";
				}
				if($img_flag!="")
                {
                    $res = $img_flag;
                }
                else
                {
                   $res = $data['imagephoto']; 
                }

                $reqarr = array(
						"companyname" =>$data['txtcompanyname'],         
						"address1" =>$data['txtaddress1'],
						"address2" => $data['txtaddress2'],
						"country"=>$data['select_country'],
						"state"=>$data['txtstate'],
						"city"=>$data['txtcity'],
						"zipcode"=>isset($data['txtzipcode'])?$data['txtzipcode']:'',
						"phoneno"=>$data['txtphoneno'],
						"reservation_contact_no"=>$data['txtrescontactno'],
						"fax"=>$data['txtfaxno'],
						"email"=>$data['txtemail'],
						"currency_sign"=>$data['txtsymbol'],
						"main_currency"=>$data['txtmaincurrency'],
						"fractional_currency"=>$data['txtfractionalcurrency'],
						"property_type"=>$data['txtpropertytype'],
						"website"=>$data['txtwebsite'],
						"registration_no1"=>$data['txtregno1'],
						"registration_no2"=>$data['txtregno2'],
						"registration_no3"=>$data['txtregno3'],
						"logo" => $img_flag,
						"image" => $res,
						"userid"=> CONFIG_UID,
						"companyid"=> CONFIG_CID
				);
				$ObjDao = new \database\companyinformationdao();
				$result = $ObjDao->editCompanyInformation($reqarr,$default_langlist);
                return $result;
            }else
                return json_encode(array('Success'=>'False','Message'=>$default_langlist->Missing_Field));
             
        }catch(Exception $e){
            $this->log->logIt($this->module.' - editCompanyInformation - '.$e);
        }
    }

    public function loadLang()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$companyinformation;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}
?>