<?php
namespace util;
class validate
{
    public $log;
    public $module='validate';
    public $encdec;
    
    public function min_length($value, $param)
    {
        return !(strlen($value) < $param);
    }
    public function max_length($value, $param)
    {
        return !(strlen($value) > $param);
    }
    public function email($value)
    {
        return (filter_var($value, FILTER_VALIDATE_EMAIL));
    }
    public function required($value)
    {
        return strlen($value) !== 0;
    }
    public function ip($value)
    {
        return (filter_var($value, FILTER_VALIDATE_IP));
    }
    public function match($value, $param)
    {
        return ($value == $param);
    }
    public function match_exact($value, $param)
    {
        return ($value === $param);
    }
    public function match_password($value, $param)
    {
        return ($value === $param);
    }
    public function alphanum($value)
    {
        return (ctype_alnum($value));
    }
    public function url($value)
    {
        return (filter_var($value, FILTER_VALIDATE_URL));
    }
    public function numeric($value)
    {
        return (is_numeric($value));
    }
    public function min($value,$param)
    {
        return !($value < $param);
    }
    public function max($value,$param)
    {
        return !($value > $param);
    }

    public function match_regex($value, $param)
    {
        return (preg_match($param, $value));
    }
    public function check_isset($arr, $field)
    {
        $flag=true;
        foreach($field AS $rec)
        {
            if(!isset($arr[$rec]))
                $flag=false;
        }
        return $flag;
    }
    public function check_notnull($arr, $field)
    {
        $flag='true';
        foreach($field AS $rec)
        {
            if(!isset($arr[$rec]) || $arr[$rec]=='')
                $flag='false';
        }
        return $flag;
    }
    public function check_combo($arr, $field)
    {
        $flag='true';
        foreach($field AS $rec)
        {
            if(!isset($arr[$rec]) || $arr[$rec]=='0')
                $flag='false';
        }
        return $flag;
    }
    public function check_numeric($arr, $field)
    {
        $flag='true';
        foreach($field AS $rec)
        {
            if (!is_numeric($arr[$rec]))
                $flag='false';
            
        }
        return $flag;
    }
    public function check_email($arr, $field)
    {
        $flag='true';
        foreach($field AS $rec)
        {
            if (!filter_var($arr[$rec], FILTER_VALIDATE_EMAIL) === false) {
                $flag='false';
            }
        }
        return $flag;
    }
    public function check_date($arr, $field)
    {
        $flag='true';
        $dateformat = \database\parameter::getBanquetParameter('dateformat');
        foreach($field AS $rec)
        {
            $date = DateTime::createFromFormat($dateformat, $arr[$rec]);
            $date_errors = DateTime::getLastErrors();
            if ($date_errors['warning_count'] + $date_errors['error_count'] > 0) {
                $flag='false';
            }
        }
        return $flag;
    }
    public function check_notzero($arr, $field)
    {
        $flag='true';
        foreach($field AS $rec)
        {
            if ($arr[$rec] == 0)
                $flag='false';
        }
        return $flag;
    }
    
}
?>