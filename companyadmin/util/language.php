<?php
namespace util;
class language
{
	public $module = "language";
    public $redis;
	public $redis_default;
	public $log;
	public $langmodule;
	public $is_redis;
    public $concatuser_id;
    public $concatcompany_locationdefault;
    public $load_default;
	
	function __construct($module=''){
		$this->langmodule = $module;

		if(!defined('CONFIG_CUSTOM_LANG')){
            define('CONFIG_CUSTOM_LANG','');
        }

		if(CONFIG_CUSTOM_LANG==1)
		{
			$this->redis = new \Redis();
			$this->is_redis = $this->redis->connect(CONFIG_REDIS_HOST, CONFIG_REDIS_PORT);
			if($this->is_redis){
				$this->redis->select(15);
				$this->createtable();
			}
            $this->concatuser_id = CONFIG_UID;
            $this->load_default = \common\staticlang::$company_default;
            $this->concatcompany_locationdefault = "company_default";
		}
		$this->log = new \util\logger();
	}
	
	public function createtable()
	{
		try
		{
			if(CONFIG_CUSTOM_LANG==1 && $this->is_redis==1)
			{
                //module lable language

				$isexist = $this->redis->exists("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->langmodule."_".CONFIG_LANG);
				if(!$isexist){
					$this->redis->set("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->langmodule."_".CONFIG_LANG,json_encode(array()));
				}

                //default label language

				$isexist = $this->redis->exists("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->concatcompany_locationdefault."_".CONFIG_LANG);
				if(!$isexist){
					$this->redis->set("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->concatcompany_locationdefault."_".CONFIG_LANG,json_encode(array()));
				}
			}
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." createtable - ".$e);
		}
	}
	public function loadlanguage($arr)
	{
		try
		{
			$this->confirmwithdeflang($arr);
			if(CONFIG_CUSTOM_LANG==1 && $this->is_redis==1)
			{
				$langarr = json_decode($this->redis->get("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->langmodule."_".CONFIG_LANG),true);
				$retarr = array();
				$iswrite = 0;
				foreach($arr AS $key=>$val){
					if(isset($langarr[$key]) && $langarr[$key]!=''){
						$retarr[$key] = $langarr[$key];
					}
					else{
						$iswrite = 1;
						$retarr[$key] = $arr[$key];
					}
				}
				if($iswrite){
					$this->writeredis_lang($retarr);
				}
				return $retarr;
			}
			else{
				return $arr;
			}
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." loadlanguage - ".$e);
		}
	}
	public function writeredis_lang($retarr)
	{
		try{
			if($this->is_redis==1){
				$this->redis->set("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->langmodule."_".CONFIG_LANG,json_encode($retarr));
			}
		}
		catch(Exception $e){
			$this->log->logIt($this->module." writeredis_lang - ".$e);
		}
	}
	public function writeredisdefault_lang($retarr)
	{
		try{
			if($this->is_redis==1){
				$this->redis->set("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->concatcompany_locationdefault."_".CONFIG_LANG,json_encode($retarr));
			}
		}
		catch(Exception $e){
			$this->log->logIt($this->module." writeredis_lang - ".$e);
		}
	}
	public function loaddefaultlanguage()
	{
		try
		{
		    if((defined('CONFIG_IS_BANQUET') && CONFIG_IS_BANQUET =='1') || (defined('CONFIG_IS_STORE') && CONFIG_IS_STORE =='1'))
            {
                $default_arr= json_decode(CONFIG_DEFAULT_ARRAY);
            }
            else{
                 $default_arr = \common\staticlang::$company_default;
            }
			if(CONFIG_CUSTOM_LANG==1 && $this->is_redis==1)
			{
			    $langarr = json_decode($this->redis->get("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->concatcompany_locationdefault."_".CONFIG_LANG),true);
				if($langarr != '')
                {
                    $retarr = array();
                    $iswrite = 0;
                    $default_arr=(array)$default_arr;
                    foreach($default_arr AS $key=>$val){
                        if(isset($langarr[$key]) && $langarr[$key]!=''){
                            $retarr[$key] = $langarr[$key];
                        }
                        else{
                            $iswrite = 1;
                            $retarr[$key] = $default_arr[$key];
                        }
                    }
                    if($iswrite){
                        $this->writeredisdefault_lang($retarr);
                    }
                    return $retarr;
                }
                else{
                    return $default_arr;
                }
			}
			else{
                return $default_arr;
			}
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." loaddefaultlanguage - ".$e);
		}
	}
	public function confirmwithdeflang($arr)
	{
		try
		{
			$redis_0 = new \Redis();
			$red = $redis_0->connect(CONFIG_REDIS_HOST, CONFIG_REDIS_PORT);
			if($red){
				$redis_0->select(0);
                if(!$redis_0->exists("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->langmodule."_".CONFIG_LANG)){
					$iswrite = 1;
				}
				else{
					$langarr = json_decode($redis_0->get("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->langmodule."_".CONFIG_LANG),true);
					$iswrite = 0;
					if(count($langarr)!=count($arr))
					{
						$iswrite = 1;
					}
					else{
						foreach($arr AS $key=>$val){
							if(!isset($langarr[$key]) || $arr[$key] != $langarr[$key]){
								$iswrite = 1;
							}
						}
					}
				}
				if($iswrite){
					$redis_0->set("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->langmodule."_".CONFIG_LANG,json_encode($arr));
				}
			}
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." confirmwithdeflang - ".$e);
		}
	}
	public function confirmdefaultlang()
	{
		try
		{
			$default_arr = \common\staticlang::$company_default;
			$redis_0 = new \Redis();
			$rec = $redis_0->connect(CONFIG_REDIS_HOST, CONFIG_REDIS_PORT);
			if($rec){
				$redis_0->select(0);
				if(!$redis_0->exists("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->concatcompany_locationdefault."_".CONFIG_LANG)){
					$iswrite = 1;
				}
				else{
					$langarr = json_decode($redis_0->get("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->concatcompany_locationdefault."_".CONFIG_LANG),true);
					$iswrite = 0;
					if(count($langarr)!=count($default_arr))
					{
						$iswrite = 1;
					}
					else{
						foreach($default_arr AS $key=>$val){
							if(!isset($langarr[$key]) || $default_arr[$key] != $langarr[$key]){
								$iswrite = 1;
							}
						}
					}
				}
				if($iswrite){
					$redis_0->set("1_".CONFIG_CID."_".$this->concatuser_id."_".$this->concatcompany_locationdefault."_".CONFIG_LANG,json_encode($default_arr));
				}
			}
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module." loaddefaultlanguage - ".$e);
		}
	}
}

?>