<?php

namespace util;
class logger
{
	private $enable=true;
	private $defaultLogFolder;
	private $defaultLogFile;
	private $defaultLogType;
	private $defaultLogMethod;
	private $defaultLogMailId;
	private $defaultEmailSubject;
	
	public function __construct($logfilename='')
	{
		$logsPath =   dirname(__DIR__).'/logs/';
		$this->defaultLogFolder 	= $logsPath;
		
		if($logfilename=='')
			$this->defaultLogFile 		= 'POS_CompanyAdmin'.date('Ymd').'.log';
		else
			$this->defaultLogFile 		= $logfilename."_".date('Ymd').'.log';
		
		if(file_exists($this->defaultLogFolder.$this->defaultLogFile))
		{
			if(filesize($this->defaultLogFolder.$this->defaultLogFile)>20000000)
			{
				rename($this->defaultLogFolder.$this->defaultLogFile,$this->defaultLogFolder.date('Ymd_His').$this->defaultLogFile);
			}
		}
		$this->defaultLogType 		= 'INFO';
		$this->defaultLogMethod 	= 'FILE';
		$this->defaultLogMailId 	= 'pureipos365@gmail.com';
		$this->defaultEmailSubject	= 'Notification';
	}
	
	public function logIt($logData, $logType = '', $logMethod = '', $logFile = '', $logMailId = '')
	{
		if(is_array($logData) || is_object($logData)) 
			$finalLogData = print_r($logData, TRUE);
		else
			$finalLogData = $logData;
	
		if(trim($logType) == '')
			$logType = $this->defaultLogType;
		else
			$logType = strtoupper(strtolower($logType));			
		
		if(trim($logMethod) == '')
			$logMethod = $this->defaultLogMethod;
		
		if(trim($logFile) == '')
			$logFile = $this->defaultLogFolder.$this->defaultLogFile;

		if(trim($logMailId) == '')
			$logMailId = $this->defaultLogMailId;		
		
		if(defined('CONFIG_CID'))
			$finalLogData = "POS - ".CONFIG_CID." - ".$_SERVER['SERVER_NAME']." - ".$this->VisitorIP()." - ".date("Y-m-d H:i:s")." => [".$logType."] ".$finalLogData."\n";
		else
			$finalLogData = "SAAS - ".$_SERVER['SERVER_NAME']." - ".$this->VisitorIP()." - ".date("Y-m-d H:i:s")." => [".$logType."] ".$finalLogData."\n";
			
		switch($logMethod)
		{
			case 'EMAIL':
				if($this->enable)
				{
					//mail($logMailId, $this->defaultEmailSubject, $finalLogData);
				}			
			break;

			case 'FILE':
			default:
				if($this->enable)
				{
					$fp = fopen($logFile, 'a');
					$server = CONFIG_SERVER;
					if($server!="local")
					{
						$findword  = 'Exception';		
						if(preg_match('/'.$findword.'/',$finalLogData))
						{
							$finalLogData="Error Found : ".$finalLogData;						
							if($server!='local')
							{
								if(defined('CONFIG_CID'))
								{
									if($_SERVER['SERVER_NAME']!='192.168.0.114')
									{
										$headers  = 'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										$headers .= 'From: saas <rahul@parghiinfotech.com>'."\r\n";
										//mail('pureipos365@gmail.com', 'Error Found on POS Company Admin', $finalLogData, $headers);
									}	
								}
							}
						}
					}
					fwrite($fp, $finalLogData);
					fclose($fp);
				}
			
			break;
		}
	}
	function VisitorIP()
	{ 
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$TheIp=$_SERVER['HTTP_X_FORWARDED_FOR'];
		else 
			$TheIp=$_SERVER['REMOTE_ADDR'];

		$iparr = explode(',',$TheIp);
		
		if(count($iparr)>0)
		{
			$TheIp=trim($iparr[0]);
		}
		
		return trim($TheIp);
	}
}
?>
