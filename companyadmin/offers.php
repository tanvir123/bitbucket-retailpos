<?php
//require_once(dirname(__FILE__)."/common/s3fileUpload.php");
class offers
{
    public $module = 'offers';
    public $log;
    public $jsdateformat;
    private $language, $lang_arr, $default_lang_arr,$objFunctions;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->objFunctions = new \common\functions();
    }

    public function load()
    {
        try {
            $this->log->logIt($this->module . ' - load');
            global $twig;
            $this->objFunctions->checkModuleAccess(77);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(77);

            $ObjUserDao = new \database\offersdao();
            $data = $ObjUserDao->offerlist(50,'0','','','', 0);

            $datetime = \util\util::getLocalDateTime();
            $jsdate='YYYY-MM-DD';
            $jsdate =  \common\staticarray::$jsbanquetdateformat[\database\parameter::getBanquetParameter('dateformat')];
            $timeformat =  \common\staticarray::$jsbanquettimeformat[\database\parameter::getBanquetParameter('timeformat')];
            $this->loadLang();
            $template = $twig->loadTemplate('offers.html');
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['banquet'] = CONFIG_IS_BANQUET;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['datalist'] = $data;
            $senderarr['jsdateformat'] = $jsdate;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['jstimeformat'] = $timeformat;
            $senderarr['datetime'] = $datetime;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['module'] = $this->module;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);

            echo $template->render($senderarr);
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function addeditfrm($data)
    {
        try {
            $this->log->logIt($this->module . ' - addeditfrm');

            $flag = \util\validate::check_notnull($data, array('startdate','enddate', 'rdo_status','title','slug'));



            $this->loadLang();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $langlist = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);


            if ($flag == 'true') {
//                $bucket=CONFIG_BUCKET_NAME;
                //$directoryname='pure-ipos-company-offer.images';
                $flag_file = isset($_FILES['upload_img'])?$_FILES['upload_img']:"";
                $img_flag="";
                if($flag_file['tmp_name']!="")
                {
                    $directoryname = dirname(__FILE__).'/uploadimg/';
                    if(!is_dir($directoryname)){
                        //Directory does not exist, so lets create it.
                        mkdir($directoryname, 0777, true);
                    }

                    $temporary = explode(".", $flag_file["name"]);
                    $file_extension = end($temporary);
                    $flag_name = time().'-'.$flag_file["name"];
                    $flag_dirname = dirname(__FILE__).'/uploadimg/'.$flag_name;
                    //$flag_redirname = dirname(__FILE__).'/uploadimg/'."re-".$flag_name;
                    $flag_db = "";

                    if ((($flag_file["type"] == "image/png") || ($flag_file["type"] == "image/jpg") || ($flag_file["type"] == "image/jpeg"))
                        && ($flag_file["size"] < 24800000))
                    {
                        move_uploaded_file($flag_file["tmp_name"],$flag_dirname);
//                        $arr_file = array();
//                        $arr_file[0]['name'] = $directoryname.'/'.$flag_name;
//                        $arr_file[0]['tmp_name'] = $flag_file['tmp_name'];
//                        $arr_file[0]['type'] = $flag_file["type"];
//
//                        $ObjFile = new s3fileUpload();
//                        $ObjFile->createBucket($bucket);
//                        $resFile = $ObjFile->uploadFiles($arr_file,$bucket);
//
//                        if($resFile!='' && $resFile!=0){
//                            $img_flag = urldecode($resFile[0]);
                        $flag_db = $flag_name;
                    }
                    $img_flag = (file_exists("uploadimg/$flag_db"))?$flag_db:"";
//                            else{
//                            $img_flag = '';
//                        }
                }


                if($img_flag!=""){
                    $imagename = CONFIG_COMMON_URL.'uploadimg/'.$data['imagephoto'];
                    if($imagename!="")
                    {
//                        $rmv_file = array();
//                        $rmv_file[] = $imagename;
//                        $ObjFile = new s3fileUpload();
//                        $ObjFile->deleteFiles($rmv_file,$bucket);
                        $path = CONFIG_COMMON_URL.'uploadimg/'.$imagename;
                        if(file_exists($path))
                        {
                            unlink($path);
                        }
                    }
                    $res = CONFIG_COMMON_URL.'uploadimg/'.$img_flag;

                }else{
                    $res =$data['imagephoto'];
                }

                if(!$res){
                    return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $langlist->LANG8)));
                }

                $reqarr = array(
                    "startdate" => $data['startdate'],
                    "enddate" => $data['enddate'],
                    "slug" => $data['slug'],
                    "rdo_status" => $data['rdo_status'],
                    "description" => $data['description'],
                    "title" => $data['title'],
                    "id" => $data['id'],
                    "image" => $res,
                    "module" => $this->module
                );
                $ObjMenuDao = new \database\offersdao();
                $data = $ObjMenuDao->addeditoffer($reqarr,$langlist,$default_langlist);
                return $data;
            } else {
                return html_entity_decode(json_encode(array('Success' => 'False', 'Message' => $default_langlist->Missing_Field)));
            }


        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - addeditfrm - ' . $e);
        }
    }

    public function rec($data)
    {
        try {
            $this->log->logIt($this->module . ' - rec');
            $limit = 50;
            $offset = 0;
            $name = $startdate = $enddate = "";

            if (isset($data['limit']) && $data['limit'] != "")
                $limit = $data['limit'];
            if (isset($data['offset']) && $data['offset'] != "")
                $offset = $data['offset'];
            if (isset($data['nm']) && $data['nm'] != "")
                $name = $data['nm'];
            if (isset($data['sd']) && $data['sd'] != "")
                $startdate = $data['sd'];
            if (isset($data['ed']) && $data['ed'] != "")
                $enddate = $data['ed'];
            $ObjUserDao = new \database\offersdao();
            $data = $ObjUserDao->offerlist($limit, $offset, $name,$startdate,$enddate);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . ' - load - ' . $e);
        }
    }

    public function editclass($data)
    {
        try {
            $this->log->logIt($this->module . " - editclass");
            $ObjMenuDao = new \database\offersdao();
            $data = $ObjMenuDao->getcatRec($data);
            return $data;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - editclass - " . $e);
            return false;
        }
    }


    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$offers;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}

?>