<?php
require_once "common/TwigConfig.php";
require_once "config/config.php";
require_once "util/logger.php";
require_once "common/functions.php";
require_once "common/encdec.php";
require_once('config/dbconnect.php');
require_once('common/dao.php');
$server = $_SERVER['HTTP_HOST'];
$oem = (isset($_GET['oem'])  && $_GET['oem']!='') ? $_GET['oem'] : (($server != '35.155.153.166') ? $server : '');

if(isset($_COOKIE['ADMIN_CONFIG_ATID']) && $_COOKIE['ADMIN_CONFIG_ATID']!="" && $_COOKIE['ADMIN_CONFIG_UN']!="")
{
    $objFunctions = new \common\functions();
    $rec = $objFunctions->checkAuth_service($_COOKIE['ADMIN_CONFIG_ATID'],$_COOKIE['ADMIN_CONFIG_UN']);
    if($rec['cnt']>0){
        header("Location:".CONFIG_COMMON_URL."cloud/dashboard");
        exit(0);
    }
    else{
        header("Location:".CONFIG_COMMON_URL."logout.php");
        exit(0);
    }
}

global $twig;
$getOEM = '';
	if($oem!='')
	{
		$oem = preg_replace('#^www\.(.+\.)#i', '$1', $oem);
		$dbconnect  = new dbconnect();
		$dbconnect->config_connect();
		$dao = new dao();
		$dao->initCommand("SELECT * FROM oem_settings WHERE name = :name");
		$dao->addParameter(':name',$oem);
		$getOEM = $dao->executeRow();
		
	}
	$GLOBALS['oemdetails'] = $getOEM;
$template = $twig->loadTemplate('login.html');

$senderarr = array();
$senderarr['commonurl'] = CONFIG_COMMON_URL;
$senderarr['oem'] = $getOEM;
echo $template->render($senderarr);
?>