<?php
namespace common;

class functions
{
    private $log;
    private $module = 'functions';
    public $encdec;

    function __construct()
    {
        try {
            $this->log = new \util\logger();
            $this->encdec = new \common\encdec();
            $this->redis = new \Redis();
            $this->redis->connect(CONFIG_REDIS_HOST, CONFIG_REDIS_PORT);
            $this->redis->select(15);
        } catch (Exception $e) {
            throw $e;
        }
    }

    function addAuthToken($data)
    {
        $this->redis->hmset("pos_admin_atk:" . $data['access_token'], [
                "access_token" => $data['access_token'],
                "dbname" => $data['dbname'],
                "username" => $data['username'],
                "userid" => $data['userid'],
                "sysdefined" => $data['sysdefined'],
                "companyid" => $data['companyid'],
                "lang_type" => $data['lang_type'],
                "lang_code" => $data['lang_code'],
                "user_type" => $data['user_type'],
                "bucket_name" => $data['bucket_name'],
                "groupid" => $data['groupid'],
                "roleid" => $data['roleid'],
                "banquet" => $data['banquet'],
                "store" => $data['store'],
                "domain" => 1,
                "all_default_array" => $data['all_default_array'],
                'oem_name' => $data['oem_name'],
                'oem_logo' => $data['oem_logo'],
                'oem_title' => $data['oem_title'],
                'oem_id' => $data['oem_id'],
            ]
        );
        $this->redis->expire("pos_admin_atk:".$data['access_token'],1800);
        $res_token = $this->redis->hgetall("pos_admin_atk:" . $data['access_token']);
        return $res_token;
    }

    function checkAuth($AuthToken, $Enc_Un)
    {
        try {
            require_once(dirname(__FILE__).'/dao.php');
            $this->log->logIt($this->module);
            $ObjEncDec = new \common\encdec();
            $un = $ObjEncDec->openssl('decrypt', $Enc_Un);
            $check_token = $this->redis->exists("pos_admin_atk:" . $AuthToken);
            if ($check_token == 1) {
                $res_token = $this->redis->hgetall("pos_admin_atk:" . $AuthToken);
                if (isset($res_token['companyid']) && isset($res_token) && $res_token['companyid']!='' && $res_token != '' && $res_token['username'] == $un && $res_token['access_token'] == $AuthToken) {
                    $this->redis->expire("pos_admin_atk:".$AuthToken,1800);
                    $dao = new \dao();
                    $dao->initCommand("SELECT * FROM ".CONFIG_MYSQL_DBNAME.".syscompany WHERE syscompanyid = :syscompanyid AND isactive = :isactive");
                    $dao->addParameter(':syscompanyid',$res_token['companyid']);
                    $dao->addParameter(':isactive',1);
                    $getOEM = $dao->executeRow();
                    if($getOEM)
                    {
                        return $res_token;
                    }
                    return 0;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - setsession -" . $e);
            return json_encode(array("Success" => "False"));
        }
    }

    function checkToken($AuthToken, $un)
    {
        try {
            require_once(dirname(__FILE__).'/dao.php');
            $check_token = $this->redis->exists("pos_admin_atk:" . $AuthToken);
            if ($check_token == 1) {
                $res_token = $this->redis->hgetall("pos_admin_atk:" . $AuthToken);
                if ($res_token != '' && $res_token['username'] == $un && $res_token['access_token'] == $AuthToken) {
                    $dao = new \dao();
                    $dao->initCommand("SELECT * FROM ".CONFIG_MYSQL_DBNAME.".syscompany WHERE syscompanyid = :syscompanyid AND isactive = :isactive");
                    $dao->addParameter(':syscompanyid',$res_token['companyid']);
                    $dao->addParameter(':isactive',1);
                    $getOEM = $dao->executeRow();
                    if($getOEM)
                    {
                        return $res_token;
                    }
                    return 0;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - setsession -" . $e);
            return json_encode(array("Success" => "False"));
        }
    }

    function checkAuth_service($AuthToken, $Enc_Un)
    {
        try {
            $arr = array(
                "service" => "login",
                "opcode" => "checktoken",
                "un" => $Enc_Un,
                "token" => $AuthToken
            );
            $arr_str = json_encode($arr);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, CONFIG_SERVICEURL);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, urlencode($arr_str));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $res = curl_exec($ch);
            curl_close($ch);
            $data = trim($res);
            $arr = json_decode($data, true);
            if ($arr['Success'] == 'True' && $arr['cnt'] > 0) {
                $rec = $arr['Data'];
                return $arr;
            } else {
                header('location: ' . CONFIG_COMMON_URL . 'logout.php');
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - setsession -" . $e);
            return json_encode(array("Success" => "False"));
        }
    }

    function removeToken($AuthToken)
    {
        try {
            $this->log->logIt($this->module);
            $check_token = $this->redis->exists("pos_admin_atk:" . $AuthToken);
            if ($check_token == 1) {
                $this->redis->del("pos_admin_atk:" . $AuthToken);
            } else {
                return 0;
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - setsession -" . $e);
            return json_encode(array("Success" => "False"));
        }
    }

    function fn_flatten_GP_array(array $var, $prefix = FALSE)
    {
        $return = array();
        foreach ($var as $idx => $value) {
            if (is_scalar($value)) {
                if ($prefix) {
                    $return[$prefix . '[' . $idx . ']'] = $value;
                } else {
                    $return[$idx] = $value;
                }
            } else {
                if (is_array($value)) {
                    $return = array_merge($return, fn_flatten_GP_array($value, $prefix ? $prefix . '[' . $idx . ']' : $idx));
                } else {
                    $return = array_merge($return, array($value));
                }
            }
        }
        return $return;
    }

    function checkModuleAccess($groupid)
    {
        try {
            $this->log->logIt($this->module . " checkModuleAccess");
            $arr_privilege = explode(',', CONFIG_GID);
            if (!in_array($groupid, $arr_privilege)) {
                header('location: ' . CONFIG_COMMON_URL . 'cloud/dashboard');
            }

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - checkModuleAccess -" . $e);
        }
    }

    function check_banquet_avilable()
    {
        try {
            $this->log->logIt($this->module . " check_banquet_avilable");

            if (defined('CONFIG_IS_BANQUET') && CONFIG_IS_BANQUET == 0) {
                header("location: " . CONFIG_COMMON_URL . "cloud/dashboard");
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - check_banquet_avilable -" . $e);
        }
    }

    function check_store_avilable()
    {
        try {
            $this->log->logIt($this->module . " check_store_avilable");
            if (defined('CONFIG_IS_STORE') && CONFIG_IS_STORE == 0) {
                header("location: " . CONFIG_COMMON_URL . "cloud/dashboard");
            }
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - check_store_avilable -" . $e);
        }
    }
}

?>
