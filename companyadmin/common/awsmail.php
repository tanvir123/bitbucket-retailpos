<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 6/12/17
 * Time: 3:51 PM
 */
class awsmail
{
    private $log;
    private $module='awsmail';
    function __construct()
    {
        try{
            $this->log = new \util\logger();
        }
        catch(Exception $e){
            throw $e;
        }
    }
    public function verifyEmailAddress($mail)
    {
        try{
            $this->log->logIt($this->module . ' - verifyEmailAddress');
            $url = CONFIG_BASE_URL.'libraries/sesoperations.php';
            $arr = array(
                "operation"=>"verifyEmail",
                "email" => $mail,
            );
            $arr_str = json_encode($arr);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 180);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $arr_str);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen(json_encode($arr)))
            );
            $result = curl_exec($ch);
            curl_close($ch);
            $rec = trim($result);
            $rec1 = json_decode($rec,true);
            return $rec;
        }catch (Exception $e){
            $this->log->logIt($this->module . ' - verifyEmailAddress - ' . $e);
        }
    }
    public function getEmailVerificationStatus($arr_mail)
    {
        try{
            $this->log->logIt($this->module . ' - getEmailVerificationStatus');
            $url = CONFIG_BASE_URL.'libraries/sesoperations.php';
            $arr = array(
                "operation"=>"getVerificationStatus",
                "email" => $arr_mail,
            );
            $arr_str = json_encode($arr);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 180);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $arr_str);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen(json_encode($arr)))
            );
            $result = curl_exec($ch);
            curl_close($ch);
            $rec = trim($result);
            $rec1 = json_decode($rec,true);
            if($rec1['Success']=="True"){
                return $rec1['Data'];
            }else{
                return 0;
            }
        }catch (Exception $e){
            $this->log->logIt($this->module . ' - getEmailVerificationStatus - ' . $e);
        }
    }
    public function deleteEmailFromVerifiedIdentity($mail)
    {
        try{
            $this->log->logIt($this->module . ' - deleteEmailFromVerifiedIdentity');
            $url = CONFIG_BASE_URL.'libraries/sesoperations.php';
            $arr = array(
                "operation"=>"deleteEmail",
                "email" => $mail,
            );
            $arr_str = json_encode($arr);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 180);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $arr_str);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen(json_encode($arr)))
            );
            $result = curl_exec($ch);
            curl_close($ch);
            $rec = trim($result);
            $rec1 = json_decode($rec,true);
            return $rec1;
        }catch (Exception $e){
            $this->log->logIt($this->module . ' - deleteEmailFromVerifiedIdentity - ' . $e);
        }
    }
}
?>