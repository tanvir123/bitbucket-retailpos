<?php
namespace common;
//require("./config/config.php");

class printreport
{
    private $module = "printreport";
    private $template;
    private $record;
    private $parameter=array();
    private $log;
    
    function __construct()
    {
        try
        {
            $this->log = new \util\logger();
            $this->parameter = array();
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
    
    public function addTemplate($template)
    {
        try
        {
            $this->template = $template;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - addTemplate - ".$e);
        }
    }
    
    public function addRecord($record)
    {
        try
        {
            $this->record = $record;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - addRecord - ".$e);
        }
    }
    
    public function Request()
    {
        try
        {
            $this->log->logIt($this->module." -Send Request");
            
            if(CONFIG_UNM!="")
            {
                $arr = array(
                    "template"=>$this->template,
                    "record"=>$this->record,
                );
                $ch=curl_init();
                curl_setopt($ch, CURLOPT_URL, CONFIG_REPORT_URL);									
                curl_setopt($ch, CURLOPT_TIMEOUT, 180);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);	
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arr));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                    'Content-Type: application/json',                                                                                
                    'Content-Length: ' . strlen(json_encode($arr)))                                                                       
                ); 
                       
                $res=curl_exec($ch);
                curl_close($ch);
                $data = trim($res);
                
                $this->parameter = array();
                $this->service = "";
                $this->opcode = "";
                return \util\util::convert_html_specials($data);
            }
            else
                header("location: ".CONFIG_COMMON_URL."amenities");
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - Request - ".$e);
        }
    }
}
?>