<?php
require_once "Twig/Autoloader.php";
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('template');
global $twig;
$twig = new Twig_Environment($loader, array(
    //'cache' => 'template_c',
    'cache' => false,
));
?>