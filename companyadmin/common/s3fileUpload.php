<?php
/**
 * Created by PhpStorm.
 * User: parghi2
 * Date: 5/12/17
 * Time: 11:04 AM
 */
class s3fileUpload
{
    private $log;
    private $module='s3fileUpload';
    function __construct()
    {
        try
        {
            $this->log = new \util\logger();
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
    public function uploadFiles($arr_files,$bucket)
    {
        try{
            $this->log->logIt($this->module . ' - uploadFiles');
            $url = CONFIG_BASE_URL.'libraries/s3files.php';
            $arr = array(
                "operation"=>"uploadFiles",
                "bucket" => $bucket,
                "array_files" => $arr_files,
            );
            $arr_str = json_encode($arr);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 180);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $arr_str);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen(json_encode($arr)))
            );
            $result = curl_exec($ch);
            curl_close($ch);
            $rec = trim($result);
            $resObj = json_decode($rec,true);
            if($resObj['Success']=="True"){
                return $resObj['Data'];
            }else{
                return 0;
            }
        }catch (Exception $e){
            $this->log->logIt($this->module." - uploadFiles -".$e);
        }
    }
    public function deleteFiles($arr_files)
    {
        try{
            $this->log->logIt($this->module . ' - deleteFiles');
            $url = CONFIG_BASE_URL.'libraries/s3files.php';
            $arr = array(
                "operation"=>"deleteFiles",
                "array_files" => $arr_files,
            );
            $arr_str = json_encode($arr);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 180);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $arr_str);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen(json_encode($arr)))
            );
            $result = curl_exec($ch);
            curl_close($ch);
            $rec = trim($result);
            $resObj = json_decode($rec,true);
            return $resObj['Status'];
        }catch (Exception $e){
            $this->log->logIt($this->module." - deleteFiles -".$e);
        }
    }
    public function createBucket($bucket)
    {
        try {

            $this->log->logIt($this->module . ' - createBucket');
            $url = CONFIG_BASE_URL . 'libraries/s3files.php';
            $arr = array(
                "operation" => "createBucket",
                "bucket" => $bucket,
                'array_files' => []
            );
            $arr_str = json_encode($arr);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 180);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $arr_str);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen(json_encode($arr)))
            );

            $result = curl_exec($ch);
            curl_close($ch);
            $rec = trim($result);
            $resObj = json_decode($rec, true);
            return $resObj;
        } catch (Exception $e) {
            $this->log->logIt($this->module . " - createBucket -" . $e);
        }
    }
}
?>