<?php
class document_numbering
{
    private $module='bm_document_numbering';
    private $log;
    private $language,$lang_arr,$default_lang_arr,$objFunctions;
    function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
        $this->objFunctions = new \common\functions();
        $this->objFunctions->check_banquet_avilable();
    }
    
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            
            global $twig;
            $this->objFunctions->checkModuleAccess(46);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(46);

            $Objstaticarray = new \common\staticarray();
            $Objdocdao = new \database\document_numberingdao();
            $docnumdata = $Objdocdao->doclist();
            
            $data = json_decode($docnumdata,true);

            $template = $twig->loadTemplate('document_numbering.html');
            $this->loadLang();
            $senderarr = array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['banquet'] = CONFIG_IS_BANQUET;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data[0]['data'];
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            $reset_language = new \util\language('reset');
            $reset_lang_arr = \common\staticlang::$reset;
            $reset_lang_arr = $reset_language->loadlanguage($reset_lang_arr);
            $rstlanguageArr=html_entity_decode(json_encode($reset_lang_arr),ENT_QUOTES);
            $rstlanglist = json_decode($rstlanguageArr);
            $senderarr['reset'] = (array)$rstlanglist;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    public function rec()
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            global $twig;
            $Objdocdao = new \database\document_numberingdao();
            $accountdata = $Objdocdao->doclist();
            return $accountdata;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    public function docnum($data)
    {
        try
        {
            $this->log->logIt($this->module." - docnum");
            $data['module']=$this->module;
            $ObjDocDao = new \database\document_numberingdao();
            $id = $ObjDocDao->docupdate($data);
            if($id=="" || $id==0)
                return json_encode(array("Success"=>"False",'Data'=>$id));
            else
                return json_encode(array("Success"=>"True",'Data'=>$id));
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module." - docnum - ".$e);
        }
    }

    public function loadLang()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$bm_document_numbering;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}
?>