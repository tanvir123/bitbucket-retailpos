<?php
class dashboard
{
    private $module='dashboard';
    private $log;
    private $encdec;
    private $language, $lang_arr, $default_lang_arr;

    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language();
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;
            $this->loadLang();
            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(48);
            $objcompanyinfo = new \database\companyinformationdao();
            $company_detail = $objcompanyinfo->loadcompanyinformationlist();
            $com= (array)json_decode($company_detail);
            $company_name = $com['Data']->companyname;
            $company_logo = html_entity_decode($com['Data']->logo,ENT_QUOTES);

	        $template = $twig->loadTemplate('dashboard.html');
            $senderarr = array();
            $data = array();
            $data['uname'] = CONFIG_UNM;
            $data['company_name'] = $company_name;
            if(isset($company_logo) && $company_logo!='')
            {
                $data['company_logo'] = CONFIG_BASE_URL."companyadmin/assets/company_logo/".$company_logo;
            }
            else{
                $data['company_logo'] ='';
            }
            $data['pure_logo'] = CONFIG_BASE_URL."companyadmin/assets/company_logo/pureITES.jpg";
            $getOEM = CONFIG_OEM_LOGO;
            if(isset($getOEM) && $getOEM!=''){
                $data['pure_logo'] = CONFIG_BASE_URL."companyadmin/assets/images/".$getOEM.'-logo.png';
            }else{
                $data['pure_logo'] = CONFIG_BASE_URL."companyadmin/assets/company_logo/pureITES.jpg";
            }
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['banquet'] = CONFIG_IS_BANQUET;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            $senderarr['accountdata'] = json_encode($data);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            if(isset($privilegeList) && $privilegeList['lnkprivilegegroupid']=="")
            {
                    $senderarr['not_access_url'] = CONFIG_BASE_URL."companyadmin/assets/block-website.png";
            }
            $senderarr['cid'] = CONFIG_CID;

            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }


    public function loadLang()
    {
        try {
            $this->log->logIt($this->module . " - loadlaguage");
            $this->default_lang_arr = $this->language->loaddefaultlanguage();

        } catch (Exception $e) {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }
}
?>