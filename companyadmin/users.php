<?php
class users   
{
    public $module='users';
    public $log;
    public $jsdateformat;
    private $language,$lang_arr,$default_lang_arr;
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module.' - load');
            global $twig;

            $jsdate = 'd-m-y';
            $jsdate =  \common\staticarray::$jsdateformat[\database\parameter::getBanquetParameter('dateformat')];
            $ObjStaticArray = new \common\staticarray();
            $arr_lang = $ObjStaticArray->language_codes;
            $ObjDao = new \database\usersdao();

            $usr_detail = $ObjDao->getUserDetail();
            $usersrole = $ObjDao->getUserRole();
            $storelist = $ObjDao->getstorelist();

            $ObjDao1 = new \database\userroledao();
            $menulist = $ObjDao1->privileges();



			$data = $ObjDao->userlist(50,'0','');
            $template = $twig->loadTemplate('users.html');
            $this->loadLang();
            $senderarr = array();

            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['banquet'] = CONFIG_IS_BANQUET;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['datalist'] = $data;
            $senderarr['module'] = $this->module;
            $senderarr['userrolelist'] = $usersrole;
            $senderarr['storelist'] = $storelist;
            $senderarr['location_list'] = $ObjDao->getLocationList();
            $senderarr['menulist'] = $menulist;
            $senderarr['jsdateformat'] = $jsdate;
            $senderarr['sysdefined'] = $usr_detail['sysdefined'];
            $senderarr['user_type'] = $usr_detail['user_type'];
            $senderarr['loginuser'] = CONFIG_UID;
            $senderarr['lang_list'] = $arr_lang;
            $senderarr['langlist'] = $this->lang_arr;
            $senderarr['default_langlist'] = $this->default_lang_arr;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;

            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }

    public function addeditfrm($data)
    {
        try
        {
            $this->log->logIt($this->module.' - addeditfrm');



            $flag1 = \util\validate::check_notnull($data,array('shortcode','name','username','password','emailid','mobileno'));

            if($flag1=='true')
            {
                $reqarr = array(
                                "shortcode" => $data['shortcode'],
                                "name" => $data['name'],
                                "username" => $data['username'],
                                "password" => $data['password'],
                                "emailid" => $data['emailid'],
                                "mobileno" => $data['mobileno'],
                                "id"=>$data['id'],                                
                                "storeid"=>$data['select_store'],                              
                                "roleunkid"=>$data['roleunkid'],
                                "usr_type"=>$data['rdo_usr_type'],
                                "usr_location"=>isset($data['slct_location'])?$data['slct_location']:'',
                                "lang_type"=>$data['rdo_lang_type'],
                                "lang_code"=>($data['rdo_lang_type']==1)?$data['select_language']:'en',
                                "module" => $this->module
                              );
                $ObjDao = new \database\usersdao();
                $data = $ObjDao->addUser($reqarr);
                return $data;
                
            }else
                return json_encode(array('Success'=>'False','Message'=>'Some field is missing'));

        }catch(Exception $e){
            $this->log->logIt($this->module.' - addeditfrm - '.$e);
        }
    }

    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            $ObjDao = new \database\usersdao();
			$data = $ObjDao->userlist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - load - '.$e);
        }
    }
    public function displayPrivileges($data)
    {
        try
        {
            $this->log->logIt($this->module." - displayPrivileges");

            $ObjUserDao = new \database\usersdao();
            $res = $ObjUserDao->displayPrivileges($data);
            return $res;
        }catch(Exception $e){
            $this->log->logIt($this->module." - displayPrivileges - ".$e);
            return false;
        }
    }
    public function getUserRec($data)
	{
		try
		{
			$this->log->logIt($this->module." - getUserRec");
			$ObjDao = new \database\usersdao();
			$data = $ObjDao->getUserRec($data);
			return $data;
		}catch(Exception $e){
			$this->log->logIt($this->module." - getUserRec - ".$e);
			return false; 
		}
	}
    public function change_password($data)
    {
        try
        {
            $this->log->logIt($this->module." - change_password");
            $flag1 = \util\validate::check_notnull($data,array('newpwd','id'));
            $data['module'] = $this->module;
            if($flag1=='true')
            {
                $ObjUserDao = new \database\usersdao();
                $res = $ObjUserDao->changePassword($data);
                return $res;
            }else{
                return json_encode(array('Success'=>'False','Message'=>'Some field is missing'));
            }
        }catch(Exception $e){
            $this->log->logIt($this->module." - change_password - ".$e);
            return false;
        }
    }


     public function loadLang()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $arr = array(
                "LANG1" => "Add User",
                "LANG2" => "Name",
                "LANG3" => "Enter Name",
                "LANG4" => "Username",
                "LANG5" => "Mobile No",
                "LANG6" => "Short code",
                "LANG7" => "Name",
                "LANG8" => "Password",
                "LANG9" => "E-Mail ID",
                "LANG10" => "Language Selection",
                "LANG11" => "Default",
                "LANG12" => "Custom",
                "LANG13" => "Select Language",
                "LANG14" => "Change Password",
                "LANG15" => "New Password",
                "LANG16" => "User Type",
                "LANG17" => "Admin User",
                "LANG18" => "Sub User",
                "LANG19" => "Select Location",
                "LANG20" => "Select User Role",
                "LANG21" => "Privileges",
                "LANG22" => "Select Store"
            );
            $this->lang_arr = $this->language->loadlanguage($arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlang - " . $e);
            return false;
        }
    }
}
?>