<?php
class common
{
    private $module='common';
    private $log;
    private $encdec;
    
    public function __construct()
    {
        $this->log = new \util\logger();
    }
    public function toggleststus($data)
	{
		try
		{
			$this->log->logIt($this->module.' - toggleststus');
			$ObjCommonDao = new \database\commondao();
			$res = $ObjCommonDao->toggleStatus($data);
			return $res;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module.' - toggleststus - '.$e);
		}	
	}
	public function remove($data)
	{
		try
		{
			$this->log->logIt($this->module.' - remove');
			$ObjCommonDao = new \database\commondao();
			$res = $ObjCommonDao->removeRecord($data);
			return $res;
		}
		catch(Exception $e)
		{
			$this->log->logIt($this->module.' - remove - '.$e);
		}	
	}
	public function switch_hotel($data)
    {
        try
        {
            $this->log->logIt($this->module . " - switch_hotel");
            $hid = $data['companyid'];
            $objCommondao = new \database\commondao();
            $function = new \common\functions();
            
            $rec = $objCommondao->validateuserforswitch($hid);
            if($rec['Success']=='True')
            {
                $rec = $function->updateauthtoken($hid,$rec['data']['userunkid']);
                if($rec==1){
                    return json_encode(array("Success"=>"True"));
                }
                else{
                    return json_encode(array("Success"=>"Fase", "Message"=>"There is internal error"));
                }
            }else{
                return json_encode(array("Success"=>"Fase", "Message"=>$rec['Message']));
            }
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - switch_hotel - " . $e);
            return false;
        }
    }

    public function statusonchnage($data)
    {
        try
        {
            $this->log->logIt($this->module.' - statusonchnage');
            $ObjCommonDao = new \database\commondao();
            $res = $ObjCommonDao->statusOnchnage($data);
            return $res;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - statusonchnage - '.$e);
        }
    }


}
?>