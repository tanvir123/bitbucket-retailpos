<?php

class userrole
{
    public $module = 'userrole';
    public $log;
    private $language,$lang_arr,$default_lang_arr;
    public function __construct()
    {
        $this->log = new \util\logger();
        $this->language = new \util\language($this->module);
    }
    public function load()
    {
        try
        {
            $this->log->logIt($this->module . ' - load ');
            global $twig;
            $ObjDao = new \database\userroledao();
            $menulist = $ObjDao->privileges();
            $data = $ObjDao->userRolelist(50,'0','');

            $ObjFunctions = new \common\functions();
            $ObjFunctions->checkModuleAccess(24);

            $OBJCOMMONDAO = new \database\commondao();
            $privilegeList = $OBJCOMMONDAO->getuserprivongroup(24);

            $template = $twig->loadTemplate('userrole.html');
            $this->loadlaguage();
            $senderarr=array();
            $senderarr['commonurl'] = CONFIG_COMMON_URL;
            $senderarr['banquet'] = CONFIG_IS_BANQUET;
            $senderarr['store'] = CONFIG_IS_STORE;
            $senderarr['module'] = $this->module;
            $senderarr['datalist'] = $data;
            $senderarr['grpprivlist'] = CONFIG_GID;
            $senderarr['PRIVLIST'] = $privilegeList['lnkprivilegegroupid'];
            //$senderarr['privileges'] = explode(',',CONFIG_PRIVILEGES);
            $senderarr['lang_type'] = CONFIG_CUSTOM_LANG;
            $senderarr['menulist'] = $menulist;
            $senderarr['user_type'] = CONFIG_USR_TYPE;
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $senderarr['langlist'] = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $senderarr['default_langlist'] = json_decode($defaultlanguageArr);
            echo $template->render($senderarr);
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module. ' - load -'.$e);
        }
    }
    public function adduserdetail($data)
    {
        try
        {
            $this->log->logIt($this->module . ' - adddata - ');

            $flag1 = \util\validate::check_notnull($data,array('user_role'));

            $this->loadlaguage();
            $languageArr=html_entity_decode(json_encode($this->lang_arr),ENT_QUOTES);
            $langlist = json_decode($languageArr);
            $defaultlanguageArr=html_entity_decode(json_encode($this->default_lang_arr),ENT_QUOTES);
            $default_langlist = json_decode($defaultlanguageArr);

            if($flag1==true)
            { //menulist reportlist
                $reqarr=array(
                    "user_role"=>$data['user_role'],
                    "privileges"=>$data['privilege'],
                    //"report_privilg"=>$data['reportlist'],
                    "id"=>$data['id'],
                    "rdo_status"=>$data['rdo_status'],
                    "lnkprivilegeid"=>isset($data['menulist'])?$data['menulist']:'',
                   // "privroleunkid"=>$data['privroleunkid'],
                    "module"=>$this->module,
                );
                $objadddao= new \database\userroledao();
                $data =$objadddao->adduserdetail($reqarr,$langlist,$default_langlist);
                return $data;
            }
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module. ' - adduserdetail -'.$e);
        }
    }

    public function rec($data)
    {
        try
        {
            $this->log->logIt($this->module.' - rec');
            $limit=50;
            $offset=0;
            $name="";
            if(isset($data['limit']) && $data['limit']!="")
                $limit = $data['limit'];
            if(isset($data['offset']) && $data['offset']!="")
                $offset = $data['offset'];
            if(isset($data['nm']) && $data['nm']!="")
                $name = $data['nm'];
            $ObjDao = new \database\userroledao();
            $data = $ObjDao->userrolelist($limit,$offset,$name);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module.' - rec - '.$e);
        }
    }
    public function getuserroleRec($data)
    {
        try
        {
            $this->log->logIt($this->module. ' - getuserroleRec -');
            $objDao= new \database\userroledao();
            $data=$objDao->getuserroleRec($data);
            return $data;
        }
        catch(Exception $e)
        {
            $this->log->logIt($this->module. ' - getuserroleRec -'.$e);
            return false;
        }
    }


    public function loadlaguage()
    {
        try
        {
            $this->log->logIt($this->module . " - loadlang");
            $default_lang_arr = \common\staticlang::$userrole;
            $this->lang_arr = $this->language->loadlanguage($default_lang_arr);
            $this->default_lang_arr = $this->language->loaddefaultlanguage();
        }
        catch (Exception $e)
        {
            $this->log->logIt($this->module . " - loadlaguage - " . $e);
            return false;
        }
    }
}

?>